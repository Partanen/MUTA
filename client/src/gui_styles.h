#ifndef MUTA_CLIENT_GUI_STYLES_H
#define MUTA_CLIENT_GUI_STYLES_H

#include "../../shared/gui/gui.h"

extern gui_button_style_t       gui_style_menu_button;
extern gui_button_style_t       gui_style_menu_button_highlighted;
extern gui_text_input_style_t   gui_style_login_text_input;
extern gui_win_style_t          gui_style_topless_window;
extern gui_win_style_t          gui_style_window_1;
extern gui_win_style_t          gui_style_chat_window;
extern gui_button_style_t       gui_style_file_list_button;
extern gui_button_style_t       gui_style_typical_button;
extern gui_button_style_t       gui_style_action_button;
extern gui_button_style_t       gui_style_menu_bar_button;
extern gui_button_style_t       gui_style_inventory_item_button;
extern gui_button_style_t       gui_style_inventory_item_button_moused_over;
extern gui_text_input_style_t   gui_style_chat_text_input;
extern gui_progress_bar_style_t gui_style_player_charge_bar;
extern gui_progress_bar_style_t gui_style_name_plate_health_bar;
extern gui_font_t               *gui_style_menu_font;
extern gui_font_t               *gui_style_menu_font_fancy;

int
gui_styles_init(void);

void
gui_styles_destroy(void);

#endif /* MUTA_CLIENT_GUI_STYLES_H */
