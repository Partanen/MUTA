#ifndef MUTA_CLIENT_SCRIPTS_H
#define MUTA_CLIENT_SCRIPTS_H

#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct ability_script_t ability_script_t;

enum script_ability_request_result
{
    SCRIPT_ABILITY_REQUEST_RESULT_SUCCESS = 0,
    SCRIPT_ABILITY_REQUEST_RESULT_CANT_CAST
};

void
scripts_init(void);

void
scripts_destroy(void);

ability_script_t *
scripts_find_ability(ability_id_t id);

#endif /* MUTA_CLIENT_SCRIPTS_H */
