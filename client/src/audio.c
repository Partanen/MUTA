#include "audio.h"
#include "assets.h"
#include "../../shared/common_utils.h"
#include "../../shared/mojoAL/AL/al.h"
#include "../../shared/mojoAL/AL/alc.h"

#ifdef __GNUC__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wunused-value"
    #include "../../shared/stb/stb_vorbis.c"
    #pragma GCC diagnostic pop
#else
    #include "../../shared/stb/stb_vorbis.c"
#endif

#define MAX_QUEUED_SOUNDS MAX_SOUND_CHANNELS

typedef struct queued_sound_t queued_sound_t;

enum queued_play_types
{
    QS_PLAY_SOUND_ASSET,
    QS_PLAY_SOUND_ASSET_ON_CHANNEL,
    QS_PLAY_MUSIC_ASSET_ON_CHANNEL
};

struct queued_sound_t
{
    as_sound_t   *sa;
    int             channel;
    float           volume, pitch;
    bool32          loop;
    int8            play_type;
};

float master_sound  = 1.0f;
float master_music  = 1.0f;

static bool32       initialized = 0;
ALCdevice           *sound_device;
ALCcontext          *sound_context;

ALfloat             position[3];
ALfloat             velocity[3];
ALfloat             orientation[6];

uint                channels[MAX_SOUND_CHANNELS];
bool32              locked_channels[MAX_SOUND_CHANNELS];

queued_sound_t      queued_sounds[MAX_QUEUED_SOUNDS];
uint32              num_queued_sounds;

int
au_play_sound_asset(as_sound_t *sa, float volume)
{
    if (!sa)
        return 1;
    if (!sa->loading)
        return au_play_sound(sa->sound, volume);
    if (num_queued_sounds == MAX_QUEUED_SOUNDS)
        return 2;
    queued_sound_t *qs = &queued_sounds[num_queued_sounds++];
    qs->sa          = sa;
    qs->volume      = volume;
    qs->play_type   = QS_PLAY_SOUND_ASSET;
    return 0;
}

int
au_play_sound_asset_on_channel(as_sound_t *sa, int channel, float volume,
    float pitch, bool32 loop)
{
    if (!sa)
        return 1;
    if (!sa->loading)
        return au_play_sound_on_channel(sa->sound, channel, volume, pitch, loop);
    if (num_queued_sounds == MAX_QUEUED_SOUNDS)
        return 2;
    queued_sound_t *qs = &queued_sounds[num_queued_sounds++];
    qs->sa          = sa;
    qs->volume      = volume;
    qs->play_type   = QS_PLAY_SOUND_ASSET_ON_CHANNEL;
    qs->channel     = channel;
    qs->pitch       = pitch;
    qs->loop        = loop;
    return 0;
}

int
au_play_music_asset_on_channel(as_sound_t *sa, int channel, float volume,
    float pitch, bool32 loop)
{
    if (!sa)
        return 1;
    if (!sa->loading)
        return au_play_music_on_channel(sa->sound, channel, volume, pitch, loop);
    if (num_queued_sounds == MAX_QUEUED_SOUNDS)
        return 2;
    queued_sound_t *qs = &queued_sounds[num_queued_sounds++];
    qs->sa          = sa;
    qs->volume      = volume;
    qs->play_type   = QS_PLAY_MUSIC_ASSET_ON_CHANNEL;
    qs->channel     = channel;
    qs->pitch       = pitch;
    qs->loop        = loop;
    return 0;
}

int
au_init()
{
    if (initialized == 1)
        return 1;

    initialized     = 0;
    sound_device    = 0;
    sound_context   = 0;

    position[0]     = 0.0;
    position[1]     = 0.0;
    position[2]     = 0.0;

    velocity[0]     = 0.0;
    velocity[1]     = 0.0;
    velocity[2]     = 0.0;

    orientation[0]  = 0.0;
    orientation[1]  = 0.0;
    orientation[2]  = -1.0;
    orientation[3]  = 0.0;
    orientation[4]  = 1.0;
    orientation[5]  = 0.0;

    num_queued_sounds = 0;

    for (int i = 0; i < MAX_SOUND_CHANNELS; ++i)
    {
        channels[i]             = 0;
        locked_channels[i]      = 0;
    }

    /*-- Initializing Device & Context --*/

    sound_device = alcOpenDevice(0);

    if (!sound_device)
    {
        DEBUG_PRINTF ("Error 404 Sound Device not found!\n");
        return -1;
    }

    sound_context = alcCreateContext(sound_device, 0);

    if (!sound_context)
    {
        int err = alcGetError(sound_device);
        DEBUG_PRINTF("Error 404 Sound Context not found! Error %d.\n", err);
        return -2; 
    }

    alcMakeContextCurrent (sound_context);

    if (au_check_al_error())
        return -3;


    /*-- Initializing Sources --*/

    alGenSources(MAX_SOUND_CHANNELS, channels);
    if (au_check_al_error())
        return -4;

    /*-- Setting Listener --*/

    alListenerfv(AL_POSITION,       position);
    alListenerfv(AL_VELOCITY,       velocity);
    alListenerfv(AL_ORIENTATION,    orientation);

    alDopplerFactor(1.0);
    alDopplerVelocity(343.0f); //?

    initialized = 1;

    DEBUG_PRINTF("Initialized audio successfully!\n");

    return 0;
}

int
au_destroy()
{
    alDeleteSources(MAX_SOUND_CHANNELS, channels);

    sound_context = alcGetCurrentContext();
    sound_device  = alcGetContextsDevice( sound_context );

    alcMakeContextCurrent(0);
    alcDestroyContext(sound_context);

    if (sound_device)
        alcCloseDevice(sound_device);

    return 0;
}

int
au_create_buffer(uint *buffer, const char *file)
{
    if (!initialized)
        return 1;

    alGetError();

    if (*buffer == 0)
        alGenBuffers(1, buffer);

    if (*buffer == 0)
    {
        DEBUG_PRINTFF("failed to generate audio buffer.\n");
        return 2;
    }

    char temp[128];
    strcpy(temp, file);

    char *file_extension;
    file_extension = strtok(temp, ".");

    if (!file_extension)
    {
        DEBUG_PRINTFF("invalid filetype or file not found '%s'\n", file);
        *buffer = 0;
        return 3;
    }

    file_extension = strtok(0, ".");

    if (!file_extension)
    {
        DEBUG_PRINTFF("invalid filetype or file not found '%s'\n", file);
        *buffer = 0;
        return 4;
    }

    if (!str_insensitive_cmp(file_extension, "wav"))
    {
        if (au_load_wav(buffer, file) != 0)
            *buffer = 0;
    } else
    if (!str_insensitive_cmp(file_extension, "ogg"))
    {
        if (au_load_ogg(buffer, file) != 0)
            *buffer = 0;
    } else
    {
        DEBUG_PRINTFF("invalid filetype or file not found '%s'\n", file);
        au_check_al_error();
        *buffer = 0;
        return 5;
    }

    if (*buffer <= 0)
    {
        DEBUG_PRINTFF("invalid filetype or file not found '%s'\n", file);
        *buffer = 0;
        return 6;
    }

    return 0;
}

int
au_destroy_buffer(uint *buf)
{
    alDeleteBuffers(1, buf);
    int ret = au_check_al_error();
    if (!ret) *buf = 0;
    return ret;
}

int
au_load_sound_to_channel(sound_t sound, uint channel)
{
    if (channel >= MAX_SOUND_CHANNELS)
        return 1;

    ALint sound_on_channel = 0;
    alGetSourcei(channels[channel], AL_BUFFER, &sound_on_channel);

    if (sound.buffer == sound_on_channel)
        return 0;

    alSourcei(channels[channel], AL_BUFFER, sound.buffer);

    if (au_check_al_error())
        return 1;

    return 0;
}

int
au_load_wav(au_buf_t *dest_buffer, const char *file)
{
    alGetError();

    FILE *fp = 0;
    fp = fopen(file, "rb");

    if (!fp)
        return 1;

    char type[4];
    uint size, chunk_size;
    short format_type, audio_channels;
    uint sample_rate, avg_bytes_per_sec;
    short bytes_per_sample, bits_per_sample;
    uint data_size;

    fread(type, sizeof(char), 4, fp);
    if (type[0] != 'R' || type[1] != 'I' || type[2] != 'F' || type[3] != 'F')
        {DEBUG_PRINTFF("no RIFF\n"); safe_fclose(fp); return 1;}

    fread(&size, sizeof(uint), 1, fp);
    fread(type, sizeof(char), 4, fp);
    if (type[0] != 'W' || type[1] != 'A' || type[2] != 'V' || type[3] != 'E')
        {DEBUG_PRINTFF("not WAVE\n"); safe_fclose(fp); return 1;}

    fread(type, sizeof(char), 4, fp);
    if (type[0] != 'f' || type[1] != 'm' || type[2] != 't' || type[3] != ' ')
        {DEBUG_PRINTFF("not fmt\n"); safe_fclose(fp); return 1;}

    fread(&chunk_size, sizeof(uint), 1, fp);
    fread(&format_type, sizeof(short), 1, fp);
    fread(&audio_channels, sizeof(short), 1, fp);
    fread(&sample_rate, sizeof(uint), 1, fp);
    fread(&avg_bytes_per_sec, sizeof(uint), 1, fp);
    fread(&bytes_per_sample, sizeof(short), 1, fp);
    fread(&bits_per_sample, sizeof(short), 1, fp);

    fread(type, sizeof(char), 4, fp);
    if (type[0] != 'd' || type[1] != 'a' || type[2] != 't' || type[3] != 'a')
    { DEBUG_PRINTF("\nMissing DATA\n"); safe_fclose(fp); return 1; }

    fread(&data_size, sizeof(uint), 1, fp);

    unsigned char* buf = calloc(1, data_size);
    fread(buf, sizeof(char), data_size, fp);

    ALuint frequency = sample_rate;
    ALenum format = 0;

    int res = 0;

    switch (bits_per_sample)
    {
        case 8:
            switch (audio_channels)
            {
                case 1:
                    format = AL_FORMAT_MONO8;
                    break;
                case 2:
                    format = AL_FORMAT_STEREO8;
                    break;
            }
            break;
        case 16:
            switch (audio_channels)
            {
                case 1:
                    format = AL_FORMAT_MONO16;
                    break;
                case 2:
                    format = AL_FORMAT_STEREO16;
                    break;
            }
            break;
        default:
            DEBUG_PRINTF("\nSomething wrong in bits per sample\n");
            res = 1;
        break;
    };

    if (res != 0)
        alBufferData(*dest_buffer, format, buf, data_size, frequency);

    free(buf);
    fclose(fp);

    if (res != 0)
        return 1;

    if (au_check_al_error())
        return 2;

    return 0;
}

int
au_load_ogg(au_buf_t *dest_buffer, const char* file)
{
    alGetError();

    int audio_channels, sample_rate, samples;
    short *buf;

    samples = stb_vorbis_decode_filename(file, &audio_channels, &sample_rate,
        &buf);
    if (samples < 0)
        return 1;

    ALuint freq     = sample_rate;
    ALenum format   = 0;

    switch (audio_channels)
    {
        case 1:
            format = AL_FORMAT_MONO16;
            break;
        default:
            format = AL_FORMAT_STEREO16;
            break;
    }

    int data_size = samples * audio_channels * sizeof(short);

    alBufferData(*dest_buffer, format, buf, data_size, freq);
    free(buf);

    if (au_check_al_error())
        return 1;

    return 0;
}

int
au_play_sound(sound_t sound, float volume)
{
    volume *= master_sound;

    if(sound.buffer <= 0 || volume <= 0)
        return 1;

    int channel = -1;

    if (au_get_free_channel(&channel))
        return 2;

    if (!au_load_sound_to_channel(sound, channel))
        return au_start_sound(channel, volume, 1.0f, 0) == 0 ? 0 : 3;

    return 4;
}

int
au_play_channel(uint channel)
{
    if (channel >= MAX_SOUND_CHANNELS)
        return 1;

    ALint sound_on_channel = 0;
    alGetSourcei(channels[channel], AL_BUFFER, &sound_on_channel);

    if (sound_on_channel == 0)
        return 1;

    if (au_get_channel_state(channel) == AL_PLAYING)
        au_stop_channel(channel);

    alSourcePlay(channels[channel]);

    if (au_check_al_error())
        return 1;

    return 0;
}

int
au_play_sound_on_channel(sound_t sound, int channel, float volume, float pitch,
    bool32 loop)
{
    volume *= master_sound;

    if (sound.buffer <= 0 || volume <= 0)
        return 1;

    if (channel < 0 && au_get_free_channel(&channel))
        return 1;

    if (!au_load_sound_to_channel(sound, channel))
        return au_start_sound(channel, volume, pitch, loop);

    return 1;
}

int
au_play_music_on_channel(sound_t sound, int channel, float volume, float pitch,
    bool32 loop)
{
    //Only difference to 'play_sound_on_channel' is this multiplier :^)
    volume *= master_music;

    if (volume <= 0.f)
        return 1;
    if (sound.buffer <= 0)
        return 2;
    if (channel < 0)
        if (au_get_free_channel(&channel))
            return 3;
    if (!au_load_sound_to_channel(sound, channel))
        return au_start_sound(channel, volume, pitch, loop) == 0 ? 0 : 4;

    return 5;
}

int
au_start_sound(uint channel, float volume, float pitch, bool32 loop)
{
    if (volume <= 0 || channel >= MAX_SOUND_CHANNELS || !initialized)
        return 1;

    if (au_get_channel_state(channel) == AL_PLAYING)
        au_stop_channel(channel);

    alSourcef(channels[channel], AL_GAIN, CLAMP(volume, 0.001f, 1.0f));
    alSourcef(channels[channel], AL_PITCH, CLAMP(pitch, 0.1f, 4.0f));
    alSourcei(channels[channel], AL_LOOPING, loop);
    alSourcePlay(channels[channel]);

    if (au_check_al_error())
        return 1;

    return 0;
}

int
au_pause_channel(uint channel)
{
    if (channel >= MAX_SOUND_CHANNELS)
        return 1;

    alGetError();

    alSourcePause(channels[channel]);

    if (au_check_al_error())
        return 1;

    return 0;
}

int
au_pause_all_channels()
{
    alGetError();

    alSourcePausev(MAX_SOUND_CHANNELS - 1, channels);

    if (au_check_al_error())
        return 1;

    return 0;
}

int
resume_channel(uint channel)
{
    if (channel >= MAX_SOUND_CHANNELS)
        return 1;

    alGetError();

    if (au_get_channel_state(channel) == AL_PAUSED)
        alSourcePlay(channels[channel]);

    if (au_check_al_error())
        return 1;

    return 0;
}

int
au_resume_all_channels()
{
    alGetError();

    for (uint i = 0; i < MAX_SOUND_CHANNELS; ++i)
    {
        if (au_get_channel_state(i) == AL_PAUSED)
            resume_channel(i);
    }

    if (au_check_al_error())
        return 1;

    return 0;
}

int
au_stop_sound(sound_t sound)
{
    for (uint i = 0; i < MAX_SOUND_CHANNELS; ++i)
    {
        ALint sound_on_channel = 0;
        alGetSourcei(channels[i], AL_BUFFER, &sound_on_channel);

        if (sound.buffer == sound_on_channel)
            return (au_stop_channel(i));
    }

    return 1;
}

int
au_stop_sounds(sound_t sound)
{
    for (uint i = 0; i < MAX_SOUND_CHANNELS; ++i)
    {
        ALint sound_on_channel = 0;
        alGetSourcei(channels[i], AL_BUFFER, &sound_on_channel);

        if (sound.buffer == sound_on_channel)
            if (au_stop_channel(i))
                return 1;
    }

    return 1;
}

int
au_stop_channel(uint channel)
{
    if (channel >= MAX_SOUND_CHANNELS)
        return 1;

    alGetError();

    alSourceStop(channels[channel]);

    if (au_check_al_error())
        return 1;

    return 0;
}

int
au_stop_all_channels()
{
    for (uint i = 0; i < MAX_SOUND_CHANNELS; ++i)
        alSourceStop(channels[i]);
    return 0;
}

int
au_get_free_channel(int *channel)
{
    for (int i = 0; i < MAX_SOUND_CHANNELS; ++i)
    {
        if (au_get_channel_state(i) != AL_PLAYING && au_get_channel_state(i) != AL_PAUSED)
        {
            if (!locked_channels[i])
            {
                *channel = i;
                return 0;
            }
        }
    }

    return 1;
}

int
au_get_channel_state(uint channel)
{
    if(channel >= MAX_SOUND_CHANNELS)
        return 0;

    int state = 0;
    alGetSourcei(channels[channel], AL_SOURCE_STATE, &state);

    return state;
}

int
au_check_al_error()
{
    ALenum error = 0;

    error = alGetError();

    if (error == AL_NO_ERROR)
        return 0;

    switch (error)
    {
        case AL_INVALID_NAME:
            DEBUG_PRINTF("ERROR: AL_INVALID_NAME\n");
            break;
        case AL_INVALID_ENUM:
            DEBUG_PRINTF("ERROR: AL_INVALID_ENUM\n");
            break;
        case AL_INVALID_VALUE:
            DEBUG_PRINTF("ERROR: AL_INVALID_VALUE\n");
            break;
        case AL_INVALID_OPERATION:
            DEBUG_PRINTF("ERROR: AL_INVALID_OPERATION\n");
            break;
        case AL_OUT_OF_MEMORY:
            DEBUG_PRINTF("ERROR: AL_OUT_OF_MEMORY\n");
            break;
        default:
            DEBUG_PRINTF("ERROR: SOMETHING WENT WRONG\n");
            break;
    }

    return 1;
}

int
au_set_channel_lock(uint channel, bool32 locked)
{
    if (channel >= MAX_SOUND_CHANNELS)
        return 1;

    locked_channels[channel] = locked;

    return 0;
}

int
au_set_channel_volume(uint channel, float channel_vol)
{
    channel_vol *= master_sound;

    if (channel >= MAX_SOUND_CHANNELS || channel_vol <= 0)
        return 1;

    alGetError();

    alSourcef(channels[channel], AL_GAIN, CLAMP(channel_vol, 0.001f, 1.0f));

    if (au_check_al_error())
        return 1;

    return 0;
}

void
au_set_master_volume(float sound_vol, float music_vol)
{
    master_sound = CLAMP(sound_vol, 0.0f, 1.0f);
    master_music = CLAMP(music_vol, 0.0f, 1.0f);
}

void
au_update()
{
    uint32 num = num_queued_sounds;

    queued_sound_t *qs;
    for (uint32 i = 0; i < num; ++i)
    {
        qs = &queued_sounds[i];
        if (qs->sa->loading)
            continue;
        if (!qs->sa->errors_loading)
        {
            switch (qs->play_type)
            {
                case QS_PLAY_SOUND_ASSET:
                    au_play_sound(qs->sa->sound, qs->volume);
                    break;
                case QS_PLAY_SOUND_ASSET_ON_CHANNEL:
                    au_play_sound_on_channel(qs->sa->sound, qs->channel,
                        qs->volume, qs->pitch, qs->loop);
                    break;
                case QS_PLAY_MUSIC_ASSET_ON_CHANNEL:
                    au_play_music_on_channel(qs->sa->sound, qs->channel,
                        qs->volume, qs->pitch, qs->loop);
                    break;
            }
        }
        memcpy(qs, qs + 1, --num_queued_sounds - i);
        num = num_queued_sounds;
    }
}

void
au_dequeue_sound_asset(as_sound_t *sa)
{
    uint32          num = num_queued_sounds;
    queued_sound_t  *qs;
    for (uint32 i = 0; i < num; ++i)
    {
        qs = &queued_sounds[i];
        if (qs->sa != sa)
            continue;
        memcpy(qs, qs + 1, --num_queued_sounds - i);
        num = num_queued_sounds;
        break;
    }
    num_queued_sounds = num;
}
