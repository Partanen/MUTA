#include "icon.h"
#include "assets.h"
#include "core.h"
#include "log.h"
#include "../../shared/common_utils.h"

as_spritesheet_t    *_spritesheet;
float               *_question_mark_clip;
cursor_t            *_question_mark_cursor;
img_t               *_tex_bitmap;
cursor_t            **_cursors;

int
icon_init(void)
{
    _spritesheet = as_claim_spritesheet(core_asset_config.icons_spritesheet_id,
        0);
    if (!_spritesheet)
    {
        LOG_ERR("Icons spritesheet not found - check ID in asset_config.cfg.");
        core_error_window("Icons spritesheet not found -  check ID in "
            "asset_config.cfg");
        return 1;
    }
    _question_mark_clip = as_spritesheet_get_clip(_spritesheet,
        "question_mark");
    if (!_question_mark_clip)
    {
        LOG_ERR("Icon with id 'question_mark' not defined in icon "
            "spritesheet.");
        core_error_window("Icon with id 'question_mark' not defined in icon "
            "spritesheet");
        return 1;
    }
    _tex_bitmap = as_tex_get_bitmap(_spritesheet->ta);
    if (!_tex_bitmap)
    {
        LOG_ERR("Icon texture does not have 'keep bitmap' set to "
            "non-zero in textures.mdb.");
        core_error_window("Icon texture does not have 'keep bitmap' set to "
            "non-zero in textures.mdb.");
        return 1;
    }
    /* Create a cursor for each icon. */
    int num_clips = _spritesheet->num_clips;
    _cursors = emalloc(num_clips * sizeof(*_cursors));
    for (int i = 0; i < num_clips; ++i)
    {
        const char  *clip_name  = _spritesheet->clip_names[i];
        float       *clip       = _spritesheet->clips[i];
        cursor_t    *cursor     = core_new_unmanaged_cursor(_tex_bitmap, clip);
        if (!cursor)
        {
            LOG_ERR("Failed to create cursor for icon clip '%s'.", clip_name);
            core_error_window("Failed to create cursor for icon clip '%s'.",
                clip_name);
            return 1;
        }
        _cursors[i] = cursor;
    }
    _question_mark_cursor = icon_get_cursor("question_mark");
    return 0;
}

void
icon_destroy(void)
{
    if (_spritesheet)
    {
        int num_clips = _spritesheet->num_clips;
        for (int i = 0; i < num_clips; ++i)
            core_free_unmanaged_cursor(_cursors[i]);
        free(_cursors);
    }
    as_unclaim_spritesheet(_spritesheet);
}

tex_t *
icon_get_tex(void)
    {return &_spritesheet->ta->tex;}

img_t *
icon_get_bitmap(void)
    {return _tex_bitmap;}

float *
icon_get_clip(const char *id, bool32 *ret_is_unknown)
{
    float *clip;
    if (id)
        clip = as_spritesheet_get_clip(_spritesheet, id);
    else
        clip = 0;
    if (!clip)
    {
        clip = _question_mark_clip;
        if (ret_is_unknown)
            *ret_is_unknown = 1;
    } else if (ret_is_unknown)
        *ret_is_unknown = 0;
    return clip;
}

cursor_t *
icon_get_cursor(const char *id)
{
    int     num_clips       = _spritesheet->num_clips;
    char    **clip_names    = _spritesheet->clip_names;
    for (int i = 0; i < num_clips; ++i)
        if (streq(clip_names[i], id))
            return _cursors[i];
    return _question_mark_cursor;
}
