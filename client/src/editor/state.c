#include "state.h"

void
ed_state_init(ed_state_t *state)
{
    memset(state, 0, sizeof(*state));
    ed_log_init(&state->log, 1000);
    arg_str_init(&state->command_parser);
}

void
ed_state_destroy(ed_state_t *state)
{
    arg_str_destroy(&state->command_parser);
    for (int i = 0; i < ED_STATE_MAX_SESSIONS; ++i)
        if (state->session_in_use[i])
            ed_session_destroy(&state->sessions[i]);
    ed_log_destroy(&state->log);
}

void
ed_state_clear(ed_state_t *state)
{
    for (int i = 0; i < ED_STATE_MAX_SESSIONS; ++i)
        if (state->session_in_use[i])
        {
            ed_session_destroy(&state->sessions[i]);
            state->session_in_use[i] = 0;
        }
    ed_log_clear(&state->log);
}

ed_session_t *
ed_state_new_session(ed_state_t *state)
{
    for (int i = 0; i < ED_STATE_MAX_SESSIONS; ++i)
        if (!state->session_in_use[i])
        {
            state->session_in_use[i] = 1;
            ed_session_init(&state->sessions[i], state);
            return &state->sessions[i];
        }
    return 0;
}

void
ed_state_destroy_session(ed_state_t *state, ed_session_t *session)
{
    int index = (int)(session - state->sessions);
    if (!state->session_in_use[index])
        return;
    ed_session_destroy(session);
    state->session_in_use[index] = 0;
}
