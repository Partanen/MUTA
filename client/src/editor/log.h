#ifndef MUTA_EDITOR_LOG_H
#define MUTA_EDITOR_LOG_H

#include "../../../shared/types.h"
#include "../../../shared/compiler.h"

typedef struct ed_log_t         ed_log_t;
typedef struct ed_log_line_t    ed_log_line_t;

enum ed_log_line_type
{
    ED_LOG_INFO,
    ED_LOG_WARNING,
    ED_LOG_ERROR,
    ED_LOG_SUCCESS
};

struct ed_log_line_t
{
    enum ed_log_line_type   type;
    dchar                   *str;
};

struct ed_log_t
{
    ed_log_line_t   *lines;
    uint32          num_lines;
    uint32          max_lines;
    uint32          newest_line;
};

void
ed_log_init(ed_log_t *log, uint32 max_history);

void
ed_log_destroy(ed_log_t *log);

void
ed_log_clear(ed_log_t *log);

void
ed_log_insert(ed_log_t *log, enum ed_log_line_type type, const char *line);

void
ed_log_insertf(ed_log_t *log, enum ed_log_line_type type, const char *fmt, ...)
CHECK_PRINTF_ARGS(3, 4);

ed_log_line_t *
ed_log_get(ed_log_t *log, uint32 index);

#endif /* MUTA_EDITOR_LOG_H */
