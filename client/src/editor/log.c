#include "log.h"
#include "../log.h"
#include "../../../shared/common_utils.h"

void
ed_log_init(ed_log_t *log, uint32 max_history)
{
    log->lines      = ecalloc(max_history * sizeof(ed_log_line_t));
    log->num_lines  = 0;
    log->max_lines  = max_history;
    for (uint32 i = 0; i < max_history; ++i)
        log->lines[i].str = dstr_create_empty(64);
}

void
ed_log_destroy(ed_log_t *log)
{
    for (uint32 i = 0; i < log->max_lines; ++i)
        dstr_free(&log->lines[i].str);
    free(log->lines);
}

void
ed_log_clear(ed_log_t *log)
{
    log->num_lines      = 0;
    log->newest_line    = 0;
}

void
ed_log_insert(ed_log_t *log, enum ed_log_line_type type, const char *line)
{
    uint32 index;
    if (log->newest_line == 0)
        index = log->max_lines - 1;
    else
        index = (log->newest_line - 1) % log->max_lines;
    log->lines[index].type = type;
    dstr_set(&log->lines[index].str, line);
    log->newest_line = index;
    if (log->num_lines < log->max_lines)
        log->num_lines++;
    LOG("%s", line);
}

void
ed_log_insertf(ed_log_t *log, enum ed_log_line_type type, const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    uint32 index;
    if (log->newest_line == 0)
        index = log->max_lines - 1;
    else
        index = (log->newest_line - 1) % log->max_lines;
    log->lines[index].type = type;
    dstr_setvf(&log->lines[index].str, fmt, args);
    log->newest_line = index;
    if (log->num_lines < log->max_lines)
        log->num_lines++;
    va_end(args);
    LOG("%s", log->lines[index].str);
}

ed_log_line_t *
ed_log_get(ed_log_t *log, uint32 index)
{
    if (index >= log->num_lines)
        return 0;
    return &log->lines[((log->newest_line + index) % log->max_lines)];
}
