#ifndef MUTA_EDITOR_SESSION_H
#define MUTA_EDITOR_SESSION_H

#include "../world.h"
#include "../render.h"
#include "../sky.h"
#include "../../../shared/mdb.h"

/* Forward declaration(s) */
typedef struct ed_state_t   ed_state_t;

/* Types defined here */
typedef struct ed_chunk_t   ed_chunk_t;
typedef struct ed_brush_t   ed_brush_t;
typedef struct ed_session_t ed_session_t;
typedef struct ed_entity_t  ed_entity_t;

#define ED_SESSION_MAX_HOVERED_ENTITIES 64

enum ed_brush
{
    ED_BRUSH_NONE,
    ED_BRUSH_TILE,
    ED_BRUSH_CREATURE,
    ED_BRUSH_STATIC_OBJECT,
    ED_BRUSH_DYNAMIC_OBJECT
};

struct ed_brush_t
{
    enum ed_brush type;
    union
    {
        tile_t                  tile;
        sobj_type_id_t    static_object;
        dobj_type_id_t   dynamic_object;
        struct
        {
            creature_type_id_t  type_id;
            int                 sex; /* enum ent_sex */
        } creature;
    };
};

struct ed_session_t
{
    ed_state_t                  *state;
    bool32                      project_open;
    world_t                     world;
    render_camera_t             render_camera;
    render_world_t              render_world;
    sky_t                       sky;
    int                         working_z;
    bool32                      tile_selector_visible;
    ed_brush_t                  brush;
    int                         brush_direction;
    mdb_t                       spawns_db;
    ed_chunk_t                  *chunks;
    dobj_runtime_id_t running_dynamic_object_runtime_id;
    creature_runtime_id_t       running_creature_runtime_id;
    dchar *tmp_common_dir_path;         /* tmp/common */
    dchar *tmp_server_dir_path;         /* tmp/server */
    dchar *common_dir_path;             /* common */
    dchar *server_dir_path;             /* server */
    dchar *tmp_map_file_path;           /* tmp/common/[map_name].map */
    dchar *common_map_file_path;        /* common/[map_name].map */
    dchar *tmp_server_spawns_file_path; /* tmp/server/spawns.mdb */
    dchar *server_spawns_file_path;     /* server/spawns.mdb */
    struct
    {
        int id;
        int spawn_type;
        int type_id;
        int x;
        int y;
        int z;
        int direction;
    } spawns_db_indices;
};

struct ed_entity_t
{
    entity_t    *entity;
    uint32      db_id;
};

void
ed_session_init(ed_session_t *session, ed_state_t *state);

void
ed_session_destroy(ed_session_t *session);

int
ed_session_new_project(ed_session_t *session, const char *directory_path,
    const char *name, int32 w_in_chunks, int32 h_in_chunks);

int
ed_session_open_project(ed_session_t *session, const char *file_path);

void
ed_session_close_project(ed_session_t *session);

void
ed_session_save_project(ed_session_t *session);

void
ed_session_update(ed_session_t *session, double dt);

void
ed_session_render_world(ed_session_t *session);

bool32
ed_session_get_hovered_tile(ed_session_t *session, int ret_position[3]);

uint32
ed_session_get_hovered_entities(ed_session_t *session,
    ed_entity_t ret_entities[ED_SESSION_MAX_HOVERED_ENTITIES]);

int
ed_session_get_working_z(ed_session_t *session);

void
ed_session_set_working_z(ed_session_t *session, int z);

void
ed_session_set_tile_selector_visibility(ed_session_t *session, bool32 value);

const char *
ed_session_get_open_project_name(ed_session_t *session);

int
ed_session_set_tile(ed_session_t *session, int x, int y, int z, tile_t tile);

void
ed_session_move_camera(ed_session_t *session, int x, int y, int z);

void
ed_session_move_camera_relative(ed_session_t *session, int x, int y, int z);

void
ed_session_get_camera_position(ed_session_t *session, int ret_position[3]);

void
ed_session_parse_command(ed_session_t *session, const char *command);

void
ed_session_set_brush(ed_session_t *session, ed_brush_t brush);

void
ed_session_set_brush_direction(ed_session_t *session, enum iso_dir direction);

ed_brush_t
ed_session_get_brush(ed_session_t *session);

int
ed_session_get_brush_direction(ed_session_t *session);

void
ed_session_draw_with_brush(ed_session_t *session, int x, int y, int z);

bool32
ed_session_is_loading(ed_session_t *session);

void
ed_session_remove_entity(ed_session_t *session, entity_t *entity);

bool32
ed_is_repeat_brush(enum ed_brush brush);

#endif /* MUTA_EDITOR_SESSION_H */
