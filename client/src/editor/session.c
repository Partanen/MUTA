#include "session.h"
#include "state.h"
#include "../log.h"
#include "../core.h"
#include "../../../shared/get_opt.h"
#include "../../../shared/entities.h"

#define NUM_COMMANDS ((int)(sizeof(_commands) / sizeof(command_t)))
#define SERVER_SPAWNS_FILE_NAME "spawns.mdb"

typedef struct command_t                command_t;
typedef struct copy_chunk_file_job_t    copy_chunk_file_job_t;
typedef struct spawn_t                  spawn_t;

enum command_status
{
    CMDS_OK = 0,
    CMDS_WRONG_ARG_NUM,
    CMDS_UNKNOWN_OPT,
    CMDS_BAD_OPT
};

struct spawn_t
{
    uint32      db_id;      /* ID in spawns.mdb */
    entity_t    *entity;    /* Entity in the editor world */
};

struct ed_chunk_t
{
    spawn_t *spawns;
};

struct command_t
{
    const char *name;
    const char *description;
    int (*callback)(ed_session_t *session, int argc, char **argv);
};

struct copy_chunk_file_job_t
{
    muta_map_file_t *map_file;
    int32_t         i;
    int32_t         j;
    const char      *dst_dir;
    int             error;
    async_job_t     *async_job;
};

enum spawn_type
{
    SPAWN_DYNAMIC_OBJECT    = 0,
    SPAWN_CREATURE          = 1
};

static int
_copy_map_files_dir_to_dir(muta_map_file_t *map_file,
    const char *src_dir, const char *dst_dir, const char *src_map_file_path,
    const char *dst_map_file_path);
/* Copy the .map file and any .dat files associated with it from one dir to
 * another. */

static void
_on_world_event_tile_changed(world_event_t *event, void *user_data);

static void
_on_world_event_chunk_load_finished(world_event_t *event, void *user_data);

static void
_on_world_event_will_unload_chunk_outside_camera(world_event_t *event,
    void *user_data);

static int
_cmd_help(ed_session_t *session, int argc, char **argv);

static int
_cmd_open(ed_session_t *session, int argc, char **argv);

static int
_cmd_clear(ed_session_t *session, int argc, char **argv);

static int
_cmd_new(ed_session_t *session, int argc, char **argv);

static int
_cmd_goto(ed_session_t *session, int argc, char **argv);

static int
_cmd_fill(ed_session_t *session, int argc, char **argv);

static int
_find_map_file(ed_log_t *log, const char *dir_path, dchar **ret_file_name);
/* ret_file_name should point to a null dchar pointer. If successful,
 * ret_file_name will contain the file name rather than the full path to the
 * file. */

static void
_copy_chunk_file(void* args);

static dobj_runtime_id_t
_next_dynamic_object_runtime_id(ed_session_t *session);
/* Generate a fake runtime id since the server doesn't do it for us in the
 * editor. */

static creature_runtime_id_t
_next_creature_runtime_id(ed_session_t *session);
/* Generate a fake runtime id since the server doesn't do it for us in the
 * editor. */

static int
_open_spawns(ed_session_t *session, const char *project_path);

static int
_create_spawns_db(ed_session_t *session,
    const char *server_spawns_path, const char *tmp_server_spawns_path);

static ed_chunk_t *
_find_chunk(ed_session_t *session, int x, int y);
/* Coordinates in tiles */

static command_t _commands[] =
{
    {
        .name           = "help",
        .description    = ": print this help dialogue.",
        .callback       = _cmd_help
    },
    {
        .name           = "open",
        .description    = " [PATH]: open map file.",
        .callback       = _cmd_open
    },
    {
        .name           = "clear",
        .description    = ": clear log.",
        .callback       = _cmd_clear
    },
    {
        .name           = "new",
        .description    = " [OPTIONS]: create new map file and open it.",
        .callback       = _cmd_new
    },
    {
        .name           = "goto",
        .description    = " [X] [Y] [Z]: move to given position on map.",
        .callback       = _cmd_goto
    },
    {
        .name           = "fill",
        .description    = " [TILE_ID] [X1] [Y1] [Z1] [X2] [Y2] [Z2]: Fill an "
                          "area with tiles.",
        .callback       = _cmd_fill
    }
};

void
ed_session_init(ed_session_t *session, ed_state_t *state)
{
    memset(session, 0, sizeof(*session));
    session->project_open                   = 0;
    session->state                          = state;
    session->tile_selector_visible          = 0;
    session->brush.type                     = ED_BRUSH_TILE;
    session->brush.tile                     = 0;
    session->brush_direction                = ISODIR_NORTH;
    session->tmp_common_dir_path            = dstr_create_empty(0);
    session->tmp_server_dir_path            = dstr_create_empty(0);
    session->common_dir_path                = dstr_create_empty(0);
    session->server_dir_path                = dstr_create_empty(0);
    session->tmp_map_file_path              = dstr_create_empty(0);
    session->common_map_file_path           = dstr_create_empty(0);
    session->tmp_server_spawns_file_path    = dstr_create_empty(0);
    session->server_spawns_file_path        = dstr_create_empty(0);
    sky_init(&session->sky);
}

void
ed_session_destroy(ed_session_t *session)
{
    ed_session_close_project(session);
    dstr_free(&session->tmp_common_dir_path);
    dstr_free(&session->tmp_server_dir_path);
    dstr_free(&session->common_dir_path);
    dstr_free(&session->server_dir_path);
    dstr_free(&session->tmp_map_file_path);
    dstr_free(&session->common_map_file_path);
    dstr_free(&session->tmp_server_spawns_file_path);
    dstr_free(&session->server_spawns_file_path);
    int num_chunks = session->world.map.w * session->world.map.h;
    for (int i = 0; i < num_chunks; ++i)
        darr_free(session->chunks[i].spawns);
    free(session->chunks);
    sky_destroy(&session->sky);
    memset(session, 0, sizeof(*session));
}

int
ed_session_new_project(ed_session_t *session, const char *directory_path,
    const char *name, int32 w_in_chunks, int32 h_in_chunks)
{
    if (w_in_chunks < 3 || h_in_chunks < 3)
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "Map width and height must be at least 3.");
        return 1;
    }
    bool32 dir_exists = file_exists(directory_path);
    /* Create a directory for the project. */
    if (!dir_exists)
    {
        if (create_directory(directory_path))
        {
            ed_log_insertf(&session->state->log, ED_LOG_ERROR,
                "Failed to create directory '%s' for project '%s'.",
                directory_path, name);
            return 2;
        }
    } else
    {
        /* Check if the directory is empty. */
        dir_entry_t     entry;
        dir_handle_t    dir = open_directory(directory_path, &entry);
        if (dir == INVALID_DIR_HANDLE)
        {
            ed_log_insertf(&session->state->log, ED_LOG_ERROR,
                "Directory of new project '%s' isn't empty.", directory_path);
            return 3;
        }
        bool32 is_directory_empty = 1;
        do
        {
            char *name = get_dir_entry_name(&entry);
            if (!streq(name, ".") && !streq(name, ".."))
            {
                is_directory_empty = 0;
                break;
            }
        } while (get_next_file_in_directory(dir, &entry));
        close_directory(dir);
        if (!is_directory_empty)
        {
            ed_log_insertf(&session->state->log, ED_LOG_ERROR,
                "Cannot create project: directory '%s' isn't empty.",
                directory_path);
            return 4;
        }
    }
    int     ret                 = 0;
    bool32  file_initialized    = 0;
    dchar   *dstr[4]; // For paths
    int     err;
    for (int i = 0; i < 4; ++i)
        dstr[i] = dstr_create_empty(256);
    /* Create subdirectories. */
    dstr_set(&dstr[0], directory_path);
    dstr_append(&dstr[0], "/tmp");
    if (create_directory(dstr[0]))
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "Failed to create directory '%s'.", dstr[0]);
        ret = 5;
        goto out;
    }
    dstr_set(&dstr[0], directory_path);
    dstr_append(&dstr[0], "/tmp/common");
    if (create_directory(dstr[0]))
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "Failed to create directory '%s'.", dstr[0]);
        ret = 6;
        goto out;
    }
    dstr_set(&dstr[0], directory_path);
    dstr_append(&dstr[0], "/tmp/server");
    if (create_directory(dstr[0]))
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "Failed to create directory '%s'.", dstr[0]);
        ret = 7;
        goto out;
    }
    dstr_set(&dstr[0], directory_path);
    dstr_append(&dstr[0], "/common");
    if (create_directory(dstr[0]))
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "Failed to create directory '%s'.", dstr[0]);
        ret = 8;
        goto out;
    }
    dstr_set(&dstr[0], directory_path);
    dstr_append(&dstr[0], "/server");
    if (create_directory(dstr[0]))
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "Failed to create directory '%s'.", dstr[0]);
        ret = 9;
        goto out;
    }
    /* Create map file. */
    muta_map_file_t map_file;
    if ((err = muta_map_file_init_for_edit(&map_file, name, 0, w_in_chunks,
        h_in_chunks)))
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "muta_map_file_init_for_edit() failed with code '%d'.", err);
        ret = 10;
        goto out;
    }
    file_initialized = 1;
    dstr_set(&dstr[0], directory_path);
    dstr_append(&dstr[0], "/tmp/common");
    if (muta_map_file_set_chunk_path_pattern(&map_file, dstr[0], name))
        {ret = 11; goto out;}
    dstr_append_char(&dstr[0], '/');
    dstr_append(&dstr[0], name);
    dstr_append(&dstr[0], ".map");
    if (muta_map_file_save(&map_file, dstr[0]))
        {ret = 12; goto out;}
    /* Create chunk files */
    for (int i = 0; i < w_in_chunks; ++i)
        for (int j = 0; j < w_in_chunks; ++j)
        {
            muta_chunk_file_t chunk_file;
            if (muta_chunk_file_init(&chunk_file))
                {ret = 11; goto out;}
            int err = muta_chunk_file_save(&chunk_file,
                muta_map_file_get_chunk_absolute_path(&map_file, i, j));
            muta_chunk_file_destroy(&chunk_file);
            if (err)
            {
                ed_log_insertf(&session->state->log, ED_LOG_ERROR,
                    "muta_chunk_file_save() failed with code %d.", err);
                ret = 13;
                goto out;
            }
        }
    dstr_set(&dstr[0], directory_path);
    dstr_append(&dstr[0], "/tmp/common");
    dstr_set(&dstr[1], directory_path);
    dstr_append(&dstr[1], "/common");
    dstr_set(&dstr[2], dstr[0]);  /* tmp/common */
    dstr_append_char(&dstr[2], '/');
    dstr_append(&dstr[2], name);
    dstr_append(&dstr[2], ".map");
    dstr_set(&dstr[3], dstr[1]);
    dstr_append_char(&dstr[3], '/');
    dstr_append(&dstr[3], name);
    dstr_append(&dstr[3], ".map");
    if ((err = _copy_map_files_dir_to_dir(&map_file, dstr[0], dstr[1], dstr[2],
        dstr[3])))
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "_copy_files_dir_to_dir() failed copying files from '%s to '%s', "
            "error:'%d'.", dstr[0], dstr[1], err);
        ret = 14;
        goto out;
    }
    muta_map_file_destroy(&map_file);
    file_initialized = 0;
    /* Create server spawn file */
    dstr_set(&dstr[0], directory_path);
    dstr_append(&dstr[0], "/server/" SERVER_SPAWNS_FILE_NAME);
    dstr_set(&dstr[1], directory_path);
    dstr_append(&dstr[1], "/tmp/server/" SERVER_SPAWNS_FILE_NAME);
    if (_create_spawns_db(session, dstr[0], dstr[1]))
    {
        ret = 15;
        goto out;
    }
    ed_log_insertf(&session->state->log, ED_LOG_SUCCESS,
        "Map project '%s' files created.", directory_path);
    out:
        for (int i = 0; i < 4; ++i)
            dstr_free(&dstr[i]);
        if (file_initialized)
            muta_map_file_destroy(&map_file);
        if (ret)
            ed_log_insertf(&session->state->log, ED_LOG_ERROR,
                "Failed to create new map '%s', error %d.", name, ret);
        else
            ed_log_insertf(&session->state->log, ED_LOG_SUCCESS,
                "Created a new project '%s' to '%s', size %dx%dx%d (in tiles).",
                name, directory_path, w_in_chunks * MAP_CHUNK_W,
                h_in_chunks * MAP_CHUNK_W, MAP_CHUNK_T);
        return ret;
}

int
ed_session_open_project(ed_session_t *session, const char *project_path)
{
    ed_session_close_project(session);
    ed_log_insertf(&session->state->log, ED_LOG_INFO,
        "Opening map project directory '%s'...", project_path);
    int     ret                         = 0;
    bool32  world_initialized           = 0;
    bool32  render_world_initialized    = 0;
    bool32  spawns_db_initialized       = 0;
    int     err;
    dchar   *dstr           = dstr_create_empty(256);
    dchar   *map_file_name  = 0;
    /* Check if required subdirectories exist. */
    const char *required_sub_dirs[2] =
    {
        "common",
        "server"
    };
    bool32 required_subdirs_exist = 1;
    for (int i = 0; i < 2; ++i)
    {
        dstr_set(&dstr, project_path);
        dstr_append_char(&dstr, '/');
        dstr_append(&dstr, required_sub_dirs[i]);
        if (!is_file_directory(dstr))
        {
            ed_log_insertf(&session->state->log, ED_LOG_ERROR,
                "Failed to open project '%s': the subdirectory '%s' is "
                "missing.", project_path, required_sub_dirs[i]);
            required_subdirs_exist = 0;
            break;
        }
    }
    if (!required_subdirs_exist)
        {ret = 1; goto out;}
    /* Set directory paths. */
    dstr_set(&session->tmp_common_dir_path, project_path);
    dstr_append(&session->tmp_common_dir_path, "/tmp/common");
    dstr_set(&session->tmp_server_dir_path, project_path);
    dstr_append(&session->tmp_server_dir_path, "/tmp/server");
    dstr_set(&session->common_dir_path, project_path);
    dstr_append(&session->common_dir_path, "/common");
    dstr_set(&session->server_dir_path, project_path);
    dstr_append(&session->server_dir_path, "/server");
    /* Find the .map file. */
    dstr_set(&dstr, project_path);
    dstr_append_char(&dstr, '/');
    dstr_append(&dstr, "common");
    if (_find_map_file(&session->state->log, dstr, &map_file_name))
        {ret = 2; goto out;}
    /* Set cached .map file paths */
    dstr_set(&session->common_map_file_path, session->common_dir_path);
    dstr_append_char(&session->common_map_file_path, '/');
    dstr_append(&session->common_map_file_path, map_file_name);
    dstr_set(&session->tmp_map_file_path, session->tmp_common_dir_path);
    dstr_append_char(&session->tmp_map_file_path, '/');
    dstr_append(&session->tmp_map_file_path, map_file_name);
    /* Set cached spawn file paths */
    dstr_set(&session->server_spawns_file_path, session->server_dir_path);
    dstr_append(&session->server_spawns_file_path, "/" SERVER_SPAWNS_FILE_NAME);
    dstr_set(&session->tmp_server_spawns_file_path,
        session->tmp_server_dir_path);
    dstr_append(&session->tmp_server_spawns_file_path,
        "/" SERVER_SPAWNS_FILE_NAME);
    /* Check if the tmp directory exists or try to create it. */
    if (!file_exists(dstr) && create_directory(dstr))
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "Failed to open project '%s': the 'tmp' subdirectory does not "
            "exist and could not be created.", project_path);
        ret = 5;
        goto out;
    }
    /* Copy files from common to tmp. */
    if ((err = _copy_map_files_dir_to_dir(&session->world.map.file,
        session->common_dir_path, session->tmp_common_dir_path,
        session->common_map_file_path, session->tmp_map_file_path)))
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "Failed to open project '%s': could not copy files from '%s' "
            "to '%s', _copy_map_files_dir_to_dir() returned %d).",
            project_path, session->common_dir_path,
            session->tmp_common_dir_path, err);
        ret = 6;
        goto out;
    }
    if (copy_file(session->server_spawns_file_path,
        session->tmp_server_spawns_file_path))
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "Failed to open project '%s': could not copy spawn file from '%s' "
            "to '%s'.", project_path, session->server_spawns_file_path,
            session->tmp_server_spawns_file_path);
        ret = 7;
        goto out;
    }
    if (mdb_open(&session->spawns_db, session->tmp_server_spawns_file_path))
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "Failed to open project '%s': could not open spawn file '%s'.",
            project_path, session->tmp_server_spawns_file_path);
        ret = 8;
        goto out;
    }
    /* Create required spawns database columns */
    session->spawns_db_indices.id = mdb_get_or_create_col_index(
        &session->spawns_db, MDB_COL_UINT32, "id", 1);
    session->spawns_db_indices.spawn_type = mdb_get_or_create_col_index(
        &session->spawns_db, MDB_COL_UINT8, "spawn_type", 0);
    session->spawns_db_indices.type_id = mdb_get_or_create_col_index(
        &session->spawns_db, MDB_COL_UINT32, "type_id", 0);
    session->spawns_db_indices.x = mdb_get_or_create_col_index(
        &session->spawns_db, MDB_COL_INT32, "x", 0);
    session->spawns_db_indices.y = mdb_get_or_create_col_index(
        &session->spawns_db, MDB_COL_INT32, "y", 0);
    session->spawns_db_indices.z = mdb_get_or_create_col_index(
        &session->spawns_db, MDB_COL_UINT8, "z", 0);
    session->spawns_db_indices.direction = mdb_get_or_create_col_index(
        &session->spawns_db, MDB_COL_UINT8, "direction", 0);
    const char *missing_col_name = 0;
    if (session->spawns_db_indices.id < 0)
        missing_col_name = "id";
    else if (session->spawns_db_indices.spawn_type < 0)
        missing_col_name = "spawn_type";
    else if (session->spawns_db_indices.type_id < 0)
        missing_col_name = "type_id";
    else if (session->spawns_db_indices.x < 0)
        missing_col_name = "x";
    else if (session->spawns_db_indices.y < 0)
        missing_col_name = "y";
    else if (session->spawns_db_indices.z < 0)
        missing_col_name = "z";
    else if (session->spawns_db_indices.direction < 0)
        missing_col_name = "direction";
    if (missing_col_name)
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "Failed to create column '%s' to spawns database.",
            missing_col_name);
        ret = 9;
        goto out;
    }
    if ((err = world_init(&session->world, 3, 3, 10000)))
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "Failed to open project '%s': world_init() returned %d.\n",
                project_path, err);
        ret = 9;
        goto out;
    }
    world_initialized = 1;
    if ((err = render_world_init(&session->render_world)))
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "Failed to open project '%s': render_world_init() returned %d.\n",
                project_path, err);
        ret = 10;
        goto out;
    }
    render_world_initialized = 1;
    if ((err = world_load_map_from_path(&session->world,
        session->tmp_map_file_path)))
    {
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "Failed to open project '%s': world_load_from_path returned %d.",
                project_path, err);
        ret = 11;
        goto out;
    }
    render_camera_init(&session->render_camera);
    world_listen_to_event(&session->world, WORLD_EVENT_TILE_CHANGED,
        _on_world_event_tile_changed, session);
    world_listen_to_event(&session->world, WORLD_EVENT_CHUNK_LOAD_FINISHED,
        _on_world_event_chunk_load_finished, session);
    world_listen_to_event(&session->world,
        WORLD_EVENT_WILL_UNLOAD_CHUNK_OUTSIDE_CAMERA,
        _on_world_event_will_unload_chunk_outside_camera, session);
    int num_chunks = session->world.map.w * session->world.map.h;
    session->chunks = ecalloc(num_chunks * sizeof(ed_chunk_t));
    if (_open_spawns(session, project_path))
    {
        ret = 12;
        goto out;
    }
    ed_log_insertf(&session->state->log, ED_LOG_SUCCESS,
        "Successfully opened project '%s'.", project_path);
    session->brush.type     = ED_BRUSH_TILE;
    session->brush.tile     = 1;
    session->project_open   = 1;
    out:
        dstr_free(&dstr);
        dstr_free(&map_file_name);
        if (ret)
        {
            if (world_initialized)
                world_destroy(&session->world);
            if (render_world_initialized)
                render_world_destroy(&session->render_world);
            if (spawns_db_initialized)
                mdb_close(&session->spawns_db);
        }
        return 0;
}

void
ed_session_close_project(ed_session_t *session)
{
    if (!session->project_open)
        return;
    ed_log_insertf(&session->state->log, ED_LOG_INFO, "Closing map '%s'.",
        session->world.map.file.header.name);
    render_world_destroy(&session->render_world);
    world_destroy(&session->world);
    mdb_close(&session->spawns_db);
    session->project_open = 0;
}

void
ed_session_save_project(ed_session_t *session)
{
    if (!session->project_open)
        return;
    ed_log_insert(&session->state->log, ED_LOG_INFO, "Saving...");
    /* Write all open chunks tmp. */
    bool32 have_errors = 0;
    for (int i = 0; i < 9; ++i)
    {
        chunk_cache_t *cache = session->world.map.cache_ptrs[i];
        int err = chunk_cache_save(cache);
        if (err)
        {
            ed_log_insertf(&session->state->log, ED_LOG_ERROR,
                "Failed to write chunk [%d, %d] of map '%s' (error %d).",
                cache->x_in_chunks, cache->y_in_chunks,
                session->world.map.file.header.name, err);
            have_errors = 1;
        }
    }
    if (_copy_map_files_dir_to_dir(&session->world.map.file,
        session->tmp_common_dir_path, session->common_dir_path,
        session->tmp_map_file_path, session->common_map_file_path))
    {
        ed_log_insert(&session->state->log, ED_LOG_ERROR, "Failed to copy "
            "files from tmp to common! Recommended to backup manually.");
        have_errors = 1;
    }
    if (mdb_save(&session->spawns_db, session->tmp_server_spawns_file_path))
    {
        ed_log_insert(&session->state->log, ED_LOG_ERROR,
            "Failed to save spawns file! Recommended to backup manually.");
        have_errors = 1;
    } else
    {
        if (copy_file(session->tmp_server_spawns_file_path,
            session->server_spawns_file_path))
        {
            ed_log_insertf(&session->state->log, ED_LOG_ERROR, "Failed to copy "
                "spawn file from '%s' to '%s'! Recommended to backup manually.",
                session->tmp_server_spawns_file_path,
                session->server_spawns_file_path);
            have_errors = 1;
        }
    }
    if (have_errors)
        ed_log_insert(&session->state->log, ED_LOG_ERROR,
            "There were errors saving files.");
    else
        ed_log_insert(&session->state->log, ED_LOG_SUCCESS,
            "Files saved successfully.");
}

void
ed_session_update(ed_session_t *session, double dt)
{
    if (!session->project_open)
        return;
    world_update(&session->world, dt);
    sky_update(&session->sky, dt);
}

void
ed_session_render_world(ed_session_t *session)
{
    if (!session->project_open)
        return;
    int viewport[4];
    core_compute_target_viewport(viewport);
    if (session->tile_selector_visible)
    {
        float mouse_x, mouse_y;
        render_world_translate_screen_to_world_pixel(&session->render_world,
            core_mouse_x(), core_mouse_y(), &mouse_x, &mouse_y);
        render_world_pixel_to_tile_position_at_z(&session->render_world,
            mouse_x, mouse_y, session->working_z,
            &session->render_camera.tile_selector_position[0],
            &session->render_camera.tile_selector_position[1]);
        session->render_camera.tile_selector_position[2] =
            session->working_z;
        /* Choose which tile selector to draw, if any (invalid area, valid area,
         * etc. */
        int *position = session->render_camera.tile_selector_position;
        if (position[0] < 0 || position[1] < 0 || position[2] < 0 ||
            position[0] >= session->world.map.tw ||
            position[1] >= session->world.map.th ||
            position[2] >= MAP_CHUNK_T)
            session->render_camera.tile_selector_id_index = 2;
        else if (core_mouse_button_down(CORE_MOUSE_BUTTON_LEFT))
            session->render_camera.tile_selector_id_index = 0;
        else
            session->render_camera.tile_selector_id_index = 1;
        session->render_camera.draw_tile_selector = 1;
    } else
        session->render_camera.draw_tile_selector = 0;
    render_world_update(&session->render_world, &session->render_camera,
        &session->world, viewport);
    sky_render(&session->sky);
}

bool32
ed_session_get_hovered_tile(ed_session_t *session, int ret_position[3])
{
    for (int i = 0; i < 3; ++i)
        ret_position[i] = session->render_camera.tile_selector_position[i];
    return session->render_camera.draw_tile_selector;
}

uint32
ed_session_get_hovered_entities(ed_session_t *session,
    ed_entity_t ret_entities[ED_SESSION_MAX_HOVERED_ENTITIES])
{
    entity_t *entities[ED_SESSION_MAX_HOVERED_ENTITIES];
    float tx, ty;
    render_world_translate_screen_to_world_pixel(&session->render_world,
        core_mouse_x(), core_mouse_y(), &tx, &ty);
    uint32 num_entities = render_world_find_entities_by_pixel_position(
        &session->render_world, tx, ty, entities,
        ED_SESSION_MAX_HOVERED_ENTITIES);
    for (uint32 i = 0; i < num_entities; ++i)
    {
        entity_t    *entity = entities[i];
        uint32      db_id   = 0xFFFFFFFF;
        /* Static object entities have no database id, because they are stored
         * in the map chunk files rather than in the spawns db. */
        if (entity_get_type_data(entity)->type != ENTITY_TYPE_STATIC_OBJECT)
        {
            int position[3];
            entity_get_position(entity, position);
            ed_chunk_t *chunk = _find_chunk(session, position[0], position[1]);
            spawn_t *spawns     = chunk->spawns;
            uint32  num_spawns  = darr_num(spawns);
            for (uint32 j = 0; j < num_spawns; ++j)
                if (spawns[j].entity == entity)
                {
                    db_id = spawns[j].db_id;
                    break;
                }
            muta_assert(db_id != 0xFFFFFFFF);
        }
        ret_entities[i].entity  = entities[i];
        ret_entities[i].db_id   = db_id;
    }
    return num_entities;
}


int
ed_session_get_working_z(ed_session_t *session)
    {return session->working_z;}

void
ed_session_set_working_z(ed_session_t *session, int z)
    {session->working_z = CLAMP(z, 0, MAP_CHUNK_T - 1);}

void
ed_session_set_tile_selector_visibility(ed_session_t *session, bool32 value)
    {session->tile_selector_visible = value;}

const char *
ed_session_get_open_project_name(ed_session_t *session)
{
    if (!session->project_open)
        return 0;
    return session->world.map.file.header.name;
}

int
ed_session_set_tile(ed_session_t *session, int x, int y, int z, tile_t tile)
{
    if (!session->project_open)
        return 1;
    return world_set_tile(&session->world, x, y, z, tile);
}

void
ed_session_move_camera(ed_session_t *session, int x, int y, int z)
{
    if (!session->project_open)
        return;
    world_set_camera_position(&session->world, x / MAP_CHUNK_W,
        y / MAP_CHUNK_W);
    int last_z = session->render_camera.position[2];
    session->render_camera.position[0] = x;
    session->render_camera.position[1] = y;
    session->render_camera.position[2] = z;
    session->render_camera.position[0] = CLAMP(
        session->render_camera.position[0], 0, session->world.map.tw - 1);
    session->render_camera.position[1] = CLAMP(
        session->render_camera.position[1], 0, session->world.map.th - 1);
    session->render_camera.position[2] = CLAMP(
        session->render_camera.position[2], 0, MAP_CHUNK_T - 1);
    for (int i = 0; i < 3; ++i)
        session->render_camera.last_position[i] =
            session->render_camera.position[i];
    int z_diff = session->working_z - last_z;
    ed_session_set_working_z(session,
        session->render_camera.position[2] + z_diff);
}

void
ed_session_move_camera_relative(ed_session_t *session, int x, int y, int z)
{
    ed_session_move_camera(session, session->render_camera.position[0] + x,
        session->render_camera.position[1] + y,
        session->render_camera.position[2] + z);
}

void
ed_session_get_camera_position(ed_session_t *session, int ret_position[3])
{
    if (!session->project_open)
    {
        for (int i = 0; i < 3; ++i)
            ret_position[i] = 0;
        return;
    }
    for (int i = 0; i < 3; ++i)
        ret_position[i] = session->render_camera.position[i];
}

void
ed_session_parse_command(ed_session_t *session, const char *command)
{
    arg_str_parse(&session->state->command_parser, command);
    char    **argv  = session->state->command_parser.argv;
    int     argc    = session->state->command_parser.argc;
    if (!argc)
        return;
    for (int i = 0; i < NUM_COMMANDS; ++i)
        if (streq(_commands[i].name, argv[0]))
        {
            int err = _commands[i].callback(session, argc, argv);
            switch (err)
            {
            case CMDS_WRONG_ARG_NUM:
                ed_log_insert(&session->state->log, ED_LOG_ERROR,
                    "Wrong number of arguments.");
                break;
            case CMDS_UNKNOWN_OPT:
                ed_log_insert(&session->state->log, ED_LOG_ERROR,
                    "Unknown options passed to command.");
                break;
            case CMDS_BAD_OPT:
                ed_log_insert(&session->state->log, ED_LOG_ERROR,
                    "Bad options passed to command.");
                break;
            case CMDS_OK:
                break;
            default:
                muta_assert(0);
            }
            return;
        }
    ed_log_insert(&session->state->log, ED_LOG_ERROR,
        "Unrecognized command.");
}

void
ed_session_set_brush(ed_session_t *session, ed_brush_t brush)
    {session->brush = brush;}

void
ed_session_set_brush_direction(ed_session_t *session, enum iso_dir direction)
{
    muta_assert(direction < NUM_ISODIRS);
    muta_assert(direction >= 0);
    session->brush_direction = direction;
}

ed_brush_t
ed_session_get_brush(ed_session_t *session)
    {return session->brush;}

int
ed_session_get_brush_direction(ed_session_t *session)
    {return session->brush_direction;}

void
ed_session_draw_with_brush(ed_session_t *session, int x, int y, int z)
{
    if (!session->project_open)
        return;
    switch (session->brush.type)
    {
    case ED_BRUSH_TILE:
        ed_session_set_tile(session, x, y, z, session->brush.tile);
        break;
    case ED_BRUSH_STATIC_OBJECT:
    {
        int position[3] = {x, y, z};
        if (static_object_spawn(&session->world, session->brush.static_object,
            session->brush_direction, position))
            ed_log_insertf(&session->state->log, ED_LOG_SUCCESS, "Drew static "
                "object of type %u at position [%d, %d, %d] facing direction "
                "%d.", session->brush.static_object, x, y, z,
                session->brush_direction);
        else
            ed_log_insertf(&session->state->log, ED_LOG_ERROR, "Failed to "
                "draw static object of type %u at position [%d, %d, %d] "
                "facing direction %d.", session->brush.static_object,
                x, y, z, session->brush_direction);
    }
        break;
    case ED_BRUSH_DYNAMIC_OBJECT:
    {
        const char *err_str;
        mdb_row_t row = 0; /* Deleted on failure */
        uint32 db_id;
        int position[3] = {x, y, z};
        entity_t *entity = dynamic_object_spawn(&session->world,
            _next_dynamic_object_runtime_id(session),
            session->brush.dynamic_object, session->brush_direction, position);
        if (!entity)
        {
            err_str = "failed to spawn entity in world";
            goto fail_dynamic_object;
        }
        if (mdb_new_row(&session->spawns_db, &db_id))
        {
            err_str = "failed to write entity to " SERVER_SPAWNS_FILE_NAME;
            goto fail_dynamic_object;
        }
        row = mdb_get_row(&session->spawns_db, db_id);
        if (!row)
        {
            err_str = "failed to get entity's row from spawns database";
            goto fail_dynamic_object;
        }
        /* Store spawn type */
        uint8 spawn_type = SPAWN_DYNAMIC_OBJECT;
        if (mdb_set_field(&session->spawns_db, row,
            session->spawns_db_indices.spawn_type, &spawn_type))
        {
            err_str = "failed to set field 'spawn_type' in spawns database.";
            goto fail_dynamic_object;
        }
        /* Store type_id */
        uint32 type_id = session->brush.dynamic_object;
        if (mdb_set_field(&session->spawns_db, row,
            session->spawns_db_indices.type_id, &type_id))
        {
            err_str = "failed to set field 'type_id' in spawns database.";
            goto fail_dynamic_object;
        }
        /* Store x */
        int32_t db_x = x;
        if (mdb_set_field(&session->spawns_db, row, session->spawns_db_indices.x,
            &db_x))
        {
            err_str = "failed to set field 'x' in spawns database.";
            goto fail_dynamic_object;
        }
        /* Store y */
        int32_t db_y = y;
        if (mdb_set_field(&session->spawns_db, row, session->spawns_db_indices.y,
            &db_y))
        {
            err_str = "failed to set field 'y' in spawns database.";
            goto fail_dynamic_object;
        }
        /* Store y */
        uint8 db_z = (uint8)z;
        if (mdb_set_field(&session->spawns_db, row, session->spawns_db_indices.z,
            &db_z))
        {
            err_str = "failed to set field 'z' in spawns database.";
            goto fail_dynamic_object;
        }
        /* Store direction */
        uint8 db_direction = (uint8)session->brush_direction;
        if (mdb_set_field(&session->spawns_db, row,
            session->spawns_db_indices.direction, &db_direction))
        {
            err_str = "failed to set field 'direction' in spawns database.";
            goto fail_dynamic_object;
        }
        ed_chunk_t *chunk = _find_chunk(session, x, y);
        spawn_t spawn = {.db_id = db_id, .entity = entity};
        darr_push(chunk->spawns, spawn);
        ed_log_insertf(&session->state->log, ED_LOG_SUCCESS, "Drew dynamic "
            "object of type %u at position [%d, %d, %d] facing direction "
            "%d.", session->brush.dynamic_object, x, y, z,
            session->brush_direction);
        break;
        fail_dynamic_object:
            if (row)
                mdb_del_row(&session->spawns_db, db_id);
            if (entity)
                entity_despawn(entity);
            ed_log_insertf(&session->state->log, ED_LOG_ERROR, "Failed to "
                "draw dynamic object of type %u at position [%d, %d, %d] "
                "facing direction %d: %s.", session->brush.dynamic_object,
                x, y, z, session->brush_direction, err_str);
    }
        break;
    case ED_BRUSH_CREATURE:
    {
        const char *err_str;
        mdb_row_t row = 0; /* Deleted on failure */
        uint32 db_id;
        int position[3] = {x, y, z};
        entity_t *entity = creature_spawn(&session->world,
            _next_creature_runtime_id(session), session->brush.creature.type_id,
            session->brush.creature.sex, session->brush_direction, position,
            0, 0, 0);
        if (!entity)
        {
            err_str = "failed to spawn entity in world";
            goto fail_creature;
        }
        if (mdb_new_row(&session->spawns_db, &db_id))
        {
            err_str = "failed to write entity to " SERVER_SPAWNS_FILE_NAME;
            goto fail_creature;
        }
        row = mdb_get_row(&session->spawns_db, db_id);
        if (!row)
        {
            err_str = "failed to get entity's row from spawns database";
            goto fail_creature;
        }
        /* Store spawn type */
        uint8 spawn_type = SPAWN_CREATURE;
        if (mdb_set_field(&session->spawns_db, row,
            session->spawns_db_indices.spawn_type, &spawn_type))
        {
            err_str = "failed to set field 'spawn_type' in spawns database.";
            goto fail_dynamic_object;
        }
        /* Store type_id */
        uint32 type_id = session->brush.dynamic_object;
        if (mdb_set_field(&session->spawns_db, row,
            session->spawns_db_indices.type_id, &type_id))
        {
            err_str = "failed to set field 'type_id' in spawns database.";
            goto fail_dynamic_object;
        }
        /* Store x */
        int32_t db_x = x;
        if (mdb_set_field(&session->spawns_db, row,
            session->spawns_db_indices.x, &db_x))
        {
            err_str = "failed to set field 'x' in spawns database.";
            goto fail_dynamic_object;
        }
        /* Store y */
        int32_t db_y = y;
        if (mdb_set_field(&session->spawns_db, row,
            session->spawns_db_indices.y, &db_y))
        {
            err_str = "failed to set field 'y' in spawns database.";
            goto fail_dynamic_object;
        }
        /* Store y */
        uint8 db_z = (uint8)z;
        if (mdb_set_field(&session->spawns_db, row,
            session->spawns_db_indices.z, &db_z))
        {
            err_str = "failed to set field 'z' in spawns database.";
            goto fail_dynamic_object;
        }
        /* Store direction */
        uint8 db_direction = (uint8)session->brush_direction;
        if (mdb_set_field(&session->spawns_db, row,
            session->spawns_db_indices.direction, &db_direction))
        {
            err_str = "failed to set field 'direction' in spawns database.";
            goto fail_creature;
        }
        ed_chunk_t *chunk = _find_chunk(session, x, y);
        spawn_t spawn = {.db_id = db_id, .entity = entity};
        darr_push(chunk->spawns, spawn);
        ed_log_insertf(&session->state->log, ED_LOG_SUCCESS, "Drew dynamic "
            "object of type %u at position [%d, %d, %d] facing direction "
            "%d.", session->brush.dynamic_object, x, y, z,
            session->brush_direction);
        break;
        fail_creature:
            if (row)
                mdb_del_row(&session->spawns_db, db_id);
            if (entity)
                entity_despawn(entity);
            ed_log_insertf(&session->state->log, ED_LOG_ERROR, "Failed to "
                "draw dynamic object of type %u at position [%d, %d, %d] "
                "facing direction %d: %s.", session->brush.dynamic_object,
                x, y, z, session->brush_direction, err_str);
    }
        break;
    default:
        muta_assert(0);
    }
}

bool32
ed_session_is_loading(ed_session_t *session)
    {return session->project_open && world_is_loading(&session->world);}

void
ed_session_remove_entity(ed_session_t *session, entity_t *entity)
{
    enum entity_type type = entity_get_type_data(entity)->type;
    int32 position[3];
    entity_get_position(entity, position);
    entity_despawn(entity);
    if (type == ENTITY_TYPE_STATIC_OBJECT)
    {
        ed_log_insert(&session->state->log, ED_LOG_INFO,
            "Removed static object.");
        return;
    }
    const char *type_name = 0;
    if (type == ENTITY_TYPE_DYNAMIC_OBJECT)
        type_name = "dynamic object";
    else if (type == ENTITY_TYPE_CREATURE)
        type_name = "creature";
    ed_chunk_t  *chunk      = _find_chunk(session, position[0], position[1]);
    spawn_t     *spawns     = chunk->spawns;
    uint32      num_spawns  = darr_num(spawns);
    for (uint32 i = 0; i < num_spawns; ++i)
    {
        if (spawns[i].entity == entity)
        {
            uint32 db_id = spawns[i].db_id;
            darr_erase_unordered(spawns, i);
            mdb_del_row(&session->spawns_db,
                mdb_get_row(&session->spawns_db, db_id));
            ed_log_insertf(&session->state->log, ED_LOG_INFO,
                "Removed %s, database ID %u.", type_name, db_id);
            return;
        }
    }
    muta_assert(0);
}

bool32
ed_is_repeat_brush(enum ed_brush brush)
    {return brush == ED_BRUSH_TILE;}

static int
_copy_map_files_dir_to_dir(muta_map_file_t *map_file, const char *src_dir,
    const char *dst_dir, const char *src_map_file_path,
    const char *dst_map_file_path)
{
    int err;
    if ((err = copy_file(src_map_file_path, dst_map_file_path)))
        return 1;
    int     ret = 0;
    int32_t num_jobs        = map_file->header.w * map_file->header.h;
    size_t  stack_offset    = core_stack_push(
        num_jobs * sizeof(copy_chunk_file_job_t));
    copy_chunk_file_job_t *jobs = core_get_stack_at(stack_offset);
    for (int32_t i = 0; i < (int32_t)map_file->header.w; ++i)
        for (int32_t j = 0; j < (int32_t)map_file->header.h; ++j)
        {
            int32_t index = j * map_file->header.w + i;
            jobs[index].map_file    = map_file;
            jobs[index].i           = i;
            jobs[index].j           = j;
            jobs[index].dst_dir     = dst_dir;
            jobs[index].async_job   = core_run_async(_copy_chunk_file,
                &jobs[index]);
        }
    for (int i = 0; i < num_jobs; ++i)
    {
        core_async_wait(jobs[i].async_job, 0, -1);
        if (jobs[i].error)
            ret = 1;
    }
    core_stack_pop(stack_offset);
    return ret;
}

static void
_on_world_event_tile_changed(world_event_t *event, void *user_data)
{
    ed_session_t *session = user_data;
    session->render_world.need_tile_update = 1;
}

static void
_on_world_event_chunk_load_finished(world_event_t *event, void *user_data)
{
    ed_session_t *session = user_data;
    session->render_world.need_tile_update = 1;
    if (event->chunk_load_finished.error)
        muta_panic_print("Editor: corrupted map file (error %d)!",
            event->chunk_load_finished.error);
    int32 x_in_chunks = event->chunk_load_finished.chunk_cache->x_in_chunks;
    int32 y_in_chunks = event->chunk_load_finished.chunk_cache->y_in_chunks;
    ed_chunk_t *chunk = &session->chunks[
        y_in_chunks * session->world.map.w + x_in_chunks];
    spawn_t *spawns             = chunk->spawns;
    uint32  num_spawns          = darr_num(spawns);
    int     spawn_type_index    = session->spawns_db_indices.spawn_type;
    int     type_id_index       = session->spawns_db_indices.type_id;
    int     x_index             = session->spawns_db_indices.x;
    int     y_index             = session->spawns_db_indices.y;
    int     z_index             = session->spawns_db_indices.z;
    int     direction_index     = session->spawns_db_indices.direction;
    uint32  num_spawned         = 0;
    for (uint32 i = 0; i < num_spawns; ++i)
    {
        if (spawns[i].entity)
            continue;
        mdb_row_t row = mdb_get_row(&session->spawns_db, spawns[i].db_id);
        uint8 spawn_type = *(uint8*)mdb_get_field(&session->spawns_db, row,
            spawn_type_index);
        uint32 type_id = *(uint32*)mdb_get_field(&session->spawns_db, row,
            type_id_index);
        int32 x = *(int32*)mdb_get_field(&session->spawns_db, row, x_index);
        int32 y = *(int32*)mdb_get_field(&session->spawns_db, row, y_index);
        uint8 z = *(uint8*)mdb_get_field(&session->spawns_db, row, z_index);
        uint8 direction  = *(uint8*)mdb_get_field(&session->spawns_db, row,
            direction_index);
        int position[3] = {x, y, z};
        if (spawn_type == SPAWN_DYNAMIC_OBJECT)
        {
            entity_t *entity = dynamic_object_spawn(&session->world,
                _next_dynamic_object_runtime_id(session), type_id, direction,
                position);
            if (entity)
                num_spawned++;
            spawns[i].entity = entity;
        } else
        if (spawn_type == SPAWN_CREATURE)
        {
            entity_t *entity = creature_spawn(&session->world,
                _next_creature_runtime_id(session), type_id,
                session->brush.creature.sex, direction, position, 0, 0, 0);
            if (entity)
                num_spawned++;
            spawns[i].entity = entity;
        } else
            ed_log_insert(&session->state->log, ED_LOG_ERROR,
                "Bad spawn type for entry in spawns database.");
    }
    if (num_spawned)
        ed_log_insertf(&session->state->log, ED_LOG_INFO,
            "Spawned %u entities to newly loaded chunk %d, %d.", num_spawned,
            x_in_chunks, y_in_chunks);
}

static void
_on_world_event_will_unload_chunk_outside_camera(world_event_t *event,
    void *user_data)
{
    DEBUG_PRINTFF("UNLOADING\n");
    ed_session_t *session = user_data;
    chunk_cache_t *cache = event->will_unload_chunk_outside_camera.chunk_cache;
    /* Despawn and save entities other than static objects. */
    ed_chunk_t *chunk = &session->chunks[
        cache->y_in_chunks * session->world.map.w + cache->x_in_chunks];
    uint32 num_spawns = darr_num(chunk->spawns);
    uint32 num_despawned = 0;
    for (uint32 i = 0; i < num_spawns; ++i)
    {
        entity_t *entity = chunk->spawns[i].entity;
        if (!entity)
            continue;
        entity_despawn(entity);
        chunk->spawns[i].entity = 0;
        num_despawned++;
    }
    ed_log_insertf(&session->state->log, ED_LOG_INFO,
        "Despawned %u entities from unloading chunk %d, %d.", num_despawned,
        cache->x_in_chunks, cache->y_in_chunks);
    int err = chunk_cache_save(cache);
    if (err)
        ed_log_insertf(&session->state->log, ED_LOG_ERROR,
            "Failed to save chunk '%s' to tmp (error %d)!",
            muta_map_file_get_chunk_absolute_path(&session->world.map.file,
                cache->x_in_chunks, cache->y_in_chunks), err);
    else
        ed_log_insertf(&session->state->log, ED_LOG_INFO,
            "Saved a chunk cache that is no longer in memory.");
}

static int
_cmd_help(ed_session_t *session, int argc, char **argv)
{
    if (argc != 1)
        return CMDS_WRONG_ARG_NUM;
    ed_log_insertf(&session->state->log, ED_LOG_INFO,
        "MUTA v. %s map editor", MUTA_VERSION_STR);
    for (int i = 0; i < NUM_COMMANDS; ++i)
        ed_log_insertf(&session->state->log, ED_LOG_INFO,
            "%s%s", _commands[i].name, _commands[i].description);
    return 0;
}

static int
_cmd_open(ed_session_t *session, int argc, char **argv)
{
    if (argc != 2)
        return CMDS_WRONG_ARG_NUM;
    ed_session_open_project(session, argv[1]);
    return CMDS_OK;
}

static int
_cmd_clear(ed_session_t *session, int argc, char **argv)
{
    if (argc != 1)
        return CMDS_WRONG_ARG_NUM;
    ed_log_clear(&session->state->log);
    return CMDS_OK;
}

static int
_cmd_new(ed_session_t *session, int argc, char **argv)
{
    get_opt_context_t   opt_ctx = GET_OPT_CONTEXT_INITIALIZER;
    int                 opt;
    const char  *name   = 0;
    const char  *path   = 0;
    int32       width   = -1;
    int32       height  = -1;
    while ((opt = get_opt(&opt_ctx, argc, argv, "n:p:x:y:h")) != -1)
    {
        switch (opt)
        {
        case 'n':
            name = opt_ctx.arg;
            break;
        case 'p':
            path = opt_ctx.arg;
            break;
        case 'x':
        {
            int v = atoi(opt_ctx.arg);
            if (v > 0)
                width = v;
        }
            break;
        case 'y':
        {
            int v = atoi(opt_ctx.arg);
            if (v > 0)
                height = v;
        }
            break;
        case 'h':
            ed_log_insert(&session->state->log, ED_LOG_INFO,
                "new\n"
                "Create new map.\n"
                "Options:\n"
                "-h: Print this help dialogue.\n"
                "-n [NAME]:     map name.\n"
                "-p [PATH]:     map path.\n"
                "-x [NUMBER]:   map width in chunks.\n"
                "-y [NUMBER]:   map height in chunks.");
            return CMDS_OK;
        case '?':
            return CMDS_UNKNOWN_OPT;
        }
    }
    if (!name || !path || width < 0 || height < 0)
        return CMDS_WRONG_ARG_NUM;
    ed_session_new_project(session, path, name, width, height);
    return CMDS_OK;
}

static int
_cmd_goto(ed_session_t *session, int argc, char **argv)
{
    if (argc != 4)
        return CMDS_WRONG_ARG_NUM;
    for (int i = 1; i < 4; ++i)
        if (!str_is_int(argv[i]))
            return CMDS_BAD_OPT;
    ed_session_move_camera(session, atoi(argv[1]), atoi(argv[2]),
        atoi(argv[3]));
    return CMDS_OK;
}

static int
_cmd_fill(ed_session_t *session, int argc, char **argv)
{
    if (argc == 1 || (argc == 2 && streq(argv[1], "-h")))
    {
        ed_log_insert(&session->state->log, ED_LOG_INFO,
            "fill [TILE_ID] [X1] [Y1] [Z1] [X2] [Y2] [Z2]\n"
            "Fill an area with tiles.");
        return CMDS_OK;
    }
    if (argc != 8)
        return CMDS_WRONG_ARG_NUM;
    for (int i = 1; i < argc; ++i)
        if (!str_is_int(argv[i]))
            return CMDS_BAD_OPT;
    int tile = atoi(argv[1]);
    if (tile < 0 || tile >= TILE_MAX_TYPES)
        return CMDS_BAD_OPT;
    int position[6];
    for (int i = 2; i < argc; ++i)
        position[i - 2] = atoi(argv[i]);
    for (int i = position[0]; i < position[3]; ++i)
        for (int j = position[1]; j < position[4]; ++j)
            for (int k = position[2]; k < position[5]; ++k)
                ed_session_set_tile(session, i, j, k, (tile_t)tile);
    ed_log_insertf(&session->state->log, ED_LOG_INFO, "Filled area [%d, %d, "
        "%d] to [%d, %d, %d] with tile %d.\n", position[0], position[1],
        position[2], position[3], position[4], position[5], tile);
    return CMDS_OK;
}

static int
_find_map_file(ed_log_t *log, const char *dir_path, dchar **ret_file_name)
{
    muta_assert(!*ret_file_name);
    dir_entry_t     entry;
    dir_handle_t    dir = open_directory(dir_path, &entry);
    if (dir == INVALID_DIR_HANDLE)
    {
        ed_log_insertf(log, ED_LOG_ERROR, "Failed to open directory '%s'.",
            dir_path);
        return 1;
    }
    bool32 found_map_file = 0;
    do
    {
        char *name = get_dir_entry_name(&entry);
        if (str_ends_with(name, ".map"))
        {
            if (!found_map_file)
            {
                *ret_file_name = dstr_create(name);
                found_map_file = 1;
            } else
            {
                ed_log_insertf(log, ED_LOG_ERROR,
                    "More than one .map files found in directory '%s'.",
                    dir_path);
                close_directory(dir);
                dstr_free(ret_file_name);
                return 2;
            }
        }
    } while (!get_next_file_in_directory(dir, &entry));
    close_directory(dir);
    return 0;
}

static void
_copy_chunk_file(void *args)
{
    copy_chunk_file_job_t *job = args;
    const char *src = muta_map_file_get_chunk_absolute_path(job->map_file,
        job->i, job->j);
    dchar *dst = dstr_create(job->dst_dir);
    dstr_append_char(&dst, '/');
    dstr_append(&dst, muta_map_file_get_chunk_relative_path(job->map_file,
        job->i, job->j));
    job->error = copy_file(src, dst);
    dstr_free(&dst);
}

static dobj_runtime_id_t
_next_dynamic_object_runtime_id(ed_session_t *session)
    {return session->running_dynamic_object_runtime_id++;}

static creature_runtime_id_t
_next_creature_runtime_id(ed_session_t *session)
    {return session->running_creature_runtime_id++;}

static int
_open_spawns(ed_session_t *session, const char *project_path)
{
    int id_col_index            = session->spawns_db_indices.id;
    int spawn_type_col_index    = session->spawns_db_indices.spawn_type;
    int type_id_col_index       = session->spawns_db_indices.type_id;
    int x_col_index             = session->spawns_db_indices.x;
    int y_col_index             = session->spawns_db_indices.y;
    int z_col_index             = session->spawns_db_indices.z;
    int direction_col_index     = session->spawns_db_indices.direction;
    /* Populate stored spawns. */
    int num_rows = mdb_num_rows(&session->spawns_db);
    for (int i = 0; i < num_rows; ++i)
    {
        mdb_row_t row = mdb_get_row_by_index(&session->spawns_db, i);
        if (!row)
        {
            ed_log_insertf(&session->state->log, ED_LOG_ERROR,
                "Could not get row index %d from spawns database.", i);
            return 2;
        }
        uint32 id = *(uint32*)mdb_get_field(&session->spawns_db, row,
            id_col_index);
        uint8 spawn_type = *(uint8*)mdb_get_field(&session->spawns_db, row,
            spawn_type_col_index);
        uint32 type_id = *(uint32*)mdb_get_field(&session->spawns_db, row,
            type_id_col_index);
        int32 x = *(int32*)mdb_get_field(&session->spawns_db, row, x_col_index);
        int32 y = *(int32*)mdb_get_field(&session->spawns_db, row, y_col_index);
        uint8 z = *(uint8*)mdb_get_field(&session->spawns_db, row, z_col_index);
        uint8 direction = *(uint8*)mdb_get_field(&session->spawns_db, row,
            direction_col_index);
        if (spawn_type == SPAWN_DYNAMIC_OBJECT)
        {
            spawn_t spawn = {.db_id = id, .entity = 0};
            if (world_is_chunk_of_tile_loaded(&session->world, x, y))
            {
                int position[3] = {x, y, z};
                spawn.entity = dynamic_object_spawn(&session->world,
                    _next_dynamic_object_runtime_id(session), type_id,
                    (int)direction,  position);
                if (!spawn.entity)
                    ed_log_insertf(&session->state->log, ED_LOG_ERROR,
                        "Failed to spawn dynamic object %u!", id);
            }
            ed_chunk_t *chunk = _find_chunk(session, x, y);
            darr_push(chunk->spawns, spawn);
        } else
        if (spawn_type == SPAWN_CREATURE)
        {
            spawn_t spawn = {.db_id = id, .entity = 0};
            if (world_is_chunk_of_tile_loaded(&session->world, x, y))
            {
                int position[3] = {x, y, z};
                spawn.entity = creature_spawn(&session->world,
                    _next_creature_runtime_id(session), type_id,
                    session->brush.creature.sex, (int)direction, position, 0,
                    0, 0);
                if (!spawn.entity)
                    ed_log_insertf(&session->state->log, ED_LOG_ERROR,
                        "Failed to spawn dynamic object %u!", id);
            }
            ed_chunk_t *chunk = _find_chunk(session, x, y);
            darr_push(chunk->spawns, spawn);
        } else
        {
            ed_log_insertf(&session->state->log, ED_LOG_ERROR,
                "Field spawn_type was incorrect for row index %u in spawn db.",
                (uint32)row);
        }
    }
    return 0;
}

static int
_create_spawns_db(ed_session_t *session, const char *server_spawns_path,
    const char *tmp_server_spawns_path)
{
    mdb_t spawns_db;
    if (mdb_new(&spawns_db))
    {
        ed_log_insert(&session->state->log, ED_LOG_ERROR,
            "Failed to create server spawns file.");
        return 1;
    }
    int ret = 0;
    if (mdb_new_col(&spawns_db, "tag", MDB_COL_STR, 0))
    {
        ed_log_insert(&session->state->log, ED_LOG_ERROR,
            "Failed to create column 'tag' to spawn database.");
        ret = 1;
        goto out;
    }
    if (mdb_new_col(&spawns_db, "spawn_type", MDB_COL_UINT8, 0))
    {
        ed_log_insert(&session->state->log, ED_LOG_ERROR,
            "Failed to create column 'spawn_type' to spawn database.");
        ret = 1;
        goto out;
    }
    if (mdb_new_col(&spawns_db, "type_id", MDB_COL_UINT32, 0))
    {
        ed_log_insert(&session->state->log, ED_LOG_ERROR,
            "Failed to create column 'type_id' to spawn database.");
        ret = 1;
        goto out;
    }
    if (mdb_new_col(&spawns_db, "direction", MDB_COL_UINT8, 0))
    {
        ed_log_insert(&session->state->log, ED_LOG_ERROR,
            "Failed to create column 'direction' to spawn database.");
        ret = 1;
        goto out;
    }
    if (mdb_new_col(&spawns_db, "x", MDB_COL_INT32, 0))
    {
        ed_log_insert(&session->state->log, ED_LOG_ERROR,
            "Failed to create column 'x' to spawn database.");
        ret = 1;
        goto out;
    }
    if (mdb_new_col(&spawns_db, "y", MDB_COL_INT32, 0))
    {
        ed_log_insert(&session->state->log, ED_LOG_ERROR,
            "Failed to create column 'y' to spawn database.");
        ret = 1;
        goto out;
    }
    if (mdb_new_col(&spawns_db, "z", MDB_COL_UINT8, 0))
    {
        ed_log_insert(&session->state->log, ED_LOG_ERROR,
            "Failed to create column 'z' to spawn database.");
        ret = 1;
        goto out;
    }
    if (mdb_new_col(&spawns_db, "sex", MDB_COL_UINT8, 0))
    {
        ed_log_insert(&session->state->log, ED_LOG_ERROR,
            "Failed to create column 'sex' to spawn database.");
        ret = 1;
        goto out;
    }
    if (mdb_save(&spawns_db, server_spawns_path))
    {
        ed_log_insert(&session->state->log, ED_LOG_ERROR,
            "Failed to save spawns database.");
        ret = 1;
        goto out;
    }
    if (copy_file(server_spawns_path, tmp_server_spawns_path))
    {
        ed_log_insert(&session->state->log, ED_LOG_ERROR,
            "Failed to copy spawns file to tmp directory.");
        ret = 1;
        goto out;
    }
    out:
        mdb_close(&spawns_db);
        return ret;
}

static ed_chunk_t *
_find_chunk(ed_session_t *session, int x, int y)
{
    int max_x = session->world.map.tw;
    int max_y = session->world.map.th;
    muta_assert(x >= 0 && x < max_x && y >= 0 && y < max_y);
    int chunk_x = x / MAP_CHUNK_W;
    int chunk_y = y / MAP_CHUNK_W;
    int index = chunk_y * session->world.map.w + chunk_x;
    DEBUG_PRINTFF("CHUNK X: %d, chunk y: %d\n", chunk_x, chunk_y);
    return &session->chunks[index];
}
