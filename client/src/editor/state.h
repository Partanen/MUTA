#ifndef MUTA_EDITOR_STATE_H
#define MUTA_EDITOR_STATE_H

#include "session.h"
#include "log.h"

#define ED_STATE_MAX_SESSIONS 1

typedef struct ed_state_t ed_state_t;

struct ed_state_t
{
    bool32          session_in_use[ED_STATE_MAX_SESSIONS];
    ed_session_t    sessions[ED_STATE_MAX_SESSIONS];
    ed_log_t        log;
    arg_str_t       command_parser;
};

void
ed_state_init(ed_state_t *state);

void
ed_state_destroy(ed_state_t *state);

void
ed_state_clear(ed_state_t *state);

ed_session_t *
ed_state_new_session(ed_state_t *state);

void
ed_state_destroy_session(ed_state_t *state, ed_session_t *session);

#endif /* MUTA_EDITOR_STATE_H */
