#include "screen.h"
#include "state.h"
#include "../core.h"
#include "../gl.h"
#include "../gui_styles.h"
#include "../filepicker.h"
#include "../gui.h"
#include "../main_menu_screen.h"

#define CAMERA_SPEED            0.1f
#define INPUT_REPEAT_DELTA      0.03f
#define COMMAND_BUFFER_SIZE     4097
#define MAX_SELECTED_ENTITIES   64
#define TOP_BAR_X \
    ((int)(0.01f * (float)core_asset_config.resolution_w))
#define TOP_BAR_Y \
    ((int)(0.01f * (float)core_asset_config.resolution_h))
#define TOP_BAR_W \
    ((int)(core_asset_config.resolution_w - 2 * TOP_BAR_X))
#define TOP_BAR_H ((int)(core_asset_config.resolution_h / 15))
#define ENTITY_DESPAWNED_LISTENER_ID 123

typedef struct ed_screen_t ed_screen_t;

enum ed_save_prompt_action
{
    ED_SAVE_PROMPT_ACTION_open_project,
    ED_SAVE_PROMPT_ACTION_NEW_PROJECT
};

enum selected_entity_window_mode
{
    SELECTED_ENTITY_WINDOW_ALL_ENTITIES,
    SELECTED_ENTITY_WINDOW_SINGLE_ENTITY
};

struct ed_screen_t
{
    ed_state_t              state;
    gui_text_input_style_t  text_input_style1;
    gui_text_input_style_t  text_input_style2;
    gui_button_style_t      button_style_no_bg;
    ed_session_t            *session;
    filepicker_t            filepicker;
    double                  accumulated_input_delta;
    struct
    {
        bool32  show;
        dchar   *path;
        char    width[12];
        char    height[12];
        char    name[MUTA_MAP_FILE_NAME_LEN + 1];
    } new_project_dialogue;
    struct
    {
        bool32  show;
        dchar   *file_path;
    } open_project_dialogue;
    struct
    {
        bool32          show;
        char            search_buf[ENT_MAX_CREATURE_TYPE_NAME_LEN + 1];
        enum ed_brush   tab;
        enum ent_sex    sex;
    } brush_selection;
    struct
    {
        bool32  show_command_line;
        char    command_buffer[COMMAND_BUFFER_SIZE];
    } log;
    struct
    {
        bool32                      show;
        enum ed_save_prompt_action  next_action;
    } save_prompt;
    struct
    {
        ed_entity_t entities[ED_SESSION_MAX_HOVERED_ENTITIES];
        uint32                              num_entities;
        enum selected_entity_window_mode    mode;
        ed_entity_t                         single_entity;
    } selected_entities;
    struct {
        bool32 show;
    } esc_menu;
};

static const char *_command_line_text_input_id = "##Command";

static int
_init(void);

static void
_destroy(void);

static void
_update(double dt);

static void
_open(void);

static void
_close(void);

static void
_clear_background(void);

static void
_draw_top_bar(void);

static void
_draw_selected_entities(void);

static void
_draw_new_project_dialogue(void);

static void
_draw_open_project_dialogue(void);

static void
_draw_log(bool32 *ret_close_log);

static void
_draw_overlay_text(void);

static void
_draw_brush_selection(void);

static void
_draw_save_prompt(void);

static void
_draw_esc_menu(void);

static void
_open_new_project_dialogue(void);

static void
_close_new_project_dialogue(void);

static void
_create_new_project(void);

static void
_open_open_project_dialogue(void);

static void
_close_open_project_dialogue(void);

static void
_open_brush_selection(void);

static void
_close_brush_selection(void);

static void
_open_esc_menu(void);

static void
_close_esc_menu(void);

static void
_update_inputs(double dt);

static void
_exit_screen(void);

static void
_close_any_blocking_windows(void);

static void
_brush_selection_for_each_creature(creature_def_t *def, void *user_data);

static void
_brush_selection_for_each_static_object(static_object_def_t *def,
    void *user_data);

static void
_brush_selection_for_each_dynamic_object(dynamic_object_def_t *def,
    void *user_data);

static void
_on_entity_despawned(entity_event_t *event, entity_listen_t *listen);

static void
_clear_selected_entities(void);

screen_t editor_screen =
{
    .name       = "editor",
    .init       = _init,
    .destroy    = _destroy,
    .update     = _update,
    .open       = _open,
    .close      = _close
};

static ed_screen_t *_s;

static int
_init(void)
{
    /* Initialize only if editor_mode was set to true in the config or the
     * command line parameter -e was passed (command line parameter overrides
     * config option). */
    if (!core_config.editor_mode)
        return 0;
    _s = ecalloc(sizeof(ed_screen_t));
    ed_state_init(&_s->state);
    _s->text_input_style1 = gui_create_text_input_style();
    for (int i = 0; i < 3; ++i)
    {
        _s->text_input_style1.states[i].title_font = gui_style_menu_font_fancy;
        _s->text_input_style1.states[i].input_font = gui_style_menu_font;
        _s->text_input_style1.states[i].title_origin = GUI_TOP_LEFT;
        _s->text_input_style1.states[i].title_pixel_offset[1] = 0;
    }
    _s->text_input_style2 = _s->text_input_style1;
    for (int i = 0; i < 3; ++i)
    {
        _s->text_input_style2.states[i].background_color[3] = 100;
        _s->text_input_style2.states[i].border.color[3]     = 100;
    }
    /* Backgroundless button style */
    _s->button_style_no_bg = gui_create_button_style();
    for (int i = 0; i < 3; ++i)
    {
        gui_button_state_style_t *s = &_s->button_style_no_bg.states[i];
        memset(s->background_color, 0, sizeof(s->background_color));
        memset(s->border.color, 0, sizeof(s->border.color));
        s->title_origin = GUI_CENTER_LEFT;
    }
    gui_set_color_array(
        _s->button_style_no_bg.states[GUI_BUTTON_STATE_HOVERED].title_color,
        190, 190, 190, 255);
    gui_set_color_array(
        _s->button_style_no_bg.states[GUI_BUTTON_STATE_PRESSED].title_color,
        128, 128, 128, 255);
    fpick_init(&_s->filepicker, FPICK_ALLOW_PICK_DIRECTORY);
    _s->new_project_dialogue.path = dstr_create_empty(256);
    return 0;
}

static void
_destroy(void)
{
    if (!core_config.editor_mode)
        return;
    fpick_destroy(&_s->filepicker);
    ed_state_destroy(&_s->state);
    dstr_free(&_s->new_project_dialogue.path);
    dstr_free(&_s->open_project_dialogue.file_path);
    free(_s);
}

static void
_update(double dt)
{
    _clear_background();
    bool32 close_log;
    _draw_log(&close_log);
    _draw_overlay_text();
    _draw_top_bar();
    _draw_selected_entities();
    _draw_brush_selection();
    _draw_new_project_dialogue();
    _draw_open_project_dialogue();
    _draw_esc_menu();
    _update_inputs(dt);
    if (gui_is_any_element_hovered())
        ed_session_set_tile_selector_visibility(_s->session, 0);
    else
        ed_session_set_tile_selector_visibility(_s->session, 1);
    ed_session_update(_s->session, dt);
    ed_session_render_world(_s->session);
    if (close_log)
        _s->log.show_command_line = 0;
}

static void
_open(void)
{
    muta_assert(core_config.editor_mode);
    dstr_set_len(&_s->new_project_dialogue.path, 0);
    ed_state_clear(&_s->state);
    _s->session                         = ed_state_new_session(&_s->state);
    _s->new_project_dialogue.show       = 0;
    _s->log.show_command_line           = 0;
    _s->log.command_buffer[0]           = 0;
    _s->brush_selection.show            = 0;
    _s->brush_selection.tab             = ED_BRUSH_TILE;
    _s->brush_selection.search_buf[0]   = 0;
    _s->brush_selection.sex             = ENT_SEX_MALE;
    _s->selected_entities.num_entities  = 0;
    _s->esc_menu.show                   = 0;
    _open_open_project_dialogue();
}

static void
_close(void)
{
    ed_state_destroy_session(&_s->state, _s->session);
    gui_button_style(0);
    gui_text_input_style(0);
    gui_win_style(0);
}

static void
_clear_background(void)
{
    gl_viewport_and_scissor(0, 0, core_window_w(), core_window_h());
    gl_color(0.0f, 0.0f, 0.0f, 1.0f);
    gl_clear(RB_CLEAR_COLOR_BIT);
    int viewport[4];
    core_compute_target_viewport(viewport);
    gl_viewport_and_scissor(viewport[0], viewport[1], viewport[2], viewport[3]);
    gl_scissor(viewport[0], viewport[1], viewport[2], viewport[3]);
    gl_color(0.125f, 0.463f, 1.f, 0.f);
    gl_clear(RB_CLEAR_COLOR_BIT);
}

static void
_draw_top_bar(void)
{

    if (_s->session->project_open)
        return;
    int x       = TOP_BAR_X;
    int y       = TOP_BAR_Y;
    int w       = TOP_BAR_W;
    int h       = TOP_BAR_H;
    gui_origin(GUI_TOP_LEFT);
    gui_win_style(&gui_style_topless_window);
    gui_begin_win("##top bar", x, y, w, h, 0);
    int num_buttons     = 4;
    int cw              = gui_get_current_win_viewport_w();
    int ch              = gui_get_current_win_viewport_h();
    int cox             = (int)((float)cw * 0.005f);
    int coy             = (int)((float)ch * 0.05f);
    int button_d        = cox;
    int button_w = (cw - cox * 2) / num_buttons - (num_buttons - 1) * button_d /
        num_buttons;
    int button_h = ch - coy * 2;
    if (gui_button("Open##top bar", cox, coy, button_w, button_h, 0))
        _open_open_project_dialogue();
    if (gui_button("New##top bar",
        gui_get_last_button_x() + gui_get_last_button_w() + button_d, coy,
        button_w, button_h, 0))
        _open_new_project_dialogue();
    if (gui_button("Save##top bar",
        gui_get_last_button_x() + gui_get_last_button_w() + button_d, coy,
        button_w, button_h, 0))
        ed_session_save_project(_s->session);
    if (gui_button("Exit##top bar",
        gui_get_last_button_x() + gui_get_last_button_w() + button_d, coy,
        button_w, button_h, 0))
        _exit_screen();
    gui_end_win();
}

static void
_draw_selected_entities(void)
{
    uint32 num_entities = _s->selected_entities.num_entities;
    if (!num_entities)
        return;
    int win_w = core_asset_config.resolution_w / 5;
    int win_h = core_asset_config.resolution_h / 3;
    gui_origin(GUI_TOP_RIGHT);
    gui_win_style(&gui_style_topless_window);
    gui_begin_win("Selected entities", TOP_BAR_X, win_h, win_w, win_h, 0);
    gui_origin(GUI_TOP_LEFT);
    if (_s->selected_entities.mode == SELECTED_ENTITY_WINDOW_ALL_ENTITIES)
    {
        uint32  button_w    = gui_get_current_win_viewport_w();
        int     y           = 0;
        int button_h = (int)(1.5f * (float)gui_get_default_font()->height);
        for (uint32 i = 0; i < num_entities; ++i)
        {
            if (gui_button(gui_format_id("%s##selected_entity_%u",
                entity_get_name(_s->selected_entities.entities[i].entity), i),
                0, y, button_w, button_h, 0))
            {
                _s ->selected_entities.single_entity =
                    _s->selected_entities.entities[i];
                _s->selected_entities.mode =
                    SELECTED_ENTITY_WINDOW_SINGLE_ENTITY;
            }
            y += button_h;
        }
    } else
    {
        int num_buttons = 3;
        int button_w = gui_get_current_win_viewport_w() / num_buttons;
        int button_h = (int)(1.5f * (float)gui_get_default_font()->height);
        gui_text(entity_get_name(_s->selected_entities.single_entity.entity),
            0, 0, 0);
        gui_textf("DB ID: %u",
            0, 0, gui_get_last_text_y() + gui_get_last_text_h(),
            _s->selected_entities.single_entity.db_id);
        gui_origin(GUI_BOTTOM_LEFT);
        if (gui_button("Remove##selected_entity_remove", 0, 0, button_w,
            button_h, 0))
            ed_session_remove_entity(_s->session,
                _s->selected_entities.single_entity.entity);
    }
    gui_end_win();
}

static void
_draw_new_project_dialogue(void)
{
    if (!_s->new_project_dialogue.show)
        return;
    int w = core_asset_config.resolution_w / 2;
    int h = core_asset_config.resolution_h / 2;
    gui_win_style(0);
    gui_origin(GUI_CENTER_CENTER);
    gui_begin_win("New map##new map dialogue", 0, 0, w, h, 0);
    gui_text_input_style(&gui_style_login_text_input);
    int guide_w = gui_get_current_win_viewport_w() / 2;
    int guide_h = gui_get_current_win_viewport_h() / 4 * 3;
    gui_begin_guide(0, 0, guide_w, guide_h);
    gui_origin(GUI_TOP_CENTER);
    gui_text_input_style(&_s->text_input_style1);
    int num_rows    = 4;
    int row_w       = guide_w;
    int row_h       = guide_h / num_rows;
    gui_text_input_dstr("Directory name##new map file path",
        &_s->new_project_dialogue.path, 0, 0, row_w, row_h, 0);
    gui_text_input("Map name##new map name", _s->new_project_dialogue.name,
       sizeof(_s->new_project_dialogue.name), gui_get_last_text_input_x(),
       gui_get_last_text_input_y() + gui_get_last_text_input_h(),
       row_w, row_h, 0);
    gui_text_input("Width (chunks)##new map width",
        _s->new_project_dialogue.width, sizeof(_s->new_project_dialogue.width),
        gui_get_last_text_input_x() - (int)roundf((float)row_w * 0.25f),
        gui_get_last_text_input_y() + gui_get_last_text_input_h(), row_w / 2,
        row_h, GUI_TEXT_INPUT_FLAG_POSITIVE_INTEGER);
    gui_text_input("Height (chunks)##new map height",
        _s->new_project_dialogue.height, sizeof(_s->new_project_dialogue.height),
        gui_get_last_text_input_x() + gui_get_last_text_input_w(),
        gui_get_last_text_input_y(), row_w / 2, row_h,
        GUI_TEXT_INPUT_FLAG_POSITIVE_INTEGER);
    float   button_offset   = 0.1f;
    int     button_w        = row_w / 2 - (int)(button_offset * (float)row_w);
    int     button_h        = row_h - 2 * (int)(button_offset * (float)row_h);
    if (gui_button("Create",
        -button_w / 2 - (int)(0.5f * button_offset * (float)row_w),
        gui_get_last_text_input_y() + gui_get_last_text_input_h() +
            (int)(button_offset * (float)row_h), button_w, button_h, 0))
        _create_new_project();
    if (gui_button("Cancel",
        button_w / 2 + (int)(0.5f * button_offset * (float)button_w),
        gui_get_last_button_y(), button_w, button_h, 0))
        _close_new_project_dialogue();
    gui_end_guide();
    gui_end_win();
}

static void
_draw_open_project_dialogue(void)
{
    if (!_s->open_project_dialogue.show)
        return;
    gui_origin(GUI_CENTER_CENTER);
    int w = core_asset_config.resolution_w / 2;
    int h = core_asset_config.resolution_h / 2;
    int r = fpick_draw(&_s->filepicker, "Open map##new map dialoge", 0, 0, w, h,
        &_s->open_project_dialogue.file_path);
    if (r == 1)
    {
        if (!ed_session_open_project(_s->session,
            _s->open_project_dialogue.file_path))
            _close_open_project_dialogue();
    } else
    if (r == -1)
        _close_open_project_dialogue();
}

static void
_draw_log(bool32 *ret_close_log)
{
    *ret_close_log = 0;
    gui_origin(GUI_BOTTOM_LEFT);
    int num_lines       = 10;
    int x               = (int)(0.01f * (float)core_asset_config.resolution_w);
    int y               = (int)(0.01f * (float)core_asset_config.resolution_h);
    int log_w           = core_asset_config.resolution_w / 3;
    int text_input_h    = 2 * gui_get_default_font()->height;
    if (_s->log.show_command_line)
    {
        gui_text_input_style(&_s->text_input_style2);
        gui_text_input(_command_line_text_input_id, _s->log.command_buffer,
            COMMAND_BUFFER_SIZE, x, y, log_w, text_input_h, 0);
        if (gui_text_input_enter_pressed())
        {
            ed_session_parse_command(_s->session, _s->log.command_buffer);
            _s->log.command_buffer[0] = 0;
            *ret_close_log = 1;
        }
    }
    y += text_input_h;
    for (int i = 0; i < num_lines; ++i)
    {
        ed_log_line_t *line = ed_log_get(&_s->state.log, i);
        if (!line)
            break;
        switch (line->type)
        {
        case ED_LOG_INFO:
            gui_color(255, 255, 255, 255);
            break;
        case ED_LOG_ERROR:
            gui_color(255, 0, 0, 255);
            break;
        case ED_LOG_WARNING:
            gui_color(255, 255, 0, 255);
            break;
        case ED_LOG_SUCCESS:
            gui_color(0, 255, 0, 255);
            break;
        }
        gui_text(line->str, log_w, x, y);
        y += gui_get_last_text_h();
    }
    if (_s->log.show_command_line)
    {
        gui_set_active_win(0);
        gui_set_active_text_input(_command_line_text_input_id);
    }
}

static void
_draw_overlay_text(void)
{
    gui_origin(GUI_BOTTOM_RIGHT);
    gui_color(255, 255, 255, 255);
    const char *open_project_name = ed_session_get_open_project_name(
        _s->session);
    int x = (int)(0.01f * (float)core_asset_config.resolution_w);
    int y = (int)(0.01f * (float)core_asset_config.resolution_h);
    gui_text(open_project_name ? open_project_name : "No map open", 0, x, y);
    /* Camera position */
    int camera[3];
    ed_session_get_camera_position(_s->session, camera);
    gui_textf("Camera: %d, %d, %d", 0, x,
        gui_get_last_text_y() + gui_get_last_text_h(), camera[0], camera[1],
        camera[2]);
    /* Tile position */
    int tile[3];
    if (ed_session_get_hovered_tile(_s->session, tile))
        gui_textf("Tile: %d, %d, %d", 0, x,
            gui_get_last_text_y() + gui_get_last_text_h(), tile[0], tile[1],
                tile[2]);
    gui_textf("FPS: %d", 0, x,
        gui_get_last_text_y() + gui_get_last_text_h(), core_fps());
}

static void
_draw_brush_selection(void)
{
    if (!_s->brush_selection.show)
        return;
    gui_origin(GUI_TOP_LEFT);
    int win_x = TOP_BAR_X;
    int win_y = TOP_BAR_Y + TOP_BAR_H +
        (int)(0.1f * core_asset_config.resolution_h);
    int win_w = core_asset_config.resolution_w / 5;
    int win_h = (int)(0.36f * (float)core_asset_config.resolution_h);
    gui_win_style(0);
    gui_begin_win("Object types", win_x, win_y, win_w, win_h, 0);
    /* Tabs */
    int num_tab_buttons = 4;
    int tab_w           = gui_get_current_win_viewport_w() / num_tab_buttons;
    int tab_h           = gui_get_current_win_viewport_h() / 20;
    int tab_x           = 0;
    int tab_y           = 0;
    int bottom_bar_h    = gui_get_default_font()->height + 2;
    if (gui_button("Tile", tab_x, tab_y, tab_w, tab_h, 0))
        _s->brush_selection.tab = ED_BRUSH_TILE;
    tab_x += tab_w;
    if (gui_button("Creature", tab_x, tab_y, tab_w, tab_h, 0))
        _s->brush_selection.tab = ED_BRUSH_CREATURE;
    tab_x += tab_w;
    if (gui_button("Static", tab_x, tab_y, tab_w, tab_h, 0))
        _s->brush_selection.tab = ED_BRUSH_STATIC_OBJECT;
    tab_x += tab_w;
    if (gui_button("Dynamic", tab_x, tab_y, tab_w, tab_h, 0))
        _s->brush_selection.tab = ED_BRUSH_DYNAMIC_OBJECT;
    /* Search bar */
    gui_text_input_style_t *text_input_style = &_s->text_input_style1;
    gui_text_input_style(text_input_style);
    int text_input_h        = tab_h;
    int text_input_min_h    = gui_get_text_input_style_min_height(
        text_input_style);
    if (text_input_h < text_input_min_h)
        text_input_h = text_input_min_h;
    gui_text_input("##search object type", _s->brush_selection.search_buf,
        sizeof(_s->brush_selection.search_buf), 0,
        gui_get_last_button_y() + gui_get_last_button_h(),
        gui_get_current_win_viewport_w(), text_input_h, 0);
    int inner_win_x = 0;
    int inner_win_y = gui_get_last_text_input_y() + gui_get_last_text_input_h();
    int inner_win_w = gui_get_current_win_viewport_w();
    int inner_win_h = gui_get_current_win_viewport_h() -
        gui_get_last_text_input_h() - gui_get_last_button_h() - bottom_bar_h;
    gui_begin_empty_win("search object type inner", inner_win_x, inner_win_y,
        inner_win_h, inner_win_h, GUI_WIN_SCROLLABLE);
    switch (_s->brush_selection.tab)
    {
    case ED_BRUSH_TILE:
    {
        int x = 0;
        int y = 0;
        int w = gui_get_current_win_viewport_w();
        int h = gui_get_default_font()->height;
        gui_origin(GUI_TOP_LEFT);
        gui_button_style(&_s->button_style_no_bg);
        for (int i = 0; i < TILE_MAX_TYPES; ++i)
        {
            tile_def_t *def = tile_get_def(i);
            if (!def->defined)
                continue;
            if (_s->brush_selection.search_buf[0] &&
                !str_insensitive_contains(def->name,
                    _s->brush_selection.search_buf))
                continue;
            if (gui_button(gui_format_id("[%d] %s##brush%d", i, def->name, i),
                x, y, w, h, 0))
                ed_session_set_brush(_s->session,
                    (ed_brush_t){.type = ED_BRUSH_TILE, .tile = (tile_t)i});
            y += h;
        }
    }
        break;
    case ED_BRUSH_CREATURE:
    {
        int xywh[4] = {
            0,
            0,
            gui_get_current_win_viewport_w(),
            gui_get_default_font()->height};
        gui_origin(GUI_TOP_LEFT);
        gui_button_style(&_s->button_style_no_bg);
        ent_for_each_creature_def(_brush_selection_for_each_creature, xywh);
    }
        break;
    case ED_BRUSH_STATIC_OBJECT:
    {
        int xywh[4] = {
            0,
            0,
            gui_get_current_win_viewport_w(),
            gui_get_default_font()->height};
        gui_origin(GUI_TOP_LEFT);
        gui_button_style(&_s->button_style_no_bg);
        ent_for_each_static_object_def(_brush_selection_for_each_static_object,
            xywh);
    }
        break;
    case ED_BRUSH_DYNAMIC_OBJECT:
    {
        int xywh[4] = {
            0,
            0,
            gui_get_current_win_viewport_w(),
            gui_get_default_font()->height};
        gui_origin(GUI_TOP_LEFT);
        gui_button_style(&_s->button_style_no_bg);
        ent_for_each_dynamic_object_def(
            _brush_selection_for_each_dynamic_object, xywh);
    }
        break;
    default:
        muta_assert(0);
    }
    gui_end_win();
    int         isodir      = ed_session_get_brush_direction(_s->session);
    const char  *dir_as_str = iso_dir_to_proper_str(isodir);
    gui_origin(GUI_TOP_CENTER);
    gui_origin(GUI_BOTTOM_LEFT);
    gui_button_style(0);
    int bottom_button_w = inner_win_w / 4;
    int bottom_button_h = bottom_bar_h;
    /* Bottom button row 1 */
    if (gui_button(dir_as_str, 0, 0, bottom_button_w, bottom_button_h, 0))
        ed_session_set_brush_direction(_s->session, (isodir + 1) % NUM_ISODIRS);
    /* Bottom button row 2 */
    if (_s->brush_selection.tab == ED_BRUSH_CREATURE)
        if (gui_button(
            _s->brush_selection.sex == ENT_SEX_MALE ? "Male" : "Female",
            bottom_button_w, 0, bottom_button_w, bottom_button_h, 0))
        {
            if (_s->brush_selection.sex == ENT_SEX_MALE)
                _s->brush_selection.sex = ENT_SEX_FEMALE;
            else
                _s->brush_selection.sex = ENT_SEX_MALE;
        }
    gui_end_win();
}

static void
_draw_save_prompt(void)
{
    int win_w = core_asset_config.resolution_w;
    int win_h = core_asset_config.resolution_h;
    gui_origin(GUI_CENTER_LEFT);
    gui_begin_win("save prompt base", 0, 0, win_w, win_h, 0);
    gui_end_win();
}

static void
_draw_esc_menu(void)
{
    if (!_s->esc_menu.show)
        return;
    gui_win_style(&gui_style_topless_window);
    gui_origin(GUI_CENTER_CENTER);
    int win_w = core_asset_config.resolution_w / 5;
    int win_h = win_w;
    gui_begin_win("#menu", 0, 0, win_w, win_h, 0);
    int num_buttons     = 5;
    int button_w        = gui_get_current_win_viewport_w();
    int button_h        = (int)roundf((float)gui_get_current_win_viewport_h() /
        (float)num_buttons);
    gui_origin(GUI_TOP_LEFT);
    int y = 0;
    if (gui_button("Objects##esc", 0, y, button_w, button_h, 0))
    {
        if (_s->brush_selection.show)
            _close_brush_selection();
        else
            _open_brush_selection();
    }
    y += button_h;
    if (gui_button("Save##esc", 0, y, button_w, button_h, 0))
        ed_session_save_project(_s->session);
    y += button_h;
    if (gui_button("Open##esc", 0, y, button_w, button_h, 0))
    {
        _close_esc_menu();
        _open_open_project_dialogue();
    }
    y += button_h;
    if (gui_button("New project##esc", 0, y, button_w, button_h, 0))
    {
        _close_esc_menu();
        _open_new_project_dialogue();
    }
    y += button_h;
    if (gui_button("Exit", 0, y, button_w, button_h, 0))
        _exit_screen();
    gui_end_win();
}

static void
_open_new_project_dialogue(void)
{
    _close_any_blocking_windows();
    _s->new_project_dialogue.show       = 1;
    _s->new_project_dialogue.path[0]    = 0;
}

static void
_close_new_project_dialogue(void)
    {_s->new_project_dialogue.show = 0;}

static void
_create_new_project(void)
{
    int32 w_in_chunks = atoi(_s->new_project_dialogue.width);
    int32 h_in_chunks = atoi(_s->new_project_dialogue.height);
    if (!ed_session_new_project(_s->session, _s->new_project_dialogue.path,
        _s->new_project_dialogue.name, w_in_chunks, h_in_chunks) &&
        !ed_session_open_project(_s->session, _s->new_project_dialogue.path))
        _s->new_project_dialogue.show = 0;
}

static void
_open_open_project_dialogue(void)
{
    _close_any_blocking_windows();
    char *working_directory = get_current_working_directory();
    fpick_change_directory(&_s->filepicker, working_directory);
    free(working_directory);
    _s->open_project_dialogue.show = 1;
}

static void
_close_open_project_dialogue(void)
    {_s->open_project_dialogue.show = 0;}

static void
_open_brush_selection(void)
{
    if (_s->session->project_open)
        _s->brush_selection.show = 1;
}

static void
_close_brush_selection(void)
    {_s->brush_selection.show = 0;}

static void
_open_esc_menu(void)
{
    if (_s->session->project_open)
        _s->esc_menu.show = 1;
}

static void
_close_esc_menu(void)
    {_s->esc_menu.show = 0;}

static void
_update_inputs(double dt)
{
    int viewport[4];
    core_compute_target_viewport(viewport);
    int mx = core_mouse_x();
    int my = core_mouse_y();
    if (mx >= viewport[0] && mx < viewport[0] + viewport[2] &&
        my >= viewport[1] && my < viewport[1] + viewport[3])
    {
        bool32 should_draw = 0;
        if (ed_session_get_brush(_s->session).type == ED_BRUSH_TILE)
        {
            if (core_mouse_button_down(CORE_MOUSE_BUTTON_LEFT) &&
                !core_mouse_button_down(CORE_MOUSE_BUTTON_RIGHT))
                should_draw = 1;
        } else
        {
            if (core_mouse_button_down_now(CORE_MOUSE_BUTTON_LEFT))
                should_draw = 1;
        }
        /* Right click on entities to open entity selection menu. */
        if (!gui_is_any_element_hovered() &&
            core_mouse_button_down_now(CORE_MOUSE_BUTTON_RIGHT))
        {
            _clear_selected_entities();
            uint32 num_entities = ed_session_get_hovered_entities(_s->session,
                _s->selected_entities.entities);
            for (uint32 i = 0; i < num_entities; ++i)
            {
                entity_t *entity = _s->selected_entities.entities[i].entity;
                entity_listen_t listen_data =
                    {.id = ENTITY_DESPAWNED_LISTENER_ID};
                entity_listen_to_entity_event(entity, ENTITY_EVENT_WILL_DESPAWN,
                    &listen_data, _on_entity_despawned);
            }
            _s->selected_entities.num_entities = num_entities;
            _s->selected_entities.mode = SELECTED_ENTITY_WINDOW_ALL_ENTITIES;
            /* If there's only one entity under the mouse, set the window mode
             * to show single entity. */
            if (num_entities == 1)
            {
                _s->selected_entities.single_entity =
                    _s->selected_entities.entities[0];
                _s->selected_entities.mode =
                    SELECTED_ENTITY_WINDOW_SINGLE_ENTITY;
            }
        }
        if (should_draw)
        {
            int tile[3];
            if (ed_session_get_hovered_tile(_s->session, tile))
                ed_session_draw_with_brush(_s->session, tile[0], tile[1],
                    tile[2]);
        }
    }
    if (gui_is_any_text_input_active())
    {
        if (core_key_down_now(CORE_KEY_ESCAPE))
        {
            _s->log.show_command_line = 0;
            _s->log.command_buffer[0] = 0;
            gui_set_active_text_input(0);
        }
        return;
    }
    if (core_key_down_now(CORE_KEY_ESCAPE))
    {
        if (_s->esc_menu.show)
            _close_esc_menu();
        else
            _open_esc_menu();
    }
    /* Keyboard. Only polled if command line is not open. */
    if (core_key_down_now(CORE_KEY_UP))
        ed_session_set_working_z(_s->session,
            ed_session_get_working_z(_s->session) + 1);
    if (core_key_down_now(CORE_KEY_DOWN))
        ed_session_set_working_z(_s->session,
            ed_session_get_working_z(_s->session) - 1);
    /* Menu */
    if (core_key_down_now(CORE_KEY_O))
    {
        if (core_key_down(CORE_KEY_LALT))
            _open_open_project_dialogue();
        else
        {
            if (_s->brush_selection.show)
                _close_brush_selection();
            else
                _open_brush_selection();
        }
    }
    if (core_key_down_now(CORE_KEY_N) && core_key_down(CORE_KEY_LALT))
        _open_new_project_dialogue();
    if (core_key_down_now(CORE_KEY_E) && core_key_down(CORE_KEY_LALT))
        _open_new_project_dialogue();
    if (core_key_down_now(CORE_KEY_S) && core_key_down(CORE_KEY_LCTRL))
        ed_session_save_project(_s->session);
    /* Log */
    if (core_key_down_now(CORE_KEY_RETURN))
    {
        if (!_s->log.show_command_line)
            _s->log.show_command_line = 1;
    }
    /* WASD movement */
    _s->accumulated_input_delta += dt;
    while (_s->accumulated_input_delta >= INPUT_REPEAT_DELTA)
    {
        _s->accumulated_input_delta -= INPUT_REPEAT_DELTA;
        if (!core_key_down(CORE_KEY_LCTRL))
        {
            if (core_key_down(CORE_KEY_D))
                ed_session_move_camera_relative(_s->session, 1, 0, 0);
            if (core_key_down(CORE_KEY_A))
                ed_session_move_camera_relative(_s->session, -1, 0, 0);
            if (core_key_down(CORE_KEY_W))
                ed_session_move_camera_relative(_s->session, 0, -1, 0);
            if (core_key_down(CORE_KEY_S))
                ed_session_move_camera_relative(_s->session, 0, 1, 0);
        }
    }
    /* Space movement (Z axis) */
    bool32 move_z = 0;
    if (!core_key_down(CORE_KEY_LCTRL))
    {
        if (core_key_down_now(CORE_KEY_SPACE))
            move_z = 1;
    } else
    {
        if (core_key_down(CORE_KEY_SPACE))
            move_z = 1;
    }
    if (move_z)
    {
        if (core_key_down(CORE_KEY_LSHIFT))
            ed_session_move_camera_relative(_s->session, 0, 0, -1);
        else
            ed_session_move_camera_relative(_s->session, 0, 0, 1);
    }
    if (core_key_down_now(CORE_KEY_TAB) && _s->brush_selection.show)
    {
        int tab = _s->brush_selection.tab;
        switch (tab)
        {
        case ED_BRUSH_TILE:
            _s->brush_selection.tab = ED_BRUSH_CREATURE;
            break;
        case ED_BRUSH_CREATURE:
            _s->brush_selection.tab = ED_BRUSH_STATIC_OBJECT;
            break;
        case ED_BRUSH_STATIC_OBJECT:
            _s->brush_selection.tab = ED_BRUSH_DYNAMIC_OBJECT;
            break;
        case ED_BRUSH_DYNAMIC_OBJECT:
            _s->brush_selection.tab = ED_BRUSH_TILE;
            break;
        }
    }
    if (core_key_down_now(CORE_KEY_ESCAPE))
        _s->selected_entities.num_entities = 0;
}

static void
_exit_screen(void)
{
    /* Should ask to save here first. */
    core_set_screen(&main_menu_screen);
}

static void
_close_any_blocking_windows(void)
{
    _s->new_project_dialogue.show   = 0;
    _s->open_project_dialogue.show  = 0;
}

static void
_brush_selection_for_each_creature(creature_def_t *def, void *user_data)
{
    if (_s->brush_selection.search_buf[0] &&
        !str_insensitive_contains(def->name, _s->brush_selection.search_buf))
        return;
    int *xywh = user_data;
    if (gui_button(gui_format_id("[%u] %s##brush creature", def->id, def->name),
        xywh[0], xywh[1], xywh[2], xywh[3], 0))
        ed_session_set_brush(_s->session,
            (ed_brush_t){
                .type       = ED_BRUSH_CREATURE,
                .creature   = {.type_id = def->id, .sex = ENT_SEX_MALE}});
    xywh[1] += xywh[3];
}

static void
_brush_selection_for_each_static_object(static_object_def_t *def,
    void *user_data)
{
    if (_s->brush_selection.search_buf[0] &&
        !str_insensitive_contains(def->name, _s->brush_selection.search_buf))
        return;
    int *xywh = user_data;
    if (gui_button(gui_format_id("[%u] %s##brush static", def->id, def->name),
        xywh[0], xywh[1], xywh[2], xywh[3], 0))
        ed_session_set_brush(_s->session,
            (ed_brush_t){
                .type           = ED_BRUSH_STATIC_OBJECT,
                .static_object  = def->id});
    xywh[1] += xywh[3];
}

static void
_brush_selection_for_each_dynamic_object(dynamic_object_def_t *def,
    void *user_data)
{
    if (_s->brush_selection.search_buf[0] &&
        !str_insensitive_contains(def->name, _s->brush_selection.search_buf))
        return;
    int *xywh = user_data;
    if (gui_button(gui_format_id("[%u] %s##brush dynamic", def->id, def->name),
        xywh[0], xywh[1], xywh[2], xywh[3], 0))
        ed_session_set_brush(_s->session,
            (ed_brush_t){
                .type = ED_BRUSH_DYNAMIC_OBJECT,
                .tile = def->id});
    xywh[1] += xywh[3];
}

static void
_on_entity_despawned(entity_event_t *event, entity_listen_t *listen)
{
    ed_entity_t *entities       = _s->selected_entities.entities;
    uint32      num_entities    = _s->selected_entities.num_entities;
    for (uint32 i = 0; i < num_entities; ++i)
    {
        if (entities[i].entity != event->entity)
            continue;
        num_entities--;
        if (i != num_entities)
            entities[i] = entities[num_entities];
        if (_s->selected_entities.single_entity.entity == event->entity)
            _s->selected_entities.num_entities = 0;
    }
}

static void
_clear_selected_entities(void)
{
    ed_entity_t *entities       = _s->selected_entities.entities;
    uint32      num_entities    = _s->selected_entities.num_entities;
    for (uint32 i = 0; i < num_entities; ++i)
        entity_stop_listening_to_entity_event(entities[i].entity,
            ENTITY_DESPAWNED_LISTENER_ID);
    _s->selected_entities.num_entities = 0;
}
