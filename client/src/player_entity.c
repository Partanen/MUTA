#include "player_entity.h"
#include "world.h"
#include "assets.h"
#include "components.h"
#include "log.h"
#include "name_plate_component.h"

enum player_flag
{
    PLAYER_FLAG_DEAD
};

entity_t *
player_spawn(world_t *world, player_runtime_id_t id, player_race_id_t race_id,
    const char *name, int sex, int direction, int position[3], uint8 move_speed,
    uint32 health_current, uint32 health_max, bool32 dead)
{
    LOG_DEBUG("Spawning player %u...", id);
    entity_t *entity = world_get_player(world, id);
    if (entity)
    {
        LOG_DEBUG("Player with ID %u already exists. Despawning first.", id);
        entity_despawn(entity);
    }
    entity = entity_spawn(world, direction, position);
    if (!entity)
    {
        LOG_ERR("Failed to spawn a player entity!");
        return entity;
    }
    hashtable_einsert(world->player_table, id, hashtable_hash(&id, sizeof(id)),
        entity);
    entity_type_data_t *type_data = entity_get_type_data(entity);
    type_data->type                     = ENTITY_TYPE_PLAYER;
    type_data->player.id                = id;
    type_data->player.race_id           = race_id;
    type_data->player.sex               = sex;
    type_data->player.health_current    = health_current;
    type_data->player.health_max        = health_max;
    type_data->player.flags             = 0;
    if (dead)
        type_data->player.flags |= PLAYER_FLAG_DEAD;
    muta_assert(sizeof(type_data->player.name) > strlen(name));
    strcpy(type_data->player.name, name);
    component_handle_t mobility_component = entity_attach_component(entity,
        &mobility_component_definition);
    if (mobility_component == INVALID_COMPONENT_HANDLE)
        goto despawn;
    mobility_component_set_speed(mobility_component,
        ent_convert_move_speed_to_seconds(move_speed));
    component_handle_t ae_animator_component = entity_attach_component(entity,
        &ae_animator_component_definition);
    if (ae_animator_component == INVALID_COMPONENT_HANDLE)
    {
        LOG_ERR("Failed to attach animator component.");
        goto despawn;
    }
    component_handle_t render_component = entity_attach_component(entity,
        &render_component_definition);
    if (render_component == INVALID_COMPONENT_HANDLE)
    {
        LOG_ERR("Failed to attach render component.");
        goto despawn;
    }
    player_race_def_t *race = ent_get_player_race_def(race_id);
    if (!race)
    {
        LOG_ERR("Race id %u not found.", (uint)race_id);
        goto despawn;
    }
    as_creature_display_t *creature_display = as_get_creature_display(
        race->display_id);
    if (!creature_display)
    {
        LOG_ERR("Creature display %u not found.", race->display_id);
        goto despawn;
    }
    component_handle_t ae_set_component = entity_attach_component(entity,
        &ae_set_component_definition);
    if (ae_set_component == INVALID_COMPONENT_HANDLE)
    {
        LOG_ERR("Failed to attach ae set component.");
        goto despawn;
    }
    /*-- Set animation based on race --*/
    ae_set_component_set_asset(ae_set_component,
        as_get_ae_set(creature_display->ae_set));
    component_handle_t name_plate_component = entity_attach_component(entity,
        &name_plate_component_definition);
    if (name_plate_component == INVALID_COMPONENT_HANDLE)
    {
        LOG_ERR("Failed to attach name plate component.");
        goto despawn;
    }
    name_plate_component_set_offset(name_plate_component,
        creature_display->name_plate_offset_x,
        creature_display->name_plate_offset_y);
    name_plate_component_set_max_health(name_plate_component, health_max);
    name_plate_component_set_current_health(name_plate_component,
        health_current);
    LOG_DEBUG("Successful, id %u, position %d %d %d.", id, position[0],
        position[1], position[2]);
    return entity;
    despawn:
        entity_despawn(entity);
        return 0;
}

void
player_walk(entity_t *entity, int *position)
{
    component_handle_t mobility_component = entity_get_component(entity,
        &mobility_component_definition);
    if (!mobility_component)
    {
        entity_set_position(entity, position);
        return;
    }
    int old_position[3];
    int substraction[2];
    entity_get_position(entity, old_position);
    substraction[0] = position[0] - old_position[0];
    substraction[1] = position[1] - old_position[1];
    int direction = vec2_to_iso_dir(substraction[0], substraction[1]);
    entity_set_direction(entity, direction);
    mobility_component_move(mobility_component, position);
    LOG_DEBUG("Moved from [%d, %d, %d] to [%d, %d, %d].", old_position[0],
        old_position[1], old_position[2], position[0], position[1],
        position[2]);
}

void
player_set_current_health(entity_t *entity, uint32 current_health)
{
    entity_type_data_t *type_data = entity_get_type_data(entity);
    type_data->player.health_current = current_health;
    name_plate_component_t *name_plate_component = entity_get_component(entity,
        &name_plate_component_definition);
    muta_assert(name_plate_component);
    name_plate_component_set_current_health(name_plate_component,
        current_health);
}
