#ifndef MUTA_CLIENT_FILEPICKER_H
#define MUTA_CLIENT_FILEPICKER_H

#include "../../shared/types.h"

typedef struct filepicker_t filepicker_t;
typedef struct async_job_t  async_job_t;

enum fpick_flag
{
    FPICK_ALLOW_PICK_FILE       = (1 << 0),
    FPICK_ALLOW_PICK_DIRECTORY  = (1 << 1)
};

struct filepicker_t
{
    dchar       *current_directory;
    dchar       *next_directory;    /* Edited via a text input field by user. */
    dchar       *tmp_str;           /* For temporary use. */
    dchar       **files;
    size_t      current_file;
    int         flags;
};

void
fpick_init(filepicker_t *fp, int flags);
/* If no flags are passed in, no file can be opened using the filepicker. See
 * enum fpick_flag.
 * This function does not populate the file list (done using
 * fpick_change_directory()). */

void
fpick_destroy(filepicker_t *fp);

int
fpick_change_directory(filepicker_t *fp, const char *path);

size_t
fpick_num_files(filepicker_t *fp);

const char *
fpick_get_file(filepicker_t *fp, size_t index);

bool32
fpick_draw(filepicker_t *fp, const char *title, int x, int y, int w,
    int h, dchar **ret_file_path);
/* Only one filepicker can be drawn in a single frame due to button id
 * collisions. */

#endif /* MUTA_CLIENT_FILEPICKER_H */
