#include "name_plate_component.h"
#include "mobility_component.h"
#include "components.h"
#include "world.h"

static int
_name_plate_system_init(world_t *world, uint32 max_components);

static void
_name_plate_system_destroy(world_t *world);

static component_handle_t
_name_plate_component_attach(entity_t *entity);

static void
_name_plate_component_detach(component_handle_t handle);

static void
_handle_mobility_component_event(entity_t *entity, component_handle_t handle,
    void *event_ptr);

static void
_handle_entity_current_health_changed_event(entity_event_t *event);

static void
_handle_entity_max_health_changed_event(entity_event_t *event);

static entity_event_interest_t _entity_event_interests[] =
{
    {
        .event      = ENTITY_EVENT_CURRENT_HEALTH_CHANGED,
        .callback   = _handle_entity_current_health_changed_event
    },
    {
        .event      = ENTITY_EVENT_MAX_HEALTH_CHANGED,
        .callback   =_handle_entity_max_health_changed_event
    }
};

static component_event_interest_t _component_event_interests[] =
{
    {
        .component_definition   = &mobility_component_definition,
        .callback               = _handle_mobility_component_event
    }
};

void
name_plate_component_set_offset(component_handle_t handle, int16 x,
    int16 y)
{
    name_plate_component_t *component = (name_plate_component_t*)handle;
    component->offset[0] = x;
    component->offset[1] = y;
}

void
name_plate_component_set_current_health(component_handle_t handle,
    uint32 health_current)
{
    name_plate_component_t *component = (name_plate_component_t*)handle;
    component->num = health_current;
}

void
name_plate_component_set_max_health(component_handle_t handle,
    uint32 health_max)
{
    name_plate_component_t *component = (name_plate_component_t*)handle;
    component->max = health_max;
}

component_definition_t name_plate_component_definition =
{
    .system_init                = _name_plate_system_init,
    .system_destroy             = _name_plate_system_destroy,
    .system_update              = 0,
    .component_attach           = _name_plate_component_attach,
    .component_detach           = _name_plate_component_detach,
    .entity_event_interests     = _entity_event_interests,
    .num_entity_event_interests =
        sizeof(_entity_event_interests) / sizeof(entity_event_interest_t),
    .component_event_interests  = _component_event_interests,
    .num_component_event_interests =
        sizeof(_component_event_interests) / sizeof(component_event_interest_t)
};

static int
_name_plate_system_init(world_t *world, uint32 max_components)
{
    name_plate_system_t *system = &world->name_plate_system;
    system->components = emalloc(max_components * sizeof(name_plate_component_t));
    system->num_components = 0;
    system->max_components = max_components;
    for (uint32 i = 0; i < NUM_COMPONENT_DEFINITIONS; ++i)
        if (component_definitions[i] == &name_plate_component_definition)
        {
            system->component_definition_index = i;
            break;
        }
    return 0;
}

static void
_name_plate_system_destroy(world_t *world)
    {free(world->name_plate_system.components);}

static component_handle_t
_name_plate_component_attach(entity_t *entity)
{
    name_plate_system_t *system = &entity_get_world(entity)->name_plate_system;
    muta_assert(system->num_components < system->max_components);
    name_plate_component_t *component =
        &system->components[system->num_components++];
    component->num      = 0;
    component->max      = 0;
    component->entity   = entity;
    int position[3];
    entity_get_position(entity, position);
    for (int i = 0; i < 3; ++i)
        component->tile_position[i] = position[i];
    for (int i = 0; i < 3; ++i)
        component->last_tile_position[i] = position[i];
    component->percentage_travelled = 0.f;
    return component;
}

static void
_name_plate_component_detach(component_handle_t handle)
{
    name_plate_component_t *component = handle;
    name_plate_system_t *system =
        &entity_get_world(component->entity)->name_plate_system;
    uint32 index = (uint32)(component - system->components);
    if (index == system->num_components - 1)
        system->num_components -= 1;
    else
    {
        system->components[index] =
            system->components[--system->num_components];
        entity_t *other_entity = system->components[index].entity;
        component_handle_t *other_components = entity_get_components(
            other_entity);
        other_components[system->component_definition_index] =
            &system->components[index];
    }
}

static void
_handle_mobility_component_event(entity_t *entity, component_handle_t handle,
    void *event_ptr)
{
    mobility_component_event_t *event = event_ptr;
    name_plate_component_t *name_plate_component = entity_get_component(entity,
        &name_plate_component_definition);
    switch (event->type)
    {
    case MOBILITY_COMPONENT_EVENT_START_MOVE:
    {
        mobility_component_start_move_event_t *data = &event->start_move;
        for (int i = 0; i < 3; ++i)
            name_plate_component->last_tile_position[i] = data->last_position[i];
        for (int i = 0; i < 3; ++i)
            name_plate_component->tile_position[i] = data->new_position[i];
        name_plate_component->percentage_travelled = 0;
    }
        break;
    case MOBILITY_COMPONENT_EVENT_STOP_MOVE:
        name_plate_component->percentage_travelled = 0;
        for (int i = 0; i < 3; ++i)
            name_plate_component->last_tile_position[i] =
                name_plate_component->tile_position[i];
        break;
    case MOBILITY_COMPONENT_EVENT_MOVED:
    {
        mobility_component_moved_event_t *data = &event->moved;
        name_plate_component->percentage_travelled = data->percentage_travelled;
    }
        break;
    };
}

static void
_handle_entity_current_health_changed_event(entity_event_t *event)
{
    name_plate_component_t *component = entity_get_component(event->entity,
        &name_plate_component_definition);
    component->num = event->current_health_changed.health_current;
}

static void
_handle_entity_max_health_changed_event(entity_event_t *event)
{
    name_plate_component_t *component = entity_get_component(event->entity,
        &name_plate_component_definition);
    component->max = event->max_health_changed.health_max;
}
