#include "../script_api.h"

static bool32
_can_use(script_entity_t *target);

static void
_on_request(script_entity_t *target);

static void
_on_response(script_entity_t *target, bool32 success);

static uint32_t
_compute_charging_time(script_entity_t *target);

static void
_on_player_moved(void);

ability_script_t auto_attack_ability_script =
{
    .id                     = 0,
    .can_use                = _can_use,
    .on_request             = _on_request,
    .on_response            = _on_response,
    .compute_charging_time  = _compute_charging_time,
    .on_player_moved        = _on_player_moved
};

static bool32
_can_use(script_entity_t *target)
{
    script_entity_t *player_entity = script_get_this_player_entity();
    if (script_is_entity_moving(player_entity))
        return 0;
    if (script_entity_is_this_player(target))
        return 0;
    script_entity_t *player = script_get_this_player_entity();
    int player_pos[3];
    int target_pos[3];
    int distance_vec[3];
    script_get_entity_position(player, player_pos);
    script_get_entity_position(target, target_pos);
    script_compute_distance_vec(player_pos, target_pos, distance_vec);
    if (distance_vec[0] > 1 || distance_vec[1] > 1 || distance_vec[2] > 0)
        return 0;
    return 1;
}

static void
_on_request(script_entity_t *target)
{
    script_log_system_message("Auto attack request sent.");
}

static void
_on_response(script_entity_t *target, bool32 success)
{
    script_log_system_message("Auto attack response received.");
}

static uint32_t
_compute_charging_time(script_entity_t *target)
{
    return 1000;
}

static void
_on_player_moved(void)
{
    script_stop_charge();
}
