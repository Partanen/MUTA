#ifndef MUTA_CLIENT_SKY_H
#define MUTA_CLIENT_SKY_H

#include "../../shared/types.h"

#define SKY_MAX_OBJECTS 32

typedef struct sky_object_t sky_object_t;
typedef struct sky_t        sky_t;

struct sky_object_t
{
    float   x, y;
    float   clip[4];
};

struct sky_t
{
    sky_object_t    objects[SKY_MAX_OBJECTS]; /* darr */
    uint32          num_objects;
    float           wind_speed;
};

int
sky_init_api(void);

void
sky_destroy_api(void);

int
sky_init(sky_t *sky);

void
sky_destroy(sky_t *sky);

void
sky_update(sky_t *sky, double delta);

void
sky_render(sky_t *sky);

#endif /* MUTA_CLIENT_SKY_H */
