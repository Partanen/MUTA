#ifndef MUTA_GAME_STATE_H
#define MUTA_GAME_STATE_H

/* game_state.h: held state for an on-going online session (world, UI, etc.) */

#include "../../shared/types.h"
#include "../../shared/common_defs.h"

/* Forward declaration(s) */
typedef struct byte_buf_t       bbuf_t;
typedef struct world_t          world_t;
typedef struct entity_t         entity_t;
typedef struct entity_handle_t  entity_handle_t;
typedef struct inventory_t      inventory_t;

enum gs_chat_entry_type
{
    GS_CHAT_CATEGORY_UNCATEGORIZED,
    GS_CHAT_CATEGORY_WARNING,
    GS_CHAT_CATEGORY_ERROR,
    GS_CHAT_CATEGORY_ADDON,
    GS_CHAT_CATEGORY_GLOBAL_BROADCAST,
};

int
gs_init(void);

void
gs_destroy(void);

void
gs_start_begin_session(void);

int
gs_end_begin_session(uint32 map_id, player_runtime_id_t id, const char *name,
    player_race_id_t race, int x, int y, int z, int dir, uint8 move_speed,
    uint32 view_distance_h, uint32 view_distance_v,
    ability_id_t abilities[MAX_PLAYER_ABILITIES], uint32 num_abilities,
    uint32 health_current, uint32 health_max, bool32 dead);
/* Begin new in-world game session once the server tells us we've entered the
 * world. Illegal tocall if a session is already in progress. */

void
gs_end_session(void);

bool32
gs_begin_started(void);

bool32
gs_in_session(void);

void
gs_update_and_render(double dt);

void
gs_mouse_up(int button, int x, int y);

void
gs_mouse_down(int button, int x, int y);

void
gs_key_down(int key, bool32 repeat);

void
gs_key_up(int key);

void
gs_flag_special_key_executed(int special_key);

void
gs_read_packet(int msg_type, bbuf_t *bb, int *ret_incomplete, int *ret_dc);
/* Read a packet. Returns < 0 if packet is illegal, > 0 if packet is incomplete,
 * or 0 if packet is ok.
 * Requests disconnect if not in session. */

void
gs_log_system_msg(const char *msg);

void
gs_stop_action_charge(void);

#endif /* MUTA_GAME_STATE_H */
