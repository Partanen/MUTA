#include "ae_animator_component.h"
#include "components.h"
#include "world.h"
#include "assets.h"

struct ae_animator_component_t
{
    entity_t    *entity;
    as_ae_t     *ae_asset;
    uint32      data_index;     /* Relevant only if playmode not PAUSED */
    uint16      current_frame;  /* Only relevant if paused. Otherwise stored in
                                   component_data */
    int8        play_mode;
};

struct ae_animator_component_data_t
{
    uint32  component_index;
    as_ae_t *ae_asset;
    uint32  current_frame;
    uint32  direction;
};

static int
_ae_animator_system_init(world_t *world, uint32 max_components);

static void
_ae_animator_system_destroy(world_t *world);

static void
_ae_animator_system_update(world_t *world, float dt);

static component_handle_t
_ae_animator_component_attach(entity_t *entity);

static void
_ae_animator_component_detach(component_handle_t handle);

static void
_handle_entity_event_set_direction(entity_event_t *event);

static inline ae_animator_component_data_t *
_get_data(ae_animator_component_t *component, ae_animator_system_t *system);

static void
_remove_data_from_array(ae_animator_component_t *component,
    ae_animator_system_t *system, ae_animator_component_data_t *array,
    uint32 *array_count);

static void
_fill_data(ae_animator_component_data_t *data,
    ae_animator_component_t *component, ae_animator_system_t *system);

static entity_event_interest_t _entity_event_interests[] =
{
    {ENTITY_EVENT_SET_DIRECTION, _handle_entity_event_set_direction}
};

component_definition_t ae_animator_component_definition =
{
    _ae_animator_system_init,
    _ae_animator_system_destroy,
    _ae_animator_system_update,
    _ae_animator_component_attach,
    _ae_animator_component_detach,
    _entity_event_interests,
    sizeof(_entity_event_interests) / sizeof(entity_event_interest_t)
};

as_ae_t *
ae_animator_component_get_asset(component_handle_t handle)
    {return ((ae_animator_component_t*)handle)->ae_asset;}

void
ae_animator_component_set_asset(component_handle_t handle, as_ae_t *ae_asset)
{
    ae_animator_component_t *component = handle;
    if (component->ae_asset == ae_asset)
        return;
    component->ae_asset = ae_asset;
    if (!ae_asset)
    {
        ae_animator_component_set_play_mode(component,
            AE_ANIMATOR_COMPONENT_PAUSED);
        return;
    }
    ae_animator_system_t *system =
        &entity_get_world(component->entity)->ae_animator_system;
    ae_animator_component_data_t *data = _get_data(component, system);
    if (!data)
    {
        component->current_frame = 0; /* Only relevant if paused */
        return;
    }
    uint32 direction = entity_get_direction(component->entity);
    data->ae_asset      = ae_asset;
    data->current_frame = 0;
    data->direction     = direction;
    ae_animator_component_event_t event;
    event.type = AE_ANIMATOR_COMPONENT_EVENT_NEW_FRAME;
    if (ae_asset->ae.anims[direction].num_frames)
        event.new_frame.frame = &ae_asset->ae.anims[direction].frames[0];
    else
        event.new_frame.frame = 0;
    entity_post_component_event(component->entity,
        &ae_animator_component_definition, &event);
}

void
ae_animator_component_set_play_mode(component_handle_t handle, int play_mode)
{
    ae_animator_component_t *component = handle;
    if (component->play_mode == play_mode)
        return;
    /*-- If there's no asset, keep paused --*/
    if (!component->ae_asset)
        return;
    ae_animator_component_data_t    data;
    bool32                          post_event = 0;
    ae_animator_system_t            *system =
        &entity_get_world(component->entity)->ae_animator_system;
    if (play_mode == AE_ANIMATOR_COMPONENT_PAUSED)
    {
        ae_animator_component_data_t    *datas;
        uint32                          *num_datas;
        if (component->play_mode == AE_ANIMATOR_COMPONENT_PLAY_LOOP)
        {
            datas       = system->looping;
            num_datas   = &system->num_looping;
        } else
        {
            datas       = system->not_looping;
            num_datas   = &system->num_not_looping;
        }
        component->current_frame =
            system->not_looping[component->data_index].current_frame;
        _remove_data_from_array(component, system, datas, num_datas);
        component->data_index = 0xFFFFFFFF;
        /*-- Frame does not change - don't send event. --*/
    } else
    if (play_mode == AE_ANIMATOR_COMPONENT_PLAY_LOOP)
    {
        if (component->play_mode == AE_ANIMATOR_COMPONENT_PLAY_NO_LOOP)
        {
            data = system->not_looping[component->data_index];
            _remove_data_from_array(component, system, system->looping,
                &system->num_looping);
        } else
        {
            _fill_data(&data, component, system);
            post_event = 1;
        }
        component->data_index = system->num_looping++;
        system->looping[component->data_index] = data;
        DEBUG_PRINTFF("STARTED LOOPING\n");
    } else
    if (play_mode == AE_ANIMATOR_COMPONENT_PLAY_NO_LOOP)
    {
        if (component->play_mode == AE_ANIMATOR_COMPONENT_PLAY_LOOP)
        {
            data = system->looping[component->data_index];
            _remove_data_from_array(component, system, system->not_looping,
                &system->num_not_looping);
        } else
        {
            _fill_data(&data, component, system);
            post_event = 1;
        }
        component->data_index = system->num_not_looping++;
        system->not_looping[component->data_index] = data;
    }
    component->play_mode = play_mode;
    if (!post_event)
        return;
    ae_animator_component_event_t event;
    event.type = AE_ANIMATOR_COMPONENT_EVENT_NEW_FRAME;
    if (data.ae_asset->ae.anims[data.direction].num_frames)
        event.new_frame.frame = &data.ae_asset->ae.anims[
            data.direction].frames[data.current_frame];
    else
        event.new_frame.frame = 0;
    entity_post_component_event(component->entity,
        &ae_animator_component_definition, &event);
}

int
ae_animator_component_get_play_mode(component_handle_t handle)
{
    ae_animator_component_t *component = handle;
    return component->play_mode;
}

uint32
ae_animator_get_current_frame(component_handle_t handle)
{
    ae_animator_component_t *component = handle;
    ae_animator_component_data_t *data = _get_data(component,
        &entity_get_world(component->entity)->ae_animator_system);
    return data ? data->current_frame : 0;
}

static int
_ae_animator_system_init(world_t *world, uint32 max_components)
{
    ae_animator_system_t *system = &world->ae_animator_system;
    memset(system, 0, sizeof(ae_animator_system_t));
    fixed_pool_init(&system->components, max_components);
    system->looping = emalloc(
        max_components * sizeof(ae_animator_component_data_t));
    system->not_looping = emalloc(
        max_components * sizeof(ae_animator_component_data_t));
    return 0;
}

static void
_ae_animator_system_destroy(world_t *world)
{
    ae_animator_system_t *system = &world->ae_animator_system;
    fixed_pool_destroy(&system->components);
    free(system->looping);
    free(system->not_looping);
    memset(system, 0, sizeof(ae_animator_system_t));
}

static void
_ae_animator_system_update(world_t *world, float dt)
{
    ae_animator_system_t            *system = &world->ae_animator_system;
    ae_animator_component_event_t   event;
    ae_animator_component_data_t    *datas;
    uint32                          num_datas;
    /*-- Update looping --*/
    datas       = system->looping;
    num_datas   = system->num_looping;
    for (uint32 i = 0; i < num_datas; ++i)
    {
        as_ae_t *asset = datas[i].ae_asset;
        uint32 direction = datas[i].direction;
        uint32 frame = (datas[i].current_frame + 1) %
            asset->ae.anims[direction].num_frames;
        datas[i].current_frame  = frame;
        event.type              = AE_ANIMATOR_COMPONENT_EVENT_NEW_FRAME;
        event.new_frame.frame   = &asset->ae.anims[direction].frames[frame];
        entity_post_component_event(
            system->components.all[datas[i].component_index].entity,
            &ae_animator_component_definition, &event);
    }
    /*-- Update non-looping --*/
    datas       = system->not_looping;
    num_datas   = system->num_not_looping;
    for (uint32 i = 0; i < num_datas; ++i)
    {
        loop_top: {}
        as_ae_t *asset = datas[i].ae_asset;
        if (!asset->ae.anims[datas[i].direction].num_frames)
            continue;
        uint32 frame = datas[i].current_frame + 1;
        if (frame >= (uint32)asset->ae.anims[datas[i].direction].num_frames)
        {
            _remove_data_from_array(
                &system->components.all[datas[i].component_index], system,
                datas, &num_datas);
            system->num_not_looping = num_datas;
            event.type = AE_ANIMATOR_COMPONENT_EVENT_ANIMATION_END;
            entity_post_component_event(
                system->components.all[datas[i].component_index].entity,
               &mobility_component_definition, &event);
            if (num_datas)
                goto loop_top;
        } else
        {
            event.type              = AE_ANIMATOR_COMPONENT_EVENT_NEW_FRAME;
            event.new_frame.frame   =
                &asset->ae.anims[datas[i].direction].frames[frame];
            entity_post_component_event(
                system->components.all[datas[i].component_index].entity,
               &mobility_component_definition, &event);
        }
    }
    system->num_not_looping = num_datas;
}

static component_handle_t
_ae_animator_component_attach(entity_t *entity)
{
    ae_animator_system_t *system =
        &entity_get_world(entity)->ae_animator_system;
    ae_animator_component_t *component = fixed_pool_new(&system->components);
    if (!component)
        return component;
    component->entity           = entity;
    component->ae_asset         = 0;
    component->play_mode        = AE_ANIMATOR_COMPONENT_PAUSED;
    component->current_frame    = 0;
    return component;
}

static void
_ae_animator_component_detach(component_handle_t handle)
{
    ae_animator_component_t *component = handle;
    ae_animator_system_t *system =
        &entity_get_world(component->entity)->ae_animator_system;
    /*-- If currently playing, remove data from the correct array. --*/
    ae_animator_component_data_t    *data_array = 0;
    uint32                          *data_array_count;
    if (component->play_mode == AE_ANIMATOR_COMPONENT_PLAY_LOOP)
    {
        data_array          = system->looping;
        data_array_count    = &system->num_looping;
    } else
    if (component->play_mode == AE_ANIMATOR_COMPONENT_PLAY_NO_LOOP)
    {
        data_array          = system->not_looping;
        data_array_count    = &system->num_not_looping;
    } else
    {
        fixed_pool_free(&system->components, component);
        return;
    }
    ae_animator_component_data_t *data = &data_array[component->data_index];
    *data = data_array[--(*data_array_count)];
    system->components.all[data->component_index].data_index =
        component->data_index;
}

static void
_handle_entity_event_set_direction(entity_event_t *event)
{
    entity_t *entity = event->entity;
    ae_animator_component_t *component = entity_get_component(entity,
        &ae_animator_component_definition);
    if (!component->ae_asset)
        return;
    ae_animator_system_t *system =
        &entity_get_world(entity)->ae_animator_system;
    ae_animator_component_data_t *data = _get_data(component, system);
    if (!data)
        goto post_event;
    data->direction = event->set_direction.new_direction;
    post_event: {}
        ae_animator_component_event_t new_event;
        new_event.type = AE_ANIMATOR_COMPONENT_EVENT_NEW_FRAME;
        new_event.new_frame.frame =
            &component->ae_asset->ae.anims[
                event->set_direction.new_direction].frames[
                component->current_frame];
        entity_post_component_event(entity, &ae_animator_component_definition,
            &new_event);
}

static inline ae_animator_component_data_t *
_get_data(ae_animator_component_t *component, ae_animator_system_t *system)
{
    if (component->play_mode == AE_ANIMATOR_COMPONENT_PAUSED)
        return 0;
    if (component->play_mode == AE_ANIMATOR_COMPONENT_PLAY_LOOP)
        return &system->looping[component->data_index];
    return &system->not_looping[component->data_index];
}

static void
_remove_data_from_array(ae_animator_component_t *component,
    ae_animator_system_t *system, ae_animator_component_data_t *array,
    uint32 *array_count)
{
    ae_animator_component_data_t *other_data = &array[--(*array_count)];
    if (*array_count == component->data_index)
        return;
    array[component->data_index] = *other_data;
    ae_animator_component_t *other_component =
        &system->components.all[array[component->data_index].component_index];
    other_component->data_index = component->data_index;
    DEBUG_PRINTFF("removed, index: %u, array_acount: %u\n",
        component->data_index, *array_count);
}

static void
_fill_data(ae_animator_component_data_t *data,
    ae_animator_component_t *component, ae_animator_system_t *system)
{
    memset(data, 0, sizeof(*data));
    data->component_index   = fixed_pool_index(&system->components, component);
    data->ae_asset          = component->ae_asset;
    data->direction         = entity_get_direction(component->entity);
    data->current_frame     = component->current_frame;
}
