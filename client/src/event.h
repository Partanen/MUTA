/* Global engine events. */

#ifndef MUTA_CLIENT_EVENT_H
#define MUTA_CLIENT_EVENT_H

#include "../../shared/types.h"

/* Types defined here */
typedef struct event_t event_t;
typedef void (*ev_callback_t)(event_t *e, void *user_data);

enum event
{
    EVENT_ENTER_WORLD_FAILED,
    NUM_EVENTS
};

struct event_t
{
    enum event type;
    union
    {
        struct
        {
            int reason;
        } enter_world_failed;
    };
};

int
ev_init(void);

void
ev_destroy(void);

uint32
ev_add(enum event event, ev_callback_t callback, void *user_data);

void
ev_del(enum event event, uint32 handle);

void
ev_post(event_t *event);
/* Calls all callbacks registered for the event's type. */

#endif
