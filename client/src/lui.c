#include "lui.h"
#include "assets.h"
#include "core.h"
#include "render.h"
#include "hotkey.h"
#include "game_state.h"
#include "shard.h"
#include "world.h"
#include "gui.h"
#include "log.h"
#include "inventory.h"
#include "../../shared/common_utils.h"
#include "../../shared/str_hashtable.h"
#include "../../shared/lua/lauxlib.h"
#include "../../shared/lua/lualib.h"
#include "../../shared/lua/lapi.h"

#define LUI_MAX_OBJECT_ID       ((uint32)0xFFFFFFFF)
#define LUI_LOG                 LOG
#define LUI_LOG_ERROR           LOG_ERR

typedef struct lui_window_obj_t         lui_window_obj_t;
typedef struct lui_window_obj_data_t    lui_window_obj_data_t;
typedef struct lui_font_t               lui_font_t;
typedef struct lui_window_t             lui_window_t;
typedef struct lui_texture_t            lui_texture_t;
typedef struct lui_text_input_t         lui_text_input_t;
typedef struct lui_button_t             lui_button_t;
typedef struct lui_text_t               lui_text_t;
typedef struct addon_parse_ctx_t        addon_parse_ctx_t;
typedef struct addon_t                  addon_t;
typedef struct lui_listener_list_t      lui_listener_list_t;
typedef struct lui_window_style_t       lui_window_style_t;
typedef struct lui_text_input_style_t   lui_text_input_style_t;
typedef struct lui_window_obj_table     lui_window_obj_table_t;
typedef struct str_tex_table            str_tex_table_t;

enum lui_window_flags
{
    LUI_WINDOW_FLAG_SHOWN                       = (1 << 0),
    LUI_WINDOW_FLAG_POSITION_RELATIVE_TO_PARENT = (1 << 1)
};

enum lui_window_obj_types
{
    LUI_WINDOW_OBJ_NONE,
    LUI_WINDOW_OBJ_TEXT,
    LUI_WINDOW_OBJ_BUTTON,
    LUI_WINDOW_OBJ_TEXTURE,
    LUI_WINDOW_OBJ_TEXT_INPUT
};

struct lui_window_t
{
    dchar                   *title;
    int                     origin;
    int                     x, y, w, h;
    int                     relative_x, relative_y; /* Only relevant if position
                                                       relative to parent. */
    uint32                  index;          /* Index in _wins.all */
    uint32                  style_index;    /* Index in _window_styles or
                                               0xFFFFFFFF if empty. */
    lui_window_obj_data_t   *objs;
    uint32                  parent;         /* 0xFFFFFFFF if none */
    uint32                  *children; /* darr */
    uint8                   listening_to[LUI_NUM_EVENTS];
    uint32                  flags;
};

struct lui_button_t
{
    dchar   *name;
    int     w, h;
    uint32  style_index;
};

struct lui_text_t
{
    dchar       *string;
    int         wrap;
    float       scale;
    uint32      font_index;
    int         w, h;
};

struct lui_texture_t
{
    tex_t   *tex;
    float   scale_x;
    float   scale_y;
    float   clip[4];
};

struct lui_text_input_t
{
    dchar   *title;
    char    *buf;
    uint32  buf_size;
    int     w, h;
    uint32  style_index;
};

struct lui_window_obj_t
{
    int     type;
    uint32  id;
    uint32  window;
    uint32  index_in_window;
};
/* Window objects are accessed by their id which is why this structure is stored
 * inside a hashtable. */

union lui_window_obj_type_data_t
{
    lui_text_t          text;
    lui_button_t        button;
    lui_texture_t       texture;
    lui_text_input_t    text_input;
};

struct lui_window_obj_data_t
{
    int                                 type;
    lui_window_obj_t                    *obj;
    int                                 origin;
    int                                 x, y;
    union lui_window_obj_type_data_t    data;
};
/* This is the actual window object structure stored in a lui_window_t */

struct lui_font_t
{
    gui_font_t *data;
};

hashtable_define(lui_window_obj_table, uint32, lui_window_obj_t*);
STR_HASHTABLE_DEFINITION(str_tex_table, tex_t*);

struct addon_parse_ctx_t
{
    int         error;
    addon_t     *addon;
    const char  *dir_path;
    dchar       *lua_file_path;
};

struct addon_t
{
    dchar *name;
    dchar *lua_file_path;
};

struct lui_listener_list_t
{
    uint32 *windows; /* darr */
};

struct lui_window_style_t
{
    dchar           *name;
    gui_win_style_t *style; /* Alloc dynamically for constant memory address. */
};

struct lui_text_input_style_t
{
    dchar                   *name;
    gui_text_input_style_t  *style;
};

static struct
{
    obj_pool_t      pool;
    lui_window_t    **all;
    lui_window_t    **shown;
} _wins;

static bool32                   _in_use;
static const char               *_window_table_key  = "__MUTA_WINDOW_TABLE";
static const char               *_button_table_key  = "__MUTA_BUTTON_TABLE";
static const char               *_hotkey_table_key  = "__MUTA_HOTKEY_TABLE";
static const char *_key_callback_key = "__MUTA_KEY_CALLBACK_TABLE";
static const char *_text_input_key = "__MUTA_TEXT_INPUT_TABLE";
static const char               *_button_id_str     = "_MUTA_BUTTON";
static const char               *_text_input_submit_key = "submit";
static const char *_default_titled_window_style_name    =
    "Default titled window";
static const char *_default_untitled_window_style_name  =
    "Default untitled window";
static lua_State                *_lua;
static addon_t                  *_addons;
static lui_event_t              *_events;
static lui_listener_list_t      _listener_lists[LUI_NUM_EVENTS];
static lui_window_style_t       *_window_styles;
static gui_button_style_t       *_button_styles;
static lui_text_input_style_t   *_text_input_styles;
static lui_font_t               *_fonts;
static uint32                   _running_key_callback_id;
static int                      _active_window_index;   /* -1 if none */
static int                      _hovered_window_index;  /* -1 if none */
static int                      _keyboard_grab_window_index; /* -1 if none. */
static uint32                   _default_font_index;

static struct
{
    obj_pool_t              pool;
    lui_window_obj_table_t  table;
    uint32                  running_id;
} _window_objs;

static struct
{
    obj_pool_t          pool;
    str_tex_table_t     table;
    tex_t               **all;
} _textures;

#define _get_window_obj(id_) \
    lui_window_obj_table_find(&_window_objs.table, (id_))

static inline lui_window_obj_data_t *
_get_window_obj_data(uint32 id)
{
    lui_window_obj_t **o = _get_window_obj(id);
    if (!o)
        return 0;
    return &_wins.all[(*o)->window]->objs[(*o)->index_in_window];
}

static int
_luiapi_get_window(lua_State *lua);

static int
_luiapi_create_window(lua_State *lua);

static int
_luiapi_show_window(lua_State *lua);

static int
_luiapi_hide_window(lua_State *lua);

static int
_luiapi_is_window_shown(lua_State *lua);

static int
_luiapi_set_window_position(lua_State *lua);

static int
_luiapi_set_window_width(lua_State *lua);

static int
_luiapi_set_window_height(lua_State *lua);

static int
_luiapi_set_window_origin(lua_State *lua);

static int
_luiapi_set_window_parent(lua_State *lua);

static int
_luiapi_set_window_title(lua_State *lua);

static int
_luiapi_set_window_style(lua_State *lua);

static int
_luiapi_is_window_active(lua_State *lua);

static int
_luiapi_is_window_hovered(lua_State *lua);

static int
_luiapi_create_button(lua_State *lua);

static int
_luiapi_create_text(lua_State *lua);

static int
_luiapi_create_texture(lua_State *lua);

static int
_luiapi_create_text_input(lua_State *lua);

static int
_luiapi_listen_to_event(lua_State *lua);

static int
_luiapi_unlisten_to_event(lua_State *lua);

static int
_luiapi_set_event_callback(lua_State *lua);

static int
_luiapi_get_window_position(lua_State *lua);

static int
_luiapi_get_window_width(lua_State *lua);

static int
_luiapi_get_window_height(lua_State *lua);

static int
_luiapi_set_window_obj_origin(lua_State *lua);

static int
_luiapi_set_window_obj_position(lua_State *lua);

static int
_luiapi_set_text_string(lua_State *lua);

static int
_luiapi_get_text_string(lua_State *lua);

static int
_luiapi_get_text_width(lua_State *lua);

static int
_luiapi_get_text_height(lua_State *lua);

static int
_luiapi_set_text_font(lua_State *lua);

static int
_luiapi_get_text_font(lua_State *lua);

static int
_luiapi_set_text_scale(lua_State *lua);

static int
_luiapi_set_button_width(lua_State *lua);

static int
_luiapi_set_button_height(lua_State *lua);

static int
_luiapi_get_button_width(lua_State *lua);

static int
_luiapi_get_button_height(lua_State *lua);

static int
_luiapi_set_button_callback(lua_State *lua);

static int
_luiapi_set_button_title(lua_State *lua);

static int
_luiapi_set_texture_clip(lua_State *lua);

static int
_luiapi_set_texture_scale(lua_State *lua);

static int
_luiapi_set_texture_vertical_scale(lua_State *lua);

static int
_luiapi_set_texture_horizontal_scale(lua_State *lua);

static int
_luiapi_set_texture_file(lua_State *lua);

static int
_luiapi_set_text_input_width(lua_State *lua);

static int
_luiapi_set_text_input_height(lua_State *lua);

static int
_luiapi_get_text_input_content(lua_State *lua);

static int
_luiapi_get_text_input_style(lua_State *lua);

static int
_luiapi_set_text_input_submit_callback(lua_State *lua);

static int
_luiapi_clear_text_input(lua_State *lua);

static int
_luiapi_set_active_text_input(lua_State *lua);

static int
_luiapi_is_text_input_active(lua_State *lua);

static int
_luiapi_create_window_style(lua_State *lua);

static int
_luiapi_set_window_style_background_color(lua_State *lua);

static int
_luiapi_set_window_style_title_color(lua_State *lua);

static int
_luiapi_get_fps(lua_State *lua);

static int
_luiapi_create_key_bind(lua_State *lua);

static int
_luiapi_bind_key(lua_State *lua);

static int
_luiapi_logout(lua_State *lua);

static int
_luiapi_exit_game(lua_State *lua);

static int
_luiapi_get_key_bindings(lua_State *lua);

static int
_luiapi_is_shift_down(lua_State *lua);

static int
_luiapi_is_alt_down(lua_State *lua);

static int
_luiapi_is_ctrl_down(lua_State *lua);

static int
_luiapi_grab_keyboard_for_window(lua_State *lua);

static int
_luiapi_ungrab_keyboard_for_window(lua_State *lua);

static int
_luiapi_send_chat_message(lua_State *lua);

static int
_luiapi_get_current_map_id(lua_State *lua);

static int
_luiapi_get_current_map_name(lua_State *lua);

static int
_luiapi_get_current_map_width(lua_State *lua);

static int
_luiapi_get_current_map_height(lua_State *lua);

static int
_luiapi_get_resolution_width(lua_State *lua);

static int
_luiapi_get_resolution_height(lua_State *lua);

static int
_luiapi_get_text_input_style_min_height(lua_State *lua);

static int
_luiapi_get_window_viewport(lua_State *lua);

static int
_luiapi_get_window_style(lua_State *lua);

static int
_luiapi_get_window_style(lua_State *lua);

static int
_luiapi_get_font_height(lua_State *lua);

static int
_luiapi_get_default_font(lua_State *lua);

static int
_luiapi_get_default_text_input_style(lua_State *lua);

static int
_luiapi_set_window_position_relative_to_parent(lua_State *lua);

static int
_luiapi_get_my_entity_handle(lua_State *lua);

static int
_luiapi_entity_type_to_string(lua_State *lua);

static int
_luiapi_get_entity_name(lua_State *lua);

static int
_luiapi_get_my_target(lua_State *lua);

static int
_luiapi_set_my_target(lua_State *lua);

/*-- Window style getters and setters --*/

static int
_luiapi_get_window_style_handle(lua_State *lua);

static int
_luiapi_get_window_style_background_color(lua_State *lua);

static int
_luiapi_get_window_style_title_origin(lua_State *lua);

static int
_luiapi_get_window_style_title_offset(lua_State *lua);

static int
_luiapi_get_window_style_title_scale(lua_State *lua);

static int
_luiapi_get_window_style_title_color(lua_State *lua);

static int
_luiapi_get_window_style_title_font(lua_State *lua);

static int
_luiapi_get_window_style_border_widths(lua_State *lua);

static int
_luiapi_get_window_style_border_color(lua_State *lua);

static int
_luiapi_get_clicked_entities(lua_State *lua);

static int
_luiapi_get_constant(lua_State *lua);

static int
_luiapi_clear_clicked_entities(lua_State *lua);

static int
_luiapi_target_clicked_entity(lua_State *lua);

static void
_parse_addon_file_callback(void *ctx, const char *opt, const char *val);

static int
_load_addons(void);

static void
_load_addon_directory(const char *dp);

static addon_t *
_get_addon(const char *name);

static lui_window_t *
_get_window(const char *name);

static lui_window_t *
_create_window(const char *name);

static lui_window_obj_data_t *
_create_window_obj(lui_window_t *win);
/* Create a window object (text, button, etc.) as the given window's child */

static inline lui_window_t *
_get_window_arg(int stack_index);

static inline lui_font_t *
_get_font_arg(int stack_index);

static inline lui_text_t *
_get_text(uint32 id);

static inline lui_button_t *
_get_button(uint32 id);

static inline lui_texture_t *
_get_texture(uint32 id);

static inline lui_text_input_t *
_get_text_input(uint32 id);

static inline gui_win_style_t *
_get_window_style(uint32 id);

static void
_propagate_event_to_window(lui_window_t *win, lui_event_t *event);

static int
_str_to_event_enum(const char *str);

static int
_str_to_gui_origin(const char *str);

static const char *
_gui_origin_to_str(enum gui_origin origin);

static inline void
_post_event(lui_event_t *event);

static tex_t *
_load_texture_file(const char *path);
/* Call this only if you've checked the texture doesn't already exist */

static int
_lua_stack_trace(lua_State *lua);

static void
_create_lua_table_for_hotkey_action(hk_action_t *action);

static int
_is_window_hovered_or_active(lua_State *lua, int *index_val);

static void
_remove_window_from_shown(lui_window_t *win);

static inline void
_update_text_wh(lui_text_t *text);

static inline lui_window_t *
_get_window_of_text(lui_text_t *text);

static inline lui_window_t *
_get_window_of_texture(lui_texture_t *texture);

static inline lui_window_t *
_get_window_of_button(lui_button_t *button);

static inline lui_window_t *
_get_window_of_text_input(lui_text_input_t *ti);

static lui_window_style_t *
_find_window_style(const char *name);

static lui_window_style_t *
_create_window_style(const char *name);

static void
_push_entity_handle(entity_t *entity);

static int
_get_entity_handle_arg(int stack_index, entity_handle_t *ret_handle);

static gui_win_state_style_t *
_get_window_state_style(uint32 style_id, const char *state_name);

static gui_win_state_style_t *
_get_window_state_style_arg(int style_handle_stack_index,
    int state_name_stack_index);

static void
_push_color(uint8 r, uint8 g, uint8 b, uint8 a);

static enum gui_win_state
_str_to_gui_win_state(const char *str);
/* Returns GUI_NUM_WIN_STATES if str does not match any. */

static lui_text_input_style_t *
_create_text_input_style(const char *name,
    gui_text_input_style_t *text_input_style);

static void
_lua_remove(lua_State *lua, int index);

static void
_lua_pop(lua_State *lua, int num);

static void
_lua_settop(lua_State *lua, int top);

int
lui_init(void)
{
    _active_window_index        = -1;
    _hovered_window_index       = -1;
    _keyboard_grab_window_index = -1;
    _lua                        = luaL_newstate();
    if (!_lua)
        return 1;
    /*-- Load Lua libraries --*/
    luaL_Reg libs[] =
    {
        {"_G", luaopen_base},
        {LUA_LOADLIBNAME, luaopen_package},
        {LUA_COLIBNAME, luaopen_coroutine},
        {LUA_TABLIBNAME, luaopen_table},
        {LUA_STRLIBNAME, luaopen_string},
        {LUA_MATHLIBNAME, luaopen_math},
        {LUA_UTF8LIBNAME, luaopen_utf8},
        {LUA_DBLIBNAME, luaopen_debug},
        {NULL, NULL}
    };
    for (luaL_Reg *lib = libs; lib->func; ++lib)
    {
        luaL_requiref(_lua, lib->name, lib->func, 1);
        _lua_pop(_lua, 1);
    }

    obj_pool_init(&_wins.pool, 64, sizeof(lui_window_t));
    darr_reserve(_wins.shown, 64);
    darr_reserve(_events, 256);
    lui_window_obj_table_einit(&_window_objs.table, 512);
    obj_pool_init(&_window_objs.pool, 256, sizeof(lui_window_obj_t));
    _window_objs.running_id = 0;

    /*-- Create the global window table --*/
    lua_pushstring(_lua, _window_table_key);
    lua_newtable(_lua);
    lua_settable(_lua, LUA_REGISTRYINDEX);

    /*-- Create the global button table --*/
    lua_pushstring(_lua, _button_table_key);
    lua_newtable(_lua);
    lua_settable(_lua, LUA_REGISTRYINDEX);

    /*-- Create the global key callback table --*/
    lua_pushstring(_lua, _key_callback_key);
    lua_newtable(_lua);
    lua_settable(_lua, LUA_REGISTRYINDEX);
    _running_key_callback_id = 0;

    /*-- Create the global text input callback table --*/
    lua_pushstring(_lua, _text_input_key);
    lua_newtable(_lua);
    lua_settable(_lua, LUA_REGISTRYINDEX);

    /*-- Textures --*/
    obj_pool_init(&_textures.pool, 64, sizeof(tex_t));
    str_tex_table_init(&_textures.table, 64);
    darr_reserve(_textures.all, 64);

    /*-- Window styles --*/
    darr_reserve(_window_styles, 16);
    /*-- Create default window styles --*/
    lui_window_style_t *win_style = _create_window_style(
        _default_titled_window_style_name);
    gui_win_state_style_t *wss = &win_style->style->states[GUI_WIN_STATE_ACTIVE];
    gui_set_color_array(wss->background_color, 13, 13, 13, 255);
    gui_set_color_array(wss->border.color, 33, 33, 33, 255);
    gui_set_color_array(wss->title_color, 255, 255, 255, 255);
    wss->title_font = as_get_default_font();
    muta_assert(wss->title_font);
    wss->border.widths[GUI_EDGE_TOP]    = 22;
    wss->border.widths[GUI_EDGE_BOTTOM] = 2;
    wss->border.widths[GUI_EDGE_LEFT]   = 2;
    wss->border.widths[GUI_EDGE_RIGHT]  = 2;
    memcpy(&win_style->style->states[GUI_WIN_STATE_HOVERED], wss,
        sizeof(gui_win_state_style_t));
    memcpy(&win_style->style->states[GUI_WIN_STATE_INACTIVE], wss,
        sizeof(gui_win_state_style_t));
    win_style = _create_window_style(_default_untitled_window_style_name);
    *win_style->style = *_find_window_style(_default_titled_window_style_name)->style;
    for (int i = 0; i < 3; ++i)
    {
        win_style->style->states[i].border.widths[GUI_EDGE_TOP] =
            win_style->style->states[i].border.widths[GUI_EDGE_BOTTOM];
        memset(win_style->style->states[i].title_color, 0,
            sizeof(win_style->style->states[i].title_color));
    }

    /*-- Button styles --*/
    _button_styles = 0;
    darr_reserve(_button_styles, 16);
    gui_button_style_t bs = gui_create_button_style();
    gui_set_color_array(bs.states[GUI_BUTTON_STATE_NORMAL].background_color,
        23, 23, 23, 255);
    gui_set_color_array(bs.states[GUI_BUTTON_STATE_HOVERED].background_color,
        13, 43, 43, 255);
    gui_set_color_array(bs.states[GUI_BUTTON_STATE_PRESSED].background_color,
        13, 83, 13, 255);
    gui_set_color_array(bs.states[GUI_BUTTON_STATE_NORMAL].border.color,
        13, 13, 13, 255);
    gui_set_color_array(bs.states[GUI_BUTTON_STATE_HOVERED].border.color,
        33, 33, 33, 255);
    gui_set_color_array(bs.states[GUI_BUTTON_STATE_PRESSED].border.color,
        43, 43, 43, 255);
    gui_set_color_array(bs.states[GUI_BUTTON_STATE_PRESSED].background_color,
        63, 63, 63, 255);
    bs.states[GUI_BUTTON_STATE_NORMAL].border.widths[GUI_EDGE_TOP]      = 2;
    bs.states[GUI_BUTTON_STATE_NORMAL].border.widths[GUI_EDGE_BOTTOM]   = 2;
    bs.states[GUI_BUTTON_STATE_NORMAL].border.widths[GUI_EDGE_LEFT]     = 2;
    bs.states[GUI_BUTTON_STATE_NORMAL].border.widths[GUI_EDGE_RIGHT]    = 2;
    bs.states[GUI_BUTTON_STATE_HOVERED].border.widths[GUI_EDGE_TOP]     = 2;
    bs.states[GUI_BUTTON_STATE_HOVERED].border.widths[GUI_EDGE_BOTTOM]  = 2;
    bs.states[GUI_BUTTON_STATE_HOVERED].border.widths[GUI_EDGE_LEFT]    = 2;
    bs.states[GUI_BUTTON_STATE_HOVERED].border.widths[GUI_EDGE_RIGHT]   = 2;
    bs.states[GUI_BUTTON_STATE_PRESSED].border.widths[GUI_EDGE_TOP]     = 2;
    bs.states[GUI_BUTTON_STATE_PRESSED].border.widths[GUI_EDGE_BOTTOM]  = 2;
    bs.states[GUI_BUTTON_STATE_PRESSED].border.widths[GUI_EDGE_LEFT]    = 2;
    bs.states[GUI_BUTTON_STATE_PRESSED].border.widths[GUI_EDGE_RIGHT]   = 2;
    darr_push(_button_styles, bs);

    /*-- Text input styles */
    _text_input_styles = 0;
    _create_text_input_style("default", gui_get_default_text_input_style());

    /*-- Fonts --*/
    _fonts = 0;
    uint32 num_fonts = as_num_fonts();
    darr_reserve(_fonts, num_fonts + 1);
    for (uint32 i = 0; i < num_fonts; ++i)
    {
        lui_font_t font = {.data = as_get_font_by_index(i)};
        darr_push(_fonts, font);
    }
    gui_font_t *default_font = as_get_default_font();
#ifdef _MUTA_DEBUG
    bool32 found_default_font = 0;
#endif
    for (uint32 i = 0; i < num_fonts; ++i)
    {
        if (_fonts[i].data != default_font)
            continue;
        _default_font_index = i;
#ifdef _MUTA_DEBUG
        found_default_font = 1;
#endif
        break;
    }
    muta_assert(found_default_font);

    /*-- Register API functions --*/
    lua_register(_lua, "CreateWindow", _luiapi_create_window);
    lua_register(_lua, "GetWindow", _luiapi_get_window);
    lua_register(_lua, "ShowWindow", _luiapi_show_window);
    lua_register(_lua, "HideWindow", _luiapi_hide_window);
    lua_register(_lua, "IsWindowShown", _luiapi_is_window_shown);
    lua_register(_lua, "SetWindowPosition", _luiapi_set_window_position);
    lua_register(_lua, "SetWindowWidth", _luiapi_set_window_width);
    lua_register(_lua, "SetWindowHeight", _luiapi_set_window_height);
    lua_register(_lua, "CreateButton", _luiapi_create_button);
    lua_register(_lua, "ListenToEvent", _luiapi_listen_to_event);
    lua_register(_lua, "UnlistenToEvent", _luiapi_unlisten_to_event);
    lua_register(_lua, "SetEventCallback", _luiapi_set_event_callback);
    lua_register(_lua, "GetWindowPosition", _luiapi_get_window_position);
    lua_register(_lua, "GetWindowWidth", _luiapi_get_window_width);
    lua_register(_lua, "GetWindowHeight", _luiapi_get_window_height);
    lua_register(_lua, "GetWindowViewport", _luiapi_get_window_viewport);
    lua_register(_lua, "GetWindowStyle", _luiapi_get_window_style);
    lua_register(_lua, "GetFontHeight", _luiapi_get_font_height);
    lua_register(_lua, "GetDefaultFont", _luiapi_get_default_font);
    lua_register(_lua, "GetDefaultTextInputStyle",
        _luiapi_get_default_text_input_style);
    lua_register(_lua, "SetWindowPositionRelativeToParent",
        _luiapi_set_window_position_relative_to_parent);
    lua_register(_lua, "GetMyEntityHandle", _luiapi_get_my_entity_handle);
    lua_register(_lua, "EntityTypeToString", _luiapi_entity_type_to_string);
    lua_register(_lua, "GetEntityName", _luiapi_get_entity_name);
    lua_register(_lua, "GetMyTarget", _luiapi_get_my_target);
    lua_register(_lua, "SetMyTarget", _luiapi_set_my_target);
    lua_register(_lua, "SetWindowOrigin", _luiapi_set_window_origin);
    lua_register(_lua, "SetWindowParent", _luiapi_set_window_parent);
    lua_register(_lua, "SetWindowTitle", _luiapi_set_window_title);
    lua_register(_lua, "SetWindowStyle", _luiapi_set_window_style);
    lua_register(_lua, "IsWindowActive", _luiapi_is_window_active);
    lua_register(_lua, "IsWindowHovered", _luiapi_is_window_hovered);
    lua_register(_lua, "CreateText", _luiapi_create_text);
    lua_register(_lua, "SetTextString", _luiapi_set_text_string);
    lua_register(_lua, "GetTextString", _luiapi_get_text_string);
    lua_register(_lua, "GetTextWidth", _luiapi_get_text_width);
    lua_register(_lua, "GetTextHeight", _luiapi_get_text_height);
    lua_register(_lua, "SetTextPosition", _luiapi_set_window_obj_position);
    lua_register(_lua, "SetTextOrigin", _luiapi_set_window_obj_origin);
    lua_register(_lua, "SetTextFont", _luiapi_set_text_font);
    lua_register(_lua, "GetTextFont", _luiapi_get_text_font);
    lua_register(_lua, "SetTextScale", _luiapi_set_text_scale);
    lua_register(_lua, "SetButtonPosition", _luiapi_set_window_obj_position);
    lua_register(_lua, "SetButtonOrigin", _luiapi_set_window_obj_origin);
    lua_register(_lua, "SetButtonWidth", _luiapi_set_button_width);
    lua_register(_lua, "SetButtonHeight", _luiapi_set_button_height);
    lua_register(_lua, "GetButtonWidth", _luiapi_get_button_width);
    lua_register(_lua, "GetButtonHeight", _luiapi_get_button_height);
    lua_register(_lua, "SetButtonCallback", _luiapi_set_button_callback);
    lua_register(_lua, "SetButtonTitle", _luiapi_set_button_title);
    lua_register(_lua, "CreateTexture", _luiapi_create_texture);
    lua_register(_lua, "SetTexturePosition", _luiapi_set_window_obj_position);
    lua_register(_lua, "SetTextureClip", _luiapi_set_texture_clip);
    lua_register(_lua, "SetTextureScale", _luiapi_set_texture_scale);
    lua_register(_lua, "SetTextureVerticalScale",
        _luiapi_set_texture_vertical_scale);
    lua_register(_lua, "SetTextureHorizontalScale",
        _luiapi_set_texture_horizontal_scale);
    lua_register(_lua, "SetTextureOrigin", _luiapi_set_window_obj_origin);
    lua_register(_lua, "SetTextureFile", _luiapi_set_texture_file);
    lua_register(_lua, "CreateTextInput", _luiapi_create_text_input);
    lua_register(_lua, "SetTextInputWidth", _luiapi_set_text_input_width);
    lua_register(_lua, "SetTextInputHeight", _luiapi_set_text_input_height);
    lua_register(_lua, "GetTextInputContent",
        _luiapi_get_text_input_content);
    lua_register(_lua, "GetTextInputStyle", _luiapi_get_text_input_style);
    lua_register(_lua, "SetTextInputOrigin", _luiapi_set_window_obj_origin);
    lua_register(_lua, "SetTextInputSubmitCallback",
        _luiapi_set_text_input_submit_callback);
    lua_register(_lua, "ClearTextInput", _luiapi_clear_text_input);
    lua_register(_lua, "SetActiveTextInput", _luiapi_set_active_text_input);
    lua_register(_lua, "IsTextInputActive", _luiapi_is_text_input_active);
    lua_register(_lua, "CreateWindowStyle", _luiapi_create_window_style);
    lua_register(_lua, "SetWindowStyleBackgroundColor",
        _luiapi_set_window_style_background_color);
    lua_register(_lua, "SetWindowStyleTitleColor",
        _luiapi_set_window_style_title_color);
    lua_register(_lua, "GetFps", _luiapi_get_fps);
    lua_register(_lua, "CreateKeyBind", _luiapi_create_key_bind);
    lua_register(_lua, "BindKey", _luiapi_bind_key);
    lua_register(_lua, "Logout", _luiapi_logout);
    lua_register(_lua, "ExitGame", _luiapi_exit_game);
    lua_register(_lua, "GetKeyBindings", _luiapi_get_key_bindings);
    lua_register(_lua, "IsShiftDown", _luiapi_is_shift_down);
    lua_register(_lua, "IsAltDown", _luiapi_is_alt_down);
    lua_register(_lua, "IsCtrlDown", _luiapi_is_ctrl_down);
    lua_register(_lua, "GrabKeyboardForWindow",
        _luiapi_grab_keyboard_for_window);
    lua_register(_lua, "UngrabKeyboardForWindow",
        _luiapi_ungrab_keyboard_for_window);
    lua_register(_lua, "SendChatMessage", _luiapi_send_chat_message);
    lua_register(_lua, "GetCurrentMapNameId", _luiapi_get_current_map_id);
    lua_register(_lua, "GetCurrentMapNameName",
        _luiapi_get_current_map_name);
    lua_register(_lua, "GetCurrentMapNameWidth",
        _luiapi_get_current_map_width);
    lua_register(_lua, "GetCurrentMapNameHeight",
        _luiapi_get_current_map_height);
    lua_register(_lua, "GetResolutionWidth", _luiapi_get_resolution_width);
    lua_register(_lua, "GetResolutionHeight", _luiapi_get_resolution_height);
    lua_register(_lua, "GetTextInputStyleMinHeight",
        _luiapi_get_text_input_style_min_height);
    lua_register(_lua, "GetWindowStyleHandle",
        _luiapi_get_window_style_handle);
    lua_register(_lua, "GetWindowStyleBackgroundColor",
        _luiapi_get_window_style_background_color);
    lua_register(_lua, "GetWindowStyleTitleOrigin",
        _luiapi_get_window_style_title_origin);
    lua_register(_lua, "GetWindowStyleTitleOffset",
        _luiapi_get_window_style_title_offset);
    lua_register(_lua, "GetWindowStyleTitleScale",
        _luiapi_get_window_style_title_scale);
    lua_register(_lua, "GetWindowStyleTitleColor",
        _luiapi_get_window_style_title_color);
    lua_register(_lua, "GetWindowStyleTitleFont",
        _luiapi_get_window_style_title_font);
    lua_register(_lua, "GetWindowStyleBorderWidths",
        _luiapi_get_window_style_border_widths);
    lua_register(_lua, "GetWindowStyleBorderColor",
        _luiapi_get_window_style_border_color);
    lua_register(_lua, "GetClickedEntities", _luiapi_get_clicked_entities);
    lua_register(_lua, "GetConstant", _luiapi_get_constant);
    lua_register(_lua, "ClearClickedEntities", _luiapi_clear_clicked_entities);
    lua_register(_lua, "TargetClickedEntity", _luiapi_target_clicked_entity);
    /* Create the key bindings table */
    uint32      num_hka;
    hk_action_t **hka = hk_get_actions(&num_hka);
    lua_pushstring(_lua, _hotkey_table_key);
    lua_newtable(_lua);
    for (uint32 i = 0; i < num_hka; ++i)
    {
        lua_pushstring(_lua, hka[i]->name);
        _create_lua_table_for_hotkey_action(hka[i]);
        lua_settable(_lua, -3);
    }
    lua_settable(_lua, LUA_REGISTRYINDEX);
    /* Load the default UI before any addons */
    _load_addon_directory("ui/default");
    _load_addons();
    _in_use = 1;
    return 0;
}

void
lui_destroy(void)
{
    if (!_in_use)
        return;
    if (_lua)
    {
        lua_close(_lua);
        _lua = 0;
    }
    for (uint32 i = 0; i < darr_num(_wins.all); ++i)
    {
        lui_window_t *win = _wins.all[i];
        for (uint32 j = 0; j < darr_num(win->objs); ++j)
        {
            switch (win->objs[j].type)
            {
            case LUI_WINDOW_OBJ_TEXT:
                dstr_free(&win->objs[j].data.text.string);
                break;
            case LUI_WINDOW_OBJ_BUTTON:
                dstr_free(&win->objs[j].data.button.name);
                break;
            case LUI_WINDOW_OBJ_TEXT_INPUT:
                free(win->objs[j].data.text_input.buf);
                dstr_free(&win->objs[j].data.text_input.title);
                break;
            }
        }
        darr_free(win->objs);
        darr_free(win->children);
        dstr_free(&win->title);
    }
    darr_free(_wins.shown);
    darr_free(_wins.all);
    obj_pool_destroy(&_wins.pool);
    for (uint32 i = 0; i < darr_num(_addons); ++i)
    {
        dstr_free(&_addons[i].name);
        dstr_free(&_addons[i].lua_file_path);
    }
    darr_free(_addons);
    darr_free(_events);
    for (int i = 0; i < LUI_NUM_EVENTS; ++i)
        darr_free(_listener_lists[i].windows);
    memset(_listener_lists, 0, sizeof(_listener_lists));
    /*-- Destroy window objects --*/
    lui_window_obj_table_destroy(&_window_objs.table);
    obj_pool_destroy(&_window_objs.pool);
    /*-- Destroy textures --*/
    for (uint32 i = 0; i < darr_num(_textures.all); ++i)
        tex_free(_textures.all[i]);
    darr_free(_textures.all);
    str_tex_table_destroy(&_textures.table);
    obj_pool_destroy(&_textures.pool);
    darr_free(_fonts);
    uint32 num_text_input_styles = darr_num(_text_input_styles);
    for (uint32 i = 0; i < num_text_input_styles; ++i)
    {
        dstr_free(&_text_input_styles[i].name);
        free(_text_input_styles[i].style);
    }
    darr_free(_text_input_styles);
    uint32 num_window_styles = darr_num(_window_styles);
    for (uint32 i = 0; i < num_window_styles; ++i)
    {
        dstr_free(&_window_styles[i].name);
        free(_window_styles[i].style);
    }
    darr_free(_window_styles);
    _in_use = 0;
}

void
lui_reload(void)
{
    lui_destroy();
    lui_init();
}

static void
_update_window(lui_window_t *win)
{
    if (!(win->flags & LUI_WINDOW_FLAG_SHOWN))
        return;
    gui_origin(win->origin);
    if (win->style_index != 0xFFFFFFFF)
    {
        gui_win_style(_window_styles[win->style_index].style);
        gui_begin_win(win->title, win->x, win->y, win->w, win->h, 0);
    } else
        gui_begin_empty_win(win->title, win->x, win->y, win->w, win->h, 0);
    if (gui_is_window_active())
        _active_window_index = win->index;
    if (gui_is_window_hovered())
        _active_window_index = win->index;
    for (uint32 j = 0; j < darr_num(win->objs); ++j)
    {
        lui_window_obj_data_t *d = &win->objs[j];
        switch (d->type)
        {
        case LUI_WINDOW_OBJ_TEXT:
        {
            lui_text_t *t = &d->data.text;
            gui_font(_fonts[t->font_index].data);
            gui_origin(d->origin);
            gui_text_s(t->string, t->wrap, d->x, d->y, t->scale);
        }
            break;
        case LUI_WINDOW_OBJ_BUTTON:
        {
            lui_button_t *b = &d->data.button;
            gui_button_style(&_button_styles[b->style_index]);
            gui_origin(d->origin);
            if (!gui_button(b->name, d->x, d->y, b->w, b->h, 0))
                break;
            int top = lua_gettop(_lua);
            lua_pushstring(_lua, _button_table_key);
            lua_gettable(_lua, LUA_REGISTRYINDEX);
            lua_assert(!lua_isnil(_lua, -1));
            lua_pushnumber(_lua, (lua_Number)win->objs[j].obj->id);
            lua_gettable(_lua, -2);
            lua_assert(!lua_isnil(_lua, -1));
            if (lua_isfunction(_lua, -1))
                lua_pcall(_lua, 0, 0, 0);
            else
                DEBUG_PRINTFF("button '%s' function was nil.\n", b->name);
            _lua_settop(_lua, top);
        }
            break;
        case LUI_WINDOW_OBJ_TEXTURE:
        {
            lui_texture_t *t = &d->data.texture;
            gui_origin(d->origin);
            gui_texture_s(t->tex, t->clip, d->x, d->y, t->scale_x, t->scale_y);
        }
            break;
        case LUI_WINDOW_OBJ_TEXT_INPUT:
        {
            lui_text_input_t *ti = &d->data.text_input;
            gui_origin(d->origin);
            gui_text_input(ti->title, ti->buf, ti->buf_size, d->x, d->y, ti->w,
                ti->h, 0);
            if (!gui_text_input_enter_pressed())
                break;
            int top = lua_gettop(_lua);
            lua_pushstring(_lua, _text_input_key);
            lua_gettable(_lua, LUA_REGISTRYINDEX);
            lua_pushnumber(_lua, (lua_Number)d->obj->id);
            lua_gettable(_lua, -2);
            lua_pushstring(_lua, _text_input_submit_key);
            lua_gettable(_lua, -2);
            if (lua_isfunction(_lua, -1))
            {
                lua_pushnumber(_lua, (lua_Number)d->obj->id);
                muta_assert(lua_isfunction(_lua, -2));
                lua_pcall(_lua, 1, 0, 0);
            }
            _lua_settop(_lua, top);
            gui_set_active_text_input(0);
            /* Flag so that the key up event will not trigger */
            gs_flag_special_key_executed(GS_SPECIAL_KEY_ENTER);
        }
            break;
        }
    }
    for (uint32 i = 0; i < darr_num(win->children); ++i)
        _update_window(_wins.all[win->children[i]]);
    gui_end_win();
}

void
lui_update(double dt)
{
    /*-- Propagate events --*/
    lui_event_t update_event =
    {
        .type           = LUI_EVENT_UPDATE,
        .update.delta   = dt
    };
    _post_event(&update_event);
    lui_event_t *evs        = _events;
    uint32      num_events  = darr_num(evs);
    size_t stack_offset = core_stack_push(num_events * sizeof(lui_event_t));
    lui_event_t *tmp_evs = core_get_stack_at(stack_offset);
    memcpy(tmp_evs, evs, num_events * sizeof(*evs));
    darr_clear(_events);
    for (uint32 i = 0; i < num_events; ++i)
    {
        tmp_evs = core_get_stack_at(stack_offset);
        switch (tmp_evs[i].type)
        {
        case LUI_EVENT_KEY_CALLBACK:
        {
            int top = lua_gettop(_lua);
            lua_pushstring(_lua, _key_callback_key);
            lua_gettable(_lua, LUA_REGISTRYINDEX);
            lua_pushinteger(_lua, (lua_Integer)tmp_evs[i].key_callback.callback_id);
            lua_gettable(_lua, -2);
            if (lua_isfunction(_lua, -1))
                lua_pcall(_lua, 0, 0, 0);
            _lua_settop(_lua, top);
            continue; /* Skip propagating */
        }
            break;
        case LUI_EVENT_KEY_BIND_CHANGED:
        {
            /* Register the key with the global hotkey table in the Lua
             * instance. Propagate the event later to listening windows. */
            lui_key_bind_changed_event_t *ed = &tmp_evs[i].key_bind_changed;
            lua_pushstring(_lua, _hotkey_table_key);
            lua_gettable(_lua, LUA_REGISTRYINDEX);
            lua_pushstring(_lua, ed->action->name);
            _create_lua_table_for_hotkey_action(ed->action);
            lua_settable(_lua, -3);
        }
            break;
        case LUI_EVENT_NEW_HOTKEY_ACTION:
        {
            /* Register the key with the global hotkey table in the Lua
             * instance. Propagate the event later to listening windows. */
            lui_new_hotkey_action_event_t *ed = &tmp_evs[i].new_hotkey_action;
            lua_pushstring(_lua, _hotkey_table_key);
            lua_gettable(_lua, LUA_REGISTRYINDEX);
            lua_pushstring(_lua, ed->action->name);
            _create_lua_table_for_hotkey_action(ed->action);
            lua_settable(_lua, -3);
        }
            break;
        case LUI_EVENT_KEY_UP:
            if (streq(tmp_evs[i].key.key_combo_name, "Escape"))
                gui_set_active_text_input(0);
            break;
        }
        muta_assert(tmp_evs[i].type < LUI_NUM_EVENTS);
        uint32  *windows        = _listener_lists[tmp_evs[i].type].windows;
        uint32  num_windows     = darr_num(windows);
        size_t  stack_offset2   = core_stack_push(num_windows * sizeof(uint32));
        uint32  *tmp_windows    = core_get_stack_at(stack_offset2);
        memcpy(tmp_windows, windows, num_windows * sizeof(*windows));
        for (uint32 j = 0; j < num_windows; ++j)
            _propagate_event_to_window(_wins.all[tmp_windows[j]], &tmp_evs[i]);
        core_stack_pop(stack_offset2);
    }
    core_stack_pop(stack_offset);
    lui_window_t    **shown   = _wins.shown;
    uint32          num_shown = darr_num(shown);
    /*-- Actual gui.h api calls --*/
    for (uint32 i = 0; i < num_shown; ++i)
    {
        /* TODO: handle case where window parent changes */
        _update_window(shown[i]);
    }
}

void
lui_post_event(lui_event_t *event)
{
    if (!_in_use)
        return;
    muta_assert(event->type < LUI_NUM_EVENTS);
    darr_push(_events, *event);
}

bool32
lui_is_grabbing_keyboard(void)
{
    return _keyboard_grab_window_index != -1 || gui_is_any_text_input_active();
}

static int
_luiapi_get_window(lua_State *lua)
{
    lui_window_t *win = _get_window(lua_tostring(lua, 1));
    if (win)
        lua_pushnumber(lua, (lua_Number)win->index);
    else
        lua_pushnil(lua);
    return 1;
}

static int
_luiapi_create_window(lua_State *lua)
{
    if (!lua_isstring(lua, -1))
        goto fail;
    const char *name = lua_tostring(lua, -1);
    lui_window_t *win = _create_window(name);
    if (!win)
        goto fail;
    lua_pushstring(lua, _window_table_key);
    lua_gettable(lua, LUA_REGISTRYINDEX);
    lua_pushvalue(lua, -2);
    lua_newtable(lua);
    lua_settable(lua, -3);
    lua_pushinteger(_lua, (lua_Integer)win->index);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_show_window(lua_State *lua)
{
    if (!lua_isinteger(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-1);
    if (!win || (win->flags & LUI_WINDOW_FLAG_SHOWN))
        return 0;
    win->flags |= LUI_WINDOW_FLAG_SHOWN;
    if (win->parent == 0xFFFFFFFF)
        darr_push(_wins.shown, win);
    return 0;
}

static int
_luiapi_hide_window(lua_State *lua)
{
    if (!lua_isinteger(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-1);
    if (!win || !(win->flags & LUI_WINDOW_FLAG_SHOWN))
        return 0;
    if (win->parent == 0xFFFFFFFF)
        _remove_window_from_shown(win);
    win->flags &= ~LUI_WINDOW_FLAG_SHOWN;
    return 0;
}

static int
_luiapi_is_window_shown(lua_State *lua)
{
    int v = 0;
    if (!lua_isinteger(lua, -1))
        goto out;
    lui_window_t *win = _get_window_arg(-1);
    if (!win)
        goto out;
    v = (win->flags & LUI_WINDOW_FLAG_SHOWN) ? 1 : 0;
    out:
        lua_pushboolean(lua, v);
        return 1;
}

static int
_luiapi_set_window_position(lua_State *lua)
{
    int success = 0;
    if (!lua_isnumber(lua, -1) || !lua_isnumber(lua, -2) ||
        !lua_isnumber(lua, -3))
        goto out;
    lui_window_t *win = _get_window_arg(-3);
    if (!win)
        goto out;
    win->x = (int)lua_tonumber(lua, -2);
    win->y = (int)lua_tonumber(lua, -1);
    success = 1;
    out:
        lua_pushboolean(lua, success);
        return 1;
}

static int
_luiapi_set_window_width(lua_State *lua)
{
    if (!lua_isnumber(lua, -1) || !lua_isnumber(lua, -2))
        return 0;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        return 0;
    win->w = (int)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_window_height(lua_State *lua)
{
    if (!lua_isnumber(lua, -1) || !lua_isnumber(lua, -2))
        return 0;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        return 0;
    win->h = (int)lua_tonumber(lua, 2);
    return 0;
}

static int
_luiapi_set_window_origin(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        return 0;
    int origin = _str_to_gui_origin(lua_tostring(lua, -1));
    if (origin != GUI_NUM_ORIGINS)
        win->origin = origin;
    return 0;
}

static int
_luiapi_set_window_parent(lua_State *lua)
{
    if (!lua_isinteger(lua, -2))
        return 0;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        return 0;
    if (lua_isinteger(lua, -1))
    {
        lui_window_t *p = _get_window_arg(-1);
        if (!p)
            return 0;
        /* Check of already a child of the parent */
        uint32 index        = win->index;
        uint32 *children    = p->children;
        uint32 num_children = darr_num(children);
        /* Check if already child of this parent. */
        for (uint32 i = 0; i < num_children; ++i)
            if (p->children[i] == index)
                return 0;
        darr_push(children, index);
        p->children = children;
        /* Remove from exiting parent */
        uint32 old_parent_index = win->parent;
        if (old_parent_index != 0xFFFFFFFF)
        {
            lui_window_t    *ep         = _wins.all[old_parent_index];
            uint32          child_index = 0xFFFFFFFF;
            children        = ep->children;
            num_children    = darr_num(children);
            for (uint32 i = 0; i < num_children; ++i)
            {
                if (children[i] != index)
                    continue;
                child_index = i;
                break;
            }
            muta_assert(child_index != 0xFFFFFFFF);
            darr_erase(children, child_index);
            ep->children = children;
        } else if (win->flags & LUI_WINDOW_FLAG_SHOWN) /* Remove from shown */
            _remove_window_from_shown(win);
        win->parent = p->index;
    } else
    if (lua_isnil(lua, -1) && win->parent != 0xFFFFFFFF) /* Rem from parent */
    {
        lui_window_t    *p              = _wins.all[win->parent];
        uint32          index           = win->index;
        uint32          *children       = p->children;
        uint32          num_children    = darr_num(children);
        for (uint32 i = 0; i < num_children; ++i)
        {
            if (children[i] != index)
                continue;
            darr_erase(children, index);
            p->children = children;
            p->parent   = 0xFFFFFFFF;
            return 0;
        }
        if (win->flags & LUI_WINDOW_FLAG_SHOWN)
            darr_push(_wins.shown, win);
    }
    return 0;
}

static int
_luiapi_set_window_title(lua_State *lua)
{
    if (!lua_isnumber(lua, -2))
        return 0;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        return 0;
    const char *title;
    if (lua_isstring(lua, -1))
        title = lua_tostring(lua, -1);
    else if (lua_isnil(lua, -1))
        title = 0;
    else
        return 0;
    dstr_set(&win->title, title);
    return 0;
}

static int
_luiapi_set_window_style(lua_State *lua)
{
    if (!lua_isinteger(lua, -2) ||
        (!lua_isstring(lua, -1) && !lua_isnil(lua, -1)))
    {
        lua_pushboolean(lua, 0);
        return 1;
    }
    lui_window_t *window = _get_window_arg(-2);
    if (!window)
    {
        lua_pushboolean(lua, 0);
        return 1;
    }
    if (lua_isnil(lua, -1))
    {
        window->style_index = 0xFFFFFFFF;
        lua_pushboolean(lua, 1);
        return 1;
    }
    lui_window_style_t *style = _find_window_style(lua_tostring(lua, -1));
    if (!style)
    {
        lua_pushboolean(lua, 0);
        return 1;
    }
    window->style_index = (uint32)(style - _window_styles);
    lua_pushboolean(lua, 1);
    return 1;
}

static int
_luiapi_is_window_active(lua_State *lua)
    {return _is_window_hovered_or_active(lua, &_active_window_index);}

static int
_luiapi_is_window_hovered(lua_State *lua)
    {return _is_window_hovered_or_active(lua, &_hovered_window_index);}

static int
_luiapi_create_button(lua_State *lua)
{
    if (!lua_isinteger(lua, -2))
        return 0;
    if (!lua_isnil(lua, -1) && !lua_isstring(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        return 0;
    const char *name = lua_tostring(lua, -1);
    if (!name)
        return 0;
    lui_window_obj_data_t *d = _create_window_obj(win);
    d->type                     = LUI_WINDOW_OBJ_BUTTON;
    d->data.button.name         = 0;
    dstr_setf(&d->data.button.name, "%s##%s%u", name, _button_id_str,
        d->obj->id);
    d->data.button.style_index  = 0;
    d->obj->type                = LUI_WINDOW_OBJ_BUTTON;
    lua_pushstring(lua, _button_table_key);
    lua_gettable(lua, LUA_REGISTRYINDEX);
    lua_pushinteger(lua, (lua_Integer)d->obj->id);
    lua_pushnil(lua);
    lua_settable(lua, -3);
    lua_pushinteger(lua, (lua_Integer)d->obj->id);
    return 1;
}

static int
_luiapi_create_text(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        goto fail;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        goto fail;
    lui_window_obj_data_t *d = _create_window_obj(win);
    d->type                 = LUI_WINDOW_OBJ_TEXT;
    d->data.text.string     = dstr_create(lua_tostring(lua, -1));
    d->data.text.font_index = _default_font_index;
    d->data.text.scale      = 1.f;
    d->obj->type            = LUI_WINDOW_OBJ_TEXT;
    _update_text_wh(&d->data.text);
    lua_pushnumber(lua, d->obj->id);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_create_texture(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        goto fail;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        goto fail;
    const char  *path       = lua_tostring(lua, -1);
    tex_t       *tex        = 0;
    tex_t       **existing  = str_tex_table_find(&_textures.table, path);
    if (existing)
        tex = *existing;
    else if (!(tex = _load_texture_file(path)))
        goto fail;
    lui_window_obj_data_t *d = _create_window_obj(win);
    d->type                 = LUI_WINDOW_OBJ_TEXTURE;
    d->data.texture.tex     = tex;
    d->data.texture.scale_x = 1.f;
    d->data.texture.scale_y = 1.f;
    d->data.texture.clip[0] = 0.f;
    d->data.texture.clip[1] = 0.f;
    d->data.texture.clip[2] = (float)tex->w;
    d->data.texture.clip[3] = (float)tex->h;
    d->obj->type            = LUI_WINDOW_OBJ_TEXTURE;
    lua_pushnumber(lua, (lua_Number)d->obj->id);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_create_text_input(lua_State *lua)
{
    if (!lua_isnumber(lua, -3) || !lua_isstring(lua, -2) ||
        !lua_isnumber(lua, -1))
        goto fail;
    lui_window_t *win = _get_window_arg(-3);
    if (!win)
        goto fail;
    lua_Number sizef = lua_tonumber(lua, -1);
    if (sizef < 1.f)
        return 0;
    uint32                  size    = (uint32)sizef + 1;
    lui_window_obj_data_t   *d      = _create_window_obj(win);
    dstr_setf(&d->data.text_input.title, "%s##%s%u", lua_tostring(lua, -2),
        _button_id_str, d->obj->id);
    d->type                         = LUI_WINDOW_OBJ_TEXT_INPUT;
    d->obj->type                    = LUI_WINDOW_OBJ_TEXT_INPUT;
    d->data.text_input.buf          = emalloc(size);
    d->data.text_input.buf_size     = size;
    d->data.text_input.buf[0]       = 0;
    lua_pushstring(lua, _text_input_key);
    lua_gettable(lua, LUA_REGISTRYINDEX);
    muta_assert(!lua_isnil(lua, -1));
    lua_pushinteger(lua, (lua_Integer)d->obj->id);
    lua_newtable(lua);
    lua_settable(lua, -3);
    lua_pushinteger(lua, (lua_Integer)d->obj->id);
    return 1;
    fail:
        muta_assert(0);
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_get_window_position(lua_State *lua)
{
    if (!lua_isnumber(lua, -1))
        goto fail;
    lui_window_t *win = _get_window_arg(-1);
    if (!win)
        goto fail;
    lua_newtable(lua);
    lua_pushstring(lua, "x");
    lua_pushnumber(lua, (lua_Number)win->x);
    lua_settable(lua, -3);
    lua_pushstring(lua, "y");
    lua_pushnumber(lua, (lua_Number)win->y);
    lua_settable(lua, -3);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_get_window_width(lua_State *lua)
{
    if (!lua_isinteger(lua, -1))
        goto fail;
    lui_window_t *win = _get_window_arg(-1);
    if (!win)
        goto fail;
    lua_pushnumber(lua, (lua_Number)win->w);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_get_window_height(lua_State *lua)
{
    if (!lua_isinteger(lua, -1))
        goto fail;
    lui_window_t *win = _get_window_arg(-1);
    if (!win)
        goto fail;
    lua_pushnumber(lua, (lua_Number)win->h);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_set_window_obj_origin(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        return 0;
    int origin = _str_to_gui_origin(lua_tostring(lua, -1));
    if (origin == GUI_NUM_ORIGINS)
        return 0;
    lui_window_obj_t **o = _get_window_obj((uint32)lua_tonumber(lua, -2));
    if (o)
        _wins.all[(*o)->window]->objs[(*o)->index_in_window].origin = origin;
    return 0;
}

static int
_luiapi_set_window_obj_position(lua_State *lua)
{
    if (!lua_isnumber(lua, -1) || !lua_isnumber(lua, -2) ||
        !lua_isnumber(lua, -3))
        return 0;
    uint32 id = (uint32)lua_tonumber(lua, -3);
    lui_window_obj_data_t *d = _get_window_obj_data(id);
    if (!d)
        return 0;
    d->x = (int)lua_tonumber(lua, -2);
    d->y = (int)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_text_string(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        return 0;
    lui_text_t *text = _get_text((uint32)lua_tonumber(lua, -2));
    if (!text)
        return 0;
    dstr_set(&text->string, lua_tostring(lua, -1));
    _update_text_wh(text);
    return 0;
}

static int
_luiapi_get_text_string(lua_State *lua)
{
    if (!lua_isnumber(lua, -1))
        goto out;
    lui_text_t *text = _get_text((uint32)lua_tonumber(lua, -1));
    if (!text)
        goto out;
    if (!text->string)
        goto out;
    lua_pushstring(lua, text->string);
    return 1;
    out:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_get_text_width(lua_State *lua)
{
    int w = 0;
    if (!lua_isnumber(lua, -1))
        goto out;
    lui_text_t *text = _get_text((uint32)lua_tonumber(lua, -1));
    if (!text)
        goto out;
    w = text->w;
    out:
        lua_pushnumber(lua, w);
        return 1;
}

static int
_luiapi_get_text_height(lua_State *lua)
{
    int h = 0;
    if (!lua_isnumber(lua, -1))
        goto out;
    lui_text_t *text = _get_text((uint32)lua_tonumber(lua, -1));
    if (!text)
        goto out;
    h = text->h;
    out:
        lua_pushnumber(lua, h);
        return 1;
}

static int
_luiapi_set_text_font(lua_State *lua)
{
    if (!lua_isinteger(lua, -2) || !lua_isinteger(lua, -1))
        return 0;
    lui_font_t *font = _get_font_arg(-1);
    if (!font)
        return 0;
    lua_Integer text_id = lua_tointeger(lua, -2);
    if (text_id < 0 || text_id > LUI_MAX_OBJECT_ID)
        return 0;
    lui_text_t *text = _get_text((uint32)text_id);
    if (!text)
        return 0;
    text->font_index = (uint32)(font - _fonts);
    _update_text_wh(text);
    return 0;
}

static int
_luiapi_get_text_font(lua_State *lua)
{
    if (!lua_isinteger(lua, -1))
        goto fail;
    lua_Integer text_id = lua_tointeger(lua, -1);
    if (text_id < 0 || text_id >= LUI_MAX_OBJECT_ID)
        goto fail;
    lui_text_t *text = _get_text((uint32)text_id);
    if (!text)
        goto fail;
    lua_pushinteger(lua, text->font_index);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_set_text_scale(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_text_t *text = _get_text((uint32)lua_tonumber(lua, -2));
    if (!text)
        return 0;
    text->scale = (float)lua_tonumber(lua, -1);
    _update_text_wh(text);
    return 0;
}

static int
_luiapi_set_button_width(lua_State *lua)
{
    if (!lua_isinteger(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_button_t *button = _get_button((uint32)lua_tointeger(lua, -2));
    if (!button)
        return 0;
    button->w = (int)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_button_height(lua_State *lua)
{
    if (!lua_isinteger(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_button_t *button = _get_button((uint32)lua_tointeger(lua, -2));
    if (!button)
        return 0;
    button->h = (int)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_get_button_width(lua_State *lua)
{
    if (!lua_isinteger(lua, -1))
        goto fail;
    lui_button_t *button = _get_button((uint32)lua_tointeger(lua, -1));
    if (!button)
        goto fail;
    lua_pushnumber(lua, button->w);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_get_button_height(lua_State *lua)
{
    if (!lua_isinteger(lua, -1))
        goto fail;
    lui_button_t *button = _get_button((uint32)lua_tointeger(lua, -1));
    if (!button)
        goto fail;
    lua_pushnumber(lua, button->h);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_set_button_callback(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isfunction(lua, -1))
        return 0;
    uint32 id = (uint32)lua_tonumber(lua, -2);
    lui_button_t *b = _get_button(id);
    if (!b)
        return 0;
    lua_pushstring(lua, _button_table_key);
    lua_gettable(lua, LUA_REGISTRYINDEX);
    muta_assert(!lua_isnil(lua, -1));
    lua_pushnumber(lua, (lua_Number)id);
    muta_assert(lua_isfunction(lua, -3));
    lua_pushvalue(lua, -3);
    lua_settable(lua, -3);
    return 0;
}

static int
_luiapi_set_button_title(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        return 0;
    uint32 id = (uint32)lua_tonumber(lua, -2);
    lui_button_t *b = _get_button(id);
    if (!b)
        return 0;
    dstr_setf(&b->name, "%s##%s%u", lua_tostring(lua, -1), _button_id_str, id);
    DEBUG_PRINTFF("setting button title to %s...\n", b->name);
    return 0;
}

static int
_luiapi_set_texture_clip(lua_State *lua)
{
    if (!lua_isnumber(lua, -5) || !lua_isnumber(lua, -4) ||
        !lua_isnumber(lua, -3) || !lua_isnumber(lua, -2) ||
        !lua_isnumber(lua, -1))
        return 0;
    lui_texture_t *t = _get_texture((uint32)lua_tonumber(lua, -5));
    if (!t)
        return 0;
    t->clip[0] = (float)lua_tonumber(lua, -4);
    t->clip[1] = (float)lua_tonumber(lua, -3);
    t->clip[2] = (float)lua_tonumber(lua, -2);
    t->clip[3] = (float)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_texture_scale(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_texture_t *t = _get_texture((uint32)lua_tonumber(lua, -2));
    if (!t)
        return 0;
    t->scale_x = (float)lua_tonumber(lua, -1);
    t->scale_y = (float)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_texture_vertical_scale(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_texture_t *t = _get_texture((uint32)lua_tonumber(lua, -2));
    if (!t)
        return 0;
    t->scale_x = (float)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_texture_horizontal_scale(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_texture_t *t = _get_texture((uint32)lua_tonumber(lua, -2));
    if (!t)
        return 0;
    t->scale_y = (float)lua_tonumber(lua, -1);
    return 0;

}

static int
_luiapi_set_texture_file(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        return 0;
    lui_texture_t *t = _get_texture((uint32)lua_tonumber(lua, -2));
    if (!t)
        return 0;
    tex_t **existing = str_tex_table_find(&_textures.table,
        lua_tostring(lua, -1));
    if (existing)
        t->tex = *existing;
    else
        t->tex = _load_texture_file(lua_tostring(lua, -1));
    t->clip[0] = 0.f;
    t->clip[1] = 0.f;
    if (t->tex)
    {
        t->clip[2] = (float)t->tex->w;
        t->clip[3] = (float)t->tex->h;
    }
    return 0;
}

static int
_luiapi_set_text_input_width(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_text_input_t *ti = _get_text_input((uint32)lua_tonumber(lua, -2));
    if (ti)
        ti->w = (int)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_text_input_height(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_text_input_t *ti = _get_text_input((uint32)lua_tonumber(lua, -2));
    if (ti)
        ti->h = (int)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_get_text_input_content(lua_State *lua)
{
    if (!lua_isnumber(lua, -1))
        goto fail;
    lui_text_input_t *ti = _get_text_input((uint32)lua_tonumber(lua, -1));
    if (!ti)
        goto fail;
    lua_pushstring(lua, ti->buf);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_get_text_input_style(lua_State *lua)
{
    if (!lua_isinteger(lua, -1))
        goto fail;
    lua_Integer id = lua_tointeger(lua, -1);
    if (id < 0 || id >= LUI_MAX_OBJECT_ID)
        goto fail;
    lui_text_input_t *text_input = _get_text_input((uint32)id);
    if (!text_input)
        goto fail;
    lua_pushinteger(lua, (lua_Integer)text_input->style_index);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_set_text_input_submit_callback(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) ||
        (!lua_isfunction(lua, -1) && !lua_isnil(lua, -1)))
        return 0;
    uint32 id = (uint32)lua_tonumber(lua, -2);
    lui_text_input_t *ti = _get_text_input(id);
    if (!ti)
        return 0;
    lua_pushstring(lua, _text_input_key);
    lua_gettable(lua, LUA_REGISTRYINDEX);
    lua_pushnumber(lua, (lua_Number)id);
    lua_gettable(lua, -2);
    lua_pushstring(lua, _text_input_submit_key);
    lua_pushvalue(lua, -4);
    lua_settable(lua, -3);
    _lua_pop(lua, 2);
    return 0;
}

static int
_luiapi_clear_text_input(lua_State *lua)
{
    if (!lua_isnumber(lua, -1))
        return 0;
    lui_text_input_t *ti = _get_text_input((uint32)lua_tonumber(lua, -1));
    if (!ti)
        return 0;
    if (ti->buf)
        ti->buf[0] = 0;
    return 0;
}

static int
_luiapi_set_active_text_input(lua_State *lua)
{
    if (lua_isnumber(lua, -1))
    {
        lui_text_input_t *ti = _get_text_input((uint32)lua_tonumber(lua, -1));
        if (!ti)
            return 0;
        gui_set_active_win(_get_window_of_text_input(ti)->title);
        gui_set_active_text_input(ti->title);
    } else if (lua_isnil(lua, -1))
        gui_set_active_text_input(0);
    return 0;
}

static int
_luiapi_is_text_input_active(lua_State *lua)
{
    int val = 0;
    if (!lua_isnumber(lua, -1))
        goto out;
    lui_text_input_t *ti = _get_text_input((uint32)lua_tonumber(lua, -1));
    if (!ti)
        goto out;
    return gui_is_text_input_active(ti->title);
    out:
        lua_pushboolean(lua, val);
        return 1;
}

static int
_luiapi_create_window_style(lua_State *lua)
{
    if (!lua_isstring(lua, -1))
    {
        lua_pushnil(lua);
        return 1;
    }
    const char *name = lua_tostring(lua, -1);
    if (_find_window_style(name))
    {
        lua_pushnil(lua);
        return 1;
    }
    lui_window_style_t *style = _create_window_style(name);
    lua_pushstring(lua, style->name);
    return 1;
}

static int
_luiapi_set_window_style_background_color(lua_State *lua)
{
    if (!lua_isnumber(lua, -6) || !lua_isstring(lua, -5))
        return 0;
    for (int i = -4; i < 0; ++i)
        if (!lua_isnumber(lua, i))
            return 0;
    gui_win_state_style_t *ss = _get_window_state_style(
        (uint32)lua_tonumber(lua, -6), lua_tostring(lua, -5));
    if (!ss)
        return 0;
    ss->background_color[0] = (uint8)CLAMP(lua_tonumber(lua, -4) * 255.f, 0, 255.f);
    ss->background_color[1] = (uint8)CLAMP(lua_tonumber(lua, -3) * 255.f, 0, 255.f);
    ss->background_color[2] = (uint8)CLAMP(lua_tonumber(lua, -2) * 255.f, 0, 255.f);
    ss->background_color[3] = (uint8)CLAMP(lua_tonumber(lua, -1) * 255.f, 0, 255.f);
    return 0;
}

static int
_luiapi_set_window_style_title_color(lua_State *lua)
{
    if (!lua_isnumber(lua, -6) || !lua_isstring(lua, -5))
        return 0;
    for (int i = -4; i < 0; ++i)
        if (!lua_isnumber(lua, i))
            return 0;
    gui_win_state_style_t *ss = _get_window_state_style(
        (uint32)lua_tonumber(lua, -6), lua_tostring(lua, -5));
    if (!ss)
        return 0;
    ss->title_color[0] = (uint8)CLAMP(lua_tonumber(lua, -4) * 255.f, 0, 255.f);
    ss->title_color[1] = (uint8)CLAMP(lua_tonumber(lua, -3) * 255.f, 0, 255.f);
    ss->title_color[2] = (uint8)CLAMP(lua_tonumber(lua, -2) * 255.f, 0, 255.f);
    ss->title_color[3] = (uint8)CLAMP(lua_tonumber(lua, -1) * 255.f, 0, 255.f);
    return 0;
}

static int
_luiapi_get_fps(lua_State *lua)
{
    lua_pushnumber(lua, (lua_Number)core_fps());
    return 1;
}

static int
_luiapi_create_key_bind(lua_State *lua)
{
    if (!lua_isstring(lua, -2) || !lua_isfunction(lua, -1))
        return 0;

    lua_pushstring(lua, _key_callback_key);
    lua_gettable(lua, LUA_REGISTRYINDEX);
    muta_assert(!lua_isnil(lua, -1));

    uint32 id = _running_key_callback_id++;
    lua_pushnumber(lua, (lua_Number)id);
    lua_pushvalue(lua, -3);
    muta_assert(lua_isfunction(lua, -1));
    lua_settable(lua, -3);

    hk_action_callback_t callback;
    callback.type   = HK_ACTION_CALLBACK_LUA;
    callback.lua.id = id;

    muta_assert(lua_isstring(lua, -3));
    hk_new_action(lua_tostring(lua, -3), callback, HK_PRESS_UP);

    DEBUG_PRINTFF("created hotkey %s.\n", lua_tostring(lua, -3));
    return 0;
}

static int
_luiapi_bind_key(lua_State *lua)
{
    if (!lua_isstring(lua, -3) || !lua_isstring(lua, -2) ||
        !lua_isinteger(lua, -1))
        return 0;
    const char  *action_name    = lua_tostring(lua, -3);
    const char  *keys           = lua_tostring(lua, -2);
    hk_action_t *action         = hk_get_action_by_name(action_name);
    int         index           = 1;
    int         err;
    if (!action)
    {
        DEBUG_PRINTFF("No action named %s found.\n", action_name);
        err = 1;
        goto err_out;
    }
    if (!lua_isinteger(_lua, -1))
    {
        if (action->keys[0].keycode != -1 && action->keys[1].keycode != -1)
            {err = 2; goto err_out;}
        index = action->keys[0].keycode == -1 ? 1 : 2;
    } else
    if ((index = (int)lua_tointeger(_lua, -1)) < 1 || index > 2)
        {err = 3; goto err_out;}
    int r = hk_bind_key_from_str(action_name, index - 1, keys);
    if (r)
    {
        DEBUG_PRINTFF("failed to bind %s to %s, code %d.\n", action_name, keys,
            r);
        err = 4;
        goto err_out;
    }
    return 0;
    err_out:
        DEBUG_PRINTFF("error %d.\n", err);
        return 0;
}

static int
_luiapi_logout(lua_State *lua)
    {shard_disconnect(); return 0;}

static int
_luiapi_exit_game(lua_State *lua)
    {core_stop(); return 0;}

static int
_luiapi_get_key_bindings(lua_State *lua)
{
    lua_pushstring(_lua, _hotkey_table_key);
    lua_gettable(_lua, LUA_REGISTRYINDEX);
    muta_assert(!lua_isnil(_lua, -1));
    muta_assert(lua_istable(_lua, -1));
    return 1;
}

static int
_luiapi_is_shift_down(lua_State *lua)
{
    int v = core_key_down(CORE_KEY_LSHIFT) ||
        core_key_down(CORE_KEY_RSHIFT);
    lua_pushboolean(lua, v);
    return 1;
}

static int
_luiapi_is_alt_down(lua_State *lua)
{
    int v = core_key_down(CORE_KEY_LALT || CORE_KEY_RALT);
    lua_pushboolean(lua, v);
    return 1;
}

static int
_luiapi_is_ctrl_down(lua_State *lua)
{
    int v = core_key_down(CORE_KEY_LCTRL) || core_key_down(CORE_KEY_RCTRL);
    lua_pushboolean(lua, v);
    return 1;
}

static int
_luiapi_grab_keyboard_for_window(lua_State *lua)
{
    if (lua_isnil(lua, -1))
    {
        _keyboard_grab_window_index = -1;
        return 0;
    }
    if (!lua_isnumber(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-1);
    if (!win)
        return 0;
    _keyboard_grab_window_index = (int)win->index;
    return 0;
}

static int
_luiapi_ungrab_keyboard_for_window(lua_State *lua)
{
    if (!lua_isnumber(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-1);
    if (!win)
        return 0;
    if (_keyboard_grab_window_index != (int)win->index)
        return 0;
    _keyboard_grab_window_index = -1;
    return 0;
}

static int
_luiapi_send_chat_message(lua_State *lua)
{
    if (!lua_isstring(lua, -1))
        return 0;
    //gs_send_chat_msg(lua_tostring(lua, -1));
    return 0;
}

static int
_luiapi_get_current_map_id(lua_State *lua)
{
    lua_pushinteger(lua, gs_get_world()->map.id);
    return 1;
}

static int
_luiapi_get_current_map_name(lua_State *lua)
{
    lua_pushstring(lua, gs_get_world()->map.file.header.name);
    return 1;
}

static int
_luiapi_get_current_map_width(lua_State *lua)
{
    lua_pushinteger(lua, muta_map_file_tw(&gs_get_world()->map.file));
    return 1;
}

static int
_luiapi_get_current_map_height(lua_State *lua)
{
    lua_pushinteger(lua, muta_map_file_th(&gs_get_world()->map.file));
    return 1;
}


static int
_luiapi_get_resolution_width(lua_State *lua)
{
    lua_pushinteger(lua, core_asset_config.resolution_w);
    return 1;
}

static int
_luiapi_get_resolution_height(lua_State *lua)
{
    lua_pushinteger(lua, core_asset_config.resolution_h);
    return 1;
}

static int
_luiapi_get_text_input_style_min_height(lua_State *lua)
{
    lua_Number min_height = 0;
    if (!lua_isinteger(lua, -1))
        goto out;
    lua_Integer index = lua_tointeger(lua, -1);
    if (index < 0 || index >= darr_num(_text_input_styles))
    {
        muta_assert(0);
        goto out;
    }
    min_height = (lua_Number)gui_get_text_input_style_min_height(
        _text_input_styles[index].style);
    out:
        lua_pushnumber(lua, min_height);
        return 1;
}

static int
_luiapi_get_window_viewport(lua_State *lua)
{
    if (!lua_isinteger(lua, -2) || !lua_isstring(lua, -1))
        goto fail;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        goto fail;
    enum gui_win_state state = _str_to_gui_win_state(lua_tostring(lua, -1));
    if (state == GUI_NUM_WIN_STATES)
        goto fail;
    int                     w, h;
    gui_win_state_style_t   *style;
    if (win->style_index == 0xFFFFFFFF)
        style = 0;
    else
        style = &_window_styles[win->style_index].style->states[state];
    gui_compute_window_viewport_size(style, win->w, win->h, &w, &h);
    lua_createtable(lua, 2, 0);
    lua_pushinteger(lua, w);
    lua_rawseti(lua, -2, 1);
    lua_pushinteger(lua, h);
    lua_rawseti(lua, -2, 2);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_get_window_style(lua_State *lua)
{
    if (!lua_isinteger(lua, -1))
        goto fail;
    lui_window_t *win = _get_window_arg(-1);
    if (!win)
        goto fail;
    if (win->style_index == 0xFFFFFFFF)
    {
        lua_pushnil(lua);
        return 1;
    }
    lua_pushinteger(lua, (lua_Integer)win->style_index);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_get_font_height(lua_State *lua)
{
    int height = 0;
    if (!lua_isinteger(lua, -1))
        goto out;
    lua_Integer index = lua_tointeger(lua, -1);
    if (index < 0 || index >= (lua_Integer)darr_num(_fonts))
        goto out;
    height = _fonts[index].data->height;
    out:
        lua_pushnumber(lua, (lua_Number)height);
        return 1;
}

static int
_luiapi_get_default_font(lua_State *lua)
{
    lua_pushinteger(lua, (lua_Integer)_default_font_index);
    return 1;
}

static int
_luiapi_get_default_text_input_style(lua_State *lua)
{
    lua_pushinteger(lua, 0);
    return 1;
}

static int
_luiapi_set_window_position_relative_to_parent(lua_State *lua)
{
    int success = 0;
    if (!lua_isinteger(lua, -2) || !lua_isboolean(lua, -1))
        goto out;
    lui_window_t *window = _get_window_arg(-2);
    if (!window)
        goto out;
    window->flags |= LUI_WINDOW_FLAG_POSITION_RELATIVE_TO_PARENT;
    success = 1;
    out:
        lua_pushboolean(lua, success);
        return 1;
}

static int
_luiapi_get_my_entity_handle(lua_State *lua)
{
    entity_t *player = gs_get_player_entity();
    _push_entity_handle(player);
    return 1;
}

static int
_luiapi_entity_type_to_string(lua_State *lua)
{
    if (!lua_isinteger(lua, -1))
        goto fail;
    lua_Integer type = lua_tointeger(lua, -1);
    const char *s;
    switch (type)
    {
    case ENTITY_TYPE_PLAYER:
        s = "PLAYER";
        break;
    case ENTITY_TYPE_CREATURE:
        s = "CREATURE";
        break;
    case ENTITY_TYPE_DYNAMIC_OBJECT:
        s = "DYNAMIC OBJECT";
        break;
    default:
        goto fail;
    }
    lua_pushstring(lua, s);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_get_entity_name(lua_State *lua)
{
    if (!lua_istable(lua, -1))
        goto fail;
    lua_pushstring(lua, "type");
    lua_gettable(lua, -2);
    if (!lua_isinteger(lua, -1))
        goto fail;
    int type = (int)lua_tointeger(lua, -1);
    _lua_pop(lua, 1);
    lua_pushstring(lua, "id");
    lua_gettable(lua, -2);
    if (!lua_isinteger(lua, -1))
        goto fail;
    lua_Integer id = lua_tointeger(lua, -1);
    const char *name = 0;
    switch (type)
    {
    case ENTITY_TYPE_PLAYER:
    {
        entity_t *entity = world_get_player(gs_get_world(), (player_runtime_id_t)id);
        if (entity)
            name = entity_get_type_data(entity)->player.name;
    }
        break;
    case ENTITY_TYPE_CREATURE:
    {
        entity_t *entity = world_get_creature(gs_get_world(), id);
        if (entity)
            name = ent_get_creature_def(
                entity_get_type_data(entity)->creature.type_id)->name;
    }
        break;
    case ENTITY_TYPE_DYNAMIC_OBJECT:
    {
        entity_t *entity = world_get_dynamic_object(gs_get_world(), id);
        if (entity)
            name = ent_get_dynamic_object_def(
                entity_get_type_data(entity)->dynamic_object.type_id)->name;
    }
        break;
    }
    if (!name)
        goto fail;
    lua_pushstring(lua, name);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_get_my_target(lua_State *lua)
{
    entity_t *entity = gs_get_target();
    if (entity)
        _push_entity_handle(entity);
    else
        lua_pushnil(lua);
    return 1;
}

static int
_luiapi_set_my_target(lua_State *lua)
{
#if 0
    entity_handle_t handle;
    if (_get_entity_handle_arg(-1, &handle))
    {
        DEBUG_PRINTFF("could not get entity handle argument.\n");
        return 0;
    }
    entity_t *entity = world_get_entity(gs_get_world(), handle);
    gs_set_target(entity);
    DEBUG_PRINTFF("Set target to %p.\n", entity);
#endif
    return 0;
}

static int
_luiapi_listen_to_event(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        return 0;
    int type = _str_to_event_enum(lua_tostring(lua, -1));
    if (type == LUI_NUM_EVENTS || win->listening_to[type])
        return 0;
    win->listening_to[type] = 1;
    darr_push(_listener_lists[type].windows, win->index);
    return 0;
}

static int
_luiapi_unlisten_to_event(lua_State *lua)
{
    IMPLEMENTME();
    muta_assert(0);
    return 0;
}

static int
_luiapi_set_event_callback(lua_State *lua)
{
    if (!lua_isnumber(lua, -3) || !lua_isstring(lua, -2) ||
        !lua_isfunction(lua, -1)) // TODO: make nil a valid value for callback
        return 0;
    lui_window_t *win = _get_window_arg(-3);
    if (!win)
    {
        DEBUG_PRINTFF("window not found.\n");
        return 0;
    }
    const char  *event_name = lua_tostring(lua, -2);
    int         event       = _str_to_event_enum(event_name);
    if (event == LUI_NUM_EVENTS)
    {
        DEBUG_PRINTFF("invalid event '%s'.\n", event_name);
        return 0;
    }
    lua_pushstring(lua, _window_table_key);
    lua_gettable(lua, LUA_REGISTRYINDEX);
    muta_assert(!lua_isnil(lua, -1));
    lua_pushstring(lua, win->title);
    lua_gettable(lua, -2);
    muta_assert(!lua_isnil(lua, -1));
    lua_pushnumber(lua, event);
    lua_pushvalue(lua, -4);
    lua_settable(lua, -3);
    return 0;
}

static int
_luiapi_get_window_style_handle(lua_State *lua)
{
    if (!lua_isstring(lua, -1))
        goto fail;
    lui_window_style_t *style = _find_window_style(lua_tostring(lua, -1));
    if (!style)
        goto fail;
    lua_pushinteger(lua, (lua_Integer)(style - _window_styles));
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_get_window_style_background_color(lua_State *lua)
{
    gui_win_state_style_t *style = _get_window_state_style_arg(-2, -1);
    if (!style)
    {
        lua_pushnil(lua);
        return 1;
    }
    _push_color(style->background_color[0], style->background_color[1],
        style->background_color[2], style->background_color[3]);
    return 1;
}

static int
_luiapi_get_window_style_title_origin(lua_State *lua)
{
    gui_win_state_style_t *style = _get_window_state_style_arg(-2, -1);
    if (!style)
    {
        lua_pushnil(lua);
        return 1;
    }
    lua_pushstring(lua, _gui_origin_to_str(style->title_origin));
    return 1;
}

static int
_luiapi_get_window_style_title_offset(lua_State *lua)
{
    gui_win_state_style_t *style = _get_window_state_style_arg(-2, -1);
    if (!style)
    {
        lua_pushnil(lua);
        return 1;
    }
    lua_createtable(lua, 0, 2);
    lua_pushstring(lua, "x");
    lua_pushnumber(lua, style->title_offset[0]);
    lua_settable(_lua, -3);
    lua_pushstring(lua, "y");
    lua_pushnumber(lua, style->title_offset[1]);
    lua_settable(_lua, -3);
    return 1;
}

static int
_luiapi_get_window_style_title_scale(lua_State *lua)
{
    gui_win_state_style_t *style = _get_window_state_style_arg(-2, -1);
    if (!style)
    {
        lua_pushnil(lua);
        return 1;
    }
    lua_createtable(lua, 0, 2);
    lua_pushstring(lua, "x");
    lua_pushnumber(lua, style->title_scale[0]);
    lua_settable(_lua, -3);
    lua_pushstring(lua, "y");
    lua_pushnumber(lua, style->title_scale[1]);
    lua_settable(_lua, -3);
    return 1;
}

static int
_luiapi_get_window_style_title_color(lua_State *lua)
{
    gui_win_state_style_t *style = _get_window_state_style_arg(-2, -1);
    if (!style)
    {
        lua_pushnil(lua);
        return 1;
    }
    _push_color(style->title_color[0], style->title_color[1],
        style->title_color[2], style->title_color[3]);
    return 1;
}

static int
_luiapi_get_window_style_title_font(lua_State *lua)
{
    gui_win_state_style_t *style = _get_window_state_style_arg(-2, -1);
    if (!style)
    {
        lua_pushnil(lua);
        return 1;
    }
    int     font_index  = -1;
    uint32  num_fonts   = darr_num(_fonts);
    for (uint32 i = 0; i < num_fonts; ++i)
        if (_fonts[i].data == style->title_font)
        {
            font_index = i;
            break;
        }
    muta_assert(font_index != -1);
    lua_pushinteger(lua, font_index);
    return 1;
}

static int
_luiapi_get_window_style_border_widths(lua_State *lua)
{
    gui_win_state_style_t *style = _get_window_state_style_arg(-2, -1);
    if (!style)
    {
        lua_pushnil(lua);
        return 1;
    }
    lua_createtable(lua, 0, 4);
    lua_pushstring(lua, "top");
    lua_pushnumber(lua, style->border.widths[GUI_EDGE_TOP]);
    lua_settable(lua, -3);
    lua_pushstring(lua, "left");
    lua_pushnumber(lua, style->border.widths[GUI_EDGE_LEFT]);
    lua_settable(lua, -3);
    lua_pushstring(lua, "right");
    lua_pushnumber(lua, style->border.widths[GUI_EDGE_RIGHT]);
    lua_settable(lua, -3);
    lua_pushstring(lua, "bottom");
    lua_pushnumber(lua, style->border.widths[GUI_EDGE_BOTTOM]);
    lua_settable(lua, -3);
    return 1;
}

static int
_luiapi_get_window_style_border_color(lua_State *lua)
{
    gui_win_state_style_t *style = _get_window_state_style_arg(-2, -1);
    if (!style)
    {
        lua_pushnil(lua);
        return 1;
    }
    _push_color(style->border.color[0], style->border.color[1],
        style->border.color[2], style->border.color[3]);
    return 1;
}

static int
_luiapi_get_clicked_entities(lua_State *lua)
{
    entity_t *entities[GS_MAX_CLICKED_ENTITIES];
    int x, y;
    int num_entities = (int)gs_get_clicked_entities(&x, &y, entities);
    lua_createtable(_lua, 0, 3);
    /* clickY */
    lua_pushstring(_lua, "clickX");
    lua_pushinteger(_lua, x);
    lua_settable(_lua, -3);
    /* clickY */
    lua_pushstring(_lua, "clickY");
    lua_pushinteger(_lua, y);
    lua_settable(_lua, -3);
    /* Entity handle array */
    lua_pushstring(_lua, "entities");
    lua_createtable(_lua, (int)num_entities, 0);
    for (int i = 0; i < num_entities; ++i)
    {
        _push_entity_handle(entities[i]);
        lua_rawseti(_lua, -2, i + 1);
    }
    lua_settable(_lua, -3);
    /* Number of entities */
    lua_pushstring(_lua, "numEntities");
    lua_pushinteger(_lua, num_entities);
    lua_settable(_lua, -3);
    return 1;
}

static int
_luiapi_get_constant(lua_State *lua)
{
    if (!lua_isstring(lua, -1))
        goto fail;
    const char *name = lua_tostring(lua, -1);
    if (streq(name, "MaxClickedEntities"))
    {
        lua_pushinteger(lua, GS_MAX_CLICKED_ENTITIES);
        return 1;
    }
    if (streq(name, "MaxBags"))
    {
        lua_pushinteger(lua, MAX_BAGS_PER_PLAYER);
        return 1;
    }
    if (streq(name, "MaxBagSize"))
    {
        lua_pushinteger(lua, MAX_BAG_SIZE);
        return 1;
    }
    DEBUG_PRINTFF("No constant named '%s'.", name);
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_clear_clicked_entities(lua_State *lua)
{
    gs_clear_clicked_entities();
    return 0;
}

static int
_luiapi_target_clicked_entity(lua_State *lua)
{
    entity_handle_t handle;
    if (_get_entity_handle_arg(-1, &handle))
    {
        DEBUG_PRINTFF("Bad argument: must be entity handle.");
        return 0;
    }
    gs_target_clicked_entity(&handle);
    return 0;
}

static void
_parse_addon_file_callback(void *ctx, const char *opt, const char *val)
{
    addon_parse_ctx_t *pc = ctx;
    if (streq(opt, "name"))
    {
        if (pc->addon->name)
        {
            printf("Addon name '%s' defined twice in the same file.\n", val);
            pc->error = 1;
            return;
        }
        if (!val)
        {
            printf("Error: defines an empty name.\n");
            pc->error = 2;
            return;
        }
        if (_get_addon(val))
        {
            printf("Warning: addon %s already exists.\n", val);
            pc->error = 1;
            return;
        }
        pc->addon->name = dstr_create(val);
    } else
    if (streq(opt, "file"))
    {
        dstr_setf(&pc->lua_file_path, pc->dir_path);
        dstr_append(&pc->lua_file_path, "/");
        dstr_append(&pc->lua_file_path, val);
        if (!file_exists(pc->lua_file_path))
        {
            printf("Warning: lua file '%s' not found (as defined for addon "
                "'%s'", pc->lua_file_path, opt);
            pc->error = 3;
        }
        pc->addon->lua_file_path = dstr_create(pc->lua_file_path);
    } else
        pc->error = 4;
}

static int
_load_addons(void)
{
    dir_entry_t     de;
    dir_handle_t    dh = open_directory("ui/addons", &de);
    if (!dh)
    {
        puts("ui/addons directory not found!");
        return 1;
    }
    dchar *dp = dstr_create_empty((uint)strlen("ui/addons/") + 64);
    while (!get_next_file_in_directory(dh, &de))
    {
        const char *name = get_dir_entry_name(&de);
        if (streq(name, ".") || streq(name, ".."))
            continue;
        dstr_set(&dp, "ui/addons/");
        dstr_append(&dp, name);
        _load_addon_directory(dp);
    }
    dstr_free(&dp);
    close_directory(dh);
    addon_t *addons     = _addons;
    uint32  num_addons  = darr_num(addons);
    for (uint32 i = 0; i < num_addons; ++i)
    {
        LOG("Attempting to run UI Lua file '%s'.", addons[i].lua_file_path);
        lua_pushcfunction(_lua, _lua_stack_trace);
        muta_assert(lua_isfunction(_lua, -1));
        luaL_loadfile(_lua, addons[i].lua_file_path);
        muta_assert(lua_isfunction(_lua, -2));
        if (!lua_pcall(_lua, 0, 0, -2))
            LOG("UI Lua file '%s' ran successfully.", addons[i].lua_file_path);
        else
            LOG("Errors running UI Lua file '%s'!", addons[i].lua_file_path);
    }
    LOG("Loaded %u addons.", darr_num(_addons));
    return 0;
}


static void
_load_addon_directory(const char *dp)
{
    LOG("attempting to load from directory %s.", dp);

    dir_entry_t         de;
    dir_handle_t        dh          = open_directory(dp, &de);
    const int           postfix_len = (int)strlen(".addon");
    bool32              success     = 0;
    addon_parse_ctx_t   ctx         = {0};

    while (!get_next_file_in_directory(dh, &de))
    {
        const char  *file_name      = get_dir_entry_name(&de);
        int         file_name_len   = (int)strlen(file_name);

        if (file_name_len < postfix_len)
            continue;
        if (!streq(file_name + file_name_len - postfix_len, ".addon"))
            continue;

        addon_t addon = {0};

        ctx.error           = 0;
        ctx.addon           = &addon;
        ctx.dir_path        = dp;
        ctx.lua_file_path   = set_dynamic_str_len(ctx.lua_file_path, 0);

        uint32  dp_len  = (uint32)strlen(dp);
        char    *fp     = stack_alloc(dp_len + strlen(file_name) + 2);
        memcpy(fp, dp, dp_len);
        fp[dp_len] = '/';
        memcpy(fp + dp_len + 1, file_name, file_name_len);
        fp[dp_len + file_name_len + 1] = 0;

        bool32 have_err = parse_cfg_file(fp, _parse_addon_file_callback, &ctx)
            || ctx.error;
        if (have_err)
        {
            LOG("Error loading addon '%s'.", fp);
            dstr_free(&addon.name);
            dstr_free(&addon.lua_file_path);
            continue;
        }
        darr_push(_addons, addon);
        success = 1;
    }
    dstr_free(&ctx.lua_file_path);
    if (success)
        LOG("loaded addon %s.", dp);
    else
        LOG("Error loading addon '%s'.", dp);
    close_directory(dh);
}

static addon_t *
_get_addon(const char *name)
{
    addon_t *addons     = _addons;
    uint32  num_addons  = darr_num(addons);
    for (uint32 i = 0; i < num_addons; ++i)
        if (streq(addons[i].name, name))
            return &addons[i];
    return 0;
}

static lui_window_t *
_get_window(const char *name)
{
    lui_window_t    **wins      = _wins.all;
    uint32          num_wins    = darr_num(wins);
    for (uint32 i = 0; i < num_wins; ++i)
        if (streq(name, wins[i]->title))
            return wins[i];
    return 0;
}

static lui_window_t *
_create_window(const char *title)
{
    if (_get_window(title))
        return 0;
    lui_window_t *ret = obj_pool_reserve(&_wins.pool);
    memset(ret, 0, sizeof(lui_window_t));
    ret->title          = dstr_create(title);
    ret->flags          = 0;
    ret->index          = darr_num(_wins.all);
    ret->origin         = GUI_TOP_LEFT;
    ret->style_index    = 0;
    ret->parent         = 0xFFFFFFFF;
    darr_push(_wins.all, ret);
    return ret;
}

static lui_window_obj_data_t *
_create_window_obj(lui_window_t *win)
{
    uint32 id = _window_objs.running_id++;
    lui_window_obj_t *obj = obj_pool_reserve(&_window_objs.pool);
    obj->type               = LUI_WINDOW_OBJ_NONE;
    obj->id                 = id;
    obj->window             = win->index;
    obj->index_in_window    = darr_num(win->objs);
    lui_window_obj_table_einsert(&_window_objs.table, id, obj);
    lui_window_obj_data_t data = {0};
    data.type       = LUI_WINDOW_OBJ_NONE;
    data.obj        = obj;
    data.origin     = GUI_TOP_LEFT;
    darr_push(win->objs, data);
    return &win->objs[obj->index_in_window];
}

static inline lui_window_t *
_get_window_arg(int stack_index)
{
    lua_Integer index = lua_tointeger(_lua, stack_index);
    if (index < 0 || index >= (lua_Integer)darr_num(_wins.all))
        return 0;
    return _wins.all[index];
}

static inline lui_font_t *
_get_font_arg(int stack_index)
{
    lua_Integer index = lua_tointeger(_lua, stack_index);
    if (index < 0 || index >= (lua_Integer)darr_num(_fonts))
        return 0;
    return &_fonts[index];
}

static inline lui_text_t *
_get_text(uint32 id)
{
    lui_window_obj_t **o = lui_window_obj_table_find(&_window_objs.table, id);
    if (!o || (*o)->type != LUI_WINDOW_OBJ_TEXT)
        return 0;
    lui_window_t *win = _wins.all[(*o)->window];
    return &win->objs[(*o)->index_in_window].data.text;
}

static inline lui_button_t *
_get_button(uint32 id)
{
    lui_window_obj_t **o = lui_window_obj_table_find(&_window_objs.table, id);
    if (!o || (*o)->type != LUI_WINDOW_OBJ_BUTTON)
        return 0;
    lui_window_t *win = _wins.all[(*o)->window];
    return &win->objs[(*o)->index_in_window].data.button;
}

static inline lui_texture_t *
_get_texture(uint32 id)
{
    lui_window_obj_t **o = lui_window_obj_table_find(&_window_objs.table, id);
    if (!o || (*o)->type != LUI_WINDOW_OBJ_TEXTURE)
        return 0;
    lui_window_t *win = _wins.all[(*o)->window];
    return &win->objs[(*o)->index_in_window].data.texture;
}

static inline lui_text_input_t *
_get_text_input(uint32 id)
{
    lui_window_obj_t **o = lui_window_obj_table_find(&_window_objs.table, id);
    if (!o || (*o)->type != LUI_WINDOW_OBJ_TEXT_INPUT)
        return 0;
    lui_window_t *win = _wins.all[(*o)->window];
    return &win->objs[(*o)->index_in_window].data.text_input;
}

static inline gui_win_style_t *
_get_window_style(uint32 id)
{
    if (id >= darr_num(_window_styles))
        return 0;
    return _window_styles[id].style;
}

static int
_get_window_event_callback(lui_window_t *win, lui_event_t *event)
{
    muta_assert(lua_gettop(_lua) >= 0);
    lua_pushstring(_lua, _window_table_key);
    lua_gettable(_lua, LUA_REGISTRYINDEX);
    muta_assert(lua_istable(_lua, -1));
    lua_pushstring(_lua, win->title);
    lua_gettable(_lua, -2);
    lua_pushnumber(_lua, (lua_Number)event->type);
    lua_gettable(_lua, -2);
    if (!lua_isfunction(_lua, -1))
    {
        DEBUG_PRINTFF("event callback for event %d in window '%s' was nil!\n",
            event->type, win->title);
        return 1;
    } else
        DEBUG_PRINTFF("Calling lua callback for event %d!\n", event->type);
    return 0;
}

static void
_propagate_event_to_window(lui_window_t *win, lui_event_t *event)
{
    int top = lua_gettop(_lua);
    /* Attempt to find the callback and push it to the lua stack. */
    if (_get_window_event_callback(win, event))
        goto out;
    muta_assert(lua_isfunction(_lua, -1));
    /* Push arguments of the function to the lua stack and call lua_pcall. */
    switch (event->type)
    {
    case LUI_EVENT_UPDATE:
        lua_newtable(_lua);
        lua_pushstring(_lua, "delta");
        lua_pushnumber(_lua, event->update.delta);
        lua_settable(_lua, -3);
        muta_assert(lua_isfunction(_lua, -2));
        lua_pcall(_lua, 1, 0, 0);
        break;
    case LUI_EVENT_APP_WINDOW_SIZE:
        lua_newtable(_lua);
        lua_pushstring(_lua, "width");
        lua_pushnumber(_lua, event->app_window.w);
        lua_settable(_lua, -3);
        lua_pushstring(_lua, "height");
        lua_pushnumber(_lua, event->app_window.h);
        lua_settable(_lua, -3);
        muta_assert(lua_isfunction(_lua, -2));
        lua_pcall(_lua, 1, 0, 0);
        break;
    case LUI_EVENT_NEW_HOTKEY_ACTION:
        lua_pushstring(_lua, _hotkey_table_key);
        lua_gettable(_lua, LUA_REGISTRYINDEX);
        lua_pushstring(_lua, event->new_hotkey_action.action->name);
        muta_assert(lua_istable(_lua, -2 ));
        lua_gettable(_lua, -2);
        muta_assert(lua_isfunction(_lua, -2));
        lua_pcall(_lua, 1, 0, 0);
        break;
    case LUI_EVENT_KEY_DOWN:
        if (_keyboard_grab_window_index != -1 &&
            win->index != _keyboard_grab_window_index)
            break;
        lua_newtable(_lua);
        lua_pushstring(_lua, "key");
        lua_pushstring(_lua, event->key.key_combo_name);
        lua_settable(_lua, -3);
        muta_assert(lua_isfunction(_lua, -2));
        lua_pcall(_lua, 1, 0, 0);
        break;
    case LUI_EVENT_KEY_UP:
        if (_keyboard_grab_window_index != -1 &&
            win->index != _keyboard_grab_window_index)
            break;
        lua_newtable(_lua);
        lua_pushstring(_lua, "key");
        lua_pushstring(_lua, event->key.key_combo_name);
        lua_settable(_lua, -3);
        muta_assert(lua_isfunction(_lua, -2));
        lua_pcall(_lua, 1, 0, 0);
        break;
    case LUI_EVENT_KEY_BIND_CHANGED:
        lua_pushstring(_lua, _hotkey_table_key);
        lua_gettable(_lua, LUA_REGISTRYINDEX);
        lua_pushstring(_lua, event->key_bind_changed.action->name);
        lua_gettable(_lua, -2);
        _lua_remove(_lua, -2);
        muta_assert(lua_isfunction(_lua, -2));
        lua_pcall(_lua, 1, 0, 0);
        break;
    case LUI_EVENT_CHAT_LOG_ENTRY:
    {
        lua_createtable(_lua, 0, 3);
        lua_pushstring(_lua, "type");
        const char *category_str;
        switch (event->chat_log_entry.category)
        {
        case GS_CHAT_CATEGORY_WARNING:
            category_str = "WARNING";
            break;
        case GS_CHAT_CATEGORY_ERROR:
            category_str = "ERROR";
            break;
        case GS_CHAT_CATEGORY_ADDON:
            category_str = "ADDON";
            break;
        case GS_CHAT_CATEGORY_GLOBAL_BROADCAST:
            category_str = "GLOBAL_BROADCAST";
            break;
        case GS_CHAT_CATEGORY_UNCATEGORIZED:
        default:
            category_str = "UNCATEGORIZED";
        }
        lua_pushstring(_lua, category_str);
        lua_settable(_lua, -3);
        lua_pushstring(_lua, "sender");
        if (event->chat_log_entry.sender)
            lua_pushstring(_lua, event->chat_log_entry.sender);
        else
            lua_pushnil(_lua);
        lua_settable(_lua, -3);
        lua_pushstring(_lua, "message");
        lua_pushstring(_lua, event->chat_log_entry.content);
        lua_settable(_lua, -3);
        muta_assert(lua_isfunction(_lua, -2));
        lua_pcall(_lua, 1, 0, 0);
    }
        break;
    case LUI_EVENT_MY_TARGET_CHANGED:
    {
        if (event->my_target_changed.entity_type == NUM_ENTITY_TYPES)
            lua_pushnil(_lua);
        else
        {
            entity_t *entity = 0;
            switch (event->my_target_changed.entity_type)
            {
            case ENTITY_TYPE_PLAYER:
                entity = world_get_player(gs_get_world(),
                    event->my_target_changed.player);
                break;
            case ENTITY_TYPE_CREATURE:
                entity = world_get_creature(gs_get_world(),
                    event->my_target_changed.creature);
                break;
            case ENTITY_TYPE_DYNAMIC_OBJECT:
                entity = world_get_dynamic_object(gs_get_world(),
                    event->my_target_changed.dynamic_object);
                break;
            }
            if (entity)
                _push_entity_handle(entity);
            else
                lua_pushnil(_lua);
        }
        muta_assert(lua_isfunction(_lua, -2));
        lua_pcall(_lua, 1, 0, 0);
    }
        break;
    case LUI_EVENT_CLICKED_ENTITIES_CHANGED:
        muta_assert(lua_isfunction(_lua, -1));
        lua_pcall(_lua, 0, 0, 0);
        break;
    default:
        DEBUG_PRINTFF("warning: unhandled event type %d.\n", event->type);
    }
    out:;
        _lua_settop(_lua, top);
}

static int
_str_to_event_enum(const char *str)
{
    if (!str)
        return LUI_NUM_EVENTS;
    if (streq(str, "UPDATE"))
        return LUI_EVENT_UPDATE;
    else if (streq(str, "APP_WINDOW_SIZE"))
        return LUI_EVENT_APP_WINDOW_SIZE;
    else if (streq(str, "KEY_CALLBACK"))
        return LUI_EVENT_KEY_CALLBACK;
    else if (streq(str, "NEW_KEY_BINDING"))
        return LUI_EVENT_NEW_HOTKEY_ACTION;
    else if (streq(str, "KEY_DOWN"))
        return LUI_EVENT_KEY_DOWN;
    else if (streq(str, "KEY_UP"))
        return LUI_EVENT_KEY_UP;
    else if (streq(str, "KEY_BIND_CHANGED"))
        return LUI_EVENT_KEY_BIND_CHANGED;
    else if (streq(str, "CHAT_LOG_ENTRY"))
        return LUI_EVENT_CHAT_LOG_ENTRY;
    else if (streq(str, "MY_TARGET_CHANGED"))
        return LUI_EVENT_MY_TARGET_CHANGED;
    else if (streq(str, "CLICKED_ENTITIES_CHANGED"))
        return LUI_EVENT_CLICKED_ENTITIES_CHANGED;
    return LUI_NUM_EVENTS;
}

static int
_str_to_gui_origin(const char *str)
{
    if (streq(str, "TOP_LEFT"))
        return GUI_TOP_LEFT;
    if (streq(str, "TOP_RIGHT"))
        return GUI_TOP_RIGHT;
    if (streq(str, "TOP_CENTER"))
        return GUI_TOP_CENTER;
    if (streq(str, "BOTTOM_LEFT"))
        return GUI_BOTTOM_LEFT;
    if (streq(str, "BOTTOM_RIGHT"))
        return GUI_BOTTOM_RIGHT;
    if (streq(str, "BOTTOM_CENTER"))
        return GUI_BOTTOM_CENTER;
    if (streq(str, "CENTER_LEFT"))
        return GUI_CENTER_LEFT;
    if (streq(str, "CENTER_RIGHT"))
        return GUI_CENTER_RIGHT;
    if (streq(str, "CENTER_CENTER"))
        return GUI_CENTER_CENTER;
    return GUI_NUM_ORIGINS;
}

static const char *
_gui_origin_to_str(enum gui_origin origin)
{
    switch (origin)
    {
    case GUI_TOP_LEFT:
        return "TOP_LEFT";
    case GUI_TOP_RIGHT:
        return "TOP_RIGHT";
    case GUI_TOP_CENTER:
        return "TOP_CENTER";
    case GUI_BOTTOM_LEFT:
        return "BOTTOM_LEFT";
    case GUI_BOTTOM_RIGHT:
        return "BOTTOM_RIGHT";
    case GUI_BOTTOM_CENTER:
        return "BOTTOM_CENTER";
    case GUI_CENTER_LEFT:
        return "CENTER_LEFT";
    case GUI_CENTER_RIGHT:
        return "CENTER_RIGHT";
    case GUI_CENTER_CENTER:
        return "CENTER_CENTER";
    default:
        muta_assert(0);
        return 0;
    }
}

static inline void
_post_event(lui_event_t *event)
{
    muta_assert(event->type < LUI_NUM_EVENTS);
    darr_push(_events, *event);
}

static tex_t *
_load_texture_file(const char *path)
{
    tex_t *tex = 0;
    img_t img;
    if (img_load(&img, path))
        return 0;
    tex_t new_tex;
    if (!tex_from_img(&new_tex, &img))
    {
        tex = obj_pool_reserve(&_textures.pool);
        *tex = new_tex;
        str_tex_table_einsert(&_textures.table, path, tex);
    }
    img_free(&img);
    return tex;
}

static int
_lua_stack_trace(lua_State *lua)
{
    luaL_traceback(_lua, _lua, "UI Lua error!", 1);
    int index = -1;
    for (; lua_isstring(_lua, index); --index)
        printf("%s\n", lua_tostring(_lua, index));
    return 0;
}

static void
_create_lua_table_for_hotkey_action(hk_action_t *action)
{
    char    buf[HK_MAX_KEY_COMBO_NAME_LEN + 1];
    int     r;
    lua_newtable(_lua);
    lua_pushstring(_lua, "name");
    lua_pushstring(_lua, action->name);
    lua_settable(_lua, -3);
    lua_pushstring(_lua, "key1");
    r = hk_key_combo_to_str(action->keys[0].keycode, action->keys[0].mods, buf);
    muta_assert(!r);
    lua_pushstring(_lua, buf);
    lua_settable(_lua, -3);
    lua_pushstring(_lua, "key2");
    r = hk_key_combo_to_str(action->keys[1].keycode, action->keys[1].mods, buf);
    muta_assert(!r);
    lua_pushstring(_lua, buf);
    lua_settable(_lua, -3);
}

static int
_is_window_hovered_or_active(lua_State *lua, int *index_val)
{
    int win_index   = *index_val;
    int v           = 0;
    if (win_index < 0 || !lua_isnumber(lua, -1))
        goto out;
    lui_window_t *win = _get_window_arg(-1);
    if (!win)
        return 0;
    v = win->index == (uint32)win_index;
    out:
        lua_pushboolean(lua, v);
        return 1;
}

static void
_remove_window_from_shown(lui_window_t *win)
{
    lui_window_t    **shown     = _wins.shown;
    uint32          num_shown   = darr_num(shown);
    for (uint32 i = 0; i < num_shown; ++i)
    {
        if (shown[i] != win)
            continue;
        darr_erase(shown, i);
        _wins.shown = shown;
        return;
    }
    muta_assert(0);
}

static inline void
_update_text_wh(lui_text_t *text)
{
    lui_window_t *win = _get_window_of_text(text);
    gui_compute_text_size(&text->w, &text->h, text->string,
        _fonts[text->font_index].data, text->scale, text->wrap, win->w, win->h);
}

#define GET_OBJ_WINDOW(obj_ptr, name_in_union) \
    lui_window_obj_data_t *od = (lui_window_obj_data_t*)((uint8*)(obj_ptr) - \
        offsetof(union lui_window_obj_type_data_t, name_in_union) - \
        offsetof(lui_window_obj_data_t, data)); \
    return _wins.all[od->obj->window];

static inline lui_window_t *
_get_window_of_text(lui_text_t *text)
    {GET_OBJ_WINDOW(text, text);}

static inline lui_window_t *
_get_window_of_texture(lui_texture_t *texture)
    {GET_OBJ_WINDOW(texture, texture);}

static inline lui_window_t *
_get_window_of_button(lui_button_t *button)
    {GET_OBJ_WINDOW(button, button);}

static inline lui_window_t *
_get_window_of_text_input(lui_text_input_t *text_input)
    {GET_OBJ_WINDOW(text_input, text_input);}

static lui_window_style_t *
_find_window_style(const char *name)
{
    uint32 num_styles = darr_num(_window_styles);
    for (uint32 i = 0; i < num_styles; ++i)
        if (streq(name, _window_styles[i].name))
            return &_window_styles[i];
    return 0;
}

static lui_window_style_t *
_create_window_style(const char *name)
{
    lui_window_style_t win_style = {
        .name   = dstr_create(name),
        .style  = ecalloc(sizeof(gui_win_style_t))};
    *win_style.style = gui_create_win_style();
    darr_push(_window_styles, win_style);
    return &_window_styles[darr_num(_window_styles) - 1];
}

static void
_push_entity_handle(entity_t *entity)
{
    muta_assert(entity);
    entity_type_data_t *type_data = entity_get_type_data(entity);
    lua_createtable(_lua, 0, 2);
    lua_pushstring(_lua, "type");
    lua_pushinteger(_lua, (lua_Integer)type_data->type);
    lua_settable(_lua, -3);
    lua_pushstring(_lua, "id");
    switch (type_data->type)
    {
    case ENTITY_TYPE_PLAYER:
        lua_pushinteger(_lua, (lua_Integer)type_data->player.id);
        break;
    case ENTITY_TYPE_CREATURE:
        lua_pushinteger(_lua, (lua_Integer)type_data->creature.id);
        break;
    case ENTITY_TYPE_DYNAMIC_OBJECT:
        lua_pushinteger(_lua, (lua_Integer)type_data->dynamic_object.id);
        break;
    default:
        muta_assert(0);
    }
    lua_settable(_lua, -3);
    muta_assert(lua_istable(_lua, -1));
}

static int
_get_entity_handle_arg(int stack_index, entity_handle_t *ret_handle)
{
    if (!lua_istable(_lua, stack_index))
        return 1;
    lua_pushstring(_lua, "type");
    lua_gettable(_lua, stack_index - 1);
    if (!lua_isinteger(_lua, -1))
        return 2;
    lua_Integer type = lua_tointeger(_lua, -1);
    _lua_pop(_lua, 1);
    lua_pushstring(_lua, "id");
    lua_gettable(_lua, stack_index - 1);
    if (!lua_isinteger(_lua, -1))
        return 3;
    lua_Integer id = lua_tointeger(_lua, -1);
    _lua_pop(_lua, 1);
    ret_handle->type    = type;
    switch (type)
    {
    case ENTITY_TYPE_PLAYER:
        ret_handle->player = (player_runtime_id_t)id;
        break;
    case ENTITY_TYPE_CREATURE:
        ret_handle->creature = (creature_runtime_id_t)id;
        break;
    case ENTITY_TYPE_DYNAMIC_OBJECT:
        ret_handle->dynamic_object = (dobj_runtime_id_t)id;
        break;
    default:
        return 1;
    }
    return 0;
}

static gui_win_state_style_t *
_get_window_state_style(uint32 style_id, const char *state_name)
{
    gui_win_style_t *style = _get_window_style(style_id);
    if (!style)
        return 0;
    enum gui_win_state state = _str_to_gui_win_state(state_name);
    if (state == GUI_NUM_WIN_STATES)
        return 0;
    return &style->states[state];
}

static gui_win_state_style_t *
_get_window_state_style_arg(int style_handle_stack_index,
    int state_name_stack_index)
{
    if (!lua_isinteger(_lua, style_handle_stack_index) ||
        !lua_isstring(_lua, state_name_stack_index))
        return 0;
    uint32 id = (uint32)lua_tointeger(_lua, style_handle_stack_index);
    const char *state_name = lua_tostring(_lua, state_name_stack_index);
    return _get_window_state_style(id, state_name);
}

static void
_push_color(uint8 r, uint8 g, uint8 b, uint8 a)
{
    lua_createtable(_lua, 0, 4);
    lua_pushstring(_lua, "r");
    lua_pushnumber(_lua, (lua_Number)r / (lua_Number)255.0);
    lua_settable(_lua, -3);
    lua_pushstring(_lua, "g");
    lua_pushnumber(_lua, (lua_Number)g / (lua_Number)255.0);
    lua_settable(_lua, -3);
    lua_pushstring(_lua, "b");
    lua_pushnumber(_lua, (lua_Number)b / (lua_Number)255.0);
    lua_settable(_lua, -3);
    lua_pushstring(_lua, "a");
    lua_pushnumber(_lua, (lua_Number)a / (lua_Number)255.0);
    lua_settable(_lua, -3);
}

static enum gui_win_state
_str_to_gui_win_state(const char *str)
{
    if (streq(str, "INACTIVE"))
        return GUI_WIN_STATE_INACTIVE;
    if (streq(str, "HOVERED"))
        return GUI_WIN_STATE_HOVERED;
    if (streq(str, "ACTIVE"))
        return GUI_WIN_STATE_ACTIVE;
    return GUI_NUM_WIN_STATES;
}

static lui_text_input_style_t *
_create_text_input_style(const char *name,
    gui_text_input_style_t *text_input_style)
{
    lui_text_input_style_t *style = darr_push_empty(
        _text_input_styles);
    style->name = dstr_create(name);
    *(style->style = emalloc(sizeof(*text_input_style))) = *text_input_style;
    return style;
}

static void
_lua_remove(lua_State *lua, int index)
{
    muta_assert(lua_gettop(lua) >= 0);
    lua_remove(_lua, index);
}

static void
_lua_pop(lua_State *lua, int num)
{
    muta_assert(lua_gettop(lua) - num >= 0);
    lua_pop(lua, num);
}

static void
_lua_settop(lua_State *lua, int top)
{
    muta_assert(top >= 0);
    lua_settop(lua, top);
}
