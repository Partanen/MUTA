#include "dynamic_object_entity.h"
#include "world.h"
#include "log.h"
#include "assets.h"

entity_t *
dynamic_object_spawn(world_t *world, dobj_runtime_id_t id,
    dobj_type_id_t type_id, int direction, int position[3])
{
    LOG_DEBUG("Spawning dynamic object %u...", id);
    entity_t *entity = world_get_dynamic_object(world, id);
    if (entity)
    {
        LOG_DEBUG(
            "Dynamic object with ID %u already exists. Despawning first.", id);
        entity_despawn(entity);
    }
    entity = entity_spawn(world, direction, position);
    if (!entity)
    {
        LOG_ERR("Failed to spawn a dynamic object entity!");
        return entity;
    }
    hashtable_einsert(world->dynamic_object_table, id,
        hashtable_hash(&id, sizeof(id)), entity);
    entity_type_data_t *type_data = entity_get_type_data(entity);
    type_data->type                     = ENTITY_TYPE_DYNAMIC_OBJECT;
    type_data->dynamic_object.id        = id;
    type_data->dynamic_object.type_id   = type_id;
    /* Attach components */
    component_handle_t ae_animator_component = entity_attach_component(entity,
        &ae_animator_component_definition);
    if (ae_animator_component == INVALID_COMPONENT_HANDLE)
    {
        LOG_ERR("Failed to attach animator component.");
        goto despawn;
    }
    component_handle_t render_component = entity_attach_component(entity,
        &render_component_definition);
    if (render_component == INVALID_COMPONENT_HANDLE)
    {
        LOG_ERR("Failed to attach render component.");
        goto despawn;
    }
    component_handle_t ae_set_component = entity_attach_component(entity,
        &ae_set_component_definition);
    if (ae_set_component == INVALID_COMPONENT_HANDLE)
    {
        LOG_ERR("Failed to attach ae set component.");
        goto despawn;
    }
    dynamic_object_def_t *def = ent_get_dynamic_object_def(type_id);
    if (!def)
    {
        LOG_ERR("Dynamic object type %u not found.", type_id);
        goto despawn;
    }
    as_dynamic_object_display_t *dynamic_object_display =
        as_get_dynamic_object_display(def->display_id);
    if (!dynamic_object_display)
        goto despawn;
    ae_set_component_set_asset(ae_set_component,
        as_get_ae_set(dynamic_object_display->ae_set));
    LOG_DEBUG("Successful, id %u, position %d %d %d.", id, position[0],
        position[1], position[2]);
    return entity;
    despawn:
        entity_despawn(entity);
        return 0;
}
