#ifndef MUTA_CLIENT_COMPONENT_H
#define MUTA_CLIENT_COMPONENT_H

/* Components are pieces of an entity. To define a component:
 * - (Optional) Create a "system" struct where the components are stored. Add an
 *   instance of it to the world_t struct. Not required if components are
 *   reserved from elsewhere (such as malloc).
 * - Create the functions required by component_definition_t.
 * - Create an instance of component_definition_t where the correct functions
 *   have been assigned.
 * - Add the address of the component_definition_t to the COMPONENT_DEFINITIONS
 *   macro in world_data.h. */

#include "../../shared/types.h"

#define INVALID_COMPONENT_HANDLE 0

/* Forward declaration(s) */
typedef struct entity_t         entity_t;
typedef struct world_t          world_t;
typedef struct entity_event_t   entity_event_t;

/* Types defined here */
typedef struct component_definition_t       component_definition_t;
typedef void*                               component_handle_t;
typedef struct entity_event_interest_t      entity_event_interest_t;
typedef struct component_event_interest_t   component_event_interest_t;
typedef void (*entity_event_callback_t)(entity_event_t*);
typedef void (*component_event_callback_t)(entity_t*, component_handle_t,
    void*);

struct component_definition_t
{
    int (*system_init)(world_t *world, uint32 num_components);
    /* system_init() can be null. Called when world is initialized. Init
     * functions are called in the order the systems are registered in. The
     * function should return non-zero on failure. */

    void (*system_destroy)(world_t *world);
    /* system_destroy() can be null. Called when the world is destroyed. Destroy
     * functions are called in the reverse order in which systems were
     * registered in. */

    void (*system_update)(world_t *world, float dt);
    /* system_update() can be null. Called every frame. Update functions are
     * called in the order systems have been registered. */

    component_handle_t (*component_attach)(entity_t *entity);
    /* component_new() should return a handle to the newly reserved component,
     * or INVALID_COMPONENT_HANDLE on failure.  Called when
     * entity_attach_component() is called. */

    void (*component_detach)(component_handle_t handle);
    /* component_detach() can be null. It is called when
     * entity_detach_component() is called, right before the component is
     * removed from the entity. */

    entity_event_interest_t     *entity_event_interests;
    uint32                      num_entity_event_interests;
    /* A pointer to an array of entity events this component is interested in.
     * */

    component_event_interest_t  *component_event_interests;
    uint32                      num_component_event_interests;
    /* A pointer to an array of events this component is interested in from
     * other components */

    uint32                      index;
    /* Autofilled, initialize to 0! */

    component_event_callback_t  *component_event_callbacks;
    /* Autofilled, initialize to 0! */
};

struct entity_event_interest_t
{
    int                     event;
    entity_event_callback_t callback;
};

struct component_event_interest_t
{
    component_definition_t      *component_definition;
    component_event_callback_t  callback;
};

#endif /* MUTA_CLIENT_COMPONENT_H */
