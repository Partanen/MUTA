#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <SDL2/SDL.h>
#include "../../shared/gui/gui.h"
#include "bitmap.h"
#include "gl.h"
#include "core.h"
#include "../../shared/glad.h"
#include "../../shared/common_utils.h"

typedef struct sb_cmd_t         sb_cmd_t;
typedef struct spritebatch_t    spritebatch_t;

#define MAX_TEXTURE_UNITS 16
#define ACTIVE_TEX_INDEX  (gl_state.active_texture - GL_TEXTURE0)
#define GUI_GLYPH(font_, index_) \
    (&(font_)->glyphs[(index_) % (font_)->num_glyphs])


#ifndef SB_USE_MAPPED_VBOS
    #define SB_USE_MAPPED_VBOS 1
#endif
/* Compile-time switch for glMapBuffer() vs glBufferSubData() */

#ifndef SB_USE_SAFE_LIMITS
    #define SB_USE_SAFE_LIMITS 1
#endif
/* Compile-time switch for whether or not the sb_sprite functions will check if
 * there's enough space for a new sprite. If this is not turned on, it is up
 * to the caller to take care of possible overflows. */

#if SB_USE_MAPPED_VBOS
    #define SB_BUFFER_USAGE GL_STREAM_DRAW
#else
    #define SB_BUFFER_USAGE GL_DYNAMIC_DRAW
#endif

#define SB_NUM_FLOATS_PER_QUAD  24
#define SIZE_OF_SPRITE          (SB_NUM_FLOATS_PER_QUAD * sizeof(float))

#if SB_USE_SAFE_LIMITS
    #define SB_CHECK_SPACE_FOR_ONE() \
        if (_sb.num_floats >= _sb.max_floats) \
            return;
    #define SB_CHECK_SPACE_FOR_NUM(num_, ret_context_) \
        if (_sb.num_floats + (num_) * SB_NUM_FLOATS_PER_QUAD) \
            return ret_context_;
#else
    #define SB_CHECK_SPACE_FOR_ONE()
    #define SB_CHECK_SPACE_FOR_NUM(num_)
#endif

#define SB_WRITE_SPRITE_VERTS(tx, vbo_mem, start_index, clip, x, y, z, w, h, \
    col) \
    (vbo_mem)[(start_index) +  0] = (x); \
    (vbo_mem)[(start_index) +  1] = (y); \
    (vbo_mem)[(start_index) +  2] = (z); \
    (vbo_mem)[(start_index) +  6] = (x) + (w); \
    (vbo_mem)[(start_index) +  7] = (y); \
    (vbo_mem)[(start_index) +  8] = (z); \
    (vbo_mem)[(start_index) + 12] = (x); \
    (vbo_mem)[(start_index) + 13] = (y) + (h); \
    (vbo_mem)[(start_index) + 14] = (z); \
    (vbo_mem)[(start_index) + 18] = (x) + (w); \
    (vbo_mem)[(start_index) + 19] = (y) + (h); \
    (vbo_mem)[(start_index) + 20] = (z); \
    (vbo_mem)[(start_index) +  3] = (clip)[0] / (tx)->w; \
    (vbo_mem)[(start_index) +  4] = (clip)[1] / (tx)->h; \
    (vbo_mem)[(start_index) +  9] = (clip)[2] / (tx)->w; \
    (vbo_mem)[(start_index) + 10] = (clip)[1] / (tx)->h; \
    (vbo_mem)[(start_index) + 15] = (clip)[0] / (tx)->w; \
    (vbo_mem)[(start_index) + 16] = (clip)[3] / (tx)->h; \
    (vbo_mem)[(start_index) + 21] = (clip)[2] / (tx)->w; \
    (vbo_mem)[(start_index) + 22] = (clip)[3] / (tx)->h; \
    int csi_ = (start_index) * 4; \
    ((GLubyte*)(vbo_mem))[csi_ + 20] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 21] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 22] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 23] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 44] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 45] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 46] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 47] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 68] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 69] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 70] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 71] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 92] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 93] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 94] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 95] = (col)[3];

#define SB_WRITE_FLIPPED_SPRITE_VERTS(tex, vbo_mem, start_index, clip, x, y, \
    z, w, h, col, flip) \
{ \
    (vbo_mem)[(start_index) +  0] = (x); \
    (vbo_mem)[(start_index) +  1] = (y); \
    (vbo_mem)[(start_index) +  2] = (z); \
    (vbo_mem)[(start_index) +  6] = (x) + (w); \
    (vbo_mem)[(start_index) +  7] = (y); \
    (vbo_mem)[(start_index) +  8] = (z); \
    (vbo_mem)[(start_index) + 12] = (x); \
    (vbo_mem)[(start_index) + 13] = (y) + ((h)); \
    (vbo_mem)[(start_index) + 14] = (z); \
    (vbo_mem)[(start_index) + 18] = (x) + (w); \
    (vbo_mem)[(start_index) + 19] = (y) + ((h)); \
    (vbo_mem)[(start_index) + 20] = (z); \
    switch (flip) \
    { \
        case SB_FLIP_NONE: \
            (vbo_mem)[(start_index) +  3] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) +  4] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) +  9] = (clip)[2] / (tex)->w; \
            (vbo_mem)[(start_index) + 10] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) + 15] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 16] = (clip)[3] / (tex)->h; \
            (vbo_mem)[(start_index) + 21] = (clip)[2] / (tex)->w; \
            (vbo_mem)[(start_index) + 22] = (clip)[3] / (tex)->h; \
            break; \
        case SB_FLIP_H: \
            (vbo_mem)[(start_index) +  3] = (clip)[2] / (tex)->w;\
            (vbo_mem)[(start_index) +  4] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) +  9] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 10] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) + 15] = (clip)[2] / (tex)->w;\
            (vbo_mem)[(start_index) + 16] = (clip)[3] / (tex)->h; \
            (vbo_mem)[(start_index) + 21] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 22] = (clip)[3] / (tex)->h; \
            break; \
        case SB_FLIP_V: \
            (vbo_mem)[(start_index) +  3] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) +  4] = (clip)[3] / (tex)->h;\
            (vbo_mem)[(start_index) +  9] = (clip)[2] / (tex)->w; \
            (vbo_mem)[(start_index) + 10] = (clip)[3] / (tex)->h;\
            (vbo_mem)[(start_index) + 15] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 16] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) + 21] = (clip)[2] / (tex)->w; \
            (vbo_mem)[(start_index) + 22] = (clip)[1] / (tex)->h; \
            break; \
        case SB_FLIP_BOTH: \
            (vbo_mem)[(start_index) +  3] = (clip)[2] / (tex)->w;\
            (vbo_mem)[(start_index) +  4] = (clip)[3] / (tex)->h;\
            (vbo_mem)[(start_index) +  9] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 10] = (clip)[3] / (tex)->h; \
            (vbo_mem)[(start_index) + 15] = (clip)[2] / (tex)->w;\
            (vbo_mem)[(start_index) + 16] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) + 21] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 22] = (clip)[1] / (tex)->h; \
            break; \
    } \
    int csi_ = (start_index) * 4; \
    ((GLubyte*)(vbo_mem))[csi_ + 20] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 21] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 22] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 23] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 44] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 45] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 46] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 47] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 68] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 69] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 70] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 71] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 92] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 93] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 94] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 95] = (col)[3]; \
}

#define SB_ROTATE_SPRITE(vbo_mem, first_index, angle, ox, oy) \
    for (int i = 0; i < 4; ++i) \
    { \
        float tx, ty, rx, ry; \
        tx = (vbo_mem)[(first_index) + (i * 6) + 0] - (ox); \
        ty = (vbo_mem)[(first_index) + (i * 6) + 1] - (oy); \
        rx = tx * cosf((angle)) - ty * sinf((angle)); \
        ry = tx * sinf((angle)) + ty * cosf((angle)); \
        (vbo_mem)[(first_index) + (i * 6) + 0] = rx + (ox); \
        (vbo_mem)[(first_index) + (i * 6) + 1] = ry + (oy); \
    } \

#define SB_WRITE_SPRITE_REPEAT_CMD(tex_, num_repeats_) \
    sb_cmd_t *cmd; \
    if (_sb.num_cmds) \
    { \
        cmd = &_sb.cmds[_sb.num_cmds - 1]; \
        if (cmd->tex == (tex_)->id) \
        { \
            cmd->num_repeats += (num_repeats_); \
        } else \
        { \
            cmd = &_sb.cmds[_sb.num_cmds]; \
            cmd->tex = (tex_)->id; \
            cmd->num_repeats = (num_repeats_); \
            _sb.num_cmds++; \
        } \
    } else \
    { \
        cmd = &_sb.cmds[0]; \
        cmd->num_repeats = (num_repeats_); \
        cmd->tex = (tex_)->id; \
        ++_sb.num_cmds; \
    } \
    _sb.num_floats += (num_repeats_) * SB_NUM_FLOATS_PER_QUAD;

#define SB_WRITE_SPRITE_CMD(tex_) SB_WRITE_SPRITE_REPEAT_CMD((tex_), 1)

#define SB_PREP_SPRITE_CMD_FOR_TEXT(cmd_ptr, tex) \
{ \
    if (_sb.num_cmds != 0) \
    { \
        cmd_ptr = &_sb.cmds[_sb.num_cmds - 1]; \
        if (cmd_ptr->tex != (tex)->id) \
        { \
            (cmd_ptr) = &_sb.cmds[_sb.num_cmds]; \
            (cmd_ptr)->tex = (tex)->id; \
            (cmd_ptr)->num_repeats = 0; \
            ++_sb.num_cmds; \
        } \
    } \
    else \
    { \
        cmd_ptr = &_sb.cmds[0]; \
        cmd_ptr->tex = tex->id; \
        cmd_ptr->num_repeats = 0; \
        ++_sb.num_cmds; \
    } \
}

#define SB_WRITE_WRAPPED_STRING_VERTS_S(sb, sf, txt, x, y, z, wrap, sx, sy, col) \
{ \
    gui_glyph_t *g; \
    float w, h, rx, ry; \
    float px    = x; \
    float py    = y; \
    float max_w = x + wrap; \
    float x_adv; \
    float y_adv = sf->vertical_advance * sx; \
    for (const char *c = txt; *c; ++c) \
    { \
        SB_CHECK_SPACE_FOR_ONE(); \
        if (*c == '\n') \
        { \
            px = x; \
            py += y_adv; \
            continue; \
        } \
        g       = GUI_GLYPH(sf, (int)*c); \
        w       = (g->clip[2] - g->clip[0]) * sx; \
        h       = (g->clip[3] - g->clip[1]) * sy; \
        x_adv   = g->advance * sx; \
        if (c != txt && px + x_adv > max_w ) \
        { \
            px = x; \
            py += y_adv; \
        } \
        rx  = px; \
        ry  = (py - h + (sy * g->y_offset)); \
        SB_WRITE_SPRITE_VERTS(tex, sb.mapped_vbo, sb.num_floats, g->clip, rx, \
            ry, z, w, h, col); \
        px += x_adv; \
        ++cmd->num_repeats; \
        sb.num_floats += SB_NUM_FLOATS_PER_QUAD; \
    } \
}

struct sb_cmd_t
{
    int     num_repeats;
    uint32  tex;
};

struct spritebatch_t
{
    int         began;
    uint32      vbo;
    uint32      ebo;
    int         vbo_offset;
    float       *mapped_vbo;
    sb_cmd_t    *cmds;
    int         max_cmds;
    int         buf_size;
    int         num_cmds;
    int         repeat_count;
    int         num_floats;
    int         max_floats; /* Saved for possible space checks */
    float       coord_space[4];
    uint32      shader;
    int         index_type;
};

static struct
{
    int     active_texture;
    uint32  bound_vbo;
    uint32  bound_ebo;
    uint32  bound_program;
    int     blend_sfactor;
    int     blend_dfactor;
    bool32  blend_enabled;
    bool32  depth_test_enabled;
    bool32  scissor_enabled;
    uint32  bound_tex2ds[MAX_TEXTURE_UNITS];
    GLint   viewport[4];
    GLint   scissor[4];
} gl_state;

static SDL_GLContext    _gl_context;
static uint8            _sb_col_white[4] = {255, 255, 255, 255};
static spritebatch_t    _sb;

int     RB_CLEAR_COLOR_BIT = GL_COLOR_BUFFER_BIT;
int     RB_CLEAR_DEPTH_BIT = GL_DEPTH_BUFFER_BIT;
uint32  sb_opaque_shader;
uint32  sb_blend_shader;

static inline GLenum
_img_pxf_to_gl_enum(enum img_pxf_t pxf);

static inline void
_bind_sb_vao(void);

int
gl_init(void *window_data)
{
    int major = 2;
    int minor = 0;
    /*-- Init OpenGL --*/
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, major);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minor);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
        SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
    _gl_context = SDL_GL_CreateContext(window_data);
    if (!_gl_context)
    {
        core_error_window("SDL_GL_CreateContext() failed: %s. "
            "MUTA requires OpenGL version %d.%d.", SDL_GetError(),
            major, minor);
        return 1;
    }
    if (!gladLoadGL())
    {
        core_error_window("gladLoadGL() failed.");
        return 1;
    }
    /*-- Init GL state --*/
    gl_active_texture(0);
    gl_enable_blend();
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    gl_state.blend_sfactor = GL_SRC_ALPHA;
    gl_state.blend_dfactor = GL_ONE_MINUS_SRC_ALPHA;
    gl_disable_depth_test();
    gl_enable_scissor();
    glGetIntegerv(GL_VIEWPORT, gl_state.viewport);
    glGetIntegerv(GL_SCISSOR_BOX, gl_state.scissor);
    return 0;
}

void
gl_destroy(void)
{
    SDL_GL_DeleteContext(_gl_context);
}

void
gl_use_program(uint32 program)
{
    if (gl_state.bound_program == program)
        return;
    glUseProgram(program);
    gl_state.bound_program = program;
}

void
gl_bind_vbo(uint32 vbo)
{
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    gl_state.bound_vbo = vbo;
}

void
gl_bind_ebo(uint32 ebo)
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    gl_state.bound_ebo = ebo;
}

void
gl_bind_tex2d(uint32 tex)
{
    glBindTexture(GL_TEXTURE_2D, tex);
    gl_state.bound_tex2ds[ACTIVE_TEX_INDEX] = tex;
}

void
gl_bind_tex2d_if_not_bound(uint32 tex)
{
    if (gl_state.bound_tex2ds[ACTIVE_TEX_INDEX] == tex)
        return;
    gl_bind_tex2d(tex);
}

void
gl_active_texture(int tex)
{
    glActiveTexture(GL_TEXTURE0 + tex);
    gl_state.active_texture = GL_TEXTURE0 + tex;
}

void
gl_enable_blend(void)
{
    glEnable(GL_BLEND);
    gl_state.blend_enabled = 1;
}

void
gl_disable_blend(void)
{
    glDisable(GL_BLEND);
    gl_state.blend_enabled = 0;
}

void
gl_enable_depth_test(void)
{
    glEnable(GL_DEPTH_TEST);
    gl_state.depth_test_enabled = 1;
}

void
gl_disable_depth_test(void)
{
    glDisable(GL_DEPTH_TEST);
    gl_state.depth_test_enabled = 0;
}

void
gl_enable_scissor(void)
{
    glEnable(GL_SCISSOR_TEST);
    gl_state.scissor_enabled = 1;
}

void
gl_disable_scissor(void)
{
    glDisable(GL_SCISSOR_TEST);
    gl_state.scissor_enabled = 0;
}

void
gl_color(float r, float g, float b, float a)
    {glClearColor(r, g, b, a);}

void
gl_clear(int mask)
    {glClear(mask);}

void
gl_scissor(int x, int y, int w, int h)
{
    glScissor(x, core_window_h() - h - y, w, h);
    gl_state.scissor[0] = x;
    gl_state.scissor[1] = y;
    gl_state.scissor[2] = w;
    gl_state.scissor[3] = h;
}

void
gl_get_scissor(int *x, int *y, int *w, int *h)
{
    *x = gl_state.scissor[0];
    *y = gl_state.scissor[1];
    *w = gl_state.scissor[2];
    *h = gl_state.scissor[3];
}

void
gl_viewport(int x, int y, int w, int h)
{
    glViewport(x, core_window_h() - h - y, w, h);
    gl_state.viewport[0] = x;
    gl_state.viewport[1] = y;
    gl_state.viewport[2] = w;
    gl_state.viewport[3] = h;
}

void
gl_get_viewport(int *x, int *y, int *w, int *h)
{
    *x = gl_state.viewport[0];
    *y = gl_state.viewport[1];
    *w = gl_state.viewport[2];
    *h = gl_state.viewport[3];
}

void
gl_viewport_and_scissor(int x, int y, int w, int h)
{
    gl_viewport(x, y, w, h);
    gl_scissor(x, y, w, h);
}

void
gl_set_vsync(bool32 opt)
    {SDL_GL_SetSwapInterval(opt);}

bool32
gl_get_vsync(void)
    {return SDL_GL_GetSwapInterval();}

void
gl_swap_buffers(void *window_data)
    {SDL_GL_SwapWindow(window_data);}

void
gl_print_errors(void)
{
    GLenum error;
    while ((error = glGetError()) != GL_NO_ERROR)
        printf("OpenGL error %i\n", error);
}

int
tex_from_mem(tex_t *tex, uint8 *pixels, int w, int h, int pxf)
{
    return tex_from_mem_ext(tex, pixels, w, h, pxf, TEX_FILTER_LINEAR,
        TEX_FILTER_NEAREST, TEX_WRAP_REPEAT);
}

int
tex_from_mem_ext(tex_t *tex, uint8 *pixels, int w, int h,
    int pxf,
    enum tex_filter_t min_filter,
    enum tex_filter_t mag_filter,
    enum tex_wrapping_t wrapping)
{
    GLenum min_filt  = min_filter == TEX_FILTER_LINEAR  ? GL_LINEAR  : GL_NEAREST;
    GLenum mag_filt  = mag_filter == TEX_FILTER_NEAREST ? GL_NEAREST : GL_LINEAR;
    GLenum wrap_type = 0;
    switch (wrapping)
    {
    default:
    case TEX_WRAP_REPEAT:          wrap_type = GL_REPEAT;          break;
    case TEX_WRAP_MIRROR_REPEAT:   wrap_type = GL_MIRRORED_REPEAT; break;
    case TEX_WRAP_CLAMP_TO_EDGE:   wrap_type = GL_CLAMP_TO_EDGE;   break;
    case TEX_WRAP_CLAMP_TO_BORDER: wrap_type = GL_CLAMP_TO_BORDER; break;
    };
    glGenTextures(1, &tex->id);
    gl_active_texture(0);
    gl_bind_tex2d(tex->id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min_filt);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag_filt);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_type);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_type);
    GLenum px_format = _img_pxf_to_gl_enum(pxf);
    glTexImage2D(GL_TEXTURE_2D, 0, px_format, w, h, 0, px_format,
        GL_UNSIGNED_BYTE, pixels);
    gl_bind_tex2d(0);
    tex->w = (GLfloat)w;
    tex->h = (GLfloat)h;
    return 0;
}

int
tex_from_img(tex_t *tex, img_t *img)
{
    int res = tex_from_mem(tex, img->pixels, img->w, img->h, img->pxf);
    if (res)
        return (res + 1);
    return 0;
}

int
tex_from_img_ext(tex_t *tex, img_t *img, int min_filter, int mag_filter,
    int wrapping)
{
    int res = tex_from_mem_ext(tex, img->pixels, img->w, img->h, img->pxf,
        min_filter, mag_filter, wrapping);
    return res ? res + 1 : 0;
}

void
tex_free(tex_t *tex)
{
    glDeleteTextures(1, &tex->id);
    memset(tex, 0, sizeof(tex_t));
}

int
gl_generate_gui_font_texture(gui_font_t *font)
{
    return gl_generate_gui_font_texture_ext(font, TEX_FILTER_LINEAR,
        TEX_FILTER_NEAREST, TEX_WRAP_REPEAT);
}

int
gl_generate_gui_font_texture_ext(gui_font_t *font,
    enum tex_filter_t min_filter,
    enum tex_filter_t mag_filter,
    enum tex_wrapping_t wrapping)
{
    tex_t tex;
    if (tex_from_mem_ext(&tex, font->bitmap, font->bitmap_w, font->bitmap_h,
        IMG_RGBA, min_filter, mag_filter, wrapping))
        return 1;
    font->tex = emalloc(sizeof(tex_t));
    *(tex_t*)font->tex = tex;
    return 0;
}

void
gl_destroy_gui_font_texture(gui_font_t *font)
{
    if (!font->tex)
        return;
    tex_free(font->tex);
    free(font->tex);
    font->tex = 0;
}

int
sb_init(int max_sprites, char *err_log, int err_log_len)
{
    if (err_log)
        err_log[0] = 0;
    if (max_sprites <= 0)
        return 1;
    _sb.cmds = malloc(max_sprites * sizeof(sb_cmd_t));
    if (!_sb.cmds)
        return 2;
    /* Create the default shader */
    const char *vtx_code =
        "#version 100\n"
        "precision lowp float;"
        "attribute vec3 position;"
        "attribute vec2 in_uv;"
        "attribute vec4 in_color;"
        "varying vec2 uv;"
        "varying vec4 color;"
        "uniform mat4 projection;"
        "void main(void)"
        "{"
            "gl_Position    = projection * vec4(position, 1.0);"
            "uv             = in_uv;"
            "color          = in_color;"
        "}";
    const char *frg_code =
        "#version 100\n"
        "precision lowp float;"
        "varying vec2 uv;"
        "varying vec4 color;"
        "uniform sampler2D tex;"
        "void main(void)\n"
        "{"
            "vec4 frag_color = texture2D(tex, uv) * color;"
            "if (frag_color.w == 0.0)"
                "discard;"
            "gl_FragColor = frag_color;"
        "}";
    sb_opaque_shader = gl_shader_from_strs(vtx_code, frg_code, err_log,
        err_log_len);
    if (!sb_opaque_shader)
        return 3;
    /*-- Create the defaut blended shader --*/
    frg_code =
        "#version 100\n"
        "precision lowp float;"
        "varying vec2 uv;"
        "varying vec4 color;"
        "uniform sampler2D tex;"
        "void main(void)\n"
        "{"
        "   gl_FragColor = texture2D(tex, uv) * color;"
        "}";
    sb_blend_shader = gl_shader_from_strs(vtx_code, frg_code, err_log,
        err_log_len);
    if (!sb_blend_shader)
        return 4;
    /*-- Gen buffers --*/
    glGenBuffers(1, &_sb.vbo);
    gl_bind_vbo(_sb.vbo);
    glBufferData(GL_ARRAY_BUFFER, max_sprites * SIZE_OF_SPRITE, 0,
        SB_BUFFER_USAGE);
#if !SB_USE_MAPPED_VBOS
    void *vbo_mem = malloc(max_sprites * SIZE_OF_SPRITE);
    if (!vbo_mem) return 5;

    _sb.mapped_vbo = (GLfloat*)vbo_mem;
#endif
    GLint loc_position  = glGetAttribLocation(sb_opaque_shader, "position");
    if (loc_position < 0)
        return 6;
    GLint loc_tex_coord = glGetAttribLocation(sb_opaque_shader, "in_uv");
    if (loc_tex_coord < 0)
        return 7;
    glVertexAttribPointer(SHADER_ATTR_LOC_POS, 3, GL_FLOAT, GL_FALSE,
        6 * sizeof(GLfloat), 0);
    glVertexAttribPointer(SHADER_ATTR_LOC_UV, 2, GL_FLOAT, GL_FALSE,
        6 * sizeof(GLfloat), (const GLvoid *)(3 * sizeof(GLfloat)));
    glVertexAttribPointer(SHADER_ATTR_LOC_COL, 4, GL_UNSIGNED_BYTE, GL_TRUE,
        6 * sizeof(GLfloat), (const GLvoid *)(5 * sizeof(GLfloat)));
    glEnableVertexAttribArray(SHADER_ATTR_LOC_UV);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_POS);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_COL);
    /* Element array buffer */
    glGenBuffers(1, &_sb.ebo);
    gl_bind_ebo(_sb.ebo);
    GLsizei single_index_size;
    int max_verts = max_sprites * 6;
    if (max_verts <= 256)
    {
        single_index_size = sizeof(GLubyte);
        _sb.index_type = GL_UNSIGNED_BYTE;
    } else
    if (max_verts <= 0xFFFF)
    {
        single_index_size = sizeof(GLushort);
        _sb.index_type = GL_UNSIGNED_SHORT;
    } else
    {
        single_index_size = sizeof(GLuint);
        _sb.index_type = GL_UNSIGNED_INT;
    }
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, max_verts * single_index_size, 0,
        SB_BUFFER_USAGE);
    void *ebo_data = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
    switch (_sb.index_type)
    {
        case GL_UNSIGNED_BYTE:
        {
            GLubyte indices[6] = {0, 1, 2, 2, 3, 1};
            for (GLubyte i = 0; i < (GLubyte)max_sprites; ++i)
                for (GLubyte j = 0; j < 6; ++j)
                    ((GLubyte*)ebo_data)[i * 6 + j] = indices[j] + i * 4;
        }
            break;
        case GL_UNSIGNED_SHORT:
        {
            GLushort indices[6] = {0, 1, 2, 2, 3, 1};
            for (GLushort i = 0; i < (GLushort)max_sprites; ++i)
                for (GLushort j = 0; j < 6; ++j)
                    ((GLushort*)ebo_data)[i * 6 + j] = indices[j] + i * 4;
        }
            break;
        case GL_UNSIGNED_INT:
        {
            GLuint indices[6] = {0, 1, 2, 2, 3, 1};
            for (GLuint i = 0; i < (GLuint)max_sprites; ++i)
                for (GLuint j = 0; j < 6; ++j)
                    ((GLuint*)ebo_data)[i * 6 + j] = indices[j] + i * 4;
        }
            break;
        default:
            assert(0);
    }
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
    gl_bind_ebo(0);
    gl_bind_vbo(0);
    _sb.max_cmds    = max_sprites;
    _sb.max_floats  = max_sprites * SB_NUM_FLOATS_PER_QUAD;
    _sb.buf_size    = max_sprites * SIZE_OF_SPRITE;
    return 0;
}

void
sb_destroy(void)
{
    glDeleteBuffers(1, &_sb.vbo);
    glDeleteBuffers(1, &_sb.ebo);
    free(_sb.cmds);
#if !SB_USE_MAPPED_VBOS
    free(_sb.mapped_vbo);
#endif
    glDeleteProgram(sb_opaque_shader);
    glDeleteProgram(sb_blend_shader);
    memset(&_sb, 0, sizeof(spritebatch_t));
}

int
sb_begin(uint32 shader, float *coord_space)
{
    assert(!_sb.began);
    _sb.began               = 1;
    _sb.num_cmds            = 0;
    _sb.repeat_count        = 0;
    _sb.cmds[0].num_repeats = 0;
    _sb.num_floats          = 0;
    if (coord_space)
    {
        _sb.coord_space[0] = coord_space[0];
        _sb.coord_space[1] = coord_space[1];
        _sb.coord_space[2] = coord_space[2];
        _sb.coord_space[3] = coord_space[3];
    } else
    {
        GLint vp[4];
        glGetIntegerv(GL_VIEWPORT, vp);
        _sb.coord_space[0] = 0.f;
        _sb.coord_space[1] = 0.f;
        _sb.coord_space[2] = (GLfloat)vp[2];
        _sb.coord_space[3] = (GLfloat)vp[3];
    }
    _sb.shader = shader;
#if SB_USE_MAPPED_VBOS
    gl_bind_vbo(_sb.vbo);
    _sb.mapped_vbo = (GLfloat*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
#endif
    return 0;
}

void
sb_sprite(tex_t *tex, float *clip, float x, float y, float z)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, _sb_col_white);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_s(tex_t *tex, float *clip, float x, float y,
    float z, float sx, float sy)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, _sb_col_white);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_c(tex_t *tex, float *clip, float x, float y,
    float z, uint8 *col)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, col);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_r(tex_t *tex, float *clip, float x, float y,
    float z, float angle, float origin_x, float origin_y)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    float ox = origin_x + x;
    float oy = origin_y + y;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, _sb_col_white);
    SB_ROTATE_SPRITE(_sb.mapped_vbo, _sb.num_floats, angle, ox, oy);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_f(tex_t *tex, float *clip, float x, float y,
    float z, enum sb_flip_t flip)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    SB_WRITE_FLIPPED_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x,
        y, z, w, h, _sb_col_white, flip);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_sc(tex_t *tex, float *clip, float x, float y,
    float z, float sx, float sy, uint8 *col)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, col);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_sr(tex_t *tex, float *clip, float x, float y,
    float z, float sx, float sy, float angle, float origin_x, float origin_y)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    float ox = origin_x + x;
    float oy = origin_y + y;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, _sb_col_white);
    SB_ROTATE_SPRITE(_sb.mapped_vbo, _sb.num_floats, angle, ox, oy);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_sf(tex_t *tex, float *clip, float x, float y,
    float z, float sx, float sy, enum sb_flip_t flip)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    SB_WRITE_FLIPPED_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip,
        x, y, z, w, h, _sb_col_white, flip);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_cr(tex_t *tex, float *clip, float x, float y,
    float z, uint8 *col, float angle, float origin_x, float origin_y)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    float ox = origin_x + x;
    float oy = origin_y + y;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, col);
    SB_ROTATE_SPRITE(_sb.mapped_vbo, _sb.num_floats, angle, ox, oy);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_cf(tex_t *tex, float *clip, float x, float y,
    float z, uint8 *col, enum sb_flip_t flip)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    SB_WRITE_FLIPPED_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x,
        y, z, w, h, col, flip);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_scr(tex_t *tex, float *clip, float x, float y,
    float z, float sx, float sy, uint8 *col, float angle, float origin_x,
    float origin_y)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    float ox = origin_x + x;
    float oy = origin_y + y;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, col);
    SB_ROTATE_SPRITE(_sb.mapped_vbo, _sb.num_floats, angle, ox, oy);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_scf(tex_t *tex, float *clip, float x, float y,
    float z, float sx, float sy, uint8 *col, enum sb_flip_t flip)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    SB_WRITE_FLIPPED_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip,
        x, y, z, w, h, col, flip);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_scrf(tex_t *tex, float *clip, float x, float y,
    float z, float sx, float sy, uint8 *col, float angle, float origin_x,
    float origin_y, enum sb_flip_t flip)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    float ox = origin_x + x;
    float oy = origin_y + y;
    SB_WRITE_FLIPPED_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x,
        y, z, w, h, col, flip);
    SB_ROTATE_SPRITE(_sb.mapped_vbo, _sb.num_floats, angle, ox, oy);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_text(gui_font_t *sf, const char *txt, float x, float y,
    float z)
{
    if (!txt[0])
        return;
    SB_CHECK_SPACE_FOR_ONE();
    tex_t *tex = sf->tex;
    gui_glyph_t *g;
    float w, h, rx, ry;
    float px = x;
    float py = y;
    sb_cmd_t *cmd;
    SB_PREP_SPRITE_CMD_FOR_TEXT(cmd, tex);
    for (const char *c = txt; *c; ++c)
    {
        SB_CHECK_SPACE_FOR_ONE();
        if (*c == '\n')
        {
            px = x;
            py += sf->vertical_advance;
            continue;
        }
        g   = GUI_GLYPH(sf, (int)*c);
        w   = g->clip[2] - g->clip[0];
        h   = g->clip[3] - g->clip[1];
        rx  = px;
        ry  = (py - h + g->y_offset);
        SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, g->clip, rx,
            ry, z, w, h, _sb_col_white);
        px += g->advance;
        ++cmd->num_repeats;
        _sb.num_floats += SB_NUM_FLOATS_PER_QUAD;
    }
}

void
sb_text_sc(gui_font_t *sf, const char *txt, float x, float y,
    float z, float sx, float sy, uint8 *col)
{
    if (!txt[0])
        return;
    SB_CHECK_SPACE_FOR_ONE();
    tex_t *tex = sf->tex;
    gui_glyph_t *g;
    float w, h, rx, ry;
    float px = x;
    float py = y;
    sb_cmd_t *cmd;
    SB_PREP_SPRITE_CMD_FOR_TEXT(cmd, tex);
    for (const char *c = txt; *c; ++c)
    {
        SB_CHECK_SPACE_FOR_ONE();
        if (*c == '\n')
        {
            px = x;
            py += (sf->vertical_advance * sy);
            continue;
        }
        g   = GUI_GLYPH(sf, (int)*c);
        w   = (g->clip[2] - g->clip[0]) * sx;
        h   = (g->clip[3] - g->clip[1]) * sy;
        rx  = px;
        ry  = (py - h + g->y_offset);
        SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, g->clip, rx,
            ry, z, w, h, col);
        px += (g->advance * sx);
        ++cmd->num_repeats;
        _sb.num_floats += SB_NUM_FLOATS_PER_QUAD;
    }
}

void
sb_text_wrap(gui_font_t *sf, const char *txt, float x, float y,
    float z, float wrap)
{
    if (!txt[0])
        return;
    SB_CHECK_SPACE_FOR_ONE();
    tex_t *tex = sf->tex;
    gui_glyph_t *g;
    float w, h, rx, ry;
    float px    = x;
    float py    = y;
    float max_w = x + wrap;
    sb_cmd_t *cmd;
    SB_PREP_SPRITE_CMD_FOR_TEXT(cmd, tex);
    for (const char *c = txt; *c; ++c)
    {
        SB_CHECK_SPACE_FOR_ONE();
        if (*c == '\n')
        {
            px = x;
            py += sf->vertical_advance;
            continue;
        }
        g   = GUI_GLYPH(sf, (int)*c);
        w   = g->clip[2] - g->clip[0];
        h   = g->clip[3] - g->clip[1];
        if (c != txt && px + g->advance > max_w )
        {
            px = x;
            py += sf->vertical_advance;
        }
        rx  = px;
        ry  = (py - h + g->y_offset);
        SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, g->clip, rx,
            ry, z, w, h, _sb_col_white);
        px += g->advance;
        ++cmd->num_repeats;
        _sb.num_floats += SB_NUM_FLOATS_PER_QUAD;
    }
}

void
sb_text_wrap_s(gui_font_t *sf, const char *txt, float x, float y, float z,
    float wrap, float sx, float sy)
{
    if (!txt[0])
        return;
    SB_CHECK_SPACE_FOR_ONE(); \
    tex_t *tex = sf->tex;
    sb_cmd_t *cmd;
    SB_PREP_SPRITE_CMD_FOR_TEXT(cmd, tex);
    SB_WRITE_WRAPPED_STRING_VERTS_S(_sb, sf, txt, x, y, z, wrap, sx, sy,
        _sb_col_white);
}

void
sb_text_wrap_sc(gui_font_t *sf, const char *txt, float x,
    float y, float z, float wrap, float sx, float sy, uint8 *col)
{
    if (!txt[0])
        return;
    SB_CHECK_SPACE_FOR_ONE(); \
    tex_t *tex = sf->tex;
    sb_cmd_t *cmd;
    SB_PREP_SPRITE_CMD_FOR_TEXT(cmd, tex);
    SB_WRITE_WRAPPED_STRING_VERTS_S(_sb, sf, txt, x, y, z, wrap, sx, sy, col);
}

sb_write_context_t
sb_reserve_sprites(tex_t *tex, int num)
{
    sb_write_context_t context = {0};
    SB_CHECK_SPACE_FOR_NUM(num, context);
    context.first_index = _sb.num_floats;
    context.tex         = *tex;
    SB_WRITE_SPRITE_REPEAT_CMD(tex, num);
    return context;
}

void
sb_write_sprite(sb_write_context_t *context, int index, tex_t *tex, float *clip,
    float x, float y, float z)
{
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    SB_WRITE_SPRITE_VERTS(&context->tex, _sb.mapped_vbo,
        context->first_index + index, clip, x, y, z, w, h, _sb_col_white);
}

int
sb_end(void)
{
    assert(_sb.began);
    _sb.began = 0;
#if SB_USE_MAPPED_VBOS
    glUnmapBuffer(GL_ARRAY_BUFFER);
#else
    gl_bind_vbo(_sb.vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, _sb.num_floats * sizeof(GLfloat),
        _sb.mapped_vbo);
#endif
    gl_bind_ebo(_sb.ebo);
    _bind_sb_vao();
    float *cs = _sb.coord_space;
    GLfloat projection[4][4];
    gl_orthographic_projection_from_coordinate_space(cs, projection);
    gl_use_program(_sb.shader);
    gl_active_texture(0);
    glUniformMatrix4fv(glGetUniformLocation(_sb.shader, "projection"),
        1, GL_FALSE, &projection[0][0]);
    GLsizei index_size;
    switch (_sb.index_type)
    {
        case GL_UNSIGNED_BYTE:  index_size = sizeof(GLubyte);   break;
        case GL_UNSIGNED_SHORT: index_size = sizeof(GLushort);  break;
        case GL_UNSIGNED_INT:   index_size = sizeof(GLuint);    break;
        default: assert(0); break;
    }
    sb_cmd_t            *cmd;
    size_t              offset      = 0;
    const GLenum        index_type  = _sb.index_type;
    int                 num_cmds    = _sb.num_cmds;
    for (int i = 0; i < num_cmds; ++i)
    {
        cmd = &_sb.cmds[i];
        gl_bind_tex2d_if_not_bound(cmd->tex);
        glDrawElements(GL_TRIANGLES, cmd->num_repeats * 6, index_type,
            (const GLvoid*)offset);
        offset += cmd->num_repeats * 6 * index_size;
    }
    /* Orphan the buffer */
    glBufferData(GL_ARRAY_BUFFER, _sb.max_floats * sizeof(GLfloat), 0,
        SB_BUFFER_USAGE);
    gl_bind_vbo(0);
    gl_bind_ebo(0);
    return 0;
}

uint32
gl_shader_from_strs(const char *vtx_code, const char *frg_code,
    char *err_log, int err_log_len)
{
    if (!vtx_code || !frg_code)
        return 0;
    GLuint vtx_sh = glCreateShader(GL_VERTEX_SHADER);
    GLuint frg_sh = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(vtx_sh, 1, &vtx_code, 0);
    glShaderSource(frg_sh, 1, &frg_code, 0);
    glCompileShader(vtx_sh);
    glCompileShader(frg_sh);
    GLint success;
    glGetShaderiv(vtx_sh, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        if (err_log)
            glGetShaderInfoLog(vtx_sh, err_log_len, 0, err_log);
        return 0;
    }
    glGetShaderiv(frg_sh, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        if (err_log)
            glGetShaderInfoLog(frg_sh, err_log_len, 0, err_log);
        return 0;
    }
    GLuint prog = glCreateProgram();
    glAttachShader(prog, vtx_sh);
    glAttachShader(prog, frg_sh);
    glBindAttribLocation(prog, SHADER_ATTR_LOC_POS, "position");
    glBindAttribLocation(prog, SHADER_ATTR_LOC_UV,  "in_uv");
    glBindAttribLocation(prog, SHADER_ATTR_LOC_COL, "in_color");
    glLinkProgram(prog);
    glDeleteShader(vtx_sh);
    glDeleteShader(frg_sh);
    glGetProgramiv(prog, GL_LINK_STATUS, &success);
    if (!success)
    {
        if (err_log) glGetProgramInfoLog(prog, err_log_len, 0, err_log);
        return 0;
    }
    return prog;
}

uint32
gl_shader_from_files(const char *vert_path, const char *frag_path,
    char *err_log, int err_log_len)
{
    dchar   *vert   = load_text_file_to_dstr(vert_path);
    dchar   *frag   = load_text_file_to_dstr(frag_path);
    GLuint  ret     = gl_shader_from_strs(vert, frag, err_log, err_log_len);
    free(vert);
    free(frag);
    return ret;
}

void
gl_orthographic_projection_from_coordinate_space(float *coordinate_space,
    float ret_projection[4][4])
{
    float *cs = coordinate_space;
    GLfloat projection[4][4] =
    {
        {2.0f / (cs[2] - cs[0]), 0.0f, 0.0f, 0.0f},
        {0.0f, 2.0f / (cs[1] - cs[3]), 0.0f, 0.0f},
        {0.0f, 0.0f, -1.0f, 0.0f},
        {- (cs[2] + cs[0]) / (cs[2] - cs[0]),
         - (cs[1] + cs[3]) / (cs[1] - cs[3]),
         0.0f, 1.0f}
    };
    memcpy(ret_projection, projection, sizeof(projection));
}

static inline GLenum
_img_pxf_to_gl_enum(enum img_pxf_t pxf)
{
    switch (pxf)
    {
    case IMG_RGB:
        return GL_RGB;
    case IMG_RGBA:
        return GL_RGBA;
    case IMG_ALPHA:
        return GL_ALPHA;
    default:
        muta_assert(0);
        return 0;
    }
}

static inline void
_bind_sb_vao(void)
{
    glVertexAttribPointer(SHADER_ATTR_LOC_POS, 3, GL_FLOAT, GL_FALSE,
        6 * sizeof(GLfloat), 0);
    glVertexAttribPointer(SHADER_ATTR_LOC_UV, 2, GL_FLOAT, GL_FALSE,
        6 * sizeof(GLfloat), (const GLvoid*)(3 * sizeof(GLfloat)));
    glVertexAttribPointer(SHADER_ATTR_LOC_COL, 4, GL_UNSIGNED_BYTE, GL_TRUE,
        6 * sizeof(GLfloat), (const GLvoid*)(5 * sizeof(GLfloat)));
    glEnableVertexAttribArray(SHADER_ATTR_LOC_UV);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_POS);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_COL);
}
