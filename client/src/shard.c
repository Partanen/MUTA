#include <inttypes.h>
#include "shard.h"
#include "client.h"
#include "game_state.h"
#include "event.h"
#include "log.h"
#include "../../shared/crypt.h"
#include "../../shared/net.h"
#include "../../shared/common_defs.h"
#include "../../shared/client_packets.h"
#include "../../shared/account_rules.h"

#define OUT_BUF_SZ              MUTA_MTU
#define IN_BUF_SZ               MUTA_MTU
#define KEEP_ALIVE_FREQUENCY    (5.0f)

static thread_t         _thread;
static socket_t         _socket = KSYS_INVALID_SOCKET;
static int              _status = SHARD_STATUS_DISCONNECTED;
static int32            _running; /* Atomic */
static cryptchan_t      _cryptchan;
static addr_t           _address;
static uint8            _auth_token[AUTH_TOKEN_SZ];
static char             _account_name[MAX_ACC_NAME_LEN + 1];
static char             _shard_name[MAX_SHARD_NAME_LEN + 1];
static int              _last_error;
static list_character_t _list_characters[MAX_CHARACTERS_PER_ACC];
static uint32           _num_list_characters;
static int              _character_creation_error;
static int              _enter_world_error;
static double           _keep_alive_timer;

static struct
{
    uint8   memory[IN_BUF_SZ];
    int     num_bytes;
} _in_buf;

static struct
{
    uint8   memory[OUT_BUF_SZ];
    int     num_bytes;
} _out_buf;

static void
_send_disconnect_event(int error);

static void
_shard_disconnect_internal(void);

static bbuf_t
_send_msg(int num_bytes);

static inline bbuf_t
_send_const_encrypted_msg(int num_bytes);

static inline bbuf_t
_send_var_encrypted_msg(int num_bytes);

static thread_ret_t
_main(void *args);

static int
_read_packet(void);

static int
_handle_svmsg_pub_key(svmsg_pub_key_t *s);

static int
_handle_svmsg_stream_header(svmsg_stream_header_t *s);

static int
_handle_svmsg_character_list(svmsg_character_list_t *s);

static int
_handle_svmsg_create_character_fail(svmsg_create_character_fail_t *s);

static int
_handle_svmsg_new_character_created(svmsg_new_character_created_t *s);

static int
_handle_svmsg_enter_world_fail(svmsg_enter_world_fail_t *s);

static int
_handle_svmsg_enter_world_start(void);

static int
_handle_svmsg_enter_world_end(svmsg_enter_world_end_t *s);

int
shard_init(void)
{
    int ret = 0;
    if (thread_init(&_thread))
        {ret = 1; goto fail;}
    return ret;
    fail:
        return ret;
}

void
shard_destroy(void)
{
}

int
shard_connect(addr_t *address, uint8 *auth_token, const char *account_name,
    const char *shard_name)
{
    int ret = 0;
    muta_assert(!_running);
    if ((_socket = net_tcp_ipv4_sock()) == KSYS_INVALID_SOCKET)
        {ret = 1; goto fail;}
    if (make_socket_non_block(_socket))
        {ret = 2; goto fail;}
    if (net_disable_nagle(_socket))
        {ret = 3; goto fail;}
    cryptchan_clear(&_cryptchan);
    memcpy(_auth_token, auth_token, AUTH_TOKEN_SZ);
    strcpy(_account_name, account_name);
    strcpy(_shard_name, shard_name);
    _num_list_characters        = 0;
    _out_buf.num_bytes          = 0;
    _in_buf.num_bytes           = 0;
    _address                    = *address;
    _last_error                 = SHARD_ERROR_NONE;
    _character_creation_error   = 0;
    _enter_world_error          = 0;
    _running                    = 1;
    if (thread_create(&_thread, _main, 0))
        {ret = 4; goto fail;}
    _keep_alive_timer   = 0;
    _status             = SHARD_STATUS_CONNECTING;
    return ret;
    fail:
        close_socket(_socket);
        _socket     = KSYS_INVALID_SOCKET;
        _status     = SHARD_STATUS_DISCONNECTED;
        _running    = 0;
        _last_error = SHARD_ERROR_SYSTEM;
        return ret;
}

void
shard_disconnect(void)
{
    if (!_running)
        return;
    _shard_disconnect_internal();
}

void
shard_disconnect_with_error(int error)
{
    if (!_running)
        return;
    _last_error = error;
    _shard_disconnect_internal();
}

int
shard_get_last_error(void)
    {return _last_error;}

int
shard_get_character_creation_error(void)
    {return _character_creation_error;}

int
shard_get_enter_world_error(void)
    {return _enter_world_error;}

const char *
shard_error_to_string(int error)
{
    switch (error)
    {
    case SHARD_ERROR_NONE:
        return "No error.";
    case SHARD_ERROR_UNABLE_TO_CONNECT:
        return "Unable to connect.";
    case SHARD_ERROR_SYSTEM:
        return "System error.";
    case SHARD_ERROR_SERVER_CLOSED_CONNECTION:
        return "Server closed connection.";
    case SHARD_ERROR_BAD_MSG:
        return "Received invalid message. Is the client up to date?";
    case SHARD_ERROR_SEND_FAILED:
        return "Failed to send message.";
    default:
        return "Unknown error.";
    }
}

bbuf_t
shard_send(int num_bytes)
{
    bbuf_t ret = _send_msg(num_bytes);
    if (!ret.max_bytes)
        shard_disconnect_with_error(SHARD_ERROR_SEND_FAILED);
    _keep_alive_timer = 0;
    return ret;
}

bbuf_t
shard_send_const_encrypted(int num_bytes)
{
    bbuf_t ret = _send_const_encrypted_msg(num_bytes);
    if (!ret.max_bytes)
        shard_disconnect_with_error(SHARD_ERROR_SEND_FAILED);
    _keep_alive_timer = 0;
    return ret;
}

bbuf_t
shard_send_var_encrypted(int num_bytes)
{
    bbuf_t ret = _send_var_encrypted_msg(num_bytes);
    if (!ret.max_bytes)
        shard_disconnect_with_error(SHARD_ERROR_SEND_FAILED);
    _keep_alive_timer = 0;
    return ret;
}

int
shard_get_status(void)
    {return _status;}

void
shard_flush(double dt)
{
    if (_status < SHARD_STATUS_HANDSHAKING)
        return;
    _keep_alive_timer += dt;
    if (_keep_alive_timer >= KEEP_ALIVE_FREQUENCY)
    {
        _keep_alive_timer = 0;
        bbuf_t bb = shard_send(0);
        if (bb.max_bytes)
            clmsg_keep_alive_write(&bb);
    }
    if (cl_flush_buf(_socket, _out_buf.memory, &_out_buf.num_bytes))
        shard_disconnect_with_error(SHARD_ERROR_SEND_FAILED);
}

uint32
shard_get_list_characters(list_character_t *ret_characters, uint32 max)
{
    uint32 num_characters = MIN(_num_list_characters, max);
    for (uint32 i = 0; i < num_characters; ++i)
        ret_characters[i] = _list_characters[i];
    return num_characters;
}

int
shard_select_character(uint32 index)
{
    if (_status != SHARD_STATUS_CONNECTED)
        return 1;
    if (index >= _num_list_characters)
        return 2;
    clmsg_enter_world_t s = {.character_id = _list_characters[index].id};
    LOG_DEBUG("Attempting to log in with character. Name: %s, id: %" PRIu64 ".",
        _list_characters[index].name, _list_characters[index].id);
    bbuf_t bb = _send_msg(CLMSG_ENTER_WORLD_SZ);
    if (!bb.max_bytes)
    {
        shard_disconnect_with_error(SHARD_ERROR_SEND_FAILED);
        return 1;
    }
    clmsg_enter_world_write(&bb, &s);
    return 0;
}

int
shard_create_character(list_character_t *character)
{
    clmsg_create_character_t s;
    s.name.len  = character->name_len;
    memcpy(s.name.data, character->name, s.name.len);
    s.race      = (uint8)character->race;
    s.sex       = (uint8)character->sex;
    byte_buf_t bb = _send_msg(clmsg_create_character_compute_sz(&s));
    if (!bb.max_bytes)
        return 1;
    clmsg_create_character_write(&bb, &s);
    _character_creation_error = 0;
    return 0;
}

void
shard_on_connected(void)
{
    if (!_running)
        return;
    clmsg_pub_key_t k_msg;
    if (cryptchan_init(&_cryptchan, k_msg.key))
    {
        DEBUG_PRINTFF("Cryptchan init failed on shard connected!\n");
        _last_error = SHARD_ERROR_SYSTEM;
        _shard_disconnect_internal();
        return;
    }
    bbuf_t bb = shard_send(CLMSG_PUB_KEY_SZ);
    if (!bb.max_bytes)
        return;
    int r = clmsg_pub_key_write(&bb, &k_msg);
    muta_assert(!r);
    _status = SHARD_STATUS_HANDSHAKING;
}

void
shard_read(cl_read_event_t *event)
{
    if (!_running)
        return;
    if (cl_read(event, _read_packet, _in_buf.memory, &_in_buf.num_bytes,
        IN_BUF_SZ, &_last_error, SHARD_ERROR_SERVER_CLOSED_CONNECTION,
        SHARD_ERROR_BAD_MSG))
        _shard_disconnect_internal();
}

const char *
shard_get_name(void)
    {return _shard_name;}

static void
_send_disconnect_event(int error)
{
    cl_event_t cl_event;
    cl_event.type = CL_EVENT_DISCONNECT_SHARD;
    cl_event.disconnect_shard.error = error;
    cl_push_event(&cl_event);
}

static void
_shard_disconnect_internal(void)
{
    DEBUG_PRINTF("Disconnecting from shard, error: %s\n",
        shard_error_to_string(_last_error));
    interlocked_decrement_int32(&_running);
    net_close_blocking_sock(_socket);
    thread_join(&_thread);
    _num_list_characters    = 0;
    _out_buf.num_bytes      = 0;
    _in_buf.num_bytes       = 0;
    _socket                 = KSYS_INVALID_SOCKET;
    _status                 = SHARD_STATUS_DISCONNECTED;
    _shard_name[0]          = 0;
}

static bbuf_t
_send_msg(int num_bytes)
{
    return cl_send(_socket, _out_buf.memory, &_out_buf.num_bytes, OUT_BUF_SZ,
        num_bytes);
}

static inline bbuf_t
_send_const_encrypted_msg(int num_bytes)
{
    return cl_send(_socket, _out_buf.memory, &_out_buf.num_bytes, OUT_BUF_SZ,
        sizeof(msg_sz_t) + num_bytes);
}

static bbuf_t
_send_var_encrypted_msg(int num_bytes)
{
    return cl_send(_socket, _out_buf.memory, &_out_buf.num_bytes, OUT_BUF_SZ,
        sizeof(msg_sz_t) + CRYPT_MSG_ADDITIONAL_BYTES + num_bytes);
}

static thread_ret_t
_main(void *args)
{
    (void)args;
    uint8           buf[MUTA_MTU];
    fd_set          socket_set;
    struct timeval  wait_tv;
    cl_event_t      cl_event;
    /*-- Connect to shard (non-blocking) --*/
    net_connect(_socket, _address);
    for (;;)
    {
        if (!interlocked_compare_exchange_int32(&_running, -1, -1))
            return 0;
        FD_ZERO(&socket_set);
        FD_SET(_socket, &socket_set);
        wait_tv.tv_sec  = 0;
        wait_tv.tv_usec = 500000;
        select((int)_socket + 1, 0, &socket_set, 0, &wait_tv);
        if (!interlocked_compare_exchange_int32(&_running, -1, -1))
            return 0;
        int result = cl_check_blocking_socket_connect(_socket, &socket_set);
        if (!result)
            break;
        if (result > 0) /* Timeout */
            continue;
        if (result < 0)
        {
            DEBUG_PRINTFF("Failed to connect to shard.");
            _send_disconnect_event(SHARD_ERROR_UNABLE_TO_CONNECT);
            return 0;
        }
    }
    cl_event.type = CL_EVENT_SHARD_CONNECTED;
    cl_push_event(&cl_event);
    /*-- Talk to the shard --*/
    for (;;)
    {
        if (!interlocked_compare_exchange_int32(&_running, -1, -1))
            return 0;
        FD_ZERO(&socket_set);
        FD_SET(_socket, &socket_set);
        wait_tv.tv_sec  = 0;
        wait_tv.tv_usec = 500000;
        select((int)_socket + 1, &socket_set, 0, 0, &wait_tv);
        if (!interlocked_compare_exchange_int32(&_running, -1, -1))
            return 0;
        if (!FD_ISSET(_socket, &socket_set))
            continue;
        int num_bytes = net_recv(_socket, buf, sizeof(buf));
        cl_event.type           = CL_EVENT_READ_SHARD;
        cl_event.read.num_bytes = num_bytes;
        if (num_bytes > 0)
        {
            cl_event.read.memory = cl_malloc(num_bytes);
            memcpy(cl_event.read.memory, buf, num_bytes);
        }
        cl_push_event(&cl_event);
        if (num_bytes <= 0)
            return 0;
    }
    return 0;
}

static int
_read_packet(void)
{
    bbuf_t  bb          = BBUF_INITIALIZER(_in_buf.memory, _in_buf.num_bytes);
    int     dc          = 0;
    bool32  incomplete  = 0;
    svmsg_t msg_type;
    while (BBUF_FREE_SPACE(&bb) >= sizeof(svmsg_t))
    {
        BBUF_READ(&bb, &msg_type);
        switch (msg_type)
        {
        case SVMSG_PUB_KEY:
        {
            svmsg_pub_key_t s;
            incomplete = svmsg_pub_key_read(&bb, &s);
            if (!incomplete)
                dc = _handle_svmsg_pub_key(&s);
        }
            break;
        case SVMSG_STREAM_HEADER:
        {
            svmsg_stream_header_t s;
            incomplete = svmsg_stream_header_read(&bb, &s);
            if (!incomplete)
                dc = _handle_svmsg_stream_header(&s);
        }
            break;
        case SVMSG_CHARACTER_LIST:
        {
            svmsg_character_list_t s;
            incomplete = svmsg_character_list_read(&bb, &s);
            if (!incomplete)
                dc = _handle_svmsg_character_list(&s);
        }
            break;
        case SVMSG_CREATE_CHARACTER_FAIL:
        {
            svmsg_create_character_fail_t s;
            incomplete = svmsg_create_character_fail_read(&bb, &s);
            if (!incomplete)
                dc = _handle_svmsg_create_character_fail(&s);
        }
            break;
        case SVMSG_NEW_CHARACTER_CREATED:
        {
            svmsg_new_character_created_t s;
            incomplete = svmsg_new_character_created_read(&bb, &s);
            if (!incomplete)
                dc = _handle_svmsg_new_character_created(&s);
        }
            break;
        case SVMSG_ENTER_WORLD_FAIL:
        {
            svmsg_enter_world_fail_t s;
            incomplete = svmsg_enter_world_fail_read(&bb, &_cryptchan, &s);
            if (!incomplete)
                dc = _handle_svmsg_enter_world_fail(&s);
        }
            break;
        case SVMSG_ENTER_WORLD_START:
            dc = _handle_svmsg_enter_world_start();
            break;
        case SVMSG_ENTER_WORLD_END:
        {
            svmsg_enter_world_end_t s;
            incomplete = svmsg_enter_world_end_read(&bb, &_cryptchan, &s);
            if (!incomplete)
                dc = _handle_svmsg_enter_world_end(&s);
        }
            break;
        default:
            gs_read_packet(msg_type, &bb, &incomplete, &dc);
        }
        if (dc || incomplete < 0)
        {
            LOG_ERR("Bad message: msg_type %u, dc %d, incomplete %d.",
                msg_type, dc, incomplete);
            muta_assert(0);
            if (gs_begin_started())
                gs_end_session();
            return 1;
        }
        if (incomplete)
        {
            LOG_DEBUG("Received incomplete message...");
            bb.num_bytes -= sizeof(svmsg_t);
            break;
        }
    }
    int free_space = BBUF_FREE_SPACE(&bb);
    memmove(_in_buf.memory, _in_buf.memory + IN_BUF_SZ - free_space,
        BBUF_FREE_SPACE(&bb));
    _in_buf.num_bytes = free_space;
    return 0;
}

static int
_handle_svmsg_pub_key(svmsg_pub_key_t *s)
{
    clmsg_stream_header_t h_msg;
    int rr;
    if ((rr = cryptchan_cl_store_pub_key(&_cryptchan, s->key, h_msg.header)))
    {
        DEBUG_PRINTF("rr: %d\n", rr);
        return 1;
    }
    bbuf_t bb = _send_msg(CLMSG_STREAM_HEADER_SZ);
    if (!bb.max_bytes)
        return 2;
    int r = clmsg_stream_header_write(&bb, &h_msg);
    muta_assert(!r);
    return 0;
}

static int
_handle_svmsg_stream_header(svmsg_stream_header_t *s)
{
    if (cryptchan_store_stream_header(&_cryptchan, s->header))
        return 1;
    if (!cryptchan_is_encrypted(&_cryptchan))
        return 2;
    /*-- Send auth proof --*/
    clmsg_auth_proof_t p_msg;
    p_msg.account_name.len = (uint8)strlen(_account_name);
    memcpy(p_msg.account_name.data, _account_name, p_msg.account_name.len);
    p_msg.shard_name.len = (uint8)strlen(_shard_name);
    memcpy(p_msg.shard_name.data, _shard_name, p_msg.shard_name.len);
    memcpy(p_msg.token, _auth_token, AUTH_TOKEN_SZ);
    bbuf_t bb = _send_var_encrypted_msg(clmsg_auth_proof_compute_sz(&p_msg));
    if (!bb.max_bytes)
        return 5;
    clmsg_auth_proof_write(&bb, &_cryptchan, &p_msg);
    DEBUG_PRINTF("Sent auth proof to shard.\n");
    return 0;
}

static int
_handle_svmsg_character_list(svmsg_character_list_t *s)
{
    if (_status != SHARD_STATUS_HANDSHAKING)
        return 1;
    for (uint32 i = 0; i < s->characters.len; ++i)
    {
        svmsg_character_list_item_t *li = &s->characters.data[i];
        list_character_t            *lc = &_list_characters[i];
        if (ar_check_player_character_name(li->name.data, li->name.len))
            return 2;
        memcpy(lc->name, li->name.data, li->name.len);
        lc->name[li->name.len]  = 0;
        lc->name_len            = li->name.len;
        lc->id                  = li->id;
        lc->race                = li->race;
        lc->sex                 = li->sex;
    }
    _num_list_characters    = s->characters.len;
    _status                 = SHARD_STATUS_CONNECTED;
    return 0;
}

static int
_handle_svmsg_create_character_fail(svmsg_create_character_fail_t *s)
{
    _character_creation_error = s->reason;
    return 0;
}

static int
_handle_svmsg_new_character_created(svmsg_new_character_created_t *s)
{
    if (_num_list_characters == MAX_CHARACTERS_PER_ACC)
        return 1;
    list_character_t *lc = &_list_characters[_num_list_characters++];
    lc->id = s->id;
    memcpy(lc->name, s->name.data, s->name.len);
    lc->name[s->name.len]       = 0;
    lc->name_len                = s->name.len;
    lc->race                    = s->race;
    lc->sex                     = s->sex;
    _character_creation_error   = 0;
    return 0;
}

static int
_handle_svmsg_enter_world_fail(svmsg_enter_world_fail_t *s)
{
    LOG_ERR("Character login failed, error %d.\n", (int)s->reason);
    _enter_world_error      = s->reason;
    event_t event =
    {
        .type                       = EVENT_ENTER_WORLD_FAILED,
        .enter_world_failed.reason  = s->reason
    };
    ev_post(&event);
    return 0;
}

static int
_handle_svmsg_enter_world_start(void)
{
    if (gs_begin_started())
    {
        LOG_ERR("A game has already been started.");
        return 1;
    }
    if (gs_in_session())
    {
        LOG_ERR("A game is already in session.");
        return 2;
    }
    gs_start_begin_session();
    return 0;
}

static int
_handle_svmsg_enter_world_end(svmsg_enter_world_end_t *s)
{
    if (!gs_begin_started())
        return 1;
    if (gs_in_session())
    {
        LOG_ERR("A game session is already going on.");
        return 2;
    }
    char name[sizeof(s->name.data) + 1];
    memcpy(name, s->name.data, s->name.len);
    name[s->name.len] = 0;
    muta_assert(sizeof(s->abilities.data[0]) == sizeof(ability_id_t));
    if (gs_end_begin_session(s->map_id, s->world_session_id, name, s->race,
        s->x, s->y, s->z, s->direction, s->move_speed, s->view_distance_h,
        s->view_distance_v, s->abilities.data, s->abilities.len,
        s->health_current, s->health_max, s->dead))
        return 3;
    return 0;
}
