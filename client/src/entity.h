#ifndef MUTA_CLIENT_ENTITY_H
#define MUTA_CLIENT_ENTITY_H

#include "component.h"
#include "player_entity.h"
#include "creature_entity.h"
#include "static_object_entity.h"
#include "dynamic_object_entity.h"
#include "../../shared/common_utils.h"

/* The maximum size if an entity's type data union. */
#define ENTITY_TYPE_DATA_SIZE \
    (((sizeof(player_entity_t) > sizeof(creature_entity_t) ? \
    sizeof(player_entity_t) : sizeof(creature_entity_t)) > \
    sizeof(static_object_entity_t) ? \
    (sizeof(player_entity_t) > sizeof(creature_entity_t) ? \
    sizeof(player_entity_t) : sizeof(creature_entity_t)) : \
    sizeof(static_object_entity_t)) > sizeof(dynamic_object_entity_t) ? \
    ((sizeof(player_entity_t) > sizeof(creature_entity_t) ? \
    sizeof(player_entity_t) : sizeof(creature_entity_t)) > \
    sizeof(static_object_entity_t) ? \
    (sizeof(player_entity_t) > sizeof(creature_entity_t) ? \
    sizeof(player_entity_t) : sizeof(creature_entity_t)) : \
    sizeof(static_object_entity_t)) : sizeof(dynamic_object_entity_t))

/* Forward declarations */
typedef struct world_t                  world_t;
typedef struct component_definition_t   component_definition_t;
typedef struct entity_event_t           entity_event_t;
enum entity_event;

/* Types defined here */
typedef struct entity_handle_t      entity_handle_t;
typedef struct entity_t             entity_t;
typedef struct entity_type_data_t   entity_type_data_t;
typedef struct entity_listen_t      entity_listen_t;
typedef struct entity_pool_t        entity_pool_t;

enum entity_type
{
    ENTITY_TYPE_UNKNOWN = 0,
    ENTITY_TYPE_PLAYER,
    ENTITY_TYPE_STATIC_OBJECT,
    ENTITY_TYPE_DYNAMIC_OBJECT,
    ENTITY_TYPE_CREATURE,
    NUM_ENTITY_TYPES
};

struct entity_handle_t
{
    enum entity_type type;
    union
    {
        player_runtime_id_t         player;
        creature_runtime_id_t       creature;
        dobj_runtime_id_t dynamic_object;
    };
};

struct entity_type_data_t
{
    enum entity_type type;
    union
    {
        player_entity_t         player;
        creature_entity_t       creature;
        static_object_entity_t  static_object;
        dynamic_object_entity_t dynamic_object;
        uint8                   unknown[ENTITY_TYPE_DATA_SIZE];
    };
    /* This union is initialized to 0 when entity_spawn() is called. */
};
/* Type-specific data for an entity. */

struct entity_listen_t
{
    int id; /* User-defined ID of this listener. */
    union /* User data. Use as seen fit or leave untouched. */
    {
        int     i;
        uint64  u64;
        uint32  u32;
        void    *ptr;
    };
};
/* Filled by the user when adding an event listener to an entity. The ID can be
 * used top remove the listener. The union can contain any data the user sees
 * fit. The structure is passed to any calls to the user's callback when events
 * fire. */

struct entity_pool_t
{
    fixed_pool(entity_t)    entities;
    uint32                  *reserved;
    obj_pool_t              event_listeners;
};
/* The entity pool is used internally */

int
entity_api_init(void);

void
entity_api_destroy(void);

entity_t *
entity_spawn(world_t *world, int direction, int *position);

void
entity_despawn(entity_t *entity);

bool32
entity_is_spawned(entity_t *entity);

world_t *
entity_get_world(entity_t *entity);

entity_type_data_t *
entity_get_type_data(entity_t *entity);

component_handle_t
entity_attach_component(entity_t *entity,
    component_definition_t *component_definition);
/* Returns INVALID_COMPONENT_HANDLE on failure. If entity already has the
 * component, the existing component will be returned. */

void
entity_detach_component(entity_t *entity,
    component_definition_t *component_definition_t);

component_handle_t
entity_get_component(entity_t *entity,
    component_definition_t *component_definition);

bool32
entity_has_component(entity_t *entity,
    component_definition_t *component_definition);

void
entity_set_direction(entity_t *entity, int direction);

int
entity_get_direction(entity_t *entity);

void
entity_set_position(entity_t *entity, int *position);

void
entity_get_position(entity_t *entity, int ret_position[3]);

component_handle_t *
entity_get_components(entity_t *entity);

void
entity_post_entity_event(entity_event_t *event);
/* First, this function calls an event handler based on the type of the entity,
 * for example player_handle_entity_event().
 * Second, it calls any entity event callbacks registered by components. */

void
entity_post_component_event(entity_t *entity,
    component_definition_t *component_definition, void *event);
/* Calls any callbacks registered for component events by component
 * definitions. */

void
entity_listen_to_entity_event(entity_t *entity, enum entity_event event,
    entity_listen_t *listen_data,
    void (*callback)(entity_event_t *, entity_listen_t *));

void
entity_stop_listening_to_entity_event(entity_t *entity, int id);

/* Entity pool functions, used internally: */

int
entity_pool_init(entity_pool_t *pool, uint32 max);
/* The pool is a fixed pool, max is the absolute maximum amount of entities. */

void
entity_pool_destroy(entity_pool_t *pool);

void
entity_pool_clear(entity_pool_t *pool);
/* Despawns all entities in the pool. */

const char *
entity_get_name(entity_t *entity);

entity_handle_t
entity_get_handle(entity_t *entity);

void
entity_get_common_entity_type_and_runtime_id(entity_t *entity,
    enum common_entity_type *ret_common_entity_type, uint32 *ret_runtime_id);
/* Convenience function for translating entity data to a format the server will
 * understand.
 * This function assumes the entity is valid for conversion into an entity type
 * the server is also aware of. */

void
entity_set_current_health(entity_t *entity, uint32 health_current);
/* Only safe to call for player or creature entities. */

void
entity_set_max_health(entity_t *entity, uint32 health_max);
/* Only safe to call for player or creature entities. */

#endif /* MUTA_CLIENT_ENTITY_H */
