#include "assets.h"
#include "world.h"
#include "log.h"

#define NUM_SPRITES 1
#define IDLE_TIME   0.15f

struct ae_set_timer_t
{
    float   time_passed;
    uint32  component;
};

struct ae_set_component_t
{
    entity_t                    *entity;
    as_ae_set_t                 *set;
    render_component_sprite_t   *sprites[NUM_SPRITES];
    as_ae_t                     *idle;
    as_ae_t                     *walk;
    uint32                      idle_timer_index;
    /* idle_timer_index
     * 0xFFFFFFFF if none. Used to track if the entity has been idling long
     * enough to swap to an idle animation. */
};

static int
_ae_set_system_init(world_t *world, uint32 num_components);

static void
_ae_set_system_destroy(world_t *world);

static void
_ae_set_system_update(world_t *world, float dt);

static component_handle_t
_ae_set_component_attach(entity_t *entity);

static void
_ae_set_component_detach(component_handle_t handle);

static void
_handle_ae_animator_component_event(entity_t *entity, component_handle_t handle,
    void *event_ptr);

static void
_handle_mobility_component_event(entity_t *entity, component_handle_t handle,
    void *event_ptr);

static component_event_interest_t _component_event_interests[] =
{
    {&ae_animator_component_definition, _handle_ae_animator_component_event},
    {&mobility_component_definition, _handle_mobility_component_event}
};

component_definition_t ae_set_component_definition =
{
    _ae_set_system_init,
    _ae_set_system_destroy,
    _ae_set_system_update,
    _ae_set_component_attach,
    _ae_set_component_detach,
    0,
    0,
    _component_event_interests,
    sizeof(_component_event_interests) / sizeof(component_event_interest_t)
};

void
ae_set_component_set_asset(component_handle_t handle, as_ae_set_t *set)
{
    ae_set_component_t *component = handle;
    if (component->set == set)
    {
        LOG_DEBUG("AE set was null.");
        return;
    }
    component->set = set;
    as_unclaim_ae(component->idle);
    as_unclaim_ae(component->walk);
    component->idle = 0;
    component->walk = 0;
    ae_animator_component_t *ae_animator_component = entity_get_component(
        component->entity, &ae_animator_component_definition);
    muta_assert(ae_animator_component);
    if (!set)
    {
        ae_animator_component_set_asset(ae_animator_component, 0);
        return;
    }
    DEBUG_PRINTFF("%u\n", set->idle_id);
    component->idle = as_claim_ae(set->idle_id, 1);
    component->walk = as_claim_ae(set->walk_id, 1);
    muta_assert(component->idle);
    muta_assert(component->walk);
    component_handle_t mobility_component = entity_get_component(
        component->entity, &mobility_component_definition);
    as_ae_t *asset;
    if (!mobility_component ||
        mobility_component_get_percentage_travelled(mobility_component) == 1.f)
        asset = component->idle;
    else
        asset = component->walk;
    ae_animator_component_set_asset(ae_animator_component, asset);
    ae_animator_component_set_play_mode(ae_animator_component,
        AE_ANIMATOR_COMPONENT_PLAY_LOOP);
}

static int
_ae_set_system_init(world_t *world, uint32 num_components)
{
    ae_set_system_t *system = &world->ae_set_system;
    fixed_pool_init(&system->components, num_components);
    system->idle_timers = emalloc(num_components * sizeof(ae_set_timer_t));
    return 0;
}

static void
_ae_set_system_destroy(world_t *world)
{
    fixed_pool_destroy(&world->ae_set_system.components);
    free(world->ae_set_system.idle_timers);
}

static void
_ae_set_system_update(world_t *world, float dt)
{
    ae_set_system_t *system     = &world->ae_set_system;
    uint32          num_timers  = system->num_idle_timers;
    ae_set_timer_t  *timers     = system->idle_timers;
    for (uint32 i = 0; i < num_timers; ++i)
    {
        loop_top: {}
        ae_set_timer_t *timer = &timers[i];
        timer->time_passed += dt;
        if (timer->time_passed < IDLE_TIME)
            continue;
        /*-- Idled long enough, remove from idler list --*/
        ae_set_component_t *component =
            &system->components.all[timer->component];
        *timer = timers[--num_timers];
        system->components.all[timer->component].idle_timer_index = i;
        component->idle_timer_index = 0xFFFFFFFF;
        component_handle_t ae_animator_component = entity_get_component(
            component->entity, &ae_animator_component_definition);
        ae_animator_component_set_asset(ae_animator_component, component->idle);
        ae_animator_component_set_play_mode(ae_animator_component,
            AE_ANIMATOR_COMPONENT_PLAY_LOOP);
        if (i < num_timers)
            goto loop_top;
    }
    system->num_idle_timers = num_timers;
}

static component_handle_t
_ae_set_component_attach(entity_t *entity)
{
    ae_set_system_t     *system     = &entity_get_world(entity)->ae_set_system;
    ae_set_component_t  *component  = fixed_pool_new(&system->components);
    memset(component, 0, sizeof(*component));
    component_handle_t render_component = entity_get_component(entity,
        &render_component_definition);
    muta_assert(render_component != INVALID_COMPONENT_HANDLE);
    component->entity           = entity;
    component->set              = 0;
    component->idle_timer_index = 0xFFFFFFFF;
    for (int i = 0; i < NUM_SPRITES; ++i)
        component->sprites[i] = render_component_new_sprite(render_component);
    return component;
}

static void
_ae_set_component_detach(component_handle_t handle)
{
    ae_set_component_t *component = handle;
    ae_set_system_t *system = &entity_get_world(
        component->entity)->ae_set_system;
    as_unclaim_ae(component->idle);
    as_unclaim_ae(component->walk);
    /*-- Delete timer if idling --*/
    if (component->idle_timer_index != 0xFFFFFFFF)
    {
        ae_set_timer_t *timer =
            &system->idle_timers[component->idle_timer_index];
        *timer = system->idle_timers[--system->num_idle_timers];
        if (system->num_idle_timers)
            system->components.all[timer->component].idle_timer_index =
                component->idle_timer_index;
    }
    for (int i = 0; i < NUM_SPRITES; ++i)
        render_component_sprite_free(component->sprites[i]);
    fixed_pool_free(&system->components, component);
}

static void
_handle_ae_animator_component_event(entity_t *entity, component_handle_t handle,
    void *event_ptr)
{
    ae_set_component_t *component  = entity_get_component(entity,
        &ae_set_component_definition);
    ae_animator_component_event_t *event = event_ptr;
    if (event->type != AE_ANIMATOR_COMPONENT_EVENT_NEW_FRAME)
        return;
    if (!event->new_frame.frame)
    {
        for (uint32 i = 0; i < NUM_SPRITES; ++i)
            if (component->sprites[i])
                render_component_sprite_set_texture(component->sprites[i], 0);
        return;
    }
    for (int i = 0; i < NUM_SPRITES; ++i)
    {
        render_component_sprite_set_texture(component->sprites[i],
            &event->new_frame.frame->layers[i].ta->tex);
        render_component_sprite_set_clip(component->sprites[i],
            event->new_frame.frame->layers[i].clip);
        render_component_sprite_set_offset(component->sprites[i],
            event->new_frame.frame->layers[i].ox + event->new_frame.frame->ox,
            event->new_frame.frame->layers[i].oy + event->new_frame.frame->oy);
    }
}

static void
_handle_mobility_component_event(entity_t *entity, component_handle_t handle,
    void *event_ptr)
{
    mobility_component_event_t  *event      = event_ptr;
    ae_set_component_t          *component  = entity_get_component(entity,
        &ae_set_component_definition);
    switch (event->type)
    {
    case MOBILITY_COMPONENT_EVENT_START_MOVE:
    {
        component_handle_t ae_animator_component = entity_get_component(
            entity, &ae_animator_component_definition);
        ae_animator_component_set_asset(ae_animator_component, component->walk);
        ae_animator_component_set_play_mode(ae_animator_component,
            AE_ANIMATOR_COMPONENT_PLAY_LOOP);
        if (component->idle_timer_index == 0xFFFFFFFF)
            break;
        ae_set_system_t *system =
            &entity_get_world(component->entity)->ae_set_system;
        muta_assert(system->num_idle_timers);
        system->idle_timers[component->idle_timer_index] =
            system->idle_timers[--system->num_idle_timers];
        system->components.all[system->idle_timers[
            component->idle_timer_index].component].idle_timer_index =
            component->idle_timer_index;
        component->idle_timer_index = 0xFFFFFFFF;
    }
        break;
    case MOBILITY_COMPONENT_EVENT_STOP_MOVE:
    {
        ae_set_system_t *system = &entity_get_world(
            component->entity)->ae_set_system;
        if (component->idle_timer_index == 0xFFFFFFFF)
            component->idle_timer_index = system->num_idle_timers++;
        else
        {
            DEBUG_PRINTFF("faulty index: %u (num_idle_timers: %u)\n",
                component->idle_timer_index, system->num_idle_timers);
            muta_assert(system->num_idle_timers);
        }
        ae_set_timer_t *timer =
            &system->idle_timers[component->idle_timer_index];
        timer->component   = fixed_pool_index(&system->components, component);
        timer->time_passed = 0.f;
    }
        break;
    }
}
