#ifndef MUTA_CLIENT_WORLD_H
#define MUTA_CLIENT_WORLD_H

#include "entity.h"
#include "mobility_component.h"
#include "ae_animator_component.h"
#include "ae_set_component.h"
#include "render_component.h"
#include "iso_sprite_component.h"
#include "name_plate_component.h"
#include "entity_event.h"
#include "../../shared/tile.h"
#include "../../shared/entities.h"
#include "../../shared/hashtable.h"
#include "../../shared/muta_map_format.h"

#define CHUNK_COMPUTE_TILE_INDEX(x, y, w) \
    (((x) % MAP_CHUNK_W) * (MAP_CHUNK_W * MAP_CHUNK_T) + \
    ((y) % MAP_CHUNK_W) * MAP_CHUNK_T + ((z) % MAP_CHUNK_T))

/* Forward declaration(s) */
typedef struct async_job_t async_job_t;

/* Types defined here */
typedef struct chunk_cache_t            chunk_cache_t;
typedef struct world_event_t            world_event_t;
typedef struct world_event_listener_t   world_event_listener_t;
typedef struct world_t                  world_t;
typedef void (*world_event_callback_t)(world_event_t *event, void *user_data);

enum world_event
{
    WORLD_EVENT_MAP_LOAD, /* Fired when a new map is loaded */
    WORLD_EVENT_CHUNK_LOAD_FINISHED,
    WORLD_EVENT_WILL_UNLOAD_CHUNK_OUTSIDE_CAMERA,
    /* When the world camera is moved, the will-unload-chunk-outside-camrea
     * event is fired before a chunk that used to be in view but no longer will
     * be is unloaded. */
    WORLD_EVENT_TILE_CHANGED,
    /* The tile changed event is fired when world_set_tile() is called, and the
     * new tile is not the same as the old tile at the given position. */
    NUM_WORLD_EVENTS
};

struct world_event_t
{
    int     type; /* One of enum world_event */
    world_t *world;
    union
    {
        struct
        {
            int             error;
            chunk_cache_t   *chunk_cache;
        } chunk_load_finished;
        struct
        {
            chunk_cache_t *chunk_cache;
        } will_unload_chunk_outside_camera;
        struct
        {
            tile_t  previous_tile;
            tile_t  new_tile;
            int     position[3];
        } tile_changed;
    };
};

struct chunk_cache_t
{
    world_t                     *world;
    int32                       x_in_chunks;
    int32                       y_in_chunks;
    entity_t                    **static_objects; /* darr */
    async_job_t                 *async_job; /* Null of not being loaded. */
    int                         load_result;
    tile_t                      tiles[MAP_CHUNK_SIZE_IN_TILES];
    muta_chunk_file_t           chunk_file;
};

struct world_t
{
    double                                      tick_accumulator;
    double                                      timestep;
    entity_pool_t                               entities;
    hashtable(player_runtime_id_t, entity_t*)   player_table;
    hashtable(creature_runtime_id_t, entity_t*) creature_table;
    hashtable(dobj_runtime_id_t, entity_t*)     dynamic_object_table;
    mobility_system_t                           mobility_system;
    ae_animator_system_t                        ae_animator_system;
    render_system_t                             render_system;
    ae_set_system_t                             ae_set_system;
    iso_sprite_system_t                         iso_sprite_system;
    name_plate_system_t                         name_plate_system;
    world_event_listener_t *event_listeners[NUM_WORLD_EVENTS];
    struct
    {
        uint32          id;
        muta_map_file_t file;
        bool32          loaded_any_caches;
        int             w, h;   /* width, height in chunks */
        int             tw, th; /* width, height in tiles */
        int             num_allocated_chunks;
        int             cam_x, cam_y;
        int             cam_tx, cam_ty;
        /* Camera coordinates determine which part of the map is loaded. */
        chunk_cache_t   caches[9];
        chunk_cache_t   *cache_ptrs[9];
        /* The tile data of the 9 chunks surrounding the player is loaded in and
         * out of these caches as the player moves on the map, starting from
         * cam_x, cam_y. None of these are ever null. */
    } map;
};
/* world_t is an instance of spatial simulation, analogous to a "scene". Each
 * entity or component belongs to a single world. */

int
world_api_init(void);
/* world_api_init()
 * Call in the program before using any other world api calls. Loads world data
 * definitions from disk. The world's address must stay the same until it is
 * destroyed. */

int
world_init(world_t *world, uint32 width_in_chunks, uint32 height_in_chunks,
    uint32 max_entities);
/* world_init()
 * Initialize a new world. max_entities is a hard limit on how many entities the
 * world may contain. width_in_chunks and height_in_chunks are predictive - they
 * will automatically change of a map of a different size is loaded. */

void
world_destroy(world_t *world);

int
world_load_map(world_t *world, uint32 map_id);
/* world_load_map() Load a map from disk. map_id is the one given in
 * muta-data/common/maps.mapdb */

int
world_load_map_from_path(world_t *world, const char *file_path);

bool32
world_set_camera_position(world_t *world, int chunk_x, int chunk_y);
/* world_set_camera_position()
 * Set the "camera position" of the currently loaded world, determining which
 * parts of the map area loaded. chunk_x and chunk_y are the chunk (not tile)
 * coordinates of the top left corner of the camera area.
 * There are nine loaded chunks at a time, each MAP_CHUNK_W * MAP_CHUNK_W *
 * MAP_CHUNK_T large.
 * The loaded chunks would look as below. In a typical scenario, the player will
 * be in the center chunk.
 * Return value is zero if the loaded chunks chaned, 0 otherwise.
 *
 * X ------->
 *
 * *--*--*--*  Y
 * |  |  |  |
 * *--*--*--*  |
 * |  |  |  |  |
 * *--*--*--*  |
 * |  |  |  |  |
 * *--*--*--*  v */

void
world_update(world_t *world, double delta);

entity_t *
world_get_player(world_t *world, player_runtime_id_t id);

entity_t *
world_get_creature(world_t *world, creature_runtime_id_t guid);

entity_t *
world_get_dynamic_object(world_t *world, dobj_runtime_id_t id);

entity_t *
world_get_entity(world_t *world, entity_handle_t handle);

void
world_listen_to_event(world_t *world, int event,
    world_event_callback_t callback, void *user_data);

void
world_stop_listening_to_event(world_t *world, int event,
    world_event_callback_t callback);

static inline tile_t
world_get_tile(world_t *world, int x, int y, int z);

bool32
world_is_loading(world_t *world);

int
world_set_tile(world_t *world, int x, int y, int z, tile_t tile);
/* Returns true on success and fires a WORLD_EVENT_TILE_CHANGED event if the
 * new tile is of a different type than the old one. Tile must be in the
 * currently loaded are of the world, otherwise the function will fail and
 * return non-zero.
 * Additionally, world_set_tile will fail if world_is_loading() is currently
 * true. */

static inline chunk_cache_t *
world_get_chunk_cache_of_tile(world_t *world, int x, int y);
/* This function wraps the numbers around, never returning null. */

static inline bool32
world_is_chunk_of_tile_loaded(world_t *world, int x, int y);

int
chunk_cache_save(chunk_cache_t *cache);
/* Save cache to disk. */

static inline tile_t
chunk_cache_get_tile(chunk_cache_t *cache, int x, int y, int z);
/* chunk_cache_get_tile()
 * Get a tile from a cache at world coordinates [x, y, z] (not relative to the
 * chunk's position, raw coords). The function does no bounds checking. */

static inline tile_t
world_get_tile(world_t *world, int x, int y, int z)
{
    if (x < 0 || x >= world->map.tw || y < 0 || y >= world->map.th ||
        z < 0 || z >= MAP_CHUNK_T)
        return 0;
    chunk_cache_t *cache = world_get_chunk_cache_of_tile(world, x, y);
    return chunk_cache_get_tile(cache, x, y, z);
}

static inline chunk_cache_t *
world_get_chunk_cache_of_tile(world_t *world, int x, int y)
{
    return world->map.cache_ptrs[
        (y / MAP_CHUNK_W - MIN(world->map.cam_y, y / MAP_CHUNK_W)) % 3 * 3 +
        ((x / MAP_CHUNK_W - MIN(world->map.cam_x, x / MAP_CHUNK_W)) % 3)];
}

static inline bool32
world_is_chunk_of_tile_loaded(world_t *world, int x, int y)
{
    int chunk_x = x / MAP_CHUNK_W;
    int chunk_y = y / MAP_CHUNK_W;
    if (chunk_x < world->map.cam_x || chunk_x >= world->map.cam_x + 3 ||
        chunk_y < world->map.cam_y || chunk_y >= world->map.cam_y + 3)
        return 0;
    return 1;
}

static inline tile_t
chunk_cache_get_tile(chunk_cache_t *cache, int x, int y, int z)
    {return cache->tiles[CHUNK_COMPUTE_TILE_INDEX(x, y, z)];}

#endif /* MUTA_CLIENT_WORLD_H */
