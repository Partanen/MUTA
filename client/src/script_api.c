#include "script_api.h"
#include "game_state.h"
#include "world.h"
#include "game_state_internal.h"

game_state_t *_gs;

void
script_set_game_state(game_state_t *gs)
    {_gs = gs;}

void
script_log_system_message(const char *msg)
    {gs_log_system_msg(msg);}

void
script_stop_charge(void)
{
    muta_assert(_gs->charge_bar.active);
    _gs->charge_bar.active = 0;
}

script_entity_t *
script_get_this_player_entity(void)
    {return _gs->player_entity;}

bool32
script_entity_is_this_player(script_entity_t *script_entity)
    {return (entity_t*)script_entity == _gs->player_entity;}

bool32
script_is_entity_moving(script_entity_t *entity)
{
    mobility_component_t *mc = entity_get_component(entity,
        &mobility_component_definition);
    if (!mc)
        return 0;
    return mobility_component_is_moving(mc);
}

void
script_get_entity_position(script_entity_t *entity, int ret_position[3])
    {entity_get_position(entity, ret_position);}

enum iso_dir
script_get_entity_direction(script_entity_t *entity)
    {return entity_get_direction(entity);}

void
script_compute_distance_vec(int position_a[3], int position_b[3], int ret[3])
{
    ret[0] = abs(position_a[0] - position_b[0]);
    ret[1] = abs(position_a[1] - position_b[1]);
    ret[2] = abs(position_a[2] - position_b[2]);
}
