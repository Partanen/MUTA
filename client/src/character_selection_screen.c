#include "character_selection_screen.h"
#include "character_creation_screen.h"
#include "main_menu_screen.h"
#include "game_screen.h"
#include "core.h"
#include "render.h"
#include "assets.h"
#include "game_state.h"
#include "shard.h"
#include "event.h"
#include "gui.h"

static void
character_select_screen_update(double dt);

static void
character_select_screen_open(void);

static void
character_select_screen_close(void);

static void
character_select_screen_text_input(const char *text);

static void
character_select_screen_keydown(int key, bool32 is_repeat);

screen_t character_select_screen =
{
    "Character Selection",
    0,
    0,
    character_select_screen_update,
    character_select_screen_open,
    character_select_screen_close,
    character_select_screen_text_input,
    character_select_screen_keydown,
    0,
    0,
    0,
    0,
    0
};

static uint32 _selected_character_index;
static bool32 _logging_in;
static bool32 _enter_world_error_window_open;
static uint32 _event_enter_world_failed_handle;

static void
_on_enter_world_failed(event_t *event, void *user_data);

static void
_draw_selected_character_window(list_character_t *cp, int x, int y);

static void
_draw_logging_in_window(void);

static void
_draw_enter_world_error_window(void);

void
character_select_screen_update(double dt)
{
    gl_viewport(0, 0, core_window_w(), core_window_h());
    gl_scissor(0, 0, core_window_w(), core_window_h());
    gl_color(0.0f, 0.0f, 0.0f, 1.0f);
    gl_clear(RB_CLEAR_COLOR_BIT);
    gui_font(as_get_default_font());
    gui_button_style(0);
    gui_win_style(0);
    gui_origin(GUI_TOP_CENTER);
    gui_text("Characters", 0, 0, 30);
    gui_origin(GUI_CENTER_CENTER);
    list_character_t characters[MAX_CHARACTERS_PER_ACC];
    uint32 num_characters = shard_get_list_characters(characters,
        MAX_CHARACTERS_PER_ACC);
    if (num_characters)
    {
        for (uint32 i = 0; i < num_characters; ++i)
            if (gui_button(characters[i].name, 0, i * 28, 96, 24, 0))
                _selected_character_index = i;
    } else
        gui_text("No characters created", 0, 0, 0);
    gui_origin(GUI_BOTTOM_LEFT);
    if (num_characters < MAX_CHARACTERS_PER_ACC &&
        gui_button("Create new character", 10, 10, 256, 32, 0))
        core_set_screen(&character_create_screen);
    gui_origin(GUI_BOTTOM_RIGHT);
    if (gui_button("Cancel##screen", 10, 10, 256, 32, 0))
    {
        shard_disconnect();
        core_set_screen(&main_menu_screen);
    }
    if (_selected_character_index < num_characters)
    {
        list_character_t *character = &characters[_selected_character_index];
        gui_origin(GUI_CENTER_RIGHT);
        _draw_selected_character_window(character, 0, 0);
        gui_origin(GUI_BOTTOM_CENTER);
        if (gui_button("Enter world", 0, 0, 64, 24, 0) && !_logging_in)
            if (!shard_select_character(_selected_character_index))
            {
                _logging_in                     = 1;
                _enter_world_error_window_open  = 0;
            }
    }
    if (_logging_in)
    {
        _draw_logging_in_window();
        if (gs_in_session())
            core_set_screen(&game_screen);
    } else if (_enter_world_error_window_open)
        _draw_enter_world_error_window();
    if (shard_get_status() != SHARD_STATUS_CONNECTED)
        core_set_screen(&main_menu_screen);
}

void
character_select_screen_open(void)
{
    _selected_character_index       = 0;
    _logging_in                     = 0;
    _enter_world_error_window_open  = 0;
    _event_enter_world_failed_handle = ev_add(EVENT_ENTER_WORLD_FAILED,
        _on_enter_world_failed, 0);
}

void
character_select_screen_close(void)
{
    ev_del(EVENT_ENTER_WORLD_FAILED, _event_enter_world_failed_handle);
}

void
character_select_screen_text_input(const char *text)
{
}

void
character_select_screen_keydown(int key, bool32 is_repeat)
{
}

static void
_on_enter_world_failed(event_t *event, void *user_data)
{
    (void)user_data;
    _enter_world_error_window_open  = 1;
    _logging_in                     = 0;
    gui_set_active_win("##enter world error");
}

static void
_draw_selected_character_window(list_character_t *cp, int x, int y)
{
    gui_begin_win("Selected character", x, y, 256, 256, 0);
    gui_origin(GUI_TOP_LEFT);
    gui_textf(
        "Name: %s\n"
        "Race: %d\n"
        "Sex: %d",
        0, 5, 5, cp->name, cp->race, cp->sex);
    gui_end_win();
}

static void
_draw_logging_in_window(void)
{
    gui_origin(GUI_CENTER_CENTER);
    gui_begin_empty_win("log in layer", 0, 0, core_asset_config.resolution_w,
        core_asset_config.resolution_h, 0);
    gui_begin_win("##log in window", 0, 0, 256, 256, 0);
    gui_text("Entering world...", 0, 0, 0);
    gui_origin(GUI_BOTTOM_CENTER);
    if (gui_button("Cancel##log in win", 0, 0, 64, 24, 0))
        shard_disconnect();
    gui_end_win();
    gui_end_win();
}

static void
_draw_enter_world_error_window(void)
{
    gui_origin(GUI_CENTER_CENTER);
    gui_begin_win("##enter world error", 0, 0, 340, 200, 0);
    const char *reason = "unknown";
    switch (shard_get_enter_world_error())
    {
    case ENTER_WORLD_ERROR_INSTANCE_DOWN:
        reason = "instance is down";
        break;
    case ENTER_WORLD_ERROR_INSTANCE_PART_DOWN:
        reason = "instance part is down";
        break;
    case ENTER_WORLD_ERROR_FULL:
        reason = "world is full";
        break;
    case ENTER_WORLD_ERROR_INTERNAL:
        reason = "internal server error";
        break;
    }
    gui_textf("Enter world failed: %s.", 0, 0, 0, reason);
    gui_origin(GUI_BOTTOM_CENTER);
    if (gui_button("Ok", 0, 5, 64, 64, 0))
        _enter_world_error_window_open = 0;
    gui_end_win();
}
