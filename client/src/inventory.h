#ifndef MUTA_CLIENT_INVENOTRY_H
#define MUTA_CLIENT_INVENOTRY_H

#include "../../shared/common_defs.h"

typedef struct item_t       item_t;
typedef struct container_t  container_t;
typedef struct equipment_t  equipment_t;
typedef struct inventory_t  inventory_t;

struct item_t
{
    dobj_runtime_id_t   runtime_id;
    dobj_type_id_t      type_id;
    float               inventory_icon_clip[4];
    bool8               repeat_icon; /* 1x1 repeatable icon or single draw? */
    uint8               x;
    uint8               y;
    uint8               w;
    uint8               h;
};

struct container_t
{
    item_t  items[MAX_BAG_SIZE];
    uint32  num_items;
    uint8   w;
    uint8   h;
};

struct equipment_t
{
    dobj_runtime_id_t   runtime_id;
    dobj_type_id_t      type_id;
    float               icon_clip[4];
    bool8               is_container;
    bool8               is_equipped;
};

struct inventory_t
{
    equipment_t equipment[NUM_EQUIPMENTS_SLOTS];
    container_t containers[NUM_EQUIPMENTS_SLOTS];
};

void
inventory_init(inventory_t *inv);

int
inventory_equip_slot(inventory_t *inv, enum equipment_slot slot,
    dobj_runtime_id_t runtime_id, dobj_type_id_t type_id);

int
inventory_unequip_slot(inventory_t *inv, enum equipment_slot slot);

container_t *
inventory_get_container(inventory_t *inv, enum equipment_slot slot);
/* Returns 0 if not a container. Note: assumes slot is equipped! */

bool32
inventory_find_item_and_container(inventory_t *inv,
    dobj_runtime_id_t dobj_runtime_id, item_t **ret_item,
    container_t **ret_container);

item_t *
inventory_find_item(inventory_t *inv, dobj_runtime_id_t dobj_runtime_id);

int
container_add_item(container_t *container, dobj_runtime_id_t runtime_id,
    dobj_type_id_t type_id, uint8 x, uint8 y);

int
container_del_item(container_t *container, dobj_runtime_id_t runtime_id);

void
container_del_item_at_index(container_t *container, uint32 index);

item_t *
container_get_item_at(container_t *container, int x, int y);

int
container_set_item_position(container_t *container, item_t *item, uint8 new_x,
    uint8 new_y);

bool32
container_can_fit(container_t *container, uint8 x, uint8 y, uint8 w, uint8 h);

bool32
container_can_move(container_t *container, dobj_runtime_id_t dobj_runtime_id,
    uint8 x, uint8 y, uint8 w, uint8 h);

#endif /* MUTA_CLIENT_INVENOTRY_H */
