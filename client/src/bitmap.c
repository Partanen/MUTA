#include "bitmap.h"
#include "../../shared/common_utils.h"
#define STB_IMAGE_IMPLEMENTATION
#include "../../shared/stb/stb_image.h"

int
img_load(img_t *img, const char *path)
{
    img->pixels = stbi_load(path, &img->w, &img->h, &img->bytes_per_px, 0);
    if (!img->pixels)
    {
        DEBUG_PRINTFF("failed for path '%s'.\n", path);
        return 2;
    }
    img->pxf = img->bytes_per_px == 3 ? IMG_RGB : IMG_RGBA;
    return 0;
}

void
img_free(img_t *img)
{
    stbi_image_free(img->pixels);
    memset(img, 0, sizeof(img_t));
}
