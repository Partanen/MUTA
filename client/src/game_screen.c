#include <math.h>
#include "game_screen.h"
#include "main_menu_screen.h"
#include "game_state.h"
#include "render.h"
#include "core.h"
#include "shard.h"
#include "gl.h"

static void
gamescreen_update(double dt);

static void
gamescreen_open(void);

static void
gamescreen_close(void);

static void
gamescreen_keydown(int key, bool32 is_repeat);

static void
gamescreen_keyup(int key);

static void
gamescreen_text_input(const char *text);

static void
gamescreen_mousebuttonup(uint8 button, int x, int y);

static void
gamescreen_mousebuttondown(uint8 button, int x, int y);

screen_t game_screen =
{
    "Game",
    0,
    0,
    gamescreen_update,
    gamescreen_open,
    gamescreen_close,
    gamescreen_text_input,
    gamescreen_keydown,
    gamescreen_keyup,
    gamescreen_mousebuttondown,
    gamescreen_mousebuttonup,
    0,
    0
};

static void
gamescreen_update(double dt)
{
    gl_viewport(0, 0, core_window_w(), core_window_h());
    gl_scissor(0, 0, core_window_w(), core_window_h());
    gl_color(0.0f, 0.0f, 0.0f, 1.0f);
    gl_clear(RB_CLEAR_COLOR_BIT);
    if (shard_get_status() == SHARD_STATUS_CONNECTED)
    {
        gs_update_and_render(dt);
        core_execute_click_events();
    } else
        core_set_screen(&main_menu_screen);
}

static void
gamescreen_open(void)
{
}

static void
gamescreen_close(void)
{
    shard_disconnect();
    if (gs_in_session())
        gs_end_session();
}

static void
gamescreen_keydown(int key, bool32 is_repeat)
    {gs_key_down(key, is_repeat);}

static void
gamescreen_keyup(int key)
    {gs_key_up(key);}

static void
gamescreen_text_input(const char *text)
{
}

static void
gamescreen_mousebuttonup(uint8 button, int x, int y)
    {gs_mouse_up((int)button, x, y);}

static void
gamescreen_mousebuttondown(uint8 button, int x, int y)
    {gs_mouse_down((int)button, x, y);}
