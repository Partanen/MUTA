#include "client.h"
#include "game_state.h"
#include "../../shared/common_utils.h"
#include "../../shared/common_defs.h"
#include "../../shared/client_packets.h"

static blocking_queue_t _event_queue;
static segpool_t        _segpool;
static mutex_t          _segpool_mutex;

int
cl_init(void)
{
    blocking_queue_init(&_event_queue, 64, sizeof(cl_event_t));
    segpool_init(&_segpool);
    mutex_init(&_segpool_mutex);
    return 0;
}

void
cl_destroy(void)
{
    mutex_destroy(&_segpool_mutex);
    segpool_destroy(&_segpool);
    blocking_queue_destroy(&_event_queue);
}

void
cl_push_event(cl_event_t *event)
    {blocking_queue_push(&_event_queue, event);}

int
cl_pop_events(cl_event_t *ret, int max)
    {return (int)blocking_queue_pop_num(&_event_queue, max, ret);}

void *
cl_malloc(uint32 num_bytes)
{
    mutex_lock(&_segpool_mutex);
    void *ret = segpool_malloc(&_segpool, num_bytes);
    mutex_unlock(&_segpool_mutex);
    return ret;
}

int
cl_flush_buf(socket_t socket, uint8 *memory, int *num_bytes)
{
    int num_sent = 0;
    while (num_sent < *num_bytes)
    {
        int num_to_send = MIN(*num_bytes - num_sent, MUTA_MTU);
        if (net_send_all(socket, memory + num_sent, num_to_send) <= 0)
            return 2;
        num_sent += num_to_send;
    }
    *num_bytes = 0;
    return 0;
}

bbuf_t
cl_send(socket_t socket, uint8 *memory, int *num_memory_bytes,
    int max_memory_bytes, int num_bytes)
{
    int     num_req_bytes   = sizeof(clmsg_t) + num_bytes;
    bbuf_t  ret             = {0};
    while (max_memory_bytes - (*num_memory_bytes) < num_req_bytes)
    {
        int num_to_send = MIN(*num_memory_bytes, MUTA_MTU);
        if (net_send_all(socket, memory + num_to_send, num_to_send))
            return ret;
        *num_memory_bytes -= num_to_send;
    }
    BBUF_INIT(&ret, memory + (*num_memory_bytes), num_req_bytes);
    (*num_memory_bytes) += num_req_bytes;
    return ret;
}

int
cl_check_blocking_socket_connect(socket_t socket, fd_set *socket_set)
{
    if (!FD_ISSET(socket, socket_set))
        return 1;
    int         result;
    socklen_t   result_len = sizeof(result);
    if (getsockopt(socket, SOL_SOCKET, SO_ERROR, (void*)&result, &result_len)
        < 0)
        return -1;
    if (!result)
        return 0;
    if (result == NET_SOCKET_CONNECT_IN_PROGRESS)
        return 2;
    return -2;
}

int
cl_read(cl_read_event_t *event, int (*read_packet)(void),
    uint8 *memory, int *num_memory_bytes, int max_memory_bytes,
    int *last_error, int server_closed_connection_error, int bad_packet_error)
{
    int ret = 0;
    if (event->num_bytes <= 0)
    {
        *last_error = server_closed_connection_error;
        return 1;
    }
    int num_read = 0;
    while (num_read < (int)event->num_bytes)
    {
        int num_to_read = MIN(max_memory_bytes - *num_memory_bytes,
            event->num_bytes - num_read);
        memcpy(memory + (*num_memory_bytes), event->memory + num_read,
            num_to_read);
        *num_memory_bytes += num_to_read;
        if (read_packet())
        {
            if (!*last_error)
                *last_error = bad_packet_error;
            ret = 2;
            break;
        }
        num_read += num_to_read;
    }
    mutex_lock(&_segpool_mutex);
    segpool_free(&_segpool, event->memory);
    mutex_unlock(&_segpool_mutex);
    return ret;
}
