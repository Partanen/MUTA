#include <stdio.h>
#include <string.h>
#include "screen.h"
#include "log.h"
#include "main_menu_screen.h"
#include "sprite_editor.h"
#include "game_screen.h"
#include "character_selection_screen.h"
#include "character_creation_screen.h"
#include "animator_screen.h"
#include "editor/screen.h"

#define NUM_SCREENS (sizeof(_screens) / sizeof(screen_t*))

static screen_t *_screens[] =
{
    &main_menu_screen,
    &sprite_editor_screen,
    &game_screen,
    &character_select_screen,
    &character_create_screen,
    &animator_screen,
    &editor_screen
};

int
screens_init(void)
{
    for (uint32 i = 0; i < NUM_SCREENS; ++i)
        if (_screens[i]->init && _screens[i]->init())
        {
            LOG("Failed to initialize screen %s!", _screens[i]->name);
            return 0;
        }
    return 0;
}

void
screens_destroy(void)
{
    for (uint32 i = 0; i < NUM_SCREENS; ++i)
        if (_screens[i]->destroy)
            _screens[i]->destroy();
}

screen_t *
screens_get(const char *name)
{
    for (size_t i = 0; i < sizeof(_screens) / sizeof(screen_t*); ++i)
        if (!strcmp(name, _screens[i]->name))
            return _screens[i];
    return 0;
}

screen_t **
screens_get_all(uint32 *ret_num)
{
    *ret_num = NUM_SCREENS;
    return _screens;
}
