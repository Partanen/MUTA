#ifndef MUTA_CLIENT_COMPONENTS_H
#define MUTA_CLIENT_COMPONENTS_H

/* components.h
 * Any component definitions must be added to the component_definitions array
 * in components.c.
 * The array size in this header must be kept up to date with the contents of
 * the array! */

#include "component.h"

#define NUM_COMPONENT_DEFINITIONS \
    (sizeof(component_definitions) / sizeof(component_definition_t*))

/* Forward declaration(s) */
typedef struct component_definition_t component_definition_t;

extern component_definition_t *component_definitions[6];

#endif /* MUTA_CLIENT_COMPONENTS_H */
