#ifndef MUTA_CLIENT_GUI_H
#define MUTA_CLIENT_GUI_H

#include "../../shared/gui/gui.h"
#include "../../shared/types.h"

gui_bool_t
gui_text_input_dstr(const char *title, dchar **buf, int x,
    int y, int w, int h, int flags);

gui_font_t *
gui_get_text_input_state_style_input_font(gui_text_input_state_style_t *style);
/* Convenience. Returns gui_default_font if font is null. */

int
gui_get_text_input_style_max_input_font_height(gui_text_input_style_t *style);
/* Find tallest character. */

int
gui_get_text_input_style_min_height(gui_text_input_style_t *style);
/* Font height + border heights. */

gui_font_t *
gui_get_text_input_state_style_title_font(gui_text_input_state_style_t *style);

int
gui_get_text_input_style_max_title_font_height(gui_text_input_style_t *style);

gui_font_t *
gui_get_button_state_style_font(gui_button_state_style_t *style);
/* Convenience. Returns gui_default_font if font is null. */

int
gui_get_button_style_max_font_height(gui_button_style_t *style);

int
gui_get_button_style_min_height(gui_button_style_t *style);

#endif /* MUTA_CLIENT_GUI_H */
