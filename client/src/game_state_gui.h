#ifndef MUTA_CLIENT_GAME_STATE_GUI_H
#define MUTA_CLIENT_GAME_STATE_GUI_H

#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct game_state_t game_state_t;

int
gsgui_init(game_state_t *gs);

void
gsgui_begin(const char *character_name);

void
gsgui_end(void);

void
gsgui_update_and_render(void);

void
gsgui_on_chat_msg_received(int type, const char *sender, const char *msg);

void
gsgui_on_system_msg_received(const char *msg);

void
gsgui_on_local_error_msg_received(const char *msg);

bool32
gsgui_is_grabbing_input(void);

void
gsgui_on_key_event(int event, int key);

void
gsgui_on_start_drag_dynamic_object(dobj_type_id_t type_id);

void
gsgui_on_stop_dragging_dynamic_object(void);

void
gsgui_toggle_options(void);

#endif /* MUTA_CLIENT_GAME_STATE_GUI_H */
