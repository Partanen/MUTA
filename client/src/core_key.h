#ifndef MUTA_CORE_KEY_H
#define MUTA_CORE_KEY_H

#include <SDL2/SDL.h>

#define CORE_KEY_UNKNOWN SDL_SCANCODE_UNKNOWN
#define CORE_KEY_A SDL_SCANCODE_A
#define CORE_KEY_B SDL_SCANCODE_B
#define CORE_KEY_C SDL_SCANCODE_C
#define CORE_KEY_D SDL_SCANCODE_D
#define CORE_KEY_E SDL_SCANCODE_E
#define CORE_KEY_F SDL_SCANCODE_F
#define CORE_KEY_G SDL_SCANCODE_G
#define CORE_KEY_H SDL_SCANCODE_H
#define CORE_KEY_I SDL_SCANCODE_I
#define CORE_KEY_J SDL_SCANCODE_J
#define CORE_KEY_K SDL_SCANCODE_K
#define CORE_KEY_L SDL_SCANCODE_L
#define CORE_KEY_M SDL_SCANCODE_M
#define CORE_KEY_N SDL_SCANCODE_N
#define CORE_KEY_O SDL_SCANCODE_O
#define CORE_KEY_P SDL_SCANCODE_P
#define CORE_KEY_Q SDL_SCANCODE_Q
#define CORE_KEY_R SDL_SCANCODE_R
#define CORE_KEY_S SDL_SCANCODE_S
#define CORE_KEY_T SDL_SCANCODE_T
#define CORE_KEY_U SDL_SCANCODE_U
#define CORE_KEY_V SDL_SCANCODE_V
#define CORE_KEY_W SDL_SCANCODE_W
#define CORE_KEY_X SDL_SCANCODE_X
#define CORE_KEY_Y SDL_SCANCODE_Y
#define CORE_KEY_Z SDL_SCANCODE_Z
#define CORE_KEY_1 SDL_SCANCODE_1
#define CORE_KEY_2 SDL_SCANCODE_2
#define CORE_KEY_3 SDL_SCANCODE_3
#define CORE_KEY_4 SDL_SCANCODE_4
#define CORE_KEY_5 SDL_SCANCODE_5
#define CORE_KEY_6 SDL_SCANCODE_6
#define CORE_KEY_7 SDL_SCANCODE_7
#define CORE_KEY_8 SDL_SCANCODE_8
#define CORE_KEY_9 SDL_SCANCODE_9
#define CORE_KEY_0 SDL_SCANCODE_0
#define CORE_KEY_RETURN SDL_SCANCODE_RETURN
#define CORE_KEY_ESCAPE SDL_SCANCODE_ESCAPE
#define CORE_KEY_BACKSPACE SDL_SCANCODE_BACKSPACE
#define CORE_KEY_TAB SDL_SCANCODE_TAB
#define CORE_KEY_SPACE SDL_SCANCODE_SPACE
#define CORE_KEY_MINUS SDL_SCANCODE_MINUS
#define CORE_KEY_EQUALS SDL_SCANCODE_EQUALS
#define CORE_KEY_LEFTBRACKET SDL_SCANCODE_LEFTBRACKET
#define CORE_KEY_RIGHTBRACKET SDL_SCANCODE_RIGHTBRACKET
#define CORE_KEY_BACKSLASH SDL_SCANCODE_BACKSLASH
#define CORE_KEY_NONUSHASH SDL_SCANCODE_NONUSHASH
#define CORE_KEY_SEMICOLON SDL_SCANCODE_SEMICOLON
#define CORE_KEY_APOSTROPHE SDL_SCANCODE_APOSTROPHE
#define CORE_KEY_GRAVE SDL_SCANCODE_GRAVE
#define CORE_KEY_COMMA SDL_SCANCODE_COMMA
#define CORE_KEY_PERIOD SDL_SCANCODE_PERIOD
#define CORE_KEY_SLASH SDL_SCANCODE_SLASH
#define CORE_KEY_CAPSLOCK SDL_SCANCODE_CAPSLOCK
#define CORE_KEY_F1 SDL_SCANCODE_F1
#define CORE_KEY_F2 SDL_SCANCODE_F2
#define CORE_KEY_F3 SDL_SCANCODE_F3
#define CORE_KEY_F4 SDL_SCANCODE_F4
#define CORE_KEY_F5 SDL_SCANCODE_F5
#define CORE_KEY_F6 SDL_SCANCODE_F6
#define CORE_KEY_F7 SDL_SCANCODE_F7
#define CORE_KEY_F8 SDL_SCANCODE_F8
#define CORE_KEY_F9 SDL_SCANCODE_F9
#define CORE_KEY_F10 SDL_SCANCODE_F10
#define CORE_KEY_F11 SDL_SCANCODE_F11
#define CORE_KEY_F12 SDL_SCANCODE_F12
#define CORE_KEY_PRINTSCREEN SDL_SCANCODE_PRINTSCREEN
#define CORE_KEY_SCROLLLOCK SDL_SCANCODE_SCROLLLOCK
#define CORE_KEY_PAUSE SDL_SCANCODE_PAUSE
#define CORE_KEY_INSERT SDL_SCANCODE_INSERT
#define CORE_KEY_HOME SDL_SCANCODE_HOME
#define CORE_KEY_PAGEUP SDL_SCANCODE_PAGEUP
#define CORE_KEY_DELETE SDL_SCANCODE_DELETE
#define CORE_KEY_END SDL_SCANCODE_END
#define CORE_KEY_PAGEDOWN SDL_SCANCODE_PAGEDOWN
#define CORE_KEY_RIGHT SDL_SCANCODE_RIGHT
#define CORE_KEY_LEFT SDL_SCANCODE_LEFT
#define CORE_KEY_DOWN SDL_SCANCODE_DOWN
#define CORE_KEY_UP SDL_SCANCODE_UP
#define CORE_KEY_NUMLOCKCLEAR SDL_SCANCODE_NUMLOCKCLEAR
#define CORE_KEY_KP_DIVIDE SDL_SCANCODE_KP_DIVIDE
#define CORE_KEY_KP_MULTIPLY SDL_SCANCODE_KP_MULTIPLY
#define CORE_KEY_KP_MINUS SDL_SCANCODE_KP_MINUS
#define CORE_KEY_KP_PLUS SDL_SCANCODE_KP_PLUS
#define CORE_KEY_KP_ENTER SDL_SCANCODE_KP_ENTER
#define CORE_KEY_KP_1 SDL_SCANCODE_KP_1
#define CORE_KEY_KP_2 SDL_SCANCODE_KP_2
#define CORE_KEY_KP_3 SDL_SCANCODE_KP_3
#define CORE_KEY_KP_4 SDL_SCANCODE_KP_4
#define CORE_KEY_KP_5 SDL_SCANCODE_KP_5
#define CORE_KEY_KP_6 SDL_SCANCODE_KP_6
#define CORE_KEY_KP_7 SDL_SCANCODE_KP_7
#define CORE_KEY_KP_8 SDL_SCANCODE_KP_8
#define CORE_KEY_KP_9 SDL_SCANCODE_KP_9
#define CORE_KEY_KP_0 SDL_SCANCODE_KP_0
#define CORE_KEY_KP_PERIOD SDL_SCANCODE_KP_PERIOD
#define CORE_KEY_NONUSBACKSLASH SDL_SCANCODE_NONUSBACKSLASH
#define CORE_KEY_APPLICATION SDL_SCANCODE_APPLICATION
#define CORE_KEY_POWER SDL_SCANCODE_POWER
#define CORE_KEY_KP_EQUALS SDL_SCANCODE_KP_EQUALS
#define CORE_KEY_F13 SDL_SCANCODE_F13
#define CORE_KEY_F14 SDL_SCANCODE_F14
#define CORE_KEY_F15 SDL_SCANCODE_F15
#define CORE_KEY_F16 SDL_SCANCODE_F16
#define CORE_KEY_F17 SDL_SCANCODE_F17
#define CORE_KEY_F18 SDL_SCANCODE_F18
#define CORE_KEY_F19 SDL_SCANCODE_F19
#define CORE_KEY_F20 SDL_SCANCODE_F20
#define CORE_KEY_F21 SDL_SCANCODE_F21
#define CORE_KEY_F22 SDL_SCANCODE_F22
#define CORE_KEY_F23 SDL_SCANCODE_F23
#define CORE_KEY_F24 SDL_SCANCODE_F24
#define CORE_KEY_EXECUTE SDL_SCANCODE_EXECUTE
#define CORE_KEY_HELP SDL_SCANCODE_HELP
#define CORE_KEY_MENU SDL_SCANCODE_MENU
#define CORE_KEY_SELECT SDL_SCANCODE_SELECT
#define CORE_KEY_STOP SDL_SCANCODE_STOP
#define CORE_KEY_AGAIN SDL_SCANCODE_AGAIN
#define CORE_KEY_UNDO SDL_SCANCODE_UNDO
#define CORE_KEY_CUT SDL_SCANCODE_CUT
#define CORE_KEY_COPY SDL_SCANCODE_COPY
#define CORE_KEY_PASTE SDL_SCANCODE_PASTE
#define CORE_KEY_FIND SDL_SCANCODE_FIND
#define CORE_KEY_MUTE SDL_SCANCODE_MUTE
#define CORE_KEY_VOLUMEUP SDL_SCANCODE_VOLUMEUP
#define CORE_KEY_VOLUMEDOWN SDL_SCANCODE_VOLUMEDOWN
#define CORE_KEY_KP_COMMA SDL_SCANCODE_KP_COMMA
#define CORE_KEY_KP_EQUALSAS400 SDL_SCANCODE_KP_EQUALSAS400
#define CORE_KEY_INTERNATIONAL1 SDL_SCANCODE_INTERNATIONAL1
#define CORE_KEY_INTERNATIONAL2 SDL_SCANCODE_INTERNATIONAL2
#define CORE_KEY_INTERNATIONAL3 SDL_SCANCODE_INTERNATIONAL3
#define CORE_KEY_INTERNATIONAL4 SDL_SCANCODE_INTERNATIONAL4
#define CORE_KEY_INTERNATIONAL5 SDL_SCANCODE_INTERNATIONAL5
#define CORE_KEY_INTERNATIONAL6 SDL_SCANCODE_INTERNATIONAL6
#define CORE_KEY_INTERNATIONAL7 SDL_SCANCODE_INTERNATIONAL7
#define CORE_KEY_INTERNATIONAL8 SDL_SCANCODE_INTERNATIONAL8
#define CORE_KEY_INTERNATIONAL9 SDL_SCANCODE_INTERNATIONAL9
#define CORE_KEY_LANG1 SDL_SCANCODE_LANG1
#define CORE_KEY_LANG2 SDL_SCANCODE_LANG2
#define CORE_KEY_LANG3 SDL_SCANCODE_LANG3
#define CORE_KEY_LANG4 SDL_SCANCODE_LANG4
#define CORE_KEY_LANG5 SDL_SCANCODE_LANG5
#define CORE_KEY_LANG6 SDL_SCANCODE_LANG6
#define CORE_KEY_LANG7 SDL_SCANCODE_LANG7
#define CORE_KEY_LANG8 SDL_SCANCODE_LANG8
#define CORE_KEY_LANG9 SDL_SCANCODE_LANG9
#define CORE_KEY_ALTERASE SDL_SCANCODE_ALTERASE
#define CORE_KEY_SYSREQ SDL_SCANCODE_SYSREQ
#define CORE_KEY_CANCEL SDL_SCANCODE_CANCEL
#define CORE_KEY_CLEAR SDL_SCANCODE_CLEAR
#define CORE_KEY_PRIOR SDL_SCANCODE_PRIOR
#define CORE_KEY_RETURN2 SDL_SCANCODE_RETURN2
#define CORE_KEY_SEPARATOR SDL_SCANCODE_SEPARATOR
#define CORE_KEY_OUT SDL_SCANCODE_OUT
#define CORE_KEY_OPER SDL_SCANCODE_OPER
#define CORE_KEY_CLEARAGAIN SDL_SCANCODE_CLEARAGAIN
#define CORE_KEY_CRSEL SDL_SCANCODE_CRSEL
#define CORE_KEY_EXSEL SDL_SCANCODE_EXSEL
#define CORE_KEY_KP_00 SDL_SCANCODE_KP_00
#define CORE_KEY_KP_000 SDL_SCANCODE_KP_000
#define CORE_KEY_THOUSANDSSEPARATOR SDL_SCANCODE_THOUSANDSSEPARATOR
#define CORE_KEY_DECIMALSEPARATOR SDL_SCANCODE_DECIMALSEPARATOR
#define CORE_KEY_CURRENCYUNIT SDL_SCANCODE_CURRENCYUNIT
#define CORE_KEY_CURRENCYSUBUNIT SDL_SCANCODE_CURRENCYSUBUNIT
#define CORE_KEY_KP_LEFTPAREN SDL_SCANCODE_KP_LEFTPAREN
#define CORE_KEY_KP_RIGHTPAREN SDL_SCANCODE_KP_RIGHTPAREN
#define CORE_KEY_KP_LEFTBRACE SDL_SCANCODE_KP_LEFTBRACE
#define CORE_KEY_KP_RIGHTBRACE SDL_SCANCODE_KP_RIGHTBRACE
#define CORE_KEY_KP_TAB SDL_SCANCODE_KP_TAB
#define CORE_KEY_KP_BACKSPACE SDL_SCANCODE_KP_BACKSPACE
#define CORE_KEY_KP_A SDL_SCANCODE_KP_A
#define CORE_KEY_KP_B SDL_SCANCODE_KP_B
#define CORE_KEY_KP_C SDL_SCANCODE_KP_C
#define CORE_KEY_KP_D SDL_SCANCODE_KP_D
#define CORE_KEY_KP_E SDL_SCANCODE_KP_E
#define CORE_KEY_KP_F SDL_SCANCODE_KP_F
#define CORE_KEY_KP_XOR SDL_SCANCODE_KP_XOR
#define CORE_KEY_KP_POWER SDL_SCANCODE_KP_POWER
#define CORE_KEY_KP_PERCENT SDL_SCANCODE_KP_PERCENT
#define CORE_KEY_KP_LESS SDL_SCANCODE_KP_LESS
#define CORE_KEY_KP_GREATER SDL_SCANCODE_KP_GREATER
#define CORE_KEY_KP_AMPERSAND SDL_SCANCODE_KP_AMPERSAND
#define CORE_KEY_KP_DBLAMPERSAND SDL_SCANCODE_KP_DBLAMPERSAND
#define CORE_KEY_KP_VERTICALBAR SDL_SCANCODE_KP_VERTICALBAR
#define CORE_KEY_KP_DBLVERTICALBAR SDL_SCANCODE_KP_DBLVERTICALBAR
#define CORE_KEY_KP_COLON SDL_SCANCODE_KP_COLON
#define CORE_KEY_KP_HASH SDL_SCANCODE_KP_HASH
#define CORE_KEY_KP_SPACE SDL_SCANCODE_KP_SPACE
#define CORE_KEY_KP_AT SDL_SCANCODE_KP_AT
#define CORE_KEY_KP_EXCLAM SDL_SCANCODE_KP_EXCLAM
#define CORE_KEY_KP_MEMSTORE SDL_SCANCODE_KP_MEMSTORE
#define CORE_KEY_KP_MEMRECALL SDL_SCANCODE_KP_MEMRECALL
#define CORE_KEY_KP_MEMCLEAR SDL_SCANCODE_KP_MEMCLEAR
#define CORE_KEY_KP_MEMADD SDL_SCANCODE_KP_MEMADD
#define CORE_KEY_KP_MEMSUBTRACT SDL_SCANCODE_KP_MEMSUBTRACT
#define CORE_KEY_KP_MEMMULTIPLY SDL_SCANCODE_KP_MEMMULTIPLY
#define CORE_KEY_KP_MEMDIVIDE SDL_SCANCODE_KP_MEMDIVIDE
#define CORE_KEY_KP_PLUSMINUS SDL_SCANCODE_KP_PLUSMINUS
#define CORE_KEY_KP_CLEAR SDL_SCANCODE_KP_CLEAR
#define CORE_KEY_KP_CLEARENTRY SDL_SCANCODE_KP_CLEARENTRY
#define CORE_KEY_KP_BINARY SDL_SCANCODE_KP_BINARY
#define CORE_KEY_KP_OCTAL SDL_SCANCODE_KP_OCTAL
#define CORE_KEY_KP_DECIMAL SDL_SCANCODE_KP_DECIMAL
#define CORE_KEY_KP_HEXADECIMAL SDL_SCANCODE_KP_HEXADECIMAL
#define CORE_KEY_LCTRL SDL_SCANCODE_LCTRL
#define CORE_KEY_LSHIFT SDL_SCANCODE_LSHIFT
#define CORE_KEY_LALT SDL_SCANCODE_LALT
#define CORE_KEY_LGUI SDL_SCANCODE_LGUI
#define CORE_KEY_RCTRL SDL_SCANCODE_RCTRL
#define CORE_KEY_RSHIFT SDL_SCANCODE_RSHIFT
#define CORE_KEY_RALT SDL_SCANCODE_RALT
#define CORE_KEY_RGUI SDL_SCANCODE_RGUI
#define CORE_KEY_MODE SDL_SCANCODE_MODE
#define CORE_KEY_AUDIONEXT SDL_SCANCODE_AUDIONEXT
#define CORE_KEY_AUDIOPREV SDL_SCANCODE_AUDIOPREV
#define CORE_KEY_AUDIOSTOP SDL_SCANCODE_AUDIOSTOP
#define CORE_KEY_AUDIOPLAY SDL_SCANCODE_AUDIOPLAY
#define CORE_KEY_AUDIOMUTE SDL_SCANCODE_AUDIOMUTE
#define CORE_KEY_MEDIASELECT SDL_SCANCODE_MEDIASELECT
#define CORE_KEY_WWW SDL_SCANCODE_WWW
#define CORE_KEY_MAIL SDL_SCANCODE_MAIL
#define CORE_KEY_CALCULATOR SDL_SCANCODE_CALCULATOR
#define CORE_KEY_COMPUTER SDL_SCANCODE_COMPUTER
#define CORE_KEY_AC_SEARCH SDL_SCANCODE_AC_SEARCH
#define CORE_KEY_AC_HOME SDL_SCANCODE_AC_HOME
#define CORE_KEY_AC_BACK SDL_SCANCODE_AC_BACK
#define CORE_KEY_AC_FORWARD SDL_SCANCODE_AC_FORWARD
#define CORE_KEY_AC_STOP SDL_SCANCODE_AC_STOP
#define CORE_KEY_AC_REFRESH SDL_SCANCODE_AC_REFRESH
#define CORE_KEY_AC_BOOKMARKS SDL_SCANCODE_AC_BOOKMARKS
#define CORE_KEY_BRIGHTNESSDOWN SDL_SCANCODE_BRIGHTNESSDOWN
#define CORE_KEY_BRIGHTNESSUP SDL_SCANCODE_BRIGHTNESSUP
#define CORE_KEY_DISPLAYSWITCH SDL_SCANCODE_DISPLAYSWITCH
#define CORE_KEY_KBDILLUMTOGGLE SDL_SCANCODE_KBDILLUMTOGGLE
#define CORE_KEY_KBDILLUMDOWN SDL_SCANCODE_KBDILLUMDOWN
#define CORE_KEY_KBDILLUMUP SDL_SCANCODE_KBDILLUMUP
#define CORE_KEY_EJECT SDL_SCANCODE_EJECT
#define CORE_KEY_SLEEP SDL_SCANCODE_SLEEP
#define CORE_KEY_APP1 SDL_SCANCODE_APP1
#define CORE_KEY_APP2 SDL_SCANCODE_APP2
#define CORE_KEY_AUDIOREWIND SDL_SCANCODE_AUDIOREWIND
#define CORE_KEY_AUDIOFASTFORWARD SDL_SCANCODE_AUDIOFASTFORWARD

#endif /* MUTA_CORE_KEY_H */
