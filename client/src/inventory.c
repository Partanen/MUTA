#include "inventory.h"
#include "log.h"
#include "icon.h"
#include "assets.h"
#include "../../shared/entities.h"
#include "../../shared/common_utils.h"

void
inventory_init(inventory_t *inv)
{
    for (int i = 0; i < NUM_EQUIPMENTS_SLOTS; ++i)
        inv->equipment[i].is_equipped = 0;
}

int
inventory_equip_slot(inventory_t *inv, enum equipment_slot slot,
    dobj_runtime_id_t runtime_id, dobj_type_id_t type_id)
{
    dynamic_object_def_t *def = ent_get_dynamic_object_def(type_id);
    if (!def)
    {
        LOG_ERR("Dynamic object definition %u not found.", type_id);
        return 1;
    }
    equipment_t *eq = &inv->equipment[slot];
    eq->runtime_id  = runtime_id;
    eq->type_id     = type_id;
    eq->is_equipped = 1;
    if (def->is_container)
    {
        eq->is_container = 1;
        container_t *container = &inv->containers[slot];
        container->num_items    = 0;
        container->w            = def->container_width;
        container->h            = def->container_height;
    } else
        eq->is_container = 0;
    as_dynamic_object_display_t *display = as_get_dynamic_object_display(
        def->display_id);
    bool32 is_icon_unknown;
    float *clip = icon_get_clip(display->inventory_icon_id, &is_icon_unknown);
    for (int i = 0; i < 4; ++i)
        eq->icon_clip[i] = clip[i];
    LOG_DEBUG("Equipped runtime id %u, type id %u in equipment slot %u.",
        runtime_id, type_id, slot);
    return 0;
}

int
inventory_unequip_slot(inventory_t *inv, enum equipment_slot slot)
{
    inv->equipment[slot].is_equipped = 0;
    LOG_DEBUG("Unequipped  equipment slot %u.", slot);
    return 0;
}

container_t *
inventory_get_container(inventory_t *inv, enum equipment_slot slot)
{
    muta_assert(inv->equipment[slot].is_equipped);
    if (!inv->equipment[slot].is_container)
        return 0;
    return &inv->containers[slot];
}

bool32
inventory_find_item_and_container(inventory_t *inv,
    dobj_runtime_id_t dobj_runtime_id, item_t **ret_item,
    container_t **ret_container)
{
    for (int i = 0; i < NUM_EQUIPMENTS_SLOTS; ++i)
    {
        if (!inv->equipment[i].is_equipped)
            continue;
        if (!inv->equipment[i].is_container)
            continue;
        container_t *container = &inv->containers[i];
        uint32 num_items = container->num_items;
        for (uint32 j = 0; j < num_items; ++j)
        {
            item_t *item = &container->items[j];
            if (item->runtime_id == dobj_runtime_id)
            {
                *ret_container  = container;
                *ret_item       = item;
                return 1;
            }
        }
    }
    return 0;
}

item_t *
inventory_find_item(inventory_t *inv, dobj_runtime_id_t dobj_runtime_id)
{
    for (int i = 0; i < NUM_EQUIPMENTS_SLOTS; ++i)
    {
        if (!inv->equipment[i].is_equipped)
            continue;
        if (!inv->equipment[i].is_container)
            continue;
        container_t *container = &inv->containers[i];
        uint32 num_items = container->num_items;
        for (uint32 j = 0; j < num_items; ++j)
        {
            item_t *item = &container->items[j];
            if (item->runtime_id == dobj_runtime_id)
                return item;
        }
    }
    return 0;
}

int
container_add_item(container_t *container,
    dobj_runtime_id_t runtime_id, dobj_type_id_t type_id,
    uint8 x, uint8 y)
{
    dynamic_object_def_t *def = ent_get_dynamic_object_def(type_id);
    if (!def)
    {
        LOG_ERR("Dynamic object type %u not found.", type_id);
        return 1;
    }
    uint8   w           = def->width_in_container;
    uint8   h           = def->height_in_container;
    uint32  num_items   = container->num_items;
    if (num_items == container->w * container->h)
    {
        LOG_ERR("Container is full.");
        return 1;
    }
    for (uint32 i = 0; i < num_items; ++i)
    {
        item_t *item = &container->items[i];
        if (x < item->x + item->w && x + w > item->x &&
            y < item->y + item->h && y + h > item->y)
        {
            LOG_ERR("Item collides with another object in container. Existing "
                "item xywh: %u, %u, %u, %u. New item xywh: %u, %u, %u, %u.",
                (uint)item->x, (uint)item->y, (uint)w, (uint)h, (uint)x,
                (uint)y, (uint)w, (uint)h);
            return 1;
        }
    }
    item_t *item = &container->items[num_items];
    as_dynamic_object_display_t *display = as_get_dynamic_object_display(
        def->display_id);
    bool32 is_icon_unknown;
    float *clip = icon_get_clip(display->inventory_icon_id, &is_icon_unknown);
    for (int i = 0; i < 4; ++i)
        item->inventory_icon_clip[i] = clip[i];
    item->runtime_id        = runtime_id;
    item->type_id           = type_id;
    item->x                 = x;
    item->y                 = y;
    item->w                 = w;
    item->h                 = h;
    item->repeat_icon       = (bool8)is_icon_unknown;
    container->num_items    = num_items + 1;
    LOG_DEBUG("Added inventory item %u of type id %u at %d, %d.",
        runtime_id, type_id, (int)x, (int)y);
    return 0;
}

int
container_del_item(container_t *container,
    dobj_runtime_id_t runtime_id)
{
    item_t *item = 0;
    uint32 num_items = container->num_items;
    for (uint32 i = 0; i < num_items; ++i)
        if (container->items[i].runtime_id == runtime_id)
        {
            item = &container->items[i];
            break;
        }
    if (!item)
    {
        LOG_ERR("Item with runtime id %u not found.", runtime_id);
        return 1;
    }
    num_items               = num_items - 1;
    container->num_items    = num_items;
    *item                   = container->items[num_items];
    return 0;
}

void
container_del_item_at_index(container_t *container, uint32 index)
    {container->items[index] = container->items[--container->num_items];}

item_t *
container_get_item_at(container_t *container, int x, int y)
{
    uint32 num_items = container->num_items;
    for (uint32 i = 0; i < num_items; ++i)
    {
        item_t *item = &container->items[i];
        if (x < item->x + item->w && x >= item->x &&
            y < item->y + item->h && y >= item->y)
            return item;
    }
    return 0;
}

int
container_set_item_position(container_t *container, item_t *item, uint8 new_x,
    uint8 new_y)
{
    item->x = new_x;
    item->y = new_y;
    return 0;
}

bool32
container_can_fit(container_t *container, uint8 x, uint8 y, uint8 w, uint8 h)
{
    uint32  num_items   = container->num_items;
    for (uint32 i = 0; i < num_items; ++i)
    {
        item_t *item = &container->items[i];
        if (x < item->x + item->w && x + w > item->x &&
            y < item->y + item->h && y + h > item->y)
            return 0;
    }
    return 1;
}

bool32
container_can_move(container_t *container, dobj_runtime_id_t dobj_runtime_id,
    uint8 x, uint8 y, uint8 w, uint8 h)
{
    if (x + w > container->w || y + h > container->h)
        return 0;
    uint32 num_items = container->num_items;
    for (uint32 i = 0; i < num_items; ++i)
    {
        item_t *item = &container->items[i];
        if (item->runtime_id == dobj_runtime_id)
            continue;
        if (x < item->x + item->w && x + w > item->x &&
            y < item->y + item->h && y + h > item->y)
            return 0;
    }
    return 1;
}
