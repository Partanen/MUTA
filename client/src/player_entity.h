#ifndef MUTA_CLIENT_PLAYER_ENTITY_H
#define MUTA_CLIENT_PLAYER_ENTITY_H

#include "../../shared/types.h"
#include "../../shared/common_defs.h"

/* Forward declaration(s) */
typedef struct entity_t         entity_t;
typedef struct world_t          world_t;
typedef struct entity_event_t   entity_event_t;
typedef void*                   component_handle_t;
typedef struct ae_asset_t       ae_asset_t;

/* Types defined here */
typedef struct player_entity_t      player_entity_t;
typedef struct player_registry_t    player_registry_t;

struct player_entity_t
{
    player_runtime_id_t id; /* World session id */
    player_race_id_t    race_id; /* Cannot be invalid */
    char                name[MAX_DISPLAY_NAME_LEN + 1];
    float               walk_speed;
    uint32              health_current;
    uint32              health_max;
    uint8               flags;
    int8                sex;
};

entity_t *
player_spawn(world_t *world, player_runtime_id_t id, player_race_id_t race_id,
    const char *name, int sex, int direction, int position[3], uint8 move_speed,
    uint32 health_current, uint32 health_max, bool32 dead);

void
player_walk(entity_t *entity, int *position);

void
player_set_current_health(entity_t *entity, uint32 current_health);

#endif /* MUTA_CLIENT_PLAYER_ENTITY_H */
