#include <time.h>
#include <stdio.h>
#include <inttypes.h>
#include "core.h"
#include "log.h"
#include "gl.h"
#include "assets.h"
#include "audio.h"
#include "sprite_context.h"
#include "client.h"
#include "login.h"
#include "shard.h"
#include "screen.h"
#include "render.h"
#include "../../shared/net.h"
#include "../../shared/common_utils.h"
#include "../../shared/rwbits.inl"
#include "../../shared/account_rules.h"
#include "../../shared/emote.h"
#include "../../shared/kthp.h"
#include "../../shared/gui/gui.h"
#include "../../shared/tile.h"

#define MAX_CLICK_EVENTS    32
#define STACK_SIZE          4194304

typedef struct perf_clock_t             perf_clock_t;
typedef struct click_event_t            click_event_t;
typedef struct mouse_state_t            mouse_state_t;
typedef struct window_t                 window_t;
typedef struct async_job_t              async_job_t;
typedef struct config_context_t         config_context_t;

struct perf_clock_t
{
    double      delta;      /* Seconds */
    uint64      last_tick;
    uint32      fps;
    uint32      target_fps;
    uint64      frame_num;
};

struct click_event_t
{
    int     x, y;
    uint8   button, down;
};

struct mouse_state_t
{
    uint32  buttons;
    uint32  last_buttons;
    int     scroll_x;
    int     scroll_y;
    int     x, y, last_x, last_y, down_x, down_y;
};

struct window_t
{
    SDL_Window  *sdl_window;
    int         w, h;
};

struct async_job_t
{
    void (*callback)(void *args);
    void    *args;
    bool32  completed;
};

struct config_context_t
{
    bool32 editor_mode_overridden;
};

static sprite_context_t _core_sprite_context;
static segpool_t        _segpool;

static struct
{
    click_event_t   events[MAX_CLICK_EVENTS];
    int             num;
} _click_events;

static struct
{
    const uint8 *keys;
    uint8       *last_keys;
    int         num_keys;
} _keyboard;

static struct
{
    obj_pool_t  job_pool;
    event_buf_t event_buf; /* Contains async_job_t pointers. */
} _async;

const char *core_auto_login_name;
const char *core_auto_login_pw;

static perf_clock_t     _perf_clock;
static bool32           _running;
static screen_t         *_next_screen;
static screen_t         *_current_screen;
static char             _text_input[32];
static mouse_state_t    _mouse_state;
static window_t         main_window;
static kth_pool_t       _thread_pool;
static dynamic_stack_t  _stack;
static bool32           _is_first_frame_of_this_screen;

config_t                core_config;
asset_config_t          core_asset_config;
sprite_context_t        *core_sprite_context  = &_core_sprite_context;

static void
_read_config(const char *path, bool32 editor_mode_overridden,
    bool32 editor_mode);

static void
_update_gui_input_state(void);

static void
_perf_clock_init(perf_clock_t *clock, uint32 fps_limit);

static void
_perf_clock_tick(perf_clock_t *clock);

static int
_window_init(window_t *win, const char *title, int w, int h, bool32 fullscreen,
    bool32 maximized, bool32 windowed);

static void
_async_init(void);

static void
_async_destroy(void);

static void
_window_destroy(window_t *win);

static int
_window_maximize(window_t *win);

static void
_read_cfg_callback(void *ctx, const char *opt, const char *val);

static int
_post_click_event(click_event_t ev);

static void
_handle_window_event(SDL_Event *ev);

static int
_save_config(const char *path);

static void
_panic_callback(const char *error_msg);

static void
_async_callback(void *args);

int
core_maximize_window(void)
    {return _window_maximize(&main_window);}

int
core_fps(void)
    {return (int)_perf_clock.fps;}

void
core_set_screen(screen_t *screen)
    {_next_screen = screen;}

int
core_init(bool32 editor_mode_overridden, bool32 editor_mode)
{
    set_muta_panic_callback(_panic_callback);
    dynamic_stack_init(&_stack, STACK_SIZE);
    segpool_init(&_segpool);
    _async_init();
    _read_config("config.cfg", editor_mode_overridden, editor_mode);
    config_option_t opts[] =
    {
        {
            .name       = "tileset_tex_id",
            .type       = CONFIG_OPTION_TYPE_UINT32,
            .optional   = 0,
            .u32.value  = &core_asset_config.tileset_tex_id
        },
        {
            .name       = "tile_width",
            .type       = CONFIG_OPTION_TYPE_INT32,
            .optional   = 0,
            .i32.value  = &core_asset_config.tile_width
        },
        {
            .name       = "tile_height",
            .type       = CONFIG_OPTION_TYPE_INT32,
            .optional   = 0,
            .i32.value  = &core_asset_config.tile_height
        },
        {
            .name       = "tile_top_height",
            .type       = CONFIG_OPTION_TYPE_INT32,
            .optional   = 0,
            .i32.value  = &core_asset_config.tile_top_height
        },
        {
            .name       = "tile_selector_id_1",
            .type       = CONFIG_OPTION_TYPE_UINT16,
            .optional   = 0,
            .u16.value  = &core_asset_config.tile_selector_ids[0]
        },
        {
            .name       = "tile_selector_id_2",
            .type       = CONFIG_OPTION_TYPE_UINT16,
            .optional   = 0,
            .u16.value  = &core_asset_config.tile_selector_ids[1]
        },
        {
            .name       = "tile_selector_id_3",
            .type       = CONFIG_OPTION_TYPE_UINT16,
            .optional   = 0,
            .u16.value  = &core_asset_config.tile_selector_ids[2]
        },
        {
            .name       = "resolution_w",
            .type       = CONFIG_OPTION_TYPE_INT32,
            .optional   = 0,
            .i32.value  = &core_asset_config.resolution_w
        },
        {
            .name       = "resolution_h",
            .type       = CONFIG_OPTION_TYPE_INT32,
            .optional   = 0,
            .i32.value  = &core_asset_config.resolution_h
        },
        {
            .name       = "matlock_tex_path",
            .type       = CONFIG_OPTION_TYPE_STR,
            .optional   = 0,
            .str.value  = &core_asset_config.matlock_tex_path
        },
        {
            .name       = "animator_screen_direction_arrow_tex_id",
            .type       = CONFIG_OPTION_TYPE_INT32,
            .optional   = 0,
            .u32.value  =
                &core_asset_config.animator_screen_direction_arrow_tex_id
        },
        {
            .name       = "ui_spritesheet_id",
            .type       = CONFIG_OPTION_TYPE_UINT32,
            .optional   = 0,
            .u32.value  = &core_asset_config.ui_spritesheet_id
        },
        {
            .name       = "icons_spritesheet_id",
            .type       = CONFIG_OPTION_TYPE_UINT32,
            .optional   = 0,
            .u32.value  = &core_asset_config.icons_spritesheet_id
        }
    };
    if (parse_cfg_file_with_option_list("assets/asset_config.cfg",
        opts, (uint32)(sizeof(opts) / sizeof(opts[0]))))
        return 1;
    srand((uint)time(0));
    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO))
    {
        core_error_window("SDL_Init() failed: %s.", SDL_GetError());
        return 1;
    }
    _keyboard.keys          = SDL_GetKeyboardState(&_keyboard.num_keys);
    _keyboard.last_keys     = ecalloc(_keyboard.num_keys);
    if (_window_init(&main_window, "MUTA v. " MUTA_VERSION_STR,
        core_config.display.win_size[0], core_config.display.win_size[1],
        core_config.display.win_fullscreen, core_config.display.win_maximized,
        core_config.display.win_windowed))
        return 1;
    if (init_socket_api())
    {
        core_error_window("Failed to init native socket API.");
        return 1;
    }
    if (kth_pool_init(&_thread_pool, 2, 256) ||
        kth_pool_run(&_thread_pool))
    {
        core_error_window("Failed to initialize and start thread pool.");
        return 1;
    }
    _perf_clock_init(&_perf_clock, core_config.fps_limit);
    if (sprite_context_init(core_sprite_context, 64))
    {
        core_error_window("Failed to init sprite context.");
        return 1;
    }
    return 0;
}

void
core_destroy(void)
{
    _async_destroy();
    free(_keyboard.last_keys);
    kth_pool_destroy(&_thread_pool);
    _window_destroy(&main_window);
    dynamic_stack_destroy(&_stack);
    sprite_context_destroy(&_core_sprite_context);
    SDL_Quit();
}

int
core_start(int argc, char **argv, screen_t *first_screen)
{
    for (int i = 1; i < argc; ++i)
    {
        if (streq(argv[i], "-l") || streq(argv[i], "--login"))
        {
            if (argc < i + 3)
                break;
            if (core_auto_login_name || core_auto_login_pw)
            {
                core_auto_login_name   = 0;
                core_auto_login_pw     = 0;
                break;
            }
            if (ar_check_player_character_name(argv[i + 1],
                    strlen(argv[i + 1])))
                break;
            core_auto_login_name    = argv[i + 1];
            core_auto_login_pw      = argv[i + 2];
        }
    }
    as_bind_queued_textures();
    core_set_screen(first_screen);
    _running = 1;
    while (_running)
        core_update();
    shard_disconnect();
    login_disconnect();
    /* Clean up */
    if (_save_config("config.cfg"))
        LOG("%s: failed to save configuration!\n", __func__);
#ifdef _MUTA_DEBUG
    /* Gracefully shutdown threads first, because they may still have ongoing
     * asset load jobs on them. This way they don't get leaked. */
    kth_pool_shutdown(&_thread_pool, 1);
#else
    kth_pool_shutdown(&_thread_pool, 0);
#endif
    return 0;
}

void
core_update(void)
{
    _text_input[0] = 0;
    _perf_clock_tick(&_perf_clock);
    as_bind_queued_textures();
    au_update();
    as_update();
    /*-- Execute a queued screen swap --*/
    if (_next_screen != _current_screen)
    {
        if (_current_screen && _current_screen->close)
        {
            DEBUG_PRINTF("Closing screen %s...\n", _current_screen->name);
            _current_screen->close();
            DEBUG_PRINTF("Successfully closed screen %s.\n",
                _current_screen->name);
        }
        if (_next_screen && _next_screen->open)
        {
            gui_clear();
            _is_first_frame_of_this_screen = 1;
            DEBUG_PRINTF("Opening screen %s...\n", _next_screen->name);
            _next_screen->open();
            DEBUG_PRINTF("Successfully opened screen %s.\n",
                _next_screen->name);
        }
        _current_screen = _next_screen;
    }
    /*-- Handle network events --*/
    cl_event_t cl_events[64];
    int num_cl_events = cl_pop_events(cl_events, 64);
    for (int i = 0; i < num_cl_events; ++i)
    {
        switch (cl_events[i].type)
        {
        case CL_EVENT_DISCONNECT_LOGIN:
            login_disconnect_with_error(cl_events[i].disconnect_login.error);
            break;
        case CL_EVENT_READ_LOGIN:
            login_read(&cl_events[i].read);
            break;
        case CL_EVENT_LOGIN_CONNECTED:
            login_on_connected();
            break;
        case CL_EVENT_READ_SHARD:
            shard_read(&cl_events[i].read);
            break;
        case CL_EVENT_DISCONNECT_SHARD:
            shard_disconnect_with_error(cl_events[i].disconnect_shard.error);
            break;
        case CL_EVENT_SHARD_CONNECTED:
            shard_on_connected();
            break;
        }
    }
    /*-- Handle input events --*/
    memcpy(_keyboard.last_keys, _keyboard.keys, _keyboard.num_keys);
    _mouse_state.last_buttons   = _mouse_state.buttons;
    _mouse_state.scroll_x       = 0;
    _mouse_state.scroll_y       = 0;
    SDL_Event e;
    while (SDL_PollEvent(&e) && _running)
    {
        switch (e.type)
        {
        case SDL_QUIT:
            core_stop();
            break;
        case SDL_KEYDOWN:
            if (_current_screen->keydown)
                _current_screen->keydown(
                    SDL_GetScancodeFromKey(e.key.keysym.sym), 0);
            int len = (int)strlen(_text_input);
            switch (e.key.keysym.sym)
            {
            #define INSERT_GUI_CHAR(name) \
                if (len >= sizeof(_text_input) - 1) \
                    break; \
                _text_input[len]        = name; \
                _text_input[len + 1]    = 0;
            case SDLK_LEFT:
                if (_keyboard.keys[SDL_SCANCODE_LCTRL])
                    {INSERT_GUI_CHAR(GUI_CHAR_LONG_LEFT);}
                else
                    {INSERT_GUI_CHAR(GUI_CHAR_LEFT);}
                break;
            case SDLK_RIGHT:
                if (_keyboard.keys[SDL_SCANCODE_LCTRL])
                    {INSERT_GUI_CHAR(GUI_CHAR_LONG_RIGHT);}
                else
                    {INSERT_GUI_CHAR(GUI_CHAR_RIGHT);}
                break;
            case SDLK_BACKSPACE:
                if (_keyboard.keys[SDL_SCANCODE_LCTRL])
                    {INSERT_GUI_CHAR(GUI_CHAR_LONG_BACKSPACE);}
                else
                    {INSERT_GUI_CHAR(GUI_CHAR_BACKSPACE);}
                break;
            case SDLK_DELETE:
                if (_keyboard.keys[SDL_SCANCODE_LCTRL])
                    {INSERT_GUI_CHAR(GUI_CHAR_LONG_DELETE);}
                else
                    {INSERT_GUI_CHAR(GUI_CHAR_DELETE);}
                break;
            case SDLK_RETURN:
                INSERT_GUI_CHAR('\n');
                break;
            case SDLK_HOME:
                INSERT_GUI_CHAR(GUI_CHAR_LINE_BEGIN);
                break;
            case SDLK_END:
                INSERT_GUI_CHAR(GUI_CHAR_LINE_END);
                break;
            #undef INSERT_GUI_CHAR
            }
            break;
        case SDL_KEYUP:
            if (_current_screen->keyup)
                _current_screen->keyup(
                    SDL_GetScancodeFromKey(e.key.keysym.sym));
            break;
        case SDL_MOUSEWHEEL:
            if (_current_screen->mousewheel)
                _current_screen->mousewheel(e.wheel.x, e.wheel.y);
            _mouse_state.scroll_x += e.wheel.x;
            _mouse_state.scroll_y += e.wheel.y;
            break;
        case SDL_TEXTINPUT:
            if (_current_screen->text_input)
                _current_screen->text_input(e.text.text);
            strncat(_text_input, e.text.text,
                sizeof(_text_input) - strlen(_text_input) - 1);
            break;
        case SDL_WINDOWEVENT:
            _handle_window_event(&e);
            break;
        case SDL_MOUSEBUTTONDOWN:
        {
            click_event_t ce = {
                .button = e.button.button,
                .x      = e.button.x,
                .y      = e.button.y,
                .down   = 1};
            _post_click_event(ce);
            _mouse_state.buttons |= SDL_BUTTON(ce.button);
        }
            break;
        case SDL_MOUSEBUTTONUP:
        {
            click_event_t ce = {
                .button = e.button.button,
                .x      = e.button.x,
                .y      = e.button.y,
                .down   = 0};
            _post_click_event(ce);
            _mouse_state.buttons &= ~SDL_BUTTON(ce.button);
        }
            break;
        case SDL_DROPFILE:
            if (_current_screen->file_drop)
                _current_screen->file_drop(e.drop.file);
            break;
        }
    }
    /*-- Update mouse state --*/
    _mouse_state.last_x = _mouse_state.x;
    _mouse_state.last_y = _mouse_state.y;
    int mx, my;
    _mouse_state.buttons    = SDL_GetMouseState(&mx, &my);
    _mouse_state.x          = mx;
    _mouse_state.y          = my;
    /*-- Flush queued network messages --*/
    login_flush();
    shard_flush(_perf_clock.delta);
    /*-- Update the current screen --*/
    _update_gui_input_state();
    gui_begin();
    if (_current_screen && _current_screen->update)
        _current_screen->update(_perf_clock.delta);
    _is_first_frame_of_this_screen = 0;
    /*-- This may have been called by the screen before --*/
    core_execute_click_events();
    gui_end();
    r_render_gui();
    gl_swap_buffers(main_window.sdl_window);
}

int
core_stop(void)
{
    DEBUG_PRINTF("Shutdown called.\n");
    _running = 0;
    return 0;
}

void
core_execute_click_events(void)
{
    int             num = _click_events.num;
    click_event_t   *ev;
    for (int i = 0; i < num; ++i)
    {
        ev = &_click_events.events[i];
        if (ev->down)
        {
            if (_current_screen->mousebuttondown)
                _current_screen->mousebuttondown(SDL_BUTTON(ev->button), ev->x,
                    ev->y);
        } else
        if (_current_screen->mousebuttonup)
            _current_screen->mousebuttonup(SDL_BUTTON(ev->button), ev->x,
                ev->y);
    }
    _click_events.num = 0;
}

float
core_compute_pos_scaled_by_target_viewport(int *x, int *y)
{
    int vp[4];
    float s = core_compute_target_viewport(vp);
    *x -= vp[0];
    *y -= vp[1];
    if (s != 1 && s != 0)
    {
        float t = 1 / s;
        *x = (int)(*x * t);
        *y = (int)(*y * t);
    }
    return s;
}

float
core_compute_target_viewport_scale(void)
{
    float sx    = (float)main_window.w / (float)core_asset_config.resolution_w;
    float sy    = (float)main_window.h / (float)core_asset_config.resolution_h;
    float s     = sx > sy ? sy : sx;
    if (core_config.display.stepped_resolution_scale)
    {
        if (s >= 1.f)
            s = floorf(s);
        else
            s = s - (float)(int)(s);
    }
    return s;
}

float
core_compute_target_viewport(int *ret)
{
    float s = core_compute_target_viewport_scale();
    ret[2] = (int)(s * (float)(core_asset_config.resolution_w));
    ret[3] = (int)(s * (float)(core_asset_config.resolution_h));
    ret[0] = main_window.w / 2 - ret[2] / 2;
    ret[1] = main_window.h / 2 - ret[3] / 2;
    return s;
}

void
core_window_to_gui_coords(int *ret_in_x, int *ret_in_y)
{
    int     vp[4];
    float   sc = core_compute_target_viewport(vp);
    sc = MAX(sc, 0.0000001f);
    *ret_in_x = (int)((float)(*ret_in_x - vp[0]) / sc);
    *ret_in_y = (int)((float)(*ret_in_y - vp[1]) / sc);
}

void
core_set_cursor(cursor_t *cursor)
    {SDL_SetCursor(cursor);}

bool32
core_mouse_button_down(int button)
    {return _mouse_state.buttons & button;}

bool32
core_mouse_button_down_now(int button)
{
    return _mouse_state.buttons & button &&
        !(_mouse_state.last_buttons & button);
}

bool32
core_mouse_button_up_now(int button)
{
    return _mouse_state.last_buttons & button &&
        !(_mouse_state.buttons & button);
}

int
core_mouse_x(void)
    {return _mouse_state.x;}

int
core_mouse_y(void)
    {return _mouse_state.y;}

bool32
core_key_down(int key)
{
    return ((key) >= 0 && (key) < _keyboard.num_keys ?
        _keyboard.keys[(key)] : 0);
}

bool32
core_key_down_now(int key)
{
    if (!_keyboard.num_keys || key < 0 || key >= _keyboard.num_keys)
        return 0;
    return _keyboard.keys[key] && !_keyboard.last_keys[key];
}

bool32
core_key_up_now(int key)
{
    if (!_keyboard.num_keys || key < 0 || key >= _keyboard.num_keys)
        return 0;
    return !_keyboard.keys[key] && _keyboard.last_keys[key];
}

void *
core_window_data(void)
    {return main_window.sdl_window;}

int
core_window_w(void)
    {return main_window.w;}

int
core_window_h(void)
    {return main_window.h;}

void
core_error_window(const char *error_msg, ...)
{
    char        buf[4096];
    const char  *msg;
    if (error_msg)
    {
        va_list args;
        va_start(args, error_msg);
        vsnprintf(buf, sizeof(buf), error_msg, args);
        va_end(args);
        msg = buf;
    } else
        msg = "Unknown error.";
    fprintf(stderr, "%s\n", msg);
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
        "MUTA has encountered an error!", msg, main_window.sdl_window);
}

size_t
core_stack_push(size_t num_bytes)
{
    void *ret = dynamic_stack_push_static(&_stack, num_bytes);
    if (!ret)
        muta_panic_print("Not enough stack memory!");
    return dynamic_stack_offset(&_stack, ret);
}

void
core_stack_pop(size_t offset)
    {dynamic_stack_pop(&_stack, offset);}

void *
core_get_stack_at(size_t offset)
    {return dynamic_stack_at(&_stack, offset);}

async_job_t *
core_run_async(void (*callback)(void *args), void *args)
{
    async_job_t *job = obj_pool_reserve(&_async.job_pool);
    job->callback   = callback;
    job->args       = args;
    job->completed  = 0;
    if (kth_pool_add_job(&_thread_pool, _async_callback, job))
        muta_panic_print("Failed to run async job!");
    return job;
}

void
core_run_async_and_forget(void (*callback)(void *args), void *args)
{
    if (kth_pool_add_job(&_thread_pool, callback, args))
        muta_panic_print("Failed to run async job!");
}

int
core_async_wait(async_job_t *job, void **ret_args, int timeout_ms)
{
    muta_assert(timeout_ms >= -1);
    if (!job->completed)
    {
        if (timeout_ms == -1)
        {
            while (!job->completed)
            {
                async_job_t *jobs[16];
                int         num_jobs = event_wait(&_async.event_buf, jobs, 16,
                    timeout_ms);
                for (int i = 0; i < num_jobs; ++i)
                    jobs[i]->completed = 1;
            }
        } else
        {
            do
            {
                uint32      time_now = SDL_GetTicks();
                async_job_t *jobs[16];
                int         num_jobs = event_wait(&_async.event_buf, jobs, 16,
                    timeout_ms);
                for (int i = 0; i < num_jobs; ++i)
                    jobs[i]->completed = 1;
                uint32 time_passed = SDL_GetTicks() - time_now;
                timeout_ms -= (int)time_passed;
            } while (!job->completed && timeout_ms > 0);
            if (!job->completed)
                return 1;
        }
    }
    if (ret_args)
        *ret_args = job->args;
    obj_pool_free(&_async.job_pool, job);
    return 0;
}

void *
core_malloc(size_t size)
{
    muta_assert(size <= 0xFFFFFFFF);
    return segpool_malloc(&_segpool, (uint32)size);
}

void
core_free(void *p)
    {segpool_free(&_segpool, p);}

bool32
core_is_first_frame_of_current_screen(void)
    {return _is_first_frame_of_this_screen;}

static void
_update_gui_input_state(void)
{
    gui_input_state_t *is = gui_get_input_state();
    int     vp[4];
    float   sc = core_compute_target_viewport(vp);
    sc = MAX(sc, 0.0000001f);
    uint32 s    = _mouse_state.buttons;
    is->mouse_position[0] = (int)((float)(_mouse_state.x - vp[0]) / sc);
    is->mouse_position[1] = (int)((float)(_mouse_state.y - vp[1]) / sc);
    is->mouse_buttons = 0;
    is->mouse_buttons |= ((s & CORE_MOUSE_BUTTON_LEFT) ?
        GUI_MOUSE_BUTTON_LEFT : 0);
    is->mouse_buttons |= ((s & CORE_MOUSE_BUTTON_RIGHT) ?
        GUI_MOUSE_BUTTON_RIGHT : 0);
    is->mouse_buttons |= ((s & CORE_MOUSE_BUTTON_MIDDLE) ?
        GUI_MOUSE_BUTTON_MIDDLE : 0);
    is->mouse_scroll[0]     = _mouse_state.scroll_x;
    is->mouse_scroll[1]     = _mouse_state.scroll_y;
    is->coordinate_space[0] = 0;
    is->coordinate_space[1] = 0;
    is->coordinate_space[2] = core_asset_config.resolution_w;
    is->coordinate_space[3] = core_asset_config.resolution_h;
    strncpy(is->text_input, _text_input, sizeof(is->text_input) - 1);
    is->delta_time = (float)_perf_clock.delta;
}

static void
_read_config(const char *path, bool32 editor_mode_overridden,
    bool32 editor_mode)
{
    /* Set default values */
    core_config.display.win_size[0]                 = 800;
    core_config.display.win_size[1]                 = 480;
    core_config.display.win_fullscreen              = 1;
    core_config.display.win_windowed                = 1;
    core_config.display.win_maximized               = 1;
    core_config.display.stepped_resolution_scale    = 0;
    core_config.vsync                               = 1;
    core_config.fps_limit                           = 300;
    core_config.sprite_batch_size                   = 65536;
    core_config.music_volume                        = 0.25f;
    core_config.sound_volume                        = 0.25f;
    if (editor_mode_overridden)
        core_config.editor_mode = editor_mode;
    else
        core_config.editor_mode = 0;
    memset(core_config.account_name, 0, sizeof(core_config.account_name));
    config_context_t context =
        {.editor_mode_overridden = editor_mode_overridden};
    parse_cfg_file(path, _read_cfg_callback, &context);
}

static void
_perf_clock_init(perf_clock_t *clock, uint32 fps_limit)
{
    clock->delta        = 0;
    clock->last_tick    = 0;
    clock->fps          = 0;
    clock->frame_num    = 0;
    clock->target_fps   = fps_limit;
}

static void
_perf_clock_tick(perf_clock_t *clock)
{
    uint64 tick_time = SDL_GetPerformanceCounter();
    uint64 perf_freq = SDL_GetPerformanceFrequency();
    if (clock->last_tick != 0.0f)
        clock->delta = (double)(tick_time - clock->last_tick) / (double)perf_freq;
    else
        clock->delta = 0.016f;
    clock->last_tick = tick_time;
    if (clock->delta > 0.0f)
        clock->fps = (uint32)(1.0f / clock->delta);
    if (clock->target_fps != 0)
    {
        double target_time = 1.0f / (double)clock->target_fps * 1000.0f;
        if ((double)(tick_time - clock->last_tick) < target_time)
        {
            uint64 substraction = tick_time - clock->last_tick;
            sleep_ms((uint)(target_time - substraction));
        }
    }
    clock->frame_num++;
}

static int
_window_init(window_t *win, const char *title, int w, int h, bool32 fullscreen,
    bool32 maximized, bool32 windowed)
{
    if (w < 1 || h < 1)
        return 1;
    int flags = SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN;
    if (fullscreen)
    {
        if (windowed) /* Windowed fullscreen */
        {
            flags |= SDL_WINDOW_BORDERLESS;
            flags |= SDL_WINDOW_MAXIMIZED;
        } else
            flags |= SDL_WINDOW_FULLSCREEN;
    } else
    {
        flags |= maximized ? SDL_WINDOW_MAXIMIZED : 0;
        flags |= SDL_WINDOW_RESIZABLE;
    }
    win->sdl_window = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED, w, h, flags);
    if (!win->sdl_window)
    {
        core_error_window("SDL_CreateWindow() failed: %s.", SDL_GetError());
        return 1;
    }
    SDL_GetWindowSize(win->sdl_window, &win->w, &win->h);
    return 0;
}

static void
_async_init(void)
{
    int num = 64;
    obj_pool_init(&_async.job_pool, num, sizeof(async_job_t));
    event_init(&_async.event_buf, sizeof(async_job_t*), num);
}

static void
_async_destroy(void)
{
    obj_pool_destroy(&_async.job_pool);
    event_destroy(&_async.event_buf);
}

static void
_window_destroy(window_t *win)
{
    SDL_DestroyWindow(win->sdl_window);
    memset(win, 0, sizeof(window_t));
}

static int
_window_maximize(window_t *win)
{
    if (!win)           return 1;
    if (!win->sdl_window)  return 2;
    SDL_MaximizeWindow(win->sdl_window);
    SDL_GetWindowSize(win->sdl_window, &win->w, &win->h);
    return 0;
}

static void
_read_cfg_callback(void *ctx, const char *opt, const char *val)
{
    config_context_t *context = ctx;
    if (streq(opt, "window fullscreen"))
    {
        bool32 v;
        if (!str_to_bool(val, &v))
            core_config.display.win_fullscreen = v;
    } else
    if (streq(opt, "window size"))
    {
        int w, h;
        if (sscanf(val, "%dx%d", &w, &h) != 2) return;
        if (w < 1 || h < 1)
            return;
        core_config.display.win_size[0] = w;
        core_config.display.win_size[1] = h;
        printf("cfg: %s = %dx%d\n", opt, core_config.display.win_size[0],
            core_config.display.win_size[1]);
    } else
    if (streq(opt, "vsync"))
    {
        bool32 v;
        if (!str_to_bool(val, &v)) core_config.vsync = v;
        printf("cfg: %s = %s\n", opt, val);
    } else
    if (streq(opt, "fps limit"))
    {
        int v = atoi(val);
        if (v > 0) core_config.fps_limit = v;
    } else
    if (streq(opt, "sound volume"))
    {
        float v = (float)atoi(val) / 100.f;
        if (v >= 0.f && v <= 1.f)
            core_config.sound_volume = v;
    } else
    if (streq(opt, "music volume"))
    {
        float v = (float)atoi(val) /  100.f;
        if (v >= 0.f && v <= 1.f)
            core_config.music_volume = v;
    } else
    if (streq(opt, "sprite batch size"))
    {
        int v = atoi(val);
        if (v > 0) core_config.sprite_batch_size = v;
    } else
    if (streq(opt, "account"))
    {
        int len = (int)strlen(val);
        if (!ar_check_player_character_name(val, len))
            memcpy(core_config.account_name, val, len + 1);
    } else
    if (streq(opt, "window maximized"))
    {
        bool32 v;
        if (!str_to_bool(val, &v))
            core_config.display.win_maximized = v;
    } else
    if (streq(opt, "window windowed"))
    {
        bool32 v;
        if (!str_to_bool(val, &v))
            core_config.display.win_windowed = v;
    } else
    if (streq(opt, "stepped resolution scale"))
    {
        bool32 v;
        if (!str_to_bool(val, &v))
            core_config.display.stepped_resolution_scale = v;
    } else
    if (streq(opt, "editor_mode"))
    {
        if (!context->editor_mode_overridden)
        {
            bool32 v;
            if (!str_to_bool(val, &v))
                core_config.editor_mode = v;
        }
    }
}

static int
_post_click_event(click_event_t ev)
{
    if (_click_events.num == MAX_CLICK_EVENTS)
        return 1;
    _click_events.events[_click_events.num++] = ev;
    return 0;
}

static void
_handle_window_event(SDL_Event *ev)
{
    switch (ev->window.event)
    {
    case SDL_WINDOWEVENT_SIZE_CHANGED:
    {
        main_window.w = ev->window.data1;
        main_window.h = ev->window.data2;
        gl_viewport(0, 0, main_window.w, main_window.h);
        gl_scissor(0, 0, main_window.w, main_window.h);
    }
        break;
    case SDL_WINDOWEVENT_MINIMIZED:
        break;
    case SDL_WINDOWEVENT_MAXIMIZED:
        break;
    }
}

static int
_save_config(const char *path)
{
    FILE *f = fopen(path, "w+");
    if (!f)
        return 1;
    fprintf(f, "window fullscreen       = %d\n",
        core_config.display.win_fullscreen);
    fprintf(f, "window size             = %dx%d\n",
        core_config.display.win_size[0], core_config.display.win_size[1]);
    fprintf(f, "window windowed         = %d\n",
        core_config.display.win_windowed);
    fprintf(f, "window maximized        = %d\n",
        core_config.display.win_maximized);
    fprintf(f, "vsync                   = %d\n", core_config.vsync);
    fprintf(f, "stepped resolution scale= %s\n",
        core_config.display.stepped_resolution_scale ? "true" : "false");
    fprintf(f, "sound volume            = %d\n",
        (int)(core_config.sound_volume * 100.f));
    fprintf(f, "music volume            = %d\n",
        (int)(core_config.music_volume * 100.f));
    fprintf(f, "sprite batch size       = %d\n", core_config.sprite_batch_size);
    fprintf(f, "account                 = %s\n", core_config.account_name);
    fclose(f);
    return 0;
}

static void
_panic_callback(const char *error_msg)
{
    const char *msg = error_msg ? error_msg : "Unknown error.";
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "MUTA has panicked!", msg,
        main_window.sdl_window);
    SET_BREAKPOINT();
    exit(1);
}

static void
_async_callback(void *args)
{
    async_job_t *job = args;
    job->callback(job->args);
    event_push(&_async.event_buf, &job, 1);
}

cursor_t *
core_new_unmanaged_cursor(img_t *img, float clip[4])
{
    SDL_Surface *surface    = 0;
    SDL_Cursor  *cursor     = 0;
    uint32      r           = TO_SYS_ENDIAN32(0x000000ff);
    uint32      g           = TO_SYS_ENDIAN32(0x0000ff00);
    uint32      b           = TO_SYS_ENDIAN32(0x00ff0000);
    uint32      a           = TO_SYS_ENDIAN32(0xff000000);
    size_t      pixel_size  = img->pxf == IMG_RGB ? 3 : 4;
    int         w           = clip[2] - clip[0];
    int         h           = clip[3] - clip[1];
    uint8       *pixels     = emalloc(w * h * pixel_size);
    for (int i = 0; i < h; ++i)
    {
        uint8 *dst = &pixels[i * w * pixel_size];
        uint8 *src = &img->pixels[i * (img->w * pixel_size) + (int)clip[0] * pixel_size];
        memcpy(dst, src, w * pixel_size);
    }
    surface = SDL_CreateRGBSurfaceFrom(pixels, w, h, img->pxf == IMG_RGB ? 24 : 32, pixel_size * w, r, g, b, a);
    if (!surface)
        goto out;
    cursor = SDL_CreateColorCursor(surface, 0, 0);
    out:
        SDL_FreeSurface(surface);
        free(pixels);
        return cursor;
}

void
core_free_unmanaged_cursor(cursor_t *cursor)
    {SDL_FreeCursor(cursor);}
