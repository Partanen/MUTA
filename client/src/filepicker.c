#include "filepicker.h"
#include "gui_styles.h"
#include "gui.h"
#include "../../shared/common_utils.h"

static void
_clear_files(filepicker_t *fp);

static int
_compare_alpha(const void *a, const void *b);

void
fpick_init(filepicker_t *fp, int flags)
{
    uint32 num_chars = 256;
    fp->current_directory   = dstr_create_empty(num_chars);
    fp->next_directory      = dstr_create_empty(num_chars);
    fp->tmp_str             = dstr_create_empty(num_chars);
    fp->files               = 0;
    fp->current_file        = 0;
    fp->flags               = flags;
}

void
fpick_destroy(filepicker_t *fp)
{
    _clear_files(fp);
    dstr_free(&fp->current_directory);
    dstr_free(&fp->next_directory);
    dstr_free(&fp->tmp_str);
    darr_free(fp->files);
}

int
fpick_change_directory(filepicker_t *fp, const char *path)
{
    dir_entry_t     entry;
    dir_handle_t    handle = open_directory(path, &entry);
    if (handle == INVALID_DIR_HANDLE)
    {
        DEBUG_PRINTFF("Could not open directory '%s'.\n", path);
        return 1;
    }
    _clear_files(fp);
    dstr_set(&fp->current_directory, path);
    dstr_set(&fp->next_directory, path);
    do
    {
        if (streq(get_dir_entry_name(&entry), ".") ||
            streq(get_dir_entry_name(&entry), ".."))
            continue;
        dchar *s = dstr_create(get_dir_entry_name(&entry));
        darr_push(fp->files, s);
        DEBUG_PRINTFF("File: %s.\n", s);
    } while (!get_next_file_in_directory(handle, &entry));
    close_directory(handle);
    qsort(fp->files, darr_num(fp->files), sizeof(dchar*), _compare_alpha);
    return 0;
}

size_t
fpick_num_files(filepicker_t *fp)
    {return darr_num(fp->files);}

const char *
fpick_get_file(filepicker_t *fp, size_t index)
{
    if (index < (size_t)darr_num(fp->files))
        return fp->files[index];
    return 0;
}

bool32
fpick_draw(filepicker_t *fp, const char *title, int x, int y, int w, int h,
    dchar **ret_file_path)
{
    int ret             = 0;
    int original_origin = gui_get_origin();
    gui_win_style(0);
    gui_begin_win(title, x, y, w, h, 0);
    gui_origin(GUI_TOP_LEFT);
    float   bar_percentage      = 0.08f;
    int     bar_h               = (int)(bar_percentage * (float)h);
    int     bottom_button_h     = (int)(0.8f * (float)bar_h);
    int     cancel_button_w     = gui_get_current_win_viewport_w() / 5;
    int     cur_dir_button_w    = gui_get_current_win_viewport_w() / 4;
    gui_begin_empty_win(gui_format_id("%s##scroll area", title), 0, bar_h,
        gui_get_current_win_viewport_w(),
        gui_get_current_win_viewport_h() - 2 * bar_h,
        GUI_WIN_SCROLLABLE);
    size_t      num_files   = fpick_num_files(fp);
    gui_font_t  *font       = gui_get_default_font();
    int         button_x    = 0;
    int         button_y    = 0;
    int         button_w    = gui_get_current_win_viewport_w();
    int         button_h    = font->height + (int)(0.5f * font->height);
    gui_button_style(&gui_style_file_list_button);
    for (size_t i = 0; i < num_files; ++i)
    {
        const char *file_name = fpick_get_file(fp, i);
        if (gui_button(gui_format_id("%s##fpick%zu", file_name, i),
            button_x, button_y, button_w, button_h, 0))
        {
            /* Try to go into the directory (if entry is one). */
            dstr_set(&fp->tmp_str, fp->current_directory);
            dstr_append(&fp->tmp_str, "/");
            dstr_append(&fp->tmp_str, file_name);
            if (!fpick_change_directory(fp, fp->tmp_str))
                continue;
            if (fp->flags & FPICK_ALLOW_PICK_FILE)
            {
                /* Return filepath to the user. */
                if (!*ret_file_path)
                    *ret_file_path = dstr_create(fp->current_directory);
                else
                    dstr_set(ret_file_path, fp->current_directory);
                uint32 len = dstr_len(*ret_file_path);
                if (len > 0 && (*ret_file_path)[len - 1] != '/')
                    dstr_append(ret_file_path, "/");
                dstr_append(ret_file_path, file_name);
                ret = 1;
            }
        }
        button_y += button_h;
    }
    gui_end_win();
    int top_button_w = bar_h;
    int top_button_h = bar_h;
    gui_button_style(&gui_style_typical_button);
    /* Back button */
    gui_origin(GUI_TOP_LEFT);
    if (gui_button("<##fpick", 0, 0,
        top_button_w, top_button_h, 0) && dstr_len(fp->current_directory))
    {
        dchar *relative_path = dstr_create(fp->current_directory);
        dstr_append(&relative_path, "/..");
        char *absolute_path = get_absolute_path(relative_path);
        fpick_change_directory(fp, absolute_path);
        dstr_free(&relative_path);
        free(absolute_path);
    }
    gui_origin(GUI_TOP_LEFT);
    /* Path text input */
    if (gui_text_input_dstr("##fpickpath", &fp->next_directory,
        top_button_w, 0,
        gui_get_current_win_viewport_w() - 2 * top_button_w, bar_h, 0) &&
        gui_text_input_enter_pressed())
    {
        if (!fpick_change_directory(fp, fp->next_directory))
            gui_set_active_text_input(0);
    }
    gui_origin(GUI_TOP_RIGHT);
    /* Go button */
    if (gui_button("Go##fpick", 0, 0, top_button_w, top_button_h, 0))
        fpick_change_directory(fp, fp->next_directory);
    gui_origin(GUI_BOTTOM_LEFT);
    int bottom_button_x =(int)(0.01f *
        (float)(gui_get_current_win_viewport_w() - cancel_button_w));
    int bottom_button_y = (int)(0.5f * (float)(bar_h - bottom_button_h));
    if (gui_button("Cancel##file picker", bottom_button_x, bottom_button_y,
        cancel_button_w, bottom_button_h, 0))
        ret = -1;
    gui_origin(GUI_BOTTOM_RIGHT);
    if (fp->flags & FPICK_ALLOW_PICK_DIRECTORY &&
        gui_button("Open current directory##fpick", bottom_button_x,
        bottom_button_y, cur_dir_button_w, bottom_button_h, 0))
    {
        if (!*ret_file_path)
            *ret_file_path = dstr_create(fp->current_directory);
        else
            dstr_set(ret_file_path, fp->current_directory);
        ret = 1;
    }
    gui_origin(GUI_BOTTOM_LEFT);
    gui_end_win();
    gui_origin(original_origin);
    return ret;
}

static void
_clear_files(filepicker_t *fp)
{
    uint32 num_files = darr_num(fp->files);
    for (uint32 i = 0; i < num_files; ++i)
        dstr_free(&fp->files[i]);
    darr_clear(fp->files);
}

static int
_compare_alpha(const void *a, const void *b)
{
    dchar * const *file1 = a;
    dchar * const *file2 = b;
    return strcmp(*file1, *file2);
}
