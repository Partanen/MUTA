#include "hotkey.h"
#include "core.h"
#include "log.h"
#include "event.h"
#include "../../shared/str_hashtable.h"
#include "../../shared/common_utils.h"

typedef struct str_hk_action_table      str_hk_action_table_t;
typedef struct uint64_hk_action_table   uint64_hk_action_table_t;
typedef struct str_int_table            str_int_table_t;
typedef struct int_str_table            int_str_table_t;
typedef struct int_bool8_table          int_bool8_table_t;

STR_HASHTABLE_DEFINITION(str_hk_action_table, hk_action_t*);
hashtable_define(uint64_hk_action_table, uint64, hk_action_t*);
STR_HASHTABLE_DEFINITION(str_int_table, int);
hashtable_define(int_str_table, int, const char*);
hashtable_define(int_bool8_table, int, bool8);

static obj_pool_t               _action_pool;
static str_hk_action_table_t    _action_table;
static uint64_hk_action_table_t _int_action_table;
static str_int_table_t          _key_str_enum_table;
static int_str_table_t          _key_enum_str_table;
static hk_action_t              **_all_actions;
static hk_action_t **_repeat_actions; /* A separate array is kept for polling */
static uint32                   _running_action_id;

static inline hk_action_t *
_get_action_by_name(const char *name)
{
    hk_action_t **ret = str_hk_action_table_find(&_action_table, name);
    return ret ? *ret : 0;
}

static int
_on_action_def(parse_def_file_context_t *ctx, const char *def, const char *val);

static int
_on_action_val(parse_def_file_context_t *ctx, const char *def, const char *val);

static uint64
_hash_from_key_combo(int key_enum, int mods);

static void
_create_key_enum(const char *name, int key_enum)
{
    DEBUG_PRINTFF("Created key %s.\n", name);
    str_int_table_einsert(&_key_str_enum_table, name, key_enum);
    int_str_table_einsert(&_key_enum_str_table, key_enum, name);
}

int
hk_init(void)
{
    str_hk_action_table_einit(&_action_table, 128);
    uint64_hk_action_table_einit(&_int_action_table, 128);
    obj_pool_init(&_action_pool, 128, sizeof(hk_action_t));
    str_int_table_einit(&_key_str_enum_table, 128);
    int_str_table_einit(&_key_enum_str_table, 128);
    darr_reserve(_all_actions, 128);
    darr_reserve(_repeat_actions, 32);
    _create_key_enum("Q", CORE_KEY_Q);
    _create_key_enum("W", CORE_KEY_W);
    _create_key_enum("E", CORE_KEY_E);
    _create_key_enum("R", CORE_KEY_R);
    _create_key_enum("T", CORE_KEY_T);
    _create_key_enum("Y", CORE_KEY_Y);
    _create_key_enum("U", CORE_KEY_U);
    _create_key_enum("I", CORE_KEY_I);
    _create_key_enum("O", CORE_KEY_O);
    _create_key_enum("P", CORE_KEY_P);
    _create_key_enum("A", CORE_KEY_A);
    _create_key_enum("S", CORE_KEY_S);
    _create_key_enum("D", CORE_KEY_D);
    _create_key_enum("F", CORE_KEY_F);
    _create_key_enum("G", CORE_KEY_G);
    _create_key_enum("H", CORE_KEY_H);
    _create_key_enum("J", CORE_KEY_J);
    _create_key_enum("K", CORE_KEY_K);
    _create_key_enum("L", CORE_KEY_L);
    _create_key_enum("Z", CORE_KEY_Z);
    _create_key_enum("X", CORE_KEY_X);
    _create_key_enum("C", CORE_KEY_C);
    _create_key_enum("V", CORE_KEY_V);
    _create_key_enum("B", CORE_KEY_B);
    _create_key_enum("N", CORE_KEY_N);
    _create_key_enum("M", CORE_KEY_M);
    _create_key_enum("Escape", CORE_KEY_ESCAPE);
    _create_key_enum("Enter", CORE_KEY_RETURN);
    _create_key_enum("F1", CORE_KEY_F1);
    _create_key_enum("F2", CORE_KEY_F2);
    _create_key_enum("F3", CORE_KEY_F3);
    _create_key_enum("F4", CORE_KEY_F4);
    _create_key_enum("F5", CORE_KEY_F5);
    _create_key_enum("F6", CORE_KEY_F6);
    _create_key_enum("F7", CORE_KEY_F7);
    _create_key_enum("F8", CORE_KEY_F8);
    _create_key_enum("F9", CORE_KEY_F9);
    _create_key_enum("F10", CORE_KEY_F10);
    _create_key_enum("F11", CORE_KEY_F11);
    _create_key_enum("F12", CORE_KEY_F12);
    _create_key_enum("F13", CORE_KEY_F13);
    _create_key_enum("0", CORE_KEY_0);
    _create_key_enum("1", CORE_KEY_1);
    _create_key_enum("2", CORE_KEY_2);
    _create_key_enum("3", CORE_KEY_3);
    _create_key_enum("4", CORE_KEY_4);
    _create_key_enum("5", CORE_KEY_5);
    _create_key_enum("6", CORE_KEY_6);
    _create_key_enum("7", CORE_KEY_7);
    _create_key_enum("8", CORE_KEY_8);
    _create_key_enum("9", CORE_KEY_9);
    hk_clear();
    return 0;
}

void
hk_destroy(void)
{
    obj_pool_destroy(&_action_pool);
    str_hk_action_table_destroy(&_action_table);
    uint64_hk_action_table_destroy(&_int_action_table);
    str_int_table_destroy(&_key_str_enum_table);
    darr_free(_all_actions);
    darr_free(_repeat_actions);
}

void
hk_clear(void)
    {str_hk_action_table_clear(&_action_table);}

int
hk_load_from_file(const char *path)
{
    hk_action_t *action = 0;
    enum parse_def_file_result result = parse_def_file(path, _on_action_def,
        _on_action_val, &action);
    int ret = 0;
    switch (result)
    {
    case PARSE_DEF_FILE_SUCCESS:
    case PARSE_DEF_FILE_FOPEN_FAILED:
        break;
    case PARSE_DEF_FILE_SYNTAX_ERROR:
        LOG_ERR("Syntax errors in hotkey file '%s'.\n", path);
        ret = 1;
        break;
    }
    return ret;
}

int
hk_save_to_file(const char *path)
{
    FILE *f = fopen(path, "w+");
    if (!f)
    {
        DEBUG_PRINTFF("Failed to open file '%s'.\n", path);
        return 1;
    }
    hk_action_t **actions   = _all_actions;
    uint32      num_actions = darr_num(actions);
    for (uint32 i = 0; i < num_actions; ++i)
    {
        hk_action_t *action = actions[i];
        if (action->keys[0].keycode == -1)
            continue;
        DEBUG_PRINTFF("NOT -1\n");
        if (i > 0)
            fputs("\n\n", f);
        char buf[HK_MAX_ACTION_NAME_LEN + 1];
        hk_key_combo_to_str(action->keys[0].keycode, action->keys[0].mods,
            buf);
        fprintf(f, "action: %s\n    key1 = %s", action->name, buf);
        if (action->keys[1].keycode != -1)
        {
            hk_key_combo_to_str(action->keys[1].keycode, action->keys[1].mods,
                buf);
            fprintf(f, "\nkey2 = %s", buf);
        }
    }
    fclose(f);
    return 0;
}

int
hk_new_action(const char *name, void (*callback)(void *user_data),
    void *user_data, int press_event_type)
{
    muta_assert(press_event_type >= 0 &&
        press_event_type < NUM_HK_PRESS_EVENT_TYPES);
    if (!name)
        return 1;
    if (strlen(name) > HK_MAX_ACTION_NAME_LEN)
        return 2;
    if (_get_action_by_name(name))
        return 0;
    hk_action_t *action = obj_pool_reserve(&_action_pool);
    action->id = _running_action_id++;
    strncpy(action->name, name, sizeof(action->name));
    action->keys[0].keycode     = -1;
    action->keys[1].keycode     = -1;
    action->callback            = callback;
    action->user_data           = user_data;
    action->press_event_type    = press_event_type;
    str_hk_action_table_einsert(&_action_table, name, action);
    darr_push(_all_actions, action);
    if (press_event_type == HK_PRESS_REPEAT)
        darr_push(_repeat_actions, action);
    return 0;
}

int
hk_str_to_keycode(const char *str)
{
    if (!str)
        return -1;
    int *ret = str_int_table_find(&_key_str_enum_table, str);
    return ret ? *ret : -1;
}

const char *
hk_key_enum_to_str(int key_enum)
{
    static const char *none = "";
    if (key_enum == -1)
        return none;
    const char **ret = int_str_table_find(&_key_enum_str_table, key_enum);
    return ret ? *ret : none;
}

int
hk_bind_key(const char *action_name, int index, int keycode, int mods)
{
    if (!action_name || index < 0 || index > 1)
        return 1;
    hk_action_t *action = _get_action_by_name(action_name);
    if (!action)
        return 2;
    if ((action->keys[0].keycode == keycode && action->keys[0].mods == mods) ||
        (action->keys[1].keycode == keycode && action->keys[1].mods == mods))
        return 0;
    action->keys[index].keycode = keycode;
    action->keys[index].mods    = mods;
    uint64 hash = _hash_from_key_combo(keycode, mods);
    /* Was key-mod combo bound to something before? */
    hk_action_t **old_action_ptr = uint64_hk_action_table_find(
        &_int_action_table, hash);
    if (old_action_ptr)
    {
        hk_action_t *old_action = *old_action_ptr;
        if (old_action == action)
            return 0;
        hk_key_mod_pair_t *mp;
        if (old_action->keys[0].keycode == keycode &&
            old_action->keys[0].mods == mods)
            mp = &old_action->keys[0];
        else
            mp = &old_action->keys[1];
        mp->keycode = -1;
        uint64_hk_action_table_erase(&_int_action_table, hash);
    }
    uint64_hk_action_table_einsert(&_int_action_table, hash, action);
    DEBUG_PRINTFF("Bound keycode %d to action %s.\n", keycode, action_name);
    return 0;
}

int
hk_clear_key(const char *action_name, int index)
{
    if (!action_name || index < 0 || index > 1)
        return 1;
    hk_action_t *action = _get_action_by_name(action_name);
    if (!action)
        return 2;
    if (index == 0)
    {
        action->keys[0] = action->keys[1];
    }
    action->keys[1].keycode = -1;
    action->keys[1].mods    = 0;
    return 0;
}

int
hk_bind_key_from_str(const char *action_name, int index, const char *key)
{
    int keycode, mods;
    int rr;
    if ((rr = hk_str_to_key_combo(key, &keycode, &mods)))
    {
        DEBUG_PRINTF("Bad keycombo '%s': code %d\n", key, rr);
        return 1;
    }
    int r = hk_bind_key(action_name, index, keycode, mods);
    if (r)
    {
        DEBUG_PRINTFF("hk_bind_key() failed with code %d.\n", r);
        return 2;
    }
    return 0;
}

hk_action_t *
hk_get_action(int key_enum, int mods)
{
    uint64 hash = _hash_from_key_combo(key_enum, mods);
    hk_action_t **ret = uint64_hk_action_table_find(
        &_int_action_table, hash);
    if (ret)
        return *ret;
    hash    = _hash_from_key_combo(key_enum, 0);
    ret     = uint64_hk_action_table_find(&_int_action_table, hash);
    return ret ? *ret : 0;
}

hk_action_t **
hk_get_actions(uint32 *ret_num)
{
    if (ret_num)
        *ret_num = darr_num(_all_actions);
    return _all_actions;
}

hk_action_t *
hk_get_action_by_name(const char *name)
{
    hk_action_t **action = str_hk_action_table_find(&_action_table, name);
    return action ? *action : 0;
}

hk_action_t **
hk_get_repeat_actions(uint32 *ret_num)
{
    if (ret_num)
        *ret_num = darr_num(_repeat_actions);
    return _repeat_actions;
}

int
hk_str_to_key_combo(const char *str, int *ret_keycode, int *ret_mods)
{
    if (!str)
        return 1;
    int         mods        = 0;
    const char  *s          = str;
    for (;;)
    {
        if (!strncmp(s, "s-", 2))
        {
            if (mods & HK_MOD_SHIFT)
                return 2;
            mods |= HK_MOD_SHIFT;
            s += 2;
        } else
        if (!strncmp(s, "c-", 2))
        {
            if (mods & HK_MOD_CTRL)
                return 3;
            mods |= HK_MOD_CTRL;
            s += 2;
        } else
        if (!strncmp(s, "a-", 2))
        {
            if (mods & HK_MOD_ALT)
                return 4;
            mods |= HK_MOD_ALT;
            s += 2;
        } else
            break;
    }
    /* Find the key without the mods */
    int *keycode = str_int_table_find(&_key_str_enum_table, s);
    if (!keycode)
        return 5;
    if (ret_keycode)
        *ret_keycode = *keycode;
    if (ret_mods)
        *ret_mods = mods;
    return 0;
}

int
hk_key_combo_to_str(int keycode, int mods,
    char ret_buf[HK_MAX_KEY_COMBO_NAME_LEN + 1])
{
    if (keycode == -1)
    {
        strcpy(ret_buf, "");
        return 0;
    }
    const char *key_name = hk_key_enum_to_str(keycode);
    if (!key_name)
        return 1;
    char *s = ret_buf;
    if (mods & HK_MOD_SHIFT)
    {
        memcpy(s, "s-", 2);
        s += 2;
    }
    if (mods & HK_MOD_CTRL)
    {
        memcpy(s, "c-", 2);
        s += 2;
    }
    if (mods & HK_MOD_ALT)
    {
        memcpy(s, "a-", 2);
        s += 2;
    }
    muta_assert(15 + strlen(key_name) <= HK_MAX_KEY_COMBO_NAME_LEN);
    strcpy(s, key_name);
    return 0;
}

void
hk_call_action(hk_action_t *action)
    {action->callback(action->user_data);}

static int
_on_action_def(parse_def_file_context_t *ctx, const char *def, const char *val)
{
    if (!streq(def, "action"))
        return 1;
    if (strlen(val) > 31)
        return 2;
    hk_action_t **ap       = ctx->user_data;
    hk_action_t *action    = _get_action_by_name(val);
    if (action)
        *ap = action;
    else
    {
        *ap = obj_pool_reserve(&_action_pool);
        str_hk_action_table_einsert(&_action_table, val, *ap);
        strcpy((*ap)->name, val);
        (*ap)->keys[0].keycode = -1;
        (*ap)->keys[1].keycode = -1;
    }
    return 0;
}

static int
_on_action_val(parse_def_file_context_t *ctx, const char *key, const char *val)
{
    hk_action_t **ap = ctx->user_data;
    if (streq(key, "key1"))
        (*ap)->keys[0].keycode = hk_str_to_keycode(val);
    else if (streq(key, "key2"))
        (*ap)->keys[1].keycode = hk_str_to_keycode(val);
    else
        return 3;
    return 0;
}

static uint64
_hash_from_key_combo(int key_enum, int mods)
{
    char buf[2 * sizeof(int)];
    memcpy(buf + 0, &key_enum, sizeof(int));
    memcpy(buf + sizeof(int), &mods, sizeof(int));
    return fnv_hash64_from_data(buf, 2 * sizeof(int));
}
