#ifndef MUTA_CLIENT_NAME_PLATE_COMPONENT_H
#define MUTA_CLIENT_NAME_PLATE_COMPONENT_H

#include "component.h"

/* Forward declaration(s) */

/* Types defined here */
typedef struct name_plate_component_t   name_plate_component_t;
typedef struct name_plate_system_t      name_plate_system_t;

struct name_plate_component_t
{
    uint32      num;
    uint32      max;
    int         tile_position[3];
    int         last_tile_position[3];
    float       percentage_travelled;
    int16       offset[2];
    entity_t    *entity;
};

struct name_plate_system_t
{
    name_plate_component_t  *components;
    uint32                  num_components;
    uint32                  max_components;
    uint32                  component_definition_index;
};

extern component_definition_t name_plate_component_definition;

void
name_plate_component_set_offset(component_handle_t handle, int16 x,
    int16 y);

void
name_plate_component_set_current_health(component_handle_t handle,
    uint32 health_current);

void
name_plate_component_set_max_health(component_handle_t handle,
    uint32 health_max);

#endif /* MUTA_CLIENT_NAME_PLATE_COMPONENT_H */
