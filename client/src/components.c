#include "components.h"
#include "mobility_component.h"
#include "ae_animator_component.h"
#include "ae_set_component.h"
#include "iso_sprite_component.h"
#include "render_component.h"
#include "name_plate_component.h"

component_definition_t *component_definitions[] =
{
    &mobility_component_definition,
    &ae_animator_component_definition,
    &ae_set_component_definition,
    &iso_sprite_component_definition,
    &render_component_definition,
    &name_plate_component_definition
};
