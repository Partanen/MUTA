#ifndef MUTA_CLIENT_GL_H
#define MUTA_CLIENT_GL_H

#include "../../shared/types.h"
#include "types.h"

/* Forward declaration(s) */
typedef struct img_t            img_t;
typedef struct ttf_t            ttf_t;
typedef struct gui_draw_list_t  gui_draw_list_t;
typedef struct gui_glyph_t      gui_glyph_t;
typedef struct gui_font_t       gui_font_t;

/* Types defined here */
typedef struct tex_t                tex_t;
typedef struct gui_font_glyph_t        gui_font_glyph_t;
typedef struct sb_write_context_t   sb_write_context_t;

enum sb_flip_t
{
    SB_FLIP_NONE = 0,
    SB_FLIP_H,
    SB_FLIP_V,
    SB_FLIP_BOTH
};

enum tex_filter_t
{
    TEX_FILTER_LINEAR = 0,
    TEX_FILTER_NEAREST,
    NUM_TEX_FILTERS
};

enum tex_wrapping_t
{
    TEX_WRAP_REPEAT,
    TEX_WRAP_MIRROR_REPEAT,
    TEX_WRAP_CLAMP_TO_EDGE,
    TEX_WRAP_CLAMP_TO_BORDER
};

enum predefined_shader_attribute_location
{
    SHADER_ATTR_LOC_POS = 0,
    SHADER_ATTR_LOC_UV,
    SHADER_ATTR_LOC_COL,
    NUM_PREDEF_SHADER_ATTR_LOCS
};

struct tex_t
{
    tex_id_t    id;
    float       w, h;
};

struct sb_write_context_t
{
    uint32  first_index;
    tex_t   tex;
};

extern int      RB_CLEAR_COLOR_BIT;
extern int      RB_CLEAR_DEPTH_BIT;
extern uint32   sb_opaque_shader;
extern uint32   sb_blend_shader;

int
gl_init(void *window_data);

void
gl_destroy(void);

void
gl_use_program(uint32 program);

void
gl_bind_vbo(uint32 vbo);

void
gl_bind_ebo(uint32 ebo);

void
gl_active_texture(int index); /* 0 to 15 */

void
gl_enable_blend(void);

void
gl_disable_blend(void);

void
gl_enable_depth_test(void);

void
gl_disable_depth_test(void);

void
gl_enable_scissor(void);

void
gl_bind_tex2d(uint32 tex);

void
gl_bind_tex2d_if_not_bound(uint32 tex);

void
gl_color(float r, float g, float b, float a);

void
gl_clear(int mask);

void
gl_scissor(int x, int y, int w, int h);

void
gl_get_scissor(int *x, int *y, int *w, int *h);

void
gl_viewport(int x, int y, int w, int h);

void
gl_get_viewport(int *x, int *y, int *w, int *h);

void
gl_viewport_and_scissor(int x, int y, int w, int h);
/* Convenience function to set both. */

void
gl_set_vsync(bool32 opt);

bool32
gl_get_vsync(void);

void
gl_color(float r, float g, float b, float a);

void
gl_swap_buffers(void *window_data);

void
gl_print_errors(void);

int
tex_from_mem(tex_t *tex, uint8 *pixels, int w, int h, int px_format);

int
tex_from_mem_ext(tex_t *tex, uint8 *pixels, int w, int h,
    int px_format,
    enum tex_filter_t min_filter,
    enum tex_filter_t mag_filter,
    enum tex_wrapping_t wrapping);

int
tex_from_img(tex_t *tex, img_t *img);

int
tex_from_img_ext(tex_t *tex, img_t *img, int min_filter, int mag_filter,
    int wrapping);

void
tex_free(tex_t *tex);

static inline bool32
tex_is_loaded(tex_t *tex)
    {return tex->id != 0;}

int
gl_generate_gui_font_texture(gui_font_t *font);

int
gl_generate_gui_font_texture_ext(gui_font_t *font,
    enum tex_filter_t min_filter,
    enum tex_filter_t mag_filter,
    enum tex_wrapping_t wrapping);

void
gl_destroy_gui_font_texture(gui_font_t *font);

void
gui_font_free(gui_font_t *gui_font);

int
sb_init(int max_sprites, char *err_log, int err_log_len);

void
sb_destroy(void);

int
sb_begin(uint32 shader, float *coord_space);
/* If shader is 0, the default shader will be used
 * If coord_space is 0, uses [x = 0, y = 0] with gl viewport width and height */

/* sprite draw function postfixes:
 * s: scale, parameters sx and sy
 * c: color, paremeter col (pointer to an array of 4 unsigned bytes)
 * r: rotate, paremeter angle in radians, ox and oy origin relative to the
 *    sprite's top left corner. ox and oy are NOT automatically multiplied by
 *    scale
 * f: flip, parameter flip - possibl e options are SB_FLIP_H and SB_FLIP_V
 *
 * Note: none of the arguments can be NULL. If SB_USE_SAFE_LIMITS is not
 * non-zero, the functions also do no checking on whether or not the batch still
 * has space for a new sprite, so overflow must be checked for by the caller. */

void
sb_sprite(tex_t *tex, float *clip, float x, float y,
    float z);

void
sb_sprite_s(tex_t *tex, float *clip, float x, float y,
    float z, float sx, float sy);

void
sb_sprite_c(tex_t *tex, float *clip, float x, float y,
    float z, uint8 *col);

void
sb_sprite_r(tex_t *tex, float *clip, float x, float y,
    float z, float angle, float ox, float oy);

void
sb_sprite_f(tex_t *tex, float *clip, float x, float y,
    float z, enum sb_flip_t flip);

void
sb_sprite_sc(tex_t *tex, float *clip, float x, float y,
    float z, float sx, float sy, uint8 *col);

void
sb_sprite_sr(tex_t *tex, float *clip, float x, float y,
    float z, float sx, float sy, float angle, float ox, float oy);

void
sb_sprite_sf(tex_t *tex, float *clip, float x, float y,
    float z, float sx, float sy, enum sb_flip_t flip);

void
sb_sprite_cr(tex_t *tex, float *clip, float x, float y, float z,
    uint8 *col, float angle, float ox, float oy);

void
sb_sprite_cf(tex_t *tex, float *clip, float x, float y, float z,
    uint8 *col, enum sb_flip_t flip);

void
sb_sprite_scr(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, uint8 *col, float angle, float ox, float oy);

void
sb_sprite_scf(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, uint8 *col, enum sb_flip_t flip);

void
sb_sprite_scrf(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, uint8 *col, float angle, float origin_x, float origin_y,
    enum sb_flip_t flip);

void
sb_text(gui_font_t *sf, const char *txt, float x, float y, float z);

void
sb_text_sc(gui_font_t *sf, const char *txt, float x, float y, float z,
    float sx, float sy, uint8 *col);

void
sb_text_wrap(gui_font_t *sf, const char *txt, float x, float y, float z,
    float wrap);

void
sb_text_wrap_s(gui_font_t *sf, const char *txt, float x, float y, float z,
    float wrap, float sx, float sy);

void
sb_text_wrap_sc(gui_font_t *sf, const char *txt, float x,
    float y, float z, float wrap, float sx, float sy, uint8 *col);

/* Parallel sprite writing functions.
 * Reserve memory for a set of sprites all using the same texture. Write to the
 * memory using the sb_write_ functions. It is safe to parallelize the use of
 * sb_write_ functions to the same block of memory. */

sb_write_context_t
sb_reserve_sprites(tex_t *tex, int num);
/* Returns a pointer to the vertices to write to. */

void
sb_write_sprite(sb_write_context_t *context, int index, tex_t *tex,
    float *clip, float x, float y, float z);

int
sb_end(void);

uint32
gl_shader_from_strs(const char *vtx_code, const char *frg_code,
    char *err_log, int err_log_len);

uint32
gl_shader_from_files(const char *vert_path, const char *frag_path,
    char *err_log, int err_log_len);

void
gl_orthographic_projection_from_coordinate_space(float *coordinate_space,
    float ret_projection[4][4]);

#endif /* MUTA_CLIENT_RENDER_GL_H */
