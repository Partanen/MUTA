#include "slash_cmd.h"
#include "../../shared/common_utils.h"
#include "../../shared/common_defs.h"

typedef struct slash_cmd_t slash_cmd_t;

struct slash_cmd_t
{
    char    name[MAX_SLASH_CMD_NAME_LEN + 1];
    void    (*callback)(void*, int, char**);
    void    *context;
};

slash_cmd_t *_slash_cmds;

static slash_cmd_t *
_find_cmd(const char *name);

int
slash_init()
{
    darr_reserve(_slash_cmds, 64);
    return 0;
}

void
slash_destroy()
    {darr_free(_slash_cmds);}

int
slash_register(const char *name,
    void (*callback)(void *context, int argc, char **argv), void *context)
{
    int len = (int)strlen(name);
    if (len < 1)
        return SLASH_CMD_NAME_TOO_SHORT;
    if (len > MAX_SLASH_CMD_NAME_LEN)
        return SLASH_CMD_NAME_TOO_LONG;
    if (_find_cmd(name))
        return SLASH_CMD_ALREADY_EXISTS;
    slash_cmd_t cmd;
    memcpy(cmd.name, name, len + 1);
    cmd.callback    = callback;
    cmd.context     = context;
    darr_push(_slash_cmds, cmd);
    return 0;
}

bool32
slash_try_execute(const char *msg)
{
    char    buf[MAX_CHAT_MSG_LEN + MAX_SLASH_CMD_ARGS]  = {0};
    int     len                                         = (int)strlen(msg);
    char    *current_arg                                = buf;
    int     num_args                                    = 0;
    char    *args[1 + MAX_SLASH_CMD_ARGS];
    if (!len || len > (int)sizeof(buf) - 1)
        return 0;
    if (msg[0] != '/')
        return 0;
    int     buf_index   = 0;
    bool32  quotes      = 0;
    bool32  escape      = 0;
    for (int i = 0;; ++i)
    {
        switch (msg[i])
        {
        case 0:
            buf[buf_index++] = 0;
            args[num_args++] = current_arg;
            goto out_of_loop;
            break;
        case '\\':
            if (!escape)
                escape = 1;
            else
            {
                buf[buf_index++] = msg[i];
                escape = 0;
            }
            break;
        case '"':
            if (escape)
                buf[buf_index++] = '"';
            else if (quotes)
            {
                buf[buf_index++] = 0;
                quotes = 0;
            } else
                quotes = 1;
            escape = 0;
            break;
        case ' ':
            if (quotes)
                buf[buf_index++] = msg[i];
            else
            {
                args[num_args++]    = current_arg;
                buf[buf_index++]    = 0;
                current_arg         = buf + buf_index;
                int j = i;
                for (const char *c = msg + i; *c == ' '; ++c)
                    j++;
                i = j - 1;
                escape = 0;
            }
            break;
        default:
            buf[buf_index++] = msg[i];
            escape = 0;
        }
    }
    out_of_loop:
    if (!num_args)
        return 0;
    slash_cmd_t *cmd = _find_cmd(args[0] + 1);
    if (!cmd)
        return 0;
    cmd->callback(cmd->context, num_args, args);
    return 1;
}

static slash_cmd_t *
_find_cmd(const char *name)
{
    uint32      num_cmds    = darr_num(_slash_cmds);
    slash_cmd_t *cmds       = _slash_cmds;
    for (uint32 i = 0; i < num_cmds; ++i)
        if (streq(name, cmds[i].name))
            return &cmds[i];
    return 0;
}
