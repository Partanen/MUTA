#include "entity.h"
#include "world.h"
#include "components.h"
#include "entity_event.h"

typedef struct entity_listener_t entity_listener_t;

enum entity_flag
{
    ENTITY_FLAG_SPAWNED = (1 << 0)
};

struct entity_t
{
    entity_type_data_t  type_data;
    world_t             *world;
    component_handle_t  components[NUM_COMPONENT_DEFINITIONS];
    entity_listener_t   *listeners;
    uint32              reserved_index;
    int32               x, y;
    int8                z;
    uint8               direction;
    uint8               flags;
};

struct entity_listener_t
{
    enum entity_event       event;
    int                     id;
    entity_listen_t         data;
    entity_listener_t       *next;
    void (*callback)(entity_event_t*, entity_listen_t*);
};

static entity_event_callback_t *_entity_event_callbacks[NUM_ENTITY_EVENTS];

int
entity_pool_init(entity_pool_t *pool, uint32 max)
{
    fixed_pool_init(&pool->entities, max);
    obj_pool_init(&pool->event_listeners, 64, sizeof(entity_listener_t));
    pool->reserved = emalloc(max * sizeof(uint32));
    return 0;
}

void
entity_pool_destroy(entity_pool_t *pool)
{
    fixed_pool_destroy(&pool->entities);
    obj_pool_destroy(&pool->event_listeners);
    free(pool->reserved);
}

void
entity_pool_clear(entity_pool_t *pool)
{
    while (pool->entities.num_free != pool->entities.max)
        entity_despawn(
            &pool->entities.all[pool->reserved[0]]);
    fixed_pool_clear(&pool->entities);
}

int
entity_api_init(void)
{
    for (uint32 i = 0; i < NUM_COMPONENT_DEFINITIONS; ++i)
    {
        component_definition_t *def = component_definitions[i];
        def->index = i;
        /*-- Register component event interests --*/
        for (uint32 j = 0; j < def->num_component_event_interests; ++j)
        {
            component_event_interest_t *interest =
                &def->component_event_interests[j];
            component_definition_t *other_def = interest->component_definition;
            darr_push(other_def->component_event_callbacks, interest->callback);
        }
        /*-- Register entity event interests --*/
        for (uint32 j = 0; j < def->num_entity_event_interests; ++j)
        {
            entity_event_interest_t *interest = &def->entity_event_interests[j];
            darr_push(_entity_event_callbacks[interest->event],
                interest->callback);
        }
    }
    return 0;
}

void
entity_api_destroy(void)
{
    for (uint32 i = 0; i < NUM_COMPONENT_DEFINITIONS; ++i)
        darr_free(component_definitions[i]->component_event_callbacks);
    for (uint32 i = 0; i < NUM_ENTITY_EVENTS; ++i)
        darr_free(_entity_event_callbacks[i]);
}

entity_t *
entity_spawn(world_t *world, int direction, int *position)
{
    uint32 reserved_index = world->entities.entities.max -
        world->entities.entities.num_free;
    entity_t *entity = fixed_pool_new(&world->entities.entities);
    if (!entity)
        return 0;
    for (uint32 i = 0; i < NUM_COMPONENT_DEFINITIONS; ++i)
        entity->components[i] = INVALID_COMPONENT_HANDLE;
    memset(&entity->type_data, 0, sizeof(entity->type_data));
    entity->type_data.type          = ENTITY_TYPE_UNKNOWN;
    entity->world                   = world;
    entity->listeners               = 0;
    entity->x                       = position[0];
    entity->y                       = position[1];
    entity->z                       = position[2];
    entity->flags                   = ENTITY_FLAG_SPAWNED;
    entity->direction               = direction;
    entity->reserved_index          = reserved_index;
    world->entities.reserved[reserved_index] = fixed_pool_index(
        &world->entities.entities, entity);
    return entity;
}

void
entity_despawn(entity_t *entity)
{
    muta_assert(entity->flags & ENTITY_FLAG_SPAWNED);
    entity_event_t event;
    event.type      = ENTITY_EVENT_WILL_DESPAWN;
    event.entity    = entity;
    entity_post_entity_event(&event);
    /*-- Remove components --*/
    for (uint32 i = 0; i < NUM_COMPONENT_DEFINITIONS; ++i)
        if (entity->components[i] != INVALID_COMPONENT_HANDLE)
            entity_detach_component(entity, component_definitions[i]);
    world_t *world = entity_get_world(entity);
    /*-- Remove from reserved list --*/
    uint32 last_index = world->entities.entities.max -
        world->entities.entities.num_free - 1;
    if (last_index)
    {
        world->entities.reserved[entity->reserved_index] =
            world->entities.reserved[last_index];
        world->entities.entities.all[world->entities.reserved[
            entity->reserved_index]].reserved_index = entity->reserved_index;
    }
    /*-- Free event listeners --*/
    entity_listener_t *listener = entity->listeners;
    while (listener)
    {
        obj_pool_free(&world->entities.event_listeners, listener);
        listener = listener->next;
    }
    /*-- Do type-specific stuff --*/
    switch (entity->type_data.type)
    {
    case ENTITY_TYPE_PLAYER:
    {
        player_runtime_id_t id = entity->type_data.player.id;
        hashtable_erase(world->player_table, id,
            hashtable_hash(&id, sizeof(id)));
    }
        break;
    case ENTITY_TYPE_CREATURE:
    {
        creature_runtime_id_t id = entity->type_data.creature.id;
        hashtable_erase(world->creature_table, id,
            hashtable_hash(&id, sizeof(id)));
    }
        break;
    case ENTITY_TYPE_STATIC_OBJECT:
    {
        chunk_cache_t *cache = world_get_chunk_cache_of_tile(world, entity->x,
            entity->y);
        if (!cache)
            break;
        uint32 index        = entity->type_data.static_object.index_in_cache;
        uint32 last_index   = --_darr_head(cache->static_objects)->num;
        cache->static_objects[index] = cache->static_objects[last_index];
        cache->static_objects[index]->type_data.static_object.index_in_cache =
            index;
    }
        break;
    case ENTITY_TYPE_DYNAMIC_OBJECT:
    {
        dobj_runtime_id_t id = entity->type_data.dynamic_object.id;
        hashtable_erase(world->dynamic_object_table, id,
            hashtable_hash(&id, sizeof(id)));
    }
        break;
    default:
        break;
    }
    entity->flags &= ~ENTITY_FLAG_SPAWNED;
    fixed_pool_free(&world->entities.entities, entity);
}

bool32
entity_is_spawned(entity_t *entity)
    {return entity->flags & ENTITY_FLAG_SPAWNED;}

world_t *
entity_get_world(entity_t *entity)
    {return entity->world;}

entity_type_data_t *
entity_get_type_data(entity_t *entity)
    {return &entity->type_data;}

component_handle_t
entity_attach_component(entity_t *entity,
    component_definition_t *component_definition)
{
    muta_assert(entity->flags & ENTITY_FLAG_SPAWNED);
    component_handle_t handle = entity_get_component(entity,
        component_definition);
    if (handle != INVALID_COMPONENT_HANDLE)
        return handle;
    handle = component_definition->component_attach(entity);
    entity->components[component_definition->index] = handle;
    entity_event_t event;
    event.type                                  = ENTITY_EVENT_ATTACH_COMPONENT;
    event.entity                                = entity;
    event.attach_component.component_definition = component_definition;
    entity_post_entity_event(&event);
    return handle;
}

void
entity_detach_component(entity_t *entity,
    component_definition_t *component_definition)
{
    muta_assert(entity->flags & ENTITY_FLAG_SPAWNED);
    muta_assert(entity->components[component_definition->index] !=
        INVALID_COMPONENT_HANDLE);
    uint32 index = component_definition->index;
    if (component_definition->component_detach)
        component_definition->component_detach(entity->components[index]);
    entity->components[index] = INVALID_COMPONENT_HANDLE;
    entity_event_t event;
    event.type                                  = ENTITY_EVENT_DETACH_COMPONENT;
    event.entity                                = entity;
    event.detach_component.component_definition = component_definition;
    entity_post_entity_event(&event);
}

component_handle_t
entity_get_component(entity_t *entity,
    component_definition_t *component_definition)
    {return entity->components[component_definition->index];}

bool32
entity_has_component(entity_t *entity,
    component_definition_t *component_definition)
{
    return entity->components[component_definition->index] !=
        INVALID_COMPONENT_HANDLE;
}

void
entity_set_direction(entity_t *entity, int direction)
{
    muta_assert(entity->flags & ENTITY_FLAG_SPAWNED);
    if (direction < 0 || direction >= NUM_ISODIRS)
        return;
    if (entity->direction == direction)
        return;
    entity_event_t event;
    event.type                          = ENTITY_EVENT_SET_DIRECTION;
    event.entity                        = entity;
    event.set_direction.last_direction  = entity->direction;
    event.set_direction.new_direction   = direction;
    entity->direction                   = direction;
    entity_post_entity_event(&event);
}

int
entity_get_direction(entity_t *entity)
    {return entity->direction;}

void
entity_set_position(entity_t *entity, int *position)
{
    muta_assert(entity->flags & ENTITY_FLAG_SPAWNED);
    entity_event_t event;
    event.type                          = ENTITY_EVENT_SET_POSITION;
    event.entity                        = entity;
    event.set_position.last_position[0] = entity->x;
    event.set_position.last_position[1] = entity->y;
    event.set_position.last_position[2] = entity->z;
    event.set_position.new_position[0]  = position[0];
    event.set_position.new_position[1]  = position[1];
    event.set_position.new_position[2]  = position[2];
    entity->x                           = position[0];
    entity->y                           = position[1];
    entity->z                           = position[2];
    entity_post_entity_event(&event);
}

void
entity_get_position(entity_t *entity, int ret_position[3])
{
    ret_position[0] = entity->x;
    ret_position[1] = entity->y;
    ret_position[2] = entity->z;
}

component_handle_t *
entity_get_components(entity_t *entity)
    {return entity->components;}

void
entity_post_entity_event(entity_event_t *event)
{
    entity_t *entity = event->entity;
    muta_assert(entity);
    muta_assert(entity->flags & ENTITY_FLAG_SPAWNED);
    /* Call component-registered callbacks */
    uint32 num_callbacks = darr_num(_entity_event_callbacks[event->type]);
    entity_event_callback_t *callbacks = _entity_event_callbacks[event->type];
    for (uint32 i = 0; i < num_callbacks; ++i)
        callbacks[i](event);
    for (entity_listener_t *listener = entity->listeners;
        listener;
        listener = listener->next)
        if (listener->event == event->type)
            listener->callback(event, &listener->data);
}

void
entity_post_component_event(entity_t *entity,
    component_definition_t *component_definition, void *event)
{
    muta_assert(entity->flags & ENTITY_FLAG_SPAWNED);
    /* Call component-registered callbacks */
    uint32 index = component_definition->index;
    component_handle_t handle = entity->components[index];
    if (handle == INVALID_COMPONENT_HANDLE)
        return;
    component_definition_t *def = component_definitions[index];
    uint32 num_callbacks = darr_num(def->component_event_callbacks);
    for (uint32 i = 0; i < num_callbacks; ++i)
        def->component_event_callbacks[i](entity, handle, event);
}

void
entity_listen_to_entity_event(entity_t *entity, enum entity_event event,
    entity_listen_t *listen_data,
    void (*callback)(entity_event_t*, entity_listen_t*))
{
    muta_assert(entity->flags & ENTITY_FLAG_SPAWNED);
    entity_pool_t *pool = &entity_get_world(entity)->entities;
    entity_listener_t *listener = obj_pool_reserve(&pool->event_listeners);
    listener->event     = event;
    listener->next      = 0;
    listener->data      = *listen_data;
    listener->callback  = callback;
    entity_listener_t **p = &entity->listeners;
    while (*p)
        p = &(*p)->next;
    *p = listener;
}

void
entity_stop_listening_to_entity_event(entity_t *entity, int id)
{
    muta_assert(entity->flags & ENTITY_FLAG_SPAWNED);
    for (entity_listener_t **listener = &entity->listeners;
        *listener;
        listener = &(*listener)->next)
    {
        if ((*listener)->data.id != id)
            continue;
        *listener = (*listener)->next;
        break;
    }
}

const char *
entity_get_name(entity_t *entity)
{
    entity_type_data_t *type_data = entity_get_type_data(entity);
    switch (type_data->type)
    {
    case ENTITY_TYPE_PLAYER:
        return type_data->player.name;
    case ENTITY_TYPE_CREATURE:
        return ent_get_creature_def(type_data->creature.type_id)->name;
    case ENTITY_TYPE_STATIC_OBJECT:
        return ent_get_static_object_def(type_data->static_object.type_id)->name;
    case ENTITY_TYPE_DYNAMIC_OBJECT:
        return ent_get_dynamic_object_def(
            type_data->dynamic_object.type_id)->name;
    default:
        return "Unknown";
    }
}

entity_handle_t
entity_get_handle(entity_t *entity)
{
    entity_type_data_t *type_data = entity_get_type_data(entity);
    entity_handle_t handle;
    handle.type = type_data->type;
    switch (handle.type)
    {
    case ENTITY_TYPE_PLAYER:
        handle.player = type_data->player.id;
        break;
    case ENTITY_TYPE_CREATURE:
        handle.creature = type_data->creature.id;
        break;
    case ENTITY_TYPE_DYNAMIC_OBJECT:
        handle.dynamic_object = type_data->dynamic_object.id;
        break;
    default:
        muta_assert(0);
    }
    return handle;
}

void
entity_get_common_entity_type_and_runtime_id(entity_t *entity,
    enum common_entity_type *ret_common_entity_type, uint32 *ret_runtime_id)
{
    switch (entity->type_data.type)
    {
    case ENTITY_TYPE_PLAYER:
        *ret_common_entity_type = COMMON_ENTITY_PLAYER;
        *ret_runtime_id         = entity->type_data.player.id;
        break;
    case ENTITY_TYPE_STATIC_OBJECT:
        *ret_common_entity_type = COMMON_ENTITY_STATIC_OBJECT;
        *ret_runtime_id         = 0xFFFFFFFF;
        break;
    case ENTITY_TYPE_DYNAMIC_OBJECT:
        *ret_common_entity_type = COMMON_ENTITY_DYNAMIC_OBJECT;
        *ret_runtime_id         = entity->type_data.dynamic_object.id;
        break;
    case ENTITY_TYPE_CREATURE:
        *ret_common_entity_type = COMMON_ENTITY_CREATURE;
        *ret_runtime_id         = entity->type_data.creature.id;
        break;
    default:
        muta_assert(0);
    }
}

void
entity_set_current_health(entity_t *entity, uint32 health_current)
{
    entity_type_data_t *type_data = entity_get_type_data(entity);
    switch (type_data->type)
    {
    case ENTITY_TYPE_PLAYER:
        type_data->player.health_current = health_current;
        break;
    case ENTITY_TYPE_CREATURE:
        type_data->creature.health_current = health_current;
        break;
    default:
        muta_assert(0);
    }
    entity_event_t event =
    {
        .type   = ENTITY_EVENT_CURRENT_HEALTH_CHANGED,
        .entity = entity,
        .current_health_changed.health_current = health_current
    };
    entity_post_entity_event(&event);
}

void
entity_set_max_health(entity_t *entity, uint32 health_max)
{
    entity_type_data_t *type_data = entity_get_type_data(entity);
    switch (type_data->type)
    {
    case ENTITY_TYPE_PLAYER:
        type_data->player.health_max = health_max;
        break;
    case ENTITY_TYPE_CREATURE:
        type_data->creature.health_max = health_max;
        break;
    default:
        muta_assert(0);
    }
    entity_event_t event =
    {
        .type                           = ENTITY_EVENT_MAX_HEALTH_CHANGED,
        .entity                         = entity,
        .max_health_changed.health_max  = health_max
    };
    entity_post_entity_event(&event);
}
