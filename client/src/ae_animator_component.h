#ifndef MUTA_CLIENT_AE_AE_ANIMATOR_COMPONENT_H
#define MUTA_CLIENT_AE_AE_ANIMATOR_COMPONENT_H

/* ae_animator_component.h
 * Animation tracking for ae (animated entity) animations */

#include "component.h"
#include "../../shared/common_utils.h"

/* Forward declaration(s*) */
typedef struct world_t                  world_t;
typedef struct entity_t                 entity_t;
typedef struct component_definition_t   component_definition_t;
typedef struct as_ae_t               as_ae_t;
typedef struct ae_frame_t               ae_frame_t;

/* Types defined here */
typedef struct ae_animator_component_t          ae_animator_component_t;
typedef struct ae_animator_component_data_t     ae_animator_component_data_t;
typedef struct ae_animator_system_t             ae_animator_system_t;
typedef struct ae_animator_component_event_t    ae_animator_component_event_t;
typedef struct ae_animator_component_new_frame_event_t
    ae_animator_component_new_frame_event_t;

enum ae_animator_component_play_mode
{
    AE_ANIMATOR_COMPONENT_PAUSED,
    AE_ANIMATOR_COMPONENT_PLAY_LOOP,
    AE_ANIMATOR_COMPONENT_PLAY_NO_LOOP
};

enum ae_animator_component_event
{
    AE_ANIMATOR_COMPONENT_EVENT_NEW_FRAME,
    /* New frame is posted every time a new frame occurs. */
    AE_ANIMATOR_COMPONENT_EVENT_ANIMATION_END
    /* Animation end is posted when the last frame of a non-looping animation
     * has been drawn. */
};

struct ae_animator_system_t
{
    fixed_pool(ae_animator_component_t) components;
    ae_animator_component_data_t        *looping;
    ae_animator_component_data_t        *not_looping;
    uint32                              num_looping;
    uint32                              num_not_looping;
};

struct ae_animator_component_new_frame_event_t
{
    ae_frame_t *frame; /* Can be null */
};

struct ae_animator_component_event_t
{
    int type;
    union
    {
        ae_animator_component_new_frame_event_t new_frame;
    };
};

extern component_definition_t ae_animator_component_definition;

as_ae_t *
ae_animator_component_get_asset(component_handle_t handle);

void
ae_animator_component_set_asset(component_handle_t handle, as_ae_t *ae_asset);

void
ae_animator_component_set_play_mode(component_handle_t handle, int play_mode);

int
ae_animator_component_get_play_mode(component_handle_t handle);

uint32
ae_animator_get_current_frame(component_handle_t handle);

#endif /* MUTA_CLIENT_AE_AE_ANIMATOR_COMPONENT_H */
