#include "client.h"
#include "login.h"
#include "shard.h"
#include "../../shared/net.h"
#include "../../shared/crypt.h"
#include "../../shared/common_defs.h"
#include "../../shared/client_packets.h"

#define OUT_BUF_SZ  MUTA_MTU
#define IN_BUF_SZ   MUTA_MTU

static struct
{
    uint8   memory[IN_BUF_SZ];
    int     num_bytes;
} _in_buf;

static struct
{
    uint8   memory[OUT_BUF_SZ];
    int     num_bytes;
} _out_buf;


static thread_t             _thread;
static int32                _running; /* Atomic */
static socket_t             _socket = KSYS_INVALID_SOCKET;
static cryptchan_t          _cryptchan;
static addr_t               _login_server_address;
static char                 _account_name[MAX_ACC_NAME_LEN + 1];
static char                 _password[MAX_PW_LEN + 1];
static int                  _status = LOGIN_STATUS_DISCONNECTED;
static int                  _selected_shard_index = -1; /* -1 if none */
static int                  _last_error;
static login_shard_info_t   _shard_infos[LOGIN_MAX_SHARD_INFOS];
static uint32               _num_shard_infos;

static void
_send_disconnect_event(int error);
/* Error is one of enum login_error. It will be set to _last_error */

static void
_read_server_info_callback(void *ctx, const char *opt, const char *val);

static void
_login_disconnect_internal(void);

static thread_ret_t
_main(void *args);

static bbuf_t
_send_msg(int num_bytes);

static inline bbuf_t
_send_const_encrypted_msg(int num_bytes);

static inline bbuf_t
_send_var_encrypted_msg(int num_bytes);

static int
_read_packet(void);

static int
_handle_svmsg_pub_key(svmsg_pub_key_t *s);

static int
_handle_svmsg_stream_header(svmsg_stream_header_t *s);

static int
_handle_svmsg_login_result(svmsg_login_result_t *s);

static int
_handle_svmsg_shard_list_item(svmsg_shard_list_item_t *s);

static int
_handle_svmsg_shard_select_fail(svmsg_shard_select_fail_t *s);

static int
_handle_svmsg_shard_select_success(svmsg_shard_select_success_t *s);

int
login_init(void)
{
    int ret = 0;
    if (thread_init(&_thread))
        {ret = 1; goto fail;}
    bool32 found[2] = {0, 0}; /* Found {address, port}? */
    if (parse_cfg_file("server_info.cfg", _read_server_info_callback, &found) ||
        !found[0])
    {
        DEBUG_PRINTF("Warning: server address could not be read from "
            "server_info.cfg.\n");
        uint16 port = found[1] ? _login_server_address.port :
            DEFAULT_LOGIN_PORT;
        addr_init(&_login_server_address, 127, 0, 0, 1, port);
    }
    return 0;
    fail:
        DEBUG_PRINTFF("Failed with code %d.\n", ret);
    return ret;
}

void
login_destroy(void)
{
}

int
login_get_status(void)
    {return _status;}

int
login_get_last_error(void)
    {return _last_error;}

int
login_connect(const char *account_name, const char *password)
{
    muta_assert(!_running);
    int ret = 0;
    size_t account_name_len     = strlen(account_name);
    if (account_name_len < MIN_ACC_NAME_LEN)
    {
        _last_error = LOGIN_ERROR_ACCOUNT_NAME_TOO_SHORT;
        return 1;
    }
    if (account_name_len > MAX_ACC_NAME_LEN)
    {
        _last_error = LOGIN_ERROR_ACCOUNT_NAME_TOO_LONG;
        return 2;
    }
    size_t password_name_len = strlen(password);
    if (password_name_len < MIN_PW_LEN)
    {
        _last_error = LOGIN_ERROR_PASSWORD_TOO_SHORT;
        return 3;
    }
    if (account_name_len > MAX_PW_LEN)
    {
        _last_error = LOGIN_ERROR_PASSWORD_TOO_LONG;
        return 4;
    }
    if ((_socket = net_tcp_ipv4_sock()) == KSYS_INVALID_SOCKET)
        {ret = 5; goto fail;}
    if (make_socket_non_block(_socket))
        {ret = 6; goto fail;}
    if (net_disable_nagle(_socket))
        {ret = 7; goto fail;}
    if (cryptchan_init(&_cryptchan, 0))
        {ret = 8; goto fail;}
    strcpy(_account_name, account_name);
    strcpy(_password, password);
    _out_buf.num_bytes      = 0;
    _in_buf.num_bytes       = 0;
    _running                = 1;
    _selected_shard_index   = -1;
    _num_shard_infos        = 0;
    DEBUG_PRINTF("Connecting to login server, address %u.%u.%u.%u:%u.\n",
        ADDR_IP_TO_PRINTF_ARGS(&_login_server_address),
        _login_server_address.port);
    if (thread_create(&_thread, _main, 0))
        {ret = 9; goto fail;}
    _status     = LOGIN_STATUS_CONNECTING;
    _last_error = LOGIN_ERROR_NONE;
    return ret;
    fail:
        close_socket(_socket);
        _socket     = KSYS_INVALID_SOCKET;
        _status     = LOGIN_STATUS_DISCONNECTED;
        _running    = 0;
        _last_error = LOGIN_ERROR_SYSTEM;
        return ret;
}

void
login_disconnect(void)
{
    if (!_running)
        return;
    _login_disconnect_internal();
}

void
login_disconnect_with_error(int error)
{
    if (!_running)
        return;
    _last_error = error;
    _login_disconnect_internal();
}

void
login_flush(void)
{
    if (_status < LOGIN_STATUS_HANDSHAKING)
        return;
    if (cl_flush_buf(_socket, _out_buf.memory, &_out_buf.num_bytes))
        login_disconnect_with_error(LOGIN_ERROR_SEND_FAILED);
}

uint32
login_get_shard_infos(login_shard_info_t *infos, uint32 max)
{
    uint32 num_infos = MIN(_num_shard_infos, max);
    for (uint32 i = 0; i < num_infos; ++i)
        infos[i] = _shard_infos[i];
    return num_infos;
}

int
login_select_shard(uint32 index)
{
    if (_status != LOGIN_STATUS_SELECTING_SHARD)
        return 1;
    if (index >= _num_shard_infos)
        return 2;
    if (!_shard_infos[index].online)
        return 3;
    clmsg_select_shard_t s_msg;
    s_msg.shard_name.len = (uint8)strlen(_shard_infos[index].name);
    memcpy(s_msg.shard_name.data, _shard_infos[index].name,
        s_msg.shard_name.len);
    bbuf_t bb = _send_msg(clmsg_select_shard_compute_sz(&s_msg));
    if (!bb.max_bytes)
        return 1;
    clmsg_select_shard_write(&bb, &s_msg);
    _selected_shard_index   = index;
    _status                 = LOGIN_STATUS_SELECTED_SHARD;
    return 0;
}

void
login_cancel_select_shard()
{
    if (_status != LOGIN_STATUS_SELECTED_SHARD)
        return;
    bbuf_t bb = _send_msg(0);
    if (!bb.max_bytes)
    {
        login_disconnect_with_error(LOGIN_ERROR_SEND_FAILED);
        return;
    }
    clmsg_cancel_select_shard_write(&bb);
    _status = LOGIN_STATUS_SELECTING_SHARD;
    return;
}

const char *
login_error_to_string(int error)
{
    switch (error)
    {
    case LOGIN_ERROR_NONE:
        return "No error.";
    case LOGIN_ERROR_UNABLE_TO_CONNECT:
        return "Unable to connect.";
    case LOGIN_ERROR_SYSTEM:
        return "System error.";
    case LOGIN_ERROR_SERVER_CLOSED_CONNECTION:
        return "Server closed connection.";
    case LOGIN_ERROR_BAD_MSG:
        return "Received invalid message.";
    case LOGIN_ERROR_SEND_FAILED:
        return "Failed to send message.";
    case LOGIN_ERROR_ACCOUNT_NAME_TOO_SHORT:
        return "Account name to short.";
    case LOGIN_ERROR_ACCOUNT_NAME_TOO_LONG:
        return "Account name too long.";
    case LOGIN_ERROR_PASSWORD_TOO_SHORT:
        return "Password too short.";
    case LOGIN_ERROR_PASSWORD_TOO_LONG:
        return "Password too long.";
    default:
        return "Unknown error.";
    }
}

void
login_read(cl_read_event_t *event)
{
    if (!_running)
        return;
    if (cl_read(event, _read_packet, _in_buf.memory, &_in_buf.num_bytes,
        IN_BUF_SZ, &_last_error, LOGIN_ERROR_SERVER_CLOSED_CONNECTION,
        LOGIN_ERROR_BAD_MSG))
        _login_disconnect_internal();
}

void
login_on_connected(void)
{
    if (!_running)
        return;
    bbuf_t bb = _send_msg(CLMSG_PUB_KEY_SZ);
    if (!bb.max_bytes)
    {
        login_disconnect_with_error(LOGIN_ERROR_SERVER_CLOSED_CONNECTION);
        return;
    }
    clmsg_pub_key_t k_msg;
    memcpy(k_msg.key, _cryptchan.pk, sizeof(_cryptchan.pk));
    clmsg_pub_key_write(&bb, &k_msg);
    _status = LOGIN_STATUS_HANDSHAKING;
}

static void
_send_disconnect_event(int error)
{
    cl_event_t cl_event;
    cl_event.type                   = CL_EVENT_DISCONNECT_LOGIN;
    cl_event.disconnect_login.error = error;
    cl_push_event(&cl_event);
}

static void
_read_server_info_callback(void *ctx, const char *opt, const char *val)
{
    bool32 *found = ctx;
    if (streq(opt, "address"))
    {
        uint16 port = found[1] ? _login_server_address.port :
            DEFAULT_LOGIN_PORT;
        if (!addr_init_from_str(&_login_server_address, val, port))
            found[0] = 1;
    } else
    if (streq(opt, "port"))
    {
        int v = atoi(val);
        if (v < 0 || v > 0xFFFF)
            return;
        _login_server_address.port = (uint16)v;
        found[1] = 1;
    }
}

static void
_login_disconnect_internal(void)
{
    DEBUG_PRINTF("Disconnecting from login server, error %s.\n",
        login_error_to_string(_last_error));
    interlocked_decrement_int32(&_running);
    net_close_blocking_sock(_socket);
    thread_join(&_thread);
    _socket                 = KSYS_INVALID_SOCKET;
    _status                 = LOGIN_STATUS_DISCONNECTED;
    _out_buf.num_bytes      = 0;
    _in_buf.num_bytes       = 0;
    _num_shard_infos        = 0;
}

static thread_ret_t
_main(void *args)
{
    (void)args;
    uint8           buf[MUTA_MTU];
    fd_set          socket_set;
    struct timeval  wait_tv;
    /*-- Connect to login server (non-blocking) --*/
    net_connect(_socket, _login_server_address);
    for (;;)
    {
        if (!interlocked_compare_exchange_int32(&_running, -1, -1))
            return 0;
        FD_ZERO(&socket_set);
        FD_SET(_socket, &socket_set);
        wait_tv.tv_sec  = 0;
        wait_tv.tv_usec = 500000;
        select((int)_socket + 1, 0, &socket_set, 0, &wait_tv);
        if (!interlocked_compare_exchange_int32(&_running, -1, -1))
            return 0;
        int result = cl_check_blocking_socket_connect(_socket, &socket_set);
        if (!result)
            break;
        if (result > 0) /* Timeout */
            continue;
        if (result < 0)
        {
            _send_disconnect_event(LOGIN_ERROR_UNABLE_TO_CONNECT);
            return 0;
        }
    }
    cl_event_t cl_event;
    cl_event.type = CL_EVENT_LOGIN_CONNECTED;
    cl_push_event(&cl_event);
    /*-- Talk to the the login server --*/
    for (;;)
    {
        if (!interlocked_compare_exchange_int32(&_running, -1, -1))
            return 0;
        FD_ZERO(&socket_set);
        FD_SET(_socket, &socket_set);
        wait_tv.tv_sec  = 0;
        wait_tv.tv_usec = 500000;
        select((int)_socket + 1, &socket_set, 0, 0, &wait_tv);
        if (!interlocked_compare_exchange_int32(&_running, -1, -1))
            return 0;
        if (!FD_ISSET(_socket, &socket_set))
            continue;
        int num_bytes = net_recv(_socket, buf, sizeof(buf));
        cl_event_t cl_event;
        cl_event.type           = CL_EVENT_READ_LOGIN;
        cl_event.read.num_bytes = num_bytes;
        if (num_bytes > 0)
        {
            cl_event.read.memory    = cl_malloc(num_bytes);
            memcpy(cl_event.read.memory, buf, num_bytes);
        }
        cl_push_event(&cl_event);
        if (num_bytes <= 0)
            return 0;
    }
    return 0;
}

static bbuf_t
_send_msg(int num_bytes)
{
    return cl_send(_socket, _out_buf.memory, &_out_buf.num_bytes, OUT_BUF_SZ,
        num_bytes);
}

static inline bbuf_t
_send_const_encrypted_msg(int num_bytes)
    {return _send_msg(CRYPT_MSG_ADDITIONAL_BYTES + num_bytes);}

static inline bbuf_t
_send_var_encrypted_msg(int num_bytes)
{
    return _send_msg(sizeof(msg_sz_t) + CRYPT_MSG_ADDITIONAL_BYTES + num_bytes);
}

static int
_read_packet(void)
{
    DEBUG_PRINTFF("Reading %d bytes.\n", _in_buf.num_bytes);
    bbuf_t  bb          = BBUF_INITIALIZER(_in_buf.memory, _in_buf.num_bytes);
    int     dc          = 0;
    bool32  incomplete  = 0;
    svmsg_t msg_type;
    while (BBUF_FREE_SPACE(&bb) >= sizeof(svmsg_t))
    {
        BBUF_READ(&bb, &msg_type);
        switch (msg_type)
        {
        case SVMSG_PUB_KEY:
        {
            DEBUG_PUTS("SVMSG_PUB_KEY");
            svmsg_pub_key_t s;
            incomplete = svmsg_pub_key_read(&bb, &s);
            if (!incomplete)
                dc = _handle_svmsg_pub_key(&s);
        }
            break;
        case SVMSG_STREAM_HEADER:
        {
            DEBUG_PUTS("SVMSG_STREAM_HEADER");
            svmsg_stream_header_t s;
            incomplete = svmsg_stream_header_read(&bb, &s);
            if (!incomplete)
                dc = _handle_svmsg_stream_header(&s);
        }
            break;
        case SVMSG_LOGIN_RESULT:
        {
            DEBUG_PUTS("SVMSG_LOGIN_RESULT");
            svmsg_login_result_t s;
            incomplete = svmsg_login_result_read(&bb, &_cryptchan, &s);
            if (!incomplete)
                dc = _handle_svmsg_login_result(&s);
        }
            break;
        case SVMSG_SHARD_LIST_ITEM:
        {
            DEBUG_PUTS("SVMSG_SHARD_LIST_ITEM");
            svmsg_shard_list_item_t s;
            incomplete = svmsg_shard_list_item_read(&bb, &s);
            if (!incomplete)
                dc = _handle_svmsg_shard_list_item(&s);
        }
            break;
        case SVMSG_SHARD_SELECT_FAIL:
        {
            DEBUG_PUTS("SVMSG_SHARD_SELECT_FAIL");
            svmsg_shard_select_fail_t s;
            incomplete = svmsg_shard_select_fail_read(&bb, &s);
            if (!incomplete)
                dc = _handle_svmsg_shard_select_fail(&s);
        }
            break;
        case SVMSG_SHARD_SELECT_SUCCESS:
        {
            DEBUG_PUTS("SVMSG_SHARD_SELECT_SUCCESS");
            svmsg_shard_select_success_t s;
            incomplete = svmsg_shard_select_success_read(&bb, &_cryptchan, &s);
            if (!incomplete)
                dc = _handle_svmsg_shard_select_success(&s);
        }
            break;
        default:
            dc = 1;
        }
        if (dc || incomplete < 0)
        {
            if (!_last_error)
                _last_error = LOGIN_ERROR_BAD_MSG;
            DEBUG_PRINTFF("msg_type %u, dc %d, incomplete %d\n", msg_type, dc,
                incomplete);
            return 1;
        }
        if (incomplete)
            break;
    }
    if (!incomplete)
    {
        _in_buf.num_bytes = 0;
        return 0;
    }
    bb.num_bytes -= sizeof(svmsg_t);
    memmove(_in_buf.memory, _in_buf.memory + IN_BUF_SZ - BBUF_FREE_SPACE(&bb),
        BBUF_FREE_SPACE(&bb));
    _in_buf.num_bytes = BBUF_FREE_SPACE(&bb);
    return 0;
}

static int
_handle_svmsg_pub_key(svmsg_pub_key_t *s)
{
    clmsg_stream_header_t h_msg;
    if (cryptchan_cl_store_pub_key(&_cryptchan, s->key, h_msg.header))
        return 1;
    bbuf_t bb = _send_msg(CLMSG_STREAM_HEADER_SZ);
    if (!bb.max_bytes)
        return 2;
    if (clmsg_stream_header_write(&bb, &h_msg))
        return 3;
    DEBUG_PRINTF("Received login server public key.\n");
    return 0;
}

static int
_handle_svmsg_stream_header(svmsg_stream_header_t *s)
{
    if (cryptchan_store_stream_header(&_cryptchan, s->header))
        return 1;
    if (!cryptchan_is_encrypted(&_cryptchan))
        return 2;
    /* Send login request*/
    clmsg_login_request_t fwd;
    fwd.account_name.len = (uint8)strlen(_account_name);
    memcpy(fwd.account_name.data, _account_name, fwd.account_name.len);
    fwd.password.len = (uint8)strlen(_password);
    memcpy(fwd.password.data, _password, fwd.password.len);
    DEBUG_PRINTF("Attempting to log in as %s...\n", _account_name);
    bbuf_t bb = _send_var_encrypted_msg(clmsg_login_request_compute_sz(&fwd));
    if (!bb.max_bytes)
        return 3;
    if (clmsg_login_request_write(&bb, &_cryptchan, &fwd))
        return 4;
    DEBUG_PUTS("Received stream header.");
    DEBUG_PRINTF("Sent login request to server (size: %d).\n", bb.max_bytes);
    return 0;
}

static int
_handle_svmsg_login_result(svmsg_login_result_t *s)
{
    if (_status != LOGIN_STATUS_HANDSHAKING)
        return 1;
    if (s->result != ACCOUNT_LOGIN_OK)
    {
        DEBUG_PRINTF("Server rejected login.");
        return 2;
    }
    _status = LOGIN_STATUS_SELECTING_SHARD;
    DEBUG_PUTS("Successfully logged in to server.");
    return 0;
}

static int
_handle_svmsg_shard_list_item(svmsg_shard_list_item_t *s)
{
    if (_status < LOGIN_STATUS_SELECTING_SHARD)
        return 1;
    muta_assert(s->name.len <= MAX_SHARD_NAME_LEN);
    char name[MAX_SHARD_NAME_LEN + 1];
    memcpy(name, s->name.data, s->name.len);
    name[s->name.len] = 0;
    if (!str_is_ascii(name))
        return 2;
    for (uint32 i = 0; i < _num_shard_infos; ++i)
        if (streq(_shard_infos[i].name, name))
        {
            _shard_infos[i].online = s->online;
            return 0;
        }
    if (_num_shard_infos == LOGIN_MAX_SHARD_INFOS)
        return 3;
    login_shard_info_t *info = &_shard_infos[_num_shard_infos++];
    memcpy(info->name, name, s->name.len + 1);
    info->online = s->online;
    return 0;
}

static int
_handle_svmsg_shard_select_fail(svmsg_shard_select_fail_t *s)
{
    if (_status < LOGIN_STATUS_SELECTING_SHARD)
        return 1;
    _status = LOGIN_STATUS_SELECTING_SHARD;
    return 0;
}

static int
_handle_svmsg_shard_select_success(svmsg_shard_select_success_t *s)
{
    if (_status < LOGIN_STATUS_SELECTING_SHARD)
        return 1;
    addr_t address;
    address.ip      = s->ip;
    address.port    = s->port;
    DEBUG_PRINTF("Received shard connection info: %hhu.%hhu.%hu.%hhu:%hu\n",
        ((uint8*)&s->ip)[3], ((uint8*)&s->ip)[2], ((uint8*)&s->ip)[1],
        ((uint8*)&s->ip)[0], s->port);
    if (shard_connect(&address, s->token, _account_name,
        _shard_infos[_selected_shard_index].name))
        return 2;
    return 0;
}
