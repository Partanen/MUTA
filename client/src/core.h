#ifndef MUTA_CLIENT_CORE_H
#define MUTA_CLIENT_CORE_H

#include "core_key.h"
#include "../../shared/common_defs.h"

#define CORE_MAX_SHARDS                     64
#define CORE_MOUSE_BUTTON_LEFT              (SDL_BUTTON(SDL_BUTTON_LEFT))
#define CORE_MOUSE_BUTTON_RIGHT             (SDL_BUTTON(SDL_BUTTON_RIGHT))
#define CORE_MOUSE_BUTTON_MIDDLE            (SDL_BUTTON(SDL_BUTTON_MIDDLE))
#define CORE_NUM_TILE_SELECTOR_TYPES        3

/* Forward declaration(s) */
typedef struct spritesheet_t        spritesheet_t;
typedef struct gui_win_style_t      gui_win_style_t;
typedef struct gui_button_style_t   gui_button_style_t;
typedef struct sprite_context_t     sprite_context_t;
typedef struct kth_pool_t           kth_pool_t;
typedef struct gui_input_state_t    gui_input_state_t;
typedef struct screen_t             screen_t;
typedef struct SDL_Cursor           cursor_t;
typedef struct img_t                img_t;

/* Types defined here */
typedef struct config_t         config_t;
typedef struct asset_config_t   asset_config_t;
typedef struct async_job_t      async_job_t;

struct config_t
{
    struct
    {
        int     win_size[2];
        bool32  win_maximized;
        bool32  win_fullscreen;
        bool32  win_windowed;
        bool32  stepped_resolution_scale;
    } display;
    dchar   *tile_tex_path;
    dchar   *tile_sprite_def_path;
    bool32  vsync;
    int     fps_limit;
    float   sound_volume;
    float   music_volume;
    int     sprite_batch_size;
    char    account_name[MAX_ACC_NAME_LEN + 1];
    bool32  editor_mode;
};
/* Configuration read from config.cfg. */

struct asset_config_t
{
    uint32  tileset_tex_id;
    tile_t  tile_selector_ids[CORE_NUM_TILE_SELECTOR_TYPES];
    /* tile_selector_ids are IDs of three special tiles that are not used as
     * part of maps, but are used as indicators of, for example, which tile is
     * currently selected. */
    int32   tile_width;
    int32   tile_height;
    int32   tile_top_height;
    int32   resolution_w;
    int32   resolution_h;
    dchar   *matlock_tex_path; /* Used to indicate unfound texture. */
    uint32  animator_screen_direction_arrow_tex_id;
    uint32  ui_spritesheet_id;
    uint32  icons_spritesheet_id;
};
/* Data read from assets/asset_config.cfg. */

extern config_t             core_config;
extern asset_config_t       core_asset_config;
extern sprite_context_t     *core_sprite_context;
extern const char           *core_auto_login_name;
extern const char           *core_auto_login_pw;

int
core_init(bool32 editor_mode_overridden, bool32 editor_mode);

void
core_destroy(void);

int
core_start(int argc, char **argv, screen_t *first_screen);

int
core_stop(void);

void
core_update(void);

void
core_execute_click_events(void);
/* May be used to prematurely execute click events before a screen's update
 * function is finished. */

float
core_compute_pos_scaled_by_target_viewport(int *x, int *y);

void
core_set_screen(screen_t *screen);

int
core_maximize_window(void);

int
core_fps(void);

float
core_compute_target_viewport_scale(void);

float
core_compute_target_viewport(int *ret);
/* core_compute_target_viewport()
 * Return value is the scale of the viewport in comparison to window w/h */

void
core_window_to_gui_coords(int *ret_in_x, int *ret_in_y);

void
core_set_cursor(cursor_t *cursor);

/* Mouse functions:
 * Parameter "button" is a bitmask and can be used to check multiple buttons. */

bool32
core_mouse_button_down(int button);

bool32
core_mouse_button_down_now(int button);

bool32
core_mouse_button_up_now(int button);

int
core_mouse_x(void);

int
core_mouse_y(void);

bool32
core_key_down(int key); /* True if the key is currently being held down */

bool32
core_key_down_now(int key); /* True if key went down this frame */

bool32
core_key_up_now(int key); /* True if key came up this frame */

void *
core_window_data(void); /* Pointer to internal window data (such as SDL_Window) */

int
core_window_w(void);

int
core_window_h(void);

void
core_error_window(const char *error_message, ...);

size_t
core_stack_push(size_t num_bytes);
/* Return value is an offset to the stack. Use core_get_stack_at() to get a
 * pointer. If the function fails, the program panics. */

void
core_stack_pop(size_t offset);

void *
core_get_stack_at(size_t offset);

async_job_t *
core_run_async(void (*callback)(void *args), void *args);
/* The job must always be freed using core_async_wait() once completed. */

void
core_run_async_and_forget(void (*callback)(void *args), void *args);
/* Same as core_run_async, but does not allocate an async job object. */

int
core_async_wait(async_job_t *job, void **ret_args, int timeout_ms);
/* Returns arguments passed, unless ret_args is null. Waits infinitely if
 * timeout_ms is -1. Return value is non-zero on a timeout, 0 otherwise. */

void *
core_malloc(size_t size);
/* Will never return null. Uses internally pooled memory. */

void
core_free(void *p);

bool32
core_is_first_frame_of_current_screen(void);
/* Returns non-zero if this is the first time the current screen is being
 * updated after it was opened. */

cursor_t *
core_new_unmanaged_cursor(img_t *img, float clip[4]);

void
core_free_unmanaged_cursor(cursor_t *cursor);

#endif /* MUTA_CLIENT_CORE_H */
