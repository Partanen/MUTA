#ifndef MUTA_CLIENT_ICON_H
#define MUTA_CLIENT_ICON_H

#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct tex_t        tex_t;
typedef struct img_t        img_t;
typedef struct SDL_Cursor   cursor_t;

int
icon_init(void);

void
icon_destroy(void);

tex_t *
icon_get_tex(void);

img_t *
icon_get_bitmap(void);

float *
icon_get_clip(const char *id, bool32 *ret_is_unknown);
/* Returns a clip the icons texture. If no clip is found, the question_mark icon
 * will be returned and ret_is_unknown will be set to non-zero.
 * ret_is_unknown is optional. */

cursor_t *
icon_get_cursor(const char *id);

#endif /* MUTA_CLIENT_ICON_H */
