/* Connection handling to shards */

#ifndef MUTA_CLIENT_SHARD_H
#define MUTA_CLIENT_SHARD_H

#include "../../shared/common_utils.h"
#include "../../shared/common_defs.h"

/* Forward declaration(s) */
typedef struct addr_t           addr_t;
typedef struct cl_read_event_t  cl_read_event_t;

/* Types defined here */
typedef struct list_character_t list_character_t;

enum shard_status
{
    SHARD_STATUS_DISCONNECTED,
    SHARD_STATUS_CONNECTING,
    SHARD_STATUS_HANDSHAKING,
    SHARD_STATUS_CONNECTED
};

enum shard_error
{
    SHARD_ERROR_NONE = 0,
    SHARD_ERROR_UNABLE_TO_CONNECT,
    SHARD_ERROR_SYSTEM,
    SHARD_ERROR_SERVER_CLOSED_CONNECTION,
    SHARD_ERROR_BAD_MSG,
    SHARD_ERROR_SEND_FAILED,
};

struct list_character_t
{
    uint64  id;
    char    name[MAX_CHARACTER_NAME_LEN + 1];
    uint32  name_len; /* TODO: remove */
    int     race;
    int     sex;
};

int
shard_init(void);

void
shard_destroy(void);

int
shard_connect(addr_t *address, uint8 *auth_token, const char *account_name,
    const char *shard_name);

void
shard_disconnect(void);

void
shard_disconnect_with_error(int error);

int
shard_get_last_error(void);

int
shard_get_character_creation_error(void);

int
shard_get_enter_world_error(void);

const char *
shard_error_to_string(int error);

bbuf_t
shard_send(int num_bytes);

bbuf_t
shard_send_const_encrypted(int num_bytes);

bbuf_t
shard_send_var_encrypted(int num_bytes);

int
shard_get_status(void);

void
shard_flush(double dt);

uint32
shard_get_list_characters(list_character_t *ret_characters, uint32 max);

int
shard_select_character(uint32 index);

int
shard_create_character(list_character_t *character);

/*-- Event handlers, called on the main thread */

void
shard_on_connected(void);

void
shard_read(cl_read_event_t *event);

const char *
shard_get_name(void);

#endif /* MUTA_CLIENT_SHARD_H */
