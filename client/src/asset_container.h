#ifndef MUTA_ASSET_CONTAINER_H
#define MUTA_ASSET_CONTAINER_H

#include "../../shared/hashtable.h"
#include "../../shared/str_hashtable.h"

#define ASSET_CONTAINER_DEFINITION(type, name) \
typedef struct name##_item_t        name##_item_t; \
typedef struct name##_t             name##_t; \
typedef struct name##_id_table      name##_id_table_t; \
typedef struct name##_name_table    name##_name_table_t; \
\
struct name##_item_t \
{ \
    uint32          id; \
    mdb_row_t       row; \
    type            asset; \
    name##_item_t   *next; \
}; \
\
hashtable_define(name##_id_table, uint32, name##_item_t*); \
STR_HASHTABLE_DEFINITION(name##_name_table, name##_item_t*); \
DYNAMIC_OBJ_POOL_DEFINITION(name##_item_t, name##_pool, 16); \
\
struct name##_t \
{ \
    name##_id_table_t   id_table; \
    name##_name_table_t name_table; \
    name##_pool_t       pool; \
    mdb_t               mdb; \
    uint32              id_mdb_index; \
    uint32              name_mdb_index; \
    uint32              path_mdb_index; \
}; \
\
int \
name##_init(name##_t *c, const char *fp); \
\
void \
name##_destroy(name##_t *c); \
\
static inline type * \
name##_get_by_id(name##_t *c, uint32 id); \
\
static inline type * \
name##_get_by_name(name##_t *c, const char *name); \
\
int \
name##_init(name##_t *c, const char *fp) \
{ \
    memset(c, 0, sizeof(name##_t)); \
    if (mdb_open(&c->mdb, fp)) return 1; \
    uint32  num_rows    = mdb_num_rows(&c->mdb); \
    int     r           = 0; \
    name##_id_table_einit(&c->id_table, num_rows); \
    name##_name_table_einit(&c->name_table, num_rows); \
    if (num_rows && name##_pool_init(&c->pool, num_rows, 4)) \
        {r = 3; goto out;} \
    int index = mdb_get_col_index(&c->mdb, "id"); \
    if (index < 0) {r = 4; goto out;} \
    c->id_mdb_index = index; \
    index = mdb_get_col_index(&c->mdb, "name"); \
    if (index < 0) {r = 5; goto out;} \
    c->name_mdb_index = index; \
    index = mdb_get_col_index(&c->mdb, "path"); \
    if (index < 0) {r = 6; goto out;} \
    c->path_mdb_index = index; \
    for (uint32 i = 0; i < num_rows; ++i) \
    { \
        mdb_row_t row = mdb_get_row_by_index(&c->mdb, i); \
        if (!row) \
            {r = 7; goto out;} \
        uint32 id = *(uint32*)mdb_get_field(&c->mdb, row, c->id_mdb_index); \
        dchar *name = *(dchar**)mdb_get_field(&c->mdb, row, c->name_mdb_index); \
        if (!name) \
        { \
            DEBUG_PRINTFF("asset on row %u had a null name.\n", i); \
            continue; \
        } \
        str_strip_trailing_spaces(name); \
        str_strip_non_ascii(name); \
        str_strip_ctrl_chars(name); \
        set_dynamic_str_len(name, (uint)strlen(name)); \
        if (!(*(dchar**)mdb_get_field(&c->mdb, row, c->path_mdb_index))) \
        { \
            DEBUG_PRINTFF("row %u's path is not defined.\n", i); \
            continue; \
        } \
        name##_item_t *item = name##_pool_ereserve(&c->pool); \
        if (name##_id_table_exists(&c->id_table, id)) \
        { \
            DEBUG_PRINTFF("row %u's id %u exists.\n", i, id); \
            name##_pool_free(&c->pool, item); \
            continue; \
        } \
        if (name##_name_table_exists(&c->name_table, name)) \
        { \
            DEBUG_PRINTFF("row %u's name %s exists.\n", i, name); \
            name##_pool_free(&c->pool, item); \
            name##_id_table_erase(&c->id_table, id); \
            continue; \
        } \
        memset(&item->asset, 0, sizeof(item->asset)); \
        item->row   = row; \
        item->id    = id; \
        name##_id_table_einsert(&c->id_table, id, item); \
        name##_name_table_einsert(&c->name_table, name, item); \
    } \
    out: \
        if (r) \
        { \
            DEBUG_PRINTFF("failed with code %d.\n", r); \
            name##_destroy(c); \
        } \
        return r; \
} \
\
void \
name##_destroy(name##_t *c) \
{ \
    name##_id_table_destroy(&c->id_table); \
    name##_name_table_destroy(&c->name_table); \
    name##_pool_destroy(&c->pool); \
    mdb_close(&c->mdb); \
    memset(c, 0, sizeof(name##_t)); \
} \
\
static inline type * \
name##_get_by_id(name##_t *c, uint32 id) \
{ \
    name##_item_t **item = name##_id_table_find(&c->id_table, id); \
    return item ? &(*item)->asset : 0; \
} \
\
static inline type * \
name##_get_by_name(name##_t *c, const char *name) \
{ \
    name##_item_t **item = name##_name_table_find(&c->name_table, name); \
    return item ? &(*item)->asset : 0; \
}

#endif /* MUTA_ASSET_CONTAINER_H */
