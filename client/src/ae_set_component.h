#ifndef MUTA_CLIENT_AE_SET_COMPONENT_H
#define MUTA_CLIENT_AE_SET_COMPONENT_H
#include "component.h"
#include "../../shared/common_utils.h"

/* Forward declaration(s) */
typedef struct  as_ae_set_t as_ae_set_t;
typedef void*   component_handle_t;

/* Types defined here */
typedef struct ae_set_component_t   ae_set_component_t;
typedef struct ae_set_timer_t       ae_set_timer_t;
typedef struct ae_set_system_t      ae_set_system_t;

struct ae_set_system_t
{
    fixed_pool(ae_set_component_t)  components;
    ae_set_timer_t                  *idle_timers;
    uint32                          num_idle_timers;
    /* After an entity enters the idle state, a timer is started before
     * transitioning to the idle animation, so as not to look continuous walk
     * animations choppy between tiles. */
};

extern component_definition_t ae_set_component_definition;

void
ae_set_component_set_asset(component_handle_t handle, as_ae_set_t *set);

#endif /* MUTA_CLIENT_AE_SET_COMPONENT_H */
