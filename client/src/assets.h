/* assets.h
 * Some assets have use reference counting to dynamically load and unload data.
 * For such assets, there are usually two functions: as_claim_* and
 * as_unclaim_*. The unclaim function should be called when the asset it stopped
 * using somewhere.
 * If the asset users no reference counting but is always loaded, the function
 * is named in the form of as_get*. */

#ifndef MUTA_ASSETS_H
#define MUTA_ASSETS_H

#include "bitmap.h"
#include "gl.h"
#include "animated_entity.h"

/* Forward declaration(s) */
typedef struct gui_font_t gui_font_t;

/* Types defined here */
typedef struct as_tex_t                     as_tex_t;
typedef struct as_tex_db_entry_t            as_tex_db_entry_t;
typedef struct as_spritesheet_t             as_spritesheet_t;
typedef struct as_animation_t               as_animation_t;
typedef struct as_ae_t                      as_ae_t;
typedef struct as_ae_set_t                  as_ae_set_t;
typedef struct sound_t                      sound_t;
typedef struct as_sound_t                   as_sound_t;
typedef struct as_cursor_t                  as_cursor_t;
typedef struct as_creature_display_t        as_creature_display_t;
typedef struct as_dynamic_object_display_t  as_dynamic_object_display_t;
typedef struct as_iso_sprite_t              as_iso_sprite_t;

enum as_tex_flag
{
    AS_TEX_KEEP_LOADED          = (1 << 0),
    AS_TEX_KEEP_BITMAP          = (1 << 1),
    AS_TEX_LOADING              = (1 << 2),
    AS_TEX_LOADED               = (1 << 3),
    AS_TEX_IN_UNCLAIMED_QUEUE   = (1 << 4)
};

struct as_tex_t
{
    tex_t       tex;
    img_t       bitmap; /* Relevant if flag AS_TEX_KEEP_BITMAP is on */
    uint32      user_count;
    uint8       flags;
    uint8       min_filter;
    uint8       mag_filter;
    uint8       wrapping;
};

struct as_tex_db_entry_t
{
    uint32      id;
    const char  *name;
};

struct as_spritesheet_t
{
    as_tex_t    *ta;
    float       (*clips)[4];
    char        **clip_names;
    int         num_clips;
    uint32      user_count;
    int8        keep_loaded;
};

struct as_animation_t
{
    anim_t      anim;
    uint32      user_count;
    int8        keep_loaded;
};

struct as_ae_t
{
    animated_entity_t   ae;
    uint32              user_count;
    int8                keep_loaded;
};

struct as_ae_set_t
{
    uint32 id;
    uint32 idle_id;
    uint32 walk_id;
};

struct sound_t
{
    uint32 buffer;
};

struct as_sound_t
{
    sound_t     sound;
    uint32      user_count;
    uint        keep_loaded:1;
    uint        loaded:1;
    uint        loading:1;
    uint        errors_loading:1;
    uint        in_unclaimed_queue:1;
};

struct as_cursor_t
{
    dchar   *name;
    void    *data;
};

struct as_creature_display_t
{
    uint32  id;
    uint32  ae_set;
    int16   name_plate_offset_x;
    int16   name_plate_offset_y;
};

struct as_dynamic_object_display_t
{
    uint32  id;
    uint32  ae_set;
    dchar   *inventory_icon_id;
};

struct as_iso_sprite_t
{
    uint32      id;
    uint32      tex_id;
    uint32      user_count;
    as_tex_t    *ta;
    float       clips[8][4];
    float       offsets[8][2];
    uint8       flags;
};

int
as_load(void);
/* as_load()
 * Load assets. This function may panic if assets are corrupted or missing. */

void
as_destroy(void);

void
as_update(void);
/* as_update()
 * Called every frame. Updates unused asset timers. */

int
as_bind_queued_textures(void);
/* as_bind_queued_textures()
 * Called continuously in game updates to finish asynchronous texture creation.
 * */

/*-- Textures --*/

as_tex_t *
as_claim_tex(uint32 id, bool32 async);

void
as_unclaim_tex(as_tex_t *t);

bool32
as_tex_exists(uint32 id);

const dchar *
as_tex_get_path(as_tex_t *ta);

uint32
as_tex_get_id(as_tex_t *ta);

img_t *
as_tex_get_bitmap(as_tex_t *ta);

as_tex_t *
as_get_default_tex(void);

int
as_get_num_tex_db_entries(void);

int
as_get_tex_db_entry(int index, as_tex_db_entry_t *ret_tex_db_entry);

/*-- Spritesheets --*/

as_spritesheet_t *
as_claim_spritesheet(uint32 id, bool32 async);

void
as_unclaim_spritesheet(as_spritesheet_t *ss);

float *
as_spritesheet_get_clip(as_spritesheet_t *ss, const char *name);

const dchar *
as_spritesheet_get_path(as_spritesheet_t *ss);

/*-- Animations --*/

as_animation_t *
as_claim_anim_asset_by_name(const char *name, bool32 async);

as_animation_t *
as_claim_anim_asset_by_id(uint32 id, bool32 async);

void
as_unclaim_anim_asset(as_animation_t *a);

const dchar *
as_animation_get_path(as_animation_t *aa);

/*-- Animated entities --*/

as_ae_t *
as_claim_ae(uint32 id, bool32 async);

void
as_unclaim_ae(as_ae_t *ae);

const dchar *
as_ae_get_path(as_ae_t *aa);

/*-- Animated entity sets --*/

as_ae_set_t *
as_get_ae_set(uint32 id);

/*-- Sounds --*/

as_sound_t *
as_claim_sound_by_name(const char *name, bool32 async);

as_sound_t *
as_claim_sound_by_id(uint32 id, bool32 async);

void
as_unclaim_sound(as_sound_t *sa);

const dchar *
as_sound_get_path(as_sound_t *sa);

/*-- Cursors --*/

as_cursor_t *
as_get_cursor(const char *name);

/*-- Creature displays --*/

as_creature_display_t *
as_get_creature_display(uint32 id);

/*-- Dynamic object displays --*/

as_dynamic_object_display_t *
as_get_dynamic_object_display(uint32 id);

/*-- Isometric sprites --*/

as_iso_sprite_t *
as_claim_iso_sprite(uint32 id, bool32 async);

void
as_unclaim_iso_sprite(as_iso_sprite_t *is);

/*-- Fonts --*/

gui_font_t *
as_get_font(const char *name);

gui_font_t *
as_get_font_by_index(uint32 index);

uint32
as_num_fonts(void);

gui_font_t *
as_get_default_font(void);

gui_font_t *
as_get_default_pixel_font(void);

#endif /* MUTA_ASSETS_H */
