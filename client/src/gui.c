#include "gui.h"
#include "../../shared/common_utils.h"

static int
_grow_dstr(const gui_text_input_event_t *event,
    union gui_text_input_callback_return_values *ret_vals);

gui_bool_t
gui_text_input_dstr(const char *title, dchar **buf, int x, int y, int w, int h,
    int flags)
{
    uint32 buf_size = dstr_cap(*buf) + 1;
    gui_bool_t ret = gui_text_input_ext(title, *buf, buf_size, x, y, w, h,
        flags, _grow_dstr, buf);
    dstr_set_len(buf, (uint32)strlen(*buf));
    return ret;
}

gui_font_t *
gui_get_text_input_state_style_input_font(gui_text_input_state_style_t *style)
    {return style->input_font ? style->input_font : gui_get_default_font();}

int
gui_get_text_input_style_max_input_font_height(gui_text_input_style_t *style)
{
    int ret = 0;
    for (int i = 0; i < 3; ++i)
    {
        int h = gui_get_text_input_state_style_input_font(
            &style->states[i])->height;
        if (h > ret)
            ret = h;
    }
    return ret;
}

int
gui_get_text_input_style_min_height(gui_text_input_style_t *style)
{
    int ret = 0;
    for (int i = 0; i < 3; ++i)
    {
        int font_h = gui_get_text_input_state_style_input_font(
            &style->states[i])->height;
        int border_th = style->states[i].border.widths[GUI_EDGE_TOP];
        int border_bh = style->states[i].border.widths[GUI_EDGE_BOTTOM];
        int h = font_h + border_th + border_bh;
        if (h > ret)
            ret = h;
    }
    return ret;
}

gui_font_t *
gui_get_text_input_state_style_title_font(gui_text_input_state_style_t *style)
{
    return style->title_font ? style->title_font : gui_get_default_font();
}

int
gui_get_text_input_style_max_title_font_height(gui_text_input_style_t *style)
{
    int ret = 0;
    for (int i = 0; i < 3; ++i)
    {
        int font_h = gui_get_text_input_state_style_title_font(
            &style->states[i])->height;
        if (font_h > ret)
            ret = font_h;
    }
    return ret;
}

gui_font_t *
gui_get_button_state_style_font(gui_button_state_style_t *style)
    {return style->font ? style->font : gui_get_default_font();}

int
gui_get_button_style_max_font_height(gui_button_style_t *style)
{
    int ret = 0;
    for (int i = 0; i < 3; ++i)
    {
        int h = gui_get_button_state_style_font(&style->states[i])->height;
        if (h > ret)
            ret = h;
    }
    return ret;
}

int
gui_get_button_style_min_height(gui_button_style_t *style)
{
    int ret = 0;
    for (int i = 0; i < 3; ++i)
    {
        int font_h = gui_get_button_state_style_font(&style->states[i])->height;
        int border_th = style->states[i].border.widths[GUI_EDGE_TOP];
        int border_bh = style->states[i].border.widths[GUI_EDGE_BOTTOM];
        int h = font_h + border_th + border_bh;
        if (h > ret)
            ret = h;
    }
    return ret;
}

static int
_grow_dstr(const gui_text_input_event_t *event,
    union gui_text_input_callback_return_values *ret_vals)
{
    dchar **dstr = event->user_data;
    dstr_reserve(dstr, event->max_size_reached.min_new_buf_size);
    ret_vals->max_size_reached.new_buf      = *dstr;
    ret_vals->max_size_reached.new_buf_size = dstr_cap(*dstr);
    return 0;
}
