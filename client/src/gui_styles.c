#include "gui_styles.h"
#include "assets.h"
#include "core.h"
#include "log.h"
#include "../../shared/common_utils.h"

gui_button_style_t          gui_style_menu_button;
gui_button_style_t          gui_style_menu_button_highlighted;
gui_text_input_style_t      gui_style_login_text_input;
gui_win_style_t             gui_style_topless_window;
gui_win_style_t             gui_style_window_1;
gui_win_style_t             gui_style_chat_window;
gui_button_style_t          gui_style_file_list_button;
gui_button_style_t          gui_style_typical_button;
gui_button_style_t          gui_style_action_button;
gui_button_style_t          gui_style_menu_bar_button;
gui_button_style_t          gui_style_inventory_item_button;
gui_button_style_t          gui_style_inventory_item_button_moused_over;
gui_text_input_style_t      gui_style_chat_text_input;
gui_progress_bar_style_t    gui_style_player_charge_bar;
gui_progress_bar_style_t    gui_style_name_plate_health_bar;
gui_font_t                  *gui_style_menu_font;
gui_font_t                  *gui_style_menu_font_fancy;

static as_spritesheet_t *_ui_spritesheet;

int
gui_styles_init(void)
{
    _ui_spritesheet = as_claim_spritesheet(core_asset_config.ui_spritesheet_id,
        0);
    /*-- Readable font --*/
    gui_style_menu_font = as_get_default_font();
    if (!gui_style_menu_font)
    {
        core_error_window("Missing font 'roboto'!");
        return 1;
    }
    /*-- Fancy font --*/
    gui_style_menu_font_fancy = as_get_default_font();
    if (!gui_style_menu_font_fancy)
    {
        core_error_window("Missing font 'fancy'!");
        return 2;
    }
    /*-- Menu button style --*/
    gui_style_menu_button = gui_create_button_style();
    for (int i = 0; i < 3; ++i)
        gui_style_menu_button.states[i].font = gui_style_menu_font;
    /*-- Highlighted menu button style --*/
    gui_style_menu_button_highlighted = gui_style_menu_button;
    for (int i = 0; i < GUI_NUM_BUTTON_STATES; ++i)
        gui_set_color_array(gui_style_menu_button_highlighted.states[i].
            background_color, 0, 0, 255, 255);
    /*-- Login text input style --*/
    gui_style_login_text_input = gui_create_text_input_style();
    for (int i = 0; i < 3; ++i)
    {
        gui_style_login_text_input.states[i].title_font =
            gui_style_menu_font_fancy;
        gui_style_login_text_input.states[i].input_font = gui_style_menu_font;
    }
    /*-- Topless window --*/
    gui_style_topless_window = gui_create_win_style();
    for (int i = 0; i < 3; ++i)
    {
        gui_win_state_style_t *s = &gui_style_topless_window.states[i];
        s->title_font                   = 0;
        s->border.widths[GUI_EDGE_TOP]  = s->border.widths[GUI_EDGE_BOTTOM];
    }
    /*-- File list button --*/
    gui_style_file_list_button = gui_create_button_style();
    for (int i = 0; i < 3; ++i)
    {
        gui_button_state_style_t *s = &gui_style_file_list_button.states[i];
        s->title_origin     = GUI_CENTER_LEFT;
        s->title_offset[0]  = 1;
        s->title_offset[1]  = -s->font->height / 4;
    }
    /* Typical button */
    gui_style_typical_button = gui_create_button_style();
    for (int i = 0; i < 3; ++i)
    {
        gui_button_state_style_t *s = &gui_style_typical_button.states[i];
        s->title_origin     = GUI_CENTER_CENTER;
        s->title_offset[0]  = 1;
        s->title_offset[1]  = -s->font->height / 4;
    }
    /* Action button */
    gui_style_action_button = gui_create_button_style();
    for (int i = 0; i < 3; ++i)
    {
        gui_button_state_style_t *ss = &gui_style_action_button.states[i];
        float *clip = as_spritesheet_get_clip(_ui_spritesheet, "action_button");
        if (!clip)
        {
            LOG_ERR("No clip defined for action_button in UI spritesheet.");
            return 3;
        }
        ss->tex = &_ui_spritesheet->ta->tex;
        for (int i = 0; i < 4; ++i)
            ss->tex_clip[i] = clip[i];
        ss->title_origin    = GUI_BOTTOM_LEFT;
        ss->title_offset[0] = 3;
        ss->title_offset[1] = 3;
        gui_set_color_array(ss->background_color, 0, 0, 0, 0);
        gui_set_color_array(ss->border.color, 0, 0, 0, 0);
    }
    /* Menu bar button */
    gui_style_menu_bar_button = gui_style_action_button;
    for (int i = 0; i < 3; ++i)
        gui_set_color_array(gui_style_menu_bar_button.states[i].title_color, 0,
            0, 0, 0);
    /* Inventory item button */
    gui_style_inventory_item_button = gui_create_button_style();
    for (int i = 0; i < 3; ++i)
    {
        gui_button_style_t *s = &gui_style_inventory_item_button;
        gui_button_state_style_t *ss = &s->states[i];
        ss->title_color[3] = 0;
    }
    /* inventory item button when moused over */
    gui_style_inventory_item_button_moused_over =
        gui_style_inventory_item_button;
    gui_style_inventory_item_button_moused_over.states[GUI_BUTTON_STATE_NORMAL]
        = gui_style_inventory_item_button.states[GUI_BUTTON_STATE_HOVERED];
    /* Game windows */
    gui_style_window_1 = gui_create_win_style();
    float *window_border_1_top_left_clip = as_spritesheet_get_clip(
        _ui_spritesheet, "window_border_1_top_left");
    float *window_border_1_horizontal_clip = as_spritesheet_get_clip(
        _ui_spritesheet, "window_border_1_horizontal");
    float *window_border_1_top_right_clip = as_spritesheet_get_clip(
        _ui_spritesheet, "window_border_1_top_right");
    float *window_border_1_vertical_clip = as_spritesheet_get_clip(
        _ui_spritesheet, "window_border_1_vertical");
    float *window_border_1_bottom_left_clip = as_spritesheet_get_clip(
        _ui_spritesheet, "window_border_1_bottom_left");
    float *window_border_1_bottom_right_clip = as_spritesheet_get_clip(
        _ui_spritesheet, "window_border_1_bottom_right");
    if (!window_border_1_top_left_clip)
    {
        LOG_ERR("No clip defined for window_border_1_top_left in UI "
            "spritesheet.");
        return 4;
    }
    if (!window_border_1_horizontal_clip)
    {
        LOG_ERR("No clip defined for window_border_1_horizontal in UI "
            "spritesheet.");
        return 5;
    }
    if (!window_border_1_top_right_clip)
    {
        LOG_ERR("No clip defined for window_border_1_top_right in UI "
            "spritesheet.");
        return 6;
    }
    if (!window_border_1_vertical_clip)
    {
        LOG_ERR("No clip defined for window_border_1_vertical in UI "
            "spritesheet.");
        return 7;
    }
    if (!window_border_1_bottom_left_clip)
    {
        LOG_ERR("No clip defined for window_border_1_bottom_left in UI "
            "spritesheet.");
        return 8;
    }
    if (!window_border_1_bottom_right_clip)
    {
        LOG_ERR("No clip defined for window_border_1_bottom_right in UI "
            "spritesheet.");
        return 9;
    }
    for (int i = 0; i < 3; ++i)
    {
        gui_win_state_style_t *ss = &gui_style_window_1.states[i];
        ss->border.tex = &_ui_spritesheet->ta->tex;
        ss->border.widths[GUI_EDGE_TOP] = window_border_1_vertical_clip[3] -
            window_border_1_vertical_clip[1];
        ss->border.widths[GUI_EDGE_LEFT] = window_border_1_horizontal_clip[2] -
            window_border_1_horizontal_clip[0];
        ss->border.widths[GUI_EDGE_RIGHT] = window_border_1_horizontal_clip[2] -
            window_border_1_horizontal_clip[0];
        ss->border.widths[GUI_EDGE_BOTTOM] = window_border_1_vertical_clip[3] -
            window_border_1_vertical_clip[1];
        gui_set_color_array(ss->border.color, 255, 255, 255, 255);
        memcpy(ss->border.clips[GUI_BORDER_TOP_LEFT],
            window_border_1_top_left_clip, 4 * sizeof(float));
        memcpy(ss->border.clips[GUI_BORDER_TOP],
            window_border_1_horizontal_clip, 4 * sizeof(float));
        memcpy(ss->border.clips[GUI_BORDER_TOP_RIGHT],
            window_border_1_top_right_clip, 4 * sizeof(float));
        memcpy(ss->border.clips[GUI_BORDER_LEFT],
            window_border_1_vertical_clip, 4 * sizeof(float));
        memcpy(ss->border.clips[GUI_BORDER_RIGHT],
            window_border_1_vertical_clip, 4 * sizeof(float));
        memcpy(ss->border.clips[GUI_BORDER_BOTTOM_LEFT],
            window_border_1_bottom_left_clip, 4 * sizeof(float));
        memcpy(ss->border.clips[GUI_BORDER_BOTTOM],
            window_border_1_horizontal_clip, 4 * sizeof(float));
        memcpy(ss->border.clips[GUI_BORDER_BOTTOM_RIGHT],
            window_border_1_bottom_right_clip, 4 * sizeof(float));
        gui_set_color_array(ss->background_color, 47, 46, 58, 255);
    }
    gui_style_chat_window = gui_create_win_style();
    for (int i = 0; i < 3; ++i)
    {
        gui_win_state_style_t *ss = &gui_style_chat_window.states[i];
        for (int j = 0; j < 3; ++j)
            ss->background_color[j] = 0;
        ss->background_color[3] = 64;
        ss->border.color[3] = 0;
        memset(ss->border.widths, 0, sizeof(ss->border.widths));
    }
    /* Chat text input style */
    gui_style_chat_text_input = gui_create_text_input_style();
    /* Charge bar */
    gui_style_player_charge_bar = gui_create_progress_bar_style();
    gui_style_name_plate_health_bar = gui_create_progress_bar_style();
    for (int i = 0; i < GUI_NUM_PROGRESS_BAR_STATES; ++i)
    {
        gui_progress_bar_state_style_t *ss =
            &gui_style_name_plate_health_bar.states[i];
        for (int j = 0; j < 4; ++j)
            ss->border.widths[j] = 1;
        ss->title_color[3]      = 0;
        ss->progress_color[3]   = 0;
    }
    for (int i = 0; i < 3; ++i)
        gui_style_name_plate_health_bar.states[GUI_PROGRESS_BAR_STATE_HOVERED].border.color[i] = 255;
    return 0;
}

void
gui_styles_destroy(void)
    {as_unclaim_spritesheet(_ui_spritesheet);}
