#ifndef MUTA_CLIENT_CREATURE_ENTITY_H
#define MUTA_CLIENT_CREATURE_ENTITY_H

#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct entity_t entity_t;
typedef struct world_t  world_t;

/* Types defined here */
typedef struct creature_entity_t    creature_entity_t;
typedef struct creature_registry_t  creature_registry_t;

struct creature_entity_t
{
    creature_runtime_id_t   id;
    creature_type_id_t      type_id; /* Cannot be invalid */
    uint32                  health_current;
    uint32                  health_max;
    uint8                   flags;
    int8                    sex;
};

entity_t *
creature_spawn(world_t *world, creature_runtime_id_t guid,
    creature_type_id_t type_id, int sex, int direction, int *position,
    uint32 health_current, uint32 health_max, bool32 dead);

void
creature_walk(entity_t *entity, int *position);

void
creature_set_current_health(entity_t *entity, uint32 current_health);

#endif /* MUTA_CLIENT_CREATURE_ENTITY_H */
