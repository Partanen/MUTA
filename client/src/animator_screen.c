#include <math.h>
#include "animator_screen.h"
#include "core.h"
#include "game_screen.h"
#include "render.h"
#include "../../shared/gui/gui.h"
#include "assets.h"
#include "main_menu_screen.h"
#include "../../shared/common_utils.h"
#include "../../shared/tile.h"
#include "gui.h"

#define NAME_LIMIT  32
#define CTYPE_LIMIT 10

enum animator_state
{
    STATE_NORMAL = 0,
    STATE_SELECTION,
    STATE_COLOR
};

enum view_state
{
    VIEW_FRAME = 0,
    VIEW_ANIMATION,
    VIEW_BOTH,
    VIEW_TEXTURE
};

static enum animator_state _animator_state;
static enum view_state     _view_toggle;
static float               _tile_sx, _tile_sy;
static float               _tile_ox, _tile_oy;

static char  _custom_input[CTYPE_LIMIT];
static char  _buf_input[128];
static int   _custom_offset = 0;
static int   _buf_offset    = 0;
static float _zoom_scale    = 1.0f;
static float _anim_ox;
static float _anim_oy;
static float _eanim_ox      = 256.0f;
static float _eanim_oy      =   0.0f;
static uint8 _clip_bcol[4]  = {  255, 255,   0, 255 };
static uint8 _select_col[4] = {  255, 255, 255, 255 };
static uint8 _bg_color[4]   = {    32,   118, 255, 255 };
static uint8 *_edited_col;

static gui_win_style_t     _window_style;
static gui_win_style_t     _window_style_color;
static gui_button_style_t  _button_style;
static gui_button_style_t  _input_style;
static as_tex_t        *_dir_arrow;
static as_tex_t        *_default_tex;
static as_tex_t        *_tile_asset;
static bool32              _view_lt;
static bool32              _view_tile;
static bool32              _anim_mode;
static bool32              _set_lay_hue;
static bool32              _message_box;
static char                _message[1024];
static char                _dropped_file[256];
static char               *_a_info = "Empty";
static int                 _confirm;
static int                 _help_box;
static int                 _layer_index, _frame_index;
static int                 _dir;
static int                 _key;
static int                 _ltshowy;
static int                 _select_ox, _select_oy;
static int                 _anchor_x,  _anchor_y;
static float               _layer_ox,  _layer_oy;
static float               _mouse_clip[4];
static double              _held;
static int                 _sidebar_width;
static int                 _help_button_wh;
static int                 _help_button_xy;
static int                 _set_layer_type_button_w;
static int                 _set_layer_type_button_h;
static bool32              _show_texture_selection;

static ae_layer_t         *_edit_layer;
static ae_frame_t         *_edit_frame;
static ae_animation_t     *_edit_anim;
static animated_entity_t   _anime;

static const float         _dur = 1.0f / 60.f;
static float               _timer;
static int                 _index;

static ae_animation_t       *_cur_anim;

static int
_animator_screen_init(void);

static void
_animator_screen_update(double dt);

static void
_animator_screen_open(void);

static void
_animator_screen_close(void);

static void
_animator_screen_keydown(int key, bool32 is_repeat);

static void
_animator_screen_text_input(const char *text);

static void
_animator_screen_mousebuttonup(uint8 button, int x, int y);

static void
_animator_screen_mousebuttondown(uint8 button, int x, int y);

static void
_animator_screen_mousewheel(int x, int y);

static void
_animator_screen_file_drop(const char *path);

static void
_update_animation(double dt);

static void
_update(double dt);

static void
_hotkeys();

static void
_buttons(int text_start_y, int *ret_lowest_button_bottom_y,
    int *ret_button_area_w);

static void
_mouse_selection();

static void
_mouse_selection_done();

static void
_color_selection();

static void
_anim_change_to(int index);

static void
_anim_clear();

static void
_frame_offset(float x, float y, bool32 all_frames);

static void
_frame_change_to(int index);

static void
_frame_move_to(int index);

static void
_frame_create(bool32 copy);

static void
_frame_clear();

static void
_frame_remove();

static void
_clip_move(float x, float y);

static void
_clip_change_size(float x, float y);

static void
_layer_start_drag();

static void
_layer_drag();

static void
_layer_offset(float x, float y);

static void
_layer_rotate(int step);

static void
_layer_change_to(int index);

static void
_layer_move_to(int index);

static void
_layer_create();

static void
_layer_remove();

static void
_layer_copy_values(ae_layer_t *dest, ae_layer_t *src);

static void
_layer_set_default_values(ae_layer_t *layer);

static void
_zoom(float adjust);

static void
_reset_animation();

static void
_free_animation();

static void
_save_animation();

static void
_save_maa();

static void
_save_mae();

static void
_load_animation_maa();

static void
_load_animation_mae();

static void
_gui_confirm_dialog(char *question, void (*yes)());

static void
_gui_message_box();

static void
_gui_help_box();

static void
_back_to_menu();

static void
_fix_path(char *path);

static void
_render_edited_frame();

static void
_render_edited_animation();

static void
_render_clip_bounds(int ox, int oy, float zs);

static void
_render_full_texture();

static void
_render_info(int ox, int *oy);

static void
_render_dir_arrow(int x, int y);

static char *
_get_layer_type_string(int layer);

static void
_draw_texture_selection(void);

screen_t animator_screen =
{
    .name               = "Animator",
    .init               = _animator_screen_init,
    .destroy            = 0,
    .update             = _animator_screen_update,
    .open               = _animator_screen_open,
    .close              = _animator_screen_close,
    .text_input         = _animator_screen_text_input,
    .keydown            = _animator_screen_keydown,
    .keyup              = 0,
    .mousebuttondown    = _animator_screen_mousebuttondown,
    .mousebuttonup      = _animator_screen_mousebuttonup,
    .mousewheel         = _animator_screen_mousewheel,
    .file_drop          = _animator_screen_file_drop
};

static int
_animator_screen_init(void)
{
    if (!(_dir_arrow = as_claim_tex(
        core_asset_config.animator_screen_direction_arrow_tex_id, 0)))
    {
        core_error_window("Animator screen: direction arrow asset not found, "
            "(check asset_config.cfg).");
        return 1;
    }
    if (!(_tile_asset = as_claim_tex(core_asset_config.tileset_tex_id, 0)))
    {
        core_error_window("Animator screen: tileset texture not found (check "
            "asset_config.cfg).");
        return 2;
    }
    _default_tex = as_get_default_tex();
    _sidebar_width = (int)(0.3f * (float)core_asset_config.resolution_w);
    _help_button_wh = (int)(0.017f * (float)core_asset_config.resolution_w);
    _help_button_xy = _help_button_wh / 2;
    _set_layer_type_button_w = (int)(0.075f * (float)core_asset_config.resolution_w);
    _set_layer_type_button_h = (int)(0.22f * (float)_set_layer_type_button_w);
    return 0;
}

static void
_animator_screen_update(double dt)
{
    gl_viewport_and_scissor(0, 0, core_window_w(), core_window_h());
    gl_color(0.0f, 0.0f, 0.0f, 1.0f);
    gl_clear(RB_CLEAR_COLOR_BIT);
    int vp[4];
    core_compute_target_viewport(vp);
    //r_viewport(0, 0, main_window.w, main_window.h);
    gl_scissor(vp[0],  vp[1], vp[2], vp[3]);
    gl_color((float)_bg_color[0] / 255.0f, (float)_bg_color[1] / 255.0f,
        (float)_bg_color[2] / 255.0f, (float)_bg_color[3] / 255.0f);
    gl_clear(RB_CLEAR_COLOR_BIT);

    gui_win_style(&_window_style);
    gui_origin(GUI_TOP_LEFT); //GUI_CENTER_CENTER
    gui_button_style(&_button_style);
    gui_font(as_get_default_font());

    switch (_animator_state)
    {
        case STATE_NORMAL:    _update(dt);        break;
        case STATE_SELECTION: _mouse_selection(); break;
        case STATE_COLOR:     _color_selection(); break;
    }

    if (gui_is_any_button_pressed())
        _held += dt;
    else
    {
        _held = 0;
        _layer_drag();
    }
    if (_show_texture_selection)
        _draw_texture_selection();
}

static void
_animator_screen_open(void)
{
    _anim_ox = (float)(core_asset_config.resolution_w / 2 - core_asset_config.tile_width / 2);
    _anim_oy = (float)(core_asset_config.resolution_h / 2 - core_asset_config.tile_top_height / 2);

    _tile_ox = 0;
    _tile_oy = 0;//(float)core_asset_config.tile_height;
    _tile_sx = 1.f;//(float)core_asset_config.tile_width / 128.0f;
    _tile_sy = 1.f;//(float)core_asset_config.tile_height / 128.0f;

    _buf_input[0] = 0;
    _buf_offset    = (int)strlen(_buf_input);
    strcpy(_custom_input, "Custom ID");
    _custom_offset = (int)strlen(_custom_input);

    _button_style = gui_create_button_style();
    for (int i = 0; i < 3; ++i)
    {
        _button_style.states[i].title_scale[0] = 1.0f;
        _button_style.states[i].title_scale[1] = 1.0f;
    }

    _input_style = gui_create_button_style();
    for (int i = 0; i < 3; ++i)
    {
        _input_style.states[i].title_scale[0] = 1.5f;
        _input_style.states[i].title_scale[1] = 1.5f;
    }

    for (int i = 0; i < 2; ++i)
    {
        _input_style.states[i].background_color[0]  = 30;
        _input_style.states[i].background_color[1]  = 30;
        _input_style.states[i].background_color[2]  = 30;
        _input_style.states[i].border.color[0]      = 20;
        _input_style.states[i].border.color[1]      = 24;
        _input_style.states[i].border.color[2]      = 73;
    }

    _input_style.states[GUI_BUTTON_STATE_PRESSED].background_color[0]   = 40;
    _input_style.states[GUI_BUTTON_STATE_PRESSED].background_color[1]   = 40;
    _input_style.states[GUI_BUTTON_STATE_PRESSED].background_color[2]   = 40;
    _input_style.states[GUI_BUTTON_STATE_PRESSED].border.color[0]       = 26;
    _input_style.states[GUI_BUTTON_STATE_PRESSED].border.color[1]       = 34;
    _input_style.states[GUI_BUTTON_STATE_PRESSED].border.color[2]       = 94;

    for (int i = 0; i < 8; ++i)
    {
        ae_animation_t *anim = &_anime.anims[i];

        if (anim->frames == 0)
        {
            anim->frames = (ae_frame_t*)malloc(sizeof(ae_frame_t));
            anim->num_frames = 1;

            _edit_frame = &anim->frames[0];
            _edit_frame->layers = (ae_layer_t*)malloc(sizeof(ae_layer_t));
            _edit_frame->num_layers = 1;
            _edit_frame->ox = _edit_frame->oy = 0;

            _edit_layer = &anim->frames->layers[0];
            _edit_layer->ta = 0;
            _layer_set_default_values(_edit_layer);
        } else
            DEBUG_PRINTF("Frames is not 0\n");
    }

    _view_toggle    = VIEW_FRAME;
    _animator_state = STATE_NORMAL;
    _edit_anim      = &_anime.anims[0];
    _edit_frame     = &_edit_anim->frames[0];
    _edit_layer     = &_edit_frame->layers[0];
    _cur_anim = _edit_anim;
    _frame_index    = _layer_index = 0;
    _zoom_scale     =  1.0f;
    _anchor_x       = -9999;
    _anchor_y       = -9999;
    _layer_ox       = -9999;
    _layer_oy       = -9999;
    _eanim_ox       = 256.0f;
    _eanim_oy       =   0.0f;
    _set_lay_hue    =   1;
    _view_tile      =   1;
    _held           =   0;
    _dir            =   0;
    _anim_mode      =   0;
    _confirm        =   0;
    _select_ox      =   0;
    _select_oy      =   0;
    _message_box    =   0;
    _help_box       =   0;
    _ltshowy        =   0;
    _timer          =   0;
    _view_lt        =   0;
    _show_texture_selection = 0;
    snprintf(_message, 128, "Something happened.");
    strcpy(_dropped_file, "Default");
    _a_info = "Animation 1/8 (N)";

    _window_style       = gui_create_win_style();
    _window_style_color = gui_create_win_style();

    for (int i = 0; i < 3; ++i)
    {
        _window_style.states[GUI_WIN_STATE_INACTIVE].background_color[i] = 98;
        _window_style.states[GUI_WIN_STATE_HOVERED].background_color[i] = 98;
    }
    for (int i = 0; i< 3; ++i)
        for (int j = 0; j < 3; ++j)
            _window_style_color.states[i].background_color[j] = 200;
    _window_style.states[GUI_WIN_STATE_INACTIVE].background_color[3]    = 255;
    _window_style.states[GUI_WIN_STATE_HOVERED].background_color[3]     = 255;
    _window_style.states[GUI_WIN_STATE_ACTIVE].background_color[0]      = 161;
    _window_style.states[GUI_WIN_STATE_ACTIVE].background_color[1]      = 142;
    _window_style.states[GUI_WIN_STATE_ACTIVE].background_color[2]      = 120;
    _window_style.states[GUI_WIN_STATE_ACTIVE].background_color[3]      = 120;
}

static void
_animator_screen_close(void)
{
    _free_animation();
}

static void
_animator_screen_text_input(const char *text)
{
}

static void
_animator_screen_keydown(int key, bool32 is_repeat)
{
    switch(key)
    {
        case CORE_KEY_TAB:
        {
            ++_view_toggle;
            if (_view_toggle > 3)
                _view_toggle = 0;
        }
        break;
        case  CORE_KEY_BACKSPACE:
        {
        }
        break;
        case CORE_KEY_ESCAPE:
        {
            if (_animator_state == STATE_SELECTION)
                _animator_state = STATE_NORMAL;
            else if (_confirm == 0)
                _confirm = 1;
        }
        break;
        case CORE_KEY_H:
        {
            if (_help_box == 0)
                _help_box = 1;
        }
        break;
    }

    _key = key;
}

static void
_animator_screen_mousebuttonup(uint8 button, int x, int y)
{
    if (_anchor_x != -9999 && _anchor_y != -9999)
    {
        _mouse_selection_done();
        _anchor_x = _anchor_y = -9999;
        _layer_ox = _layer_oy = -9999;
    }
}

static void
_animator_screen_mousebuttondown(uint8 button, int x, int y)
{
    if (_anchor_x == -9999 && _anchor_y == -9999)
    {
        _anchor_x = x;
        _anchor_y = y;
        core_compute_pos_scaled_by_target_viewport(&_anchor_x, &_anchor_y);
        _layer_start_drag();
    }
}

static void
_animator_screen_mousewheel(int x, int y)
{
    int layer_selection = _layer_index;
    if (y > 0)      layer_selection += 1;
    else if (y < 0) layer_selection -= 1;

    if (layer_selection != _layer_index)
        _layer_change_to(layer_selection);
}

static void
_animator_screen_file_drop(const char *path)
{
    DEBUG_PRINTF("Dropped File %s\n", path);
    char temp[256];
    strcpy(temp, path);
    _fix_path(temp);
    strcpy(_dropped_file, temp);

    char file[256];
    strcpy(file, path);

    char *file_extension;
    file_extension = strtok(temp, ".");
    if (!file_extension)
        goto warning;

    file_extension = strtok(0, ".");
    if (!file_extension)
        goto warning;

    if (str_insensitive_cmp(file_extension, "maa") == 0)
    {
        _confirm = 3;
        return;
    } else
    if (str_insensitive_cmp(file_extension, "mae") == 0)
    {
        _confirm = 4;
        return;
    }

    warning:
    strcpy(_dropped_file, "Default");
    snprintf(_message, 512, "'%s'\nis not a valid file for Entity Animator.\n"
                            "Make sure that the file is inside the MUTA's "
                            "directory and/or that it is included in assets.dat file.",
                            file);
    _message_box = 1;
}

static void
_update_animation(double dt)
{
    _timer += (float)dt;

    if (_timer >= _dur)
    {
        _timer = 0;
        _index++;
        if (_index >= _cur_anim->num_frames)
            _index = 0;
    }
}

static void
_update(double dt)
{
    if (core_key_down(CORE_KEY_LSHIFT))
    {
        if (core_key_down(CORE_KEY_LEFT))       _eanim_ox -= 4;
        else if (core_key_down(CORE_KEY_RIGHT)) _eanim_ox += 4;
        if (core_key_down(CORE_KEY_UP))         _eanim_oy -= 4;
        else if (core_key_down(CORE_KEY_DOWN))  _eanim_oy += 4;
    } else
    {
        if (core_key_down(CORE_KEY_LEFT))       _anim_ox -= 4;
        else if (core_key_down(CORE_KEY_RIGHT)) _anim_ox += 4;
        if (core_key_down(CORE_KEY_UP))         _anim_oy -= 4;
        else if (core_key_down(CORE_KEY_DOWN))  _anim_oy += 4;
    }

    _update_animation(dt);
    switch (_view_toggle)
    {
        case VIEW_FRAME:     _render_edited_frame();     break;
        case VIEW_ANIMATION: _render_edited_animation(); break;
        case VIEW_BOTH:      _render_edited_animation();
                             _render_edited_frame();     break;
        case VIEW_TEXTURE:   _render_full_texture();     break;
    }

    switch (_confirm)
    {
        case 1:
        {
            _gui_confirm_dialog("Are you sure you want to exit?\n"
                                "All unsaved changes will be lost forever.",
                                _back_to_menu);
        }
        break;
        case 2:
        {
            char temp_question[384];
            snprintf(temp_question, 383, "Are you sure you want to save "
                                         "the animation as\n'%s'?\n"
                                         "You are currently in '%s' mode.\n\n"
                                         "File will be saved in "
                                         "assets/animations.\n"
                                         "If file already exists, it will be "
                                         "overwritten.",
                                         _buf_input,
                                         _anim_mode ? "Animation": "Entity");
            _gui_confirm_dialog(temp_question, _save_animation);
        }
        break;
        case 3:
        {
            _gui_confirm_dialog("Are you sure you want to load the dropped "
                                "Animation? It will replace currently edited "
                                "Animation.", _load_animation_maa);
        }
        break;
        case 4:
        {
            _gui_confirm_dialog("Are you sure you want to load the dropped\n"
                                "Entity Animation? You will lose all unsaved "
                                "changes.", _load_animation_mae);
        }
        break;
    }

    if (_confirm != 0)
        return;
    else if (_message_box)
    {
		_gui_message_box();
        return;
    } else
	if (_help_box)
    {
		_gui_help_box();
    }

    uint8 back_col[4] = { 25, 25, 25, 127 };
    gui_color(back_col[0], back_col[1], back_col[2], back_col[3]);
    gui_rectangle(0, 0, _sidebar_width, core_asset_config.resolution_h);
    gui_color(255, 255, 255, 255);

    gui_text_s("Entity Animator", 0, 16, 0, 2.0f);
    int title_text_bottom_y = gui_get_last_text_y() + gui_get_last_text_h();

    _hotkeys();
    int lowest_button_bottom_y;
    int bottom_bar_w;
    _buttons(title_text_bottom_y, &lowest_button_bottom_y, &bottom_bar_w);

    gui_text_input_style(0);
    gui_text_input("Save File", _buf_input, sizeof(_buf_input),
        (_sidebar_width - bottom_bar_w) / 2,
        lowest_button_bottom_y + gui_get_default_font()->height + 4,
        bottom_bar_w, 20, 0);

}

static void
_hotkeys()
{
    if (_animator_state != STATE_NORMAL || gui_is_any_text_input_active() ||
        _view_toggle == VIEW_ANIMATION  || _view_toggle == VIEW_TEXTURE ||
        _help_box    || _message_box)
    {
        _key = 0;
        return;
    }

    bool32 ctrl  = core_key_down(CORE_KEY_LCTRL);
    bool32 shift = core_key_down(CORE_KEY_LSHIFT);

    if (!ctrl)
    {
        float step = shift ? 5.0f : 1.0f;
        if (core_key_down(CORE_KEY_W))
            _frame_offset(0, -step, 0);
        if (core_key_down(CORE_KEY_S))
            _frame_offset(0,  step, 0);
        if (core_key_down(CORE_KEY_A))
            _frame_offset(-step, 0, 0);
        if (core_key_down(CORE_KEY_D))
            _frame_offset(step,  0, 0);
    }
    switch(_key)
    {
        case CORE_KEY_S:
        {
            if (ctrl)
                _layer_create();
        }
        break;
        case CORE_KEY_D:
        {
            if (ctrl)
                _frame_create(1);
        }
        break;
        case CORE_KEY_DELETE:
        {
            if (ctrl)
                _frame_remove();
            else
                _layer_remove();
        }
        break;
        case CORE_KEY_Q:
            _frame_change_to(_frame_index - 1);
        break;
        case CORE_KEY_E:
            _frame_change_to(_frame_index + 1);
        break;
        case CORE_KEY_KP_PLUS:   _zoom(0.1f); break;
        case CORE_KEY_KP_MINUS: _zoom(-0.1f); break;
        case CORE_KEY_1:  _anim_change_to(0); break;
        case CORE_KEY_2:  _anim_change_to(1); break;
        case CORE_KEY_3:  _anim_change_to(2); break;
        case CORE_KEY_4:  _anim_change_to(3); break;
        case CORE_KEY_5:  _anim_change_to(4); break;
        case CORE_KEY_6:  _anim_change_to(5); break;
        case CORE_KEY_7:  _anim_change_to(6); break;
        case CORE_KEY_8:  _anim_change_to(7); break;
    }
    if (core_key_down_now(CORE_KEY_T))
        _show_texture_selection = _show_texture_selection ? 0 : 1;

    _key = 0;
}

static void
_buttons(int text_start_y, int *ret_lowest_button_bottom_y,
    int *ret_button_area_w)
{
    int   btn_spacing = 2;
    int   btn_w    = (int)(0.065f * (float)core_asset_config.resolution_w);
    int   btn_xs   = (_sidebar_width - (btn_w * 4 + 3 * btn_spacing)) / 2;
    int   btn_h    = gui_get_button_style_min_height(&_button_style);
    int   btn_x1   = btn_xs + btn_w * 3 + btn_spacing * 3;
    int   btn_x2   = btn_xs + btn_w * 2 + btn_spacing * 2;
    int   btn_x3   = btn_xs + btn_w * 1 + btn_spacing * 1;
    int   btn_x4   = btn_xs + btn_w * 0 + btn_spacing * 0;
    int   btn_tx_y = 2;
    int   btn_offy = 4 + btn_h + btn_tx_y + btn_h / 5;
    int   txt_y    = text_start_y + 2;
    int   txt_off  = 4 + btn_h + btn_tx_y + btn_h / 5;
    float step     = 1.0f;
    int   btn_y;

    if (_held > 4.0f)
        step = 6.0f;
    else if (_held > 2.0f)
        step = 3.0f;

    //gui_origin(GUI_TOP_RIGHT);
    _render_info(0, &txt_y);

    gui_origin(GUI_TOP_RIGHT);
    if (gui_button("?", _help_button_xy, _help_button_xy, _help_button_wh,
        _help_button_wh, 0))
        _help_box = _help_box ? 0 : 1;
    gui_origin(GUI_TOP_LEFT);

    gui_text("Create/delete/edit Frames", 0, 20, txt_y);
    btn_y = gui_get_last_text_y() + gui_get_last_text_h() + 2;
    if (gui_button("Delete Frame", btn_x1, btn_y, btn_w, btn_h, 0))
        _frame_remove();
    if (gui_button("New Frame",    btn_x2, btn_y, btn_w, btn_h, 0))
        _frame_create(0);
    if (gui_button("Next Frame",   btn_x3, btn_y, btn_w, btn_h, 0))
        _frame_change_to(_frame_index + 1);
    if (gui_button("Prev. Frame",   btn_x4, btn_y, btn_w, btn_h, 0))
        _frame_change_to(_frame_index - 1);

    txt_y = gui_get_last_button_y() + gui_get_last_button_h() + 2;
    btn_y += (int)(btn_offy / 1.5f);
    if (gui_button("Clear Frame",  btn_x1, btn_y, btn_w, btn_h, 0))
        _frame_clear();
    if (gui_button("Duplicate F.", btn_x2, btn_y, btn_w, btn_h, 0))
        _frame_create(1);
    if (gui_button("Send F. Front", btn_x3, btn_y, btn_w, btn_h, 0))
        _frame_move_to(_edit_anim->num_frames - 1);
    if (gui_button("Move F. Up",   btn_x4, btn_y, btn_w, btn_h, 0))
        _frame_move_to(_frame_index + 1);

    txt_y = gui_get_last_button_y() + gui_get_last_button_h() + 2;
    btn_y += (int)(btn_offy / 1.5f);
    if (gui_button("Send F. Back", btn_x3, btn_y, btn_w, btn_h, 0))
        _frame_move_to(0);
    if (gui_button("Move F. Down",  btn_x4, btn_y, btn_w, btn_h, 0))
        _frame_move_to(_frame_index - 1);

    txt_y = gui_get_last_button_y() + gui_get_last_button_h() + 2;
    gui_text("Frame Offsets", 0, 20, txt_y);
    btn_y = gui_get_last_text_y() + gui_get_last_text_h() + 2;
    if (gui_button("Offset All Y+", btn_x1, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Offset All Y+",
        btn_x2, btn_y, btn_w, btn_h, 0)))
        _frame_offset(0,  step, 1);
    if (gui_button("Offset All X+", btn_x2, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Offset All X+",
        btn_x4, btn_y, btn_w, btn_h, 0)))
        _frame_offset(step,  0, 1);
    if (gui_button("Offset Y+", btn_x3, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Offset Y+",
        btn_x2, btn_y, btn_w, btn_h, 0)))
        _frame_offset(0,  step, 0);
    if (gui_button("Offset X+", btn_x4, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Offset X+",
        btn_x4, btn_y, btn_w, btn_h, 0)))
        _frame_offset(step,  0, 0);

    btn_y = gui_get_last_button_y() + gui_get_last_button_h() + 2;
    if (gui_button("Offset All Y-", btn_x1, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Offset All Y-",
        btn_x1, btn_y, btn_w, btn_h, 0)))
        _frame_offset(0, -step, 1);
    if (gui_button("Offset All X-", btn_x2, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Offset All X-",
        btn_x3, btn_y, btn_w, btn_h, 0)))
        _frame_offset(-step, 0, 1);
    if (gui_button("Offset Y-", btn_x3, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Offset Y-",
        btn_x1, btn_y, btn_w, btn_h, 0)))
        _frame_offset(0, -step, 0);
    if (gui_button("Offset X-", btn_x4, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Offset X-",
        btn_x3, btn_y, btn_w, btn_h, 0)))
        _frame_offset(-step, 0, 0);

    txt_y = gui_get_last_button_y() + gui_get_last_button_h() + 2;
    gui_text("Create/delete/edit Layers", 0, 20, txt_y);
    btn_y = gui_get_last_text_y() + gui_get_last_text_h() + 2;
    if (gui_button("Delete Layer", btn_x1, btn_y, btn_w, btn_h, 0))
        _layer_remove();
    if (gui_button("New Layer",    btn_x2, btn_y, btn_w, btn_h, 0))
        _layer_create();
    if (gui_button("Next Layer",   btn_x3, btn_y, btn_w, btn_h, 0))
        _layer_change_to(_layer_index + 1);
    if (gui_button("Prev. Layer",   btn_x4, btn_y, btn_w, btn_h, 0))
        _layer_change_to(_layer_index - 1);

    btn_y = gui_get_last_button_y() + gui_get_last_button_h() + 2;
    if (gui_button("Send L. Front", btn_x1, btn_y, btn_w, btn_h, 0))
        _layer_move_to(_edit_frame->num_layers - 1);
    if (gui_button("Move L. Up",    btn_x2, btn_y, btn_w, btn_h, 0))
        _layer_move_to(_layer_index + 1);
    if (gui_button("L. Offset Y+",  btn_x3, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("L. Offset Y+",
        btn_x2, btn_y, btn_w, btn_h, 0)))
        _layer_offset(0, step);
    if (gui_button("L. Offset X+",  btn_x4, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("L. Offset X+",
        btn_x4, btn_y, btn_w, btn_h, 0)))
        _layer_offset(step, 0);

    btn_y = gui_get_last_button_y() + gui_get_last_button_h() + 2;
    if (gui_button("Send L. Back",  btn_x1, btn_y, btn_w, btn_h, 0))
        _layer_move_to(0);
    if (gui_button("Move L. Down",  btn_x2, btn_y, btn_w, btn_h, 0))
        _layer_move_to(_layer_index - 1);
    if (gui_button("L. Offset Y-",  btn_x3, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("L. Offset Y-",
        btn_x1, btn_y, btn_w, btn_h, 0)))
        _layer_offset(0, -step);
    if (gui_button("L. Offset X-",  btn_x4, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("L. Offset X-",
        btn_x3, btn_y, btn_w, btn_h, 0)))
        _layer_offset(-step, 0);

    btn_y = gui_get_last_button_y() + gui_get_last_button_h() + 2;
    if (gui_button("Rotate Right",  btn_x3, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Rotate Right",
        btn_x3, btn_y, btn_w, btn_h, 0)))
        _layer_rotate((int)step);
    if (gui_button("Rotate Left",  btn_x4, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Rotate Left",
        btn_x1, btn_y, btn_w, btn_h, 0)))
        _layer_rotate((int)-step);
    if (gui_button("Flip Layer", btn_x2, btn_y, btn_w, btn_h, 0))
    {
        _edit_layer->flip++;
        if (_edit_layer->flip > 3) _edit_layer->flip = 0;
    }
    if (gui_button("Clear Layer",  btn_x1, btn_y, btn_w, btn_h, 0))
        _layer_set_default_values(_edit_layer);

    txt_y = gui_get_last_button_y() + gui_get_last_button_h() + 2;
    gui_text("Edit Clip", 0, 20, txt_y);
    btn_y = gui_get_last_text_y() + gui_get_last_text_h() + 2;
    if (gui_button("Move Right", btn_x1, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Move Right",
        btn_x4, btn_y, btn_w, btn_h, 0)))
        _clip_move(step, 0);
    if (gui_button("Move Up",    btn_x2, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Move Up",
        btn_x1, btn_y, btn_w, btn_h, 0)))
        _clip_move(0, -step);
    if (gui_button("Clip Height+", btn_x3, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Clip Height+",
        btn_x2, btn_y, btn_w, btn_h, 0)))
        _clip_change_size(0,  step);
    if (gui_button("Clip Width+",  btn_x4, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Clip Width+",
        btn_x4, btn_y, btn_w, btn_h, 0)))
        _clip_change_size(step, 0);

    btn_y = gui_get_last_button_y() + gui_get_last_button_h() + 2;
    if (gui_button("Move Left",  btn_x1, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Move Left",
        btn_x3, btn_y, btn_w, btn_h, 0)))
        _clip_move(-step, 0);
    if (gui_button("Move Down",  btn_x2, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Move Down",
        btn_x2, btn_y, btn_w, btn_h, 0)))
        _clip_move(0, step);
    if (gui_button("Clip Height-", btn_x3, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Clip Height-",
        btn_x1, btn_y, btn_w, btn_h, 0)))
        _clip_change_size(0, -step);
    if (gui_button("Clip Width-",  btn_x4, btn_y, btn_w, btn_h, 0) ||
        (_held >= 0.25 && gui_invisible_repeat_button("Clip Width-",
        btn_x3, btn_y, btn_w, btn_h, 0)))
        _clip_change_size(-step, 0);

    txt_y = gui_get_last_button_y() + gui_get_last_button_h() + 2;
    gui_text("Change Animation", 0, 20, txt_y);
    btn_y = gui_get_last_text_y() + gui_get_last_text_h() + 2;
    if (gui_button("Next Anim.", btn_x3, btn_y, btn_w, btn_h, 0))
        _anim_change_to(_dir + 1);
    if (gui_button("Prev. Anim.", btn_x4, btn_y, btn_w, btn_h, 0))
        _anim_change_to(_dir - 1);
    gui_text(_a_info, 0, btn_x2 + 6, btn_y);
    _render_dir_arrow(btn_x1 + 32, btn_y);

    txt_y = gui_get_last_button_y() + gui_get_last_button_h() + 2;
    btn_y = txt_y;
    gui_text("Clip Area", 0, btn_x1, txt_y);
    char zoom_buf[10];
    snprintf(zoom_buf, 10, "Zoom: %.1f", _zoom_scale);
    gui_text(zoom_buf, 0, 20, txt_y);
    if (gui_button("Default Zoom", btn_x2, btn_y, btn_w, btn_h, 0))
        _zoom_scale = 1.0f;
    if (gui_button("Zoom out",     btn_x3, btn_y, btn_w, btn_h, 0))
        _zoom(-0.1f);
    if (gui_button("Zoom in",      btn_x4, btn_y, btn_w, btn_h, 0))
        _zoom(0.1f);
    if (gui_button("Select Clip",  btn_x1, btn_y, btn_w, btn_h, 0))
    {
        _held           = 0;
        _animator_state = STATE_SELECTION;
        _select_ox = _select_oy = 64;
        _anchor_x  = _anchor_y  = -9999;
    }

    txt_y = gui_get_last_button_y() + gui_get_last_button_h() + 2;
    btn_y = txt_y;
    gui_text("Change Colors", 0, btn_x4, txt_y);
    gui_text("Toggle View",   0, btn_x1, txt_y);
    char *current_view = 0;
    switch(_view_toggle)
    {
        default:
        case 0: current_view = "Show Frame";   break;
        case 1: current_view = "Show Anim.";   break;
        case 2: current_view = "Show Both";    break;
        case 3: current_view = "Show Texture"; break;
    }

    if (gui_button(current_view, btn_x1, btn_y, btn_w, btn_h, 0))
    {
        ++_view_toggle;
        if (_view_toggle > 3)
            _view_toggle = 0;
    }
    if (gui_button("BG. Color", btn_x3, btn_y, btn_w, btn_h, 0))
    {
        _held           = 0;
        _set_lay_hue    = 0;
        _animator_state = STATE_COLOR;

        _edited_col     = _bg_color;
        _select_col[0]  = _bg_color[0];
        _select_col[1]  = _bg_color[1];
        _select_col[2]  = _bg_color[2];
        _select_col[3]  = _bg_color[3];
    }
    if (gui_button("Layer Hue", btn_x4, btn_y, btn_w, btn_h, 0))
    {
        _held           = 0;
        _set_lay_hue    = 1;
        _animator_state = STATE_COLOR;

        _edited_col     = _edit_layer->color;
        _select_col[0]  = _edit_layer->color[0];
        _select_col[1]  = _edit_layer->color[1];
        _select_col[2]  = _edit_layer->color[2];
        _select_col[3]  = _edit_layer->color[3];
    }
    if (gui_button("Border Color", btn_x2, btn_y, btn_w, btn_h, 0))
    {
        _held           = 0;
        _set_lay_hue    = 0;
        _animator_state = STATE_COLOR;

        _edited_col     = _clip_bcol;
        _select_col[0]  = _clip_bcol[0];
        _select_col[1]  = _clip_bcol[1];
        _select_col[2]  = _clip_bcol[2];
        _select_col[3]  = _clip_bcol[3];
    }

    gui_origin(GUI_TOP_RIGHT);
    if (gui_button("Set Layer Type", _help_button_xy + _help_button_wh + 2,
        _help_button_xy, _set_layer_type_button_w, _set_layer_type_button_h, 0))
        _view_lt = _view_lt == 1 ? 0 : 1;
    int lty = _help_button_xy + _help_button_wh;

    int lt_btn_h = gui_get_button_style_max_font_height(&_button_style);

    if (_view_lt)
    {
        _ltshowy += 2;
        if (_ltshowy > lt_btn_h + 2)
            _ltshowy = lt_btn_h + 2;
    }
    else
    {
        _ltshowy -= 2;
        if (_ltshowy < 0)
            _ltshowy = 0;
    }

    if (_ltshowy)
    {
        int lt_btn_w = (int)(0.100f * (float)core_asset_config.resolution_w);
        for (int i = 0; i < NUM_AE_LAYER_TYPES + 1; ++i)
        {
            lty += _ltshowy;
            if (gui_button(_get_layer_type_string(i),
                _help_button_xy, lty,
                lt_btn_w, lt_btn_h,
                0))
                _edit_layer->type = i;
        }

        lty += _ltshowy * 2;
        if (gui_button("Custom ID",
            _help_button_xy, lty, lt_btn_w, lt_btn_h, 0))
        {
            int temp = NUM_AE_LAYER_TYPES;
            int msg = 1;
            if (sscanf(_custom_input, "%d", &temp) == 1)
            {
                if (temp >= NUM_AE_LAYER_TYPES)
                {
                    _edit_layer->type = temp;
                    msg = 0;
                }
            }
            if (msg)
            {
                snprintf(_message, 512, "Given number is in "
                                        "incorrect format. Make sure "
                                        "that it is\nnumber, and that "
                                        "its equal or larger than %d."
                                        , NUM_AE_LAYER_TYPES + 1);
                _message_box = 1;
            }
        }
    }

    gui_origin(GUI_TOP_LEFT);

    txt_y += (int)(txt_off);
    btn_y += (int)(btn_offy);
    if (gui_button("Toggle Tile", btn_x1, btn_y, btn_w, btn_h, 0))
        _view_tile = _view_tile == 1 ? 0 : 1;
    if (gui_button("Select texture", btn_x2, btn_y, btn_w, btn_h, 0))
        _show_texture_selection = 1;

    btn_y += gui_get_last_button_h() + (int)(btn_offy);

    char *anim_mode = _anim_mode ? "Type: Anim" : "Type: Entity";

    if (gui_button("Exit", btn_x4, btn_y, btn_w, btn_h, 0))
        _confirm = 1;

    if (gui_button("Save", btn_x3, btn_y, btn_w, btn_h, 0))
        _confirm = 2;

    if (gui_button(anim_mode, btn_x2, btn_y, btn_w * 2 + 2, btn_h, 0))
        _anim_mode = _anim_mode == 1 ? 0 : 1;

    *ret_lowest_button_bottom_y = gui_get_last_button_y() +
        gui_get_last_button_h();
    *ret_button_area_w = btn_x1 + btn_w - btn_x4;
}

static void
_mouse_selection()
{
    if (core_key_down(CORE_KEY_LEFT))       _select_ox -= 4;
    else if (core_key_down(CORE_KEY_RIGHT)) _select_ox += 4;
    if (core_key_down(CORE_KEY_UP))         _select_oy -= 4;
    else if (core_key_down(CORE_KEY_DOWN))  _select_oy += 4;

    gui_origin(GUI_TOP_LEFT);
    gui_text_s("Selecting Clip, press ESC to cancel", 0, 16, 0, 2.0f);

    float clip[4] = { 0, 0, _edit_layer->ta->tex.w, _edit_layer->ta->tex.h };
    gui_texture(&_edit_layer->ta->tex, clip, (int)_select_ox, (int)_select_oy);
    _render_clip_bounds((int)(_select_ox + _edit_layer->clip[0]),
                        (int)(_select_oy + _edit_layer->clip[1]), 1.0f);

    if (_anchor_x == -9999 || _anchor_y == -9999)
        return;

    int oax = _anchor_x;
    int oay = _anchor_y;
    int mx  = core_mouse_x();
    int my  = core_mouse_y();
    core_compute_pos_scaled_by_target_viewport(&mx, &my);

    if (mx < _anchor_x)
    {
        int temp  = _anchor_x;
        _anchor_x = mx;
        mx        = temp;
    }
    if (my < _anchor_y)
    {
        int temp  = _anchor_y;
        _anchor_y = my;
        my        = temp;
    }

    int w  = ABS(mx - _anchor_x);
    int h  = ABS(my - _anchor_y);

    uint8 colori[4];
    colori[0] = 255;
    colori[1] = colori[2] = 0;
    colori[3] = 60;
    gui_color(colori[0], colori[1], colori[2], colori[3]);
    gui_rectangle(_anchor_x, _anchor_y, w, h);
    gui_color(255, 255, 255, 255);
    _mouse_clip[0] = (float)(_anchor_x - _select_ox);
    _mouse_clip[1] = (float)(_anchor_y - _select_oy);
    _mouse_clip[2] = (float)(mx - _select_ox);
    _mouse_clip[3] = (float)(my - _select_oy);

    _anchor_x = oax;
    _anchor_y = oay;
}

static void
_mouse_selection_done()
{
    if (_animator_state != STATE_SELECTION)
        return;
    
    _animator_state = STATE_NORMAL;
    _mouse_clip[0]  = CLAMP(_mouse_clip[0], 0, _edit_layer->ta->tex.w - 1);
    _mouse_clip[1]  = CLAMP(_mouse_clip[1], 0, _edit_layer->ta->tex.h - 1);
    _mouse_clip[2]  = CLAMP(_mouse_clip[2],
                      _mouse_clip[0] + 1, _edit_layer->ta->tex.w);
    _mouse_clip[3]  = CLAMP(_mouse_clip[3],
                      _mouse_clip[1] + 1, _edit_layer->ta->tex.h);

    _edit_layer->clip[0] = _mouse_clip[0];
    _edit_layer->clip[1] = _mouse_clip[1];
    _edit_layer->clip[2] = _mouse_clip[2];
    _edit_layer->clip[3] = _mouse_clip[3];
}

static void
_color_selection()
{
    gui_win_style(&_window_style_color);
    gui_origin(GUI_TOP_LEFT);

    int w = 640;
    int h = 448;
    int x = core_asset_config.resolution_w / 2 - w / 2;
    int y = core_asset_config.resolution_h / 2 - h / 2;

    gui_begin_win("  ", x, y, w, h, 0);

    char r_val[8];
    char g_val[8];
    char b_val[8];
    char a_val[8];

    snprintf(r_val, 8, "R: %d", _select_col[0]);
    snprintf(g_val, 8, "G: %d", _select_col[1]);
    snprintf(b_val, 8, "B: %d", _select_col[2]);
    snprintf(a_val, 8, "A: %d", _select_col[3]);

    int right_x = 136;
    int left_x  = 32;
    int txt_x   = 80;
    int pos_y   = 32;

    uint8 base_color[4] = { 25, 25, 25, 100 };
    gui_color(base_color[0], base_color[0], base_color[0], base_color[0]);
    gui_rectangle(0, 0, 224, h);
    gui_color(255, 255, 255, 255);

    if (gui_button("R+", right_x, pos_y, 36, 36, 0) || (_held >= 0.25 && 
        gui_invisible_repeat_button("R+", right_x, 48, 36, 36, 0)))
	{
        if (_select_col[0] < 255)
            ++_select_col[0];
        else
            _select_col[0] = 0;
	}
    if (gui_button("R-", left_x, pos_y, 36, 36, 0) || (_held >= 0.25 && 
        gui_invisible_repeat_button("R-", left_x, 48, 36, 36, 0)))
	{
        if (_select_col[0] > 0)
            --_select_col[0];
        else
            _select_col[0] = 255;
	}
    gui_text_s(r_val, 0, txt_x, pos_y, 1.5f);
    pos_y += 48;

    if (gui_button("G+", right_x, pos_y, 36, 36, 0) || (_held >= 0.25 && 
        gui_invisible_repeat_button("G+", right_x, 96, 36, 36, 0)))
	{
		if (_select_col[1] < 255)
            ++_select_col[1];
        else
            _select_col[1] = 0;
	}
    if (gui_button("G-", left_x, pos_y, 36, 36, 0) || (_held >= 0.25 && 
        gui_invisible_repeat_button("G-", left_x, 96, 36, 36, 0)))
	{
		if (_select_col[1] > 0)
            --_select_col[1];
        else
            _select_col[1] = 255;
	}
    gui_text_s(g_val, 0, txt_x, pos_y, 1.5f);
    pos_y += 48;

    if (gui_button("B+", right_x, pos_y, 36, 36, 0) || (_held >= 0.25 && 
        gui_invisible_repeat_button("B+", right_x, 144, 36, 36, 0)))
	{
        if (_select_col[2] < 255)
            ++_select_col[2];
        else
            _select_col[2] = 0;
	}
    if (gui_button("B-", left_x, pos_y, 36, 36, 0) || (_held >= 0.25 && 
        gui_invisible_repeat_button("B-", left_x, 144, 36, 36, 0)))
	{
        if (_select_col[2] > 0)
            --_select_col[2];
        else
            _select_col[2] = 255;
	}
    gui_text_s(b_val, 0, txt_x, pos_y, 1.5f);
    pos_y += 48;

    if (gui_button("A+", right_x, pos_y, 36, 36, 0) || (_held >= 0.25 && 
        gui_invisible_repeat_button("A+", right_x, 192, 36, 36, 0)))
	{
        if (_select_col[3] < 255)
            ++_select_col[3];
        else
            _select_col[3] = 0;
	}
    if (gui_button("A-", left_x, pos_y, 36, 36, 0) || (_held >= 0.25 && 
        gui_invisible_repeat_button("A-", left_x, 192, 36, 36, 0)))
	{
        if (_select_col[3] > 0)
            --_select_col[3];
        else
            _select_col[3] = 255;
	}
    gui_text_s(a_val, 0, txt_x, pos_y, 1.5f);
    pos_y += 48;

    if (gui_button("Red", 30, pos_y, 64, 36, 0))
    {
        _select_col[0] = 255;
        _select_col[1] = 0;
        _select_col[2] = 0;
        _select_col[3] = 255;
    }
    if (gui_button("Green", 30 + 80, pos_y, 64, 36, 0))
    {
        _select_col[0] = 0;
        _select_col[1] = 255;
        _select_col[2] = 0;
        _select_col[3] = 255;
    }
    pos_y += 48;
    if (gui_button("Blue", 30, pos_y, 64, 36, 0))
    {
        _select_col[0] = 0;
        _select_col[1] = 0;
        _select_col[2] = 255;
        _select_col[3] = 255;
    }
    if (gui_button("Yellow", 30 + 80, pos_y, 64, 36, 0))
    {
        _select_col[0] = 255;
        _select_col[1] = 255;
        _select_col[2] = 0;
        _select_col[3] = 255;
    }
    pos_y += 48;
    if (gui_button("Black", 30, pos_y, 64, 36, 0))
    {
        _select_col[0] = 0;
        _select_col[1] = 0;
        _select_col[2] = 0;
        _select_col[3] = 255;
    }
    if (gui_button("White", 30 + 80, pos_y, 64, 36, 0))
    {
        _select_col[0] = 255;
        _select_col[1] = 255;
        _select_col[2] = 255;
        _select_col[3] = 255;
    }

    if (gui_button("Accept", 30, h - 64, 80, 36, 0))
    {
        _edited_col[0] = _select_col[0];
        _edited_col[1] = _select_col[1];
        _edited_col[2] = _select_col[2];
        _edited_col[3] = _select_col[3];
        _animator_state = STATE_NORMAL;
    }

    if (gui_button("Cancel", 30 + 80 + 16, h - 64, 80, 36, 0))
        _animator_state = STATE_NORMAL;

    if (_set_lay_hue)
        gui_texture_scfr(&_edit_layer->ta->tex, _edit_layer->clip, 
            240, 16, 1.0f, 1.0f, _select_col, _edit_layer->flip,
            _edit_layer->rot);
    else
    {
        gui_color(_select_col[0], _select_col[1], _select_col[2],
            _select_col[3]);
        gui_rectangle(240, 16, w - 256, h - 48);
        gui_color(255, 255, 255, 255);
    }

    gui_end_win();

    gui_origin(GUI_BOTTOM_LEFT);
    gui_win_style(&_window_style);
}

static void
_anim_change_to(int index)
{
    if (_dir == index)
        return;

    _dir = index;
    if (_dir > 7)
        _dir = 0;
    else if (_dir < 0)
        _dir = 7;

    _reset_animation();
    _edit_anim       = &_anime.anims[_dir];
    _edit_frame      = &_edit_anim->frames[0];
    _edit_layer      = &_edit_frame->layers[0];
    _cur_anim  = _edit_anim;
    _frame_index     = _layer_index = 0;

    switch(_dir)
    {
        case 0:  _a_info = "Animation 1/8 (N)";  break;
        case 1:  _a_info = "Animation 2/8 (NE)"; break;
        case 2:  _a_info = "Animation 3/8 (E)";  break;
        case 3:  _a_info = "Animation 4/8 (SE)"; break;
        case 4:  _a_info = "Animation 5/8 (S)";  break;
        case 5:  _a_info = "Animation 6/8 (SW)"; break;
        case 6:  _a_info = "Animation 7/8 (W)";  break;
        case 7:  _a_info = "Animation 8/8 (NW)"; break;
        default: _a_info = "Suomiräppäri";       break;
    }
}

static void
_anim_clear()
{
    _frame_change_to(0);
    int loop = _edit_anim->num_frames;
    for (int i = 0; i < loop; ++i)
        _frame_remove();
    _frame_clear();
}

static void
_frame_offset(float x, float y, bool32 all_frames)
{
    if (all_frames)
    {
        for (int i = 0; i < _edit_anim->num_frames; ++i)
        {
            _edit_anim->frames[i].ox += x;
            _edit_anim->frames[i].oy += y;
        }
    }
    else
    {
        _edit_frame->ox += x;
        _edit_frame->oy += y;
    }
}

static void
_frame_change_to(int index)
{
    int orig_layer = _layer_index;

    if (index < 0)
        index = _edit_anim->num_frames - 1;
    else if (index > _edit_anim->num_frames - 1)
        index = 0;

    _frame_index  = index;
    _edit_frame   = &_edit_anim->frames[_frame_index];

    if (_edit_frame->num_layers >= orig_layer)
        _layer_change_to(orig_layer);
    else
        _layer_change_to(0);
}

static void
_frame_move_to(int index)
{
    if (index < 0 || index == _frame_index ||
        index > _edit_anim->num_frames - 1)
        return;

    int step = index < _frame_index ? -1 : 1;

    for (int i = 0; i < _edit_anim->num_frames; ++i)
    {
        int test = _frame_index + step;
        ae_frame_t temp = _edit_anim->frames[_frame_index];
        _edit_anim->frames[_frame_index] = _edit_anim->frames[test];
        _edit_anim->frames[test]        = temp;

        _frame_index  = test;
        _edit_frame = &_edit_anim->frames[_frame_index];

        if (_frame_index == index)
            break;
    }

    _edit_layer = &_edit_frame->layers[_layer_index];
}

static void
_frame_create(bool32 copy)
{
    ae_frame_t *new_frames = realloc(_edit_anim->frames,
        (_edit_anim->num_frames + 1) * sizeof(ae_frame_t));

    if (new_frames == 0)
    {
        DEBUG_PRINTF("Failed to realloc frames in %s\n", __func__);
        return;
    }

    int layer_to_use = 0;
    int insert = _frame_index + 1;
    _reset_animation();
    _edit_anim->frames = new_frames;

    ae_frame_t *new_frame = &new_frames[_edit_anim->num_frames];
    if (copy)
    {
        layer_to_use = _layer_index;
        ae_frame_t *copy_frame = &new_frames[_frame_index];
        new_frame->num_layers = copy_frame->num_layers;
        new_frame->ox          = copy_frame->ox;
        new_frame->oy          = copy_frame->oy;

        new_frame->layers = (ae_layer_t*)malloc(sizeof(ae_layer_t)
                            * new_frame->num_layers);
        for (int i = 0; i < new_frame->num_layers; ++i)
            _layer_copy_values(&new_frame->layers[i], &copy_frame->layers[i]);
    }
    else
    {
        new_frame->num_layers = 1;
        new_frame->layers = (ae_layer_t*)malloc(sizeof(ae_layer_t));
        new_frame->ox = new_frame->oy = 0;
        new_frame->layers[0].ta = 0;
        _layer_set_default_values(&new_frame->layers[0]);
    }

    _edit_frame   = new_frame;
    _frame_index  = _edit_anim->num_frames;
    _edit_anim->num_frames += 1;
    _layer_index  = layer_to_use;
    _edit_layer   = &_edit_frame->layers[_layer_index];
    _frame_move_to(insert);
}

static void
_frame_clear()
{
    for (int i = 0; i < _edit_frame->num_layers; ++i)
    {
        _edit_layer = &_edit_frame->layers[i];
        _layer_index  = i;
        _layer_remove();
    }

    _edit_frame->ox = _edit_frame->oy = 0;
    _edit_layer = &_edit_frame->layers[0];
    _layer_index = 0;
    _edit_frame->num_layers = 1;
    _layer_set_default_values(&_edit_frame->layers[0]);
    
}

static void
_frame_remove()
{
    if (_edit_anim->num_frames <= 1)
        return;

    _reset_animation();

    int temp_index = _frame_index;

    _frame_move_to(_edit_anim->num_frames - 1);
    _frame_index    = _edit_anim->num_frames - 1;
    _edit_frame   = &_edit_anim->frames[_frame_index];

    _edit_layer   = 0;
    _layer_index    = 0;

    free(_edit_frame->layers);
    _edit_frame->layers      = 0;
    _edit_frame->num_layers = 0;

    ae_frame_t *new_frames = realloc(_edit_anim->frames,
        (_edit_anim->num_frames - 1) * sizeof(ae_frame_t));
    if (new_frames == 0)
    {
        DEBUG_PRINTF("Failed to realloc frames in %s, \
            it's layers are lost forever :(\n", __func__);
        _frame_move_to(temp_index);
        _frame_index  = temp_index;
        _edit_frame = &_edit_anim->frames[temp_index];
        _edit_frame->layers      = (ae_layer_t*)malloc(sizeof(ae_layer_t));
        _edit_frame->num_layers = 1;

        _edit_layer = &_edit_frame->layers[0];
        _layer_set_default_values(&_edit_frame->layers[0]);
        return;
    }

    if (temp_index > _edit_anim->num_frames - 2)
        temp_index = _edit_anim->num_frames - 2;

    _frame_index = temp_index;
    _layer_index = 0;

    _edit_anim->num_frames -= 1;
    _edit_anim->frames       = new_frames;
    _edit_frame              = &new_frames[temp_index];
    _edit_layer              = &_edit_frame->layers[0];
}

static void
_clip_move(float x, float y)
{
    float x1 = _edit_layer->clip[0];
    float y1 = _edit_layer->clip[1];
    float x2 = _edit_layer->clip[2];
    float y2 = _edit_layer->clip[3];

    float w = x2 - x1;
    float h = y2 - y1;

    x1 = CLAMP(x1 + x, 0, _edit_layer->ta->tex.w);
    y1 = CLAMP(y1 + y, 0, _edit_layer->ta->tex.h);

    x2 = x1 + w;
    y2 = y1 + h;
    
    if (x1 >= x2)
        x1 = x2 - 1;
    if (y1 >= y2)
        y1 = y2 - 1;

    if (x != 0 && x2 >= _edit_layer->ta->tex.w)
        return;
    if (y != 0 && y2 >= _edit_layer->ta->tex.h)
        return;

    _edit_layer->clip[0] = x1;
    _edit_layer->clip[1] = y1;
    _edit_layer->clip[2] = x2;
    _edit_layer->clip[3] = y2;
}

static void
_clip_change_size(float x, float y)
{
    float x1 = _edit_layer->clip[0];
    float y1 = _edit_layer->clip[1];
    float x2 = _edit_layer->clip[2];
    float y2 = _edit_layer->clip[3];

    x2 += x;
    y2 += y;

    if (x2 > _edit_layer->ta->tex.w)
        x2 = _edit_layer->ta->tex.w;
    if (x2 <= x1)
        x2 = x1 + 1;

    if (y2 > _edit_layer->ta->tex.h)
        y2 = _edit_layer->ta->tex.h;
    if (y2 <= y1)
        y2 = y1 + 1;

    _edit_layer->clip[0] = x1;
    _edit_layer->clip[1] = y1;
    _edit_layer->clip[2] = x2;
    _edit_layer->clip[3] = y2;
}

static void
_layer_start_drag()
{
    if (_animator_state != STATE_NORMAL ||
        _view_toggle == VIEW_ANIMATION  || _view_toggle == VIEW_TEXTURE)
        return;

    float zoom_oxy = _zoom_scale != 1 ? (_zoom_scale - 1) * -64 : 0;
    float layer_x  = _anim_ox + zoom_oxy +
                    (_edit_layer->ox + _edit_frame->ox) * _zoom_scale;
    float layer_y  = _anim_oy + zoom_oxy +
                    (_edit_layer->oy + _edit_frame->oy) * _zoom_scale;

    float clipw    = _edit_layer->clip[2] - _edit_layer->clip[0];
    float cliph    = _edit_layer->clip[3] - _edit_layer->clip[1];

    if (_anchor_x >= layer_x && _anchor_x <= layer_x + clipw * _zoom_scale)
    {
        if (_anchor_y >= layer_y && _anchor_y <= layer_y + cliph * _zoom_scale)
        {
            _layer_ox = _edit_layer->ox;
            _layer_oy = _edit_layer->oy;
        }
    }
}

static void
_layer_drag()
{
    if (_anchor_x == -9999 || _anchor_y == -9999 ||
        _layer_ox == -9999 || _layer_oy == -9999 ||
        _animator_state != STATE_NORMAL)
        return;

    int mx  = core_mouse_x();
    int my  = core_mouse_y();
    core_compute_pos_scaled_by_target_viewport(&mx, &my);

    int offx = mx - _anchor_x;
    int offy = my - _anchor_y;

    float ox = (float)(int)round(_layer_ox + (offx / _zoom_scale));
    float oy = (float)(int)round(_layer_oy + (offy / _zoom_scale));

    _edit_layer->ox = ox;
    _edit_layer->oy = oy;
}

static void
_layer_offset(float x, float y)
{
    _edit_layer->ox += x;
    _edit_layer->oy += y;
}

static void
_layer_rotate(int step)
{
    float rad = step * (float)PI / 180.0f;
    _edit_layer->rot += rad;
    int angle_deg = (int)round(_edit_layer->rot * 180.0f / (float)PI);

    if (angle_deg > 360 && step > 0)
        _edit_layer->rot = 0;
    else if (angle_deg < 0 && step < 0)
        _edit_layer->rot = 360 * (float)PI / 180.0f;
}

static void
_layer_change_to(int index)
{
    if (index < 0)
        index = _edit_frame->num_layers - 1;
    else if (index > _edit_frame->num_layers - 1)
        index = 0;

    _layer_index  = index;
    _edit_layer = &_edit_frame->layers[_layer_index];
}

static void
_layer_move_to(int index)
{
    if (index < 0 || index == _layer_index ||
        index > _edit_frame->num_layers - 1)
        return;

    int step = index < _layer_index ? -1 : 1;

    for (int i = 0; i < _edit_frame->num_layers; ++i)
    {
        int test = _layer_index + step;
        ae_layer_t temp = _edit_frame->layers[_layer_index];
        _edit_frame->layers[_layer_index] = _edit_frame->layers[test];
        _edit_frame->layers[test]        = temp;

        _layer_index  = test;
        _edit_layer = &_edit_frame->layers[_layer_index];

        if (_layer_index == index)
            break;
    }
}

static void
_layer_create()
{
    ae_layer_t *new_layers = realloc(_edit_frame->layers,
        (_edit_frame->num_layers + 1) * sizeof(ae_layer_t));

    if (new_layers == 0)
    {
        DEBUG_PRINTF("Failed to realloc layers in %s\n", __func__);
        return;
    }

    int insert = _layer_index + 1;
    _edit_frame->layers   = new_layers;
    _edit_layer           = &_edit_frame->layers[_layer_index];

    ae_layer_t *new_layer = &new_layers[_edit_frame->num_layers];
    _layer_copy_values(new_layer, _edit_layer);

    _layer_index = _edit_frame->num_layers;
    _edit_frame->num_layers += 1;
    _edit_layer = new_layer;
    _layer_move_to(insert);
}

static void
_layer_remove()
{
    if (_edit_frame->num_layers <= 1)
        return;

    int temp_index = _layer_index;
    _layer_move_to(_edit_frame->num_layers - 1);
    _layer_index    = _edit_frame->num_layers - 1;
    _edit_layer   = &_edit_frame->layers[_edit_frame->num_layers - 1];

    ae_layer_t *new_layers = realloc(_edit_frame->layers,
        (_edit_frame->num_layers - 1) * sizeof(ae_layer_t));

    if (new_layers == 0)
    {
        DEBUG_PRINTF("Failed to realloc layers in %s\n", __func__);
        _layer_move_to(temp_index);
        _layer_index  = temp_index;
        _edit_layer = &_edit_frame->layers[temp_index];
        return;
    }

    if (temp_index > _edit_frame->num_layers - 2)
        temp_index = _edit_frame->num_layers - 2;

    _edit_frame->num_layers -= 1;
    _edit_frame->layers       = new_layers;
    _edit_layer               = &_edit_frame->layers[temp_index];
    _layer_index                = temp_index;
}

static void
_zoom(float adjust)
{
    _zoom_scale += adjust;
    if (_zoom_scale < 0.1f)
        _zoom_scale = 0.1f;
    else if (_zoom_scale > 5.0f)
        _zoom_scale = 5.0f;
}

static void
_layer_set_default_values(ae_layer_t *layer)
{
    if (layer == 0)
        return;

    if (layer->ta == 0)
        layer->ta = _default_tex;
    layer->color[0] = 255;
    layer->color[1] = 255;
    layer->color[2] = 255;
    layer->color[3] = 255;
    layer->clip[0]  = 0;
    layer->clip[1]  = 0;
    layer->clip[2]  = layer->ta->tex.w;
    layer->clip[3]  = layer->ta->tex.h;
    layer->ox = layer->oy = 0;
    layer->rot      = 0;
    layer->flip     = 0;
    layer->type     = NUM_AE_LAYER_TYPES;
}

static void
_layer_copy_values(ae_layer_t *dest, ae_layer_t *src)
{
    dest->ta       = src->ta;
    dest->color[0] = src->color[0];
    dest->color[1] = src->color[1];
    dest->color[2] = src->color[2];
    dest->color[3] = src->color[3];
    dest->clip[0]  = src->clip[0];
    dest->clip[1]  = src->clip[1];
    dest->clip[2]  = src->clip[2];
    dest->clip[3]  = src->clip[3];
    dest->ox       = src->ox;
    dest->oy       = src->oy;
    dest->rot      = src->rot;
    dest->flip     = src->flip;
    dest->type     = src->type;
}

static void
_reset_animation()
{
    _timer = 0;
    _index = 0;
}

static void
_free_animation()
{
    for (int i = 0; i < 8; ++i)
    {
        ae_animation_t *anim = &_anime.anims[i];
        if (anim)
        {
            if (anim->frames)
            {
                for (int j = 0; j < anim->num_frames; ++j)
                {
                    ae_frame_t *frame = &anim->frames[j];
                    if (frame)
                        free(frame->layers);
                    frame->layers = 0;
                    frame->num_layers = 0;
                }
                free(anim->frames);
                anim->frames = 0;
                anim->num_frames = 0;
            }
        }
    }

    _edit_layer  = 0;
    _edit_frame  = 0;
    _edit_anim   = 0;
    _frame_index = _layer_index = 0;
}

static void
_save_animation()
{
    if ((int)strlen(_buf_input) <= 3)
    {
        snprintf(_message, 512, "File name is too short.\n");
        _message_box = 1;
        return;
    }

    if (_anim_mode)
        _save_maa();
    else
        _save_mae();
}

static void
_save_maa()
{
    // Saving single animation
    char buf[386];
    snprintf(buf, 386, "assets/animations/%s.maa", _buf_input);

    FILE *f;
    f = fopen(buf, "w");

    if (!f)
    {
        snprintf(_message, 512, "Failed to create file\n'%s'\n."
                                "Does assets/animations exist?", buf);
        _message_box = 1;
        return;
    }

    int fc = _edit_anim->num_frames;
    fprintf(f, "%s\n", _buf_input);
    fprintf(f, "Frames %d\n", fc);

    for (int i = 0; i < fc; ++i)
    {
        fprintf(f, "\n");
        ae_frame_t *frame = &_edit_anim->frames[i];
        int lc = frame->num_layers;

        fprintf(f, "Frame %d\n", i);
        fprintf(f, "Layers %d\n", lc);
        fprintf(f, "Offsets %d %d\n", (int)round(frame->ox),
                                      (int)round(frame->oy));

        for (int j = 0; j < lc; ++j)
        {
            ae_layer_t *layer = &frame->layers[j];
            fprintf(f, "Layer %d\n", j);
            fprintf(f, "Texture %u\n", as_tex_get_id(layer->ta));
            fprintf(f, "Color %d %d %d %d\n",
                (int)(layer->color[0]), (int)(layer->color[1]),
                (int)(layer->color[2]), (int)(layer->color[3]));
            fprintf(f, "Clip %d %d %d %d\n",
                (int)round(layer->clip[0]), (int)round(layer->clip[1]),
                (int)round(layer->clip[2]), (int)round(layer->clip[3]));
            fprintf(f, "Offsets %d %d\n",   (int)round(layer->ox),
                                            (int)round(layer->oy));
            fprintf(f, "Rotation %f\n", layer->rot);
            fprintf(f, "Flip %d\n",     layer->flip);
            fprintf(f, "Type %d\n",     layer->type);
        }
    }

    safe_fclose(f);
    snprintf(_message, 512, "Successfully saved the file as\n"
                            "'assets/animations/%s.maa'", _buf_input);
    _message_box = 1;
}

static void
_save_mae()
{
    char buf[386];
    snprintf(buf, 386, "assets/animations/%s.mae", _buf_input);

    FILE *f;
    f = fopen(buf, "w");

    if (!f)
    {
        snprintf(_message, 512, "Failed to create file\n'%s'\n."
                                "Does assets/animations exist?", buf);
        _message_box = 1;
        return;
    }

    fprintf(f, "%s\n", _buf_input);

    for (int a = 0; a < 8; ++a)
    {
        ae_animation_t *anim = &_anime.anims[a];
        fprintf(f, "\n");
        fprintf(f, "Animation %d\n", a);

        int fc = anim->num_frames;
        fprintf(f, "Frames %d\n", fc);

        for (int i = 0; i < fc; ++i)
        {
            ae_frame_t *frame = &anim->frames[i];
            int lc = frame->num_layers;

            fprintf(f, "\nFrame %d\n", i);
            fprintf(f, "Layers %d\n", lc);
            fprintf(f, "Offsets %d %d\n", (int)round(frame->ox),
                                          (int)round(frame->oy));

            for (int j = 0; j < lc; ++j)
            {
                ae_layer_t *layer = &frame->layers[j];
                fprintf(f, "Layer %d\n", j);
                DEBUG_PRINTFF("WRITING %u\n", as_tex_get_id(layer->ta));
                fprintf(f, "Texture %u\n", as_tex_get_id(layer->ta));
                fprintf(f, "Color %d %d %d %d\n",
                    (int)(layer->color[0]), (int)(layer->color[1]),
                    (int)(layer->color[2]), (int)(layer->color[3]));
                fprintf(f, "Clip %d %d %d %d\n",
                    (int)round(layer->clip[0]), (int)round(layer->clip[1]),
                    (int)round(layer->clip[2]), (int)round(layer->clip[3]));
                fprintf(f, "Offsets %d %d\n", (int)round(layer->ox),
                                              (int)round(layer->oy));
                fprintf(f, "Rotation %f\n", layer->rot);
                fprintf(f, "Flip %d\n",     layer->flip);
                fprintf(f, "Type %d\n",     layer->type);
            }
        }
    }

    safe_fclose(f);
    snprintf(_message, 512, "Successfully saved the file as\n"
                            "'assets/animations/%s.mae'", _buf_input);
    _message_box = 1;
}

static void
_load_animation_maa()
{
    FILE *f;
    f = fopen(_dropped_file, "r");

    if (!f)
    {
        snprintf(_message, 512, "Failed to open dropped Animation\n"
                                "'%s'.\n", _dropped_file);
        _message_box = 1;
        return;
    }

    _anim_clear();

    char name[NAME_LIMIT];
    char line[512];
    int fc = 0;

    fgets(line, 512, f);
    sscanf(line, "%s", name);
    fgets(line, 512, f);
    sscanf(line, "Frames %d", &fc);

    int error_code = 0;

    if (fc <= 0) { error_code = 1; goto error; }

    for (int i = 0; i < fc; ++i)
    {
        int lc = 0;
        fgets(line, 512, f);
        fgets(line, 512, f);
        fgets(line, 512, f);
        if (sscanf(line, "Layers %d", &lc) != 1)
        { error_code = 2; goto error; }

        int fox = 0, foy = 0;
        fgets(line, 512, f);
        if (sscanf(line, "Offsets %d %d", &fox, &foy) != 2)
        { error_code = 3; goto error; }

        if (i > 0)
            _frame_create(0);

        _edit_frame->ox  = (float)fox;
        _edit_frame->oy  = (float)foy;

        for (int j = 0; j < lc; ++j)
        {
            fgets(line, 512, f);
            uint32 tex_id;
            fgets(line, 512, f);
            if (sscanf(line, "Texture %u", &tex_id) != 1)
            { error_code = 4; goto error; }

            int color[4];
            fgets(line, 512, f);
            if (sscanf(line, "Color %d %d %d %d",
                &color[0], &color[1], &color[2], &color[3]) != 4)
            { error_code = 5; goto error; }

            int clip[4];
            fgets(line, 512, f);
            if (sscanf(line, "Clip %d %d %d %d",
                &clip[0], &clip[1], &clip[2], &clip[3]) != 4)
            { error_code = 6; goto error; }

            int lox = 0, loy = 0;
            fgets(line, 512, f);
            if (sscanf(line, "Offsets %d %d", &lox, &loy) != 2)
            { error_code = 7; goto error; }

            float rot = 0;
            fgets(line, 512, f);
            if (sscanf(line, "Rotation %f", &rot) != 1)
            { error_code = 8; goto error; }

            int flip = 0;
            fgets(line, 512, f);
            if (sscanf(line, "Flip %d", &flip) != 1)
            { error_code = 9; goto error; }

            int type = NUM_AE_LAYER_TYPES;
            fgets(line, 512, f);
            if (sscanf(line, "Type %d", &type) != 1)
            { error_code = 10; goto error; }

            if (j > 0)
                _layer_create();

            as_tex_t *ta = as_claim_tex(tex_id, 1);
            if (ta == 0)
            { error_code = 11; goto error; }

            _edit_layer->ta       = ta;
            _edit_layer->color[0] = (uint8)color[0];
            _edit_layer->color[1] = (uint8)color[1];
            _edit_layer->color[2] = (uint8)color[2];
            _edit_layer->color[3] = (uint8)color[3];
            _edit_layer->clip[0]  = (float)clip[0];
            _edit_layer->clip[1]  = (float)clip[1];
            _edit_layer->clip[2]  = (float)clip[2];
            _edit_layer->clip[3]  = (float)clip[3];
            _edit_layer->ox       = (float)lox;
            _edit_layer->oy       = (float)loy;
            _edit_layer->rot      = rot;
            _edit_layer->flip     = flip;
            _edit_layer->type     = type;
        }
    }

    safe_fclose(f);
    return;

    error:
    safe_fclose(f);
    char *errors = "Something went wrong";
    switch (error_code)
    {
        case 1:  errors = "Could not get frame count";    break;
        case 2:  errors = "Could not get layer count";    break;
        case 3:  errors = "Could not get frame offsets";  break;
        case 4:  errors = "Could not get texture name";   break;
        case 5:  errors = "Could not get layer color";    break;
        case 6:  errors = "Could not get texture clip";   break;
        case 7:  errors = "Could not get layer offsets";  break;
        case 8:  errors = "Could not get layer rotation"; break;
        case 9:  errors = "Could not get layer flip";     break;
        case 10: errors = "Could not get layer type";     break;
        case 11: errors = "Could not load texture";       break;
    }

    snprintf(_message, 512, "Failed to load file:\nError %d, '%s'.\n"
                            "Check that the file is in correct format.",
                            error_code, errors);
    _message_box = 1;
}

static void
_load_animation_mae()
{
    FILE *f;
    f = fopen(_dropped_file, "r");

    if (!f)
    {
        snprintf(_message, 512, "Failed to open dropped Animation\n"
                                "'%s'.\n", _dropped_file);
        _message_box = 1;
        return;
    }

    int orig_anim = _dir;
    char name[NAME_LIMIT];
    char line[512];

    fgets(line, 512, f);
    sscanf(line, "%s", name);

    int error_code = 0;

    for (int a = 0; a < 8; ++a)
    {
        _anim_change_to(a);
        _anim_clear();

        int fc = 0;
        fgets(line, 512, f);
        fgets(line, 512, f);
        fgets(line, 512, f);
        sscanf(line, "Frames %d", &fc);
        if (fc <= 0) { error_code = 1; goto error; }

        for (int i = 0; i < fc; ++i)
        {
            fgets(line, 512, f);
            fgets(line, 512, f);
            fgets(line, 512, f);
            int lc = 0;
            if (sscanf(line, "Layers %d", &lc) != 1)
            { error_code = 2; goto error; }

            int fox = 0, foy = 0;
            fgets(line, 512, f);
            if (sscanf(line, "Offsets %d %d", &fox, &foy) != 2)
            { error_code = 3; goto error; }

            if (i > 0)
                _frame_create(0);

            _edit_frame->ox  = (float)fox;
            _edit_frame->oy  = (float)foy;

            for (int j = 0; j < lc; ++j)
            {
                fgets(line, 512, f);
                uint32 tex_id;
                fgets(line, 512, f);
                if (sscanf(line, "Texture %u", &tex_id) != 1)
                { error_code = 4; goto error; }

                DEBUG_PRINTFF("TEXID: %u\n", tex_id);

                int color[4];
                fgets(line, 512, f);
                if (sscanf(line, "Color %d %d %d %d",
                    &color[0], &color[1], &color[2], &color[3]) != 4)
                { error_code = 5; goto error; }

                int clip[4];
                fgets(line, 512, f);
                if (sscanf(line, "Clip %d %d %d %d",
                    &clip[0], &clip[1], &clip[2], &clip[3]) != 4)
                { error_code = 6; goto error; }

                int lox = 0, loy = 0;
                fgets(line, 512, f);
                if (sscanf(line, "Offsets %d %d", &lox, &loy) != 2)
                { error_code = 7; goto error; }

                float rot = 0;
                fgets(line, 512, f);
                if (sscanf(line, "Rotation %f", &rot) != 1)
                { error_code = 8; goto error; }

                int flip = 0;
                fgets(line, 512, f);
                if (sscanf(line, "Flip %d", &flip) != 1)
                { error_code = 9; goto error; }

                int type = NUM_AE_LAYER_TYPES;
                fgets(line, 512, f);
                if (sscanf(line, "Type %d", &type) != 1)
                { error_code = 10; goto error; }

                if (j > 0)
                    _layer_create();

                as_tex_t *ta = as_claim_tex(tex_id, 1);
                if (ta == 0)
                { error_code = 11; goto error; }

                _edit_layer->ta       = ta;
                _edit_layer->color[0] = (uint8)color[0];
                _edit_layer->color[1] = (uint8)color[1];
                _edit_layer->color[2] = (uint8)color[2];
                _edit_layer->color[3] = (uint8)color[3];
                _edit_layer->clip[0]  = (float)clip[0];
                _edit_layer->clip[1]  = (float)clip[1];
                _edit_layer->clip[2]  = (float)clip[2];
                _edit_layer->clip[3]  = (float)clip[3];
                _edit_layer->ox       = (float)lox;
                _edit_layer->oy       = (float)loy;
                _edit_layer->rot      = rot;
                _edit_layer->flip     = flip;
                _edit_layer->type     = type;
            }
        }
    }

    safe_fclose(f);
    _anim_change_to(orig_anim);
    return;

    error:
    safe_fclose(f);
    char *errors = "Something went wrong";
    switch (error_code)
    {
        case 1:  errors = "Could not get frame count";    break;
        case 2:  errors = "Could not get layer count";    break;
        case 3:  errors = "Could not get frame offsets";  break;
        case 4:  errors = "Could not get texture name";   break;
        case 5:  errors = "Could not get layer color";    break;
        case 6:  errors = "Could not get texture clip";   break;
        case 7:  errors = "Could not get layer offsets";  break;
        case 8:  errors = "Could not get layer rotation"; break;
        case 9:  errors = "Could not get layer flip";     break;
        case 10: errors = "Could not get layer type";     break;
        case 11: errors = "Could not load texture";       break;
    }

    snprintf(_message, 512, "Failed to load file:\nError %d, '%s'.\n"
                            "Check that the file is in correct format.",
                            error_code, errors);
    _message_box = 1;
}

static void
_back_to_menu()
{
    core_set_screen(&main_menu_screen);
}

static void
_fix_path(char *path)
{
    if (path == 0)
    {
        DEBUG_PRINTF("Cannot fix given path because it's 0\n");
        return;
    }

    char *base_path = SDL_GetBasePath();
    while (path == strstr(path, base_path))
        memmove(path, path + strlen(base_path),
        1 + strlen(path + strlen(base_path)));

    int len = (int)strlen(path);
    for (int i = 0; i < len; ++i)
    {
        if (path[i] == '\\')
            path[i] = '/';
    }
}

static void
_gui_confirm_dialog(char *question, void (*yes)())
{
    gui_origin(GUI_TOP_LEFT);

    int x = core_asset_config.resolution_w / 2 - 256;
    int y = core_asset_config.resolution_h / 2 - 256;
    int w = 512;
    int h = 256;

    gui_begin_win("  ", x, y, w, h, 0);
    gui_text_s(question, w - 16, 8, 8, 1.5f);

    if (gui_button("Yes", 64, h - 64, 96, 36, 0))
    {
        _help_box = 0;
        yes();
        _confirm = 0;
    }
    if (gui_button("No", 512 - 128 - 64, h - 64, 96, 36, 0))
    {
        _help_box = 0;
        _confirm = 0;
    }

    gui_end_win();

    gui_origin(GUI_BOTTOM_LEFT);
}

static void
_gui_message_box()
{
    gui_origin(GUI_TOP_LEFT);

    int x = core_asset_config.resolution_w / 2 - 256;
    int y = core_asset_config.resolution_h / 2 - 256;
    int w = 512;
    int h = 256;

    gui_begin_win("        ", x, y, w, h, 0);

    gui_text(_message, w - 16, 8, 8);
    if (gui_button("OK", w / 2 - 48, h - 64, 96, 36, 0))
        _message_box = 0;

    gui_end_win();
}

static void
_gui_help_box()
{
    int x = core_asset_config.resolution_w / 2 - 256;
    int y = core_asset_config.resolution_h / 2 - 512;
    int w = 512;
    int h = 1024;

    gui_begin_win("HELP", x, y, w, h, 0);

    char *help = "HELP";

    if (_help_box == 1)
    {
        help = "In the top left corner, you see information about;\n"
               "Frame: Currently edited frame / frame count.\n"
               "Offsets: Edited frame's offsets, affects its layers.\n"
               "Layer: Currently edited layer / layer count.\n"
               "Offsets: Edited layer's offsets.\n"
               "Rotation: Layer's rotation, currently not visualized.\n"
               "Flip: Layer's flip, can be horizontal, vertical or both.\n"
               "Type: Layer's type. To set type, click the button in top\n"
               "right corner to show/hide the type selection panel.\n"
               "Clip: Part of the texture the edited layer is using\n\n"
               "Prev/Next Frame changes edited frame.\n"
               "New Frame creates new frame with default values.\n"
               "Delete Frame deletes current frame.\n"
               "Duplicate Frame creates copy of current frame.\n"
               "Clear Frame sets layers values to defaults.\n"
               "Move/Send F buttons change the frame's order.\n\n"
               "Frame Offset buttons change the frame's offset.\n"
               "Offset All buttons offset ALL animation's frames.\n"
               "Layer buttons work similarly to frame buttons.\n\n"
               "Clip buttons are used to edit clip.\n"
               "Width/Height buttons are self-explanatory.\n"
               "Move buttons change the clip's starting position.\n"
               "Select Clip button allows you to select clip with\n"
               "Mouse similarly to selection tool in Paint.\n\n"
               "Prev/Next Anim buttons change the edited animation.\n"
               "Next to buttons is current direction of edited animation.\n\n"
               "Zoom buttons, wait for it, change the zoom value.\n\n"
               "Color buttons allow you to change colors of\n"
               "the edited layer, background color and layer's border.\n\n"
               "Toggle View button changes current view.\n"
               "Toggle Tile sets tile's visibility.\n\n";
    }
    else
    {
        help = "To change texture, just drag n drop it on the editor.\n"
               "The texture MUST be inside game's directory, for example\n"
               "in assets/textures/example.png\n"
               "If the texture doesn't work, check if it is included in\n"
               "assets/assets.dat file.\n\n"
               "IMPORTANT: Go in the texture folder from rundir's folder,\n"
               "otherwise texture paths might use full path from\n"
               "computer's root. In that case you'd have to manually\n"
               "open the .maa or .mae file and correct saved paths.\n"
               "Hint: If you use similar sprite sheets for each direction,\n"
               "just make one direction, save the animation as .mae, \n"
               "open it in text editor and copy the values from the\n"
               "animation to all others and just change the file path to\n"
               "correct texture.\n\n"
               "Animations are saved in assets/animations folder.\n"
               "If you want to edit or continue working on existing\n"
               "animation, just drag n drop it on the editor.\n\n"
               "When you are done, enter a file name and press Save.\n\n"
               "Hotkeys:\n"
               "Arrow Keys - move view area (LShift to move the anim).\n"
               "WASD - change currently edited frame's offset.\n"
               "          Hold Shift for boost.\n"
               "You can drag edited layer with mouse.\n\n"
               "E - change to next frame\n"
               "Q - change to previous frame\n\n"
               "Ctrl + D - create duplicate frame\n"
               "Ctrl + S - create duplicate layer\n"
               "Ctrl + Delete - delete's current frame\n"
               "Delete - delete's current layer\n\n"
               "Numpad plus/minus - zoom adjust\n"
               "Numbers 1-8 - jump to given Animation\n";
    }

    gui_text_s(help, w - 16, 8, 8, 1.f);
    if (gui_button("OK", w / 2 - 144, h - 64, 96, 36, 0))
        _help_box = 0;
    if (gui_button(_help_box == 1 ? "Next Page" : "Prev Page",
        w / 2 + 48, h - 64, 96, 36, 0))
        _help_box = _help_box == 1 ? 2 : 1;

    gui_end_win();
}

static void
_render_edited_frame()
{
    float zoom_oxy = _zoom_scale != 1 ? (_zoom_scale - 1) * -(core_asset_config.tile_width / 2) : 0;
    float zox      = _anim_ox + zoom_oxy;
    float zoy      = _anim_oy + zoom_oxy;

    if (_view_tile)
    {
        tile_graphic_def_t *gd = tile_get_graphic_def(
            core_asset_config.tile_selector_ids[0]);
        gui_texture_s(&_tile_asset->tex, gd->clip,
            (int)(zox + (_tile_ox + gd->ox) * _zoom_scale),
            (int)(zoy + (_tile_oy + gd->oy) * _zoom_scale),
            _zoom_scale * _tile_sx, _zoom_scale * _tile_sy);
    }

    for (int i = 0; i < _edit_frame->num_layers; ++i)
    {
        ae_layer_t *layer = &_edit_frame->layers[i];
        if (layer)
        {
            gui_texture_scfr(&layer->ta->tex, layer->clip,
                (int)(zox + (layer->ox + _edit_frame->ox) * _zoom_scale),
                (int)(zoy + (layer->oy + _edit_frame->oy) * _zoom_scale),
                _zoom_scale, _zoom_scale, layer->color, layer->flip,
                layer->rot);
        }
    }

    _render_clip_bounds(
        (int)(zox + (_edit_layer->ox + _edit_frame->ox) * _zoom_scale),
        (int)(zoy + (_edit_layer->oy + _edit_frame->oy) * _zoom_scale),
        _zoom_scale);
}

static void
_render_edited_animation()
{
    ae_frame_t *frame = &_cur_anim->frames[_index];
    if (frame == 0)
        return;

    float zoom_oxy = _zoom_scale != 1 ? (_zoom_scale - 1) * -(core_asset_config.tile_width / 2) : 0;
    float zox      = _anim_ox + zoom_oxy;
    float zoy      = _anim_oy + zoom_oxy;
    if (_view_toggle == 2)
    {
        zox += _eanim_ox * _zoom_scale;
        zoy += _eanim_oy * _zoom_scale;
    }

    if (_view_tile)
    {
        tile_graphic_def_t *gd = tile_get_graphic_def(
            core_asset_config.tile_selector_ids[0]);
        gui_texture_s(&_tile_asset->tex, gd->clip,
            (int)(zox + (_tile_ox + gd->ox) * _zoom_scale),
            (int)(zoy + (_tile_oy + gd->oy) * _zoom_scale),
            _zoom_scale * _tile_sx, _zoom_scale * _tile_sy);
    }

    for (int i = 0; i < frame->num_layers; ++i)
    {
        ae_layer_t *layer = &frame->layers[i];
        if (layer)
        {
            gui_texture_scfr(&layer->ta->tex, layer->clip, 
                (int)(zox + (layer->ox + frame->ox) * _zoom_scale),
                (int)(zoy + (layer->oy + frame->oy) * _zoom_scale),
                _zoom_scale, _zoom_scale, layer->color, layer->flip,
                layer->rot);
        }
    }
}

static void
_render_clip_bounds(int ox, int oy, float zs)
{
    int xtop = ox;
    int ytop = oy;
    int xend = (int)(_edit_layer->clip[2] - _edit_layer->clip[0]) + ox;
    int yend = (int)(_edit_layer->clip[3] - _edit_layer->clip[1]) + oy;
    int w    = (int)((xend - xtop) * zs);
    int h    = (int)((yend - ytop) * zs);

    gui_color(_clip_bcol[0], _clip_bcol[1], _clip_bcol[2], _clip_bcol[3]);
    gui_rectangle(xtop    , ytop    , w, 1);
    gui_rectangle(xtop    , ytop + h, w, 1);
    gui_rectangle(xtop    , ytop    , 1, h);
    gui_rectangle(xtop + w, ytop    , 1, h);
    gui_color(255, 255, 255, 255);
}

static void
_render_full_texture()
{
    float clip[4] = { 0, 0, _edit_layer->ta->tex.w, _edit_layer->ta->tex.h };
    gui_texture(&_edit_layer->ta->tex, clip, (int)_anim_ox, (int)_anim_oy);
    _render_clip_bounds((int)(_anim_ox + _edit_layer->clip[0]),
                        (int)(_anim_oy + _edit_layer->clip[1]), 1.0f);
}

static void
_render_info(int ox, int *oy)
{
    char f_info[32];
    char f_info_oxy[32];
    snprintf(f_info, 32, "Frame:  %d/%d", _frame_index + 1,
        _edit_anim->num_frames);
    snprintf(f_info_oxy, 32, "Offsets: X%d, Y%d",
        (int)round(_edit_frame->ox), (int)round(_edit_frame->oy));

    char l_info[32];
    char l_info_oxy[32];
    char l_info_deg[32];
    char l_info_type[32];
    char *l_info_flip = 0;

    switch (_edit_layer->flip)
    {
        default:
        case 0: l_info_flip = "Not Flipped";       break;
        case 1: l_info_flip = "Horizontal Flip";   break;
        case 2: l_info_flip = "Vertical Flip";     break;
        case 3: l_info_flip = "Vert & Horiz Flip"; break;
    }

    snprintf(l_info, 32, "Layer:   %d/%d", _layer_index + 1,
        _edit_frame->num_layers);
    snprintf(l_info_oxy, 32, "Offsets: X%d, Y%d",
        (int)round(_edit_layer->ox), (int)round(_edit_layer->oy));
    snprintf(l_info_deg, 32, "Rotation: %d degrees",
        (int)round(_edit_layer->rot * 180.0f / (float)PI));
    if (_edit_layer->type <= NUM_AE_LAYER_TYPES)
        snprintf(l_info_type, 32, "Type: %s",
            _get_layer_type_string(_edit_layer->type));
    else
        snprintf(l_info_type, 32, "Type: Custom (%d)", _edit_layer->type);

    char c_info[32];
    char c_info_wh[32];
    snprintf(c_info, 32, "Clip: X%d, Y%d",
        (int)_edit_layer->clip[0], (int)_edit_layer->clip[1]);
    snprintf(c_info_wh, 32, "Width: %d, Height: %d",
        (int)(_edit_layer->clip[2] - _edit_layer->clip[0]),
        (int)(_edit_layer->clip[3] - _edit_layer->clip[1]));

    /* Left texts */
    int top_oy = *oy;
    ox += 2;
    gui_text(f_info,      0, ox, *oy);
    *oy = gui_get_last_text_y() + gui_get_last_text_h() + 2;
    gui_text(f_info_oxy,  0, ox, *oy);
    *oy = gui_get_last_text_y() + gui_get_last_text_h();
    gui_text(c_info,      0, ox, *oy);
    *oy = gui_get_last_text_y() + gui_get_last_text_h();
    gui_text(c_info_wh,   0, ox, *oy);
    /* Right texts */
    gui_origin(GUI_TOP_RIGHT);
    *oy = top_oy;
    ox  = core_asset_config.resolution_w - _sidebar_width;
    gui_text(l_info,      0, ox, *oy);
    *oy = gui_get_last_text_y() + gui_get_last_text_h() + 2;
    gui_text(l_info_oxy,  0, ox, *oy);
    *oy = gui_get_last_text_y() + gui_get_last_text_h() + 2;
    gui_text(l_info_deg,  0, ox, *oy);
    *oy = gui_get_last_text_y() + gui_get_last_text_h() + 2;
    gui_text(l_info_flip, 0, ox, *oy);
    *oy = gui_get_last_text_y() + gui_get_last_text_h() + 2;
    gui_text(l_info_type, 0, ox, *oy);

    *oy = gui_get_last_text_y() + 2 * gui_get_last_text_h();
}

static void
_render_dir_arrow(int x, int y)
{
    if (!_dir_arrow)
        return;

    float clip[4];
    clip[1] = 0;
    clip[3] = 32;
    int flip = 0;

    switch (_dir)
    {
        default:
        case 0: clip[0] = 103; clip[2] = 120; flip = 1; break;
        case 1: clip[0] = 128; clip[2] = 150; flip = 1; break;
        case 2: clip[0] = 71;  clip[2] = 89;  flip = 1; break;
        case 3: clip[0] = 38;  clip[2] = 56;            break;
        case 4: clip[0] = 71;  clip[2] = 89;            break;
        case 5: clip[0] = 128; clip[2] = 150;           break;
        case 6: clip[0] = 103; clip[2] = 120;           break;
        case 7: clip[0] = 7;   clip[2] = 25;            break;
    }

    gui_texture_f(&_dir_arrow->tex, clip, x, y - (int)(clip[3] - clip[1]) / 2,
        flip);
}

static char *
_get_layer_type_string(int layer)
{
    switch (layer)
    {
    case AE_LAYER_BODY:         return "Body";
    case AE_LAYER_HEAD:         return "Head";
    case AE_LAYER_LHAND:        return "Left hand";
    case AE_LAYER_RHAND:        return "Right hand";
    case AE_LAYER_LEGS:         return "Legs";
    case AE_LAYER_FEET:         return "Feet";
    case AE_LAYER_BODY_ARMOUR:  return "Body Armour";
    case AE_LAYER_HEAD_ARMOUR:  return "Helmet";
    case AE_LAYER_LHAND_ARMOUR: return "Left hand Armour";
    case AE_LAYER_RHAND_ARMOUR: return "Right hand Armour";
    case AE_LAYER_LEGS_ARMOUR:  return "Leg Armour";
    case AE_LAYER_FEET_ARMOUR:  return "Foot Armour";
    case AE_LAYER_LHAND_ITEM:   return "Left hand item";
    case AE_LAYER_RHAND_ITEM:   return "Right hand item";
    }
    return "Other";
}

static void
_draw_texture_selection(void)
{
    gui_origin(GUI_BOTTOM_RIGHT);
    int win_w       = core_asset_config.resolution_w / 4;
    int win_h       = core_asset_config.resolution_h / 2;
    gui_begin_win("Textures", 2, 2, win_w, win_h, 0);
    gui_origin(GUI_TOP_LEFT);
    uint32  num_textures    = as_get_num_tex_db_entries();
    int     button_x        = 2;
    int     y               = 2;
    int     button_w        = win_w / 8;
    int     button_h        = win_w / 20;
    int     text_x          = button_x + button_w + 2;
    gui_begin_empty_win("textures_internal", 0, 0,
        gui_get_current_win_viewport_w(),
        gui_get_current_win_viewport_h() - button_h, GUI_WIN_SCROLLABLE);
    for (uint32 i = 0; i < num_textures; ++i)
    {
        as_tex_db_entry_t entry;
        as_get_tex_db_entry(i, &entry);
        gui_textf("%u: %s", 0, text_x, y, entry.id, entry.name);
        if (gui_button(gui_format_id("Select##tex%u", i), button_x, y, button_w,
            button_h, 0))
        {
            if (_edit_layer->ta)
                as_unclaim_tex(_edit_layer->ta);
            _edit_layer->ta = as_claim_tex(entry.id, 0);
        }
        y = gui_get_last_button_y() + gui_get_last_button_h() + 2;
    }
    gui_end_win();
    gui_origin(GUI_BOTTOM_RIGHT);
    if (gui_button("Close", 2, 2, button_w, button_h, 0))
        _show_texture_selection = 0;
    gui_end_win();
}
