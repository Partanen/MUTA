#ifndef MUTA_CLIENT_GAME_STATE_INTERNAL_H
#define MUTA_CLIENT_GAME_STATE_INTERNAL_H

#include "../../shared/common_defs.h"
#include "entity.h"
#include "world.h"
#include "render.h"
#include "sky.h"
#include "inventory.h"
#include "action_button.h"

#define GS_MAX_CLICKED_ENTITIES     32
#define MAX_CHARGE_BAR_TITLE_LEN    32

/* Forward declaration(s) */
typedef struct item_t item_t;

/* Types defined here */
typedef struct game_state_t game_state_t;

enum gs_special_keys
{
    GS_SPECIAL_KEY_ENTER = 0,
    NUM_GS_SPECIAL_KEYS
};

enum gs_session_state
{
    GS_SESSION_STATE_NOT_IN_GAME = 0,
    GS_SESSION_STATE_LOGGING_IN,
    GS_SESSION_STATE_IN_GAME
};

enum gs_drag_type
{
    GS_DRAG_TYPE_OBJECT_IN_INVENTORY,   /* Moving item inside inventory */
    GS_DRAG_TYPE_OBJECT_IN_WORLD        /* Picking up object from world. */
};

enum gs_move_request_type
{
    GS_MOVE_REQUEST_TYPE_NONE = 0,
    GS_MOVE_REQUEST_TYPE_WITHIN_INVENTORY,
    GS_MOVE_REQUEST_TYPE_DROP
};

struct game_state_t
{
    struct
    {
        entity_t    *all[GS_MAX_CLICKED_ENTITIES];
        uint32      num;
        int         click_x;
        int         click_y;
    } clicked_entities;
    struct
    {
        entity_handle_t handle;
        bool32          is_null;
        bool32          have;
        bool32          target_despawned;
    } next_target;
    struct
    {
        bool32 clear;
    } next_clicked_entities;
    enum gs_session_state   session_state;
    bool32                  should_end_session;
    bool32                  dragging_to_move;
    uint8                   special_keys[NUM_GS_SPECIAL_KEYS];
    uint32                  view_distance_h;
    uint32                  view_distance_v;
    /* The view distance, set by the server, determines at what range we receive
     * updates of objects. If an object ends up further away from the player than
     * the current view distance, it should be safe to remove it - if the object
     * comes into visible range again, the server will resend it's information. */
    entity_t                **delayed_despawns;
    world_t                 world;
    entity_t                *player_entity;
    entity_t                *target_entity;
    render_world_t          render_world;
    render_camera_t         render_camera;
    int                     hovered_tile[3];
    bool32                  draw_tile_selector;
    sky_t                   sky;
    inventory_t             inventory;
    entity_t                **visible_server_entities;
    /* A darr of entities (creatures, players, dynamic objects) sent to us by the
     * server that are in the current view distance range. When the player moves too
     * far from these, the entities are despawned on the client. */
    action_button_t         action_buttons[MAX_ACTION_BUTTONS];
    struct
    {
        ability_id_t    data[MAX_PLAYER_ABILITIES];
        uint32          num;
    } abilities;
    struct
    {
        bool32              have;
        dobj_runtime_id_t   runtime_id;
        enum gs_drag_type   drag_type;
        int                 w_in_inventory;
        int                 h_in_inventory;
    } dragged_item;
    struct
    {
        enum gs_move_request_type   type;
        dobj_runtime_id_t           runtime_id;
    } move_requested_item; /* Sent a request to server to move inventory item */
    dchar *action_buttons_path;
    dchar *hotkeys_path;
    struct
    {
        bool32          active;
        uint32_t        current_ms;
        uint32_t        max_ms;
        char            title[MAX_CHARGE_BAR_TITLE_LEN + 1];
        ability_id_t    ability_id;
    } charge_bar;
};

void
gsi_set_up_paths_and_create_directories(game_state_t *gs,
    const char *character_name, const char *shard_name);

void
gsi_target_clicked_entity(game_state_t *gs, uint32 index);

void
gsi_pick_up_dragged_item(game_state_t *gs, enum equipment_slot equipment_slot,
    int x, int y);

void
gsi_clear_dragged_item(game_state_t *gs);

void
gsi_drag_item_in_inventory(game_state_t *gs, item_t *item);

void
gsi_request_move_dragged_item_in_inventory(game_state_t *gs,
    enum equipment_slot new_equipment_slot, uint8 new_x, uint8 new_y);

void
gsi_write_local_error_message(const char *msg);
/* Write an error message in the chat log. */

void
gsi_use_ability(game_state_t *gs, ability_id_t ability_id);

#endif /* MUTA_CLIENT_GAME_STATE_INTERNAL_H */
