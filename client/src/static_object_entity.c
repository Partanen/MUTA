#include "static_object_entity.h"
#include "world.h"
#include "log.h"

entity_t *
static_object_spawn(world_t *world, creature_type_id_t type, int direction,
    int position[3])
{
    LOG_DEBUG("Spawning static object %u, direction %d, position [%d, %d, %d].",
        type, direction, position[0], position[1], position[2]);
    chunk_cache_t *cache = world_get_chunk_cache_of_tile(world, position[0],
        position[1]);
    if (!cache)
        return 0;
    entity_t *entity = entity_spawn(world, direction, position);
    if (!entity)
        return 0;
    entity_type_data_t *type_data = entity_get_type_data(entity);
    type_data->type = ENTITY_TYPE_STATIC_OBJECT;
    type_data->static_object.type_id = type;
    type_data->static_object.index_in_cache = darr_num(cache->static_objects);
    darr_push(cache->static_objects, entity);
    static_object_def_t *def = ent_get_static_object_def(type);
    if (def)
        switch (def->graphic_type)
        {
        case ENT_GRAPHIC_ISO_SPRITE:
        {
            component_handle_t render_component = entity_attach_component(entity,
                &render_component_definition);
            if (render_component == INVALID_COMPONENT_HANDLE)
                goto fail;
            component_handle_t component = entity_attach_component(entity,
                &iso_sprite_component_definition);
            if (component == INVALID_COMPONENT_HANDLE)
                goto fail;
            iso_sprite_component_set_asset(component,
                def->graphic_data.iso_sprite);
        }
            break;
        }
    return entity;
    fail:
        entity_despawn(entity);
        return 0;
}
