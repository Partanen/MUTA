#include "gl.h"
#include "core.h"
#include "render.h"
#include "../../shared/gui/gui.h"
#include "bitmap.h"
#include "assets.h"
#include "world.h"
#include "log.h"
#include "../../shared/glad.h"

#define FLOATS_PER_TILE     24
#define MAX_TILE_SPRITES    (4 * 0xFFFF)
#define NUM_EXTRA_TILES     3 /* Used when an area is rendered. */
#define GUI_GLYPH(font_, index_) \
    (&(font_)->glyphs[(index_) % (font_)->num_glyphs])

/* The following 2 macros require the following variables to be in the same
 * scope: tile_w, tile_h, tile_top_h. */
#define COMPUTE_PIXEL_X(tx_, ty_, offset_) \
    ((tx_) * tile_w / 2 - (ty_) * tile_w / 2 + (offset_))
#define COMPUTE_PIXEL_Y(tx_, ty_, tz_, offset_) \
    ((tx_) * tile_w / 4 + (ty_) * tile_top_h / 2 - (tz_) * tile_h / \
        2 + (offset_))

struct render_world_cmd_t
{
    tex_t       texture;
    float       clip[4];
    float       x, y, z;
    entity_t    *entity;
};

struct render_ghost_tile_t
{
    int32   x;
    int32   y;
    uint8   z;
    tile_t  tile;
    uint16  tag;
};

static GLuint   _gui_shader;
static tex_t    _white_dummy_tex;
static GLuint   _gui_vbo;
static GLuint   _gui_vbo_size;
static GLuint   _gui_ebo;
static GLuint   _tile_shader;
static GLuint   _tile_shader_offset_index;
static GLuint   _tile_shader_projection_index;
static tex_t    *_tile_sheet_tex;
static uint8    _tile_depth_color_table[MAP_CHUNK_T][4];
/* Table for color multipliers of tile layers on the map, to create the
 * perception of depth on the Z layer. */

/* Extern */

static inline void
_bind_gui_vao(void);

static inline void
_bind_tile_vao(void);

static inline void
_write_tile_verts(void *memory, tex_t *tex, float *clip, float x, float y,
    float z, uint8 color[4]);

static inline int
_compare_render_world_cmds(const void *a, const void *b);

static inline void
_compute_row_base_and_diff(float tx, float ty, float end0plus1,
    float *restrict ret_base, float *restrict ret_diff);

static inline float
_compute_tile_pixel_z(float tx, float ty, float tz, float end_z, float row_base,
    float row_diff);

static inline float
_compute_object_pixel_z(float tx, float ty, float tz, int end[3]);

static void
_pixel_to_isometric_raw(render_world_t *render_world, float pixel_x,
    float pixel_y, int *restrict ret_x, int *restrict ret_y);

int
r_init(void)
{
    int         ret = 0;
    char        err_log[512];
    const int   err_log_len = 512;
    if (sb_init(core_config.sprite_batch_size, err_log, err_log_len))
    {
        core_error_window("Failed to init sprite batch. Error log: %s",
            err_log);
        puts(err_log);
        ret = 1;
        goto fail;
    }
    /*-- Init the gui rendering stuff --*/
    const char *vtx_code =
        "#version 100\n"
        "precision lowp float;"
        "attribute vec2 position;"
        "attribute vec2 in_uv;"
        "attribute vec4 in_color;"
        "varying vec2 uv;"
        "varying vec4 color;"
        "uniform mat4 projection;"
        "void main(void)"
        "{"
            "gl_Position    = projection * vec4(position, 1.0, 1.0);"
            "uv             = in_uv;"
            "color          = in_color;"
        "}";
    const char *frg_code =
        "#version 100\n"
        "precision lowp float;"
        "varying vec4 color;"
        "varying vec2 uv;"
        "uniform sampler2D tex;"
        "void main(void)\n"
        "{"
            "gl_FragColor = texture2D(tex, uv) * color;"
        "}";
    _gui_shader = gl_shader_from_strs(vtx_code, frg_code, err_log, err_log_len);
    if (!_gui_shader)
    {
        core_error_window("Failed to init GUI shader. Error log: %s", err_log);
        ret = 2;
        goto fail;
    }
    /*-- Tile shader --*/
    vtx_code =
        "#version 130\n"
        "precision lowp float;"
        "attribute vec3 position;"
        "attribute vec2 in_uv;"
        "attribute vec4 in_color;"
        "varying vec2 uv;"
        "varying vec4 color;"
        "uniform mat4 projection;"
        "uniform vec2 offset;"
        "void main(void)"
        "{"
            "vec4 final_position = vec4(position, 1.0);"
            "final_position.x += offset.x;"
            "final_position.y += offset.y;"
            "final_position.x = final_position.x;"
            "final_position.y = final_position.y;"
            "gl_Position    = projection * final_position;"
            "uv             = in_uv;"
            "color          = in_color;"
        "}";
    frg_code =
        "#version 130\n"
        "precision lowp float;"
        "varying vec2 uv;"
        "varying vec4 color;"
        "uniform sampler2D tex;"
        "void main(void)\n"
        "{"
            "vec4 frag_color = texture2D(tex, uv) * color;"
            "if (frag_color.w == 0.0)"
                "discard;"
            "gl_FragColor = frag_color;"
        "}";
    _tile_shader = gl_shader_from_strs(vtx_code, frg_code, err_log, err_log_len);
    if (!_tile_shader)
    {
        core_error_window("Failed to init GUI shader. Error log: %s", err_log);
        ret = 3;
        goto fail;
    }
    _tile_shader_offset_index = glGetUniformLocation(_tile_shader, "offset");
    if (_tile_shader_offset_index == -1)
        {ret = 4; goto fail;}
    _tile_shader_projection_index = glGetUniformLocation(_tile_shader,
        "projection");
    if (_tile_shader_projection_index == -1)
        {ret = 5; goto fail;}
    /* Create the while dummy texture */
    uint8 white_tex_px[16];
    for (int i = 0; i < 16; ++i)
        white_tex_px[i] = 255;
    if (tex_from_mem(&_white_dummy_tex, white_tex_px, 2, 2, IMG_RGBA))
        {ret = 5; goto fail;}
    /* Create the GUI rendering buffers */
    const uint num_quads = 16384; /* TODO: fix the hardcoded value */
    DEBUG_PRINTFF("gui num quads is hardcoded FIXME\n");
    const uint num_verts = num_quads * 6;
    _gui_vbo_size = num_verts * GUI_VERT_SIZE;
    glGenBuffers(1, &_gui_vbo);
    glGenBuffers(1, &_gui_ebo);
    /*-- VBO --*/
    gl_bind_vbo(_gui_vbo);
    glBufferData(GL_ARRAY_BUFFER, _gui_vbo_size, 0, GL_DYNAMIC_DRAW);
    _bind_gui_vao();
    /*-- EBO --*/
    gl_bind_ebo(_gui_ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, num_verts / 4 * 6 * sizeof(GLushort),
        0, GL_STREAM_DRAW);
    GLushort *ebo_data = (GLushort*)glMapBuffer(GL_ELEMENT_ARRAY_BUFFER,
        GL_WRITE_ONLY);
    GLushort indices[6] = {0, 1, 2, 2, 3, 1};
    for (GLushort i = 0; i < num_quads; ++i)
        for (GLushort j = 0; j < 6; ++j)
            ebo_data[i * 6 + j] = indices[j] + i * 4;
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
    gl_bind_ebo(0);
    gl_bind_vbo(0);
    gl_set_vsync(1);
    as_tex_t *ta = as_claim_tex(core_asset_config.tileset_tex_id, 1);
    if (!ta)
    {
        core_error_window("Cannot intialize renderer: tilesheet not found!");
        return 6;
    }
    _tile_sheet_tex = &ta->tex;
    /* Fill tile depth color table. */
    uint8 color_mul_min     = 140;
    uint8 color_mul_max     = 255;
    uint8 color_mul_diff    = color_mul_max - color_mul_min;
    for (int i = 0; i < MAP_CHUNK_T; ++i)
    {
        uint8 addition = (uint8)((float)i / (float)MAP_CHUNK_T * (float)color_mul_diff);
        if (addition > color_mul_diff)
            addition = color_mul_diff;
        for (int j = 0; j < 3; ++j)
            _tile_depth_color_table[i][j] = color_mul_min + addition;
        _tile_depth_color_table[i][3] = 255;
    }
    return ret;
    fail:
        core_error_window("Failed to initialize renderer, error %d.", ret);
        return ret;
}

int
r_destroy(void)
{
    sb_destroy();
    tex_free(&_white_dummy_tex);
    glDeleteBuffers(1, &_gui_vbo);
    glDeleteBuffers(1, &_gui_ebo);
    return 0;
}

void
r_render_gui(void)
{
    int num_lists;
    gui_draw_list_t *lists = gui_get_draw_lists(&num_lists);
    gui_draw_cmd_t  *cmd;
    gui_draw_list_t *list;
    gl_disable_depth_test();
    int prev_vp[4], prev_scis[4];
    gl_get_viewport(&prev_vp[0], &prev_vp[1], &prev_vp[2], &prev_vp[3]);
    gl_get_scissor(&prev_scis[0], &prev_scis[1], &prev_scis[2], &prev_scis[3]);
    int tar_vp[4];
    float sc = core_compute_target_viewport(tar_vp);
    gl_viewport_and_scissor(tar_vp[0], tar_vp[1], tar_vp[2], tar_vp[3]);
    gl_use_program(_gui_shader);
    GLfloat cs[4] = {0.f, 0.f, (float)core_asset_config.resolution_w,
        (float)core_asset_config.resolution_h};
    GLfloat projection[4][4];
    gl_orthographic_projection_from_coordinate_space(cs, projection);
    glUniformMatrix4fv(glGetUniformLocation(_gui_shader, "projection"),
        1, GL_FALSE, &projection[0][0]);
    gl_bind_vbo(_gui_vbo);
    gl_bind_ebo(_gui_ebo);
    _bind_gui_vao();
    gl_active_texture(0);
    gl_bind_tex2d(_white_dummy_tex.id);
    tex_t *next_tex;
    int     i, j, k, num_cmds;
    size_t  offset;
    GLint   num_elements;
    int     last_raw_scissor[4] = {0};
    sc = MAX(sc, 0.00000001f);
    for (i = 0; i < num_lists; ++i)
    {
        /* TODO: resize buf if necessary */
        list        = &lists[i];
        offset      = 0;
        num_cmds    = list->num_cmds;
        glBufferSubData(GL_ARRAY_BUFFER, 0,
            list->num_verts * GUI_VERT_SIZE, list->verts);
        for (j = 0; j < num_cmds; ++j)
        {
            cmd = &list->cmds[j];
            for (k = 0; k < 4; ++k)
            {
                if (cmd->scissor[k] == last_raw_scissor[k])
                    continue;
                memcpy(last_raw_scissor, cmd->scissor, 4 * sizeof(int));
                gl_scissor(
                    (int)((float)last_raw_scissor[0] * sc) + tar_vp[0],
                    (int)((float)last_raw_scissor[1] * sc) + tar_vp[1],
                    (int)((float)last_raw_scissor[2] * sc),
                    (int)((float)last_raw_scissor[3] * sc));
                break;
            }
            next_tex = cmd->tex ? cmd->tex : &_white_dummy_tex;
            num_elements = cmd->num_verts / 4 * 6;
            gl_bind_tex2d_if_not_bound(next_tex->id);
            glDrawElements(GL_TRIANGLES, num_elements, GL_UNSIGNED_SHORT,
                (const GLvoid*)(offset));
            offset += num_elements * sizeof(GLushort);
        }
    }
    glBufferData(GL_ARRAY_BUFFER, _gui_vbo_size, 0, GL_DYNAMIC_DRAW);
    gl_bind_ebo(0);
    gl_bind_vbo(0);
    gl_use_program(0);
    gl_viewport(prev_vp[0], prev_vp[1], prev_vp[2], prev_vp[3]);
    gl_scissor(prev_scis[0], prev_scis[1], prev_scis[2], prev_scis[3]);
}

void
render_camera_init(render_camera_t *camera)
{
    memset(camera, 0, sizeof(*camera));
    camera->percentage_moved = 1.f;
}

int
render_world_init(render_world_t *render_world)
{
    memset(render_world, 0, sizeof(*render_world));
    GLuint buffers[2];
    glGenBuffers(2, buffers);
    gl_bind_vbo(buffers[0]);
    glBufferData(GL_ARRAY_BUFFER,
        MAX_TILE_SPRITES * FLOATS_PER_TILE * sizeof(GLfloat), 0,
        GL_DYNAMIC_DRAW);
    gl_bind_ebo(buffers[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, MAX_TILE_SPRITES * 6 * sizeof(GLuint),
        0, GL_STREAM_DRAW); /* Static or dynamic draw? */
    GLuint *ebo_memory = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
    if (!ebo_memory)
        muta_panic_print(
            "Failed to map vertex index memory for map rendering!");
    GLuint indices[6] = {0, 1, 2, 2, 3, 1};
    for (GLuint i = 0; i < MAX_TILE_SPRITES; ++i)
        for (GLuint j = 0; j < 6; ++j)
            ebo_memory[i * 6 + j] = indices[j] + i * 4;
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
    gl_bind_ebo(0);
    gl_bind_vbo(0);
    render_world->tile_vbo          = buffers[0];
    render_world->tile_ebo          = buffers[1];
    render_world->need_tile_update  = 1;
    render_world->max_cmds          = 256;
    render_world->cmds              = emalloc(
        render_world->max_cmds * sizeof(render_world_cmd_t));
    for (int i = 0; i < 3; ++i)
        render_world->last_position[i] = 0;
    return 0;
}

void
render_world_destroy(render_world_t *render_world)
{
    GLuint buffers[2] = {render_world->tile_vbo, render_world->tile_ebo};
    glDeleteBuffers(2, buffers);
    free(render_world->cmds);
    darr_free(render_world->ghost_tiles);
    memset(render_world, 0, sizeof(*render_world));
}

void
render_world_clear(render_world_t *render_world)
{
    render_world->need_tile_update  = 1;
    render_world->num_tile_sprites  = 0;
    render_world->num_cmds          = 0;
    for (int i = 0; i < 3; ++i)
        render_world->last_position[i] = 0;
    memset(&render_world->cached, 0, sizeof(render_world->cached));
}

void
render_world_update(render_world_t *rw, render_camera_t *camera, world_t *world,
    int *screen_target)
{
    int     tile_position[3]; /* From top left */
    int     start[3]            = {0};
    int     end[3];
    int     tile_w              = core_asset_config.tile_width;
    int     tile_h              = core_asset_config.tile_height;
    int     tile_top_h          = core_asset_config.tile_top_height;
    tex_t   tex                 = *_tile_sheet_tex;
    int     extra_w             = NUM_EXTRA_TILES * tile_w;
    int     extra_h             = NUM_EXTRA_TILES * tile_h;
    int     res_w               = core_asset_config.resolution_w + extra_w;
    int     res_h               = core_asset_config.resolution_h + extra_h;
    float   coordinate_space[4] = {0, 0, (float)res_w, (float)res_h};
    float   sx      = (float)screen_target[2] / (float)(res_w - extra_w);
    float   sy      = (float)screen_target[3] / (float)(res_h - extra_h);
    int     area_w  = (int)coordinate_space[2] / MAX(tile_w, 2) * 2 + 3;
    int     area_h  = (int)coordinate_space[3] / MAX(tile_top_h, 2) * 2 + 3;
    int     area_t  = (int)coordinate_space[3] / (tile_h - tile_top_h);
    int     area    = MAX(area_w, area_h);
    chunk_cache_t *cache;
    end[0] = area;
    end[1] = area;
    end[2] = area_t;
    tile_position[0] = camera->position[0] - area / 2;
    tile_position[1] = camera->position[1] - area / 2;
    tile_position[2] = camera->position[2] - area_t / 2;
    for (int i = 0; i < 3; ++i)
        if (tile_position[i] < 0)
            start[i] -= tile_position[i];
    if (tile_position[0] + area > world->map.tw)
        end[0] -= world->map.tw - (tile_position[0] + area);
    if (tile_position[1] + area > world->map.th)
        end[1] -= world->map.th - (tile_position[1] + area);
    if (tile_position[2] + area_t > MAP_CHUNK_T)
        end[2] -= MAP_CHUNK_T - (tile_position[2] + area_t);
    float end0plus1 = (float)(end[0] + end[1]);
    float end_z     = (float)end[2];
    float ox        = 0.f;
    float oy        = 0.f;
    if (camera->percentage_moved != 1.f)
    {
        int     direction[3];
        float   percentages[3];
        for (int i = 0; i < 3; ++i)
        {
            direction[i]    = camera->last_position[i] - camera->position[i];
            percentages[i]  = (float)direction[i] * camera->percentage_moved;
        }
        ox = percentages[0] * tile_w / 2 - percentages[1] * tile_w / 2;
        oy = percentages[0] * tile_w / 4 + percentages[1] * tile_top_h / 2 -
            percentages[2] * tile_h / 2;
        ox -= (float)direction[0] * tile_w / 2 - (float)direction[1] *
            tile_w / 2;
        oy -= (float)direction[0] * tile_w / 4 + (float)direction[1] *
            tile_top_h / 2 - percentages[2] * tile_h / 2;
    }
    float bx = 0.5f * coordinate_space[2] - (float)(tile_w / 2) + ox;
    float by = (float)(area_t * tile_top_h / 2 - area * tile_top_h / 4) -
        tile_top_h + oy;
    /* bx = roundf(bx);
       by = roundf(by); */
    gl_bind_vbo(rw->tile_vbo);
    /* Update tiles if necessary. Tiles are cached into a ready-built vertex
     * buffer. */
    if (rw->need_tile_update || memcmp(camera->position, rw->last_position,
       sizeof(camera->position)))
    {
        /* Clamp numbers to map dimensions */
        int start_i = start[0];
        if (tile_position[0] + start_i < 0)
        {
            start_i += abs(tile_position[0] + start_i);
            muta_assert(tile_position[0] + start_i == 0);
        }
        int start_j = start[1];
        if (tile_position[1] + start_j < 0)
        {
            start_j += abs(tile_position[1] + start_j);
            muta_assert(tile_position[1] + start_j  == 0);
        }
        int start_k = start[2];
        if (tile_position[2] + start_k < 0)
        {
            start_k += abs(tile_position[2] + start_k);
            muta_assert(tile_position[2] + start_k == 0);
        }
        int end_i = end[0];
        if (end_i + tile_position[0] > world->map.tw)
            end_i -= (end_i + tile_position[0]) - world->map.tw;
        int end_j = end[1];
        if (end_j + tile_position[1] > world->map.th)
            end_j -= (end_j + tile_position[1]) - world->map.th;
        int end_k = end[2];
        if (end_k + tile_position[2] > MAP_CHUNK_T)
            end_k -= (end_k + tile_position[2]) - MAP_CHUNK_T;
        glBufferData(GL_ARRAY_BUFFER,
            MAX_TILE_SPRITES * FLOATS_PER_TILE * sizeof(GLfloat), 0,
            GL_DYNAMIC_DRAW); /* Orphan the buffer */
        float   *memory = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
        uint32  offset  = 0;
        if (!memory)
            muta_panic_print("Failed to map vertex buffer!");
        for (int i = start_i; i < end_i; ++i)
        {
            for (int j = start_j; j < end_j; ++j)
            {
                int tx = tile_position[0] + i;
                int ty = tile_position[1] + j;
                cache = world_get_chunk_cache_of_tile(world, tx, ty);
                float row_base, row_diff;
                _compute_row_base_and_diff((float)i, (float)j, end0plus1,
                    &row_base, &row_diff);
                for (int k = start_k; k < end_k; ++k)
                {
                    int     tz      = tile_position[2] + k;
                    tile_t  tile    = chunk_cache_get_tile(cache, tx, ty, tz);
                    if (!tile)
                        continue;
                    tile_graphic_def_t *graphic = tile_get_graphic_def(tile);
                    float x = COMPUTE_PIXEL_X(i, j, graphic->ox);
                    float y = COMPUTE_PIXEL_Y(i, j, k + 2, graphic->oy);
                    float z = _compute_tile_pixel_z((float)i, (float)j,
                        (float)k, end_z, row_base, row_diff);
                    /* Write into the vertex buffer */
                    muta_assert(tz < MAP_CHUNK_T);
                    _write_tile_verts(memory + offset, &tex, graphic->clip, x,
                        y, z, _tile_depth_color_table[tz]);
                    offset += FLOATS_PER_TILE;
                    if (offset == MAX_TILE_SPRITES * FLOATS_PER_TILE)
                    {
                        LOG_ERR("Maximum number of tile sprites reached!\n");
                        goto tiles_done;
                    }
                }
            }
        }
        tiles_done:
        glUnmapBuffer(GL_ARRAY_BUFFER);
        rw->num_tile_sprites = offset / FLOATS_PER_TILE;
        if (!tex.id) /* Partially loaded texture, load again next frame */
            rw->need_tile_update = 1;
        else
            rw->need_tile_update = 0;
        memcpy(rw->last_position, camera->position, sizeof(camera->position));
    }
    gl_viewport(screen_target[0] - (int)(0.5f * sx * (float)extra_w),
        screen_target[1] - (int)(0.5f * sy * (float)extra_h),
        screen_target[2] + (int)(sx * (float)extra_w),
        screen_target[3] + (int)(sy * (float)extra_h));
    gl_scissor(screen_target[0], screen_target[1], screen_target[2],
        screen_target[3]);
    gl_color(0.125f, 0.463f, 1.f, 0.f);
    gl_clear(RB_CLEAR_COLOR_BIT | RB_CLEAR_DEPTH_BIT);
    gl_enable_depth_test();
    gl_use_program(_tile_shader);
    glUniform2f(_tile_shader_offset_index, bx, by);
    GLfloat projection[4][4];
    gl_orthographic_projection_from_coordinate_space(coordinate_space,
        projection);
    glUniformMatrix4fv(_tile_shader_projection_index, 1, GL_FALSE,
        &projection[0][0]);
    gl_bind_ebo(rw->tile_ebo);
    _bind_tile_vao();
    gl_active_texture(0);
    gl_bind_tex2d_if_not_bound(tex.id);
    glDrawElements(GL_TRIANGLES, rw->num_tile_sprites * 6, GL_UNSIGNED_INT, 0);
    gl_bind_vbo(0);
    gl_bind_ebo(0);
    /*-- Render entities --*/
    int cull_area[6];
    for (int i = 0; i < 3; ++i)
        cull_area[i] = tile_position[i] + start[i];
    for (int i = 0; i < 3; ++i)
        cull_area[3 + i] = end[i] - start[i];
    render_system_set_cull_area(&world->render_system, cull_area);
    render_component_cmd_t *cmds = world->render_system.cmds;
    uint32 num_cmds = world->render_system.num_cmds;
    /*-- Make sure there's space for draw cmds --*/
    if (rw->max_cmds < num_cmds)
    {
        uint32 max_cmds = num_cmds + 16;
        rw->cmds        = emalloc(max_cmds * sizeof(render_world_cmd_t));
        rw->max_cmds    = max_cmds;
    }
    rw->num_cmds = num_cmds;
    render_world_cmd_t *rw_cmds = rw->cmds;
    for (uint32 i = 0; i < num_cmds; ++i)
    {
        render_component_cmd_t *cmd = &cmds[i];
        float   lx  = (float)(cmd->last_tile_position[0] - tile_position[0]);
        float   ly  = (float)(cmd->last_tile_position[1] - tile_position[1]);
        float   lz  = (float)(cmd->last_tile_position[2] - tile_position[2]);
        float   nx  = (float)(cmd->tile_position[0] - tile_position[0]);
        float   ny  = (float)(cmd->tile_position[1] - tile_position[1]);
        float   nz  = (float)(cmd->tile_position[2] - tile_position[2]);
        float   lpx = COMPUTE_PIXEL_X(lx, ly, bx + cmd->offset[0]);
        float   lpy = COMPUTE_PIXEL_Y(lx, ly, lz + 1, by + cmd->offset[1]);
        float   lpz = _compute_object_pixel_z(lx, ly, lz, end);
        float   npx = COMPUTE_PIXEL_X(nx, ny, bx + cmd->offset[0]);
        float   npy = COMPUTE_PIXEL_Y(nx, ny, nz + 1, by + cmd->offset[1]);
        float   npz = _compute_object_pixel_z(nx, ny, nz, end);
        float   pz  = lpz;
        /* If moving, modify the z value based on the direction. Otherwise
         * objects may get clipped under tiles temporarily. */
        if (cmd->percentage_travelled != 1.f)
        {
            if (nx - lx > 0)
                pz = npz;
            else if (ny - ly > 0)
                pz = npz;
        }
        rw_cmds[i].texture = *cmd->texture;
        for (int j = 0; j < 4; ++j)
            rw_cmds[i].clip[j] = cmd->clip[j];
        rw_cmds[i].x = lpx + cmd->percentage_travelled * (npx - lpx);
        rw_cmds[i].y = lpy + cmd->percentage_travelled * (npy - lpy),
        rw_cmds[i].z = pz;//lpz + cmd->percentage_travelled * (npz - lpz);
        rw_cmds[i].entity = cmd->entity;
    }
    qsort(rw_cmds, num_cmds, sizeof(render_world_cmd_t),
        _compare_render_world_cmds);
    sb_begin(sb_opaque_shader, coordinate_space);
    /*-- Render ghost tiles --*/
    uint32 num_ghost_tiles = darr_num(rw->ghost_tiles);
    for (uint32 i = 0; i < num_ghost_tiles; ++i)
    {
        render_ghost_tile_t *rt         = &rw->ghost_tiles[i];
        if (rt->x < tile_position[0] || rt->y < tile_position[1] ||
            rt->z < tile_position[2] || rt->x >= tile_position[0] + area_w ||
            rt->y >= tile_position[1] + area_w ||
            rt->z >= tile_position[2] + area_t)
            continue;
        int                 i           = (int)rt->x - tile_position[0];
        int                 j           = (int)rt->y - tile_position[1];
        int                 k           = (int)rt->z - tile_position[2];
        float               row_base, row_diff;
        _compute_row_base_and_diff((float)i, (float)j, end0plus1, &row_base,
            &row_diff);
        tile_graphic_def_t  *graphic = tile_get_graphic_def(rt->tile);
        float x = COMPUTE_PIXEL_X(i, j, bx + graphic->ox);
        float y = COMPUTE_PIXEL_Y(i, j, k, by + graphic->oy);
        float z = _compute_tile_pixel_z((float)i, (float)j, (float)k, end_z,
            row_base, row_diff);
        sb_sprite_c(&tex, graphic->clip, x, y, z,
            _tile_depth_color_table[rt->z]);
    }
    /*-- Render entities from commands --*/
    for (uint32 i = 0; i < num_cmds; ++i)
        sb_sprite(&rw_cmds[i].texture, rw_cmds[i].clip, rw_cmds[i].x,
            rw_cmds[i].y, rw_cmds[i].z);
    /*-- Render the tile selector. --*/
    if (camera->draw_tile_selector)
    {
        tile_t tile = core_asset_config.tile_selector_ids[
            camera->tile_selector_id_index];
        if (tile)
        {
            float   tp[3];
            int     limits[3] = {area, area, area_t};
            for (int i = 0; i < 3; ++i)
            {
                if (camera->tile_selector_position[i] < tile_position[i] ||
                    camera->tile_selector_position[i] >=
                        tile_position[i] + limits[i])
                    goto out;
                tp[i] = (float)(camera->tile_selector_position[i] -
                    tile_position[i]);
            }
            tp[2] += 1.f;
            float px = COMPUTE_PIXEL_X(tp[0], tp[1], bx);
            float py = COMPUTE_PIXEL_Y(tp[0], tp[1], tp[2] + 2,
                by + tile_top_h);
            float pz = _compute_object_pixel_z(tp[0], tp[1], tp[2], end);
            muta_assert(camera->tile_selector_id_index >= 0 &&
                camera->tile_selector_id_index < CORE_NUM_TILE_SELECTOR_TYPES);
            tile_graphic_def_t *graphic = tile_get_graphic_def(tile);
            sb_sprite(_tile_sheet_tex, graphic->clip, px + graphic->ox,
                py + graphic->oy, pz);
        }
    }
    out:
    sb_end();
    rw->cached.area_w       = area_w;
    rw->cached.area_h       = area_h;
    rw->cached.area_t       = area_t;
    rw->cached.bx           = bx;
    rw->cached.by           = by;
    rw->cached.tile_w       = (float)tile_w;
    rw->cached.tile_h       = (float)tile_h;
    rw->cached.tile_top_h   = (float)tile_top_h;
    rw->cached.extra_w      = extra_w;
    rw->cached.extra_h      = extra_h;
    rw->cached.scale_x      = sx;
    rw->cached.scale_y      = sy;
    memcpy(rw->cached.coordinate_space, coordinate_space, 4 * sizeof(float));
    memcpy(rw->cached.tile_position, tile_position, 3 * sizeof(int));
    memcpy(rw->cached.screen_area, screen_target, 4 * sizeof(int));
}

void
render_world_translate_screen_to_world_pixel(render_world_t *render_world,
    int screen_x, int screen_y, float *restrict ret_x, float *restrict ret_y)
{
    float sx = render_world->cached.scale_x;
    float sy = render_world->cached.scale_y;
    if (sx)
    {
        float x = (float)(screen_x - render_world->cached.screen_area[0]);
        *ret_x = x / sx + render_world->cached.extra_w / 2;
    } else
        *ret_x = 0;
    if (sy)
    {
        float y = (float)(screen_y - render_world->cached.screen_area[1]);
        *ret_y = y / sy + render_world->cached.extra_h / 2;
    } else
        *ret_y = 0;
}

void
render_world_translate_world_to_screen_pixel(render_world_t *render_world,
    float world_x, float world_y, int *restrict ret_x, int *restrict ret_y)
{
    float sx = render_world->cached.scale_x;
    float sy = render_world->cached.scale_y;
    float x = (world_x - 0.5f * (float)render_world->cached.extra_w);
    float y = (world_y - 0.5f * (float)render_world->cached.extra_h);
    *ret_x = render_world->cached.screen_area[0] + (int)(sx * x);
    *ret_y = render_world->cached.screen_area[1] + (int)(sy * y);
}

void
render_world_pixel_to_tile_position(render_world_t *rw, float pixel_x,
    float pixel_y, int *restrict ret_x, int *restrict ret_y)
{
    if (!rw->cached.coordinate_space[2] || !rw->cached.coordinate_space[3])
    {
        *ret_x = 0;
        *ret_y = 0;
        return;
    }
    int ux, uy;
    _pixel_to_isometric_raw(rw, pixel_x, pixel_y, &ux, &uy);
    *ret_x = ux + rw->cached.tile_position[0] + rw->cached.area_t;
    *ret_y = uy + rw->cached.tile_position[1] + rw->cached.area_t;
}

void
render_world_pixel_to_tile_position_at_z(render_world_t *rw,
    float pixel_x, float pixel_y, int z, int *restrict ret_x,
    int *restrict ret_y)
{
    if (!rw->cached.coordinate_space[2] || !rw->cached.coordinate_space[3])
    {
        *ret_x = 0;
        *ret_y = 0;
        return;
    }
    int ux, uy;
    _pixel_to_isometric_raw(rw, pixel_x, pixel_y, &ux, &uy);
    int rz = z - rw->cached.tile_position[2];
    *ret_x = ux + rw->cached.tile_position[0] + rz + 1;
    *ret_y = uy + rw->cached.tile_position[1] + rz + 1;
}

void
render_world_tile_to_pixel_position(render_world_t *render_world,
    int tile_position[3], int *restrict ret_x, int *restrict ret_y)
{
    float tp[3];
    for (int i = 0; i < 3; ++i)
        tp[i] = (float)(tile_position[i] -
            render_world->cached.tile_position[i]);
    float   tile_w      = render_world->cached.tile_w;
    float   tile_h      = render_world->cached.tile_h;
    float   tile_top_h  = render_world->cached.tile_top_h;
    float   ox          = render_world->cached.bx + 0.5f * tile_w;
    float   oy          = render_world->cached.by + 0.5f * tile_top_h;
    *ret_x = (int)COMPUTE_PIXEL_X(tp[0], tp[1], ox);
    *ret_y = (int)COMPUTE_PIXEL_Y(tp[0], tp[1], tp[2], oy);
}

bool32
render_world_find_tile_by_pixel_position(render_world_t *rw, world_t *world,
    int pixel_x, int pixel_y, int ret_position[3])
{
    int min_z   = MAX(rw->cached.tile_position[2], 0);
    int i       = 0;
    int iso_position[2];
    render_world_pixel_to_tile_position(rw, (float)pixel_x, (float)pixel_y,
        &iso_position[0], &iso_position[1]);
    for (int z = rw->cached.tile_position[2] + rw->cached.area_t - 1;
        z >= min_z; --z, ++i)
    {
        int         x           = iso_position[0] - i;
        int         y           = iso_position[1] - i;
        tile_t      tile        = world_get_tile(world, x, y, z);
        tile_def_t *tile_def    = tile_get_def(tile);
        if (!(!tile_def->passthrough || z < 0))
            continue;
        ret_position[0] = x;
        ret_position[1] = y;
        ret_position[2] = z;
        return 1;
    }
    return 0;
}

uint32
render_world_find_entities_by_pixel_position(render_world_t *render_world,
    float x, float y, entity_t **ret_entities, uint32 max_ret_entities)
{
    if (!max_ret_entities)
        return 0;
    render_world_cmd_t *cmds        = render_world->cmds;
    uint32              num_cmds    = render_world->num_cmds;
    uint32              ret         = 0;
    for (uint32 i = 0; i < num_cmds; ++i)
    {
        uint32 j = num_cmds - 1 - i;
        if (x < cmds[j].x)
            continue;
        if (x > cmds[j].x + (cmds[j].clip[2] - cmds[j].clip[0]))
            continue;
        if (y < cmds[j].y)
            continue;
        if (y > cmds[j].y + (cmds[j].clip[3] - cmds[j].clip[1]))
            continue;
        if (!entity_is_spawned(cmds[j].entity))
            continue;
        ret_entities[ret++] = cmds[j].entity;
        if (ret == max_ret_entities)
            break;
    }
    return ret;
}

void
render_world_add_ghost_tile(render_world_t *rw, uint16 tag, tile_t tile,
    int32 x, int32 y, uint8 z)
{
    render_world_remove_ghost_tile_by_position(rw, x, y, z);
    render_ghost_tile_t rt = {.tile = tile, .x = x, .y = y, .z = z, .tag = tag};
    darr_push(rw->ghost_tiles, rt);
    DEBUG_PRINTFF("Added %d, %d, %d\n",
        rw->ghost_tiles[darr_num(rw->ghost_tiles) - 1].x,
        rw->ghost_tiles[darr_num(rw->ghost_tiles) - 1].y,
        (int)rw->ghost_tiles[darr_num(rw->ghost_tiles) - 1].z);
}

void
render_world_clear_ghost_tiles(render_world_t *rw)
    {darr_clear(rw->ghost_tiles);}

void
render_world_remove_tiles_by_tag(render_world_t *rw, uint16 tag)
{
    for (uint32 i = 0; i < darr_num(rw->ghost_tiles); ++i)
        if (rw->ghost_tiles[i].tag == tag)
            darr_erase(&rw->ghost_tiles, i--);
}

void
render_world_remove_ghost_tile_by_position(render_world_t *rw, int32 x,
    int32 y, uint8 z)
{
    uint32 num = darr_num(rw->ghost_tiles);
    for (uint32 i = 0; i < num; ++i)
    {
        render_ghost_tile_t *rt = &rw->ghost_tiles[i];
        if (rt->x == x && rt->y == y && rt->z == z)
        {
            darr_erase(&rw->ghost_tiles, i);
            break;
        }
    }
}

int
render_world_compute_tile_pixel_x(render_world_t *render_world, int tx, int ty,
    int offset)
{
    int tile_w = render_world->cached.tile_w;
    return COMPUTE_PIXEL_X(tx, ty, offset) + render_world->cached.bx -
        render_world->cached.extra_w / 2;
}

int
render_world_compute_tile_pixel_y(render_world_t *render_world, int tx, int ty,
    int tz, int offset)
{
    int tile_w = render_world->cached.tile_w;
    int tile_h = render_world->cached.tile_h;
    int tile_top_h = render_world->cached.tile_top_h;
    return COMPUTE_PIXEL_Y(tx, ty, tz, offset) + render_world->cached.by -
        render_world->cached.extra_h / 2 - tile_top_h;
}

static inline void
_bind_gui_vao(void)
{
    glVertexAttribPointer(SHADER_ATTR_LOC_POS, 2, GL_FLOAT, GL_FALSE,
        5 * sizeof(GLfloat), 0);
    glVertexAttribPointer(SHADER_ATTR_LOC_UV, 2, GL_FLOAT, GL_FALSE,
        5 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
    glVertexAttribPointer(SHADER_ATTR_LOC_COL, 4, GL_UNSIGNED_BYTE, GL_TRUE,
        5 * sizeof(GLfloat), (GLvoid*)(4 * sizeof(GLfloat)));
    glEnableVertexAttribArray(SHADER_ATTR_LOC_UV);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_POS);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_COL);
}

static inline void
_bind_tile_vao(void)
{
    uint32 stride = 6 * sizeof(GLfloat);
    glVertexAttribPointer(SHADER_ATTR_LOC_POS, 3, GL_FLOAT, GL_FALSE, stride,
        0);
    glVertexAttribPointer(SHADER_ATTR_LOC_UV, 2, GL_FLOAT, GL_FALSE, stride,
        (GLvoid*)(3 * sizeof(GLfloat)));
    glVertexAttribPointer(SHADER_ATTR_LOC_COL, 4, GL_UNSIGNED_BYTE, GL_TRUE,
        stride, (GLvoid*)(5 * sizeof(GLfloat)));
    glEnableVertexAttribArray(SHADER_ATTR_LOC_POS);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_UV);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_COL);
}

static inline void
_write_tile_verts(void *memory, tex_t *tex, float *clip, float x, float y,
    float z, uint8 color[4])
{
    float *floats   = memory;
    float w         = clip[2] - clip[0];
    float h         = clip[3] - clip[1];
    floats[0]   = x;
    floats[1]   = y;
    floats[2]   = z;
    floats[3]   = clip[0] / tex->w;
    floats[4]   = clip[1] / tex->h;
    memcpy(&floats[5], color, sizeof(float));
    floats[6]   = x + w;
    floats[7]   = y;
    floats[8]   = z;
    floats[9]   = clip[2] / tex->w;
    floats[10]   = clip[1] / tex->h;
    memcpy(&floats[11], color, sizeof(float));
    floats[12]  = x;
    floats[13]  = y + h;
    floats[14]  = z;
    floats[15]  = clip[0] / tex->w;
    floats[16]  = clip[3] / tex->h;
    memcpy(&floats[17], color, sizeof(float));
    floats[18]  = x + w;
    floats[19]  = y + h;
    floats[20]  = z;
    floats[21]  = clip[2] / tex->w;
    floats[22]  = clip[3] / tex->h;
    memcpy(&floats[23], color, sizeof(float));
#if 0
    floats[0]   = x;
    floats[1]   = y;
    floats[2]   = z;
    floats[3]   = clip[0] / tex->w;
    floats[4]   = clip[1] / tex->h;
    floats[5]   = x + w;
    floats[6]   = y;
    floats[7]   = z;
    floats[8]   = clip[2] / tex->w;
    floats[9]   = clip[1] / tex->h;
    floats[10]  = x;
    floats[11]  = y + h;
    floats[12]  = z;
    floats[13]  = clip[0] / tex->w;
    floats[14]  = clip[3] / tex->h;
    floats[15]  = x + w;
    floats[16]  = y + h;
    floats[17]  = z;
    floats[18]  = clip[2] / tex->w;
    floats[19]  = clip[3] / tex->h;
#endif
}

static inline int
_compare_render_world_cmds(const void *a, const void *b)
{
    const render_world_cmd_t *cmd_a = a;
    const render_world_cmd_t *cmd_b = b;
    return cmd_a->texture.id < cmd_b->texture.id;
}

static inline void
_compute_row_base_and_diff(float tx, float ty, float end0plus1,
    float *restrict ret_base, float *restrict ret_diff)
{
    float row_max = (tx + ty + 2) / end0plus1;
    *ret_base = (tx + ty) / end0plus1;
    *ret_diff = row_max - *ret_base;
}

static inline float
_compute_tile_pixel_z(float tx, float ty, float tz, float end_z, float row_base,
    float row_diff)
    {return row_base + row_diff * (tz / end_z);}

static inline float
_compute_object_pixel_z(float tx, float ty, float tz, int end[3])
{
    float row_base, row_diff;
    _compute_row_base_and_diff(tx, ty, (float)(end[0] + end[1]), &row_base,
        &row_diff);
    return row_base + row_diff * (tz / (float)end[2]);
}

static void
_pixel_to_isometric_raw(render_world_t *rw, float pixel_x, float pixel_y,
    int *restrict ret_x, int *restrict ret_y)
{
    float tile_w        = rw->cached.tile_w;
    float tile_h        = rw->cached.tile_h;
    float tile_half_w   = 0.5f * tile_w;
    float tile_half_h   = 0.5f * tile_h;
    float trans_x       = pixel_x - rw->cached.bx - tile_half_w;
    float trans_y       = pixel_y - rw->cached.by + tile_half_h;
    float half_twh      = 0.5f * tile_w * tile_h;
    *ret_x = (int)floorf((tile_w * trans_y + tile_half_h * trans_x) / half_twh);
    *ret_y = (int)floorf((tile_w * trans_y - tile_half_h * trans_x) / half_twh);
}
