#include "scripts.h"
#include "scripts/all_scripts.h"
#include "../../shared/hashtable.h"
#include "../../shared/types.h"
#include "../../shared/common_utils.h"
#include "../../shared/abilities.h"

hashtable_define(ability_id_ability_script_table, ability_id_t,
    ability_script_t*);

typedef struct ability_id_ability_script_table
    ability_id_ability_script_table_t;

static ability_id_ability_script_table_t _abilities_table;

void
scripts_init(void)
{
    ability_id_ability_script_table_einit(&_abilities_table,
        num_all_ability_scripts);
    uint32 num = num_all_ability_scripts;
    for (uint32 i = 0; i < num; ++i)
    {
        ability_script_t *script = all_ability_scripts[i];
        muta_assert(script->on_request);
        muta_assert(script->on_response);
        ability_id_t id = script->id;
        muta_assert(!ability_id_ability_script_table_exists(&_abilities_table,
            id));
        ability_id_ability_script_table_einsert(&_abilities_table, id, script);
        script->def = ab_get(script->id);
        muta_assert(script->def);
    }
}

void
scripts_destroy(void)
    {ability_id_ability_script_table_destroy(&_abilities_table);}

ability_script_t *
scripts_find_ability(ability_id_t id)
{
    ability_script_t **script = ability_id_ability_script_table_find(
        &_abilities_table, id);
    if (!script)
        return 0;
    return *script;
}
