#include "iso_sprite_component.h"
#include "render_component.h"
#include "assets.h"
#include "world.h"

struct iso_sprite_component_t
{
    entity_t                    *entity;
    as_iso_sprite_t             *asset;
    render_component_sprite_t   *sprite;
};

static int
_iso_sprite_system_init(world_t *world, uint32 num_components);

static void
_iso_sprite_system_destroy(world_t *world);

static component_handle_t
_iso_sprite_component_attach(entity_t *entity);

static void
_iso_sprite_component_detach(component_handle_t handle);

static void
_update_sprite(iso_sprite_component_t *component);

component_definition_t iso_sprite_component_definition =
{
    _iso_sprite_system_init,
    _iso_sprite_system_destroy,
    0,
    _iso_sprite_component_attach,
    _iso_sprite_component_detach
};

void
iso_sprite_component_set_asset(component_handle_t handle, uint32 iso_sprite_id)
{
    iso_sprite_component_t *component = handle;
    if (!component->sprite)
        return;
    as_unclaim_iso_sprite(component->asset);
    component->asset = as_claim_iso_sprite(iso_sprite_id, 1);
    muta_assert(component->asset);
    _update_sprite(component);
}

static int
_iso_sprite_system_init(world_t *world, uint32 num_components)
{
    fixed_pool_init(&world->iso_sprite_system.components, num_components);
    return 0;
}

static void
_iso_sprite_system_destroy(world_t *world)
    {fixed_pool_destroy(&world->iso_sprite_system.components);}

static component_handle_t
_iso_sprite_component_attach(entity_t *entity)
{
    iso_sprite_system_t *system =
        &entity_get_world(entity)->iso_sprite_system;
    iso_sprite_component_t *component = fixed_pool_new(&system->components);;
    if (!component)
        return 0;
    component->entity   = entity;
    component->asset    = 0;
    component->sprite   = 0;
    component_handle_t render_component = entity_get_component(entity,
        &render_component_definition);
    if (!render_component)
        return component;
    component->sprite   = render_component_new_sprite(render_component);
    if (!component->sprite)
    {
        fixed_pool_free(&system->components, component);
        return 0;
    }
    return component;
}

static void
_iso_sprite_component_detach(component_handle_t handle)
{
    iso_sprite_component_t *component = handle;
    as_unclaim_iso_sprite(component->asset);
    if (component->sprite)
    {
        component_handle_t render_component = entity_get_component(
            component->entity, &render_component_definition);
        if (render_component)
            render_component_sprite_free(component->sprite);
    }
    fixed_pool_free(
        &entity_get_world(component->entity)->iso_sprite_system.components,
        component);
}

static void
_update_sprite(iso_sprite_component_t *component)
{
    int direction = entity_get_direction(component->entity);
    render_component_sprite_t *sprite = component->sprite;
    render_component_sprite_set_texture(sprite, &component->asset->ta->tex);
    render_component_sprite_set_clip(sprite,
        component->asset->clips[direction]);
    float offset_x = component->asset->offsets[direction][0];
    float offset_y = component->asset->offsets[direction][1];
    render_component_sprite_set_offset(sprite, offset_x, offset_y);
}
