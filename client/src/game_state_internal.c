#include "game_state_internal.h"
#include "game_state_gui.h"
#include "shard.h"
#include "log.h"
#include "script_api.h"
#include "scripts.h"
#include "../../shared/client_packets.h"
#include "../../shared/abilities.h"

static void
_set_target(game_state_t *gs, entity_t *entity)
{
    if (!entity)
        gs->next_target.is_null = 1;
    else
    {
        gs->next_target.is_null = 0;
        gs->next_target.handle  = entity_get_handle(entity);
    }
    gs->next_target.have = 1;

}

static void
_clear_clicked_entities(game_state_t *gs)
    {gs->next_clicked_entities.clear = 1;}

void
gsi_set_up_paths_and_create_directories(game_state_t *gs,
    const char *character_name, const char *shard_name)
{
    const char *characters_dir_path = "saved_data/characters";
    create_directory(characters_dir_path);
    dchar *path = dstr_create_empty(256);
    dstr_setf(&path, "%s/%s", characters_dir_path, shard_name);
    create_directory(path);
    dstr_append_char(&path, '/');
    dstr_append(&path, character_name);
    create_directory(path);
    /* Action buttons file path */
    dstr_set(&gs->action_buttons_path, path);
    dstr_append(&gs->action_buttons_path, "/action_buttons.cfg");
    /* Hotkeys file path */
    dstr_set(&gs->hotkeys_path, path);
    dstr_append(&gs->hotkeys_path, "/hotkeys.def");
    dstr_free(&path);
}

void
gsi_target_clicked_entity(game_state_t *gs, uint32 index)
{
    entity_t *entity = gs->clicked_entities.all[index];
    _set_target(gs, entity);
    _clear_clicked_entities(gs);
}

void
gsi_pick_up_dragged_item(game_state_t *gs, enum equipment_slot equipment_slot,
    int x, int y)
{
    muta_assert(gs->dragged_item.have);
    LOG_DEBUG("Attempt to pick up dynamic object runtime id %u in equipment "
        "slot %d at %d, %d.", gs->dragged_item.runtime_id, (int)equipment_slot,
        x, y);
    clmsg_pick_up_dynamic_object_t s =
    {
        .dobj_runtime_id = gs->dragged_item.runtime_id,
        .equipment_slot  = (uint8)equipment_slot,
        .x_in_bag        = (uint8)x,
        .y_in_bag        = (uint8)y
    };
    bbuf_t bb = shard_send(CLMSG_PICK_UP_DYNAMIC_OBJECT_SZ);
    if (!bb.max_bytes)
        return;
    clmsg_pick_up_dynamic_object_write(&bb, &s);
    gs->dragged_item.have = 0;
    gsgui_on_stop_dragging_dynamic_object();
}

void
gsi_clear_dragged_item(game_state_t *gs)
{
    gs->dragged_item.have = 0;
    gsgui_on_stop_dragging_dynamic_object();
}

void
gsi_drag_item_in_inventory(game_state_t *gs, item_t *item)
{
    muta_assert(!gs->dragged_item.have);
    gs->dragged_item.have           = 1;
    gs->dragged_item.w_in_inventory = (int)item->w;
    gs->dragged_item.h_in_inventory = (int)item->h;
    gs->dragged_item.runtime_id     = item->runtime_id;
    gs->dragged_item.drag_type      = GS_DRAG_TYPE_OBJECT_IN_INVENTORY;
    gsgui_on_start_drag_dynamic_object(item->type_id);
}

void
gsi_request_move_dragged_item_in_inventory(game_state_t *gs,
    enum equipment_slot new_equipment_slot, uint8 new_x, uint8 new_y)
{
    muta_assert(gs->dragged_item.have);
    muta_assert(gs->dragged_item.drag_type == GS_DRAG_TYPE_OBJECT_IN_INVENTORY);
    muta_assert(!gs->move_requested_item.type);
    clmsg_move_inventory_item_t s =
    {
        .dobj_runtime_id    = gs->dragged_item.runtime_id,
        .new_equipment_slot = (uint8)new_equipment_slot,
        .new_x              = (uint8)new_x,
        .new_y              = (uint8)new_y
    };
    bbuf_t bb = shard_send(CLMSG_MOVE_INVENTORY_ITEM_SZ);
    if (bb.max_bytes)
        clmsg_move_inventory_item_write(&bb, &s);
    gs->move_requested_item.type        = GS_MOVE_REQUEST_TYPE_WITHIN_INVENTORY;
    gs->move_requested_item.runtime_id  = gs->dragged_item.runtime_id;
    gsi_clear_dragged_item(gs);
    LOG_DEBUG("Sent request to move item %u to equipment slot %d at position "
        "%u, %u.", gs->dragged_item.runtime_id, new_equipment_slot,
        (uint8)new_x, (uint8)new_y);
}

void
gsi_write_local_error_message(const char *msg)
    {gsgui_on_local_error_msg_received(msg);}

void
gsi_use_ability(game_state_t *gs, ability_id_t ability_id)
{
    if (gs->charge_bar.active)
        return;
    clmsg_use_ability_t s       = {.ability_id = ability_id};
    entity_t            *entity = gs->target_entity;
    if (entity)
    {
        entity_type_data_t *type_data = entity_get_type_data(entity);
        switch (type_data->type)
        {
        case ENTITY_TYPE_CREATURE:
            s.target_type = COMMON_ENTITY_CREATURE;
            s.target_id = type_data->creature.id;
            break;
        case ENTITY_TYPE_DYNAMIC_OBJECT:
            s.target_type = COMMON_ENTITY_DYNAMIC_OBJECT;
            s.target_id = type_data->dynamic_object.id;
            break;
        case ENTITY_TYPE_PLAYER:
            s.target_type = COMMON_ENTITY_PLAYER;
            s.target_id = type_data->player.id;
            break;
        case ENTITY_TYPE_STATIC_OBJECT:
            DEBUG_PRINTFF("Can't use abilities on a static object.");
            return;
        default:
            muta_assert(0);
        }
    } else
    {
        entity          = gs->player_entity;
        s.target_type   = COMMON_ENTITY_PLAYER;
        s.target_id     = entity_get_type_data(gs->player_entity)->player.id;
    }
    ability_script_t *script = scripts_find_ability(ability_id);
    muta_assert(script);
    if (script->can_use && !script->can_use(entity))
    {
        LOG_DEBUG("Can't use ability.");
        return;
    }
    bbuf_t bb = shard_send(CLMSG_USE_ABILITY_SZ);
    if (!bb.max_bytes)
        return;
    clmsg_use_ability_write(&bb, &s);
    script->on_request(entity);
    if (!script->compute_charging_time)
        return;
    uint32 charging_time = script->compute_charging_time(entity);
    gs->charge_bar.active       = 1;
    gs->charge_bar.max_ms       = charging_time;
    gs->charge_bar.current_ms   = 0;
    gs->charge_bar.ability_id   = ability_id;
    uint32 title_len        = strlen(script->def->name);
    uint32 max_title_len    = MAX_CHARGE_BAR_TITLE_LEN - 3;
    uint32 num_to_copy;
    bool32 add_3_dots = 0;
    if (title_len > max_title_len)
    {
        num_to_copy = max_title_len;
        add_3_dots  = 1;
    } else
        num_to_copy = title_len;
    memcpy(gs->charge_bar.title, script->def->name, num_to_copy);
    if (add_3_dots)
    {
        for (uint32 i = 0; i < 3; ++i)
            gs->charge_bar.title[num_to_copy + i] = '.';
        gs->charge_bar.title[num_to_copy + 3] = 0;
    } else
        gs->charge_bar.title[num_to_copy] = 0;
}
