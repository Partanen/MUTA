#include "main_menu_screen.h"
#include "animator_screen.h"
#include "sprite_editor.h"
#include "character_selection_screen.h"
#include "core.h"
#include "render.h"
#include "assets.h"
#include "sprite_context.h"
#include "audio.h"
#include "login.h"
#include "shard.h"
#include "game_state.h"
#include "gui_styles.h"
#include "editor/screen.h"
#include "gui.h"

static void
_main_menu_update(double dt);

static void
_main_menu_open(void);

static void
_main_menu_close(void);

static void
_main_menu_keydown(int key, bool32 is_repeat);

static void
_try_connect(void);

static void
_cancel_connection_attempt(void);

static void
_draw_login_info_fields(void);

static void
_draw_login_progress_win(void);

static void
_draw_shards_win(void);

static void
_draw_dc_reason_win(void);

static inline bool32
_disconnected_this_frame(void);

screen_t main_menu_screen =
{
    .name       = "Main Menu",
    .update     = _main_menu_update,
    .open       = _main_menu_open,
    .close      = _main_menu_close,
    .keydown    = _main_menu_keydown
};

static sprite_t             *_bg_sprite     = 0;
static as_animation_t       *_bg_anim_asset = 0;
static as_sound_t           *_login_music   = 0;
static char                 _account_name_buf[MAX_ACC_NAME_LEN + 1];
static char                 _password_buf[MAX_PW_LEN + 1];
static bool32               _show_login_progress_win;
static bool32               _show_developer_options = 0;
static bool32               _show_dc_reason_win;
static gui_button_style_t   _offline_shard_button_style;
static char                 _selected_shard_name[MAX_SHARD_NAME_LEN + 1];
static int                  _last_login_status;
static bool32               _make_text_input_active;

static void
_main_menu_update(double dt)
{
    if (_disconnected_this_frame() && login_get_last_error())
        _show_dc_reason_win = 1;
    /* Clear whole window with black */
    gl_viewport_and_scissor(0, 0, core_window_w(), core_window_h());
    gl_color(0.0f, 0.0f, 0.0f, 1.0f);
    gl_clear(RB_CLEAR_COLOR_BIT);
    /* Set resolution proper */
    int viewport[4];
    core_compute_target_viewport(viewport);
    gl_viewport_and_scissor(viewport[0], viewport[1], viewport[2], viewport[3]);
    gl_color(0.125f, 0.463f, 1.f, 0.f);
    gl_clear(RB_CLEAR_COLOR_BIT);
    sprite_context_update(core_sprite_context, dt);
    gui_font(gui_style_menu_font_fancy);
    gui_button_style(&gui_style_menu_button);
    /*-- Background --*/
    gui_origin(GUI_CENTER_CENTER);
    gui_texture(sprite_get_cur_tex(_bg_sprite), 0, 0, 0);
    /*-- Bottom version text --*/
    gui_origin(GUI_BOTTOM_LEFT);
    int corner_btn_xy = (int)(0.005f * (float)core_asset_config.resolution_w);
    gui_textf("MUTA v. %s", 0, corner_btn_xy, corner_btn_xy, MUTA_VERSION_STR);
    /*-- Logo --*/
    gui_origin(GUI_BOTTOM_RIGHT);
    int corner_btn_w =
        (int)(0.175f * (float)core_asset_config.resolution_w);
    int corner_btn_h = (int)(0.06f* (float)core_asset_config.resolution_h);
    if (gui_button("exit", corner_btn_xy, corner_btn_xy, corner_btn_w,
        corner_btn_h, 0))
        core_stop();
    if (_show_developer_options)
    {
        if (gui_button("animator", corner_btn_xy,
            gui_get_last_button_y() + gui_get_last_button_h() + 2,
            corner_btn_w, corner_btn_h, 0))
            core_set_screen(&animator_screen);
        if (core_config.editor_mode)
            if (gui_button("editor", corner_btn_xy,
                gui_get_last_button_y() + gui_get_last_button_h() + 2,
                corner_btn_w, corner_btn_h, 0))
                core_set_screen(&editor_screen);
        if (gui_button("sprite_editor", corner_btn_xy,
            gui_get_last_button_y() + gui_get_last_button_h() + 2, corner_btn_w,
            corner_btn_h, 0))
            core_set_screen(&sprite_editor_screen);
    }
    /*-- Draw the login info entry window --*/
    _draw_login_info_fields();
    gui_origin(GUI_CENTER_CENTER);
    switch (login_get_status())
    {
    case LOGIN_STATUS_DISCONNECTED:
        if (_show_dc_reason_win)
            _draw_dc_reason_win();
        break;
    case LOGIN_STATUS_CONNECTING:
    case LOGIN_STATUS_HANDSHAKING:
        _draw_login_progress_win();
        break;
    case LOGIN_STATUS_SELECTING_SHARD:
    case LOGIN_STATUS_SELECTED_SHARD:
        _draw_shards_win();
        break;
    }
    if (shard_get_status() == SHARD_STATUS_CONNECTED)
    {
        login_disconnect();
        core_set_screen(&character_select_screen);
    }
    _last_login_status = login_get_status();
    if (_make_text_input_active)
    {
        gui_set_active_win(0);
        if (_account_name_buf[0])
            gui_set_active_text_input("Password");
        else
            gui_set_active_text_input("Name");
        _make_text_input_active = 0;
    }
}

static void
_main_menu_open(void)
{
    sprite_context_clear(core_sprite_context);
    _bg_sprite = create_sprite(core_sprite_context);
    sprite_set_loop(_bg_sprite, 1);
    _bg_anim_asset = as_claim_anim_asset_by_name("login bg", 1);
    if (_bg_anim_asset)
        sprite_play_anim(_bg_sprite, &_bg_anim_asset->anim);
    _offline_shard_button_style = *gui_get_default_button_style();
    uint8 color[4];
    memcpy(color, gui_get_default_button_style()->states[GUI_BUTTON_STATE_NORMAL].background_color, 4);
    memcpy(_offline_shard_button_style.states[GUI_BUTTON_STATE_HOVERED].background_color, color, 4);
    memcpy(_offline_shard_button_style.states[GUI_BUTTON_STATE_PRESSED].background_color, color, 4);
    /*-- Clear login buffers --*/
    _password_buf[0]    = 0;
    _login_music        = as_claim_sound_by_name("night1", 1);
    au_stop_channel(31);
    au_play_music_asset_on_channel(_login_music, 31, 1.0f, 1.0f, 1);
    _show_login_progress_win    = 0;
    _show_dc_reason_win         = login_get_last_error();
    if (!_show_dc_reason_win)
        _show_dc_reason_win = shard_get_last_error();
    if (strlen(core_config.account_name))
        strncpy(_account_name_buf, core_config.account_name,
            sizeof(_account_name_buf) - 1);
    else
        _account_name_buf[0] = 0;
    if (!_account_name_buf[0])
        gui_set_active_text_input("Name");
    else
        gui_set_active_text_input("Password");
    _last_login_status      = login_get_status();
    _make_text_input_active = 1;
    as_cursor_t *cursor = as_get_cursor("default");
    muta_assert(cursor);
    core_set_cursor(cursor->data);
}

static void
_main_menu_close(void)
{
    _account_name_buf[0] = 0;
    as_unclaim_anim_asset(_bg_anim_asset);
    as_unclaim_sound(_login_music);
}

static void
_main_menu_keydown(int key, bool32 is_repeat)
{
    switch (key)
    {
    case CORE_KEY_TAB:
        break;
    case CORE_KEY_BACKSPACE:
        break;
    case CORE_KEY_RETURN:
        if (_show_dc_reason_win)
        {
            _cancel_connection_attempt();
            _show_dc_reason_win     = 0;
            _make_text_input_active = 1;
        }
        break;
    case CORE_KEY_ESCAPE:
        if (!_show_dc_reason_win && !_show_login_progress_win)
            core_stop();
        else if (login_get_status() == LOGIN_STATUS_SELECTING_SHARD)
            login_disconnect();
        else if (login_get_status() == LOGIN_STATUS_SELECTED_SHARD)
            login_cancel_select_shard();
        else
        {
            _cancel_connection_attempt();
            _show_dc_reason_win     = 0;
            _make_text_input_active = 1;
        }
        break;
    case CORE_KEY_F1:
        _show_developer_options = _show_developer_options ? 0 : 1;
        break;
    }
}

static void
_try_connect(void)
{
    _show_login_progress_win = 1;
    int res = login_connect(_account_name_buf, _password_buf);
    if (res)
    {
        _show_dc_reason_win = 1;
        return;
    }
    strncpy(core_config.account_name, _account_name_buf, MAX_ACC_NAME_LEN);
    _last_login_status = login_get_status();
}

static void
_cancel_connection_attempt(void)
{
    login_disconnect();
    _show_login_progress_win = 0;
}

static void
_draw_login_info_fields(void)
{
    int w = (int)(0.28f * (float)core_asset_config.resolution_w);
    int h = (int)(0.16f * (float)w);
    gui_origin(GUI_CENTER_CENTER);
    gui_begin_guide(0, 0, w, h * 4);
    gui_origin(GUI_TOP_CENTER);
    gui_text_input_style(&gui_style_login_text_input);
    int font_h = gui_get_text_input_style_max_title_font_height(
        &gui_style_login_text_input);
    bool32 connect = 0;
    bool32 swapped = 0;
    if (gui_text_input("Name", _account_name_buf, sizeof(_account_name_buf), 0,
        0, w, h, 0) && core_key_down_now(CORE_KEY_TAB))
    {
        gui_set_active_text_input("Password");
        swapped = 1;
    }
    if (gui_text_input_enter_pressed())
        connect = 1;
    if (gui_text_input("Password", _password_buf, sizeof(_password_buf), 0,
        gui_get_last_text_input_y() + gui_get_last_text_input_h() + 2 * font_h,
        w, h, GUI_TEXT_INPUT_FLAG_PASSWORD) &&
        core_key_down_now(CORE_KEY_TAB) && !swapped)
        gui_set_active_text_input("Name");
    if (gui_text_input_enter_pressed())
        connect = 1;
    if (gui_button("Connect", 0,
        gui_get_last_text_input_y() + gui_get_last_text_input_h() + 2 * font_h,
            w / 2, (int)(0.12f * (float)w), 0) || connect)
        _try_connect();
    gui_end_guide();
}

static void
_draw_login_progress_win(void)
{
    gui_origin(GUI_CENTER_CENTER);
    gui_win_style(0);
    gui_begin_win("##Login progress", 0, 0, 256, 128, 0);
    const char *s;
    switch (login_get_status())
    {
        case LOGIN_STATUS_DISCONNECTED:
            s = "Disconnected";
            break;
        case LOGIN_STATUS_CONNECTING:
            s = "Connecting";
            break;
        case LOGIN_STATUS_HANDSHAKING:
            s = "Handshaking";
            break;
        case LOGIN_STATUS_SELECTING_SHARD:
        case LOGIN_STATUS_SELECTED_SHARD:
            s = "Connected";
            break;
        default:
            break;
    }
    gui_text(s, 0, 0, 0);
    if (gui_button("Cancel", 0, 40, 64, 32, 0))
    {
        _cancel_connection_attempt();
        _show_dc_reason_win = 0;
    }
    gui_end_win();
}

static void
_draw_shards_win(void)
{
    int win_w = (int)(0.8f * core_asset_config.resolution_w);
    int win_h = (int)(0.8f * core_asset_config.resolution_h);
    gui_win_style(0);
    gui_begin_win("Shards", 0, 0, win_w, win_h, 0);
    gui_origin(GUI_TOP_LEFT);
    gui_text("Shard selection", 0, 2, 2);
    int top_text_height = gui_get_last_text_y() + gui_get_last_text_h() + 2;
    int dc_button_w = (int)(0.15f * (float)gui_get_current_win_viewport_w());
    int dc_button_h = (int)(0.08f * (float)gui_get_current_win_viewport_h());
    int dc_button_xy = (int)(0.005f * (float)gui_get_current_win_viewport_h());
    int inner_win_w = gui_get_current_win_viewport_w();
    int inner_win_h = gui_get_current_win_viewport_h() - dc_button_h -
        top_text_height - 2 * dc_button_xy;
    gui_begin_empty_win("shards_inner", 0, top_text_height, inner_win_w,
        inner_win_h, GUI_WIN_SCROLLABLE);
    login_shard_info_t shard_infos[64];
    uint32 num_shards = login_get_shard_infos(shard_infos, 64);
    login_shard_info_t *si;
    gui_button_style_t *s;
    int shard_button_x = 2;
    int shard_button_w = gui_get_current_win_viewport_w() - shard_button_x * 2;
    int shard_button_h = (int)((float)gui_get_button_style_max_font_height(
        &_offline_shard_button_style) * 1.5f);
    int shard_button_spacing = 2;
    int shard_button_y = 0;
    for (uint32 i = 0; i < num_shards; ++i)
    {
        si  = &shard_infos[i];
        const char *online_text;
        if (si->online)
        {
            online_text = "Online";
            s           = 0;
        } else
        {
            online_text = "Offline";
            s           = &_offline_shard_button_style;
        }
        gui_button_style(s);
        if (gui_button(gui_format_id("%s##shard_button%u", si->name, i),
            shard_button_x, shard_button_y,
            shard_button_w, shard_button_h, 0) && si->online)
        {
            login_select_shard(i);
            strcpy(_selected_shard_name, si->name);
        }
        gui_text(online_text, 0, gui_get_last_button_x(),
            gui_get_last_button_y());
        shard_button_y += shard_button_h + shard_button_spacing;
    }
    if (shard_get_status() != SHARD_STATUS_DISCONNECTED)
    {
        gui_origin(GUI_CENTER_CENTER);
        int w = (int)(0.25f * (float)core_asset_config.resolution_w);
        int h = w / 2;
        gui_begin_win("##Shard Connect", 0, 0,
            w, h, 0);
        gui_origin(GUI_CENTER_CENTER);
        int fh = gui_get_default_font()->height;
        gui_textf("Connecting to %s", 0, 0, -fh / 2 - 1, _selected_shard_name);
        const char *status_string = 0;
        switch (shard_get_status())
        {
            case SHARD_STATUS_CONNECTING:
                status_string = "Connecting...";
                break;
            case SHARD_STATUS_HANDSHAKING:
                status_string = "Handshaking...";
                break;
            case SHARD_STATUS_CONNECTED:
                status_string = "Connected!";
                break;
            default:
                muta_assert(0);
        }
        gui_text(status_string, 0, 0,
            gui_get_last_text_y() + gui_get_last_text_h() + 2);
        gui_origin(GUI_BOTTOM_CENTER);
        int cancel_btn_h = (int)(0.1f * (float)w);
        if (gui_button("Cancel##Shard Connect", 0, h / 4 - cancel_btn_h,
            (int)(0.4f * (float)w), cancel_btn_h, 0))
            login_cancel_select_shard();
        gui_end_win();
    }
    gui_end_win();
    gui_origin(GUI_BOTTOM_RIGHT);
    if (gui_button("Disconnect##Shard Selection", dc_button_xy, dc_button_xy,
        dc_button_w, dc_button_h, 0) &&
        shard_get_status() != SHARD_STATUS_CONNECTED)
    {
        shard_disconnect();
        login_disconnect();
    }
    gui_end_win();
    gui_set_active_win("Shards");
}

static void
_draw_dc_reason_win(void)
{
    gui_begin_empty_win("dc frame", 0, 0, core_asset_config.resolution_w,
        core_asset_config.resolution_h, 0);
    gui_origin(GUI_CENTER_CENTER);
    gui_win_style(0);
    int win_w = (int)(0.27f * (float)core_asset_config.resolution_w);
    int win_h = win_w / 2;
    gui_begin_win("##Disconnect reason", 0, 0, win_w, win_h, 0);
    if (login_get_last_error())
        gui_text(login_error_to_string(login_get_last_error()), 0, 0, 0);
    else
        gui_text(shard_error_to_string(shard_get_last_error()), 0, 0, 0);
    int button_y = gui_get_last_text_y() + gui_get_last_text_h();
    gui_text("Disconnected:", 0, 0, -gui_get_last_text_h());
    int button_h = (int)(0.1875f * (float)win_h);
    if (gui_button("OK", 0, button_y + 2 + button_h / 2, 64, button_h, 0))
    {
        _show_dc_reason_win = 0;
        _make_text_input_active = 1;
    }
    gui_end_win();
    gui_end_win();
    gui_set_active_win("##Disconnect reason");
}

static inline bool32
_disconnected_this_frame(void)
{
    int status = login_get_status();
    return status == LOGIN_STATUS_DISCONNECTED && status != _last_login_status;
}
