#include "sprite_editor.h"
#include "core.h"
#include "render.h"
#include "../../shared/gui/gui.h"
#include "world.h"
#include "assets.h"
#include "main_menu_screen.h"
#include "../../shared/containers.h"

#define INPUT_FIELD_LIMIT 128

typedef struct editor_sprite_t  editor_sprite_t;
typedef editor_sprite_t         editor_sprite_darr_t;

struct editor_sprite_t
{
    char name[64];
    float clip[4];
};

enum gui_text_input_field_t
{
    CHOP_WIDTH = 0,
    CHOP_HEIGHT,
    CHOP_SPACE,
    CLIP0,
    CLIP1,
    CLIP2,
    CLIP3,
    NAME,
    PATH
};

static void
sprite_editor_update(double dt);

static void
sprite_editor_open(void);

static void
sprite_editor_close(void);

static void
sprite_editor_keydown(int key, bool32 is_repeat);

static void
sprite_editor_mousewheel(int x, int y);

static void
sprite_editor_mousebuttonup(uint8 button, int x, int y);

static void
sprite_editor_mousebuttondown(uint8 button, int x, int y);

static void
sprite_editor_text_input(const char *text);

static void
sprite_editor_file_drop(const char *path);

screen_t sprite_editor_screen =
{
    "Sprite Editor",
    0,
    0,
    sprite_editor_update,
    sprite_editor_open,
    sprite_editor_close,
    sprite_editor_text_input,
    sprite_editor_keydown,
    0,
    sprite_editor_mousebuttondown,
    sprite_editor_mousebuttonup,
    sprite_editor_mousewheel,
    sprite_editor_file_drop
};

static char _save_and_load_path[256];
static char _image_load_path[256];
static char _chop_width_buf[INPUT_FIELD_LIMIT];
static char _chop_height_buf[INPUT_FIELD_LIMIT];
static char _chop_spacing_buf[INPUT_FIELD_LIMIT];
static char _sprite_clip0_buf[INPUT_FIELD_LIMIT];
static char _sprite_clip1_buf[INPUT_FIELD_LIMIT];
static char _sprite_clip2_buf[INPUT_FIELD_LIMIT];
static char _sprite_clip3_buf[INPUT_FIELD_LIMIT];
static char _sprite_name_buf[INPUT_FIELD_LIMIT];
static char *_input_buf = 0;

static as_tex_t	*_sprite_sheet;

static editor_sprite_darr_t *editor_sprite_defs;

static int _selected_sprite;
static int _sprite_selector;
static int _input_buf_offset = 0;
static int _sprite_sheet_x, _sprite_sheet_y;
static int _manual_clip_win_id;
static int _rows;
static int _confirm_value;
static int _anchor_x, _anchor_y;
static int _tex_x, _tex_y;
static int _tempx, _tempy;
static int _sprite_indic;

int rows;

static double held_timer;

static float _tex_scale;
static float _tex_off_x, _tex_off_y;

static bool32 _sprite_sheet_loaded;
static bool32 _auto_chop_win;
static bool32 _manual_clip_win;
static bool32 _text_allowed;
static bool32 _can_write;
static bool32 _is_sprite_selected;
static bool32 _type_save_name;
static bool32 _help_window_toggle;

static uint8 quad_col[4];
static uint8 black[4];

static int
_read_sprite_defs(const char *fp);

static int
_save_sprite_defs(const char *fp);

static void
_select_sprite();

static void
_auto_chop(int width, int height, int spacing);

static void
_draw_rect(int x, int y, int w, int h, uint8 col[4]);

static void
_confirm_action(char *txt, void(*yes_answer)());

static void
_main_menu();

static void
_create_empty_file_format_for_sheet();

static void
_select_what_file_to_load(char *txt);

static void
_create_new_clip();

static void
_delete_clip();

static void
_start_drag();

static void
_dragging();

static void
_help_window();

void
sprite_editor_update(double dt)
{
    gui_font(as_get_default_font());
    static int viewport[4];
    core_compute_target_viewport(viewport);
    gl_viewport(viewport[0], viewport[1], viewport[2], viewport[3]);
    gl_scissor(viewport[0], viewport[1], viewport[2], viewport[3]);
    gl_color(0.2f, 0.3f, 0.8f, 1.0f);
    gl_clear(RB_CLEAR_COLOR_BIT);

    gui_origin(GUI_TOP_LEFT);

    char buf[256];


    rows = CLAMP((int)darr_num(editor_sprite_defs), 0, _rows);


    if (_sprite_sheet_loaded)
    {
        _tex_off_x = (_sprite_sheet->tex.w / 2) * _tex_scale;
        _tex_off_y = (_sprite_sheet->tex.h / 2) * _tex_scale;
        _select_sprite();
        gui_texture_s(&_sprite_sheet->tex, 0,(int)( _tex_x - _tex_off_x),
            (int)(_tex_y - _tex_off_y), _tex_scale, _tex_scale);
        float x, y, w, h;
        x = editor_sprite_defs[_selected_sprite].clip[0] * _tex_scale;
        y = editor_sprite_defs[_selected_sprite].clip[1] * _tex_scale;
        w = (editor_sprite_defs[_selected_sprite].clip[2] * _tex_scale
            - x);
        h = (editor_sprite_defs[_selected_sprite].clip[3] * _tex_scale
            - y);
        if (_manual_clip_win)
        _draw_rect((int)(x + (_tex_x - _tex_off_x)), 
            (int)(y + (_tex_y - _tex_off_y)), (int)w, (int)h, black);

        gui_origin(GUI_BOTTOM_CENTER);
        gui_text_s("TEXTURE SCALE", 0, 0, 90, 2);
        if (gui_button("+", -50, 50, 30, 30, 0) || (held_timer >= 0.25
            && gui_invisible_repeat_button("+", -50, 50, 30, 30, 0)))
        {
            if(_tex_scale < 4)
                _tex_scale += 0.01f;
        }
        if (gui_button("-", 50, 50, 30, 30, 0) || (held_timer >= 0.25
            && gui_invisible_repeat_button("-", 50, 50, 30, 30, 0)))
        {
            if(_tex_scale > 0)
                _tex_scale -= 0.01f;
        }
        gui_origin(GUI_TOP_LEFT);
    }

    /* AUTO CHOP WINDOW FOR SPRITE SHEET */
    if (_auto_chop_win && _sprite_sheet_loaded)
    {
        gui_begin_win("clipping", 300, core_asset_config.resolution_h - 200, 300, 150, 0);

        if (gui_text_input("Columns", _chop_width_buf,
            sizeof(_chop_width_buf), 60, 20, 50, 20, 0))
            _input_buf = _chop_width_buf;

        if (gui_text_input("Rows", _chop_height_buf, sizeof(_chop_height_buf),
            60, 64, 50, 20, 0))
            _input_buf = _chop_height_buf;

        gui_text("SPACING: ", 0, 10, 108);
        if (gui_text_input("Spacing", _chop_spacing_buf,
            sizeof(_chop_spacing_buf), 60, 108, 50, 20, 0))
            _input_buf = _chop_spacing_buf;

        if (gui_button("CHOP", 120, 70, 128, 36, 0))
        {
            _input_buf = 0;
            int width = atoi(_chop_width_buf);
            int height = atoi(_chop_height_buf);
            int space = atoi(_chop_spacing_buf);
            if (width > 0 && height > 0)
            {
                _auto_chop(width, height, space);
                _auto_chop_win = 0;
            }
        }
        gui_end_win();
    }

    /* MANUAL CLIP AND NAME FOR ONE SPRITE */
    if (_manual_clip_win)
    {
        if (!_can_write)
        {
            stbsp_sprintf(buf, "%s",
                editor_sprite_defs[_selected_sprite].name);
            strcpy(_sprite_name_buf, buf);
            stbsp_sprintf(buf, "%d",
                (int)editor_sprite_defs[_selected_sprite].clip[0]);
            strcpy(_sprite_clip0_buf, buf);
            stbsp_sprintf(buf, "%d",
                (int)editor_sprite_defs[_selected_sprite].clip[1]);
            strcpy(_sprite_clip1_buf, buf);
            stbsp_sprintf(buf, "%d",
                (int)editor_sprite_defs[_selected_sprite].clip[2]);
            strcpy(_sprite_clip2_buf, buf);
            stbsp_sprintf(buf, "%d",
                (int)editor_sprite_defs[_selected_sprite].clip[3]);
            strcpy(_sprite_clip3_buf, buf);
        }

        gui_origin(GUI_TOP_LEFT);
        _manual_clip_win_id = gui_begin_empty_win("man clip",
            core_asset_config.resolution_w - 300, core_asset_config.resolution_h / 3, 300, 210, 0);

        if (gui_get_active_win_id() == _manual_clip_win_id)
        {
            _can_write = 1;
        }	else
        {
            _text_allowed = 0;
            _can_write = 0;
        }

        gui_text("CLIP[0] : ", 0, 10, 20);
        gui_text("CLIP[1] : ", 0, 10, 64);
        gui_text("CLIP[2] : ", 0, 10, 108);
        gui_text("CLIP[3] : ", 0, 10, 152);
        gui_text("NAME : ", 0, 130, 20);

        if (gui_text_input("NAME: ", _sprite_name_buf, sizeof(_sprite_name_buf),
             130, 40, 100, 20, 0))
        {
            _text_allowed = 1;
            _input_buf = _sprite_name_buf;
        }
        if (gui_text_input("CLIP[0]", _sprite_clip0_buf,
            sizeof(_sprite_clip0_buf), 55, 20, 50, 20, 0))
        {
            _text_allowed = 0;
            _input_buf = _sprite_clip0_buf;
        }
        if (gui_text_input("CLIP[1]", _sprite_clip1_buf,
            sizeof(_sprite_clip1_buf), 55, 64, 50, 20, 0))
        {
            _text_allowed = 0;
            _input_buf = _sprite_clip1_buf;
        }
        if (gui_text_input("CLIP[2]", _sprite_clip2_buf,
            sizeof(_sprite_clip2_buf), 55, 108, 50, 20, 0))
        {
            _text_allowed = 0;
            _input_buf = _sprite_clip2_buf;
        }
        if (gui_text_input("CLIP[3]", _sprite_clip3_buf,
            sizeof(_sprite_clip3_buf), 55, 152, 50, 20, 0))
        {
            _text_allowed = 0;
            _input_buf = _sprite_clip3_buf;
        }
        if (gui_button("SET CLIP", 120, 100, 128, 36, 0))
        {
            _input_buf = 0;
            strcpy(editor_sprite_defs[_selected_sprite].name,
                _sprite_name_buf);

            editor_sprite_defs[_selected_sprite].clip[0] =
                (float)atoi(_sprite_clip0_buf);
            editor_sprite_defs[_selected_sprite].clip[1] =
                (float)atoi(_sprite_clip1_buf);
            editor_sprite_defs[_selected_sprite].clip[2] =
                (float)atoi(_sprite_clip2_buf);
            editor_sprite_defs[_selected_sprite].clip[3] =
                (float)atoi(_sprite_clip3_buf);
        }
        gui_end_win();
    }
    
    /* UI THINGS */

    if (gui_button("AUTO CHOP", core_asset_config.resolution_w - 200, 800, 100, 50, 0))
    {
        if (_sprite_sheet_loaded)
        {
            strcpy(_chop_width_buf, "");
            strcpy(_chop_height_buf, "");
            strcpy(_chop_spacing_buf, "");
            _auto_chop_win = _auto_chop_win ? 0 : 1;
        }
    }
    if (gui_button("SAVE", core_asset_config.resolution_w - 200, 860, 100, 50, 0))
    {
        _confirm_value = 4;

    }
    if (gui_button("CREATE NEW CLIP", core_asset_config.resolution_w - 200, 740, 100, 50, 0))
    {
        if (_sprite_sheet_loaded)
        {
            _create_new_clip();
        }
    }
    if (gui_button("DELETE CLIP", core_asset_config.resolution_w - 200, 680, 100, 50, 0))
    {
        if(_is_sprite_selected)
        _delete_clip();
    }

    /* OTHER STUFF */
    if (gui_is_any_button_pressed())
        held_timer += dt;
    else
    {
        held_timer = 0;
        _dragging();
    }
        

    if (_confirm_value != 0)
    {
        switch (_confirm_value)
        {
        case 1:
        {
            _confirm_action("Are you sure you want to exit? "
                "All unsaved changes will be lost", _main_menu);
        } break;
        case 2:
        {
            _confirm_action("File name does not exist. "
                "Do you wanna create it automaticly?",
                _create_empty_file_format_for_sheet);
        } break;
        case 3:
        {
            _select_what_file_to_load("From what file you want "
                "to load spritesheet data");
        } break;
        case 4:
        {
            _select_what_file_to_load("Insert filename you want "
                "to save all data");
        } break;
        case 99:
        {
            DEBUG_PRINTF("\n\nUNEXPECTED ERROR!!\n\n\n");
        } break;
        }
    }
    if (_help_window_toggle)
    {
        _help_window();
    }
}

void
sprite_editor_open(void)
{
    /* UINT8 */
    quad_col[0]				= 0;
    quad_col[1]				= 0;
    quad_col[2]				= 0;
    quad_col[3]				= 128;

    black[0]				= 0;
    black[1]				= 0;
    black[2]				= 0;
    black[3]				= 255;

    /* BOOLS */
    _sprite_sheet_loaded	= 0;
    _auto_chop_win			= 0;
    _text_allowed			= 0;
    _can_write				= 0;
    _is_sprite_selected		= 0;
    _type_save_name			= 0;
    _help_window_toggle		= 0;

    /* INTS */
    _sprite_sheet_x			= core_asset_config.resolution_w / 2;
    _sprite_sheet_y			= core_asset_config.resolution_h / 2;
    _tex_x = core_asset_config.resolution_w / 2;
    _tex_y = core_asset_config.resolution_h / 2;
    _manual_clip_win_id		= 0;
    _confirm_value			= 0;
    _anchor_x				= -1;
    _anchor_y				= -1;
    _tempx					= -1;
    _tempy					= -1;
    _rows					= 13;
    _sprite_indic			= 0;

    /* FLOATS */
    _tex_scale				= 1.0f;

    darr_reserve(editor_sprite_defs, 256);
}

void
sprite_editor_close(void)
{
    darr_free(editor_sprite_defs);
}

void
sprite_editor_keydown(int key, bool32 is_repeat)
{
    switch (key)
    {
    case CORE_KEY_ESCAPE:
    {
        _confirm_value = 1;
    } break;
    case CORE_KEY_BACKSPACE:
    {
        if (_auto_chop_win || _can_write ||_type_save_name)
        {
            _input_buf_offset = (int)strlen(_input_buf);
            if (_input_buf_offset > 0) _input_buf[_input_buf_offset - 1] = 0;
        }
    } break;
    case CORE_KEY_A:
    {
        if(!_auto_chop_win && !_can_write && !_type_save_name)
        {
            if (_manual_clip_win)
            {
                --editor_sprite_defs[_selected_sprite].clip[0];
                --editor_sprite_defs[_selected_sprite].clip[2];
            }
        }
    } break;
    case CORE_KEY_D:
    {
        if (!_auto_chop_win && !_can_write && !_type_save_name)
        {
            if (_manual_clip_win)
            {
                ++editor_sprite_defs[_selected_sprite].clip[0];
                ++editor_sprite_defs[_selected_sprite].clip[2];
            }
        }
    } break;
    case CORE_KEY_W:
    {
        if (!_auto_chop_win && !_can_write && !_type_save_name)
        {
            if (_manual_clip_win)
            {
                --editor_sprite_defs[_selected_sprite].clip[1];
                --editor_sprite_defs[_selected_sprite].clip[3];
            }
        }
    } break;
    case CORE_KEY_S:
    {
        if (!_auto_chop_win && !_can_write && !_type_save_name)
        {
            if (_manual_clip_win)
            {
                ++editor_sprite_defs[_selected_sprite].clip[1];
                ++editor_sprite_defs[_selected_sprite].clip[3];
            }
        }
    } break;
    case CORE_KEY_UP:
    {
        if (!_auto_chop_win && !_can_write && !_type_save_name)
        {
            if (_manual_clip_win)
                --editor_sprite_defs[_selected_sprite].clip[3];
        }
    } break;
    case CORE_KEY_DOWN:
    {
        if (!_auto_chop_win && !_can_write && !_type_save_name)
        {
            if (_manual_clip_win)
                ++editor_sprite_defs[_selected_sprite].clip[3];
        }
    } break;
    case CORE_KEY_LEFT:
    {
        if (!_auto_chop_win && !_can_write && !_type_save_name)
        {
            if(_manual_clip_win)
                --editor_sprite_defs[_selected_sprite].clip[2];
        }
    } break;
    case CORE_KEY_RIGHT:
    {
        if (!_auto_chop_win && !_can_write && !_type_save_name)
        {
            if(_manual_clip_win)
                ++editor_sprite_defs[_selected_sprite].clip[2];
        }
    } break;
    case CORE_KEY_H:
    {
        if (!_can_write && !_type_save_name)
            _help_window_toggle = _help_window_toggle ? 0 : 1;
    } break;
    }
}

void
sprite_editor_mousebuttonup(uint8 button, int x, int y)
{
    if (_anchor_x != -1 && _anchor_y != -1)
    {
        _anchor_x = _anchor_y = -1;
        _tempx = _tempy = -1;
    }
}

void
sprite_editor_mousebuttondown(uint8 button, int x, int y)
{
    if (_anchor_x == -1 && _anchor_y == -1)
    {
        _anchor_x = x;
        _anchor_y = y;
        core_compute_pos_scaled_by_target_viewport(&_anchor_x, &_anchor_y);
        _start_drag();
    }
}

void
sprite_editor_mousewheel(int x, int y)
{
    _sprite_selector -= y;
    if (_sprite_selector < 0)
        _sprite_selector = (int)darr_num(editor_sprite_defs) - 1;
    if (_sprite_selector >(int)darr_num(editor_sprite_defs) - 1)
        _sprite_selector = 0;

    _sprite_indic += y;
    if (_sprite_indic < 0)
        _sprite_indic = (int)darr_num(editor_sprite_defs) - 1;
    if (_sprite_indic > (int)darr_num(editor_sprite_defs) - 1)
        _sprite_indic = 0;
}

void
sprite_editor_text_input(const char *text)
{
    char buf[256];

    if (_auto_chop_win || _can_write || _type_save_name)
    {
        _input_buf_offset = (int)strlen(_input_buf);
        if (_input_buf_offset < INPUT_FIELD_LIMIT - 1)
        {
            strcpy(buf, text);
            if (!_text_allowed)
                str_strip_non_numbers(buf);
            else
                strip_illegal_chat_msg_symbols(buf);
            strncat(_input_buf, buf, sizeof(_input_buf) - 1);
        }
    }
}

void
sprite_editor_file_drop(const char *path)
{
    _manual_clip_win = 0;
    _sprite_selector = 0;
    _is_sprite_selected = 0;
    _tex_scale = 1.0f;
    darr_clear(editor_sprite_defs);

    char temp[256];
    char read_path[256];
    strcpy(temp, path);
    char *base_path = SDL_GetBasePath();

    while (temp == strstr(temp, base_path))
        memmove(temp, temp + strlen(base_path),
            1 + strlen(temp + strlen(base_path)));
    char file[256];
    strcpy(file, path);

    strcpy(_image_load_path, temp);
    int loop = (int)sizeof(_image_load_path);
    for (int i = 0; i < loop; ++i)
    {
        if (_image_load_path[i] == '\\')
            _image_load_path[i] = '/';
    }

    char *file_extension;
    file_extension = strtok(temp, ".");
    if (!file_extension)
        goto clean;

    stbsp_snprintf(read_path, 256, "%s.ss", temp);
    strcpy(_save_and_load_path, read_path);
    file_extension = strtok(0, ".");

    if (!file_extension || str_insensitive_cmp(file_extension, "png"))
        goto clean;

    as_tex_t *tex = 0;
    FIXME();
    if (tex)
    {
        _confirm_value = 3;
        _sprite_sheet = tex;
    }
    return;
    clean:
        DEBUG_PRINTF("'%s' is not a valid file for sprite editor\n", file);
}

static int
_read_sprite_defs(const char *fp)
{
    editor_sprite_t *def;

    int ret = 0;
    FILE *f = fopen(fp, "r");
    if (!f) { ret = 1; goto cleanup; }

    const int line_len = 256;
    char line[256];
    int index = 0;
    int num_clips = 0;

    char name[64];
    float tmpf;
    float clip[4];

    while (fgets(line, line_len, f))
    {
        if (sscanf(line, "%63s %f %f %f %f", name,
            &tmpf, &tmpf, &tmpf, &tmpf) == 5)
        {
            editor_sprite_t tmp = { 0 };
            darr_reserve(editor_sprite_defs, (uint32)(num_clips + 1));
            editor_sprite_defs[num_clips] = tmp;
            ++num_clips;
        }
    }
    rewind(f);

    while (fgets(line, line_len, f))
    {
        def = &editor_sprite_defs[index];
        if (def)
        {
            if (sscanf(line, "%63s %f %f %f %f", name,
                &clip[0], &clip[1], &clip[2], &clip[3]) == 5)
            {
                strcpy(def->name, name);
                def->clip[0] = clip[0];
                def->clip[1] = clip[1];
                def->clip[2] = clip[2];
                def->clip[3] = clip[3];
                index++;
            }
            else
            {
                DEBUG_PRINTF("%s: scanning clip[%d] to spritesheet failed\n",
                    __func__, index);
            }
        }
    }

cleanup:;
    safe_fclose(f);
    return ret;
}

static int
_save_sprite_defs(const char *fp)
{
    FILE *f = fopen(fp, "w+");

    if (!f)
        return 1;
    for (int i = 0; i < (int)darr_num(editor_sprite_defs); ++i)
    {
        if (i == 0)
            fprintf(f, "PATH: %s\n", _image_load_path);

        if (strlen(editor_sprite_defs[i].name) == 0)
            strcpy(editor_sprite_defs[i].name, "Empty");

        fprintf(f, "%s %d %d %d %d\n", editor_sprite_defs[i].name,
            (int)editor_sprite_defs[i].clip[0],
            (int)editor_sprite_defs[i].clip[1],
            (int)editor_sprite_defs[i].clip[2],
            (int)editor_sprite_defs[i].clip[3]);
    }
    safe_fclose(f);
    return 0;
}

static void
_select_sprite()
{
    int index;
    char buf[256];
    static uint8 white[4] = {255, 255, 255, 255};
    gui_color(quad_col[0], quad_col[1], quad_col[2], quad_col[3]);
    gui_rectangle(0, 0, 150, core_asset_config.resolution_h);
    gui_color(255, 255, 255, 255);

    for (int i = 0; i < rows; ++i)
    {
        _is_sprite_selected = 1;
        index = _sprite_selector + i;
        if (index >(int)darr_num(editor_sprite_defs) - 1)
            index -= darr_num(editor_sprite_defs);

        stbsp_snprintf(buf, 128, "%i. %s", index,
            editor_sprite_defs[index].name);

        if (gui_button(buf, 25, 50 + 80 * i, 100, 40, 0))
        {
            _sprite_indic = i;
            _manual_clip_win = 1;
            _selected_sprite = index;
        }
    }
    if (_manual_clip_win)
    {
        _draw_rect(25, 50 + 80 * _sprite_indic, 100, 40, white);
    }
}

static void
_auto_chop(int vertical, int horizontal, int spacing)
{
    int width = (int)(_sprite_sheet->tex.w / vertical);
    int height = (int)(_sprite_sheet->tex.h / horizontal);
    int index = 0;
    int clip[4];
    editor_sprite_t *def;

    for (int j = 0; j < horizontal; j++)
    {
        for (int i = 0; i < vertical; i++)
        {
            clip[0] = (i * width) + (spacing * i * 2) + spacing;
            clip[1] = (j * height) + (spacing * j * 2) + spacing;
            clip[2] = (width + (width * i)) + (spacing * i * 2) + spacing;
            clip[3] = (height + (height * j)) + (spacing * j * 2) + spacing;
            if ((uint32)index < darr_num(editor_sprite_defs))
            {
                def = &editor_sprite_defs[index];
                def->clip[0] = (float)clip[0];
                def->clip[1] = (float)clip[1];
                def->clip[2] = (float)clip[2];
                def->clip[3] = (float)clip[3];
            } else
            {
                darr_reserve(editor_sprite_defs, (uint32)(index + 1));
                editor_sprite_defs[index].clip[0] = (float)clip[0];
                editor_sprite_defs[index].clip[1] = (float)clip[1];
                editor_sprite_defs[index].clip[2] = (float)clip[2];
                editor_sprite_defs[index].clip[3] = (float)clip[3];
            }
            ++index;
        }
    }
}

static void
_draw_rect(int x, int y, int w, int h, uint8 col[4])
{
    gui_color(col[0], col[1], col[2], col[3]);
    gui_rectangle(x - 2, y - 2, w + 4, 2);
    gui_rectangle(x - 2, y, 2, h + 2);
    gui_rectangle(x, y + h, w + 2, 2);
    gui_rectangle(x + w, y, 2, h + 2);
    gui_color(255, 255, 255, 255);
}

static void
_confirm_action(char *txt, void(*yes_answer)())
{
    gui_origin(GUI_TOP_CENTER);
    gui_begin_win("confirm", 0, core_asset_config.resolution_h / 3, 500, 250, 0);
    gui_text(txt, 0, 0, 30);
    if (gui_button("YES", -60, 70, 80, 40, 0))
    {
        _confirm_value = 0;
        yes_answer();
    }
    if (gui_button("NO", 60, 70, 80, 40, 0))
        _confirm_value = 0;
    gui_end_win();
}

static void
_main_menu()
{
    core_set_screen(&main_menu_screen);
}

static void
_create_empty_file_format_for_sheet()
{
    FILE *f = fopen(_save_and_load_path, "w+");
    if (!f)
        _confirm_value = 99;
    fprintf(f, "PATH: %s\n%s %d %d %d %d\n", _image_load_path, "Empty", 0, 0,
        (int)_sprite_sheet->tex.w,
        (int)_sprite_sheet->tex.h);

    safe_fclose(f);

    _read_sprite_defs(_save_and_load_path);
    _sprite_sheet_loaded = 1;
}

static void
_select_what_file_to_load(char *txt)
{
    gui_origin(GUI_TOP_CENTER);
    gui_begin_win("asdf", 0, core_asset_config.resolution_h / 3, 500, 250, 0);
    gui_text(txt, 0, 0, 5);
    if (gui_text_input("Path", _save_and_load_path, sizeof(_save_and_load_path),
        0, 30, 300, 20, 0))
    {
        _type_save_name = 1;
        _text_allowed = 1;
        _input_buf = _save_and_load_path;
    }

    if (_confirm_value == 3)
    {
        if (gui_button("LOAD", 0, 100, 50, 25, 0))
        {
            strtok(_save_and_load_path, ".");

            stbsp_snprintf(_save_and_load_path, 256, "%s.ss",
                _save_and_load_path);

            _type_save_name = 0;
            _text_allowed = 0;

            if (!_read_sprite_defs(_save_and_load_path))
            {
                _confirm_value = 0;
                _sprite_sheet_loaded = 1;
            }
            else
                _confirm_value = 2;
        }
    }
    if (_confirm_value == 4)
    {
        if (gui_button("SAVE", 0, 100, 50, 25, 0))
        {
            strtok(_save_and_load_path, ".");
            stbsp_snprintf(_save_and_load_path, 256, "%s.ss",
                _save_and_load_path);

            _type_save_name = 0;
            _text_allowed = 0;
            if (!_save_sprite_defs(_save_and_load_path))
            {
                _confirm_value = 0;
            }
            else
                _confirm_value = 99;

        }
    }

    gui_end_win();
}

static void
_create_new_clip()
{
    _is_sprite_selected = 1;
    darr_push_empty(editor_sprite_defs);
    strcpy(editor_sprite_defs[darr_num(editor_sprite_defs) - 1].name, "Empty");
    editor_sprite_defs[darr_num(editor_sprite_defs) - 1].clip[0] = 0;
    editor_sprite_defs[darr_num(editor_sprite_defs) - 1].clip[1] = 0;
    editor_sprite_defs[darr_num(editor_sprite_defs) - 1].clip[2] = 0;
    editor_sprite_defs[darr_num(editor_sprite_defs) - 1].clip[3] = 0;

    _selected_sprite = darr_num(editor_sprite_defs) - 1;
    _sprite_selector = darr_num(editor_sprite_defs) - _rows;
    if (_sprite_selector < 0)
        _sprite_selector = 0;
    _manual_clip_win = 1;

    _sprite_indic = rows - 1;
}

static void
_delete_clip()
{
    if (_manual_clip_win && darr_num(editor_sprite_defs) - 1 >= 1)
    {

        darr_erase(editor_sprite_defs, _selected_sprite);

        if (_selected_sprite == (int)darr_num(editor_sprite_defs))
            --_selected_sprite;

        if (_sprite_selector == (int)darr_num(editor_sprite_defs) -
            _sprite_indic)
        {
            if(_sprite_selector > 0)
            --_sprite_selector;
        }
        if (_sprite_indic > (int)darr_num(editor_sprite_defs) - 1)
        {
            --_sprite_indic;
        }
    }
}

static void
_start_drag()
{
    if (_sprite_sheet_loaded)
    {
        float clipw = _sprite_sheet->tex.w * _tex_scale - _tex_off_x;
        float cliph = _sprite_sheet->tex.h *_tex_scale - _tex_off_y ;
        if (_anchor_x >= _tex_x - _tex_off_x && _anchor_x <= _tex_x + clipw)
        {
            if (_anchor_y >= _tex_y - _tex_off_y && _anchor_y <= _tex_y + cliph)
            {
                _tempx = _tex_x;
                _tempy = _tex_y;
            }
        }
    }
}

static void
_dragging()
{
    if (_anchor_x == -1 || _anchor_y == -1 ||
        _tempx == -1 || _tempy == -1)
        return;

    int mx = core_mouse_x();
    int my = core_mouse_y();
    core_compute_pos_scaled_by_target_viewport(&mx, &my);

    int x = mx - _anchor_x;
    int y = my - _anchor_y;
    _tex_x = _tempx + x;
    _tex_y = _tempy + y;
}

static void
_help_window()
{
    gui_origin(GUI_TOP_CENTER);
    gui_begin_win("HELP", 0, 0, core_asset_config.resolution_w, core_asset_config.resolution_h, 0);

    gui_text("HELP WINDOW: ", 0, 0, 0);

    gui_text("Escape: Shuts down editor. NOTE!", 0, 0, 20);
    gui_text("WASD: Moves manually selected sprites clip. "
        "Text fields can't be active at the time", 0, 0, 60);
    gui_text("Down arrow: Increases selected sprite clip height", 0, 0, 80);
    gui_text("Up arrow: Decreases selected sprite clip height", 0, 0, 100);
    gui_text("Left arrow: Decreases selected sprite clip width", 0, 0, 120);
    gui_text("Right arrow: Increases selected sprite  clip width", 0, 0, 140);
    gui_text("Using keyboard to change clip info, text fields "
        "can't be active.", 0, 0, 180);
    gui_text("Mouse scroll: Scrolls sprites on left bar", 0, 0, 220);
    gui_text("Click spritesheet and drag: Moves the sheet around", 0, 0, 260);
    gui_text("AUTO CHOP: Automatically cuts spritesheet into clips. "
        "Insert how many rows and columns", 0, 0, 300);
    gui_text("SAVE: Saves all changes to customformat file.", 0, 0, 340);
    gui_text("DELETE CLIP: Deletes currently selected tile", 0, 0, 380);
    gui_text("CREATE NEW CLIP: Creates new empty sprite", 0, 0, 420);

    gui_text("Drag 'n drop the sprite sheet you want to work with", 0, 0, 460);
    gui_text("Editor will ask you the file name where you want to load "
        "spritesheet information. If file does not exist, "
        "it creates it for you", 0, 0, 500);
    gui_text("No need to enter .ss file format either on loading or saving. "
        "Editor adds it automaticly if missing", 0, 0, 540);
    gui_text("GIVE FEEDBACK!!", 0, 0, 760);

    gui_end_win();
}
