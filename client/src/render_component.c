#include "render_component.h"
#include "mobility_component.h"
#include "world.h"

#define SPRITES_PER_ENTITY 2

struct render_component_sprite_t
{
    entity_t    *entity;
    uint32      next;   /* 0xFFFFFFFF if none */
    uint32      cmd;    /* 0xFFFFFFFF if none */
    tex_t       *texture;
    float       clip[4];
    float       offset[2];
    int         last_tile_position[3];
    float       percentage_travelled; /* Always relevant */
};

struct render_component_t
{
    entity_t    *entity;
    uint32      sprites; /* 0xFFFFFFFF if none */
    uint32      reserved_index;
    uint8       culled;
    /* Whether the component's sprites are in range for drawing or not. */
};

static int
_render_system_init(world_t *world, uint32 max_components);

static void
_render_system_destroy(world_t *world);

static component_handle_t
_render_component_attach(entity_t *entity);

static void
_render_component_detach(component_handle_t handle);

static void
_handle_entity_set_position_event(entity_event_t *event);

static void
_handle_mobility_component_event(entity_t *entity, component_handle_t handle,
    void *event_ptr);

static void
_handle_world_event_load_map(world_event_t *event, void *user_data);

static bool32
_should_cull(int *cull_area, int *position);

static void
_delete_cmd(render_component_sprite_t *sprite, render_system_t *system);

static void
_fill_cmd(render_component_sprite_t *sprite, render_component_cmd_t *cmd,
    render_system_t *system);

static inline render_component_t *
_get_component_of_sprite(render_component_sprite_t *sprite);

static entity_event_interest_t _entity_event_interests[] =
{
    {ENTITY_EVENT_SET_POSITION, _handle_entity_set_position_event}
};

static component_event_interest_t _component_event_interests[] =
{
    {&mobility_component_definition, _handle_mobility_component_event}
};

component_definition_t render_component_definition =
{
    _render_system_init,
    _render_system_destroy,
    0,
    _render_component_attach,
    _render_component_detach,
    _entity_event_interests,
    sizeof(_entity_event_interests) / sizeof(entity_event_interest_t),
    _component_event_interests,
    sizeof(_component_event_interests) / sizeof(component_event_interest_t)
};

void
render_system_set_cull_area(render_system_t *system, int area[6])
{
    bool32 refresh = memcmp(system->cull_area, area, 6 * sizeof(int));
    if (!refresh)
        return;
    for (int i = 0; i < 6; ++i)
        system->cull_area[i] = area[i];
    uint32 *indices     = system->reserved;
    uint32 num_indices  = system->components.max - system->components.num_free;
    for (uint32 i = 0; i < num_indices; ++i)
    {
        render_component_t *component = &system->components.all[indices[i]];
        uint32 sprite_index = component->sprites;
        if (sprite_index == 0xFFFFFFFF)
            continue;
        int position[3];
        entity_get_position(component->entity, position);
        bool32 was_culled   = component->culled;
        bool32 will_cull    = _should_cull(area, position);
        if (was_culled == will_cull)
            continue;
        if (was_culled)
        {
            /*-- Make visible --*/
            while (sprite_index != 0xFFFFFFFF)
            {
                render_component_sprite_t *sprite =
                    &system->sprites.all[sprite_index];
                muta_assert(sprite->cmd == 0xFFFFFFFF);
                sprite_index    = sprite->next;
                if (!sprite->texture)
                    continue;
                sprite->cmd     = system->num_cmds++;
                _fill_cmd(sprite, &system->cmds[sprite->cmd], system);
            }
            component->culled = 0;
        } else
        {
            /*-- Make invisible --*/
            while (sprite_index != 0xFFFFFFFF)
            {
                render_component_sprite_t *sprite =
                    &system->sprites.all[sprite_index];
                sprite_index = sprite->next;
                if (sprite->cmd != 0xFFFFFFFF)
                    _delete_cmd(sprite, system);
            }
            component->culled = 1;
        }
    }
}

bool32
render_component_is_culled(component_handle_t handle)
{
    render_component_t *component = handle;
    return component->culled;
}

render_component_sprite_t *
render_component_new_sprite(component_handle_t handle)
{
    render_component_t *component = handle;
    render_system_t *system =
        &entity_get_world(component->entity)->render_system;
    render_component_sprite_t *sprite = fixed_pool_new(&system->sprites);
    if (!sprite)
        return 0;
    sprite->entity      = component->entity;
    sprite->next        = 0xFFFFFFFF;
    sprite->cmd         = 0xFFFFFFFF;
    sprite->texture     = 0;
    for (int i = 0; i < 4; ++i)
        sprite->clip[i] = 0;
    entity_get_position(component->entity, sprite->last_tile_position);
    uint32 *sprite_index = &component->sprites;
    while (*sprite_index != 0xFFFFFFFF)
    {
        render_component_sprite_t *other_sprite =
            &system->sprites.all[*sprite_index];
        sprite_index = &other_sprite->next;
    }
    *sprite_index = fixed_pool_index(&system->sprites, sprite);
    return sprite;
}

void
render_component_sprite_free(render_component_sprite_t *sprite)
{
    render_component_t *component = entity_get_component(sprite->entity,
        &render_component_definition);
    render_system_t *system = &entity_get_world(sprite->entity)->render_system;
    /*-- Delete cmd --*/
    if (sprite->cmd != 0xFFFFFFFF)
    {
        system->cmds[sprite->cmd] = system->cmds[--system->num_cmds];
        render_component_sprite_t *other_sprite =
            &system->sprites.all[system->cmds[sprite->cmd].sprite];
        other_sprite->cmd   = sprite->cmd;
        sprite->cmd         = 0xFFFFFFFF;
    }
    /*-- Delete from linked list --*/
    uint32 index        = fixed_pool_index(&system->sprites, sprite);
    uint32 *index_ptr   = &component->sprites;
    while (*index_ptr != index)
    {
        muta_assert(*index_ptr != 0xFFFFFFFF);
        render_component_sprite_t *other_sprite =
            &system->sprites.all[*index_ptr];
        index_ptr = &other_sprite->next;
    }
    *index_ptr = sprite->next;
    fixed_pool_free(&system->sprites, sprite);
}

void
render_component_sprite_set_texture(render_component_sprite_t *sprite,
    tex_t *texture)
{
    render_system_t *system = &entity_get_world(sprite->entity)->render_system;
    render_component_cmd_t *cmd;
    sprite->texture = texture;
    if (texture)
    {
        if (sprite->cmd == 0xFFFFFFFF)
        {
            int position[3];
            entity_get_position(sprite->entity, position);
            if (_get_component_of_sprite(sprite)->culled)
                return;
            /*-- Create new cmd --*/
            sprite->cmd = system->num_cmds++;
            cmd         = &system->cmds[sprite->cmd];
            _fill_cmd(sprite, cmd, system);
        } else
            cmd = &system->cmds[sprite->cmd]; /* Modify existing cmd */
        cmd->texture = texture;
    } else if (sprite->cmd != 0xFFFFFFFF)
        _delete_cmd(sprite, system);
}

void
render_component_sprite_set_clip(render_component_sprite_t *sprite, float *clip)
{
    memcpy(sprite->clip, clip, 4 * sizeof(float));
    if (sprite->cmd == 0xFFFFFFFF)
        return;
    render_system_t *system = &entity_get_world(sprite->entity)->render_system;
    render_component_cmd_t *cmd = &system->cmds[sprite->cmd];
    memcpy(cmd->clip, clip, 4 * sizeof(float));
}

void
render_component_sprite_set_offset(render_component_sprite_t *sprite, float x,
    float y)
{
    sprite->offset[0] = x;
    sprite->offset[1] = y;
    if (sprite->cmd == 0xFFFFFFFF)
        return;
    render_system_t *system = &entity_get_world(sprite->entity)->render_system;
    render_component_cmd_t *cmd = &system->cmds[sprite->cmd];
    cmd->offset[0] = x;
    cmd->offset[1] = y;
}

static int
_render_system_init(world_t *world, uint32 max_components)
{
    render_system_t *system = &world->render_system;
    memset(system, 0, sizeof(*system));
    fixed_pool_init(&system->components, max_components);
    fixed_pool_init(&system->sprites,
        max_components * SPRITES_PER_ENTITY);
    system->cmds = emalloc(
        max_components * SPRITES_PER_ENTITY * sizeof(render_component_cmd_t));
    system->reserved = emalloc(max_components * sizeof(uint32));
    system->num_cmds = 0;
    world_listen_to_event(world, WORLD_EVENT_MAP_LOAD,
        _handle_world_event_load_map, 0);
    return 0;
}

static void
_render_system_destroy(world_t *world)
{
    render_system_t *system = &world->render_system;
    fixed_pool_destroy(&system->components);
    fixed_pool_destroy(&system->sprites);
    free(system->cmds);
    free(system->reserved);
}

static component_handle_t
_render_component_attach(entity_t *entity)
{
    render_system_t *system = &entity_get_world(entity)->render_system;
    uint32 index = system->components.max - system->components.num_free;
    render_component_t *component = fixed_pool_new(&system->components);
    component->entity           = entity;
    component->sprites          = 0xFFFFFFFF;
    component->reserved_index   = index;
    int position[3];
    entity_get_position(entity, position);
    component->culled = (uint8)_should_cull(system->cull_area, position);
    system->reserved[component->reserved_index] = fixed_pool_index(
        &system->components, component);
    return component;
}

static void
_render_component_detach(component_handle_t handle)
{
    render_component_t  *component  = handle;
    entity_t            *entity     = component->entity;
    render_system_t     *system     = &entity_get_world(entity)->render_system;
    while (component->sprites != 0xFFFFFFFF)
        render_component_sprite_free(&system->sprites.all[component->sprites]);
    fixed_pool_free(&system->components, component);
}

static bool32
_should_cull(int *cull_area, int *position)
{
    for (int i = 0; i < 3; ++i)
        if (position[i] < cull_area[i])
            return 1;
    for (int i = 0; i < 3; ++i)
        if (position[i] >= cull_area[i] + cull_area[3 + i])
            return 1;
    return 0;
}

static void
_handle_entity_set_position_event(entity_event_t *event)
{
    entity_t            *entity     = event->entity;
    render_component_t  *component  = entity_get_component(entity,
        &render_component_definition);
    render_system_t *system = &entity_get_world(entity)->render_system;
    if (component->sprites == 0xFFFFFFFF)
        return;
    /*-- Was not culled before, but should be now --*/
    if (!component->culled &&
        _should_cull(system->cull_area, event->set_position.new_position))
    {
        uint32 index = component->sprites;
        while (index != 0xFFFFFFFF)
        {
            render_component_sprite_t *sprite = &system->sprites.all[index];
            index = sprite->next;
            memcpy(sprite->last_tile_position,
                event->set_position.last_position, 3 * sizeof(int));
            if (sprite->cmd != 0xFFFFFFFF)
                _delete_cmd(sprite, system);
        }
        component->culled = 1;
    } else
    /*-- Was culled before, but should be so no longer --*/
    if (component->culled &&
        !_should_cull(system->cull_area, event->set_position.new_position))
    {
        /*-- Culled before, but should no longer be culled --*/
        uint32 index = component->sprites;
        while (index != 0xFFFFFFFF)
        {
            render_component_sprite_t *sprite = &system->sprites.all[index];
            muta_assert(sprite->cmd == 0xFFFFFFFF);
            index = sprite->next;
            memcpy(sprite->last_tile_position,
                event->set_position.last_position, 3 * sizeof(int));
            if (!sprite->texture)
                continue;
            sprite->cmd = system->num_cmds++;
            _fill_cmd(sprite, &system->cmds[sprite->cmd], system);
        }
        component->culled = 0;
    } else
    {
        uint32 index = component->sprites;
        while (index != 0xFFFFFFFF)
        {
            render_component_sprite_t *sprite = &system->sprites.all[index];
            index = sprite->next;
            for (int i = 0; i < 3; ++i)
                sprite->last_tile_position[i] =
                    event->set_position.last_position[i];
            if (sprite->cmd == 0xFFFFFFFF)
                continue;
            render_component_cmd_t *cmd = &system->cmds[sprite->cmd];
            for (int i = 0; i < 3; ++i)
                cmd->tile_position[i] = event->set_position.new_position[i];
            for (int i = 0; i < 3; ++i)
                cmd->last_tile_position[i] =
                    event->set_position.last_position[i];
        }
    }
}

static void
_handle_mobility_component_event(entity_t *entity, component_handle_t handle,
    void *event_ptr)
{
    mobility_component_event_t *event = event_ptr;
    switch (event->type)
    {
    case MOBILITY_COMPONENT_EVENT_START_MOVE:
    {
        render_component_t *component = entity_get_component(entity,
            &render_component_definition);
        render_system_t *system = &entity_get_world(entity)->render_system;
        uint32 index = component->sprites;
        while (index != 0xFFFFFFFF)
        {
            render_component_sprite_t *sprite = &system->sprites.all[index];
            index = sprite->next;
            sprite->percentage_travelled = 0;
            if (sprite->cmd != 0xFFFFFFFF)
                system->cmds[sprite->cmd].percentage_travelled = 0;
        }
    }
        break;
    case MOBILITY_COMPONENT_EVENT_MOVED:
    {
        render_component_t *component = entity_get_component(entity,
            &render_component_definition);
        render_system_t *system = &entity_get_world(entity)->render_system;
        uint32 index = component->sprites;
        while (index != 0xFFFFFFFF)
        {
            render_component_sprite_t *sprite = &system->sprites.all[index];
            index = sprite->next;
            sprite->percentage_travelled = event->moved.percentage_travelled;
            if (sprite->cmd != 0xFFFFFFFF)
                system->cmds[sprite->cmd].percentage_travelled =
                    event->moved.percentage_travelled;
        }
    }
        break;
    case MOBILITY_COMPONENT_EVENT_STOP_MOVE:
    {
        render_component_t *component = entity_get_component(entity,
            &render_component_definition);
        render_system_t *system = &entity_get_world(entity)->render_system;
        uint32 index = component->sprites;
        while (index != 0xFFFFFFFF)
        {
            render_component_sprite_t *sprite = &system->sprites.all[index];
            index = sprite->next;
            sprite->percentage_travelled = 0.f;
            int position[3];
            entity_get_position(entity, position);
            for (int i = 0; i < 3; ++i)
                sprite->last_tile_position[i] = position[i];
            if (sprite->cmd == 0xFFFFFFFF)
                continue;
            system->cmds[sprite->cmd].percentage_travelled = 0.f;
            for (int i = 0; i < 3; ++i)
                system->cmds[sprite->cmd].last_tile_position[i] = position[i];
        }
    }
        break;
    }
}

static void
_handle_world_event_load_map(world_event_t *event, void *user_data)
    {memset(event->world->render_system.cull_area, 0, 6 * sizeof(int));}

static void
_delete_cmd(render_component_sprite_t *sprite, render_system_t *system)
{
    system->cmds[sprite->cmd] = system->cmds[--system->num_cmds];
    system->sprites.all[system->cmds[sprite->cmd].sprite].cmd = sprite->cmd;
    sprite->cmd = 0xFFFFFFFF;
}

static void
_fill_cmd(render_component_sprite_t *sprite, render_component_cmd_t *cmd,
    render_system_t *system)
{
    muta_assert(sprite->texture);
    cmd->sprite     = fixed_pool_index(&system->sprites, sprite);
    cmd->texture    = sprite->texture;
    memcpy(cmd->clip, sprite->clip, sizeof(cmd->clip));
    memcpy(cmd->last_tile_position, sprite->last_tile_position,
        sizeof(cmd->last_tile_position));
    cmd->offset[0] = sprite->offset[0];
    cmd->offset[1] = sprite->offset[1];
    entity_get_position(sprite->entity, cmd->tile_position);
    cmd->entity = sprite->entity;
}

static inline render_component_t *
_get_component_of_sprite(render_component_sprite_t *sprite)
{
    return entity_get_component(sprite->entity, &render_component_definition);
}
