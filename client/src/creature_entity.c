#include "creature_entity.h"
#include "world.h"
#include "assets.h"
#include "log.h"
#include "name_plate_component.h"

enum creature_flag
{
    CREATURE_FLAG_DEAD
};

entity_t *
creature_spawn(world_t *world, creature_runtime_id_t guid,
    creature_type_id_t type_id, int sex, int direction, int *position,
    uint32 health_current, uint32 health_max, bool32 dead)
{
    entity_t *entity = world_get_creature(world, guid);
    if (entity)
        entity_despawn(entity);
    entity = entity_spawn(world, direction, position);
    if (!entity)
        return entity;
    hashtable_einsert(world->creature_table, guid,
        hashtable_hash(&guid, sizeof(guid)), entity);
    entity_type_data_t *type_data = entity_get_type_data(entity);
    type_data->type                     = ENTITY_TYPE_CREATURE;
    type_data->creature.id              = guid;
    type_data->creature.type_id         = type_id;
    type_data->creature.sex             = sex;
    type_data->creature.health_current  = health_current;
    type_data->creature.health_max      = health_max;
    type_data->creature.flags           = 0;
    if (dead)
        type_data->creature.flags |= CREATURE_FLAG_DEAD;
    component_handle_t mobility_component = entity_attach_component(entity,
        &mobility_component_definition);
    if (mobility_component == INVALID_COMPONENT_HANDLE)
        goto despawn;
    mobility_component_set_speed(mobility_component,
        ent_convert_move_speed_to_seconds(ENT_BASE_MOVE_SPEED));
    component_handle_t render_component = entity_attach_component(entity,
        &render_component_definition);
    if (render_component == INVALID_COMPONENT_HANDLE)
        goto despawn;
    creature_def_t *def = ent_get_creature_def(type_id);
    if (!def)
        goto despawn;
    /*-- Animations --*/
    component_handle_t ae_animator_component = entity_attach_component(
        entity, &ae_animator_component_definition);
    if (ae_animator_component == INVALID_COMPONENT_HANDLE)
        goto despawn;
    component_handle_t ae_set_component = entity_attach_component(entity,
        &ae_set_component_definition);
    if (!ae_set_component)
        goto despawn;
    as_creature_display_t *creature_display = as_get_creature_display(
        def->display_id);
    if (!creature_display)
        goto despawn;
    ae_set_component_set_asset(ae_set_component,
        as_get_ae_set(creature_display->ae_set));
    component_handle_t name_plate_component = entity_attach_component(entity,
        &name_plate_component_definition);
    if (name_plate_component == INVALID_COMPONENT_HANDLE)
    {
        LOG_ERR("Failed to attach name plate component.");
        goto despawn;
    }
    name_plate_component_set_offset(name_plate_component,
        creature_display->name_plate_offset_x,
        creature_display->name_plate_offset_y);
    name_plate_component_set_max_health(name_plate_component, health_max);
    name_plate_component_set_current_health(name_plate_component,
        health_current);
    LOG_DEBUG("Successful, id %u, type %u, position %d, %d, %d.", guid, type_id,
        position[0], position[1], position[2]);
    return entity;
    despawn:
        LOG_ERR("failed!\n");
        entity_despawn(entity);
        return 0;
}

void
creature_walk(entity_t *creature, int *position)
{
    component_handle_t mobility_component = entity_get_component(creature,
        &mobility_component_definition);
    if (!mobility_component)
    {
        entity_set_position(creature, position);
        return;
    }
    int old_position[3];
    int substraction[2];
    entity_get_position(creature, old_position);
    substraction[0] = position[0] - old_position[0];
    substraction[1] = position[1] - old_position[1];
    int direction = vec2_to_iso_dir(substraction[0], substraction[1]);
    entity_set_direction(creature, direction);
    mobility_component_move(mobility_component, position);
}

void
creature_set_current_health(entity_t *entity, uint32 current_health)
{
    entity_type_data_t *type_data = entity_get_type_data(entity);
    type_data->creature.health_current = current_health;
    name_plate_component_t *name_plate_component = entity_get_component(entity,
        &name_plate_component_definition);
    muta_assert(name_plate_component);
    name_plate_component_set_current_health(name_plate_component,
        current_health);
}
