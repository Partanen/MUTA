#ifndef MUTA_CLIENT_ANIMATED_ENTITY_H
#define MUTA_CLIENT_ANIMATED_ENTITY_H

/* Forward declaration(s) */
typedef struct as_tex_t             as_tex_t;

/* Types defined here */
typedef struct ae_layer_t           ae_layer_t;
typedef struct ae_frame_t           ae_frame_t;
typedef struct ae_animation_t       ae_animation_t;
typedef struct animated_entity_t    animated_entity_t;

enum ae_layer_type
{
    AE_LAYER_BODY = 0,
    AE_LAYER_HEAD,
    AE_LAYER_LHAND,
    AE_LAYER_RHAND,
    AE_LAYER_LEGS,
    AE_LAYER_FEET,
    AE_LAYER_BODY_ARMOUR,
    AE_LAYER_HEAD_ARMOUR,
    AE_LAYER_LHAND_ARMOUR,
    AE_LAYER_RHAND_ARMOUR,
    AE_LAYER_LEGS_ARMOUR,
    AE_LAYER_FEET_ARMOUR,
    AE_LAYER_LHAND_ITEM,
    AE_LAYER_RHAND_ITEM,
    NUM_AE_LAYER_TYPES
};

struct ae_layer_t
{
    as_tex_t    *ta;
    uint8       color[4];
    float       clip[4];
    float       ox, oy;
    float       rot;
    int         flip;
    int         type;
};

struct ae_frame_t
{
    ae_layer_t *layers;
    int         num_layers;
    float       ox, oy;
};

struct ae_animation_t
{
    ae_frame_t  *frames;
    int         num_frames;
};

struct animated_entity_t
{
    ae_animation_t  anims[8];
};

#endif /* MUTA_CLIENT_ANIMATED_ENTITY_H */
