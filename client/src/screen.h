/* screen.h
 * Screens define program states. After initialization, some screen will always
 * be active. */

#ifndef MUTA_CLIENT_SCREEN_H
#define MUTA_CLIENT_SCREEN_H
#include "../../shared/types.h"

typedef struct screen_t screen_t;

struct screen_t
{
    const char *name;
    int (*init)(void);          /* Called at program startup. Can be null. */
    void (*destroy)(void);      /* Called at program shutdown. Can be null. */
    void (*update)(double dt);  /* Called every frame if active */
    void (*open)(void);         /* Called when set as current */
    void (*close)(void);        /* Called when stops being current */
    void (*text_input)(const char *text);
    void (*keydown)(int key, bool32 is_repeat);
    void (*keyup)(int key);
    void (*mousebuttondown)(uint8 button, int x, int y);
    void (*mousebuttonup)(uint8 button, int x, int y);
    void (*mousewheel)(int x, int y);
    void (*file_drop)(const char *path);
};

int
screens_init(void);

void
screens_destroy(void);

screen_t *
screens_get(const char *name);

screen_t **
screens_get_all(uint32 *ret_num);

#endif /* MUTA_CLIENT_SCREEN_H */
