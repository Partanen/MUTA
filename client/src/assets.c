#include "assets.h"
#include "audio.h"
#include "render.h"
#include "core.h"
#include "asset_container.h"
#include "log.h"
#include "../../shared/gui/gui.h"
#include "../../shared/common_utils.h"
#include "../../shared/mdb.h"
#include "../../shared/kthp.h"
#include "../../shared/common_defs.h"

#define ASSET_PANIC(path_, code_) \
    muta_panic_print("Corrupted or missing assets: %s (code %d).", path_, code_)

typedef struct tex_job_t                    tex_job_t;
typedef struct iso_sprite_load_context_t    iso_sprite_load_context_t;
typedef struct font_load_context_t          font_load_context_t;
typedef struct str_as_ae_set_table_t        str_as_ae_set_table_t;
typedef struct str_cursor_table             str_cursor_table_t;
typedef struct str_gui_font_table           str_gui_font_table_t;
typedef struct uint32_as_creature_display_table
    uint32_as_creature_display_table_t;
typedef struct uint32_as_dynamic_object_display_table
    uint32_as_dynamic_object_display_table_t;
typedef struct uint32_as_ae_set_table       uint32_as_ae_set_table_t;
typedef struct uint32_as_iso_sprite_table   uint32_as_iso_sprite_table_t;

enum as_iso_sprite_flag
{
    AS_ISO_SPRITE_KEEP_LOADED = (1 << 0)
};

struct iso_sprite_load_context_t
{
    as_iso_sprite_t *is;
    int             current_direction; /* -1 if none */
    bool32          have_direction[NUM_ISODIRS];
    bool32          have_texture;
};

struct font_load_context_t
{
    gui_font_t *font;
    dchar   *path;
    int     size;
    enum
    {
        FONT_DEFAULT_NONE,
        FONT_DEFAULT,
        FONT_DEFAULT_PIXEL
    } default_type;
};

struct tex_job_t
{
    as_tex_t *ta;
};

#define TEX_ASSET_ITEM(ta_) \
    ((tex_asset_container_item_t*)((char*)(ta_) - \
    offsetof(tex_asset_container_item_t, asset)))
#define ANIM__ASSET_ITEM(aa_) \
    ((anim_asset_container_item_t*)((char*)(aa_) - \
    offsetof(anim_asset_container_item_t, asset)))
#define AE_ASSET_ITEM(aa_) \
    ((ae_asset_container_item_t*)((char*)(aa_) - \
    offsetof(ae_asset_container_item_t, asset)))
#define SPRITESHEET_ITEM(ss_) \
    ((spritesheet_container_item_t*)((char*)(ss_) - \
    offsetof(spritesheet_container_item_t, asset)))
#define SOUND_ASSET_ITEM(sa_) \
    ((sound_asset_container_item_t*)((char*)(sa_) - \
    offsetof(sound_asset_container_item_t, asset)))
#define CURSOR_BUCKET_SZ 2

typedef struct unclaimed_asset_t    unclaimed_asset_t;
typedef unclaimed_asset_t           unclaimed_asset_darr_t;

enum asset_types
{
    ASSET_TEX,
    ASSET_SOUND
};

struct unclaimed_asset_t
{
    void    *asset;
    uint32  time;
    int8    type;
};

static struct
{
    tex_job_t   *jobs;
    int         num, max;
    mutex_t     mtx;
} tex_jobs;

ASSET_CONTAINER_DEFINITION(as_tex_t, tex_asset_container);
ASSET_CONTAINER_DEFINITION(as_animation_t, anim_asset_container);
ASSET_CONTAINER_DEFINITION(as_ae_t, ae_asset_container);
ASSET_CONTAINER_DEFINITION(as_spritesheet_t, spritesheet_container);
ASSET_CONTAINER_DEFINITION(as_sound_t, sound_asset_container);
STR_HASHTABLE_DEFINITION(str_as_ae_set_table, as_ae_set_t*);
STR_HASHTABLE_DEFINITION(str_cursor_table, as_cursor_t);
STR_HASHTABLE_DEFINITION(str_gui_font_table, gui_font_t*);
hashtable_define(uint32_as_creature_display_table, uint32,
    as_creature_display_t*);
hashtable_define(uint32_as_dynamic_object_display_table, uint32,
    as_dynamic_object_display_t*);
hashtable_define(uint32_as_ae_set_table, uint32, as_ae_set_t);
hashtable_define(uint32_as_iso_sprite_table, uint32, as_iso_sprite_t*);

static tex_asset_container_t        _tex_assets;
static anim_asset_container_t       _anim_assets;
static ae_asset_container_t         _ae_assets;
static spritesheet_container_t      _spritesheets;
static sound_asset_container_t      _sound_assets;
static unclaimed_asset_darr_t       *_unclaimed_assets;
static uint32_as_ae_set_table_t     _ae_sets;

/* Default assets if none are found. */
static tex_asset_container_item_t   _matlock;
static dchar                        *_matlock_path;
static as_iso_sprite_t              _default_iso_sprite;

static struct
{
    obj_pool_t              pool;
    str_gui_font_table_t    table;
    gui_font_t              *default_font;
    gui_font_t              *default_pixel_font;
    gui_font_t              **all; /* darr */
} _fonts;

static str_cursor_table_t _cursors;

static struct
{
    obj_pool_t                          pool;
    uint32_as_creature_display_table_t  table;
} _creature_displays;

static struct
{
    obj_pool_t                          pool;
    uint32_as_dynamic_object_display_table_t  table;
} _dynamic_object_displays;

static struct
{
    obj_pool_t                      pool;
    uint32_as_iso_sprite_table_t    table;
} _iso_sprites;

static void
_load_tex_assets(void);

static void
_load_spritesheets(void);

static void
_load_anim_assets(void);

static void
_load_ae_assets(void);

static void
_load_ae_sets(void);

static void
_load_sound_assets(void);

static void
_load_cursors(void);

static void
_load_creature_displays(void);

static void
_load_dynamic_object_displays(void);

static void
_load_iso_sprites(void);

static void
_load_fonts(void);

static void
_destroy_tex_assets(void);

static void
_destroy_spritesheets(void);

static void
_destroy_anim_assets(void);

static void
_destroy_ae_assets(void);

static void
_destroy_ae_sets(void);

static void
_destroy_sound_assets(void);

static void
_destroy_cursors(void);

static void
_destroy_fonts(void);

static void
_tex_asset_async_callback(void *args);

static int
_tex_asset_claim(as_tex_t *ta, bool32 async);

static int
_tex_asset_load_bitmap(as_tex_t *ta);

static int
_tex_asset_generate_gl_texture(as_tex_t *ta);

static int
_tex_asset_load_bitmap_and_generate_texture(as_tex_t *ta);

static int
_launch_tex_asset_async_load(as_tex_t *ta);

static int
_spritesheet_load(as_spritesheet_t *sheet, bool32 async);

static void
_spritesheet_unload(as_spritesheet_t *ss);

static as_spritesheet_t *
_spritesheet_claim(as_spritesheet_t *ss, bool32 async);

static int
_anim_asset_load(as_animation_t *aa, bool32 async);

static void
_anim_asset_unload(as_animation_t *a);

static as_animation_t *
_anim_asset_claim(as_animation_t *aa, bool32 async);

static int
_ae_asset_load(as_ae_t *aes, bool32 async);

static void
_sound_asset_load_callback(void *args);

static int
_sound_asset_load(as_sound_t *sa, bool32 async);

static int
_claim_sound(as_sound_t *sa, bool32 async);

static void
_load_cursors_callback(void *ctx, const char *opt, const char *val);

static int
_on_iso_sprite_def(parse_def_file_context_t *ctx, const char *def,
    const char *val);

static int
_on_iso_sprite_opt(parse_def_file_context_t *ctx, const char *opt,
    const char *val);

static int
_iso_sprite_load(as_iso_sprite_t *is, bool32 async);

static int
_load_font(gui_font_t *font, const char *path, int font_size);

static int
_font_on_def(parse_def_file_context_t *ctx, const char *def, const char *val);

static int
_font_on_val(parse_def_file_context_t *ctx, const char *opt, const char *val);

static int
_push_gen_tex_job(as_tex_t *ta);

int
as_load(void)
{
    /* Defaults */
    _matlock.asset.flags    = 0;
    _matlock.row            = 0xFFFFFFFF;
    _matlock.id             = 0xFFFFFFFF;
    _matlock.asset.flags |= AS_TEX_KEEP_LOADED;
    _matlock.asset.flags |= AS_TEX_LOADED;
    _matlock.asset.flags |= AS_TEX_KEEP_BITMAP;
    if (img_load(&_matlock.asset.bitmap, core_asset_config.matlock_tex_path))
    {
        core_error_window("Could not find matlock texture at '%s' as defined "
            "in asset_config.cfg.", core_asset_config.matlock_tex_path);
        return 1;
    }
    _matlock_path = dstr_create_empty(0);
    tex_from_img(&_matlock.asset.tex, &_matlock.asset.bitmap);
    _default_iso_sprite.id      = 0xFFFFFFFF;
    _default_iso_sprite.tex_id  = _matlock.asset.tex.id;
    _default_iso_sprite.ta      = &_matlock.asset;
    for (int i = 0; i < 8; ++i)
    {
        _default_iso_sprite.clips[i][0]     = 0;
        _default_iso_sprite.clips[i][1]     = 0;
        _default_iso_sprite.clips[i][2]     = _matlock.asset.tex.w;
        _default_iso_sprite.clips[i][3]     = _matlock.asset.tex.h;
        _default_iso_sprite.offsets[i][0]   = _matlock.asset.tex.w * 0.5f;
        _default_iso_sprite.offsets[i][1]   = _matlock.asset.tex.w * 0.5f;
    }
    tex_jobs.max    = 64;
    tex_jobs.jobs   = emalloc(sizeof(tex_job_t) * tex_jobs.max);
    tex_jobs.num    = 0;
    mutex_init(&tex_jobs.mtx);
    darr_reserve(_unclaimed_assets, 128);
    _load_sound_assets();
    _load_tex_assets();
    _load_anim_assets();
    _load_ae_assets();
    _load_ae_sets();
    _load_fonts();
    _load_spritesheets();
    _load_iso_sprites();
    _load_cursors();
    _load_creature_displays();
    _load_dynamic_object_displays();
    as_bind_queued_textures();
    return 0;
}

void
as_destroy(void)
{
    dstr_free(&_matlock_path);
    _destroy_sound_assets();
    _destroy_anim_assets();
    _destroy_ae_assets();
    _destroy_ae_sets();
    _destroy_fonts();
    _destroy_spritesheets();
    _destroy_tex_assets();
    _destroy_cursors();
    tex_free(&_matlock.asset.tex);
    img_free(&_matlock.asset.bitmap);
}

void
as_update(void)
{
    uint32              time_now            = SDL_GetTicks();
    uint32              num                 = darr_num(_unclaimed_assets);
    uint32              sub;
    as_tex_t         *ta;
    as_sound_t       *sa;
    for (int32 i = 0; i < (int32)num; ++i)
    {
        sub = time_now - _unclaimed_assets[i].time;
        switch (_unclaimed_assets[i].type)
        {
        case ASSET_TEX:
        {
            ta  = _unclaimed_assets[i].asset;
            if (sub < 30000 || ta->flags & AS_TEX_LOADING ||
                !(ta->flags & AS_TEX_LOADED))
                continue;
            if (ta->user_count == 0)
            {
                if (ta->flags & AS_TEX_KEEP_BITMAP)
                    img_free(&ta->bitmap);
                tex_free(&ta->tex);
                ta->flags &= ~AS_TEX_LOADED;
            }
            ta->flags &= ~AS_TEX_IN_UNCLAIMED_QUEUE;
        }
            break;
        case ASSET_SOUND:
        {
            sa = _unclaimed_assets[i].asset;
            if (sub < 30000 || sa->loading || !sa->loaded)
                continue;
            if (sa->user_count == 0)
            {
                au_dequeue_sound_asset(sa);
                au_destroy_buffer(&sa->sound.buffer);
                sa->loaded = 0;
                DEBUG_PRINTFF("freed sound %s.\n", as_sound_get_path(sa));
            }
            sa->in_unclaimed_queue = 0;
        }
            break;
        }
        _unclaimed_assets[i] = _unclaimed_assets[--num];
    }
    muta_assert(_unclaimed_assets);
    darr_head(_unclaimed_assets)->num = num;
}

int
as_bind_queued_textures(void)
{
    if (tex_jobs.num <= 0)
        return 0;
    mutex_lock(&tex_jobs.mtx);
    for (int i = 0; i < tex_jobs.num; ++i)
    {
        _tex_asset_generate_gl_texture(tex_jobs.jobs[i].ta);
        tex_jobs.jobs[i].ta->flags &= ~AS_TEX_LOADING;
        tex_jobs.jobs[i].ta->flags |= AS_TEX_LOADED;
    }
    tex_jobs.num = 0;
    mutex_unlock(&tex_jobs.mtx);
    return 0;
}

as_tex_t *
as_claim_tex(uint32 id, bool32 async)
{
    tex_asset_container_item_t **item = tex_asset_container_id_table_find(
        &_tex_assets.id_table, id);
    if (!item)
        return &_matlock.asset;
    muta_assert(as_tex_get_id(&(*item)->asset) == id);
    return _tex_asset_claim(&(*item)->asset, async) ? 0 : &(*item)->asset;
}

void
as_unclaim_tex(as_tex_t *ta)
{
    if (!ta)
        return;
    if (ta == &_matlock.asset)
        return;
    muta_assert(ta->user_count > 0);
    ta->user_count--;
    if (ta->user_count > 0 || ta->flags & AS_TEX_KEEP_LOADED ||
        ta->flags & AS_TEX_IN_UNCLAIMED_QUEUE)
        return;
    ta->flags |= AS_TEX_IN_UNCLAIMED_QUEUE;
    unclaimed_asset_t ua;
    ua.asset    = ta;
    ua.time     = SDL_GetTicks();
    ua.type     = ASSET_TEX;
    darr_push(_unclaimed_assets, ua);
}

bool32
as_tex_exists(uint32 id)
    {return tex_asset_container_id_table_exists(&_tex_assets.id_table, id);}

const dchar *
as_tex_get_path(as_tex_t *ta)
{
    if (!ta)
        return 0;
    if (ta == &_matlock.asset)
        return "";
    return *(dchar**)mdb_get_field(&_tex_assets.mdb, TEX_ASSET_ITEM(ta)->row,
        _tex_assets.path_mdb_index);
}

uint32
as_tex_get_id(as_tex_t *ta)
{
    if (!ta)
        return 0xFFFFFFFF;
    if (ta == &_matlock.asset)
        return _matlock.id;
    return TEX_ASSET_ITEM(ta)->id;
}

img_t *
as_tex_get_bitmap(as_tex_t *ta)
{
    if (!(ta->flags & AS_TEX_KEEP_BITMAP))
        return 0;
    return &ta->bitmap;
}

as_tex_t *
as_get_default_tex(void)
    {return &_matlock.asset;}

int
as_get_num_tex_db_entries(void)
    {return mdb_num_rows(&_tex_assets.mdb);}

int
as_get_tex_db_entry(int index, as_tex_db_entry_t *ret_tex_db_entry)
{
    mdb_row_t row = mdb_get_row_by_index(&_tex_assets.mdb, index);
    if (!row)
        return 1;
    ret_tex_db_entry->id = *(uint32*)mdb_get_field(&_tex_assets.mdb, row,
        _tex_assets.id_mdb_index);
    ret_tex_db_entry->name = *(dchar**)mdb_get_field(&_tex_assets.mdb, row,
        _tex_assets.name_mdb_index);
    return 0;
}

as_spritesheet_t *
as_claim_spritesheet(uint32 id, bool32 async)
{
    spritesheet_container_item_t **item =
        spritesheet_container_id_table_find(&_spritesheets.id_table, id);
    return item ? _spritesheet_claim(&(*item)->asset, async) : 0;
}

void
as_unclaim_spritesheet(as_spritesheet_t *ss)
{
    if (!ss)
        return;
    muta_assert(ss->user_count > 0);
    if (!--ss->user_count && !ss->keep_loaded)
        _spritesheet_unload(ss);
}

float *
as_spritesheet_get_clip(as_spritesheet_t *ss, const char *name)
{
    int     num     = ss->num_clips;
    char    **names = ss->clip_names;
    for (int i = 0; i < num; ++i)
        if (streq(names[i], name))
            return ss->clips[i];
    return 0;
}

const dchar *
as_spritesheet_get_path(as_spritesheet_t *ss)
{
    if (!ss) return 0;
    return *(dchar**)mdb_get_field(&_spritesheets.mdb,
        SPRITESHEET_ITEM(ss)->row, _spritesheets.path_mdb_index);
}

as_animation_t *
as_claim_anim_asset_by_name(const char *name, bool32 async)
{
    anim_asset_container_item_t **item =
        anim_asset_container_name_table_find(&_anim_assets.name_table, name);
    if (!item) return 0;
    return _anim_asset_claim(&(*item)->asset, async);
}

as_animation_t *
as_claim_anim_asset_by_id(uint32 id, bool32 async)
{
    anim_asset_container_item_t **item =
        anim_asset_container_id_table_find(&_anim_assets.id_table, id);
    if (!item) return 0;
    return _anim_asset_claim(&(*item)->asset, async);
}

void
as_unclaim_anim_asset(as_animation_t *a)
{
    if (!a)
        return;
    muta_assert(a->user_count > 0);
    if (--a->user_count)
        return;
    _anim_asset_unload(a);
}

const dchar *
as_animation_get_path(as_animation_t *aa)
{
    if (!aa) return 0;
    return *(dchar**)mdb_get_field(&_anim_assets.mdb, ANIM__ASSET_ITEM(aa)->row,
        _anim_assets.path_mdb_index);
}

as_ae_t *
as_claim_ae(uint32 id, bool32 async)
{
    ae_asset_container_item_t **item =
        ae_asset_container_id_table_find(&_ae_assets.id_table, id);
    if (!item)
        return 0;
    as_ae_t *aa = &(*item)->asset;
    if (!aa->user_count && _ae_asset_load(aa, async))
        return 0;
    aa->user_count++;
    return aa;
}

void
as_unclaim_ae(as_ae_t *ae)
{
    IMPLEMENTME();
    if (!ae)
        return;
    muta_assert(ae->user_count > 0);
    ae->user_count--;
}

const dchar *
as_ae_get_path(as_ae_t *aa)
{
    if (!aa) return 0;
    return *(dchar**)mdb_get_field(&_ae_assets.mdb, AE_ASSET_ITEM(aa)->row,
        _ae_assets.path_mdb_index);
}

as_ae_set_t *
as_get_ae_set(uint32 id)
    {return uint32_as_ae_set_table_find(&_ae_sets, id);}

as_sound_t *
as_claim_sound_by_name(const char *name, bool32 async)
{
    sound_asset_container_item_t **item = sound_asset_container_name_table_find(
        &_sound_assets.name_table, name);
    if (!item) return 0;
    if (_claim_sound(&(*item)->asset, async))
        return 0;
    return &(*item)->asset;
}

as_sound_t *
as_claim_sound_by_id(uint32 id, bool32 async)
{
    sound_asset_container_item_t **item = sound_asset_container_id_table_find(
        &_sound_assets.id_table, id);
    if (!item) return 0;
    if (_claim_sound(&(*item)->asset, async))
        return 0;
    return &(*item)->asset;
}

void
as_unclaim_sound(as_sound_t *sa)
{
    if (!sa) return;
    muta_assert(sa->user_count > 0);
    if (--sa->user_count > 0 || sa->keep_loaded || sa->in_unclaimed_queue)
        return;
    sa->in_unclaimed_queue = 1;
    unclaimed_asset_t ua;
    ua.asset    = sa;
    ua.time     = SDL_GetTicks();
    ua.type     = ASSET_SOUND;
    darr_push(_unclaimed_assets, ua);
}

const dchar *
as_sound_get_path(as_sound_t *sa)
{
    if (!sa) return 0;
    return *(dchar**)mdb_get_field(&_sound_assets.mdb,
        SOUND_ASSET_ITEM(sa)->row, _sound_assets.path_mdb_index);
}


as_cursor_t *
as_get_cursor(const char *name)
    {return str_cursor_table_find(&_cursors, name);}

as_creature_display_t *
as_get_creature_display(uint32 id)
{
    as_creature_display_t **ret = uint32_as_creature_display_table_find(
        &_creature_displays.table, id);
    return ret ? *ret : 0;
}

as_dynamic_object_display_t *
as_get_dynamic_object_display(uint32 id)
{
    as_dynamic_object_display_t **ret =
        uint32_as_dynamic_object_display_table_find(
            &_dynamic_object_displays.table, id);
    return ret ? *ret : 0;
}

as_iso_sprite_t *
as_claim_iso_sprite(uint32 id, bool32 async)
{
    as_iso_sprite_t **is = uint32_as_iso_sprite_table_find(
        &_iso_sprites.table, id);
    if (!is)
        return &_default_iso_sprite;
    if (!((*is)->flags & AS_ISO_SPRITE_KEEP_LOADED) && !(*is)->user_count &&
        _iso_sprite_load(*is, async))
        ASSET_PANIC("iso_sprite", 1);
    (*is)->user_count++;
    return *is;
}

void
as_unclaim_iso_sprite(as_iso_sprite_t *is)
{
    if (!is)
        return;
    if (is == &_default_iso_sprite)
        return;
    muta_assert(is->user_count > 0);
    is->user_count--;
    if (is->user_count || is->flags & AS_ISO_SPRITE_KEEP_LOADED)
        return;
    as_unclaim_tex(is->ta);
}

gui_font_t *
as_get_font(const char *name)
{
    gui_font_t **ret = str_gui_font_table_find(&_fonts.table, name);
    return ret ? *ret : 0;
}

gui_font_t *
as_get_font_by_index(uint32 index)
{
    if (index >= darr_num(_fonts.all))
        return 0;
    return _fonts.all[index];
}

uint32
as_num_fonts(void)
    {return darr_num(_fonts.all);}

gui_font_t *
as_get_default_font(void)
    {return _fonts.default_font;}

gui_font_t *
as_get_default_pixel_font(void)
    {return _fonts.default_pixel_font;}

static void
_load_tex_assets(void)
{
    const char *path = "assets/textures.mdb";
    int err = 0;
    if (tex_asset_container_init(&_tex_assets, path))
        {err = 1; goto fail;}
    uint32 keep_loaded_i;
    uint32 keep_bitmap_i;
    uint32 min_filter_i;
    uint32 mag_filter_i;
    int i;
    i = mdb_get_col_index(&_tex_assets.mdb, "keep loaded");
    if (i < 0)
        {err = 2; goto fail;}
    keep_loaded_i = i;
    i = mdb_get_col_index(&_tex_assets.mdb, "keep bitmap");
    if (i < 0)
        {err = 3; goto fail;}
    keep_bitmap_i = i;
    i = mdb_get_col_index(&_tex_assets.mdb, "min filter");
    if (i < 0)
        {err = 4; goto fail;}
    min_filter_i = i;
    i = mdb_get_col_index(&_tex_assets.mdb, "mag filter");
    if (i < 0)
        {err = 5; goto fail;}
    mag_filter_i = i;
    int8 keep_loaded, keep_bitmap, min_filter, mag_filter;
    tex_asset_container_item_t  *item;
    as_tex_t                    *ta;
    for (item = tex_asset_container_pool_first(&_tex_assets.pool);
         item;
         item = item->next)
    {
        ta = &item->asset;
        ta->flags = 0;
        keep_loaded = *(int8*)mdb_get_field(&_tex_assets.mdb, item->row,
            keep_loaded_i);
        keep_bitmap = *(int8*)mdb_get_field(&_tex_assets.mdb, item->row,
            keep_bitmap_i);
        min_filter = *(int8*)mdb_get_field(&_tex_assets.mdb, item->row,
            min_filter_i);
        mag_filter = *(int8*)mdb_get_field(&_tex_assets.mdb, item->row,
            mag_filter_i);
        if (keep_loaded)
            ta->flags |= AS_TEX_KEEP_LOADED;
        if (keep_bitmap)
            ta->flags |= AS_TEX_KEEP_BITMAP;
        ta->min_filter  = min_filter == 1 ? TEX_FILTER_NEAREST :
            TEX_FILTER_LINEAR;
        ta->mag_filter  = mag_filter == 1 ? TEX_FILTER_NEAREST :
            TEX_FILTER_LINEAR;
        ta->wrapping    = TEX_WRAP_CLAMP_TO_EDGE; /* tmp */
        /* Load asynchronously */
        if (keep_loaded)
            _tex_asset_load_bitmap_and_generate_texture(ta);
        DEBUG_PRINTFF("Loaded tex asset %u, path %s, keep bitmap: %s\n",
            as_tex_get_id(ta), as_tex_get_path(ta),
            (ta->flags & AS_TEX_KEEP_BITMAP) ? "true" : "false" );
    }
    return;
    fail:
        ASSET_PANIC(path, err);
}

static void
_load_spritesheets(void)
{
    const char  *path   = "assets/spritesheets.mdb";
    int         err     = 0;
    if (spritesheet_container_init(&_spritesheets, path))
        {err = 1; goto fail;}
    int index = mdb_get_col_index(&_anim_assets.mdb, "keep loaded");
    if (index < 0)
        {err = 2; goto fail;}
    uint32 keep_loaded_i = index;
    spritesheet_container_item_t *item;
    for (item = spritesheet_container_pool_first(&_spritesheets.pool);
         item;
         item = item->next)
    {
        dchar *path = *(dchar**)mdb_get_field(&_spritesheets.mdb, item->row,
            _spritesheets.path_mdb_index);
        if (!path)
            continue;
        bool32 keep_loaded = *(int8*)mdb_get_field(&_spritesheets.mdb,
            item->row, keep_loaded_i);
        item->asset.keep_loaded = keep_loaded ? 1 : 0;
        if (keep_loaded)
        {
            if (_spritesheet_load(&item->asset, 0))
                goto fail;
        }
    }
    return;
    fail:
        ASSET_PANIC(path, err);
}

static void
_load_anim_assets(void)
{
    const char *path = "assets/animations.mdb";
    int err = 0;
    if (anim_asset_container_init(&_anim_assets, path))
        {err = 1; goto fail;}
    int index = mdb_get_col_index(&_anim_assets.mdb, "keep loaded");
    if (index < 0)
        {err = 2; goto fail;}
    uint32 keep_loaded_i = index;
    anim_asset_container_item_t *item;
    for (item = anim_asset_container_pool_first(&_anim_assets.pool);
         item;
         item = item->next)
    {
        dchar *path = *(dchar**)mdb_get_field(&_anim_assets.mdb, item->row,
            _anim_assets.path_mdb_index);
        if (!path)
            continue;
        bool32 keep_loaded = *(int8*)mdb_get_field(&_anim_assets.mdb, item->row,
            keep_loaded_i);
        item->asset.keep_loaded = keep_loaded ? 1 : 0;
        if (keep_loaded)
            _anim_asset_load(&item->asset, 1);
    }
    return;
    fail:
        ASSET_PANIC(path, err);
}

static void
_load_ae_assets(void)
{
    const char *path = "assets/animated_entities.mdb";
    int err = 0;
    if (ae_asset_container_init(&_ae_assets, path))
        {err = 1; goto fail;}
    int index = mdb_get_col_index(&_anim_assets.mdb, "keep loaded");
    if (index < 0)
        {err = 2; goto fail;}
    uint32 keep_loaded_i = index;
    ae_asset_container_item_t *item;
    for (item = ae_asset_container_pool_first(&_ae_assets.pool);
         item;
         item = item->next)
    {
        dchar *path = *(dchar**)mdb_get_field(&_ae_assets.mdb, item->row,
            _ae_assets.path_mdb_index);
        if (!path)
            continue;
        bool32 keep_loaded = *(int8*)mdb_get_field(&_ae_assets.mdb,
            item->row, keep_loaded_i);
        item->asset.keep_loaded = keep_loaded ? 1 : 0;
        if (keep_loaded)
            _ae_asset_load(&item->asset, 1);
    }
    return;
    fail:
        ASSET_PANIC(path, err);
}

static void
_load_ae_sets(void)
{
    const char *path = "assets/ae_sets.mdb";
    int err = 0;
    mdb_t mdb;
    if (mdb_open(&mdb, path))
        {err = 1; goto fail;}
    int index;
    if ((index = mdb_get_col_index(&mdb, "id")) < 0)
        {err = 2; goto fail;}
    uint32 id_col_index = (uint32)index;
    if ((index = mdb_get_col_index(&mdb, "idle")) < 0)
        {err = 3; goto fail;}
    uint32 idle_col_index = (uint32)index;
    if ((index = mdb_get_col_index(&mdb, "walk")) < 0)
        {err = 4; goto fail;}
    uint32 walk_col_index   = (uint32)index;
    uint32 num_rows         = mdb_num_rows(&mdb);
    for (uint32 i = 0; i < num_rows; ++i)
    {
        as_ae_set_t ae_set;
        mdb_row_t row = mdb_get_row_by_index(&mdb, i);
        ae_set.id       = *(uint32*)mdb_get_field(&mdb, row, id_col_index);
        ae_set.idle_id  = *(uint32*)mdb_get_field(&mdb, row, idle_col_index);
        ae_set.walk_id  = *(uint32*)mdb_get_field(&mdb, row, walk_col_index);
        uint32_as_ae_set_table_einsert(&_ae_sets, ae_set.id, ae_set);
    }
    mdb_close(&mdb);
    return;
    fail:
        ASSET_PANIC(path, err);
}

static void
_load_sound_assets(void)
{
    const char *path = "assets/sounds.mdb";
    int err = 0;
    if (sound_asset_container_init(&_sound_assets, path))
        {err = 1; goto fail;}
    int index = mdb_get_col_index(&_sound_assets.mdb, "keep loaded");
    if (index < 0)
        {err = 2; goto fail;}
    uint32 keep_loaded_i = index;
    sound_asset_container_item_t *item;
    for (item = sound_asset_container_pool_first(&_sound_assets.pool);
         item;
         item = item->next)
    {
        dchar *path = *(dchar**)mdb_get_field(&_sound_assets.mdb, item->row,
            _sound_assets.path_mdb_index);
        if (!path)
            continue;
        bool32 keep_loaded = *(int8*)mdb_get_field(&_sound_assets.mdb,
            item->row, keep_loaded_i);
        item->asset.keep_loaded = keep_loaded ? 1 : 0;
        if (keep_loaded)
            _sound_asset_load(&item->asset, 1);
    }
    return;
    fail:
        ASSET_PANIC(path, err);
}

static void
_load_cursors(void)
{
    const char *path = "assets/cursors.cfg";
    str_cursor_table_einit(&_cursors, 8);
    int r = parse_cfg_file(path, _load_cursors_callback, 0);
    if (r)
        ASSET_PANIC(path, r);
}

static void
_load_creature_displays(void)
{
    const char *path = "assets/creature_displays.mdb";
    int err = 0;
    mdb_t mdb;
    if (mdb_open(&mdb, path))
        {err = 1; goto fail;}
    int id_index    = mdb_get_col_index_with_type(&mdb, MDB_COL_UINT32, "id");
    if (id_index < 0)
        {err = 2; goto fail;}
    int ae_set_id_index = mdb_get_col_index_with_type(&mdb, MDB_COL_UINT32,
        "ae_set_id");
    if (ae_set_id_index < 0)
        {err = 3; goto fail;}
    int name_plate_offset_x_index = mdb_get_col_index_with_type(&mdb,
        MDB_COL_INT16, "name_plate_offset_x");
    if (name_plate_offset_x_index < 0)
        {err = 4; goto fail;}
    int name_plate_offset_y_index = mdb_get_col_index_with_type(&mdb,
        MDB_COL_INT16, "name_plate_offset_y");
    if (name_plate_offset_y_index < 0)
        {err = 6; goto fail;}
    uint32 num_rows = mdb_num_rows(&mdb);
    obj_pool_init(&_creature_displays.pool, num_rows,
        sizeof(as_creature_display_t));
    uint32_as_creature_display_table_init(&_creature_displays.table, num_rows);
    for (uint32 i = 0; i < num_rows; ++i)
    {
        as_creature_display_t *display = obj_pool_reserve(
            &_creature_displays.pool);
        mdb_row_t row = mdb_get_row_by_index(&mdb, i);
        display->id     = *(uint32*)mdb_get_field(&mdb, row, id_index);
        display->ae_set = *(uint32*)mdb_get_field(&mdb, row, ae_set_id_index);
        display->name_plate_offset_x = *(uint16*)mdb_get_field(&mdb, row,
            name_plate_offset_x_index);
        display->name_plate_offset_y = *(uint16*)mdb_get_field(&mdb, row,
            name_plate_offset_y_index);
        uint32_as_creature_display_table_einsert(&_creature_displays.table,
            display->id, display);
    }
    mdb_close(&mdb);
    return;
    fail:
        ASSET_PANIC(path, err);
}

static void
_load_dynamic_object_displays(void)
{
    const char *path = "assets/dynamic_object_displays.mdb";
    int err = 0;
    mdb_t mdb;
    if (mdb_open(&mdb, path))
        {err = 1; goto fail;}
    int id_index    = mdb_get_col_index(&mdb, "id");
    if (id_index < 0)
        {err = 2; goto fail;}
    int ae_set_id_index = mdb_get_col_index(&mdb, "ae_set_id");
    if (ae_set_id_index < 0)
        {err = 3; goto fail;}
    int inventory_icon_id_index = mdb_get_col_index(&mdb, "inventory_icon_id");
    if (inventory_icon_id_index < 0)
        {err = 4; goto fail;}
    uint32 num_rows = mdb_num_rows(&mdb);
    obj_pool_init(&_dynamic_object_displays.pool, num_rows,
        sizeof(as_dynamic_object_display_t));
    uint32_as_dynamic_object_display_table_init(&_dynamic_object_displays.table,
        num_rows);
    for (uint32 i = 0; i < num_rows; ++i)
    {
        as_dynamic_object_display_t *display = obj_pool_reserve(
            &_dynamic_object_displays.pool);
        mdb_row_t row = mdb_get_row_by_index(&mdb, i);
        display->id     = *(uint32*)mdb_get_field(&mdb, row, id_index);
        display->ae_set = *(uint32*)mdb_get_field(&mdb, row, ae_set_id_index);
        display->inventory_icon_id = dstr_create(
            *(dchar**)mdb_get_field(&mdb, row, inventory_icon_id_index));
        uint32_as_dynamic_object_display_table_einsert(
            &_dynamic_object_displays.table, display->id, display);
    }
    mdb_close(&mdb);
    return;
    fail:
        ASSET_PANIC(path, err);
}

static void
_load_iso_sprites(void)
{
    const char *path = "assets/iso_sprites.mdb";
    int err = 0;
    mdb_t mdb;
    if (mdb_open(&mdb, path))
        {err = 1; goto fail;}
    int index;
    if ((index = mdb_get_col_index(&mdb, "id")) < 0)
        {err = 2; goto fail;}
    uint32 id_col_index = (uint32)index;
    if ((index = mdb_get_col_index(&mdb, "path")) < 0)
        {err = 3; goto fail;}
    uint32 path_col_index   = (uint32)index;
    if ((index = mdb_get_col_index(&mdb, "keep_loaded")) < 0)
        {err = 4; goto fail;}
    uint32 keep_loaded_col_index    = (uint32)index;
    uint32 num_rows                 = mdb_num_rows(&mdb);
    obj_pool_init(&_iso_sprites.pool, num_rows ? num_rows : 8,
        sizeof(as_iso_sprite_t));
    uint32_as_iso_sprite_table_init(&_iso_sprites.table, num_rows);
    for (uint32 i = 0; i < num_rows; ++i)
    {
        mdb_row_t       row = mdb_get_row_by_index(&mdb, i);
        as_iso_sprite_t *is = obj_pool_reserve(&_iso_sprites.pool);
        memset(is, 0, sizeof(*is));
        is->id = *(uint32*)mdb_get_field(&mdb, row, id_col_index);
        if (*(uint8*)mdb_get_field(&mdb, row, keep_loaded_col_index))
            is->flags |= AS_ISO_SPRITE_KEEP_LOADED;
        iso_sprite_load_context_t load_context = {0};
        load_context.current_direction  = -1;
        load_context.is                 = is;
        if (parse_def_file(*(dchar**)mdb_get_field(&mdb, row, path_col_index),
            _on_iso_sprite_def, _on_iso_sprite_opt, &load_context))
            {err = 5; goto fail;}
        if (!as_tex_exists(is->tex_id))
            {err = 6; goto fail;}
        if (is->flags & AS_ISO_SPRITE_KEEP_LOADED && _iso_sprite_load(is, 1))
            {err = 7; goto fail;}
        int first_have = -1;
        for (int j = 0; j < NUM_ISODIRS; ++j)
            if (load_context.have_direction[j])
            {
                first_have = j;
                break;
            }
        if (first_have == -1)
            {err = 8; goto fail;}
        for (int j = 0; j < NUM_ISODIRS; ++j)
            if (!load_context.have_direction[j])
                for (int k = 0; k < 4; ++k)
                    is->clips[j][k] = is->clips[first_have][k];
        uint32_as_iso_sprite_table_einsert(&_iso_sprites.table, is->id, is);
    }
    mdb_close(&mdb);
    return;
    fail:
        ASSET_PANIC(path, err);
}

static void
_load_fonts(void)
{
    const char *path = "assets/fonts.def";
    obj_pool_init(&_fonts.pool, 32, sizeof(gui_font_t));
    str_gui_font_table_einit(&_fonts.table, 64);
    /* Add default gui font as first font. */
    str_gui_font_table_einsert(&_fonts.table, "Default",
        gui_get_default_font());
    darr_push(_fonts.all, gui_get_default_font());
    font_load_context_t ctx = {0};
    ctx.path = dstr_create_empty(255);
    if (parse_def_file(path, _font_on_def, _font_on_val, &ctx))
        ASSET_PANIC(path, 1);
    dstr_free(&ctx.path);
    /* Fall back to GUI fonts in case no defaults are defined. */
    if (!_fonts.default_font)
        _fonts.default_font = gui_get_default_font();
    if (!_fonts.default_pixel_font)
        _fonts.default_pixel_font = gui_get_default_font();
}

static void
_destroy_tex_assets(void)
{
    tex_asset_container_item_t *item;
    for (item = _tex_assets.pool.res; item; item = item->next)
    {
        if (!(item->asset.flags & AS_TEX_LOADED))
            continue;
        if (item->asset.flags & AS_TEX_KEEP_BITMAP)
            img_free(&item->asset.bitmap);
        tex_free(&item->asset.tex);
        item->asset.flags &= ~AS_TEX_LOADED;
    }
    tex_asset_container_destroy(&_tex_assets);
}

static void
_destroy_spritesheets(void)
{
    spritesheet_container_item_t *item;
    for (item = _spritesheets.pool.res; item; item = item->next)
        _spritesheet_unload(&item->asset);
    spritesheet_container_destroy(&_spritesheets);
}

static void
_destroy_anim_assets(void)
{
    anim_asset_container_item_t *item;
    for (item = _anim_assets.pool.res; item; item = item->next)
        _anim_asset_unload(&item->asset);
    anim_asset_container_destroy(&_anim_assets);
}

static void
_destroy_ae_assets(void)
{
    ae_asset_container_item_t *item;
    for (item = _ae_assets.pool.res; item; item = item->next)
        for (int i = 0; i < 8; ++i)
        {
            ae_animation_t *animation = &item->asset.ae.anims[i];
            for (int j = 0; j < animation->num_frames; ++j)
                free(animation->frames[j].layers);
            free(animation->frames);
        }
    ae_asset_container_destroy(&_ae_assets);
}

static void
_destroy_ae_sets(void)
    {uint32_as_ae_set_table_destroy(&_ae_sets);}

static void
_destroy_sound_assets(void)
{
    sound_asset_container_item_t *item;
    for (item = _sound_assets.pool.res; item; item = item->next)
    {
        if (!item->asset.loaded)
            continue;
        au_dequeue_sound_asset(&item->asset);
        au_destroy_buffer(&item->asset.sound.buffer);
        item->asset.loaded = 0;
    }
    sound_asset_container_destroy(&_sound_assets);
}

static void
_destroy_cursors(void)
{
    const char  *key;
    as_cursor_t value;
    hashtable_for_each_pair(_cursors, key, value)
    {
        dstr_free(&value.name);
        SDL_FreeCursor(value.data);
    }
    str_cursor_table_destroy(&_cursors);
}

static void
_destroy_fonts(void)
{
    IMPLEMENTME();
}

static void
_tex_asset_async_callback(void *args)
{
    as_tex_t *ta = args;
    muta_assert(!(ta->flags & AS_TEX_LOADED));
    if (!_tex_asset_load_bitmap(ta))
    {
        if (!_push_gen_tex_job(ta))
            return;
        img_free(&ta->bitmap);
    }
    DEBUG_PRINTFF("failed.");
    ta->flags &= ~AS_TEX_LOADING;
}

static int
_tex_asset_claim(as_tex_t *ta, bool32 async)
{
    if (ta->user_count++ > 0 || ta->flags & AS_TEX_KEEP_LOADED ||
        ta->flags & AS_TEX_LOADING)
        return 0;
    if (ta->flags & AS_TEX_LOADED)
        return 0;
    int ret = 0;
    if (async)
    {
        if (_launch_tex_asset_async_load(ta))
            {ret = 1; goto fail;}
    } else
    if (_tex_asset_load_bitmap_and_generate_texture(ta))
        {ret = 2; goto fail;}
    return ret;
    fail:
        ta->user_count--;
        return ret;
}

static int
_tex_asset_load_bitmap(as_tex_t *ta)
{
    if (ta->bitmap.pixels) return 0;
    return img_load(&ta->bitmap, as_tex_get_path(ta)) ? 1 : 0;
}

static int
_tex_asset_generate_gl_texture(as_tex_t *ta)
{
    if (tex_from_img_ext(&ta->tex, &ta->bitmap,
        ta->min_filter, ta->mag_filter, ta->wrapping))
        return 2;
    if (!(ta->flags & AS_TEX_KEEP_BITMAP))
        img_free(&ta->bitmap);
    return 0;
}

static int
_tex_asset_load_bitmap_and_generate_texture(as_tex_t *ta)
{
    if (_tex_asset_load_bitmap(ta))
        return 1;
    if (_tex_asset_generate_gl_texture(ta))
        return 2;
    ta->flags |= AS_TEX_LOADED;
    return 0;
}

static int
_launch_tex_asset_async_load(as_tex_t *ta)
{
    muta_assert(!(ta->flags & AS_TEX_LOADING));
    ta->flags |= AS_TEX_LOADING;
    core_run_async_and_forget(_tex_asset_async_callback, ta);
    return 0;
}

static int
_spritesheet_load(as_spritesheet_t *sheet, bool32 async)
{
    const char *fp = as_spritesheet_get_path(sheet);
    memset(sheet, 0, sizeof(as_spritesheet_t));
    FILE *f = fopen(fp, "r");
    if (!f)
        return 1;
    int ret = 0;
    char  line[512];
    char  str_buf[512];
    int   line_len  = 512;
    int   num_clips = 0;
    int   index     = 0;
    float tmpf;
    float clip[4];
    while(fgets(line, line_len, f))
        if (sscanf(line, "%511s %f %f %f %f", str_buf, &tmpf, &tmpf, &tmpf,
            &tmpf) == 5)
            ++num_clips;
    sheet->clips        = calloc(num_clips, sizeof(float) * 4);
    sheet->clip_names   = calloc(num_clips, sizeof(dchar*));
    if(!sheet->clips || !sheet->clip_names)
    {
        ret = 2;
        goto out;
    }
    sheet->num_clips = num_clips;
    rewind(f);
    fgets(line, line_len, f);
    if (strncmp(line, "texture_id: ", 12))
        {ret = 3; goto out;}
    strncpy(str_buf, line + 11, 512);
    sheet->ta = as_claim_tex(str_to_uint32(str_buf), async);
    if (!sheet->ta)
        {ret = 4; goto out;}
    while (fgets(line, line_len, f))
    {
        if (sscanf(line, "%511s %f %f %f %f", str_buf, &clip[0], &clip[1],
            &clip[2], &clip[3]) == 5)
        {
            sheet->clips[index][0]      = clip[0];
            sheet->clips[index][1]      = clip[1];
            sheet->clips[index][2]      = clip[2];
            sheet->clips[index][3]      = clip[3];
            sheet->clip_names[index]    = create_dynamic_str(str_buf);
            if (!sheet->clip_names[index])
            {
                ret = 5;
                goto out;
            }
            index++;
        } else
            DEBUG_PRINTFF("failed to scan clip %d.\n", index);
    }
    out:
        if (ret)
        {
            for (int i = 0; i < sheet->num_clips; ++i)
                free_dynamic_str(sheet->clip_names[i]);
            free(sheet->clip_names);
            free(sheet->clips);
            as_unclaim_tex(sheet->ta);
            DEBUG_PRINTFF("failed to load '%s' with code %d!\n", fp, ret);
        }
        safe_fclose(f);
        return ret;
}

static void
_spritesheet_unload(as_spritesheet_t *ss)
{
    as_unclaim_tex(ss->ta);
    free(ss->clips);
    if (ss->clip_names)
        for (int i = 0; i < ss->num_clips; ++i)
            dstr_free(&ss->clip_names[i]);
    free(ss->clip_names);
    ss->clips       = 0;
    ss->ta          = 0;
    ss->clip_names  = 0;
    ss->num_clips   = 0;
}

static as_spritesheet_t *
_spritesheet_claim(as_spritesheet_t *ss, bool32 async)
{
    if (!ss->user_count && !ss->keep_loaded && _spritesheet_load(ss, async))
        return 0;
    ss->user_count++;
    return ss;
}

static int
_anim_asset_load(as_animation_t *aa, bool32 async)
{
    anim_t *anim        = &aa->anim;
    const char *path    = as_animation_get_path(aa);
    FILE *fp = 0;
    fp = fopen(path, "r");
    if(!fp)
        return 1;
    int ret = 0;
    char name[128] = {0};
    fgets(name, 128, fp);
    for (int i = 0; i < 128; ++i)
        if (name[i] == '\n') {name[i] = '\0'; break;}
    char line[1024];
    anim->num_frames = 0;
    int num_frames = -1;
    fgets(line, 1024, fp);
    sscanf(line, "%d", &num_frames);
    if (num_frames <= 0)
        {ret = 1; goto out;}
    anim->num_frames = num_frames;
    anim_frame_t *frames_for_anim = calloc(num_frames, sizeof(anim_frame_t));
    anim->frames = frames_for_anim;
    anim->total_dur = 0.f;
    for (int i = 0; i < num_frames; ++i)
    {
        char texture_id[128] = {0};
        fgets(texture_id, 128, fp);
        for (int j = 0; j < 128; ++j)
            if (texture_id[j] == '\n') {texture_id[j] = '\0'; break;}
        int x, y, w, h, offx, offy;
        fgets(line, 1024, fp);
        sscanf(line, "%d %d %d %d %d %d", &x, &y, &w, &h, &offx, &offy);
        float dur = 0.016667f;
        fgets(line, 1024, fp);
        sscanf(line, "%f", &dur);
        as_tex_t *frame_asset = as_claim_tex(str_to_uint32(texture_id), async);
        if (frame_asset == &_matlock.asset)
        {
            x = y = offx = offy = 0;
            w = 128;
            h = 144;
        }
        anim->frames[i].tex = &frame_asset->tex;
        anim->frames[i].clip[0] = (float)x;
        anim->frames[i].clip[1] = (float)y;
        anim->frames[i].clip[2] = (float)w;
        anim->frames[i].clip[3] = (float)h;
        anim->frames[i].ox      = (float)offx;
        anim->frames[i].oy      = (float)offy;
        anim->frames[i].dur     = dur;
        anim->total_dur += dur;
    }
    out:
        fclose(fp);
        return ret;
}

static void
_anim_asset_unload(as_animation_t *a)
{
    as_tex_t *ta;
    for (int i = 0; i < a->anim.num_frames; ++i)
    {
        if (!a->anim.frames[i].tex)
            continue;
        char *tmp   = (char*)a->anim.frames[i].tex - offsetof(as_tex_t, tex);
        ta          = (as_tex_t*)tmp;
        as_unclaim_tex(ta);
    }
    free(a->anim.frames);
    a->anim.frames      = 0;
    a->anim.num_frames  = 0;
    a->anim.total_dur   = 0.f;
}

static as_animation_t *
_anim_asset_claim(as_animation_t *aa, bool32 async)
{
    if (!aa->anim.num_frames && _anim_asset_load(aa, async))
        return 0;
    aa->user_count++;
    return aa;
}

static int
_ae_asset_load(as_ae_t *aes, bool32 async)
{
    animated_entity_t   *ae     = &aes->ae;
    const char          *path   = as_ae_get_path(aes);
    FILE *f;
    f = fopen(path, "r");
    if (!f)
        return 1;
    char name[512];
    char line[512];
    fgets(line, 512, f);
    sscanf(line, "%511s", name);
    int error_code = 0;
    for (int a = 0; a < 8; ++a)
    {
        int fc = 0;
        fgets(line, 512, f);
        fgets(line, 512, f);
        fgets(line, 512, f);
        sscanf(line, "Frames %d", &fc);
        if (fc <= 0) { error_code = 1; goto error; }
        ae_animation_t *anim = &ae->anims[a];
        anim->frames      = (ae_frame_t*)calloc(fc, sizeof(ae_frame_t));
        anim->num_frames  = fc;
        for (int i = 0; i < fc; ++i)
        {
            ae_frame_t *frame = &anim->frames[i];
            fgets(line, 512, f);
            fgets(line, 512, f);
            fgets(line, 512, f);
            int lc = 0;
            if (sscanf(line, "Layers %d", &lc) != 1)
            { error_code = 2; goto error; }
            int fox = 0, foy = 0;
            fgets(line, 512, f);
            if (sscanf(line, "Offsets %d %d", &fox, &foy) != 2)
            { error_code = 3; goto error; }
            frame->layers      = (ae_layer_t*)malloc(sizeof(ae_layer_t) * lc);
            frame->num_layers  = lc;
            frame->ox          = (float)fox;
            frame->oy          = (float)foy;
            for (int j = 0; j < lc; ++j)
            {
                ae_layer_t *layer = &frame->layers[j];
                fgets(line, 512, f);
                char tex_id[512];
                fgets(line, 512, f);
                if (strncmp(line, "Texture ", 8) || !line[8])
                    {error_code = 4; goto error;}
                strncpy(tex_id, line + 8, 512);
                str_strip_trailing_spaces(tex_id);
                int color[4];
                fgets(line, 512, f);
                if (sscanf(line, "Color %d %d %d %d",
                    &color[0], &color[1], &color[2], &color[3]) != 4)
                { error_code = 5; goto error; }
                int clip[4];
                fgets(line, 512, f);
                if (sscanf(line, "Clip %d %d %d %d",
                    &clip[0], &clip[1], &clip[2], &clip[3]) != 4)
                { error_code = 6; goto error; }
                int lox = 0, loy = 0;
                fgets(line, 512, f);
                if (sscanf(line, "Offsets %d %d", &lox, &loy) != 2)
                { error_code = 7; goto error; }
                float rot = 0;
                fgets(line, 512, f);
                if (sscanf(line, "Rotation %f", &rot) != 1)
                { error_code = 8; goto error; }
                int flip = 0;
                fgets(line, 512, f);
                if (sscanf(line, "Flip %d", &flip) != 1)
                { error_code = 9; goto error; }
                int type = NUM_AE_LAYER_TYPES;
                fgets(line, 512, f);
                if (sscanf(line, "Type %d", &type) != 1)
                { error_code = 10; goto error; }
                as_tex_t *ta = as_claim_tex(str_to_uint32(tex_id), async);
                if (ta == 0)
                { error_code = 11; goto error; }
                layer->ta       = ta;
                layer->color[0] = (uint8)color[0];
                layer->color[1] = (uint8)color[1];
                layer->color[2] = (uint8)color[2];
                layer->color[3] = (uint8)color[3];
                layer->clip[0]  = (float)clip[0];
                layer->clip[1]  = (float)clip[1];
                layer->clip[2]  = (float)clip[2];
                layer->clip[3]  = (float)clip[3];
                layer->ox       = (float)lox;
                layer->oy       = (float)loy;
                layer->rot      = rot;
                layer->flip     = flip;
                layer->type     = type;
            }
        }
    }
    safe_fclose(f);
    return 0;
    error:
    safe_fclose(f);
    char *errors = "Something went wrong";
    switch (error_code)
    {
    case 1:  errors = "Could not get frame count";      break;
    case 2:  errors = "Could not get layer count";      break;
    case 3:  errors = "Could not get frame offsets";    break;
    case 4:  errors = "Could not get texture name";     break;
    case 5:  errors = "Could not get layer color";      break;
    case 6:  errors = "Could not get texture clip";     break;
    case 7:  errors = "Could not get layer offsets";    break;
    case 8:  errors = "Could not get layer rotation";   break;
    case 9:  errors = "Could not get layer flip";       break;
    case 10: errors = "Could not get layer type";       break;
    case 11: errors = "Could not load texture";         break;
    }
    DEBUG_PRINTF("Failed to load Entity Animation, error code %d (%s)\n",
                error_code, errors);
    return error_code;
}

static void
_sound_asset_load_callback(void *args)
{
    as_sound_t *sa = args;
    muta_assert(!sa->loaded);
    int r;
    if ((r = au_create_buffer(&sa->sound.buffer, as_sound_get_path(sa))))
    {
        DEBUG_PRINTFF("error loading (code %d).\n", r);
        sa->errors_loading = 1;
    } else
        sa->errors_loading = 0;
    sa->loaded  = 1;
    sa->loading = 0;
}

static int
_sound_asset_load(as_sound_t *sa, bool32 async)
{
    muta_assert(!sa->loaded);
    const char *fp = as_sound_get_path(sa);
    if (!fp) return 1;
    muta_assert(!sa->loading);
    if (async)
    {
        sa->loading = 1;
        core_run_async_and_forget(_sound_asset_load_callback, sa);
    } else
    if (au_create_buffer(&sa->sound.buffer, fp))
    {
        sa->loaded = 1;
        return 3;
    }
    return 0;
}

static int
_claim_sound(as_sound_t *sa, bool32 async)
{
    if (sa->user_count++ > 0)
        return 0;
    if (sa->loaded)
        return 0;
    if (!sa->loading && _sound_asset_load(sa, async))
    {
        sa->user_count--;
        return 1;
    }
    return 0;
}

static void
_load_cursors_callback(void *ctx, const char *opt, const char *val)
{
    if (str_cursor_table_exists(&_cursors, opt))
    {
        LOG_ERR("Warning: cursor '%s' defined twice.", opt);
        return;
    }
    img_t img;
    if (img_load(&img, val))
    {
        LOG_ERR("Error: failed to load cursor '%s' from path '%s'.\n", opt, val);
        return;
    }
    uint32      r           = TO_SYS_ENDIAN32(0x000000ff);
    uint32      g           = TO_SYS_ENDIAN32(0x0000ff00);
    uint32      b           = TO_SYS_ENDIAN32(0x00ff0000);
    uint32      a           = TO_SYS_ENDIAN32(0xff000000);
    int         err         = 0;
    as_cursor_t cursor      = {0};
    SDL_Surface *surface  = SDL_CreateRGBSurfaceFrom(img.pixels, img.w, img.h,
        img.pxf == IMG_RGB ? 24 : 32, (img.pxf == IMG_RGB ? 3 : 4) * 17, r, g,
        b, a);
    if (!surface)
        {err = 1; goto out;}
    cursor.data = SDL_CreateColorCursor(surface, 0, 0);
    if (!cursor.data)
        {err = 1; goto out;}
    cursor.name = dstr_create(opt);
    str_cursor_table_einsert(&_cursors, opt, cursor);
    out:
        if (err)
        {
            dstr_free(&cursor.name);
            if (cursor.data)
                SDL_FreeCursor(cursor.data);
            LOG_ERR("Error loading cursor '%s', code %d.", opt, err);
        } else
            LOG_DEBUG("Loaded cursor '%s'.", cursor.name);
        SDL_FreeSurface(surface);
        img_free(&img);
}

static int
_on_iso_sprite_def(parse_def_file_context_t *ctx, const char *def,
    const char *val)
{
    iso_sprite_load_context_t *load_context = ctx->user_data;
    if (streq(def, "direction"))
    {
        if (streq(val, "north"))
            load_context->current_direction = ISODIR_NORTH;
        else if (streq(val, "north_east"))
            load_context->current_direction = ISODIR_NORTH_EAST;
        else if (streq(val, "east"))
            load_context->current_direction = ISODIR_EAST;
        else if (streq(val, "south_east"))
            load_context->current_direction = ISODIR_SOUTH_EAST;
        else if (streq(val, "south"))
            load_context->current_direction = ISODIR_SOUTH;
        else if (streq(val, "south_west"))
            load_context->current_direction = ISODIR_SOUTH_WEST;
        else if (streq(val, "west"))
            load_context->current_direction = ISODIR_WEST;
        else if (streq(val, "north_west"))
            load_context->current_direction = ISODIR_NORTH_WEST;
        else
            return 1;
        if (load_context->have_direction[load_context->current_direction])
            return 2;
        load_context->have_direction[load_context->current_direction] = 1;
    } else
    if (streq(def, "texture_id"))
    {
        if (load_context->have_texture)
            return 3;
        load_context->is->tex_id        = str_to_uint32(val);
        load_context->have_texture      = 1;
        load_context->current_direction = -1;
    }
    return 0;
}

static int
_on_iso_sprite_opt(parse_def_file_context_t *ctx, const char *opt,
    const char *val)
{
    iso_sprite_load_context_t *load_context = ctx->user_data;
    if (load_context->current_direction == -1)
        return 1;
    float *clip = load_context->is->clips[load_context->current_direction];
    if (streq(opt, "x1"))
        clip[0] = (float)atof(val);
    else if (streq(opt, "y1"))
        clip[1] = (float)atof(val);
    else if (streq(opt, "x2"))
        clip[2] = (float)atof(val);
    else if (streq(opt, "y2"))
        clip[3] = (float)atof(val);
    else if (streq(opt, "offset_x"))
        load_context->is->offsets[load_context->current_direction][0] =
            (float)atof(val);
    else if (streq(opt, "offset_y"))
        load_context->is->offsets[load_context->current_direction][1] =
            (float)atof(val);
    else
        return 2;
    return 0;
}

static int
_iso_sprite_load(as_iso_sprite_t *is, bool32 async)
    {return (is->ta = as_claim_tex(is->tex_id, async)) ? 0 : 1;}

static int
_load_font(gui_font_t *font, const char *path, int font_size)
{
    if (gui_font_load_from_file(font, path, font_size))
        return 1;
    IMPLEMENTME();
    if (gl_generate_gui_font_texture(font))
        return 2;
    return 0;
}

static int
_font_on_def(parse_def_file_context_t *ctx, const char *def, const char *val)
{
    int default_type = FONT_DEFAULT_NONE;
    if (streq(def, "font"))
        default_type = FONT_DEFAULT_NONE;
    else if (streq(def, "default_font"))
        default_type = FONT_DEFAULT;
    else if (streq(def, "default_pixel_font"))
        default_type = FONT_DEFAULT_PIXEL;
    else
        return 1;
    if (str_gui_font_table_exists(&_fonts.table, val))
    {
        LOG("Warning: font '%s' defined more than once. Using first "
            "definition.", val);
        return 2;
    }
    font_load_context_t *lc = ctx->user_data;
    gui_font_t *font = obj_pool_reserve(&_fonts.pool);
    memset(font, 0, sizeof(gui_font_t));
    str_gui_font_table_einsert(&_fonts.table, val, font);
    lc->font = font;
    set_dynamic_str_len(lc->path, 0);
    lc->size            = -1;
    lc->default_type    = default_type;
    darr_push(_fonts.all, font);
    return 0;
}

static int
_font_on_val(parse_def_file_context_t *ctx, const char *opt, const char *val)
{
    font_load_context_t *lc = ctx->user_data;
    if (!lc->font)
        return 3;
    if (streq(opt, "path"))
        dstr_set(&lc->path, val);
    else if (streq(opt, "size"))
        lc->size = atoi(val);
    else
        return 3;
    if (dstr_len(lc->path) && lc->size > 0)
    {
        int r = _load_font(lc->font, lc->path, lc->size);
        if (!r)
        {
            if (lc->default_type == FONT_DEFAULT || !_fonts.default_font)
            {
                _fonts.default_font = lc->font;
                if (!_fonts.default_pixel_font)
                    _fonts.default_pixel_font = lc->font;
            } else
            if (lc->default_type == FONT_DEFAULT_PIXEL)
                _fonts.default_pixel_font = lc->font;
        }
        lc->font            = 0;
        lc->default_type    = FONT_DEFAULT_NONE;
    }
    return 0;
}

static int
_push_gen_tex_job(as_tex_t *ta)
{
    int ret = 0;

    mutex_lock(&tex_jobs.mtx);

    if (ta->tex.id != 0)
        goto close;

    for (int i = 0; i < tex_jobs.num; ++i)
        if (tex_jobs.jobs[i].ta == ta)
            goto close;

    if (tex_jobs.num == tex_jobs.max)
    {
        uint32 new_max = tex_jobs.max * 2;
        uint32 req_max = tex_jobs.max + 4;
        if (new_max < req_max)
            new_max = req_max;
        tex_job_t *new_jobs = realloc(tex_jobs.jobs,
            (new_max) * sizeof(tex_job_t));

        if (new_jobs == 0)
        {
            ret = 1;
            goto close;
        }

        tex_jobs.jobs = new_jobs;
        tex_jobs.max  = new_max;
    }

    tex_jobs.jobs[tex_jobs.num++].ta = ta;

    close:
        mutex_unlock(&tex_jobs.mtx);
        return ret;
}
