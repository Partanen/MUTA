/* Connection handling to the login server */

#ifndef MUTA_CLIENT_LOGIN_H
#define MUTA_CLIENT_LOGIN_H

#include "../../shared/common_defs.h"

#define LOGIN_MAX_SHARD_INFOS 256

/* Forward declaration(s) */
typedef struct cl_read_event_t cl_read_event_t;

/* Types defined here */
typedef struct login_shard_info_t login_shard_info_t;

enum login_status
{
    LOGIN_STATUS_DISCONNECTED,
    LOGIN_STATUS_CONNECTING,
    LOGIN_STATUS_HANDSHAKING,
    LOGIN_STATUS_SELECTING_SHARD,
    LOGIN_STATUS_SELECTED_SHARD
};

enum login_error
{
    LOGIN_ERROR_NONE = 0,
    LOGIN_ERROR_UNABLE_TO_CONNECT,
    LOGIN_ERROR_SYSTEM,
    LOGIN_ERROR_SERVER_CLOSED_CONNECTION,
    LOGIN_ERROR_BAD_MSG, /* Server sent an invalid message */
    LOGIN_ERROR_SEND_FAILED,
    LOGIN_ERROR_ACCOUNT_NAME_TOO_SHORT,
    LOGIN_ERROR_ACCOUNT_NAME_TOO_LONG,
    LOGIN_ERROR_PASSWORD_TOO_SHORT,
    LOGIN_ERROR_PASSWORD_TOO_LONG
};

struct login_shard_info_t
{
    bool32  online;
    char    name[MAX_SHARD_NAME_LEN + 1];
};

int
login_init(void);

void
login_destroy(void);

int
login_get_status(void);
/* Returns one of enum login_status */

int
login_get_last_error(void);
/* If the last connection attempt failed, will return one of enum login_error */

int
login_connect(const char *account_name, const char *password);
/* Does nothing if status is not LOGIN_STATUS_DISCONNECTED */

void
login_disconnect(void);
/* Safe to call even if already disconnected */

void
login_disconnect_with_error(int error);

void
login_flush(void);

uint32
login_get_shard_infos(login_shard_info_t *ret_infos, uint32 max);

int
login_select_shard(uint32 index);

void
login_cancel_select_shard();

const char *
login_error_to_string(int error);

/*-- Event handlers, called from the main thread. --*/

void
login_read(cl_read_event_t *event);

void
login_on_connected(void);

#endif /* MUTA_CLIENT_LOGIN_H */
