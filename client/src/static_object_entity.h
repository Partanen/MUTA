#ifndef MUTA_CLIENT_STATIC_OBJECT_ENTITY_H
#define MUTA_CLIENT_STATIC_OBJECT_ENTITY_H

#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct entity_t entity_t;
typedef struct world_t  world_t;

/* Types defined here */
typedef struct static_object_entity_t static_object_entity_t;

struct static_object_entity_t
{
    sobj_type_id_t  type_id; /* Cannot be invalid */
    uint32          index_in_cache;
};

entity_t *
static_object_spawn(world_t *world, creature_type_id_t type, int direction,
    int position[3]);

#endif /* MUTA_CLIENT_STATIC_OBJECT_ENTITY_H */
