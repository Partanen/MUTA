#include "world.h"
#include "components.h"
#include "core.h"
#include "log.h"
#include "../../shared/kthp.h"
#include "../../shared/muta_map_format.h"
#include "../../shared/entities.h"
#include "../../shared/tile.h"

#define COMPUTE_CACHE_PTR_COORDS(map, cache, ret_x, ret_y) \
    *(ret_x) = (int)((cache) - (map)->cache_ptrs) % 3; \
    *(ret_y) = (int)((cache) - (map)->cache_ptrs) / 3;

struct world_event_listener_t
{
    world_event_callback_t  callback;
    void                    *user_data;
};

static muta_map_db_t _map_db;

static void
_compute_cache_coords(world_t *world, chunk_cache_t **cache, int *ret_x,
    int *ret_y);

static void
_post_event(world_event_t *event);

static void
_set_camera_position(world_t *world, int chunk_x, int chunk_y);
/* Values must be clamped to map size w in chunks - 3 and h in chunks - 3. */

static void
_wait_for_loading_caches(world_t *world);

static void
_load_cache_callback(void *args);
/* Run async */

static void
_copy_loaded_tiles_callback(void *args);
/* Run async. Copy loaded tiles from a muta_map_chunk_file_t to chunk cache's
 * tile array. */

static void
_clear_cache_static_objects(chunk_cache_t *cache);

static void
_clear_cache(chunk_cache_t *cache);

int
world_api_init(void)
{
    const char  *err_str = 0;
    int         err;
    if ((err = tile_load(1)))
    {
        err_str = "Missing or corrupted data: tile definitions";
        goto fail;
    }
    if ((err = ent_load(0)))
    {
        if (err & ENT_LOAD_CREATURES)
            err_str ="Missing or corrupted data: creature definitions";
        else if (err & ENT_LOAD_PLAYER_RACES)
            err_str ="Missing or corrupted data: player race definitions";
        else if (err & ENT_LOAD_DYNAMIC_OBJECTS)
            err_str ="Missing or corrupted data: dynamic object definitions";
        else if (err & ENT_LOAD_STATIC_OBJECTS)
            err_str ="Missing or corrupted data: static object definitions";
        else
            muta_assert(0);
        goto fail;
    }
    if ((err = muta_map_db_load(&_map_db, "data/common/maps.def")))
    {
        err_str = "Missing or corrupted data: map database";
        goto fail;
    }
    if ((err = entity_api_init()))
    {
        err_str = "Failed to initialize entity API.";
        goto fail;
    }
    return 0;
    fail:
        LOG_ERR("Failed to init world API with error %s.", err_str);
        muta_panic_print("%s (code %d).", err_str, err);
        return 1;
}

void
world_api_destroy(void)
{
    muta_map_db_destroy(&_map_db);
    entity_api_destroy();
    ent_destroy();
}

int
world_init(world_t *world, uint32 width_in_chunks, uint32 height_in_chunks,
    uint32 max_entities)
{
    int ret = 0;
    memset(world, 0, sizeof(world_t));
    world->map.w    = width_in_chunks;
    world->map.h    = height_in_chunks;
    world->map.tw   = width_in_chunks * MAP_CHUNK_W;
    world->map.th   = height_in_chunks * MAP_CHUNK_W;
    for (int i = 0; i < 9; ++i)
    {
        const uint32 num_to_reserve = 512;
        darr_reserve(world->map.caches[i].static_objects, num_to_reserve);
        world->map.caches[i].world = world;
        muta_chunk_file_init(&world->map.caches[i].chunk_file);
    }
    for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 3; ++j)
        {
            int k = j * 3 + i;
            world->map.cache_ptrs[k] = &world->map.caches[k];
        }
    if (entity_pool_init(&world->entities, max_entities))
        {ret = 2; goto fail;}
    int err;
    hashtable_init(world->player_table, max_entities, &err);
    if (err)
        {ret = 3; goto fail;}
    hashtable_init(world->creature_table, max_entities, &err);
    if (err)
        {ret = 4; goto fail;}
    world->timestep         = 1.0 / 60.0;
    world->tick_accumulator = 0;
    for (uint32 i = 0; i < NUM_COMPONENT_DEFINITIONS; ++i)
        if (component_definitions[i]->system_init &&
            component_definitions[i]->system_init(world, max_entities))
            {ret = 5; goto fail;}
    return ret;
    fail:
        world_destroy(world);
        return ret;
}

void
world_destroy(world_t *world)
{
    _wait_for_loading_caches(world);
    for (int i = (int)NUM_COMPONENT_DEFINITIONS - 1; i >= 0; --i)
        if (component_definitions[i]->system_destroy)
            component_definitions[i]->system_destroy(world);
    entity_pool_destroy(&world->entities);
    hashtable_destroy(world->player_table, 0);
    hashtable_destroy(world->creature_table, 0);
    hashtable_destroy(world->dynamic_object_table, 0);
    for (int i = 0; i < NUM_WORLD_EVENTS; ++i)
        darr_free(world->event_listeners[i]);
    for (int i = 0; i < 9; ++i)
        muta_chunk_file_destroy(&world->map.caches[i].chunk_file);
    muta_map_file_destroy(&world->map.file);
    for (int i = 0; i < 9; ++i)
        darr_free(world->map.caches[i].static_objects);
    memset(world, 0, sizeof(*world));
}

int
world_load_map(world_t *world, uint32 map_id)
{
    muta_map_db_entry_t *entry = muta_map_db_get_entry_by_id(&_map_db, map_id);
    if (!entry)
        return 1;
    if (world_load_map_from_path(world, entry->file_path))
        return 2;
    world->map.id = map_id;
    return 0;
}

int
world_load_map_from_path(world_t *world, const char *file_path)
{
    _wait_for_loading_caches(world);
    /*-- Despawn all entities --*/
    entity_pool_clear(&world->entities);
    hashtable_clear(world->player_table, 0);
    hashtable_clear(world->creature_table, 0);
    world->tick_accumulator = 0;
    if (muta_map_file_load(&world->map.file, file_path))
        return 1;
    world->map.id                   = 0xFFFFFFFF;
    world->map.w                    = world->map.file.header.w;
    world->map.h                    = world->map.file.header.h;
    world->map.tw                   = muta_map_file_tw(&world->map.file);
    world->map.th                   = muta_map_file_th(&world->map.file);
    world->map.loaded_any_caches    = 0;
    for (int i = 0; i < 9; ++i)
        _clear_cache(&world->map.caches[i]);
    _set_camera_position(world, 0, 0);
    world_event_t event;
    event.type  = WORLD_EVENT_MAP_LOAD;
    event.world = world;
    _post_event(&event);
    return 0;

}

bool32
world_set_camera_position(world_t *world, int chunk_x, int chunk_y)
{
    int cx = CLAMP(chunk_x, 0, world->map.w - 3);
    int cy = CLAMP(chunk_y, 0, world->map.h - 3);
    if (cx == world->map.cam_x && cy == world->map.cam_y)
        return 0;
    _set_camera_position(world, cx, cy);
    return 1;
}

void
world_update(world_t *world, double delta)
{
    float timestep = (float)world->timestep;
    world->tick_accumulator += delta;
    while (world->tick_accumulator >= timestep)
    {
        _wait_for_loading_caches(world);
        world->tick_accumulator -= timestep;
        for (uint32 i = 0; i < NUM_COMPONENT_DEFINITIONS; ++i)
            if (component_definitions[i]->system_update)
                component_definitions[i]->system_update(world, timestep);
    }
}

entity_t *
world_get_player(world_t *world, player_runtime_id_t id)
{
    entity_t **entity = hashtable_find(world->player_table, id,
        hashtable_hash(&id, sizeof(id)));
    return entity ? *entity : 0;
}

entity_t *
world_get_creature(world_t *world, creature_runtime_id_t id)
{
    entity_t **entity = hashtable_find(world->creature_table, id,
        hashtable_hash(&id, sizeof(id)));
    return entity ? *entity : 0;
}

entity_t *
world_get_dynamic_object(world_t *world, dobj_runtime_id_t id)
{
    entity_t **entity = hashtable_find(world->dynamic_object_table, id,
        hashtable_hash(&id, sizeof(id)));
    return entity ? *entity : 0;
}

entity_t *
world_get_entity(world_t *world, entity_handle_t handle)
{
    entity_t *entity;
    switch (handle.type)
    {
    case ENTITY_TYPE_PLAYER:
        entity = world_get_player(world, handle.player);
        break;
    case ENTITY_TYPE_CREATURE:
        entity = world_get_creature(world, handle.creature);
        break;
    case ENTITY_TYPE_DYNAMIC_OBJECT:
        entity = world_get_dynamic_object(world, handle.dynamic_object);
        break;
    default:
        muta_assert(0);
        entity = 0;
        break;
    }
    return entity;
}

void
world_listen_to_event(world_t *world, int event,
    world_event_callback_t callback, void *user_data)
{
    /* Make sure this callback wasn't registered before. */
    uint32 num_listeners = darr_num(world->event_listeners[event]);
    for (uint32 i = 0; i < num_listeners; ++i)
        if (world->event_listeners[event][i].callback == callback)
            return;
    struct world_event_listener_t listener = {
        .user_data  = user_data,
        .callback   = callback};
    darr_push(world->event_listeners[event], listener);
}

void
world_stop_listening_to_event(world_t *world, int event,
    world_event_callback_t callback)
{
    uint32 num_listeners = darr_num(world->event_listeners[event]);
    for (uint32 i = 0; i < num_listeners; ++i)
        if (world->event_listeners[event][i].callback == callback)
        {
            darr_erase(world->event_listeners[event], i);
            break;
        }
}

bool32
world_is_loading(world_t *world)
{
    for (int i = 0; i < 9; ++i)
        if (world->map.caches[i].async_job)
            return 1;
    return 0;
}

int
world_set_tile(world_t *world, int x, int y, int z, tile_t tile)
{
    if (world_is_loading(world))
        return 1;
    if (x < 0 || x >= world->map.tw || y < 0 || y >= world->map.th ||
        z < 0 || z >= MAP_CHUNK_T)
        return 2;
    chunk_cache_t *cache = world_get_chunk_cache_of_tile(world, x, y);
    if (!cache)
        return 3;
    int index = CHUNK_COMPUTE_TILE_INDEX(x, y, w);
    if (cache->tiles[index] == tile)
        return 4;
    world_event_t event = {
        .type                       = WORLD_EVENT_TILE_CHANGED,
        .world                      = world,
        .tile_changed.previous_tile = cache->tiles[index],
        .tile_changed.new_tile      = tile,
        .tile_changed.position[0]   = x,
        .tile_changed.position[1]   = y,
        .tile_changed.position[2]   = z};
    cache->tiles[index] = tile;
    _post_event(&event);
    return 0;
}

int
chunk_cache_save(chunk_cache_t *cache)
{
    _wait_for_loading_caches(cache->world);
    memcpy(cache->chunk_file.tiles, cache->tiles,
        MAP_CHUNK_SIZE_IN_TILES * sizeof(tile_t));
    muta_chunk_file_clear_static_objects(&cache->chunk_file);
    uint32 num_static_objs = darr_num(cache->static_objects);
    for (uint32 i = 0; i < num_static_objs; ++i)
    {
        entity_t    *e  = cache->static_objects[i];
        int         pos[3];
        entity_get_position(e, pos);
        if (muta_chunk_file_push_static_obj(&cache->chunk_file,
                entity_get_type_data(e)->static_object.type_id,
                (uint8)entity_get_direction(e),
                (uint8)pos[0] - cache->x_in_chunks * MAP_CHUNK_W,
                (uint8)pos[1] - cache->y_in_chunks * MAP_CHUNK_W,
                (uint8)pos[2]))
            return 1;
    }
    int err = muta_chunk_file_save(&cache->chunk_file,
        muta_map_file_get_chunk_absolute_path(&cache->world->map.file,
        cache->x_in_chunks, cache->y_in_chunks));
    if (err)
    {
        DEBUG_PRINTFF("muta_chunk_file_save() failed with code %d.\n", err);
        return 2;
    }
    return 0;
}

static void
_compute_cache_coords(world_t *world, chunk_cache_t **cache, int *ret_x,
    int *ret_y)
{
    *(ret_x) = (int)(cache - world->map.cache_ptrs) % 3;
    *(ret_y) = (int)(cache - world->map.cache_ptrs) / 3;
}

static void
_post_event(world_event_t *event)
{
    uint32 num_callbacks = darr_num(event->world->event_listeners[event->type]);
    for (uint32 i = 0; i < num_callbacks; ++i)
        event->world->event_listeners[event->type][i].callback(event,
            event->world->event_listeners[event->type][i].user_data);
}

static void
_set_camera_position(world_t *world, int cx, int cy)
{
    _wait_for_loading_caches(world);
    int i, j, k, index;
    /* First figure out the caches that have relevant data readily loaded so
     * that we don't need to load it again. Simply move the cache pointers
     * around. */
    chunk_cache_t *cache_ptrs[9] = {0};
    bool32 is_cache_used[9] = {0};
    /*-- Set cache pointers to caches already loaded. --*/
    if (world->map.loaded_any_caches)
    {
        for (i = 0; i < 3; ++i)
            for (j = 0; j < 3; ++j)
            {
                index = j * 3 + i;
                for (k = 0; k < 9; ++k)
                {
                    int32 x_in_chunks = cx + i;
                    int32 y_in_chunks = cy + j;
                    if (world->map.caches[k].x_in_chunks == x_in_chunks &&
                        world->map.caches[k].y_in_chunks == y_in_chunks)
                    {
                        cache_ptrs[index]   = &world->map.caches[k];
                        is_cache_used[k]    = 1;
                        break;
                    }
                }
            }
        /* Post event. */
        for (int i = 0; i < 9; ++i)
        {
            if (is_cache_used[i])
                continue;
            chunk_cache_t *cache = &world->map.caches[i];
            world_event_t event = {
                .type   = WORLD_EVENT_WILL_UNLOAD_CHUNK_OUTSIDE_CAMERA,
                .world  = world,
                .will_unload_chunk_outside_camera.chunk_cache = cache};
            _post_event(&event);
        }
    }
    /*-- Remove static objects from the chunks that will now be left out --*/
    for (i = 0; i < 9; ++i)
        if (!is_cache_used[i])
            _clear_cache_static_objects(&world->map.caches[i]);
    /* Create asynchronous jobs to load all the caches from disk that need it.
     * */
    for (i = 0; i < 9; ++i)
    {
        if (cache_ptrs[i]) /* Assigned to loaded chunk. */
            continue;
        /*-- Find an unassigned cache for ptr i --*/
        for (j = 0; j < 9; ++j)
        {
            if (is_cache_used[j])
                continue;
            cache_ptrs[i]       = &world->map.caches[j];
            is_cache_used[j]    = 1;
            int cache_x = i % 3;
            int cache_y = i / 3;
            world->map.caches[j].x_in_chunks = cx + cache_x;
            world->map.caches[j].y_in_chunks = cy + cache_y;
            muta_assert(!world->map.caches[j].async_job);
            world->map.caches[j].async_job = core_run_async(
                _load_cache_callback, &world->map.caches[j]);
            break;
        }
    }
    world->map.cam_x                = cx;
    world->map.cam_y                = cy;
    world->map.cam_tx               = cx * MAP_CHUNK_W;
    world->map.cam_ty               = cy * MAP_CHUNK_W;
    world->map.loaded_any_caches    = 1;
    for (int i = 0; i < 9; ++i)
    {
        muta_assert(cache_ptrs[i]);
        world->map.cache_ptrs[i] = cache_ptrs[i];
    }
}

static void
_wait_for_loading_caches(world_t *world)
{
    world_event_t   events[9];
    int             num_events = 0;
    async_job_t     *tile_copy_jobs[9];
    int             num_tile_copy_jobs = 0;
    for (int i = 0; i < 9; ++i)
        if (world->map.caches[i].async_job)
        {
            chunk_cache_t *cache = &world->map.caches[i];
            int r = core_async_wait(cache->async_job, 0, -1);
            if (r)
                muta_panic_print("core_async_wait() failed unexpectedly with "
                    "code %d!", r);
            cache->async_job = 0;
            world_event_t *event = &events[num_events++];
            event->type = WORLD_EVENT_CHUNK_LOAD_FINISHED;
            event->world                            = world;
            event->chunk_load_finished.error        = cache->load_result;
            event->chunk_load_finished.chunk_cache  = cache;
            if (cache->load_result) /* Loading failed */
            {
                LOG_ERR("Loading cache from chunk file %s failed!",
                    muta_map_file_get_chunk_absolute_path(&world->map.file,
                        cache->x_in_chunks, cache->y_in_chunks));
                _clear_cache(cache);
                continue;
            } else
                _clear_cache_static_objects(cache);
            /* Start moving the tiles to the correct array asynchronously. */
            tile_copy_jobs[num_tile_copy_jobs++] = core_run_async(
                _copy_loaded_tiles_callback, cache);
            /* Load static objects. */
            int chunk_x, chunk_y;
            _compute_cache_coords(world, &world->map.cache_ptrs[i], &chunk_x,
                &chunk_y);
            int     chunk_tx            = chunk_x * MAP_CHUNK_W;
            int     chunk_ty            = chunk_x * MAP_CHUNK_W;
            uint32  num_static_objects  =
                cache->chunk_file.header.num_static_objs;
            LOG_DEBUG("num_static_objects from file: %u.", num_static_objects);
            for (uint32 j = 0; j < num_static_objects; ++j)
            {
                stored_static_obj_t *obj = &cache->chunk_file.static_objs[j];
                /* Translate position to map. */
                int position[3] = {
                    chunk_tx + obj->x,
                    chunk_ty + obj->y,
                    obj->z};
                static_object_spawn(world, obj->type_id, obj->dir, position);
            }
            muta_chunk_file_clear_static_objects(&cache->chunk_file);
            LOG("Successfully loaded chunk file %s.",
                muta_map_file_get_chunk_absolute_path(&world->map.file,
                    cache->x_in_chunks, cache->y_in_chunks));
        }
    for (int i = 0; i < num_tile_copy_jobs; ++i)
        core_async_wait(tile_copy_jobs[i], 0, -1);
    for (int i = 0; i < num_events; ++i)
        _post_event(&events[i]);
}

static void
_load_cache_callback(void *args)
{
    chunk_cache_t *cache = args;
    const char *path = muta_map_file_get_chunk_absolute_path(
        &cache->world->map.file, cache->x_in_chunks, cache->y_in_chunks);
    DEBUG_PRINTFF("Attempting to load chunk file from path '%s'.\n", path);
    cache->load_result = muta_chunk_file_load(&cache->chunk_file, path);
}

static void
_copy_loaded_tiles_callback(void *args)
{
    chunk_cache_t *cache = args;
    memcpy(cache->tiles, cache->chunk_file.tiles,
        MAP_CHUNK_SIZE_IN_TILES * sizeof(tile_t));
}

static void
_clear_cache_static_objects(chunk_cache_t *cache)
{
    while (darr_num(cache->static_objects))
        entity_despawn(cache->static_objects[0]);
}

static void
_clear_cache(chunk_cache_t *cache)
{
    _clear_cache_static_objects(cache);
    muta_chunk_file_clear_static_objects(&cache->chunk_file);
    memset(cache->tiles, 0, sizeof(cache->tiles));
}
