#include "mobility_component.h"
#include "world.h"
#include "entity.h"
#include "component.h"
#include "components.h"

struct mobility_component_t
{
    entity_t    *entity;
    uint32      data_index; /* 0xFFFFFFFF if not moving */
    float       move_speed;
};

struct mobility_component_data_t
{
    uint32  component_index;
    float   move_timer;
    float   time_to_move;
};

static int
_mobility_system_init(world_t *world, uint32 max_components);

static void
_mobility_system_destroy(world_t *world);

static void
_mobility_system_update(world_t *world, float dt);

static component_handle_t
_mobility_component_attach(entity_t *entity);

static void
_mobility_component_detach(component_handle_t handle);

static void
_remove_data(mobility_component_t *component, mobility_system_t *system);

component_definition_t mobility_component_definition =
{
    _mobility_system_init,
    _mobility_system_destroy,
    _mobility_system_update,
    _mobility_component_attach,
    _mobility_component_detach
};

void
mobility_component_set_speed(component_handle_t handle, float speed)
{
    mobility_component_t *component = handle;
    component->move_speed = speed;
}

void
mobility_component_move(component_handle_t handle, int *position)
{
    mobility_component_t *component = handle;
    int current_position[3];
    entity_get_position(component->entity, current_position);
    if (!memcmp(current_position, position, 3 * sizeof(int)))
        return;
    mobility_system_t *system = &entity_get_world(
        component->entity)->mobility_system;
    if (component->move_speed > 0)
    {
        /*-- Create new data if one doesn't exist --*/
        if (component->data_index == 0xFFFFFFFF)
            component->data_index = system->num_moving++;
        mobility_component_data_t *data =
            &system->moving[component->data_index];
        data->component_index = fixed_pool_index(&system->components,
            component);
        data->move_timer    = 0;
        data->time_to_move  = component->move_speed;
    } else /* If time <= 0, remove from moving list. */
    if (component->data_index != 0xFFFFFFFF)
        _remove_data(component, system);
    entity_set_position(component->entity, position);
    mobility_component_event_t event;
    event.type                  = MOBILITY_COMPONENT_EVENT_START_MOVE;
    event.start_move.seconds    = component->move_speed;
    for (int i = 0; i < 3; ++i)
        event.start_move.new_position[i] = position[i];
    for (int i = 0; i < 3; ++i)
        event.start_move.last_position[i] = current_position[i];
    entity_post_component_event(component->entity,
        &mobility_component_definition, &event);
    if (component->move_speed > 0)
        return;
    /*-- Post a stop move event if moved instantly --*/
    event.type = MOBILITY_COMPONENT_EVENT_STOP_MOVE;
    entity_post_component_event(component->entity,
        &mobility_component_definition, &event);
}

float
mobility_component_get_percentage_travelled(component_handle_t handle)
{
    mobility_component_t *component = handle;
    if (component->data_index == 0xFFFFFFFF)
        return 1.f;
    mobility_system_t *system = &entity_get_world(
        component->entity)->mobility_system;
    mobility_component_data_t *data = &system->moving[component->data_index];
    return data->move_timer / data->time_to_move;
}

bool32
mobility_component_is_moving(component_handle_t handle)
    {return mobility_component_get_percentage_travelled(handle) != 1.0f;}

static int
_mobility_system_init(world_t *world, uint32 max_components)
{
    mobility_system_t *system = &world->mobility_system;
    fixed_pool_init(&system->components, max_components);
    system->moving = emalloc(
        max_components * sizeof(mobility_component_data_t));
    system->num_moving  = 0;
    return 0;
}

static void
_mobility_system_destroy(world_t *world)
{
    mobility_system_t *system= &world->mobility_system;
    fixed_pool_destroy(&system->components);
    free(system->moving);
    memset(system, 0, sizeof(mobility_system_t));
}

static void
_mobility_system_update(world_t *world, float dt)
{
    mobility_system_t           *system     = &world->mobility_system;
    mobility_component_data_t   *moving     = system->moving;
    uint32                      num_moving  = world->mobility_system.num_moving;
    mobility_component_event_t  event;
    for (uint32 i = 0; i < num_moving; ++i)
    {
        loop_begin: {}
        mobility_component_data_t *data = &moving[i];
        entity_t *entity = system->components.all[data->component_index].entity;
        data->move_timer += dt;
        if (data->move_timer < data->time_to_move)
        {
            float percentage = data->move_timer / data->time_to_move;
            event.type                          = MOBILITY_COMPONENT_EVENT_MOVED;
            event.moved.percentage_travelled    = percentage;
            entity_post_component_event(entity, &mobility_component_definition,
                &event);
            continue;
        }
        system->components.all[data->component_index].data_index = 0xFFFFFFFF;
        *data = moving[--num_moving];
        if (i < num_moving)
            system->components.all[data->component_index].data_index = i;
        event.type = MOBILITY_COMPONENT_EVENT_STOP_MOVE;
        entity_post_component_event(entity, &mobility_component_definition,
            &event);
        if (i < num_moving)
            goto loop_begin;
    }
    system->num_moving = num_moving;
}

static component_handle_t
_mobility_component_attach(entity_t *entity)
{
    mobility_system_t *system =
        &entity_get_world(entity)->mobility_system;
    mobility_component_t *component = fixed_pool_new(&system->components);
    if (!component)
        return 0;
    int position[3];
    entity_get_position(entity, position);
    component->entity       = entity;
    component->data_index   = 0xFFFFFFFF;
    component->move_speed   = 1.f;
    return component;
}

static void
_mobility_component_detach(component_handle_t handle)
{
    mobility_component_t *component = handle;
    mobility_system_t *system =
        &entity_get_world(component->entity)->mobility_system;
    if (component->data_index != 0xFFFFFFFF)
    {
        /* Swap the last data in the place of this one */
        mobility_component_data_t *data =
            &system->moving[component->data_index];
        *data = system->moving[--system->num_moving];
        system->components.all[data->component_index].data_index =
            component->data_index;
    }
    fixed_pool_free(&system->components, component);
}

static void
_remove_data(mobility_component_t *component, mobility_system_t *system)
{
    muta_assert(0);
    uint32 data_index = component->data_index;
    component->data_index = 0xFFFFFFFF;
    system->moving[data_index] = system->moving[--system->num_moving];
    if (!system->num_moving)
        return;
    system->components.all[
        system->moving[data_index].component_index].data_index = data_index;
}
