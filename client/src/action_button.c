#include "action_button.h"
#include "icon.h"
#include "../../shared/abilities.h"

void
action_button_set_ability(action_button_t *button, ability_def_t *ability)
{
    float *clip;
    if (ability)
        clip = icon_get_clip(ability->icon_id, 0);
    else
        clip = icon_get_clip("question_mark", 0);
    button->ability = ability;
    for (int i = 0; i < 4; ++i)
        button->icon_clip[i] = clip[i];
}
