#ifndef MUTA_CLIENT_LOG_H
#define MUTA_CLIENT_LOG_H

#include <stdio.h>

#define LOG(fmt, ...) \
    fprintf(stdout, "[%s] " fmt "\n", __func__, ##__VA_ARGS__)
#define LOG_ERR(fmt, ...) \
    fprintf(stderr, "[%s] " fmt "\n", __func__, ##__VA_ARGS__)
#ifdef _MUTA_DEBUG
  #define LOG_DEBUG(fmt, ...) \
      fprintf(stdout, "[%s] " fmt "\n", __func__, ##__VA_ARGS__)
#else
  #define LOG_DEBUG(fmt, ...) ((void)0)
#endif

#endif /* MUTA_CLIENT_LOG_H */
