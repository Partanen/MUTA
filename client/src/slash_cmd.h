#ifndef MUTA_CLIENT_SLASH_CMD_H
#define MUTA_CLIENT_SLASH_CMD_H

/* Slash commands are executed from the chat if a message begins with '/'.
 * The messages are parsed in a C command line argument manner, where the first
 * argument is always the name of the command (including the slash), followed by
 * arguments separated by spaces.
 * Quotation marks (") can be used to pass in parameters with spaces. The
 * quotation marks are not written to the argument. Quotation marks can be
 * escaped using backslash (\).
 * Slash command names are case-sensitive. */

#include "../../shared/types.h"

#define MAX_SLASH_CMD_NAME_LEN  32
#define MAX_SLASH_CMD_ARGS      32

enum slash_error
{
    SLASH_CMD_NO_ERROR = 0,
    SLASH_CMD_NAME_TOO_SHORT,
    SLASH_CMD_NAME_TOO_LONG,
    SLASH_CMD_ALREADY_EXISTS
};

int
slash_init();

void
slash_destroy();

int
slash_register(const char *name,
    void (*callback)(void *context, int argc, char **argv), void *context);
/* Fails if name length < 1 or > MAX_SLASH_CMD_NAME_LEN, or if a command
 * with the same name aready exists. The name does not need to have a slash at
 * the begining. */

bool32
slash_try_execute(const char *msg);

#endif /* MUTA_CLIENT_SLASH_CMD_H */
