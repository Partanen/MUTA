#include "event.h"
#include "../../shared/common_utils.h"

typedef struct listener_t listener_t;

struct listener_t
{
    ev_callback_t   callback;
    void            *user_data;
};

dynamic_pool(listener_t)    _listeners[NUM_EVENTS];
uint32                      *_listener_indices[NUM_EVENTS];

int
ev_init(void)
{
    for (int i = 0; i < NUM_EVENTS; ++i)
        dynamic_pool_init(&_listeners[i], 4);
    return 0;
}

void
ev_destroy(void)
{
}

uint32
ev_add(enum event event, ev_callback_t callback, void *user_data)
{
    muta_assert(event >= 0);
    muta_assert(event < NUM_EVENTS);
    listener_t *l = dynamic_pool_new(&_listeners[event]);
    l->callback   = callback;
    l->user_data  = user_data;
    uint32 index = dynamic_pool_index(&_listeners[event], l);
    darr_push(_listener_indices[event], index);
    return index;
}

void
ev_del(enum event event, uint32 handle)
    {dynamic_pool_free(&_listeners[event], &_listeners[event].all[handle]);}

void
ev_post(event_t *event)
{
    muta_assert(event->type >= 0);
    muta_assert(event->type < NUM_EVENTS);
    uint32 num = darr_num(_listener_indices[event->type]);
    for (uint32 i = 0; i < num; ++i)
    {
        listener_t *l =
            &_listeners[event->type].all[_listener_indices[event->type][i]];
        l->callback(event, l->user_data);
    }
}
