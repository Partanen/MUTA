#ifndef MUTA_CLIENT_ACTION_BUTTON_H
#define MUTA_CLIENT_ACTION_BUTTON_H

#include "hotkey.h"

/* Forward declaration(s) */
typedef struct ability_def_t ability_def_t;

/* Types defined here */
typedef struct action_button_t action_button_t;

#define MAX_ACTION_BUTTONS 64

struct action_button_t
{
    char            key_name[HK_MAX_ACTION_NAME_LEN + 1];
    ability_def_t   *ability;
    float           icon_clip[4]; /* Relevant only if ability not null. */
};

void
action_button_set_ability(action_button_t *button, ability_def_t *ability);
/* Ability can be null for setting the button to empty. */

#endif /* MUTA_CLIENT_ACTION_BUTTON_H */
