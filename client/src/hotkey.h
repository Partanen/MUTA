#ifndef MUTA_HOTKEY_H
#define MUTA_HOTKEY_H

#include "../../shared/types.h"

typedef struct hk_action_callback_t     hk_action_callback_t;
typedef struct hk_key_mod_pair_t        hk_key_mod_pair_t;
typedef struct hk_action_t              hk_action_t;

#define HK_MAX_ACTION_NAME_LEN      31
#define HK_MAX_KEY_COMBO_NAME_LEN   31

enum hk_modifiers
{
    HK_MOD_SHIFT    = (1 << 0),
    HK_MOD_CTRL     = (1 << 1),
    HK_MOD_ALT      = (1 << 2)
};

enum hk_press_event_type
{
    HK_PRESS_DOWN = 0,
    HK_PRESS_UP,
    HK_PRESS_REPEAT,
    NUM_HK_PRESS_EVENT_TYPES
};

struct hk_key_mod_pair_t
{
    int keycode;
    int mods;
};

struct hk_action_t
{
    uint32                  id;
    char                    name[HK_MAX_ACTION_NAME_LEN + 1];
    hk_key_mod_pair_t       keys[2];
    int                     press_event_type; /* Key up, down or repeat */
    void (*callback)(void *user_data);
    void *user_data;
};

int
hk_init(void);

void
hk_destroy(void);

void
hk_clear(void);
/* Clear all key bindings. */

int
hk_load_from_file(const char *path);
/* Load from a character specific hotkeys.def file. No keys are cleared if
 * already set and not defined in the file.
 * Returns success even if file is not found, but not if there are syntax errors
 * in the file. */

int
hk_save_to_file(const char *path);
/* Save current hotkeys to file. */

int
hk_new_action(const char *name, void (*callback)(void *user_data),
    void *user_data, int press_event_type);
/* Failure may happen if name is longer than HK_MAX_ACTION_NAME_LEN */

int
hk_str_to_keycode(const char *str);

const char *
hk_key_enum_to_str(int key_enum);

int
hk_bind_key(const char *action_name, int index, int keycode, int mods);
/* Index must be 0 or 1. mods is a bitmask of modifier keys. */

int
hk_clear_key(const char *action_name, int index);

int
hk_bind_key_from_str(const char *action_name, int index, const char *key);

hk_action_t *
hk_get_action(int key_enum, int mods);
/* If a key is not found with the given modifiers but a key with no modifiers
 * is found instead, the latter will be returned. */

hk_action_t **
hk_get_actions(uint32 *ret_num);

hk_action_t *
hk_get_action_by_name(const char *name);

hk_action_t **
hk_get_repeat_actions(uint32 *ret_num);

int
hk_str_to_key_combo(const char *str, int *ret_keycode, int *ret_mods);
/* Any paremeter is allowed to be null. Format must follow the convention:
 * Q
 * SHIFT-Q
 * CTRL-SHIFT-Q
 * The order of modifiers does not matter. */

int
hk_key_combo_to_str(int keycode, int mods,
    char ret_buf[HK_MAX_KEY_COMBO_NAME_LEN + 1]);
/* Writes "None" if keycode is -1. */

void
hk_call_action(hk_action_t *action);

#endif /* MUTA_HOTKEY_H */
