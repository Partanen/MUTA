/* Common things used by the client modules "login" and "shard". Also, network
 * events, handled in core.c. */

#ifndef MUTA_CLIENT_H
#define MUTA_CLIENT_H

#include "../../shared/types.h"
#include "../../shared/net.h"
#include "../../shared/common_utils.h"

/* Types defined here */
typedef struct cl_read_event_t              cl_read_event_t;
typedef struct cl_disconnect_login_event_t  cl_disconnect_login_event_t;
typedef struct cl_disconnect_shard_event_t  cl_disconnect_shard_event_t;
typedef struct cl_event_t                   cl_event_t;

enum cl_event
{
    CL_EVENT_READ_LOGIN,
    CL_EVENT_DISCONNECT_LOGIN,
    CL_EVENT_LOGIN_CONNECTED,
    CL_EVENT_READ_SHARD,
    CL_EVENT_DISCONNECT_SHARD,
    CL_EVENT_SHARD_CONNECTED
};

struct cl_read_event_t
{
    int     num_bytes;
    uint8   *memory;
};

struct cl_disconnect_login_event_t
{
    int error;
};

struct cl_disconnect_shard_event_t
{
    int error;
};

struct cl_event_t
{
    int type;
    union
    {
        cl_read_event_t             read;
        cl_disconnect_login_event_t disconnect_login;
        cl_disconnect_shard_event_t disconnect_shard;
    };
};

int
cl_init(void);

void
cl_destroy(void);

void
cl_push_event(cl_event_t *event);

int
cl_pop_events(cl_event_t *ret, int max);

void *
cl_malloc(uint32 num_bytes);
/* Allocate memory for a read socket event */

int
cl_flush_buf(socket_t socket, uint8 *memory, int *num_bytes);

bbuf_t
cl_send(socket_t socket, uint8 *memory, int *num_memory_bytes,
    int max_memory_bytes, int num_bytes);
/* Buffer a message, sending first if full. */

int
cl_check_blocking_socket_connect(socket_t socket, fd_set *socket_set);
/* Return values:
 * < 0: error
 * > 0: timeout
 * 0: connected successfully */

int
cl_read(cl_read_event_t *event, int (*read_packet)(void),
    uint8 *memory, int *num_memory_bytes, int max_memory_bytes,
    int *last_error, int server_closed_connection_error, int bad_packet_error);
/* This function will set last_error to the server_closed_connection_error
 * if the event num_bytes is <= 0.
 * If read_packet returns non-zero and the last_error is zero, it will
 * set last_error to bad_packet_error. */

void
cl_set_dc_reason(int reason);

#endif /* MUTA_CLIENT_H */
