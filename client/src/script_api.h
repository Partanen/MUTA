#ifndef MUTA_CLIENT_SCRIPT_API_H
#define MUTA_CLIENT_SCRIPT_API_H

#include <stdint.h>
#include <stdio.h>
#include "../../shared/common_defs.h"

#define SCRIPT_LOG(fmt, ...) \
    fprintf(stdout, "[script %s: %s] " fmt "\n", __FILE__, __func__, \
        ##__VA_ARGS__)

/* Forward declaration(s) */
typedef struct ability_def_t    ability_def_t;
typedef struct game_state_t     game_state_t;

/* Types defined here */
typedef uint32_t                ability_id_t;
typedef int                     bool32;
typedef struct ability_script_t ability_script_t;
typedef void                    script_entity_t;

enum script_entity_type
{
    SCRIPT_ENTITY_TYPE_PLAYER,
    SCRIPT_ENTITY_TYPE_CREATURE,
    SCRIPT_ENTITY_DYNAMIC_OBJECT
};

struct ability_script_t
{
    ability_id_t id;

    bool32 (*can_use)(script_entity_t *target);
    /* Optional. */

    void (*on_request)(script_entity_t *target);
    /* Required.
     * Called when a request to use the ability is sent to the server. */


    void (*on_charge_finished)(script_entity_t *target,
        enum common_ability_charge_result result);
    /* Required if computer_charging_time is not null.
     * If the ability has a charging time, this will be called when the server
     * sends a message of the ability having been fully charged. */

    void (*on_response)(script_entity_t *target, bool32 success);
    /* Required.
     * Called when the server responds to an ability use request. */

    uint32_t (*compute_charging_time)(script_entity_t *target);
    /* Optional.
     * If null, ability is considered instant. Otherwise a charging bar will be
     * shown */

    void (*on_player_moved)(void);
    /* Optional. Run if player starts moving during charging. */

    /* The following values are automatically filled at load time. Initialize to
     * 0. */
    ability_def_t *def;
};

void
script_set_game_state(game_state_t *gs);

void
script_log_system_message(const char *msg);

void
script_stop_charge(void);

script_entity_t *
script_get_this_player_entity(void);


bool32
script_entity_is_this_player(script_entity_t *script_entity);

bool32
script_is_entity_moving(script_entity_t *entity);

void
script_get_entity_position(script_entity_t *entity, int ret_position[3]);

enum iso_dir
script_get_entity_direction(script_entity_t *entity);

void
script_compute_distance_vec(int position_a[3], int position_b[3], int ret[3]);

#endif /* MUTA_CLIENT_SCRIPT_API_H */
