#ifndef MUTA_CLIENT_RENDER_H
#define MUTA_CLIENT_RENDER_H

#include "../../shared/types.h"
#include "../../shared/compiler.h"
#include "types.h"

#define NUM_TILETYPES   255
#define CLIP_W(clip)    (clip[2] - clip[0])
#define CLIP_H(clip)    (clip[3] - clip[1])

/* Forward declaration(s) */
typedef struct gui_draw_list_t      gui_draw_list_t;
typedef struct world_t              world_t;
typedef struct img_t                img_t;
typedef struct tex_t                tex_t;
typedef struct entity_t             entity_t;

/* Types defined here */
typedef struct render_world_t       render_world_t;
typedef struct render_camera_t      render_camera_t;
typedef struct render_world_cmd_t   render_world_cmd_t;
typedef struct render_ghost_tile_t        render_ghost_tile_t;

struct render_camera_t
{
    int     position[3];            /* Tile at the center of the camera */
    int     last_position[3];       /* If percentage_moved is 1.f, ignored. */
    float   percentage_moved;

    /* Tile selector
     * A single special tile can be drawn at the position given below to
     * indicate something special, such as the mouse's position on the map.
     * - tile_selector_position is the raw tile coordinate of where to draw the
     *   tile.
     * - draw_tile_selector indicates if the selector is drawn or not (1 or 0).
     * - tile_selector_id_index is an index >= 0 && <
     *   CORE_NUM_TILE_SELECTOR_TYPES. */
    bool32  draw_tile_selector;
    int     tile_selector_position[3];
    int     tile_selector_id_index;
};
/* render_camera_t
 * Position data for rendering the world.
 * No functions for modifying this, just fiddle with the struct. */

struct render_world_t
{
    uint32              tile_vbo;
    uint32              tile_ebo;
    bool32              need_tile_update;
    int                 last_position[3];
    uint32              num_tile_sprites;
    render_world_cmd_t  *cmds;
    uint32              max_cmds;
    uint32              num_cmds;
    render_ghost_tile_t *ghost_tiles;
    struct
    {
        int     tile_position[3];
        int     area_w, area_h, area_t;
        float   bx, by;
        float   coordinate_space[4];
        int     screen_area[4];
        float   tile_w;
        float   tile_h;
        float   tile_top_h;
        int     extra_w, extra_h;
        float   scale_x, scale_y;
    } cached;
};
/* render_world_t
 * A cache of renderable world state. From here it is also possible to fetch
 * render-related data, such as coordinates of entities on the screen. */

int
r_init(void);

int
r_destroy(void);

void
r_render_gui(void);

void
render_camera_init(render_camera_t *camera);

int
render_world_init(render_world_t *render_world);

void
render_world_destroy(render_world_t *render_world);

void
render_world_clear(render_world_t *render_world);
/* Clear the cache out for a freshly loaded world. */

void
render_world_update(render_world_t *render_world, render_camera_t *camera,
    world_t *world, int *screen_target);
/* Updates the render world's state from the given world at the given camera
 * position and renders it to the screen.
 * screen_target is in the form x, y, w, h in screen pixel coordinates.
 * Note that this function also modifies the world_t by setting the
 * render_system's culling area. */

void
render_world_translate_screen_to_world_pixel(render_world_t *render_world,
    int screen_x, int screen_y, float *restrict ret_x, float *restrict ret_y);

void
render_world_translate_world_to_screen_pixel(render_world_t *render_world,
    float world_x, float world_y, int *restrict ret_x, int *restrict ret_y);

void
render_world_pixel_to_tile_position(render_world_t *render_world, float pixel_x,
    float pixel_y, int *restrict ret_x, int *restrict ret_y);
/* The tile translated to would exist on the uppermost drawn tile layer. */

void
render_world_pixel_to_tile_position_at_z(render_world_t *render_worldw,
    float pixel_x, float pixel_y, int z, int *restrict ret_x,
    int *restrict ret_y);
/* Same as render_world_pixel_to_tile_position(), but z layer is given in as a
 * parameter rather than assumed to be the topmost one. */

void
render_world_tile_to_pixel_position(render_world_t *render_world,
    int tile_position[3], int *restrict ret_x, int *restrict ret_y);
/* Returns the pixel position of the top-center of the tile in world-space
 * pixel coordinates. */

bool32
render_world_find_tile_by_pixel_position(render_world_t *render_world,
    world_t *world, int pixel_x, int pixel_y, int ret_position[3]);
/* Returns non-zero if a solid tile is found. */

uint32
render_world_find_entities_by_pixel_position(render_world_t *render_world,
    float x, float y, entity_t **ret_entities, uint32 max_ret_entities);
/* Can be used to pick entities by pixel position. Up to max_ret_entities are
 * written to ret_entities.
 * Note that despawned entities may still exist in the render world's cache.
 * Therefore, after calling render_world_update(), do not perform any despawn
 * calls before calling this function. */

void
render_world_add_ghost_tile(render_world_t *render_world, uint16 tag,
    tile_t tile, int32 x, int32 y, uint8 z);

void
render_world_clear_ghost_tiles(render_world_t *render_world);

void
render_world_remove_ghost_tiles_by_tag(render_world_t *render_world, uint16 tag);

void
render_world_remove_ghost_tile_by_position(render_world_t *render_world,
    int32 x, int32 y, uint8 z);

int
render_world_compute_tile_pixel_x(render_world_t *render_world, int tx, int ty,
    int offset);

int
render_world_compute_tile_pixel_y(render_world_t *render_world, int tx, int ty,
    int tz, int offset);

#endif /* MUTA_CLIENT_RENDER_H */
