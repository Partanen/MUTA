#include <time.h>
#include "character_creation_screen.h"
#include "character_selection_screen.h"
#include "main_menu_screen.h"
#include "assets.h"
#include "render.h"
#include "core.h"
#include "render.h"
#include "shard.h"
#include "../../shared/gui/gui.h"
#include "../../shared/entities.h"

#define MID_WINDOW_W 400
#define MID_WINDOW_H 200

static void
character_create_screen_update(double dt);

static void
character_create_screen_open(void);

static void
character_create_screen_close(void);

static void
character_create_screen_keydown(int key, bool32 is_repeat);

screen_t character_create_screen =
{
    "Character Creation",
    0,
    0,
    character_create_screen_update,
    character_create_screen_open,
    character_create_screen_close,
    0,
    character_create_screen_keydown,
    0,
    0,
    0,
    0,
    0
};

static list_character_t     _character;
static player_race_def_t    *_selected_race_def;
static const char           *_err_msg;
static bool32               _started_creating;
static player_race_def_t    *_race_defs;
static const char           *_name_text_input_id    = "Name";
static const char           *_name_win_id           = "##name_text_input";

enum text_fields
{
    TEXT_FIELD_CHARACTER_NAME
};

static void
_draw_selected_options(int x, int y);

static void
_draw_local_error_window(int x, int y);

static void
_draw_server_error_window(int x, int y);

static void
_create_character();

static void
_add_race_def(player_race_def_t *def, void *user_data);

void
character_create_screen_update(double dt)
{
    if (core_is_first_frame_of_current_screen())
    {
        gui_set_active_win(_name_win_id);
        gui_set_active_text_input(_name_text_input_id);
    }
    gl_viewport(0, 0, core_window_w(), core_window_h());
    gl_scissor(0, 0, core_window_w(), core_window_h());
    gl_color(0.0f, 0.0f, 0.0f, 1.0f);
    gl_clear(RB_CLEAR_COLOR_BIT);
    gui_font(as_get_default_font());
    gui_button_style(0);
    gui_win_style(0);
    gui_origin(GUI_TOP_CENTER);
    gui_text("Create new character", 0, 0, 30);
    gui_origin(GUI_BOTTOM_RIGHT);
    if (gui_button("Cancel##screen", 10, 10, 256, 64, 0))
        core_set_screen(&character_select_screen);
    gui_origin(GUI_TOP_LEFT);
    uint32 num_race_defs = ent_num_player_race_defs();
    for (uint32 i = 0; i < num_race_defs; ++i)
        if (gui_button(_race_defs[i].name, 10, 10 + i * 66, 256, 64, 0))
            _character.race = _race_defs[i].id;
    gui_origin(GUI_BOTTOM_LEFT);
    if (gui_button("Male", 10, 10, 96, 96, 0))
        _character.sex = ENT_SEX_MALE;
    if (gui_button("Female", 10 + 96 + 2, 10, 96, 96, 0))
        _character.sex = ENT_SEX_FEMALE;
    gui_origin(GUI_CENTER_CENTER);
    _draw_selected_options(0, 0);
    gui_origin(GUI_BOTTOM_CENTER);
    gui_begin_empty_win(_name_win_id, 0, 40, 128, 16, 0);
    gui_text_input(_name_text_input_id, _character.name,
        sizeof(_character.name), 0, 0, 128, 16, 0);
    gui_end_win();
    _character.name_len = (uint32)strlen(_character.name);
    if (gui_button("Create", 0, 10, 96, 24, 0))
        _create_character();
    gui_origin(GUI_CENTER_CENTER);
    if (_err_msg)
        _draw_local_error_window(0, 0);
    else if (_started_creating)
    {
        if (shard_get_character_creation_error())
            _draw_server_error_window(0, 0);
        else
            core_set_screen(&character_select_screen);
    } else
    if (core_key_down(CORE_KEY_RETURN))
        _create_character();
    if (shard_get_status() != SHARD_STATUS_CONNECTED)
        core_set_screen(&main_menu_screen);
}

void
character_create_screen_open(void)
{
    _character.race     = 0; /* Default */
    _selected_race_def  = ent_get_player_race_def(0);
    _character.sex      = rand() % 2;
    _character.name[0]  = 0;
    darr_clear(_race_defs);
    ent_for_each_player_race_def(_add_race_def, 0);
    _started_creating   = 0;
    _err_msg            = 0;
}

void
character_create_screen_close(void)
{
}

void
character_create_screen_keydown(int key, bool32 is_repeat)
{
}

static void
_draw_selected_options(int x, int y)
{
    gui_begin_win("##selectedopts", x, y, 100, 100, 0);
    gui_origin(GUI_TOP_LEFT);
    gui_textf(
        "Race: %s\n"
        "Sex: %s",
        0, 5, 5,
        _selected_race_def ? _selected_race_def->name : "none",
        _character.sex == ENT_SEX_MALE ? "male" : "female");
    gui_end_win();
}

static void
_draw_local_error_window(int x, int y)
{
    gui_begin_empty_win("local error layer", 0, 0, core_asset_config.resolution_w,
        core_asset_config.resolution_h, 0);
    gui_begin_win("##local error win", 0, 0, MID_WINDOW_W, MID_WINDOW_H, 0);
    gui_origin(GUI_CENTER_CENTER);
    gui_text(_err_msg, 0, 0, 0);
    gui_origin(GUI_BOTTOM_CENTER);
    if (gui_button("Ok##local_error_win", 0, 10, 32, 32, 0))
        _err_msg = 0;
    gui_end_win();
    gui_end_win();
    gui_set_active_win("error layer");
}

static void
_draw_server_error_window(int x, int y)
{
    gui_begin_empty_win("server error layer", 0, 0, core_asset_config.resolution_w,
        core_asset_config.resolution_h, 0);
    gui_begin_win("##server error win", 0, 0, MID_WINDOW_W, MID_WINDOW_H, 0);
    gui_origin(GUI_CENTER_CENTER);
    gui_textf("Character creation failed: %s.", 0, 0, 0,
        character_creation_fail_to_str(shard_get_character_creation_error()));
    gui_origin(GUI_BOTTOM_CENTER);
    if (gui_button("Ok##server_error_win", 0, 10, 64, 24, 0))
        _started_creating = 0;
    gui_end_win();
    gui_end_win();
}

static void
_create_character()
{
    if (_character.name_len < MIN_CHARACTER_NAME_LEN)
    {
        _err_msg = "Character name is too short.";
        return;
    }
    if (_character.name_len > MAX_CHARACTER_NAME_LEN)
    {
        _err_msg = "Character name is too short.";
        return;
    }
    if (shard_create_character(&_character))
    {
        _err_msg = "Failed to contact server.";
        return;
    }
    _started_creating = 1;
}

static void
_add_race_def(player_race_def_t *def, void *user_data)
    {darr_push(_race_defs, *def);}
