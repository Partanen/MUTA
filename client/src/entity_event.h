#ifndef MUTA_CLIENT_ENTITY_EVENT_H
#define MUTA_CLIENT_ENTITY_EVENT_H

/* Forward declaration(s) */
typedef struct entity_t                 entity_t;
typedef struct component_definition_t   component_definition_t;
typedef void*                           component_handle_t;

/* Types defined here */
typedef struct entity_set_position_event_t      entity_set_position_event_t;
typedef struct entity_set_direction_event_t     entity_set_direction_event_t;
typedef struct entity_attach_component_event_t  entity_attach_component_event_t;
typedef struct entity_detach_component_event_t  entity_detach_component_event_t;
typedef struct entity_current_health_changed_event_t
    entity_current_health_changed_event_t;
typedef struct entity_max_health_changed_event_t
    entity_max_health_changed_event_t;
typedef struct entity_event_t                   entity_event_t;

enum entity_event
{
    ENTITY_EVENT_SET_POSITION = 0,
    ENTITY_EVENT_SET_DIRECTION,
    ENTITY_EVENT_WILL_DESPAWN,
    ENTITY_EVENT_ATTACH_COMPONENT,
    ENTITY_EVENT_DETACH_COMPONENT,
    ENTITY_EVENT_CURRENT_HEALTH_CHANGED,
    ENTITY_EVENT_MAX_HEALTH_CHANGED,
    NUM_ENTITY_EVENTS
};

struct entity_set_position_event_t
{
    int last_position[3];
    int new_position[3];
};

struct entity_set_direction_event_t
{
    int last_direction;
    int new_direction;
};

struct entity_attach_component_event_t
{
    component_definition_t  *component_definition;
};

struct entity_detach_component_event_t
{
    component_definition_t *component_definition;
};

struct entity_current_health_changed_event_t
{
    uint32 health_current;
};

struct entity_max_health_changed_event_t
{
    uint32 health_max;
};

struct entity_event_t
{
    int         type;
    entity_t    *entity;
    union
    {
        entity_set_position_event_t             set_position;
        entity_set_direction_event_t            set_direction;
        entity_attach_component_event_t         attach_component;
        entity_detach_component_event_t         detach_component;
        entity_current_health_changed_event_t   current_health_changed;
        entity_max_health_changed_event_t       max_health_changed;
    };
};

#endif /* MUTA_CLIENT_ENTITY_EVENT_H */
