#include "game_state.h" /* Disiclude later */
#include "game_state_internal.h"
#include "gui.h"
#include "gui_styles.h"
#include "core.h"
#include "shard.h"
#include "hotkey.h"
#include "assets.h"
#include "log.h"
#include "icon.h"
#include "event.h"
#include "../../shared/client_packets.h"
#include "../../shared/abilities.h"

static game_state_t *_gs;

#define MAX_CHAT_ENTRY_LEN  (MAX_DISPLAY_NAME_LEN + 2 + MAX_CHAT_MSG_LEN)
#define MAX_CHAT_LOG_SIZE   128

typedef struct chat_entry_t chat_entry_t;

enum options_mode
{
    OPTIONS_MODE_OFF,
    OPTIONS_MODE_SHOW_MENU,
    OPTIONS_MODE_HOTKEYS
};

enum character_mode
{
    CHARACTER_MODE_OFF,
    CHARACTER_MODE_PAPERDOLL
};

struct chat_entry_t
{
    int     type;
    char    str[MAX_CHAT_ENTRY_LEN + 1];
};

static struct
{
    chat_entry_t    entries[MAX_CHAT_LOG_SIZE];
    uint32          first;
    uint32          num;
    char            msg[MAX_CHAT_MSG_LEN + 1];
    bool32          input_active;
} _chat;

static struct
{
    enum options_mode mode;
    struct
    {
        bool32  have;
        uint32  action_id;
        uint32  slot;
        char    action_name[HK_MAX_ACTION_NAME_LEN + 1];
    } modified_keybind;
} _options;

static struct
{
    bool32 show;
} _debug_window;

static struct
{
    enum character_mode mode;
} _character;

static struct
{
    bool32              show;
    enum equipment_slot selected_slot;
} _inventory;

static const char   *_next_active_window;
static const char   *_next_active_text_input;
static bool32       _clear_active_text_input;
static int32        _action_button_wh;
static int32        _border_gap;

static void
_new_action_and_keybind(const char *action_name,
    void (*callback)(void *user_data), void *user_data,
    enum hk_press_event_type event_type, int keycode);

static void
_capi_toggle_chat_input(void *user_data);

static void
_capi_toggle_debug_window(void *user_data);

static void
_capi_toggle_inventory(void *user_data);

static void
_capi_toggle_paperdoll(void *user_data);

static void
_capi_use_action_button(void *user_data);

static void
_draw_chat(void);

static void
_draw_options(void);

static void
_draw_inventory(void);

static void
_draw_action_buttons(void);

static void
_draw_debug_window(void);

static void
_draw_menu_bar(void);

static void
_draw_character(void);

static void
_draw_clicked_entities(void);

static void
_draw_entity_frames(void);

static void
_draw_fps(void);

static void
_draw_charge_bar(void);

static void
_draw_name_plates(void);

static void
_setup_hotkeys(void);

static void
_load_character_data(const char *character_name);

static void
_parse_actionbar_cfg_callback(void *ctx, const char *opt, const char *val);

static bool32
_is_str_action_button_name(const char *str);

static void
_update_action_button_names(void);

static void
_call_action_button(action_button_t *button);

int
gsgui_init(game_state_t *gs)
{
    _gs = gs;
    _new_action_and_keybind("Toggle Chat Input", _capi_toggle_chat_input, 0,
        HK_PRESS_UP, CORE_KEY_RETURN);
    _new_action_and_keybind("Toggle Debug Window", _capi_toggle_debug_window, 0,
        HK_PRESS_UP, CORE_KEY_F11);
    _new_action_and_keybind("Toggle Inventory", _capi_toggle_inventory, 0,
        HK_PRESS_UP, CORE_KEY_I);
    _new_action_and_keybind("Toggle Paperdoll", _capi_toggle_paperdoll, 0,
        HK_PRESS_UP, CORE_KEY_P);
    _action_button_wh = (int32)(gui_style_action_button.states[0].tex_clip[2] -
        gui_style_action_button.states[0].tex_clip[0]);
    _border_gap = (int)(0.005f * (float)core_asset_config.resolution_w);
    return 0;
}

void
gsgui_begin(const char *character_name)
{
    _next_active_window             = 0;
    _next_active_text_input         = 0;
    _clear_active_text_input        = 0;
    /* Chat */
    _chat.num                       = 0;
    _chat.first                     = 1;
    /* Options */
    _options.mode                   = OPTIONS_MODE_OFF;
    _options.modified_keybind.have  = 0;
    /* Debug window */
    _debug_window.show              = 0;
    /* Inventory */
    _inventory.show                 = 0;
    _inventory.selected_slot        = EQUIPMENT_SLOT_BAG1;
    _setup_hotkeys();
    _load_character_data(character_name);
    _update_action_button_names();
}

void
gsgui_end(void)
{
}

void
gsgui_update_and_render(void)
{
    gui_origin(GUI_TOP_LEFT);
    if (_next_active_window)
    {
        gui_set_active_win(_next_active_window);
        _next_active_window = 0;
    }
    if (_next_active_text_input)
    {
        gui_set_active_text_input(_next_active_text_input);
        _next_active_text_input = 0;
    }
    if (_clear_active_text_input)
    {
        gui_set_active_text_input(0);
        _clear_active_text_input = 0;
    }
    _draw_name_plates();
    _draw_fps();
    _draw_debug_window();
    _draw_chat();
    _draw_action_buttons();
    _draw_menu_bar();
    _draw_inventory();
    _draw_character();
    _draw_clicked_entities();
    _draw_entity_frames();
    _draw_charge_bar();
    _draw_options();
}

void
gsgui_on_chat_msg_received(int type, const char *sender, const char *msg)
{
    uint32 index = _chat.first == 0 ? MAX_CHAT_LOG_SIZE - 1 : _chat.first - 1;
    _chat.entries[index].type = type;
    snprintf(_chat.entries[index].str, MAX_CHAT_ENTRY_LEN, "%s: %s", sender, msg);
    _chat.first = index;
    if (_chat.num != MAX_CHAT_LOG_SIZE)
        _chat.num++;
}

void
gsgui_on_system_msg_received(const char *msg)
{
    uint32 index = _chat.first == 0 ? MAX_CHAT_LOG_SIZE - 1 : _chat.first - 1;
    _chat.entries[index].type = CHAT_MSG_SYSTEM;
    strncpy(_chat.entries[index].str, msg, MAX_CHAT_ENTRY_LEN);
    _chat.first = index;
    if (_chat.num != MAX_CHAT_LOG_SIZE)
        _chat.num++;
}

void
gsgui_on_local_error_msg_received(const char *msg)
{
    uint32 index = _chat.first == 0 ? MAX_CHAT_LOG_SIZE - 1 : _chat.first - 1;
    _chat.entries[index].type = CHAT_MSG_SYSTEM;
    strncpy(_chat.entries[index].str, msg, MAX_CHAT_ENTRY_LEN);
    _chat.first = index;
    if (_chat.num != MAX_CHAT_LOG_SIZE)
        _chat.num++;
}

bool32
gsgui_is_grabbing_input(void)
    {return _chat.input_active || _options.modified_keybind.have;}

void
gsgui_on_key_event(int event, int key)
{
    if (key == CORE_KEY_LSHIFT || key == CORE_KEY_RSHIFT ||
        key == CORE_KEY_LCTRL  || key == CORE_KEY_RCTRL ||
        key == CORE_KEY_LALT   || key == CORE_KEY_RALT)
        return;
    if (_options.modified_keybind.have)
    {
        if (event == HK_PRESS_UP)
        {
            int mods = 0;
            mods |= (core_key_down(CORE_KEY_LSHIFT) ? HK_MOD_SHIFT : 0);
            mods |= (core_key_down(CORE_KEY_RSHIFT) ? HK_MOD_SHIFT : 0);
            mods |= (core_key_down(CORE_KEY_LCTRL) ? HK_MOD_CTRL : 0);
            mods |= (core_key_down(CORE_KEY_RCTRL) ? HK_MOD_CTRL : 0);
            mods |= (core_key_down(CORE_KEY_LALT) ? HK_MOD_ALT : 0);
            mods |= (core_key_down(CORE_KEY_RALT) ? HK_MOD_ALT : 0);
            if (!hk_bind_key(_options.modified_keybind.action_name,
                (int)_options.modified_keybind.slot, key, mods) &&
                _is_str_action_button_name(
                    _options.modified_keybind.action_name))
                _update_action_button_names();
            _options.modified_keybind.have = 0;
        }
        return;
    }
    if (_options.mode == OPTIONS_MODE_HOTKEYS)
    {
        if (key == CORE_KEY_ESCAPE && event == HK_PRESS_UP)
        {
            _options.mode                   = OPTIONS_MODE_SHOW_MENU;
            _options.modified_keybind.have  = 0;
            return;
        }
    }
    if (_chat.input_active && event == HK_PRESS_UP)
    {
        if (key == CORE_KEY_ESCAPE)
        {
            _chat.msg[0]                = 0;
            _clear_active_text_input    = 1;
            return;
        }
        if (key == CORE_KEY_RETURN)
        {
            uint8 len = (uint8)strlen(_chat.msg);
            if (len)
            {
                clmsg_chat_message_t s;
                memcpy(s.message.data, _chat.msg, len);
                s.message.len = len;
                bbuf_t bb = shard_send(clmsg_chat_message_compute_sz(&s));
                if (bb.max_bytes)
                    clmsg_chat_message_write(&bb, &s);
                _chat.msg[0] = 0;
            }
            _clear_active_text_input = 1;
        }
    }
}

void
gsgui_on_start_drag_dynamic_object(dobj_type_id_t type_id)
{
    dynamic_object_def_t *def = ent_get_dynamic_object_def(type_id);
    as_dynamic_object_display_t *display = as_get_dynamic_object_display(
        def->display_id);
    const char *inventory_icon_id = "question_mark";
    if (display->inventory_icon_id)
        inventory_icon_id = display->inventory_icon_id;
    cursor_t *cursor = icon_get_cursor(inventory_icon_id);
    muta_assert(cursor);
    core_set_cursor(cursor);
    _inventory.show = 1;
}

void
gsgui_on_stop_dragging_dynamic_object(void)
{
    as_cursor_t *cursor = as_get_cursor("default");
    core_set_cursor(cursor->data);
}

void
gsgui_toggle_options(void)
{
    bool32 early_return = 0;
    if (_character.mode != CHARACTER_MODE_OFF)
    {
        _character.mode = CHARACTER_MODE_OFF;
        early_return = 1;
    }
    if (_inventory.show)
    {
        _inventory.show = 0;
        early_return = 1;
    }
    if (early_return)
        return;
    switch (_options.mode)
    {
    case OPTIONS_MODE_OFF:
        _options.mode = OPTIONS_MODE_SHOW_MENU;
        break;
    case OPTIONS_MODE_SHOW_MENU:
        _options.mode = OPTIONS_MODE_OFF;
        break;
    case OPTIONS_MODE_HOTKEYS:
        _options.mode = OPTIONS_MODE_SHOW_MENU;
        break;
    }
}

static void
_new_action_and_keybind(const char *action_name,
    void (*callback)(void *user_data), void *user_data,
    enum hk_press_event_type event_type, int keycode)
{
    hk_new_action(action_name, callback, user_data, event_type);
    hk_bind_key(action_name, 0, keycode, 0);
}

static void
_capi_toggle_chat_input(void *user_data)
{
    if (_chat.input_active)
        return;
    const char *id = "##chat";
    _next_active_window     = id;
    _next_active_text_input = id;
    return;
}

static void
_capi_toggle_debug_window(void *user_data)
{
    if (_debug_window.show)
        _debug_window.show = 0;
    else
        _debug_window.show = 1;
}

static void
_capi_toggle_inventory(void *user_data)
{
    if (_inventory.show)
        _inventory.show = 0;
    else
        _inventory.show = 1;
}

static void
_capi_toggle_paperdoll(void *user_data)
{
    if (_character.mode != CHARACTER_MODE_PAPERDOLL)
        _character.mode = CHARACTER_MODE_PAPERDOLL;
    else
        _character.mode = CHARACTER_MODE_OFF;
}


static void
_capi_use_action_button(void *user_data)
    {_call_action_button(user_data);}

static void
_draw_chat(void)
{
    int chat_x = _border_gap;
    int chat_y = chat_x;
    int chat_w = (int)(0.24f * (float)core_asset_config.resolution_w);
    int chat_h = (int)(0.7f * (float)chat_w);
    gui_origin(GUI_BOTTOM_LEFT);
    gui_win_style(&gui_style_chat_window);
    gui_begin_win("##chat", chat_x, chat_y, chat_w, chat_h, 0);
    gui_origin(GUI_BOTTOM_LEFT);
    gui_text_input_style(0);
    /* Input */
    _chat.input_active = gui_text_input("##chat", _chat.msg, sizeof(_chat.msg),
        0, 0, gui_get_current_win_viewport_w(), 12, 0);
    /* Chat log */
    gui_font(0);
    uint32 first    = _chat.first;
    uint32 num      = _chat.num;
    if (num)
    {
        uint32 end = (first + num) % MAX_CHAT_LOG_SIZE;
        int y = gui_get_last_text_input_y() + gui_get_last_text_input_h() + 2;
        int cw      = gui_get_current_win_viewport_w();
        int ch      = gui_get_current_win_viewport_h();
        for (uint32 i = first; i != end; i = (i + 1) % MAX_CHAT_LOG_SIZE)
        {
            if (y >= ch)
                break;
            gui_text(_chat.entries[i].str, cw - 2, 2, y);
            y += gui_get_last_text_h() + 2;
        }
    }
    gui_end_win();
}

static void
_draw_options(void)
{
    switch (_options.mode)
    {
    case OPTIONS_MODE_OFF:
        break;
    case OPTIONS_MODE_SHOW_MENU:
    {
        int *border = gui_style_topless_window.states[GUI_WIN_STATE_INACTIVE].border.widths;
        int win_w = (int)((float)core_asset_config.resolution_w / 7.5f);
        int button_w = win_w - border[GUI_EDGE_LEFT] - border[GUI_EDGE_RIGHT];
        int button_h = (int)((float)core_asset_config.resolution_h / 35.f);
        int win_h = 4 * button_h + border[GUI_EDGE_TOP] + border[GUI_EDGE_BOTTOM];
        gui_win_style(&gui_style_topless_window);
        gui_button_style(&gui_style_menu_button);
        gui_origin(GUI_CENTER_CENTER);
        gui_begin_win("mainmenu", 0, 0, win_w, win_h, 0);
        gui_origin(GUI_TOP_LEFT);
        int y = 0;
        if (gui_button("Resume", 0, y, button_w, button_h, 0))
            _options.mode = OPTIONS_MODE_OFF;
        y += button_h;
        if (gui_button("Hotkeys", 0, y, button_w, button_h, 0))
            _options.mode = OPTIONS_MODE_HOTKEYS;
        y += button_h;
        if (gui_button("Logout", 0, y, button_w, button_h, 0))
            shard_disconnect();
        y += button_h;
        if (gui_button("Exit Game", 0, y, button_w, button_h, 0))
            core_stop();
        gui_end_win();
    }
        break;
    case OPTIONS_MODE_HOTKEYS:
    {
        int win_w = (int)((float)core_asset_config.resolution_w * 0.4f);
        int win_h = (int)((float)core_asset_config.resolution_h * 0.8f);
        gui_win_style(0);
        gui_font(0);
        gui_origin(GUI_CENTER_CENTER);
        gui_begin_win("Hotkeys", 0, 0, win_w, win_h, 0);
        gui_origin(GUI_TOP_LEFT);
        int col_w = (int)(1.f / 3.f * (float)gui_get_current_win_viewport_w());
        int col_h = gui_get_button_style_min_height(&gui_style_menu_button) + 2;
        int col_y = 0;
        gui_begin_empty_win("HotkeysInner", 0, 0,
            gui_get_current_win_viewport_w(),
            gui_get_current_win_viewport_h() - col_h, GUI_WIN_SCROLLABLE);
        uint32 num_actions;
        hk_action_t **actions = hk_get_actions(&num_actions);
        for (uint32 i = 0; i < num_actions; ++i)
        {
            char buf[HK_MAX_KEY_COMBO_NAME_LEN + 1];
            /* Action name */
            gui_text(actions[i]->name, 0, 0, col_y);
            /* 1st keybind button */
            hk_key_combo_to_str(actions[i]->keys[0].keycode,
                actions[i]->keys[0].mods, buf);
            const char *id;
            id = gui_format_id("%s##kb0_%u", buf, i);
            if (_options.modified_keybind.have &&
                _options.modified_keybind.action_id == actions[i]->id &&
                _options.modified_keybind.slot == 0)
                gui_button_style(&gui_style_menu_button_highlighted);
            else
                gui_button_style(&gui_style_menu_button);
            if (gui_button(id, col_w, col_y, col_w, col_h, 0))
            {
                _options.modified_keybind.action_id = actions[i]->id;
                _options.modified_keybind.slot      = 0;
                _options.modified_keybind.have      = 1;
                strcpy(_options.modified_keybind.action_name, actions[i]->name);
            }
            /* 2nd keybind button */
            if (_options.modified_keybind.have &&
                _options.modified_keybind.action_id == actions[i]->id &&
                _options.modified_keybind.slot == 1)
                gui_button_style(&gui_style_menu_button_highlighted);
            else
                gui_button_style(&gui_style_menu_button);
            hk_key_combo_to_str(actions[i]->keys[1].keycode,
                actions[i]->keys[1].mods, buf);
            id = gui_format_id("%s##kb1_%u", buf, i);
            if (gui_button(id, 2 * col_w, col_y, col_w, col_h, 0))
            {
                _options.modified_keybind.action_id = actions[i]->id;
                _options.modified_keybind.slot      = 1;
                _options.modified_keybind.have      = 1;
                strcpy(_options.modified_keybind.action_name, actions[i]->name);
            }
            col_y += col_h;
        }
        gui_end_win();
        gui_origin(GUI_BOTTOM_RIGHT);
        gui_button_style(&gui_style_menu_button);
        if (gui_button("Done##kb", 0, 0, col_w, col_h, 0))
        {
            _options.mode                   = OPTIONS_MODE_SHOW_MENU;
            _options.modified_keybind.have  = 0;
            hk_save_to_file(_gs->hotkeys_path);
        }
        gui_origin(GUI_BOTTOM_LEFT);
        if (gui_button("Unbind##kb", 0, 0, col_w, col_h, 0) &&
            _options.modified_keybind.have)
        {
            if (!hk_clear_key(_options.modified_keybind.action_name,
                (int)_options.modified_keybind.slot) &&
                _is_str_action_button_name(
                    _options.modified_keybind.action_name))
                _update_action_button_names();
            _options.modified_keybind.have = 0;
        }
        gui_end_win();
    }
        break;
    }
}

static void
_draw_inventory(void)
{
    if (!_inventory.show)
        return;
    int w = (int)(0.267f * (float)core_asset_config.resolution_w);
    int h = w;
    int x = _border_gap;
    if (_character.mode != CHARACTER_MODE_OFF)
        x = 2 * _border_gap + w;
    gui_origin(GUI_BOTTOM_RIGHT);
    gui_button_style(&gui_style_action_button);
    gui_win_style(&gui_style_window_1);
    gui_origin(GUI_CENTER_LEFT);
    gui_begin_win("Inventory", x, 0, w, h, 0);
    gui_button_style(&gui_style_action_button);
    gui_origin(GUI_TOP_LEFT);
    int button_x = 2;
    int button_y = 2;
    for (int i = EQUIPMENT_SLOT_BAG1, j = 0; i <= EQUIPMENT_SLOT_BAG5; ++i, j++)
    {
        equipment_t *equipment = &_gs->inventory.equipment[i];
        if (!equipment->is_equipped)
            continue;
        gui_texture(icon_get_tex(), equipment->icon_clip, button_x, button_y);
        gui_button(gui_format_id("##bag%d", j), button_x, button_y,
            _action_button_wh, _action_button_wh, 0);
        button_y += _action_button_wh + 2;
    }
    const equipment_t *equipment =
        &_gs->inventory.equipment[_inventory.selected_slot];
    if (equipment->is_equipped)
    {
        dynamic_object_def_t *def = ent_get_dynamic_object_def(
            equipment->type_id);
        if (def->is_container)
        {
            gui_origin(GUI_TOP_LEFT);
            gui_color(0, 0, 0, 255);
            container_t *container = &_gs->inventory.containers[
                _inventory.selected_slot];
            int     container_w         = container->w;
            int     container_h         = container->h;
            int     w                   = _action_button_wh;
            int     h                   = _action_button_wh;
            bool32  have_hovered_button = 0;
            bool32  drag_hilight        = 0;
            int     drag_hilight_w;
            int     drag_hilight_h;
            int     hovered_button_x;
            int     hovered_button_y;
            int sx = gui_get_current_win_w() - (gui_style_window_1.states[0].border.widths[GUI_EDGE_RIGHT] + 2) - container_w * (w + 1);
            int sy = gui_style_window_1.states[0].border.widths[GUI_EDGE_TOP] + 2;
            int x = sx;
            int y = sy;
            for (int i = 0; i < container_w; ++i)
            {
                for (int j = 0; j < container_h; ++j)
                {
                    /* Choose style */
                    gui_button_style_t *style = &gui_style_inventory_item_button;
                    if (drag_hilight)
                    {
                        int min_x = hovered_button_x;
                        int max_x = hovered_button_x + (drag_hilight_w - 1);
                        int min_y = hovered_button_y;
                        int max_y = hovered_button_y + (drag_hilight_h - 1);
                        if (i >= min_x && i <= max_x &&
                            j >= min_y && j <= max_y)
                            style =
                                &gui_style_inventory_item_button_moused_over;
                    }
                    gui_button_style(style);
                    const char *button_id = gui_format_id(
                        "inventory_button%dx%d", i, j);
                    if (gui_button(button_id, x, y, w, h, 0) &&
                        _gs->move_requested_item.type ==
                            GS_MOVE_REQUEST_TYPE_NONE)
                    {
                        if (_gs->dragged_item.have)
                        {
                            if (_gs->dragged_item.drag_type ==
                                GS_DRAG_TYPE_OBJECT_IN_WORLD)
                            {
                                entity_t *entity = world_get_dynamic_object(
                                    &_gs->world, _gs->dragged_item.runtime_id);
                                muta_assert(entity);
                                entity_type_data_t *type_data =
                                    entity_get_type_data(entity);
                                dynamic_object_def_t *def =
                                    ent_get_dynamic_object_def(
                                        type_data->dynamic_object.type_id);
                                int x = i;
                                int y = j;
                                if (container_can_fit(container, x, y,
                                    def->width_in_container,
                                    def->height_in_container))
                                    gsi_pick_up_dragged_item(_gs,
                                        _inventory.selected_slot, x, y);
                                else
                                {
                                    gsi_clear_dragged_item(_gs);
                                    gsi_write_local_error_message("Object does "
                                        "not fit in container.");
                                    LOG_DEBUG("Moved object does not fit in "
                                        "this slot (slot: %d, %d, object size: "
                                        "%d, %d).", x, y,
                                        (int)def->width_in_container,
                                        (int)def->height_in_container);
                                }
                            } else if (_gs->dragged_item.drag_type ==
                                GS_DRAG_TYPE_OBJECT_IN_INVENTORY)
                            {
                                /* Try to move it. */
                                item_t *item = inventory_find_item(
                                    &_gs->inventory,
                                    _gs->dragged_item.runtime_id);
                                dynamic_object_def_t *item_def =
                                    ent_get_dynamic_object_def(item->type_id);
                                int x = i;
                                int y = j;
                                if (container_can_move(container,
                                    _gs->dragged_item.runtime_id, x, y,
                                    item_def->width_in_container,
                                    item_def->height_in_container))
                                    gsi_request_move_dragged_item_in_inventory(
                                        _gs, _inventory.selected_slot, (uint8)x,
                                        (uint8)y);
                                else
                                {
                                    gsi_clear_dragged_item(_gs);
                                    gsi_write_local_error_message("Object does "
                                        "not fit in that slot.");
                                    LOG_DEBUG("Picked up object does not "
                                        "fit in this inventory slot (slot: %d, "
                                        "%d, object size: %d, %d).", x, y,
                                        (int)def->width_in_container,
                                        (int)def->height_in_container);
                                }
                            }
                        } else
                        {
                            int x = i;
                            int y = j;
                            item_t *item = container_get_item_at(container,
                                x, y);
                            if (item)
                                gsi_drag_item_in_inventory(_gs, item);
                        }
                    }
                    if (!have_hovered_button && gui_is_button_hovered())
                    {
                        have_hovered_button = 1;
                        hovered_button_x    = i;
                        hovered_button_y    = j;
                        if (_gs->dragged_item.have)
                        {
                            drag_hilight    = 1;
                            drag_hilight_w  = _gs->dragged_item.w_in_inventory;
                            drag_hilight_h  = _gs->dragged_item.h_in_inventory;
                        }
                    }
                    y += h + 1;
                }
                x += w + 1;
                y = gui_style_window_1.states[0].border.widths[GUI_EDGE_TOP] +
                    2;
            }
            bool32 dragging_inventory_item = _gs->dragged_item.have &&
                _gs->dragged_item.drag_type == GS_DRAG_TYPE_OBJECT_IN_INVENTORY;
            dobj_runtime_id_t dragged_item_id = _gs->dragged_item.runtime_id;
            bool32 requested_item_move =
                _gs->move_requested_item.type != GS_MOVE_REQUEST_TYPE_NONE;
            dobj_runtime_id_t moved_item_id =
                _gs->move_requested_item.runtime_id;
            uint32 num_items = container->num_items;
            for (uint32 i = 0; i < num_items; ++i)
            {
                item_t  *item       = &container->items[i];
                int     x           = sx + item->x * (w + 1);
                int     y           = sy + item->y * (h + 1);
                if (!item->repeat_icon)
                {
                    if (dragging_inventory_item &&
                        dragged_item_id == item->runtime_id)
                    {
                        uint8 color[4] = {255, 255, 255, 128};
                        gui_texture_c(icon_get_tex(), item->inventory_icon_clip,
                            x, y, color);
                    } else if (requested_item_move &&
                        moved_item_id == item->runtime_id)
                    {
                        uint8 color[4] = {255, 128, 128, 128};
                        gui_texture_c(icon_get_tex(), item->inventory_icon_clip,
                            x, y, color);
                    } else
                        gui_texture(icon_get_tex(), item->inventory_icon_clip,
                            x, y);
                } else
                {
                    uint8 color[4];
                    if (dragging_inventory_item &&
                        dragged_item_id == item->runtime_id)
                    {
                        color[0] = 255;
                        color[1] = 255;
                        color[2] = 255;
                        color[3] = 128;
                    } else if (requested_item_move &&
                        moved_item_id == item->runtime_id)
                    {
                        color[0] = 255;
                        color[1] = 128;
                        color[2] = 128;
                        color[3] = 128;
                    } else
                    {
                        color[0] = 255;
                        color[1] = 255;
                        color[2] = 255;
                        color[3] = 255;
                    }
                    for (int j = 0; j < item->w; ++j)
                        for (int k = 0; k < item->h; ++k)
                            gui_texture_c(icon_get_tex(),
                                item->inventory_icon_clip, x + j * (w + 1),
                                y + k * (h + 1), color);
                }
            }
        }
    }
    gui_end_win();
}

static void
_draw_action_buttons(void)
{
    muta_assert(MAX_ACTION_BUTTONS == 64);
    tex_t   *icon_tex   = icon_get_tex();
    int     y           = _border_gap;
    int     row_size    = 16;
    int     spacing     = 1;
    int     width       = _action_button_wh * row_size + (row_size - 1) * spacing;
    int     start_x     = core_asset_config.resolution_w / 2 - (width / 2);
    int     x           = start_x;
    gui_origin(GUI_BOTTOM_LEFT);
    gui_button_style(&gui_style_action_button);
    gui_color(0, 0, 0, 255);
    int first_button_index = 0;
    /* Bottom bars */
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < row_size; ++j)
        {
            action_button_t *button = &_gs->action_buttons[first_button_index + j];
            if (!button->ability)
                gui_rectangle(x, y, _action_button_wh, _action_button_wh);
            else
                gui_texture(icon_tex, button->icon_clip, x, y);
            if (gui_button(gui_format_id("%s##ab", button->key_name), x, y,
                _action_button_wh, _action_button_wh, 0))
                _call_action_button(button);
            x += _action_button_wh + spacing;
        }
        first_button_index += row_size;
        x = start_x;
        y += _action_button_wh + spacing;
    }
}

static void
_draw_debug_window(void)
{
    const world_t *world = &_gs->world;
    int x = _border_gap;
    int y = x;
    int w = (int)(0.267f * (float)core_asset_config.resolution_w);
    int h = w;
    if (!_debug_window.show)
        return;
    gui_win_style(0);
    gui_win_style(&gui_style_window_1);
    gui_origin(GUI_TOP_RIGHT);
    gui_begin_win("Debug view", x, y, w, h, GUI_WIN_CLICKTHROUGH);
    gui_origin(GUI_TOP_LEFT);
    gui_font(0);
    gui_textf("Entity sprite cmds: %u", 0, 2, 2, world->render_system.num_cmds);
    uint32 num_creature_cmds    = 0;
    uint32 num_player_cmds      = 0;
    for (uint32 i = 0; i < world->render_system.num_cmds; ++i)
    {
        int type = entity_get_type_data(
            world->render_system.cmds[i].entity)->type;
        switch (type)
        {
        case ENTITY_TYPE_CREATURE:
            num_creature_cmds++;
            break;
        case ENTITY_TYPE_PLAYER:
            num_player_cmds++;
            break;
        }
    }
    gui_textf("    Creature sprite cmds: %u", 0, gui_get_last_text_x(),
        gui_get_last_text_y() + gui_get_last_text_h(), num_creature_cmds);
    gui_textf("    Player sprite cmds: %u", 0, gui_get_last_text_x(),
        gui_get_last_text_y() + gui_get_last_text_h(), num_player_cmds);
    gui_textf("Entities moving: %u", 0, gui_get_last_text_x(),
        gui_get_last_text_y() + gui_get_last_text_h(),
        world->mobility_system.num_moving);
    gui_textf("Creatures: %u", 0, gui_get_last_text_x(),
        gui_get_last_text_y() + gui_get_last_text_h(),
        (uint32)hashtable_num_values(world->creature_table));
    gui_textf("Players: %u", 0, gui_get_last_text_x(),
        gui_get_last_text_y() + gui_get_last_text_h(),
        (uint32)hashtable_num_values(world->player_table));
    gui_textf("FPS : %d", 0, gui_get_last_text_x(),
        gui_get_last_text_y() + gui_get_last_text_h(), core_fps());
    gui_end_win();
}

static void
_draw_menu_bar(void)
{
    gui_origin(GUI_BOTTOM_RIGHT);
    gui_button_style(&gui_style_menu_bar_button);
    int x = _border_gap;
    int y = _border_gap;
    int w = _action_button_wh;
    int h = _action_button_wh;
    as_spritesheet_t *ss = as_claim_spritesheet(
        core_asset_config.icons_spritesheet_id, 0);
    float *clip = as_spritesheet_get_clip(ss, "question_mark");
    gui_texture(&ss->ta->tex, clip, x, y);
    if (gui_button("Character", x, y, w, h, 0))
    {
        if (_character.mode == CHARACTER_MODE_OFF)
            _character.mode = CHARACTER_MODE_PAPERDOLL;
        else
            _character.mode = CHARACTER_MODE_OFF;
    }
    x += w + 2;
    if (gui_button("Inventory", x, y, w, h, 0))
    {
        if (_inventory.show)
            _inventory.show = 0;
        else
            _inventory.show = 1;
    }
    as_unclaim_spritesheet(ss);
}

static void
_draw_character(void)
{
    switch (_character.mode)
    {
    case CHARACTER_MODE_OFF:
        break;
    case CHARACTER_MODE_PAPERDOLL:
    {
        int w = (int)(0.267f * (float)core_asset_config.resolution_w);
        int h = w;
        gui_origin(GUI_CENTER_LEFT);
        gui_win_style(&gui_style_window_1);
        gui_begin_win("Character", _border_gap, 0, w, h, 0);
        gui_origin(GUI_TOP_CENTER);
        gui_color(255, 255, 255, 255);
        entity_type_data_t *type_data = entity_get_type_data(
            _gs->player_entity);
        player_race_def_t *race = ent_get_player_race_def(
            type_data->player.race_id);
        gui_text(type_data->player.name, 0, 2, 0);
        gui_text(race->name, 0, 0,
            gui_get_last_text_y() + gui_get_last_text_h() + 2);
        as_creature_display_t *creature_display = as_get_creature_display(
            race->display_id);
        muta_assert(creature_display);
        as_ae_set_t     *ae_set = as_get_ae_set(creature_display->ae_set);
        as_ae_t         *ae     = as_claim_ae(ae_set->idle_id, 0);
        ae_animation_t  *anim   = &ae->ae.anims[ISODIR_SOUTH_EAST];
        if (anim->num_frames)
        {
            ae_frame_t *frame = &anim->frames[0];
            ae_layer_t *layer = &frame->layers[AE_LAYER_BODY];
            gui_origin(GUI_CENTER_CENTER);
            gui_texture_s(&layer->ta->tex, layer->clip, 0, 0, 4.f, 4.f);
        }
        as_unclaim_ae(ae);
        gui_end_win();
    }
        break;
    }
}

static void
_draw_clicked_entities(void)
{
    uint32 num = _gs->clicked_entities.num;
    if (!num)
        return;
    gui_button_style_t *button_style = gui_get_default_button_style();
    int window_w = (int)((float)core_asset_config.resolution_w / 12.f);
    int button_w = window_w;
    int button_h = gui_get_button_style_max_font_height(button_style) + 4;
    int window_h = num * button_h;
    gui_origin(GUI_TOP_LEFT);
    gui_win_style(&gui_style_topless_window);
    gui_button_style(0);
    gui_begin_empty_win("ClickedEntities", _gs->clicked_entities.click_x,
        _gs->clicked_entities.click_y, window_w, window_h, 0);
    for (uint32 i = 0; i < num; ++i)
    {
        if (gui_button(
            gui_format_id("%s%u", entity_get_name(_gs->clicked_entities.all[i]),
            i), 0, i * button_h, button_w, button_h, 0))
            gsi_target_clicked_entity(_gs, i);
    }
    gui_end_win();
}

static void
_draw_entity_frames(void)
{
    gui_win_style(&gui_style_window_1);
    gui_origin(GUI_TOP_LEFT);
    gui_font(0);
    gui_color(255, 255, 255, 255);
    int x = _border_gap;
    int y = _border_gap;
    int w = _action_button_wh * 3;
    int h = _action_button_wh;
    gui_begin_win("PlayerFrame", x, y, w, h, 0);
    gui_origin(GUI_CENTER_CENTER);
    gui_text(entity_get_name(_gs->player_entity), 0, 0, 0);
    gui_end_win();
    if (_gs->target_entity)
    {
        gui_origin(GUI_TOP_LEFT);
        x += w + _border_gap;
        gui_begin_win("TargetFrame", x, y, w, h, 0);
        gui_origin(GUI_CENTER_CENTER);
        gui_text(entity_get_name(_gs->target_entity), 0, 0, 0);
        gui_end_win();
        gui_origin(GUI_TOP_LEFT);
        /* Target frame menu */
        entity_type_data_t *type_data = entity_get_type_data(
            _gs->target_entity);
        if (type_data->type == ENTITY_TYPE_DYNAMIC_OBJECT)
        {
            gui_begin_win("TargetFrameMenu", x, y + h, w, h, 0);
            gui_button_style(0);
            dynamic_object_def_t *def = ent_get_dynamic_object_def(
                type_data->dynamic_object.type_id);
            if (!(def->flags & ENT_DYNAMIC_OBJECT_DISABLE_PICK_UP) &&
                gui_button("Pick up", 0, 0, w, h, 0) &&
                !_gs->dragged_item.have)
            {
                muta_assert(def);
                _gs->dragged_item.have       = 1;
                _gs->dragged_item.runtime_id = type_data->dynamic_object.id;
                _gs->dragged_item.drag_type  = GS_DRAG_TYPE_OBJECT_IN_WORLD;
                _gs->dragged_item.w_in_inventory = def->width_in_container;
                _gs->dragged_item.h_in_inventory = def->height_in_container;
                gsgui_on_start_drag_dynamic_object(
                    type_data->dynamic_object.type_id);
            }
            gui_end_win();
        }
    }
}

static void
_draw_fps(void)
{
    gui_origin(GUI_TOP_RIGHT);
    gui_textf("FPS: %d", 0, 2, 2, core_fps());
}

static void
_draw_charge_bar(void)
{
    if (!_gs->charge_bar.active)
        return;
    gui_origin(GUI_BOTTOM_CENTER);
    gui_progress_bar_style(&gui_style_player_charge_bar);
    gui_progress_bar_int(gui_format_id("%s##chargebar", _gs->charge_bar.title),
        0, 200, 200, 32, GUI_PROGRESS_DIRECTION_LEFT_TO_RIGHT, 0,
        _gs->charge_bar.current_ms, _gs->charge_bar.max_ms);
}

static void
_draw_name_plates(void)
{
    gui_origin(GUI_TOP_LEFT);
    gui_progress_bar_style(&gui_style_name_plate_health_bar);
    name_plate_system_t *system = &_gs->world.name_plate_system;
    render_world_t *render_world = &_gs->render_world;
    uint32 num_components = system->num_components;
    for (uint32 i = 0; i < num_components; ++i)
    {
        name_plate_component_t *component = &system->components[i];
        int last_tile_position[3];
        for (int j = 0; j < 3; ++j)
            last_tile_position[j] = component->last_tile_position[j] -
                render_world->cached.tile_position[j];
        int tile_position[3];
        for (int j = 0; j < 3; ++j)
            tile_position[j] = component->tile_position[j] -
                render_world->cached.tile_position[j];
        int x1 = render_world_compute_tile_pixel_x(render_world,
            last_tile_position[0], last_tile_position[1], 0);
        int y1 = render_world_compute_tile_pixel_y(render_world,
            last_tile_position[0], last_tile_position[1], last_tile_position[2],
            0);
        int x2 = render_world_compute_tile_pixel_x(render_world,
            tile_position[0], tile_position[1], 0);
        int y2 = render_world_compute_tile_pixel_y(render_world,
            tile_position[0], tile_position[1], tile_position[2], 0);
        int x = x1 + (int)(component->percentage_travelled *
            (float)(x2 - x1)) + 1 + component->offset[0];
        int y = y1 + (int)(component->percentage_travelled * (float)(y2 - y1)) +
            component->offset[1];
        gui_progress_bar_int(gui_format_id("##nameplate%u", i), x, y,
            render_world->cached.tile_w - 2, 4,
            GUI_PROGRESS_DIRECTION_LEFT_TO_RIGHT, 0, component->num,
            component->max);
    }
}

static void
_setup_hotkeys(void)
{
    for (int i = 1; i < MAX_ACTION_BUTTONS + 1; ++i)
    {
        char buf[32];
        snprintf(buf, sizeof(buf), "Action Button %d", i);
        hk_new_action(buf, _capi_use_action_button, &_gs->action_buttons[i - 1],
            HK_PRESS_DOWN);
    }
    for (int i = 1; i < 10; ++i)
    {
        char buf1[32];
        snprintf(buf1, sizeof(buf1), "Action Button %d", i);
        char buf2[32];
        snprintf(buf2, sizeof(buf2), "%d", i);
        hk_bind_key(buf1, 0, hk_str_to_keycode(buf2), 0);
    }
    hk_bind_key("Action Button 10", 0, hk_str_to_keycode("0"), 0);
    /* Load player's own keys. */
    if (hk_load_from_file(_gs->hotkeys_path))
        LOG_ERR("Failed to load player hotkeys completely from file '%s'.",
            _gs->hotkeys_path);
}

static void
_load_character_data(const char *character_name)
{
    enum parse_cfg_file_result result = parse_cfg_file(_gs->action_buttons_path,
        _parse_actionbar_cfg_callback, 0);
    switch (result)
    {
    case PARSE_CFG_FILE_SUCCESS:
    case PARSE_CFG_FILE_FOPEN_FAILED:
        break;
    case PARSE_CFG_FILE_SYNTAX_ERROR:
        LOG_ERR("Syntax errors in action buttons file '%s'.",
            _gs->action_buttons_path);
        break;
    }
}

static void
_parse_actionbar_cfg_callback(void *ctx, const char *opt, const char *val)
{
    if (!strncmp(opt, "action_button_", 14))
    {
        uint32 index;
        if (sscanf(opt, "action_button_%u ", &index) != 1 &&
            sscanf(opt, "action_button_%u=", &index) != 1)
            return;
        if (index >= MAX_ACTION_BUTTONS)
            return;
        if (!str_is_int(val))
            return;
        if (val[0] == '-')
            return;
        uint32 ability_id = str_to_uint32(val);
        ability_def_t *ability = ab_get(ability_id);
        if (!ability)
            return;
        action_button_t *button = &_gs->action_buttons[index];
        action_button_set_ability(button, ability);
    }
}

static bool32
_is_str_action_button_name(const char *str)
{
    return !strncmp(_options.modified_keybind.action_name, "Action Button ",
        14);
}

static void
_update_action_button_names(void)
{
    for (int i = 0; i < MAX_ACTION_BUTTONS; ++i)
    {
        char buf[32];
        snprintf(buf, sizeof(buf), "Action Button %d", i + 1);
        hk_action_t *action = hk_get_action_by_name(buf);
        muta_assert(action);
        action_button_t *button = &_gs->action_buttons[i];
        hk_key_mod_pair_t *key_and_mod = &action->keys[0];
        hk_key_combo_to_str(key_and_mod->keycode, key_and_mod->mods,
            button->key_name);
    }
}

static void
_call_action_button(action_button_t *button)
{
    if (!button->ability)
        return;
    gsi_use_ability(_gs, button->ability->id);
}
