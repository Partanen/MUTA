#ifndef AUDIO_SYSTEM_H
#define AUDIO_SYSTEM_H

#include "../../shared/types.h"
#include "types.h"

#define MAX_SOUND_CHANNELS 32

/* Forward declaration(s) */
typedef struct as_sound_t   as_sound_t;
typedef struct sound_t      sound_t;

int
au_play_sound_asset(as_sound_t *sound, float volume);

int
au_play_sound_asset_on_channel(as_sound_t *sound, int channel, float volume,
    float pitch, bool32 loop);

int
au_play_music_asset_on_channel(as_sound_t *sound, int channel, float volume,
    float pitch, bool32 loop);

int
au_init();

int
au_destroy();

int
au_create_buffer(uint *buffer, const char *file);

int
au_destroy_buffer(uint *buffer);

int
au_load_sound_to_channel(sound_t sound, uint channel);

int
au_load_ogg(au_buf_t *dest_buffer, const char *file);

int
au_load_wav(au_buf_t *dest_buffer, const char *file);

int
au_play_sound(sound_t sound, float volume);

int
au_play_channel(uint channel);

int
au_play_sound_on_channel(sound_t sound, int channel, float volume, float pitch,
    bool32 loop);

int
au_play_music_on_channel(sound_t sound, int channel, float volume, float pitch,
    bool32 loop);

int
au_start_sound(uint channel, float volume, float pitch, bool32 loop);

int
au_pause_channel(uint channel);

int
au_pause_all_channels();

int
au_resume_channel(uint channel);

int
au_resume_all_channels();

int
au_stop_sound(sound_t sound);

int
au_stop_sounds(sound_t sound);

int
au_stop_channel(uint channel);

int
au_stop_all_channels();

int
au_get_free_channel(int *channel);

int
au_get_channel_state(uint channel);

int
au_check_al_error();

int
au_set_channel_lock(uint channel, bool32 locked);

int
au_set_channel_volume(uint channel, float channel_vol);

void
au_set_master_volume(float sound_vol, float music_vol);

void
au_update();

void
au_dequeue_sound_asset(as_sound_t *sa);

#endif /* AUDIO_SYSTEM_H */
