#ifndef MUTA_CLIENT_MOBILITY_COMPONENT_H
#define MUTA_CLIENT_MOBILITY_COMPONENT_H

/* mobility_component.h
 * A component that tracks time while an entity is non-instantaneously moving
 * from one tile to another. */

#include "component.h"
#include "../../shared/common_utils.h"

/* Forward declaration(s) */
typedef struct world_t                  world_t;
typedef struct entity_t                 entity_t;
typedef struct component_definition_t   component_definition_t;
typedef void*                           component_handle_t;

/* Types defined here */
typedef struct mobility_component_t         mobility_component_t;
typedef struct mobility_system_t            mobility_system_t;
typedef struct mobility_component_data_t    mobility_component_data_t;
typedef struct mobility_component_start_move_event_t
    mobility_component_start_move_event_t;
typedef struct mobility_component_moved_event_t
    mobility_component_moved_event_t;
typedef struct mobility_component_event_t   mobility_component_event_t;

enum mobility_component_event
{
    MOBILITY_COMPONENT_EVENT_START_MOVE,
    MOBILITY_COMPONENT_EVENT_STOP_MOVE,
    MOBILITY_COMPONENT_EVENT_MOVED
    /* MOBILITY_COMPONENT_MOVED is posted every frame when the entity is
     * transitioning from one tile to another, except on the last frame, in
     * which case MOBILITY_COMPONENT_EVENT_STOP_MOVE is posted. */
};

struct mobility_system_t
{
    fixed_pool(mobility_component_t)    components;
    mobility_component_data_t           *moving;
    uint32                              num_moving;
};

struct mobility_component_start_move_event_t
{
    float   seconds;
    int     new_position[3];
    int     last_position[3];
};

struct mobility_component_moved_event_t
{
    float percentage_travelled;
};

struct mobility_component_event_t
{
    int type;
    union
    {
        mobility_component_start_move_event_t   start_move;
        mobility_component_moved_event_t        moved;
    };
};
/* Struct passed to any mobility component events */

extern component_definition_t mobility_component_definition;

void
mobility_component_set_speed(component_handle_t handle, float speed);
/* Speed is the time in seconds it takes to pass from one tile to another.
 * If the entity is already moving, the change will not take immediate effect,
 * but will work after the current movement finishes. */

void
mobility_component_move(component_handle_t handle, int *position);
/* Walks the entity to given position in the given time. The new position is
 * immediately set to the destination.
 * If position is same as current position, does nothing. */

float
mobility_component_get_percentage_travelled(component_handle_t handle);
/* Returns a float from 0 to 1 based on how far the entity is from reaching it's
 * destionation tile. */

bool32
mobility_component_is_moving(component_handle_t handle);

#endif /* MUTA_CLIENT_MOBILITY_COMPONENT_H */
