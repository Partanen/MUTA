#ifndef MUTA_CLIENT_iso_sprite_component_H
#define MUTA_CLIENT_iso_sprite_component_H
#include "component.h"
#include "../../shared/common_utils.h"

typedef struct iso_sprite_component_t   iso_sprite_component_t;
typedef struct iso_sprite_system_t      iso_sprite_system_t;

struct iso_sprite_system_t
{
    fixed_pool(iso_sprite_component_t) components;
};

extern component_definition_t iso_sprite_component_definition;

void
iso_sprite_component_set_asset(component_handle_t handle, uint32 iso_sprite_id);

#endif /* MUTA_CLIENT_iso_sprite_component_H */
