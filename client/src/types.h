#ifndef MUTA_CLIENT_TYPES_H
#define MUTA_CLIENT_TYPES_H

typedef unsigned int tex_id_t; /* Renderer texture id */
typedef unsigned int au_buf_t; /* Audio buffer identifier */

#endif /* MUTA_CLIENT_TYPES_H */
