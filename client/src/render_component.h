#ifndef MUTA_CLIENT_RENDER_COMPONENT_H
#define MUTA_CLIENT_RENDER_COMPONENT_H

/* render_component.h
 * The component storing the actual renderable data of an entity after other
 * components have transformed it. */

#include "component.h"
#include "../../shared/common_utils.h"

/* Forward declaration(s) */
typedef struct world_t  world_t;
typedef struct entity_t entity_t;
typedef struct tex_t    tex_t;

/* Types defined here */
typedef struct render_component_t           render_component_t;
typedef struct render_entity_t              render_entity_t;
typedef struct render_system_t              render_system_t;
typedef struct render_component_sprite_t    render_component_sprite_t;
typedef struct render_component_cmd_t       render_component_cmd_t;

struct render_component_cmd_t
{
    uint32      sprite; /* Used internally */
    /*-- Use the below data to render --*/
    tex_t       *texture;
    float       clip[4];
    int         tile_position[3];
    int         last_tile_position[3];
    float       offset[2];
    float       percentage_travelled;
    /*-- The entity pointer is saved for mouse picking --*/
    entity_t    *entity;
};
/* render_component_cmd_t: draw command for a single entity sprite. */

struct render_system_t
{
    fixed_pool(render_component_t)          components;
    fixed_pool(render_component_sprite_t)   sprites;
    render_component_cmd_t                  *cmds;
    uint32                                  *reserved;
    uint32                                  num_cmds;
    int                                     cull_area[6];
};

extern component_definition_t render_component_definition;

void
render_system_set_cull_area(render_system_t *system, int area[6]);
/* render_system_set_cull_area() sets the area from which entities will be
 * pushed into the drawable list. Format is x, y, z, w, h, t. */

bool32
render_component_is_culled(component_handle_t handle);

render_component_sprite_t *
render_component_new_sprite(component_handle_t handle);

void
render_component_sprite_free(render_component_sprite_t *sprite);

void
render_component_sprite_set_texture(render_component_sprite_t *sprite,
    tex_t *texture);

void
render_component_sprite_set_clip(render_component_sprite_t *sprite,
    float *clip);

void
render_component_sprite_set_offset(render_component_sprite_t *sprite, float x,
    float y);

#endif /* MUTA_CLIENT_RENDER_COMPONENT_H */
