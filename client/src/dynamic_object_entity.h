/* Dynamic objects are any interactable objects in the world that aren't
 * creatures, for example, trees that may be cut down, or chests that could be
 * opened. In general, they behave rather similarly to creatures, but lack
 * health and similar attributes. */

#ifndef MUTA_CLIENT_DYNAMIC_OBJECT_ENTITY_H
#define MUTA_CLIENT_DYNAMIC_OBJECT_ENTITY_H

#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct entity_t entity_t;
typedef struct world_t  world_t;

/* Types defined here */
typedef struct dynamic_object_entity_t dynamic_object_entity_t;

struct dynamic_object_entity_t
{
    dobj_runtime_id_t   id;
    dobj_type_id_t      type_id;
};

entity_t *
dynamic_object_spawn(world_t *world, dobj_runtime_id_t id,
    dobj_type_id_t type, int direction, int position[3]);

#endif /* MUTA_CLIENT_DYNAMIC_OBJECT_ENTITY_H */
