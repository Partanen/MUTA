#include "core.h"
#include "assets.h"
#include "gl.h"
#include "render.h"
#include "client.h"
#include "login.h"
#include "shard.h"
#include "game_state.h"
#include "audio.h"
#include "slash_cmd.h"
#include "screen.h"
#include "gui_styles.h"
#include "world.h"
#include "event.h"
#include "gui.h"
#include "sky.h"
#include "icon.h"
#include "hotkey.h"
#include "scripts.h"
#include "../../shared/crypt.h"
#include "../../shared/get_opt.h"
#include "../../shared/abilities.h"

static gui_init_config_t    _gui_init_config;
static const char           *_help_str;

static void
_get_tex_dimensions(void *tex_ptr, int *ret_w, int *ret_h);

int main(int argc, char **argv)
{
    const char          *screen_name            = "Main Menu";
    bool32              editor_mode             = 0;
    bool32              editor_mode_overridden  = 0;
    get_opt_context_t   opt_context             = GET_OPT_CONTEXT_INITIALIZER;
    int                 opt;
    while ((opt = get_opt(&opt_context, argc, argv, "hs:le:")) != -1)
    {
        switch (opt)
        {
        case 'h':
            puts(_help_str);
            return 0;
        case 's':
            screen_name = opt_context.arg;
            break;
        case 'e':
            if (streq(opt_context.arg, "true"))
                editor_mode = 1;
            else if (streq(opt_context.arg, "false"))
                editor_mode = 0;
            else
            {
                puts("Invalid parameter for option -e (must be true or "
                    "false).");
                return 1;
            }
            editor_mode_overridden = 1;
            break;
        case 'l':
        {
            puts("Screens:");
            uint32      num_screens;
            screen_t    **screens = screens_get_all(&num_screens);
            for (uint32 i = 0; i < num_screens; ++i)
                printf("\t%s\n", screens[i]->name);
            return 0;
        }
            break;
        case '?':
            puts("Unknown command line option.");
            return 1;
        }
    }
    _gui_init_config.get_texture_dimensions_callback = _get_tex_dimensions;
    /* Each init function should pop up an error window of it's own on failure
     * before returning. */
    if (core_init(editor_mode_overridden, editor_mode))
    {
        fprintf(stderr, "core_init() failed.\n");
        return 1;
    }
    if (ev_init())
    {
        fprintf(stderr, "ev_init() failed.\n");
        return 1;
    }
    if (gui_init(&_gui_init_config))
    {
        fprintf(stderr, "gui_init() failed.\n");
        return 1;
    }
    if (gl_init(core_window_data()))
    {
        fprintf(stderr, "gl_init() failed.\n");
        return 1;
    }
    gui_font_t *default_font = gui_get_default_font();
    if (gl_generate_gui_font_texture(default_font))
    {
        fprintf(stderr, "gl_generate_gui_font_texture() failed.\n");
        return 1;
    }
    if (as_load())
    {
        fprintf(stderr, "as_load() failed.\n");
        return 1;
    }
    if (icon_init())
    {
        fprintf(stderr, "icon_init() failed.\n");
        return 1;
    }
    if (ab_load())
    {
        const char *err = "ab_load() failed.\n";
        core_error_window(err);
        return 1;
    }
    scripts_init();
    if (r_init())
    {
        fprintf(stderr, "r_init() failed.\n");
        return 1;
    }
    if (crypt_init())
    {
        fprintf(stderr, "crypt_init() failed.\n");
        return 1;
    }
    if (cl_init())
    {
        fprintf(stderr, "cl_init() failed.\n");
        return 1;
    }
    if (login_init())
    {
        fprintf(stderr, "login_init() failed.\n");
        return 1;
    }
    if (shard_init())
    {
        fprintf(stderr, "shard_init() failed.\n");
        return 1;
    }
    if (au_init())
    {
        fprintf(stderr, "au_init() failed.\n");
        return 1;
    }
    if (world_api_init())
    {
        fprintf(stderr, "world_api_init() failed.\n");
        return 1;
    }
    if (slash_init())
    {
        fprintf(stderr, "slash_init() failed.\n");
        return 1;
    }
    if (gui_styles_init())
    {
        fprintf(stderr, "gui_styles_init() failed.\n");
        return 1;
    }
    if (hk_init())
    {
        fprintf(stderr, "hk_init() failed.\n");
        return 1;
    }
    if (gs_init())
    {
        fprintf(stderr, "gs_init() failed.\n");
        return 1;
    }
    if (sky_init_api())
    {
        fprintf(stderr, "sky_init() failed.\n");
        return 1;
    }
    if (screens_init())
    {
        fprintf(stderr, "screens_init() failed.\n");
        return 1;
    }
    /*-- Set config options --*/
    gl_set_vsync(core_config.vsync);
    au_set_master_volume(core_config.sound_volume, core_config.music_volume);
    screen_t *screen = screens_get(screen_name);
    if (!screen)
    {
        fprintf(stderr, "No screen named '%s' found.\n", screen_name);
        return 1;
    }
    as_cursor_t *cursor = as_get_cursor("default");
    if (!cursor)
    {
        core_error_window("Cursor with name 'default' not found.");
        return 1;
    }
    core_set_cursor(cursor->data);
    if (core_start(argc, argv, screen))
    {
        fprintf(stderr, "core_start() failed.\n");
        return 1;
    }
#ifdef _MUTA_DEBUG /* Free stuff in dev builds only to detect leaks builds. */
    sky_destroy_api();
    as_bind_queued_textures();
    screens_destroy();
    gui_styles_destroy();
    gs_destroy();
    slash_destroy();
    gl_destroy_gui_font_texture(gui_get_default_font());
    gui_destroy();
    shard_destroy();
    login_destroy();
    cl_destroy();
    scripts_destroy();
    ab_destroy();
    icon_destroy();
    as_destroy();
    r_destroy();
    gl_destroy();
    au_destroy();
    ev_destroy();
    core_destroy();
#endif
    return 0;
}

static void
_get_tex_dimensions(void *tex_ptr, int *ret_w, int *ret_h)
{
    tex_t *tex = tex_ptr;
    if (tex)
    {
        *ret_w = (int)tex->w;
        *ret_h = (int)tex->h;
    } else
    {
        *ret_w = 0;
        *ret_h = 0;
    }
}

static const char *_help_str =
    "MUTA Client\n"
    "Command line options:\n"
    "-h:               Print this help dialogue.\n"
    "-s [SCREEN_NAME]: Start in different screen (for debugging purposes).\n"
    "-e [OPTION]:      Enable editor mode (true or false).";
