#ifndef MUTA_BITMAP_H
#define MUTA_BITMAP_H

/* Image and font stuctures
 * img_t is the pure bitmap data format used by the program.
 * The animation structures utilize the renderable structures - they are
 * generally pure data. */

#include "../../shared/stb/stb_image.h"
#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct tex_t tex_t;

/* Types defined here */
typedef struct img_t                img_t;
typedef struct iso_anim_frame_t     iso_anim_frame_t;
typedef struct iso_anim_t           iso_anim_t;
typedef struct anim_frame_t         anim_frame_t;
typedef struct anim_t               anim_t;

enum img_pxf_t
{
    IMG_RGB,
    IMG_RGBA,
    IMG_ALPHA
};

struct img_t
{
    uint8           *pixels;
    int             w, h;
    enum img_pxf_t  pxf;
    int             bytes_per_px;
};

struct iso_anim_frame_t
{
    struct
    {
        tex_t           *tex;
        float           clip[4];
        float           ox, oy;
        int             flip;
    } tex_areas[8];
    float dur;
};

struct iso_anim_t
{
    iso_anim_frame_t    *frames;
    int                 num_frames;
    float               base_dur;
};

struct anim_frame_t
{
    tex_t   *tex;
    float   clip[4];
    float   ox, oy;
    int     flip;
    float   dur;
};

struct anim_t
{
    anim_frame_t    *frames;
    int             num_frames;
    float           total_dur;
};

int
img_load(img_t *img, const char *path);

void
img_free(img_t *img);

#endif /* MUTA_BITMAP_H */
