#include "sky.h"
#include "core.h"
#include "assets.h"
#include "log.h"
#include "../../shared/common_utils.h"
#include <inttypes.h>

typedef struct cloud_def_t cloud_def_t;
typedef struct parse_context_t parse_context_t;

enum parse_mode
{
    PARSE_MODE_NONE,
    PARSE_MODE_SKY_TEXTURE_ID,
    PARSE_MODE_CLOUD
};

struct cloud_def_t
{
    float clip[4];
};

struct parse_context_t
{
    enum parse_mode mode;
    uint32          sky_texture_id;
};

static as_tex_t     *_sky_tex_asset;
static cloud_def_t  *_cloud_defs;
static float        _x_speed_proportion;
static float        _y_speed_proportion;

/* Virtual canvas that constitutes the sky. */
static float        _canvas_w;
static float        _canvas_h;
static float        _canvas_border_w; /* Area outside of screen (horizontal) */
static float        _canvas_border_h; /* Area outside of screen (vertical) */
static float        _cloud_max_x;
static float        _cloud_max_y;

static int
_on_clouds_file_def(parse_def_file_context_t *ctx, const char *def,
    const char *val);

static int
_on_clouds_file_opt(parse_def_file_context_t *ctx, const char *opt,
    const char *val);

int
sky_init_api(void)
{
    const char *file_path = "assets/sky.def";
    parse_context_t context =
    {
        .sky_texture_id = 0xFFFFFFFF,
        .mode           = PARSE_MODE_NONE
    };
    if (parse_def_file(file_path, _on_clouds_file_def, _on_clouds_file_opt, &context))
    {
        core_error_window("Failed to parse %s.", file_path);
        return 1;
    }
    _sky_tex_asset = as_claim_tex(context.sky_texture_id, 0);
    int tile_width      = core_asset_config.tile_width;
    int tile_top_height = core_asset_config.tile_top_height;
    _x_speed_proportion = 1.f;
    _y_speed_proportion = (float)tile_top_height / (float)tile_width;
    float max_cloud_w = 0;
    float max_cloud_h = 0;
    uint32 num_cloud_defs = darr_num(_cloud_defs);
    for (uint32 i = 0; i < num_cloud_defs; ++i)
    {
        float w = _cloud_defs[i].clip[2] - _cloud_defs[i].clip[0];
        float h = _cloud_defs[i].clip[3] - _cloud_defs[i].clip[1];
        if (w > max_cloud_w)
            max_cloud_w = w;
        if (h > max_cloud_h)
            max_cloud_h = h;
    }
    _canvas_w = (float)core_asset_config.resolution_w + 2.f * max_cloud_w;
    _canvas_h = (float)core_asset_config.resolution_h + 2.f * max_cloud_h;
    _canvas_border_w    = max_cloud_w;
    _canvas_border_h    = max_cloud_h;
    _cloud_max_x        = _canvas_w + max_cloud_w;
    _cloud_max_y        = _canvas_h + max_cloud_h;
    return 0;
}

void
sky_destroy_api(void)
{
    as_unclaim_tex(_sky_tex_asset);
    darr_free(_cloud_defs);
}

int
sky_init(sky_t *sky)
{
    memset(sky, 0, sizeof(*sky));
    int     canvas_w        = (int)_canvas_w;
    int     canvas_h        = (int)_canvas_h;
    float   canvas_border_w = _canvas_border_w;
    float   canvas_border_h = _canvas_border_h;
    int     num_cloud_defs  = (int)darr_num(_cloud_defs);
    sky->num_objects = num_cloud_defs ? SKY_MAX_OBJECTS : 0;
    uint32  num_objects     = sky->num_objects;
    for (uint32 i = 0; i < num_objects; ++i)
    {
        float       x           = (float)(rand() % canvas_w) - canvas_border_w;
        float       y           = (float)(rand() % canvas_h) - canvas_border_h;
        int         def_index   = rand() % num_cloud_defs;
        cloud_def_t *def        = &_cloud_defs[def_index];
        sky_object_t object =
        {
            .x      = x,
            .y      = y,
            .clip   = {def->clip[0], def->clip[1], def->clip[2], def->clip[3]}
        };
        sky->objects[i] = object;
    }
    sky->wind_speed = 10.f;
    return 0;
}

void
sky_destroy(sky_t *sky)
{
}

void
sky_update(sky_t *sky, double delta)
{
    float   wind_speed      = sky->wind_speed;
    float   x_speed         = delta * wind_speed * _x_speed_proportion;
    float   y_speed         = delta * wind_speed * _y_speed_proportion;
    float   max_x           = _cloud_max_x;
    float   max_y           = _cloud_max_y;
    int     canvas_h        = (int)_canvas_h;
    float   canvas_border_w = _canvas_border_w;
    float   canvas_border_h = _canvas_border_h;
    uint32  num_objects = sky->num_objects;
    for (uint32 i = 0; i < num_objects; ++i)
    {
        sky->objects[i].x += x_speed;
        sky->objects[i].y += y_speed;
        if (sky->objects[i].x < max_x && sky->objects[i].y < max_y)
            continue;
        float new_x = -canvas_border_w;
        float new_y = (float)(rand() % canvas_h) - canvas_border_h;
        sky->objects[i].x = new_x;
        sky->objects[i].y = new_y;
    }
}

void
sky_render(sky_t *sky)
{
    int     resolution_w        = core_asset_config.resolution_w;
    int     resolution_h        = core_asset_config.resolution_h;
    float   coordinate_space[4] = {0, 0, resolution_w, resolution_h};
    sb_begin(sb_opaque_shader, coordinate_space);
    uint32  num_objects = sky->num_objects;
    tex_t   *tex        = &_sky_tex_asset->tex;
    for (uint32 i = 0; i < num_objects; ++i)
    {
        sky_object_t *object = &sky->objects[i];
        sb_sprite(tex, object->clip, object->x, object->y, 0);
    }
    sb_end();
}

static int
_on_clouds_file_def(parse_def_file_context_t *ctx, const char *def,
    const char *val)
{
    parse_context_t *context = ctx->user_data;
    if (streq(def, "cloud"))
    {
        cloud_def_t def = {0};
        darr_push(_cloud_defs, def);
        context->mode = PARSE_MODE_CLOUD;
    } else
    if (streq(def, "sky_texture_id"))
    {
        uint32 v;
        if (sscanf(val, "%" PRIu32, &v) != 1)
        {
            LOG_ERR("Malformed texture ID for sky objects in sky.def.");
            return 1;
        }
        context->sky_texture_id = v;
        context->mode           = PARSE_MODE_SKY_TEXTURE_ID;
    }
    return 0;
}

static int
_on_clouds_file_opt(parse_def_file_context_t *ctx, const char *opt,
    const char *val)
{
    parse_context_t *context = ctx->user_data;
    switch (context->mode)
    {
    case PARSE_MODE_CLOUD:
    {
        if (streq(opt, "clip"))
        {
            cloud_def_t *def = &_cloud_defs[darr_num(_cloud_defs) - 1];
            if (sscanf(val, "%f, %f, %f, %f", &def->clip[0], &def->clip[1],
                &def->clip[2], &def->clip[3]) != 4)
            {
                LOG_ERR("Bad cloud clip in sky.def.");
                return 1;
            }
        }
    }
    default:
        break;
    }
    return 0;
}
