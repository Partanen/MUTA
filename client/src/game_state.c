#include <inttypes.h>
#include "game_state.h"
#include "game_state_internal.h"
#include "hotkey.h"
#include "shard.h"
#include "core.h"
#include "slash_cmd.h"
#include "world.h"
#include "render.h"
#include "log.h"
#include "sky.h"
#include "inventory.h"
#include "game_state_gui.h"
#include "icon.h"
#include "assets.h"
#include "script_api.h"
#include "scripts.h"
#include "../../shared/gui/gui.h"
#include "../../shared/emote.h"
#include "../../shared/client_packets.h"

#define MAX_ENTITIES 10000

enum target_event
{
    TARGET_EVENT_SET_MY_POSITION,
    TARGET_EVENT_DESPAWN
};

static game_state_t _gs;

static void
_move_in_direction(int dir);

static int
_get_mod_key_mask(void);

static void
_slash_exit(void *context, int argc, char **argv);

static void
_slash_logout(void *context, int argc, char **argv);

static void
_slash_reload(void *context, int argc, char **argv);

static bool32
_get_tile_by_screen_position(int x, int y, int ret_position[3]);

static void
_update_keyboard(void);

static void
_update_mouse(void);

static void
_update_charge_bar(double dt);

static void
_render_map(void);

static void
_capi_move_north(void *user_data);

static void
_capi_move_south(void *user_data);

static void
_capi_move_west(void *user_data);

static void
_capi_move_east(void *user_data);

static void
_capi_pick_up_target(void *user_data);

static void
_capi_toggle_options(void *user_data);

static void
_other_player_set_position_event_callback(entity_event_t *event,
    entity_listen_t *listen_data);

static void
_dynamic_object_set_position_event_callback(entity_event_t *event,
    entity_listen_t *listen_data);

static void
_dynamic_object_will_despawn_event_callback(entity_event_t *event,
    entity_listen_t *listen_data);

static void
_creature_set_position_event_callback(entity_event_t *event,
    entity_listen_t *listen_data);

static void
_creature_will_despawn_event_callback(entity_event_t *event,
    entity_listen_t *listen_data);

static bool32
_drag_to_move(entity_t *entity, render_world_t *render_world, int mouse_x,
    int mouse_y, int *ret_dir);
/* Compute a direction an entity should move to or change facing direction to
 * based on the given mouse coordinates. Returns non-zero if entity can move
 * to given direction. */

static void
_set_target(entity_t *entity);

static void
_move_render_camera_with_entity(render_camera_t *render_camera,
    entity_t *entity);

static void
_on_target_despawned(entity_event_t *event, entity_listen_t *listen);

static inline bool32
_should_cull(int player_position[3], int other_position[3]);
/* Check if a position is out of server-determined view distance. */

static void
_delayed_despawn(entity_t *entity);

static void
_clear_clicked_entities(void);

static void
_update_target(void);

static void
_update_clicked_entities(void);

static void
_new_action_and_keybind(const char *action_name,
    void (*callback)(void *user_data), void *user_data,
    enum hk_press_event_type event_type, int keycode);

static void
_setup_hotkeys(void);

static ability_script_t *
_get_current_charging_ability(void);

static void
_handle_player_set_position_event(entity_event_t *event,
    entity_listen_t *listen_data);

static int
_handle_svmsg_global_chat_broadcast(svmsg_global_chat_broadcast_t *s);

static int
_handle_svmsg_move_creature(svmsg_move_creature_t *s);

static int
_handle_svmsg_move_player(svmsg_move_player_t *s);

static int
_handle_svmsg_add_creature(svmsg_add_creature_t *s);

static int
_handle_svmsg_add_player(svmsg_add_player_t *s);

static int
_handle_svmsg_add_dynamic_object(svmsg_add_dynamic_object_t *s);

static int
_handle_svmsg_remove_creature(svmsg_remove_creature_t *s);

static int
_handle_svmsg_remove_player(svmsg_remove_player_t *s);

static int
_handle_svmsg_remove_dynamic_object(svmsg_remove_dynamic_object_t *s);

static int
_handle_svmsg_update_player_direction(svmsg_update_player_direction_t *s);

static int
_handle_svmsg_update_player_position(svmsg_update_player_position_t *s);

static int
_handle_svmsg_update_creature_direction(svmsg_update_creature_direction_t *s);

static int
_handle_svmsg_move_me(svmsg_move_me_t *s);

static int
_handle_svmsg_equip_on_me(svmsg_equip_on_me_t *s);

static int
_handle_svmsg_unequip_on_me(svmsg_unequip_on_me_t *s);

static int
_handle_svmsg_add_inventory_item(svmsg_add_inventory_item_t *s);

static int
_handle_svmsg_remove_inventory_item(svmsg_remove_inventory_item_t *s);

static int
_handle_svmsg_pick_up_dynamic_object_success(
    svmsg_pick_up_dynamic_object_success_t *s);

static int
_handle_svmsg_pick_up_dynamic_object_fail(
    svmsg_pick_up_dynamic_object_fail_t *s);

static int
_handle_svmsg_move_inventory_item(svmsg_move_inventory_item_t *s);

static int
_handle_svmsg_move_inventory_item_fail(svmsg_move_inventory_item_fail_t *s);

static int
_handle_svmsg_drop_inventory_item_success(
    svmsg_drop_inventory_item_success_t *s);

static int
_handle_svmsg_drop_inventory_item_fail(svmsg_drop_inventory_item_fail_t *s);

static int
_handle_svmsg_use_ability_result(svmsg_use_ability_result_t *s);

static int
_handle_svmsg_player_used_ability(svmsg_player_used_ability_t *s);

static int
_handle_svmsg_player_finished_ability_charge(
    svmsg_player_finished_ability_charge_t *s);

static int
_handle_svmsg_entity_dealt_damage(svmsg_entity_dealt_damage_t *s);

int
gs_init(void)
{
    int ret = 0;
    /*-- Register slash commands --*/
    if (slash_register("exit", _slash_exit, 0) ||
        slash_register("logout", _slash_logout, 0) ||
        slash_register("reload", _slash_reload, 0))
        {ret = 1; goto fail;}
    if (world_init(&_gs.world, 0, 0, MAX_ENTITIES))
        {ret = 2; goto fail;}
    if (render_world_init(&_gs.render_world))
        {ret = 3; goto fail;}
    _gs.visible_server_entities = 0;
    darr_reserve(_gs.visible_server_entities, 256);
    gsgui_init(&_gs);
    script_set_game_state(&_gs);
    _gs.action_buttons_path = dstr_create_empty(256);
    _gs.hotkeys_path        = dstr_create_empty(256);
    return ret;
    fail:
        core_error_window("Game state initialization failed with code %d.",
            ret);
        return ret;
}

void
gs_destroy(void)
{
    darr_free(_gs.visible_server_entities);
    world_destroy(&_gs.world);
    render_world_destroy(&_gs.render_world);
    hk_destroy();
}

void
gs_start_begin_session(void)
{
    muta_assert(_gs.session_state == GS_SESSION_STATE_NOT_IN_GAME);
    _gs.session_state = GS_SESSION_STATE_LOGGING_IN;
    inventory_init(&_gs.inventory);
}

int
gs_end_begin_session(uint32 map_id, player_runtime_id_t id, const char *name,
    player_race_id_t race, int x, int y, int z, int direction,
    uint8 move_speed, uint32 view_distance_h, uint32 view_distance_v,
    ability_id_t abilities[MAX_PLAYER_ABILITIES], uint32 num_abilities,
    uint32 health_current, uint32 health_max, bool32 dead)
{
    muta_assert(_gs.session_state == GS_SESSION_STATE_LOGGING_IN);
    muta_assert(strlen(name) <= MAX_CHARACTER_NAME_LEN);
    LOG("New session, map_id %u, player_id %u, name %s, race %u, "
        "x %d, y %d, z %d, direction %d, move speed %" PRIu8 ", horizontal "
        "view distance %u, vertical view distance %u, num abilities: %u.",
        map_id, id, name, race, x, y, z, direction, move_speed, view_distance_h,
        view_distance_v, num_abilities);
    muta_assert(_gs.player_entity == 0);
    if (world_load_map(&_gs.world, map_id))
        return 1;
    gsi_set_up_paths_and_create_directories(&_gs, name, shard_get_name());
    int pos[3] = {x, y, z};
    _gs.target_entity = 0;
    _gs.player_entity = player_spawn(&_gs.world, id, race, name, 0, direction,
        pos, move_speed, health_current, health_max, dead);
    entity_listen_t listen_data = {.id = TARGET_EVENT_SET_MY_POSITION};
    entity_listen_to_entity_event(_gs.player_entity, ENTITY_EVENT_SET_POSITION,
        &listen_data, _handle_player_set_position_event);
    _gs.session_state = GS_SESSION_STATE_IN_GAME;
    memset(_gs.special_keys, 0, sizeof(_gs.special_keys));
    render_world_clear(&_gs.render_world);
    entity_get_position(_gs.player_entity, _gs.render_camera.position);
    _gs.render_camera.draw_tile_selector       = 0;
    _gs.render_camera.tile_selector_id_index   = 1;
    _gs.dragging_to_move                       = 0;
    _gs.view_distance_h                        = view_distance_h;
    _gs.view_distance_v                        = view_distance_v;
    memset(_gs.hovered_tile, 0, sizeof(_gs.hovered_tile));
    _gs.draw_tile_selector = 0;
    darr_clear(_gs.delayed_despawns);
    sky_init(&_gs.sky);
    _gs.clicked_entities.num        = 0;
    _gs.next_target.have            = 0;
    _gs.next_clicked_entities.clear = 0;
    _gs.dragged_item.have           = 0;
    _gs.move_requested_item.type    = GS_MOVE_REQUEST_TYPE_NONE;
    memcpy(_gs.abilities.data, abilities, num_abilities * sizeof(abilities[0]));
    _gs.abilities.num       = num_abilities;
    _gs.charge_bar.active   = 0;
    _setup_hotkeys();
    gsgui_begin(name);
    core_set_cursor(as_get_cursor("default")->data);
    _gs.should_end_session = 0;
    return 0;
}

void
gs_end_session(void)
{
    muta_assert(_gs.session_state != GS_SESSION_STATE_NOT_IN_GAME);
    _gs.session_state   = GS_SESSION_STATE_NOT_IN_GAME;
    _gs.player_entity   = 0;
    _gs.target_entity   = 0;
    sky_destroy(&_gs.sky);
}

bool32
gs_begin_started(void)
    {return _gs.session_state == GS_SESSION_STATE_LOGGING_IN;}

bool32
gs_in_session(void)
    {return _gs.session_state == GS_SESSION_STATE_IN_GAME;}

void
gs_update_and_render(double dt)
{
    muta_assert(gs_in_session());
    if (_get_tile_by_screen_position(core_mouse_x(), core_mouse_y(),
        _gs.hovered_tile))
        _gs.draw_tile_selector = 1;
    else
        _gs.draw_tile_selector = 0;
    _update_keyboard();
    _update_mouse();
    _update_charge_bar(dt);
    if (_gs.dragging_to_move)
        _gs.draw_tile_selector = 0;
    _update_target();
    _update_clicked_entities();
    /* Complete delayed despawns */
    for (uint32 i = 0; i < darr_num(_gs.delayed_despawns); ++i)
        if (entity_is_spawned(_gs.delayed_despawns[i]))
            entity_despawn(_gs.delayed_despawns[i]);
    darr_clear(_gs.delayed_despawns);
    world_update(&_gs.world, dt);
    sky_update(&_gs.sky, dt);
    _render_map();
    gsgui_update_and_render();
    gui_origin(GUI_TOP_RIGHT);
    if (_gs.should_end_session)
        gs_end_session();
}

void
gs_mouse_up(int button, int x, int y)
{
}

void
gs_mouse_down(int button, int x, int y)
{
}

static void
_on_key_event(int event_type, int key)
{
    if (gsgui_is_grabbing_input())
    {
        gsgui_on_key_event(event_type, key);
        return;
    }
    if (key == CORE_KEY_LSHIFT || key == CORE_KEY_RSHIFT ||
        key == CORE_KEY_LCTRL  || key == CORE_KEY_RCTRL ||
        key == CORE_KEY_LALT   || key == CORE_KEY_RALT)
        return;
    int mods = _get_mod_key_mask();
    hk_action_t *action = hk_get_action(key, mods);
    if (action && action->press_event_type == event_type)
        hk_call_action(action);
}

void
gs_key_down(int key, bool32 repeat)
    {_on_key_event(HK_PRESS_DOWN, key);}

void
gs_key_up(int key)
{
    if (key == CORE_KEY_RETURN && _gs.special_keys[GS_SPECIAL_KEY_ENTER])
        _gs.special_keys[GS_SPECIAL_KEY_ENTER] = 0;
    else
        _on_key_event(HK_PRESS_UP, key);
}

void
gs_flag_special_key_executed(int special_key)
{
    muta_assert(special_key >= 0 && special_key < NUM_GS_SPECIAL_KEYS);
    int scancode;
    switch (special_key)
    {
    case GS_SPECIAL_KEY_ENTER:
        scancode = CORE_KEY_RETURN;
        break;
    }
    if (core_key_down(scancode))
        _gs.special_keys[special_key] = 1;
}

void
gs_read_packet(int type, bbuf_t *bb, int *ret_incomplete, int *ret_dc)
{
    switch (type)
    {
    case SVMSG_GLOBAL_CHAT_BROADCAST:
    {
        svmsg_global_chat_broadcast_t s;
        *ret_incomplete = svmsg_global_chat_broadcast_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_global_chat_broadcast(&s);
    }
        break;
    case SVMSG_MOVE_PLAYER:
    {
        svmsg_move_player_t s;
        *ret_incomplete = svmsg_move_player_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_move_player(&s);
    }
        break;
    case SVMSG_MOVE_CREATURE:
    {
        svmsg_move_creature_t s;
        *ret_incomplete = svmsg_move_creature_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_move_creature(&s);
    }
        break;
    case SVMSG_ADD_CREATURE:
    {
        svmsg_add_creature_t s;
        *ret_incomplete = svmsg_add_creature_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_add_creature(&s);
    }
        break;
    case SVMSG_ADD_PLAYER:
    {
        svmsg_add_player_t s;
        *ret_incomplete = svmsg_add_player_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_add_player(&s);
    }
        break;
    case SVMSG_REMOVE_CREATURE:
    {
        svmsg_remove_creature_t s;
        *ret_incomplete = svmsg_remove_creature_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_remove_creature(&s);
    }
        break;
    case SVMSG_REMOVE_PLAYER:
    {
        svmsg_remove_player_t s;
        *ret_incomplete = svmsg_remove_player_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_remove_player(&s);
    }
        break;
    case SVMSG_UPDATE_PLAYER_DIRECTION:
    {
        svmsg_update_player_direction_t s;
        *ret_incomplete = svmsg_update_player_direction_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_update_player_direction(&s);
    }
        break;
    case SVMSG_UPDATE_PLAYER_POSITION:
    {
        svmsg_update_player_position_t s;
        *ret_incomplete = svmsg_update_player_position_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_update_player_position(&s);
    }
        break;
    case SVMSG_UPDATE_CREATURE_DIRECTION:
    {
        svmsg_update_creature_direction_t s;
        *ret_incomplete = svmsg_update_creature_direction_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_update_creature_direction(&s);
    }
        break;
    case SVMSG_MOVE_ME:
    {
        svmsg_move_me_t s;
        *ret_incomplete = svmsg_move_me_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_move_me(&s);
    }
        break;
    case SVMSG_ADD_DYNAMIC_OBJECT:
    {
        svmsg_add_dynamic_object_t s;
        *ret_incomplete = svmsg_add_dynamic_object_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_add_dynamic_object(&s);
    }
        break;
    case SVMSG_REMOVE_DYNAMIC_OBJECT:
    {
        svmsg_remove_dynamic_object_t s;
        *ret_incomplete = svmsg_remove_dynamic_object_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_remove_dynamic_object(&s);
    }
        break;
    case SVMSG_EQUIP_ON_ME:
    {
        svmsg_equip_on_me_t s;
        *ret_incomplete = svmsg_equip_on_me_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_equip_on_me(&s);
    }
        break;
    case SVMSG_UNEQUIP_ON_ME:
    {
        svmsg_unequip_on_me_t s;
        *ret_incomplete = svmsg_unequip_on_me_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_unequip_on_me(&s);
    }
        break;
    case SVMSG_ADD_INVENTORY_ITEM:
    {
        svmsg_add_inventory_item_t s;
        *ret_incomplete = svmsg_add_inventory_item_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_add_inventory_item(&s);
    }
        break;
    case SVMSG_REMOVE_INVENTORY_ITEM:
    {
        svmsg_remove_inventory_item_t s;
        *ret_incomplete = svmsg_remove_inventory_item_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_remove_inventory_item(&s);
    }
        break;
    case SVMSG_PICK_UP_DYNAMIC_OBJECT_SUCCESS:
    {
        svmsg_pick_up_dynamic_object_success_t s;
        *ret_incomplete = svmsg_pick_up_dynamic_object_success_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_pick_up_dynamic_object_success(&s);
    }
        break;
    case SVMSG_PICK_UP_DYNAMIC_OBJECT_FAIL:
    {
        svmsg_pick_up_dynamic_object_fail_t s;
        *ret_incomplete = svmsg_pick_up_dynamic_object_fail_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_pick_up_dynamic_object_fail(&s);
    }
        break;
    case SVMSG_MOVE_INVENTORY_ITEM:
    {
        svmsg_move_inventory_item_t s;
        *ret_incomplete = svmsg_move_inventory_item_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_move_inventory_item(&s);
    }
        break;
    case SVMSG_MOVE_INVENTORY_ITEM_FAIL:
    {
        svmsg_move_inventory_item_fail_t s;
        *ret_incomplete = svmsg_move_inventory_item_fail_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_move_inventory_item_fail(&s);
    }
        break;
    case SVMSG_DROP_INVENTORY_ITEM_SUCCESS:
    {
        svmsg_drop_inventory_item_success_t s;
        *ret_incomplete = svmsg_drop_inventory_item_success_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_drop_inventory_item_success(&s);
    }
        break;
    case SVMSG_DROP_INVENTORY_ITEM_FAIL:
    {
        svmsg_drop_inventory_item_fail_t s;
        *ret_incomplete = svmsg_drop_inventory_item_fail_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_drop_inventory_item_fail(&s);
    }
        break;
    case SVMSG_USE_ABILITY_RESULT:
    {
        svmsg_use_ability_result_t s;
        *ret_incomplete = svmsg_use_ability_result_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_use_ability_result(&s);
    }
        break;
    case SVMSG_PLAYER_USED_ABILITY:
    {
        svmsg_player_used_ability_t s;
        *ret_incomplete = svmsg_player_used_ability_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_player_used_ability(&s);
    }
        break;
    case SVMSG_PLAYER_FINISHED_ABILITY_CHARGE:
    {
        svmsg_player_finished_ability_charge_t s;
        *ret_incomplete = svmsg_player_finished_ability_charge_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_player_finished_ability_charge(&s);
    }
        break;
    case SVMSG_ENTITY_DEALT_DAMAGE:
    {
        svmsg_entity_dealt_damage_t s;
        *ret_incomplete = svmsg_entity_dealt_damage_read(bb, &s);
        if (!*ret_incomplete)
            *ret_dc = _handle_svmsg_entity_dealt_damage(&s);
    }
        break;
    default:
        LOG_ERR("Undefined message type %d.\n", (int)type);
        *ret_dc = 1;
    }
    if (*ret_dc)
    {
        LOG_DEBUG("Set session state to GS_SESSION_STATE_NOT_IN_GAME.");
        _gs.should_end_session = 1;
    }
}

void
gs_log_system_msg(const char *msg)
{
    muta_assert(_gs.session_state == GS_SESSION_STATE_IN_GAME);
    gsgui_on_system_msg_received(msg);
}

void
gs_stop_action_charge(void)
{
    if (!_gs.charge_bar.active)
        return;
    bbuf_t bb = shard_send(CLMSG_STOP_ABILITY_CHARGE_SZ);
    if (bb.max_bytes)
        clmsg_stop_ability_charge_write(&bb);
    _gs.charge_bar.active = 0;
}

static void
_move_in_direction(int direction)
{
    component_handle_t handle = entity_get_component(_gs.player_entity,
        &mobility_component_definition);
    float travelled = mobility_component_get_percentage_travelled(handle);
    if (travelled < 0.8f)
        return;
    bbuf_t bb = shard_send(CLMSG_MOVE_IN_DIRECTION_SZ);
    if (!bb.max_bytes)
        return;
    clmsg_move_in_direction_t s;
    s.direction = (uint8)direction;
    clmsg_move_in_direction_write(&bb, &s);
}

static int
_get_mod_key_mask(void)
{
    int mods = 0;
    mods |= (core_key_down(CORE_KEY_LSHIFT) ? HK_MOD_SHIFT : 0);
    mods |= (core_key_down(CORE_KEY_RSHIFT) ? HK_MOD_SHIFT : 0);
    mods |= (core_key_down(CORE_KEY_LCTRL) ? HK_MOD_CTRL : 0);
    mods |= (core_key_down(CORE_KEY_RCTRL) ? HK_MOD_CTRL : 0);
    mods |= (core_key_down(CORE_KEY_LALT) ? HK_MOD_ALT : 0);
    mods |= (core_key_down(CORE_KEY_RALT) ? HK_MOD_ALT : 0);
    return mods;
}

static void
_slash_exit(void *context, int argc, char **argv)
    {core_stop();}

static void
_slash_logout(void *context, int argc, char **argv)
    {shard_disconnect();}

static void
_slash_reload(void *context, int argc, char **argv)
    {IMPLEMENTME();}

static bool32
_get_tile_by_screen_position(int x, int y, int ret_position[3])
{
    float world_x, world_y;
    render_world_translate_screen_to_world_pixel(&_gs.render_world, x, y,
        &world_x, &world_y);
    return render_world_find_tile_by_pixel_position(&_gs.render_world,
        &_gs.world, (int)world_x, (int)world_y, ret_position);
}

static void
_update_keyboard(void)
{
    if (gsgui_is_grabbing_input())
        return;
    uint32      num_hka;
    hk_action_t **hka = hk_get_repeat_actions(&num_hka);
    for (uint32 i = 0; i < num_hka; ++i)
        for (int j = 0; j < 2; ++j)
        {
            if (hka[i]->keys[j].keycode >= 0 &&  /* < 0 indicates unbound key */
                core_key_down(hka[i]->keys[j].keycode))
            {
                hk_call_action(hka[i]);
                break;
            }
        }
}

static void
_update_mouse(void)
{
    gui_bool_t  gui_hovered = gui_is_any_element_hovered();
    gui_bool_t  gui_pressed = gui_is_any_element_pressed();
    int         x           = core_mouse_x();
    int         y           = core_mouse_y();
    int         viewport[4];
    core_compute_target_viewport(viewport);
    bool32 mouse_within_viewport =
        x >= viewport[0] &&
        x < viewport[0] + viewport[2] &&
        y >= viewport[1] &&
        y < viewport[1] + viewport[3];
    if (mouse_within_viewport)
    {
        if (!_gs.dragged_item.have)
        {
            if (core_mouse_button_down_now(CORE_MOUSE_BUTTON_LEFT) &&
                !core_mouse_button_down(CORE_MOUSE_BUTTON_RIGHT) &&
                !gui_hovered && !gui_pressed)
            {
                if (_gs.clicked_entities.num > 0)
                    _clear_clicked_entities();
                else
                {
                    byte_buf_t bb = shard_send(CLMSG_FIND_PATH_SZ);
                    if (!bb.max_bytes)
                        return;
                    clmsg_find_path_t s;
                    s.x = _gs.hovered_tile[0];
                    s.y = _gs.hovered_tile[1];
                    s.z = _gs.hovered_tile[2] + 1;
                    clmsg_find_path_write(&bb, &s);
                    DEBUG_PRINTFF("Sent find path message to position "
                        "%d.%d.%d.\n", s.x, s.y, s.z);
                }
            }
            /* Right click to set target or open target selection menu. */
            if (!gui_is_any_element_hovered())
            {
                if (core_mouse_button_down_now(CORE_MOUSE_BUTTON_RIGHT) &&
                    !core_mouse_button_down(CORE_MOUSE_BUTTON_LEFT))
                {
                    float tx, ty;
                    render_world_translate_screen_to_world_pixel(&_gs.render_world,
                        x, y, &tx, &ty);
                    if (!_gs.dragged_item.have)
                    {
                        entity_t *entities[GS_MAX_CLICKED_ENTITIES];
                        uint32 num = render_world_find_entities_by_pixel_position(
                            &_gs.render_world, tx, ty, entities,
                            (uint32)(sizeof(entities) / sizeof(entities[0])));
                        if (num == 0)
                            _set_target(0);
                        else if (num == 1)
                            _set_target(entities[0]);
                        else if (num != _gs.clicked_entities.num ||
                            memcmp(_gs.clicked_entities.all, entities,
                                num * sizeof(entities[0])))
                        {
                            for (int i = 0; i < num; ++i)
                                _gs.clicked_entities.all[i] = entities[i];
                            _gs.clicked_entities.num       = num;
                            int tmp_x = x;
                            int tmp_y = y;
                            core_compute_pos_scaled_by_target_viewport(&tmp_x,
                                &tmp_y);
                            _gs.clicked_entities.click_x = tmp_x;
                            _gs.clicked_entities.click_y = tmp_y;
                        }
                    }
                }
            }
        } else /* Dragging something */
        {
            if (core_mouse_button_down_now(CORE_MOUSE_BUTTON_RIGHT))
                gsi_clear_dragged_item(&_gs);
            if (!gui_is_any_element_hovered())
            {
                if (core_mouse_button_down_now(CORE_MOUSE_BUTTON_LEFT))
                {
                    float tx, ty;
                    render_world_translate_screen_to_world_pixel(
                        &_gs.render_world, x, y, &tx, &ty);
                    if (_gs.dragged_item.have)
                    {
                        int tile_position[3];
                        if (render_world_find_tile_by_pixel_position(
                            &_gs.render_world, &_gs.world, tx, ty,
                            tile_position) && tile_position[2] < MAP_CHUNK_T)
                        {
                            int my_position[3];
                            entity_get_position(_gs.player_entity, my_position);
                            int x_diff = abs(tile_position[0] - my_position[0]);
                            int y_diff = abs(tile_position[1] - my_position[1]);
                            int z_diff = tile_position[2] - my_position[2];
                            int distance = (int)roundf(
                                sqrtf((float)(x_diff * x_diff + y_diff * y_diff)));
                            if (distance > MAX_ITEM_THROW_DISTANCE ||
                                z_diff > MAX_ITEM_THROW_HEIGHT)
                            {
                                LOG_DEBUG("Cannot drop dynamic object: target "
                                    "tile too far away.");
                                gsi_clear_dragged_item(&_gs);
                            } else
                            {
                                clmsg_drop_inventory_item_t s =
                                {
                                    .dobj_runtime_id =
                                        _gs.dragged_item.runtime_id,
                                    .x = tile_position[0],
                                    .y = tile_position[1],
                                    .z = (uint8)(tile_position[2] + 1)
                                };
                                _gs.move_requested_item.type =
                                    GS_MOVE_REQUEST_TYPE_DROP;
                                _gs.move_requested_item.runtime_id =
                                    _gs.dragged_item.runtime_id;
                                bbuf_t bb = shard_send(
                                    CLMSG_DROP_INVENTORY_ITEM_SZ);
                                if (bb.max_bytes)
                                    clmsg_drop_inventory_item_write(&bb, &s);
                                LOG_DEBUG("Attempt to drop dynamic object %u "
                                    "at %d, %d, %d.",
                                    _gs.dragged_item.runtime_id,
                                    tile_position[0], tile_position[1],
                                    tile_position[2]);
                            }
                        }
                        gsi_clear_dragged_item(&_gs);
                    }
                }
            }
        }
    }
    /*-- Drag to move with both mouse keys down --*/
    if (core_mouse_button_down(CORE_MOUSE_BUTTON_LEFT) &&
        core_mouse_button_down(CORE_MOUSE_BUTTON_RIGHT) &&
        !gui_pressed)
    {
        int direction;
        if (_drag_to_move(_gs.player_entity, &_gs.render_world,
            core_mouse_x(), core_mouse_y(), &direction))
            _move_in_direction(direction);
        _gs.dragging_to_move = 1;
    } else
        _gs.dragging_to_move = 0;
}

static void
_update_charge_bar(double dt)
{
    if (!_gs.charge_bar.active)
        return;
    _gs.charge_bar.current_ms += (uint32_t)round(dt * 1000.0);
    if (_gs.charge_bar.current_ms > _gs.charge_bar.max_ms)
        _gs.charge_bar.current_ms = _gs.charge_bar.max_ms;
}

static void
_render_map(void)
{
    int viewport[4];
    core_compute_target_viewport(viewport);
    _move_render_camera_with_entity(&_gs.render_camera, _gs.player_entity);
    float world_mouse_x, world_mouse_y;
    render_world_translate_screen_to_world_pixel(&_gs.render_world,
        core_mouse_x(), core_mouse_y(), &world_mouse_x, &world_mouse_y);
    /* Draw the tile selector...
     * - 1. if a location has been chosen with the right mouse.
     * - 2. if no UI element is pressed or hovered and we are not dragging to
     *      move the character. */
    if (_gs.draw_tile_selector &&  !gui_is_any_element_pressed() &&
        !gui_is_any_element_hovered())
    {
        _gs.render_camera.draw_tile_selector = 1;
        for (int i = 0; i < 3; ++i)
            _gs.render_camera.tile_selector_position[i] = _gs.hovered_tile[i];
    } else
        _gs.render_camera.draw_tile_selector = 0;
    render_world_update(&_gs.render_world, &_gs.render_camera, &_gs.world,
        viewport);
    sky_render(&_gs.sky);
}

static void
_capi_move_north(void *user_data)
    {_move_in_direction(ISODIR_NORTH);}

static void
_capi_move_south(void *user_data)
    {_move_in_direction(ISODIR_SOUTH);}

static void
_capi_move_west(void *user_data)
    {_move_in_direction(ISODIR_WEST);}

static void
_capi_move_east(void *user_data)
    {_move_in_direction(ISODIR_EAST);}

static void
_capi_pick_up_target(void *user_data)
{
    if (!_gs.target_entity)
        return;
    entity_type_data_t *type_data = entity_get_type_data(_gs.target_entity);
    if (type_data->type != ENTITY_TYPE_DYNAMIC_OBJECT)
        return;
    dynamic_object_def_t *def = ent_get_dynamic_object_def(
        type_data->dynamic_object.type_id);
    if (def->flags & ENT_DYNAMIC_OBJECT_DISABLE_PICK_UP)
        return;
    _gs.dragged_item.have       = 1;
    _gs.dragged_item.runtime_id = type_data->dynamic_object.id;
    _gs.dragged_item.drag_type  = GS_DRAG_TYPE_OBJECT_IN_WORLD;
    gsgui_on_start_drag_dynamic_object(type_data->dynamic_object.type_id);
}

static void
_capi_toggle_options(void *user_data)
{
    if (_gs.charge_bar.active) {
        gs_stop_action_charge();
        return;
    }
    if (_gs.dragged_item.have)
    {
        gsi_clear_dragged_item(&_gs);
        return;
    }
    gsgui_toggle_options();
}

static void
_other_player_set_position_event_callback(entity_event_t *event,
    entity_listen_t *listen_data)
{
    int *position_a = event->set_position.new_position;
    int position_b[3];
    entity_get_position(_gs.player_entity, position_b);
    if (_should_cull(position_b, position_a))
    {
        _delayed_despawn(event->entity);
        DEBUG_PRINTFF("Despawning player %u.\n",
            entity_get_type_data(event->entity)->player.id);
    }
}

static void
_dynamic_object_set_position_event_callback(entity_event_t *event,
    entity_listen_t *listen_data)
{
    int *position_a = event->set_position.new_position;
    int position_b[3];
    entity_get_position(_gs.player_entity, position_b);
    if (_should_cull(position_b, position_a))
    {
        _delayed_despawn(event->entity);
        DEBUG_PRINTFF("Despawning dynamic object %u.\n",
            entity_get_type_data(event->entity)->dynamic_object.id);
    }
}

static void
_dynamic_object_will_despawn_event_callback(entity_event_t *event,
    entity_listen_t *listen_data)
{
    uint32 num = darr_num(_gs.visible_server_entities);
    for (uint32 i = 0; i < num; ++i)
    {
        if (_gs.visible_server_entities[i] != event->entity)
            continue;
        darr_erase_unordered(_gs.visible_server_entities, i);
        DEBUG_PRINTFF("Removed dynamic object from "
            "_gs.visible_server_entities.\n");
        break;
    }
}

static void
_creature_set_position_event_callback(entity_event_t *event,
    entity_listen_t *listen_data)
{
    int *position_a = event->set_position.new_position;
    int position_b[3];
    entity_get_position(_gs.player_entity, position_b);
    if (_should_cull(position_b, position_a))
    {
        _delayed_despawn(event->entity);
        DEBUG_PRINTFF("Despawning creature %u.\n",
            entity_get_type_data(event->entity)->creature.id);
    }
}

static void
_creature_will_despawn_event_callback(entity_event_t *event,
    entity_listen_t *listen_data)
{
    uint32 num = darr_num(_gs.visible_server_entities);
    for (uint32 i = 0; i < num; ++i)
    {
        if (_gs.visible_server_entities[i] != event->entity)
            continue;
        darr_erase_unordered(_gs.visible_server_entities, i);
        DEBUG_PRINTFF("Removed creature from _gs.visible_server_entities.\n");
        break;
    }
}

static bool32
_drag_to_move(entity_t *entity, render_world_t *render_world, int mouse_x,
    int mouse_y, int *ret_dir)
{
    float world_x, world_y;
    render_world_translate_screen_to_world_pixel(render_world, mouse_x,
        mouse_y, &world_x, &world_y);
    int position[3];
    entity_get_position(entity, position);
    int px, py;
    render_world_tile_to_pixel_position(render_world, position, &px, &py);
    int     dx      = (int)(world_x - (float)px);
    int     dy      = (int)(world_y - (float)py);
    double  r       = atan2((float)dx, (float)dy) + PI;
    double  deg22   = 0.3926990817;
    if (r <= deg22)
        *ret_dir = ISODIR_NORTH_WEST;
    else if (r < deg22 * 3)
        *ret_dir = ISODIR_WEST;
    else if (r < deg22 * 5)
        *ret_dir = ISODIR_SOUTH_WEST;
    else if (r < deg22 * 7)
        *ret_dir = ISODIR_SOUTH;
    else if (r < deg22 * 9)
        *ret_dir = ISODIR_SOUTH_EAST;
    else if (r < deg22 * 11)
        *ret_dir = ISODIR_EAST;
    else if (r < deg22 * 13)
        *ret_dir = ISODIR_NORTH_EAST;
    else if (r < deg22 * 15)
        *ret_dir = ISODIR_NORTH;
    else
        *ret_dir = ISODIR_NORTH_WEST;
    return sqrtf((float)(dx * dx + dy * dy)) >= render_world->cached.tile_w;
}

static void
_set_target(entity_t *entity)
{
    if (!entity)
        _gs.next_target.is_null = 1;
    else
    {
        _gs.next_target.is_null    = 0;
        _gs.next_target.handle     = entity_get_handle(entity);
    }
    _gs.next_target.have = 1;
}

static void
_move_render_camera_with_entity(render_camera_t *render_camera,
    entity_t *entity)
{
    entity_get_position(entity, render_camera->position);
    component_handle_t mobility_component = entity_get_component(entity,
        &mobility_component_definition);
    render_camera->percentage_moved =
        mobility_component_get_percentage_travelled(mobility_component);
}

static void
_on_target_despawned(entity_event_t *event, entity_listen_t *listen)
{
    _gs.next_target.have   = 0;
    _gs.target_entity      = 0;
}

static inline bool32
_should_cull(int player_position[3], int other_position[3])
{
    int max_distance_h = _gs.view_distance_h;
    int max_distance_v = _gs.view_distance_v;
    if (abs(player_position[0] - other_position[0]) > max_distance_h)
        return 1;
    if (abs(player_position[1] - other_position[1]) > max_distance_h)
        return 1;
    if (abs(player_position[2] - other_position[2]) > max_distance_v)
        return 1;
    return 0;
}

static void
_delayed_despawn(entity_t *entity)
    {darr_push(_gs.delayed_despawns, entity);}

static void
_clear_clicked_entities(void)
    {_gs.next_clicked_entities.clear = 1;}


static void
_update_target(void)
{
    if (!_gs.next_target.have)
        return;
    _gs.next_target.have = 0;
    entity_t *entity;
    if (_gs.next_target.is_null)
        entity = 0;
    else
        entity = world_get_entity(&_gs.world, _gs.next_target.handle);
    if (_gs.target_entity == entity)
        return;
    if (_gs.target_entity != 0)
    {
        muta_assert(entity_is_spawned(_gs.target_entity));
        entity_stop_listening_to_entity_event(_gs.target_entity,
            TARGET_EVENT_DESPAWN);
    }
    _gs.target_entity = entity;
    if (entity)
    {
        entity_listen_t listen_data = {.id = TARGET_EVENT_DESPAWN};
        entity_listen_to_entity_event(entity, ENTITY_EVENT_WILL_DESPAWN,
            &listen_data, _on_target_despawned);
    }
    _clear_clicked_entities();
}

static void
_update_clicked_entities(void)
{
    if (!_gs.next_clicked_entities.clear)
        return;
    _gs.next_clicked_entities.clear = 0;
    if (_gs.clicked_entities.num == 0)
        return;
    _gs.clicked_entities.num = 0;
}

static void
_new_action_and_keybind(const char *action_name,
    void (*callback)(void *user_data), void *user_data,
    enum hk_press_event_type event_type, int keycode)
{
    hk_new_action(action_name, callback, user_data, event_type);
    hk_bind_key(action_name, 0, keycode, 0);
}

static void
_setup_hotkeys(void)
{
    /* Default key bindings */
    hk_clear();
    _new_action_and_keybind("Move North", _capi_move_north, 0, HK_PRESS_REPEAT,
        hk_str_to_keycode("W"));
    _new_action_and_keybind("Move South", _capi_move_south, 0, HK_PRESS_REPEAT,
        hk_str_to_keycode("S"));
    _new_action_and_keybind("Move West", _capi_move_west, 0, HK_PRESS_REPEAT,
        hk_str_to_keycode("A"));
    _new_action_and_keybind("Move East", _capi_move_east, 0, HK_PRESS_REPEAT,
        hk_str_to_keycode("D"));
    _new_action_and_keybind("Pick Up Target", _capi_pick_up_target, 0,
        HK_PRESS_UP, hk_str_to_keycode("F"));
    _new_action_and_keybind("Toggle Options", _capi_toggle_options, 0,
        HK_PRESS_UP, CORE_KEY_ESCAPE);
}

static ability_script_t *
_get_current_charging_ability(void)
{
    if (!_gs.charge_bar.active)
        return 0;
    return scripts_find_ability(_gs.charge_bar.ability_id);
}

static void
_handle_player_set_position_event(entity_event_t *event,
    entity_listen_t *listen_data)
{
    for (int i = 0; i < 3; ++i)
        _gs.render_camera.last_position[i] =
            event->set_position.last_position[i];
    world_set_camera_position(&_gs.world,
        event->set_position.new_position[0] / MAP_CHUNK_W,
        event->set_position.new_position[1] / MAP_CHUNK_W);
    uint32 num = darr_num(_gs.visible_server_entities);
    for (uint32 i = 0; i < num; ++i)
    {
        entity_t *entity = _gs.visible_server_entities[i];
        if (!entity_is_spawned(entity))
            continue;
        int position[3];
        entity_get_position(entity, position);
        if (_should_cull(event->set_position.new_position, position))
            _delayed_despawn(entity);
    }
}

static int
_handle_svmsg_remove_creature(svmsg_remove_creature_t *s)
{
    entity_t *e = world_get_creature(&_gs.world, s->runtime_id);
    if (!e)
    {
        DEBUG_PRINTFF("entity not found!\n");
        return 0;
    }
    entity_despawn(e);
    return 0;
}

static int
_handle_svmsg_remove_player(svmsg_remove_player_t *s)
{
    entity_t *entity = world_get_player(&_gs.world, s->world_session_id);
    if (!entity)
    {
        DEBUG_PRINTFF("player %u not found\n", s->world_session_id);
        return 0;
    }
    if (entity == _gs.player_entity)
        return 1;
    entity_despawn(entity);
    return 0;
}

static int
_handle_svmsg_update_player_direction(svmsg_update_player_direction_t *s)
{
    entity_t *entity = world_get_player(&_gs.world, s->world_session_id);
    if (!entity)
    {
        DEBUG_PRINTFF("entity not found!\n");
        return 0;
    }
    entity_set_direction(entity, s->direction);
    return 0;
}

static int
_handle_svmsg_add_creature(svmsg_add_creature_t *s)
{
    int position[3] = {s->x, s->y, s->z};
    entity_t *creature = creature_spawn(&_gs.world, s->runtime_id, s->type_id,
        s->sex, s->direction, position, s->health_current, s->health_max,
        s->dead);
    if (!creature)
        return 0;
    entity_listen_t listen_data = {.id = 0};
    entity_listen_to_entity_event(creature, ENTITY_EVENT_SET_POSITION,
        &listen_data, _creature_set_position_event_callback);
    listen_data.id = 1;
    entity_listen_to_entity_event(creature, ENTITY_EVENT_WILL_DESPAWN,
        &listen_data, _creature_will_despawn_event_callback);
    darr_push(_gs.visible_server_entities, creature);
    DEBUG_PRINTFF("Add creature message for creature %u.\n", s->runtime_id);
    return 0;
}

static int
_handle_svmsg_add_dynamic_object(svmsg_add_dynamic_object_t *s)
{
    int position[3] = {s->x, s->y, (int)s->z};
    entity_t *obj = dynamic_object_spawn(&_gs.world, s->runtime_id, s->type_id,
        s->direction, position);
    if (!obj)
        return 0;
    entity_listen_t listen_data = {.id = 0};
    entity_listen_to_entity_event(obj, ENTITY_EVENT_SET_POSITION,
        &listen_data, _dynamic_object_set_position_event_callback);
    listen_data.id = 1;
    entity_listen_to_entity_event(obj, ENTITY_EVENT_WILL_DESPAWN,
        &listen_data, _dynamic_object_will_despawn_event_callback);
    darr_push(_gs.visible_server_entities, obj);
    DEBUG_PRINTFF("Add dynamic object message for object %u.\n", s->runtime_id);
    return 0;
}

static int
_handle_svmsg_remove_dynamic_object(svmsg_remove_dynamic_object_t *s)
{
    entity_t *entity = world_get_dynamic_object(&_gs.world, s->runtime_id);
    if (!entity)
    {
        DEBUG_PRINTFF("dynamic object %u not found.\n", s->runtime_id);
        return 0;
    }
    entity_despawn(entity);
    return 0;
}

static int
_handle_svmsg_add_player(svmsg_add_player_t *s)
{
    char name[sizeof(s->display_name.data) + 1];
    memcpy(name, s->display_name.data, s->display_name.len);
    name[s->display_name.len] = 0;
    int position[3] = {s->x, s->y, s->z};
    entity_t *player = player_spawn(&_gs.world, s->world_session_id, s->race,
        name, s->sex, s->direction, position, s->move_speed, s->health_current,
        s->health_max, s->dead);
    if (!player)
        return 0;
    entity_listen_t listen_data = {.id = 0};
    entity_listen_to_entity_event(player, ENTITY_EVENT_SET_POSITION,
        &listen_data, _other_player_set_position_event_callback);
    darr_push(_gs.visible_server_entities, player);
    DEBUG_PRINTFF("Add player message for player %u.\n", s->world_session_id);
    return 0;
}

static int
_handle_svmsg_move_creature(svmsg_move_creature_t *s)
{
    entity_t *entity = world_get_creature(&_gs.world, s->runtime_id);
    if (!entity)
        return 0;
    int position[3] = {s->x, s->y, s->z};
    creature_walk(entity, position);
    return 0;
}

static int
_handle_svmsg_move_player(svmsg_move_player_t *s)
{
    entity_t *entity = world_get_player(&_gs.world, s->world_session_id);
    if (!entity)
        return 0;
    int position[3] = {s->x, s->y, s->z};
    player_walk(entity, position);
    return 0;
}

static int
_handle_svmsg_global_chat_broadcast(svmsg_global_chat_broadcast_t *s)
{
    char sender[sizeof(s->name.data) + 1];
    char msg[sizeof(s->message.data) + 1];
    memcpy(sender, s->name.data, s->name.len);
    memcpy(msg, s->message.data, s->message.len);
    sender[s->name.len] = 0;
    msg[s->message.len] = 0;
    gsgui_on_chat_msg_received(CHAT_MSG_GLOBAL_BROADCAST, sender, msg);
    return 0;
}

static int
_handle_svmsg_update_player_position(svmsg_update_player_position_t *s)
{
    entity_t *entity = world_get_player(&_gs.world, s->world_session_id);
    if (!entity)
    {
        DEBUG_PRINTF("%s: player not found.\n", __func__);
        return 0;
    }
    int position[3] = {s->x, s->y, s->z};
    entity_set_position(entity, position);
    return 0;
}

static int
_handle_svmsg_update_creature_direction(svmsg_update_creature_direction_t *s)
{
    entity_t *entity = world_get_creature(&_gs.world, s->runtime_id);
    if (!entity)
    {
        DEBUG_PRINTFF("creature %u not found.\n", s->runtime_id);
        return 0;
    }
    entity_set_direction(entity, s->direction);
    return 0;
}

static int
_handle_svmsg_move_me(svmsg_move_me_t *s)
{
    int position[3] = {s->x, s->y, s->z};
    int last_position[3];
    entity_get_position(_gs.player_entity, last_position);
    DEBUG_PRINTFF("Moving from %d %d %d to %d %d %d\n", last_position[0],
        last_position[1], last_position[2], position[0], position[1],
        position[2]);
    player_walk(_gs.player_entity, position);
    ability_script_t *script = _get_current_charging_ability();
    if (script && script->on_player_moved)
        script->on_player_moved();
    return 0;
}

static int
_handle_svmsg_equip_on_me(svmsg_equip_on_me_t *s)
{
    if (inventory_equip_slot(&_gs.inventory, s->equipment_slot, s->runtime_id,
        s->type_id))
        return 1;
    return 0;
}

static int
_handle_svmsg_unequip_on_me(svmsg_unequip_on_me_t *s)
{
    inventory_unequip_slot(&_gs.inventory, s->equipment_slot);
    return 0;
}

static int
_handle_svmsg_add_inventory_item(svmsg_add_inventory_item_t *s)
{
    if (_gs.session_state != GS_SESSION_STATE_LOGGING_IN &&
        _gs.session_state != GS_SESSION_STATE_IN_GAME)
    {
        LOG_ERR("Cannot add inventory item: not in game or logging in.");
        return 1;
    }
    container_t *container = inventory_get_container(&_gs.inventory,
        s->item.equipment_slot);
    if (!container)
    {
        LOG_ERR("No container found at slot %u.", s->item.equipment_slot);
        return 1;
    }
    if (container_add_item(container, s->item.runtime_id, s->item.type_id,
        s->item.x, s->item.y))
    {
        LOG_ERR("container_add_item() failed.");
        return 2;
    }
    return 0;
}

static int
_handle_svmsg_remove_inventory_item(svmsg_remove_inventory_item_t *s)
{
    IMPLEMENTME();
    return 0;
}

static int
_handle_svmsg_pick_up_dynamic_object_success(
    svmsg_pick_up_dynamic_object_success_t *s)
{
    muta_assert(s->equipment_slot < NUM_EQUIPMENTS_SLOTS);
    inventory_t *inv = &_gs.inventory;
    if (!inv->equipment[s->equipment_slot].is_equipped)
    {
        LOG_ERR("Equipment slot %u not equipped.", (uint)s->equipment_slot);
        return 1;
    }
    container_t *container = inventory_get_container(&_gs.inventory,
        s->equipment_slot);
    if (!container)
    {
        LOG_ERR("Equipment slot %u is not a container",
            (uint)s->equipment_slot);
        return 2;
    }
    if (container_add_item(container, s->runtime_id, s->type_id, s->x, s->y))
    {
        LOG_ERR("Couldn't add item to container.");
        return 3;
    }
    return 0;
}

static int
_handle_svmsg_pick_up_dynamic_object_fail(
    svmsg_pick_up_dynamic_object_fail_t *s)
{
    IMPLEMENTME();
    return 0;
}

static int
_handle_svmsg_move_inventory_item(svmsg_move_inventory_item_t *s)
{
    if (_gs.move_requested_item.type != GS_MOVE_REQUEST_TYPE_WITHIN_INVENTORY)
    {
        LOG_ERR("No item move was requested by the client.");
        return 1;
    }
    if (_gs.move_requested_item.runtime_id != s->dobj_runtime_id)
    {
        LOG_ERR("Moved dynamic object runtime IDs don't match (client: %u, "
            "server: %u).", _gs.move_requested_item.runtime_id,
            s->dobj_runtime_id);
        return 2;
    }
    _gs.move_requested_item.type = GS_MOVE_REQUEST_TYPE_NONE;
    item_t      *old_item = 0;
    container_t *old_container = 0;
    bool32 found = inventory_find_item_and_container(&_gs.inventory,
        s->dobj_runtime_id, &old_item, &old_container);
    muta_assert(found);
    container_t *new_container = inventory_get_container(&_gs.inventory,
        s->new_equipment_slot);
    if (!new_container)
    {
        LOG_ERR("No container in equipment slot %u.\n",
            (uint)s->new_equipment_slot);
        return 3;
    }
    if (old_container != new_container)
    {
        dobj_type_id_t type_id = old_item->type_id;
        container_del_item_at_index(old_container,
            (uint32)(old_item - old_container->items));
        if (container_add_item(new_container, s->dobj_runtime_id, type_id,
            s->new_x, s->new_y))
        {
            LOG_ERR("Failed to add item to new container.");
            return 4;
        }
    } else
    {
        if (container_set_item_position(old_container, old_item, s->new_x,
            s->new_y))
        {
            LOG_ERR("Failed update item position.");
            return 5;
        }
    }
    return 0;
}

static int
_handle_svmsg_move_inventory_item_fail(svmsg_move_inventory_item_fail_t *s)
{
    if (_gs.move_requested_item.type != GS_MOVE_REQUEST_TYPE_WITHIN_INVENTORY)
    {
        LOG_ERR("No item move was requested by the client.");
        return 1;
    }
    if (_gs.move_requested_item.runtime_id != s->dobj_runtime_id)
    {
        LOG_ERR("Moved dynamic object runtime IDs don't match (client: %u, "
            "server: %u).", _gs.move_requested_item.runtime_id,
            s->dobj_runtime_id);
        return 2;
    }
    _gs.move_requested_item.type = GS_MOVE_REQUEST_TYPE_NONE;
    return 0;
}

static int
_handle_svmsg_drop_inventory_item_success(
    svmsg_drop_inventory_item_success_t *s)
{
    if (_gs.move_requested_item.type != GS_MOVE_REQUEST_TYPE_DROP)
    {
        LOG_ERR("No item drop was requested by the client.");
        return 1;
    }
    item_t      *item;
    container_t *container;
    if (!inventory_find_item_and_container(&_gs.inventory,
        s->dobj_runtime_id, &item, &container))
    {
        LOG_ERR("Bad dynamic object runtime ID of dropped item (was %u, should "
            "be %u.", s->dobj_runtime_id,
            _gs.move_requested_item.runtime_id);
        return 2;
    }
    container_del_item_at_index(container, (uint32)(item - container->items));
    _gs.move_requested_item.type = GS_MOVE_REQUEST_TYPE_NONE;
    return 0;
}

static int
_handle_svmsg_drop_inventory_item_fail(svmsg_drop_inventory_item_fail_t *s)
{
    if (_gs.move_requested_item.type != GS_MOVE_REQUEST_TYPE_DROP)
    {
        LOG_ERR("No item drop was requested by the client.");
        return 1;
    }
    if (!inventory_find_item(&_gs.inventory, s->dobj_runtime_id))
    {
        LOG_ERR("No item with dynamic object %u in inventory.",
            s->dobj_runtime_id);
        return 2;
    }
    _gs.move_requested_item.type = GS_MOVE_REQUEST_TYPE_NONE;
    gsgui_on_system_msg_received("Failed to move dynamic object");
    return 0;
}

static int
_handle_svmsg_use_ability_result(svmsg_use_ability_result_t *s)
{
    bool32 have_ability = 0;
    uint32 num_abilities = _gs.abilities.num;
    for (uint32 i = 0; i < num_abilities; ++i)
        if (_gs.abilities.data[i] == s->ability_id)
        {
            have_ability = 1;
            break;
        }
    if (!have_ability)
    {
        LOG_ERR("Player does not have ability with ID %u.", s->ability_id);
        return 1;
    }
    entity_t *target_entity = 0;
    switch (s->target_type)
    {
    case COMMON_ENTITY_PLAYER:
        target_entity = world_get_player(&_gs.world, s->target_id);
        break;
    case COMMON_ENTITY_CREATURE:
        target_entity = world_get_creature(&_gs.world, s->target_id);
        break;
    case COMMON_ENTITY_DYNAMIC_OBJECT:
        target_entity = world_get_dynamic_object(&_gs.world, s->target_id);
        break;
    }
    if (!target_entity)
    {
        LOG_ERR("No target entity in server's ability use result message.");
        return 2;
    }
    ability_script_t *script = scripts_find_ability(s->ability_id);
    muta_assert(script);
    script->on_response(target_entity, s->success);
    return 0;
}

static int
_handle_svmsg_player_used_ability(svmsg_player_used_ability_t *s)
{
    IMPLEMENTME();
    return 0;
}

static int
_handle_svmsg_player_finished_ability_charge(
    svmsg_player_finished_ability_charge_t *s)
{
    if (s->target_type != COMMON_ENTITY_PLAYER &&
        s->target_type != COMMON_ENTITY_DYNAMIC_OBJECT &&
        s->target_type != COMMON_ENTITY_CREATURE)
    {
        LOG_ERR("Bad target type %u.", (uint)s->target_type);
        return 1;
    }
    if (s->player_runtime_id ==
        entity_get_type_data(_gs.player_entity)->player.id)
    {
        _gs.charge_bar.active = 0;
        ability_script_t *script = scripts_find_ability(s->ability_id);
        muta_assert(script);
        if (script->on_charge_finished)
        {
            script_entity_t *target = 0;
            switch (s->target_type)
            {
            case COMMON_ENTITY_PLAYER:
                target = world_get_player(&_gs.world, s->target_runtime_id);
                break;
            case COMMON_ENTITY_DYNAMIC_OBJECT:
                target = world_get_dynamic_object(&_gs.world,
                    s->target_runtime_id);
                break;
            case COMMON_ENTITY_CREATURE:
                target = world_get_creature(&_gs.world, s->target_runtime_id);
                break;
            }
            if (!target)
            {
                LOG_DEBUG("Target lost: should we handle this?.");
                return 3;
            }
            script->on_charge_finished(target, s->result);
        }
    } else
    {
        DEBUG_PRINTFF("Other player casting unimplemented.");
    }
    return 0;
}

static int
_handle_svmsg_entity_dealt_damage(svmsg_entity_dealt_damage_t *s)
{
    LOG_DEBUG("entity_type: %d, entity_runtime_id, %d, target_type: %d, target_runtime_id: %d",
        (int)s->entity_type, (int)s->entity_runtime_id, (int)s->target_type, (int)s->target_runtime_id);
    switch (s->target_type)
    {
    case COMMON_ENTITY_PLAYER:
    {
        entity_t *entity = world_get_player(&_gs.world, s->target_runtime_id);
        player_set_current_health(entity, s->health_current);
    }
        break;
    case COMMON_ENTITY_CREATURE:
    {
        entity_t *entity = world_get_creature(&_gs.world, s->target_runtime_id);
        creature_set_current_health(entity, s->health_current);
    }
        break;
    default:
        LOG_ERR("Bad entity type.");
        return 1;
    }
    return 0;
}
