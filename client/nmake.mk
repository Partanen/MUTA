include config.mk

CC					=	cl
EXE_NAME			=	muta.exe
INCLUDES			=	/I ..\libs\windows\include /I ..\libs\common\include
LIBS				=	SDL2.lib SDL2main.lib opengl32.lib libsodium.lib
LINK_PROGRAM		=	link $(L_FLAGS) $(OBJS) $(LIBS) /OUT:$(RUN_DIR)\$(EXE_NAME)
RM					=	del /Q /F
S					=	\\
ALL_DEPS			=	$(OBJS) link
ALL					=
COMPILE_OBJ			=	cl /c $(C_FLAGS) $< /Fo$@
COMPILE_SCRIPT_OBJ	=	cl /c $(C_FLAGS) $? /Fo$@
C_FLAGS_COM		=	/W3 $(INCLUDES) /D_CRT_SECURE_NO_WARNINGS /DAL_LIBTYPE_STATIC /EHsc
C_FLAGS_DB		=	$(C_FLAGS_COM) /D_MUTA_DEBUG /DMUTA_GUI_DEBUG /DEBUG /Zi /Od /Tc
C_FLAGS_OPT		=	$(C_FLAGS_COM) /W3 /Ox /GL /Tc
C_FLAGS			=	$(C_FLAGS_DB)
LIB_DIR			=	..\libs\windows\lib\$(ARCH)
L_FLAGS_DB		=	/SUBSYSTEM:CONSOLE /DEBUG /LIBPATH:$(LIB_DIR)
L_FLAGS_OPT		=	/SUBSYSTEM:WINDOWS /LIBPATH:$(LIB_DIR) /LTCG
L_FLAGS			=	$(L_FLAGS_DB)

.default: all

run:
	cd rundir && $(EXE_NAME) -s "Main Menu" -e true
