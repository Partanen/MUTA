CC					=	gcc
EXE_NAME			=	muta
C_FLAGS_COMMON		=	-Wall -std=gnu99 $(INCLUDES)
C_FLAGS_DEBUG		=	$(C_FLAGS_COMMON) -g -DEBUG -D_MUTA_DEBUG -DMUTA_GUI_DEBUG
C_FLAGS_SEMI_DEBUG	=	$(C_FLAGS_COMMON) -g -DNDEBUG -O1
C_FLAGS_OPT			=	$(C_FLAGS_COMMON) -DNDEBUG -O3 -flto
C_FLAGS				=	$(C_FLAGS_DEBUG)
L_FLAGS_COMMON		=	-std=gnu99 -Wall -pthread $(LIB_DIRS) -Wl,-rpath='.'
L_FLAGS_DEBUG		=	$(L_FLAGS_COMMON) -g
L_FLAGS_SEMI_DEBUG	=	$(L_FLAGS_COMMON) -g -O1 -flto
L_FLAGS_OPT			=	$(L_FLAGS_COMMON) -O3 -flto -static-libgcc
L_FLAGS				=	$(L_FLAGS_DEBUG)
ALL					=	make all-internal
ALL_SANITIZE		=	make all-sanitize-internal
COMPILE_OBJ			=	$(CC) -c $(C_FLAGS) $< -o $@
COMPILE_SCRIPT_OBJ	=	$(CC) -c $(C_FLAGS) $? -o $@
LINK_PROGRAM		=	$(CC) $(L_FLAGS) $(OBJS) $(LIBS) -o $(RUN_DIR)/$(EXE_NAME)
S					=	/
INCLUDES			=	-I ../libs/linux/include -I ../libs/common/include
LIB_DIRS			=	-L ../libs/linux/lib -L ../libs/linux/lib64
LIBS				=	-l:libSDL2.so -lm -lGL -ldl -l:libsodium.so -llua
RM					=	rm -f
OBJS_SANITIZE_DEPS	=	C_FLAGS += -fsanitize=address
LINK_SANITIZE_DEPS	=	L_FLAGS += -fsanitize=address
DEPEND				= cd ../shared/lua && make -B && cp liblua.a ../../libs/linux/lib

.DEFAULT_GOAL := all

all-internal:
	make -j$(shell grep -c '^processor' /proc/cpuinfo) objs
	make link

all-semi-debug: C_FLAGS = $(C_FLAGS_SEMI_DEBUG)
all-semi-debug: L_FLAGS = $(L_FLAGS_SEMI_DEBUG)
all-semi-debug: $(OBJS)

all-optimized: C_FLAGS = $(C_FLAGS_OPT)
all-optimized: L_FLAGS = $(L_FLAGS_OPT)
all-optimized: $(OBJS)

run:
	cd $(RUN_DIR) && ./muta -s "Main Menu" -e true

run-debug:
	cd $(RUN_DIR) && gdb --args muta -s "Main Menu" -e true
