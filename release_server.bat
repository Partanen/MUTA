:: MUTA server release packing script

if exist server_release rmdir /s /q server_release

call configure.bat

cd server
nmake clean
nmake all-opt
cd ..

cd worldd
nmake clean
nmake all-opt
cd ..

cd db-server
nmake clean
nmake all-opt
cd ..
    
if exist "server_release" rmdir /S /Q "release"

mkdir server_release
mkdir server_release\master
mkdir server_release\db
mkdir server_release\worldd

:: master
copy "server\default_config.cfg"        "server_release\master\config.cfg"
copy "server\rundir\muta_server.exe"    "server_release\master\muta_server.exe"

:: db
copy "db-server\default_config.cfg" "server_release\db\config.cfg"
copy "db-server\build\muta_db.exe" "server_release\db\muta_db.exe"
copy "db-server\default_sys_accs.txt" "server_release\sys_accs.txt"

:: worldd
copy "worldd\default_config.cfg" "server_release\worldd\config.cfg"
copy "worldd\rundir\muta_worldd.exe" "server_release\worldd\muta_worldd.exe"

if exist "MUTA-Data" xcopy /i /e "MUTA-Data" "server_release\muta-data"
xcopy /i /e "data" "server_release\data"

mkdir server_release\paperwork
xcopy /i /e "paperwork" "server_release\paperwork"

mkdir server_release\ql
xcopy /i /e "sql" "server_release\sql"

copy "COPYING" "server_release\COPYING"
copy "dep\muta-bin-deps\winbin\libsodium.dll" "server_release\libsodium.dll"
copy "db-server\build\libmariadb.dll" "server_release\libmariadb.dll"

echo start /b cmd /k call master\muta_server.exe -c master/config.cfg >> server_release\run.bat
echo start cmd /k call db\muta_db.exe -c db/config.cfg >> server_release\run.bat
echo start cmd /k call worldd\muta_worldd.exe -c worldd/config.cfg >> server_release\run.bat





:: Delete unnecessary files
del /s server_release\*~
del /s server_release\*.swp
del /s server_release\*.swpo

