CC					=	gcc
EXE_NAME			=	muta_master
INCLUDES			=	-I ../libs/linux/include
C_FLAGS_COMMON		=	$(INCLUDES) -Wall -std=gnu99
C_FLAGS_DEBUG		=	$(C_FLAGS_COMMON) -g -DEBUG -D_MUTA_DEBUG
C_FLAGS_SEMI_DEBUG	=	$(C_FLAGS_COMMON) -g -DNDEBUG -O1
C_FLAGS_OPTIMIZED	=	$(C_FLAGS_COMMON) -DNDEBUG -O2 -flto
C_FLAGS				=	$(C_FLAGS_DEBUG)
L_FLAGS_COMMON		=	$(LIB_DIRS) -std=gnu99 -Wall -pthread -Wl,-rpath='.'
L_FLAGS_DEBUG		=	$(L_FLAGS_COMMON) -g
L_FLAGS_SEMI_DEBUG	=	$(L_FLAGS_COMMON) -O1 -flto -g
L_FLAGS_OPTIMIZED	=	$(L_FLAGS_COMMON) -O2 -flto -static-libgcc
L_FLAGS				=	$(L_FLAGS_DEBUG)
LIBS				=	-lm -lsodium -lmariadb
LIB_DIRS			=	-L ../libs/linux/lib -Wl,-rpath='.'
COMPILE_OBJ			=	$(CC) -c $(C_FLAGS) $< -o $@
ALL					=	make -j$(shell grep -c '^processor' /proc/cpuinfo) objs && make link
ALL_DEPS			=
S					=	/
RM					=	rm -f
LINK_PROGRAM		=	$(CC) $(L_FLAGS) $(OBJS) $(LIBS) -o $(RUN_DIR)$(S)$(EXE_NAME)

.DEFAULT_GOAL := all

all-sdb: C_FLAGS = $(C_FLAGS_SEMI_DEBUG)
all-sdb: L_FLAGS = $(L_FLAGS_SEMI_DEBUG)
all-sdb: all

all-opt: C_FLAGS = $(C_FLAGS_OPTIMIZED)
all-opt: L_FLAGS = $(L_FLAGS_OPTIMIZED)
all-opt: all

run:
	cd rundir && ./run.sh

run-debug:
	cd rundir && ./run-debug.sh

../libs/linux/lib/libsodium.so:
	cd ../dep && make all
