#include "proxy.h"
#include "common.h"
#include "login_server.h"
#include "client.h"
#include "../../shared/netpoll.h"
#include "../../shared/net.h"
#include "../../shared/common_defs.h"
#include "../../shared/crypt.h"
#include "../../shared/proxy_packets.h"
#include "../../shared/containers.h"
#include "../../shared/hashtable.h"

#define MAX_PROXIES         16
#define PROXY_OUT_BUF_SZ    (8 * MUTA_MTU)
#define PROXY_IN_BUF_SZ     MUTA_MTU
#define PROXY_INDEX(ptr_)   ((uint32)((proxy_t*)(ptr_) - _proxies.all))

typedef struct proxy_t              proxy_t;
typedef struct proxy_socket_t       proxy_socket_t;
typedef struct account_socket_table account_socket_table_t;
typedef struct id_socket_table      id_socket_table_t;

enum proxy_flag
{
    PROXY_FLAG_AUTHED           = (1 << 0),
    PROXY_FLAG_WILL_DISCONNECT  = (1 << 1)
};

enum proxy_socket_flag
{
    PROXY_SOCKET_FLAG_IN_USE                = (1 << 0),
    PROXY_SOCKET_FLAG_CONFIRMED_BY_PROXY    = (1 << 1)
};

struct proxy_t
{
    socket_t    socket;
    uint32      flags;
    uint32      id;
    cryptchan_t cryptchan;
    struct
    {
        int     num_bytes;
        uint8   memory[PROXY_IN_BUF_SZ];
    } in_buf;
    struct
    {
        uint8   *memory;
        int     num_bytes;
    } out_buf;
};

/* A proxy socket represents a single client connection through a proxy */
struct proxy_socket_t
{
    uint32          proxy;
    uint32          id;
    uint32          login_session_id;
    account_db_id_t account_id;
    uint32          flags;
    uint32          used_index;
    void            *client;
    char            account_name[MAX_ACC_NAME_LEN + 1];
    /* We need this because when the client attempts to connect, it will not
     * know it's account id. */
};

hashtable_define(account_socket_table, uint64, uint32);
hashtable_define(id_socket_table, uint32, uint32);

static fixed_pool(proxy_t)  _proxies;
static uint32               _running_proxy_id;
static proxy_t              *_authed_proxies[MAX_PROXIES];
static uint32               _num_authed_proxies;
static uint8                _proxy_out_buf_mem[MAX_PROXIES * PROXY_OUT_BUF_SZ];
static netpoll_t            _netpoll;
static thread_t             _read_thread;
static bool32               _running;
static socket_t             _listen_socket;
static char                 _password[MAX_PROXY_PW_LEN + 1];
static segpool_t            _segpool;
static mutex_t              _segpool_mutex;

static struct
{
    fixed_pool(proxy_socket_t)  pool;
    uint32                      *used; /* For iteration */
    uint32                      num_used;
    account_socket_table_t      account_table;
    id_socket_table_t           id_table;
    uint32                      next_proxy_index;
    uint32                      running_id;
} _sockets;

static thread_ret_t
_main(void *args);

static void
_handle_read_proxy(com_event_read_proxy_t *event);

static void
_handle_read_unauthed_proxy(com_event_read_proxy_t *event);

static void
_disconnect_proxy(proxy_t *proxy, bool32 del_from_netpoll);

static int
_auth_proxy(proxy_t *proxy);

static int
_read_proxy_packet(proxy_t *proxy);

static int
_read_unauthed_proxy_packet(proxy_t *proxy);
/* Returns > 0 if proxy is to be authed, zero if nothing should be done and
 * < 0 if the proxy should be disconnected. */

static int
_handle_proxy_msg_pub_key(proxy_t *proxy, proxy_msg_pub_key_t *s);

static int
_handle_proxy_msg_stream_header(proxy_t *proxy, proxy_msg_stream_header_t *s);

static int
_handle_proxy_msg_login(proxy_t *proxy, fproxy_msg_login_t *s);

static int
_handle_fproxy_msg_opened_socket_result(proxy_t *proxy,
    fproxy_msg_opened_socket_result_t *s);

static int
_handle_fproxy_msg_player_cryptchan(proxy_t *proxy,
    fproxy_msg_player_cryptchan_t *s);

static int
_handle_fproxy_msg_player_packet(proxy_t *proxy, fproxy_msg_player_packet_t *s);

static int
_handle_fproxy_msg_close_socket(proxy_t *proxy, fproxy_msg_close_socket_t *s);

static bbuf_t
_send_msg_to_proxy(proxy_t *proxy, int size);

static inline bbuf_t
_send_const_crypt_msg_to_proxy(proxy_t *proxy, int size);

static inline bbuf_t
_send_var_crypt_msg_to_proxy(proxy_t *proxy, int size);

static int
_flush_proxy(proxy_t *proxy);

static void
_free_proxy_socket_resources(proxy_socket_t *socket);

static void
_close_proxy_socket(proxy_socket_t *socket);
/* This disconnects the proxy if notifying it of the closed socket fails */

static proxy_socket_t *
_get_socket_by_id(proxy_t *proxy, uint32 id);

int
proxy_init(void)
{
    int ret = 0;
    _num_authed_proxies = 0;
    if (netpoll_init(&_netpoll))
        {ret = 1; goto fail;}
    if (thread_init(&_read_thread))
        {ret = 2; goto fail;}
    segpool_init(&_segpool);
    mutex_init(&_segpool_mutex);
    fixed_pool_init(&_proxies, MAX_PROXIES);
    _running_proxy_id = 0;
    for (uint32 i = 0; i < MAX_PROXIES; ++i)
    {
        uint8 *memory = _proxy_out_buf_mem + i * PROXY_OUT_BUF_SZ;
        _proxies.all[i].out_buf.memory  = memory;
        _proxies.all[i].socket          = KSYS_INVALID_SOCKET;
    }
    uint32 max_players = com_config.max_clients;
    hashtable_einit(_sockets.account_table, max_players);
    hashtable_einit(_sockets.id_table, max_players);
    fixed_pool_init(&_sockets.pool, max_players);
    _sockets.used = emalloc(max_players * sizeof(uint32));
    strcpy(_password, "password");
    return 0;
    fail:
        LOG_EXT("Failed with code %d.", ret);
        return ret;
}

void
proxy_destroy(void)
{
    netpoll_destroy(&_netpoll);
    thread_destroy(&_read_thread);
}

int
proxy_start(void)
{
    int ret = 0;
    _listen_socket = net_tcp_ipv4_listen_sock(com_config.proxy_port, 5);
    if (_listen_socket == KSYS_INVALID_SOCKET)
    {
        LOG_ERROR("Failed to create proxy listen socket.");
        ret = 1;
        goto out;
    }
    netpoll_event_t netpoll_event;
    netpoll_event.events    = NETPOLL_READ;
    netpoll_event.data.ptr  = &_listen_socket;
    if (netpoll_add(&_netpoll, _listen_socket, &netpoll_event))
    {
        LOG_ERROR("netpoll_add() failed for proxy listen socket.");
        ret = 2;
        goto out;
    }
    LOG("Began to listen for proxies on port %u.", com_config.proxy_port);
    _running = 1;
    if (thread_create(&_read_thread, _main, 0))
    {
        LOG_ERROR("thread_create() failed for proxy thread.");
        ret = 3;
        goto out;
    }
    out:;
        return ret;
}

void
proxy_stop(void)
{
    _running = 0;
    net_close_blocking_sock(_listen_socket);
    thread_join(&_read_thread);
}

void
proxy_flush(void)
{
    for (uint32 i = 0; i < _num_authed_proxies; ++i)
        _flush_proxy(_authed_proxies[i]);
}

uint32
proxy_new_socket(const char *account_name, uint64 account_id,
    uint32 login_session_id, uint32 ip, uint8 *token)
{
    uint32 *old_socket_index = account_socket_table_find(
        &_sockets.account_table, account_id);
    if (old_socket_index)
    {
        LOG_DEBUG_EXT("Account already logged in. Disconnecting.");
        proxy_socket_t *ps = &_sockets.pool.all[*old_socket_index];
        if (ps->client)
            cl_disconnect(ps->client);
        else
            _close_proxy_socket(ps);
        return INVALID_PROXY_SOCKET;
    }
    if (!_num_authed_proxies)
    {
        LOG_DEBUG_EXT("No authed proxies available!");
        return INVALID_PROXY_SOCKET;
    }
    proxy_socket_t *ps = fixed_pool_new(&_sockets.pool);
    if (!ps)
        return INVALID_PROXY_SOCKET;
    /*-- Attempt to distribute connections to different proxies --*/
    uint32 connected_proxy_index =
        _sockets.next_proxy_index % _num_authed_proxies;
    _sockets.next_proxy_index =
        (_sockets.next_proxy_index + 1) % _num_authed_proxies;
    proxy_t *proxy  = _authed_proxies[connected_proxy_index];
    muta_assert(proxy->socket != KSYS_INVALID_SOCKET);
    /* Initialize the socket */
    uint32 ret = fixed_pool_index(&_sockets.pool, ps);
    uint32 id  = _sockets.running_id++;
    ps->account_id          = account_id;
    ps->proxy               = fixed_pool_index(&_proxies, proxy);
    ps->id                  = id;
    ps->login_session_id    = login_session_id;
    ps->flags               = 0;
    ps->client              = 0;
    strcpy(ps->account_name, account_name);
    /* Send a message to the proxy */
    tproxy_msg_opened_socket_t a_msg;
    a_msg.socket_id         = ps->id;
    a_msg.account_id        = account_id;
    a_msg.ip                = ip;
    a_msg.account_name      = ps->account_name;
    a_msg.account_name_len  = (uint8)strlen(ps->account_name);
    memcpy(a_msg.token, token, AUTH_TOKEN_SZ);
    bbuf_t bb = _send_var_crypt_msg_to_proxy(proxy,
        TPROXY_MSG_OPENED_SOCKET_COMPUTE_SZ(a_msg.account_name_len));
    if (!bb.max_bytes)
    {
        fixed_pool_free(&_sockets.pool, ps);
        return INVALID_PROXY_SOCKET;
    }
    int r = tproxy_msg_opened_socket_write_var_encrypted(&bb, &proxy->cryptchan,
        &a_msg);
    muta_assert(!r);
    account_socket_table_einsert(&_sockets.account_table, account_id, ret);
    id_socket_table_einsert(&_sockets.id_table, id, ret);
    ps->flags |= PROXY_SOCKET_FLAG_IN_USE;
    ps->used_index = _sockets.num_used;
    _sockets.used[_sockets.num_used++] = fixed_pool_index(&_sockets.pool, ps);
    LOG("Created a proxy socket for an incoming player.");
    return ret;
}

void
proxy_cancel_socket(uint64 account_id, uint32 login_session_id)
{
    uint32 *socket_index = account_socket_table_find(&_sockets.account_table,
        account_id);
    if (!socket_index)
        return;
    proxy_socket_t *ps = &_sockets.pool.all[*socket_index];
    if (ps->login_session_id != login_session_id)
    {
        LOG_EXT("Warning: different login session IDs!");
        return;
    }
    if (ps->client)
        cl_disconnect(ps->client);
    else
        _close_proxy_socket(ps);

}

void
proxy_delete_socket(uint32 socket_index)
    {_close_proxy_socket(&_sockets.pool.all[socket_index]);}

bbuf_t
proxy_forward_msg(uint32 socket_index, int size)
{
    muta_assert(socket_index < _sockets.pool.max);
    proxy_socket_t  *ps     = &_sockets.pool.all[socket_index];
    proxy_t         *proxy  = &_proxies.all[ps->proxy];
    muta_assert(proxy->socket != KSYS_INVALID_SOCKET);
    bbuf_t bb = _send_msg_to_proxy(proxy,
        TPROXY_MSG_PLAYER_PACKET_COMPUTE_SZ(size));
    if (!bb.max_bytes)
        return bb;
    uint8 *mem = bb.mem;
    WRITE_PROXY_MSG_TYPE(mem, TPROXY_MSG_PLAYER_PACKET);
    WRITE_UINT16(mem, size);
    WRITE_UINT32(mem, ps->id);
    bb.max_bytes -= (int)(mem - bb.mem);
    bb.mem          = mem;
    bb.num_bytes    = 0;
    muta_assert(bb.max_bytes == size);
    return bb;
}

uint64
proxy_socket_get_account_id(uint32 socket_index)
{
    muta_assert(socket_index < _sockets.pool.max);
    return _sockets.pool.all[socket_index].account_id;
}

void *
proxy_get_client_by_account_id(uint64 account_id)
{
    uint32 *proxy_socket_index = account_socket_table_find(
        &_sockets.account_table, account_id);
    if (!proxy_socket_index)
        return 0;
    return _sockets.pool.all[*proxy_socket_index].client;
}

void
proxy_read(com_event_read_proxy_t *event)
{
    proxy_t *proxy = &_proxies.all[event->proxy_index];
    if (proxy->socket == KSYS_INVALID_SOCKET ||
        proxy->id != event->proxy_id)
    {
        if (event->num_bytes > 0)
            goto free_memory;
        return;
    }
    if (proxy->flags & PROXY_FLAG_AUTHED)
        _handle_read_proxy(event);
    else
        _handle_read_unauthed_proxy(event);
    /* FIXME: handle the case where a proxy becomes authed, but still has
     * data in the event to read. */
    if (event->num_bytes <= 0)
        return;
    free_memory:
        mutex_lock(&_segpool_mutex);
        segpool_free(&_segpool, event->memory);
        mutex_unlock(&_segpool_mutex);
}

void
proxy_accept(com_event_accept_proxy_t *event)
{
    proxy_t *proxy = fixed_pool_new(&_proxies);
    if (!proxy)
    {
        LOG("No slot found for new proxy! Closing...");
        goto fail;
    }
    if (net_disable_nagle(event->socket))
    {
        LOG("Failed to disable Nagle on an incoming socket.");
        goto fail;
    }
    if (make_socket_non_block(event->socket))
    {
        LOG("Failed to make socket non-block.");
        goto fail;
    }
    proxy->socket               = event->socket;
    proxy->in_buf.num_bytes     = 0;
    proxy->out_buf.num_bytes    = 0;
    proxy->flags                = 0;
    proxy->id                   = _running_proxy_id++;
    cryptchan_clear(&proxy->cryptchan);
    uint32 data[2] = {fixed_pool_index(&_proxies, proxy), proxy->id};
    netpoll_event_t e;
    e.events = NETPOLL_READ | NETPOLL_ET;
    memcpy(&e.data.u64, data, 8);
    netpoll_add(&_netpoll, event->socket, &e);
    LOG_DEBUG("Accepted new proxy.");
    return;
    fail:
        close_socket(event->socket);

}

void
proxy_disconnect(com_event_disconnect_proxy_t *event)
{
    proxy_t *proxy = &_proxies.all[event->proxy_index];
    muta_assert(proxy->socket != KSYS_INVALID_SOCKET);
    proxy->socket = KSYS_INVALID_SOCKET;
    netpoll_del(&_netpoll, proxy->socket);
    net_shutdown_sock(proxy->socket, SOCKSD_BOTH);
    fixed_pool_free(&_proxies, proxy);
    if (!(proxy->flags & PROXY_FLAG_AUTHED))
        return;
    /* If authed, remove from the array of authed proxies */
    bool32 removed_from_authed = 0;
    for (uint32 i = 0; i < _num_authed_proxies; ++i)
    {
        if (_authed_proxies[i] != proxy)
            continue;
        _authed_proxies[i]  = _authed_proxies[--_num_authed_proxies];
        removed_from_authed = 1;
        break;
    }
    muta_assert(removed_from_authed);
    /*-- Disconnect players currently connected through this proxy --*/
    uint32          proxy_index = fixed_pool_index(&_proxies, proxy);
    uint32          num_used_sockets = _sockets.num_used;
    proxy_socket_t  *ps;
    for (uint32 i = 0; i < num_used_sockets; ++i)
    {
        ps = &_sockets.pool.all[_sockets.used[i]];
        if (ps->proxy != proxy_index)
            continue;
        if (ps->client)
            cl_disconnect(ps->client);
        else
            _free_proxy_socket_resources(ps);
    }
    LOG("Disconnected a proxy.");
}

static void
_check_netpoll_events(netpoll_event_t *events, int num_events)
{
    uint8 buf[MUTA_MTU];
    for (int i = 0; i < num_events; ++i)
    {
        if (!(events[i].events & (NETPOLL_READ | NETPOLL_HUP)))
            continue;
        if (events[i].data.ptr == (void*)&_listen_socket)
        {
            addr_t      a;
            socket_t    s = net_accept(_listen_socket, &a);
            if (s == KSYS_INVALID_SOCKET)
                continue;
            com_event_t new_event;
            new_event.type                  = COM_EVENT_ACCEPT_PROXY;
            new_event.accept_proxy.socket   = s;
            com_push_events(&new_event, 1);
        } else /* It's a proxy's socket */
        {
            uint32 data[2];
            memcpy(data, &events[i].data.u64, 8);
            proxy_t *proxy      = &_proxies.all[data[0]];
            int     num_bytes   = net_recv(proxy->socket, buf, sizeof(buf));
            com_event_t new_event;
            new_event.type                      = COM_EVENT_READ_PROXY;
            new_event.read_proxy.num_bytes      = num_bytes;
            new_event.read_proxy.proxy_index    = data[0];
            new_event.read_proxy.proxy_id       = data[1];
            if (num_bytes > 0)
            {
                mutex_lock(&_segpool_mutex);
                new_event.read_proxy.memory = segpool_malloc(&_segpool,
                    num_bytes);
                mutex_unlock(&_segpool_mutex);
                memcpy(new_event.read_proxy.memory, buf, num_bytes);
            } else
                netpoll_del(&_netpoll, proxy->socket);
            com_push_events(&new_event, 1);
        }
    }
}

static thread_ret_t
_main(void *args)
{
    LOG("Beginning to listen for proxies.");
    int             num_events;
    netpoll_event_t events[MAX_PROXIES];
    while (_running)
    {
        num_events = netpoll_wait(&_netpoll, events, MAX_PROXIES, 33);
        _check_netpoll_events(events, num_events);
    }
    return 0;
}

static void
_handle_read_proxy(com_event_read_proxy_t *event)
{
    proxy_t *proxy = &_proxies.all[event->proxy_index];
    if (event->num_bytes <= 0)
    {
        LOG("Proxy closed connection.");
        _disconnect_proxy(proxy, 0);
        return;
    }
    int num_bytes   = event->num_bytes;
    int num_read    = 0;
    while (num_read < num_bytes)
    {
        int max_read    = PROXY_IN_BUF_SZ - proxy->in_buf.num_bytes;
        int num_to_read = MIN(max_read, num_bytes - num_read);
        memcpy(proxy->in_buf.memory + proxy->in_buf.num_bytes,
            (uint8*)event->memory + num_read, num_to_read);
        proxy->in_buf.num_bytes += num_to_read;
        num_read += num_to_read;
        if (!_read_proxy_packet(proxy))
            continue;
        _disconnect_proxy(proxy, 1);
        break;
    }
    mutex_lock(&_segpool_mutex);
    segpool_free(&_segpool, event->memory);
    mutex_unlock(&_segpool_mutex);
}

static void
_handle_read_unauthed_proxy(com_event_read_proxy_t *event)
{
    proxy_t *proxy = &_proxies.all[event->proxy_index];
    if (event->num_bytes <= 0)
    {
        LOG("Proxy closed connection.");
        _disconnect_proxy(proxy, 0);
        return;
    }
    int num_bytes   = event->num_bytes;
    int num_read    = 0;
    int authed      = 0;
    while (num_read < num_bytes)
    {
        int max_read    = PROXY_IN_BUF_SZ - proxy->in_buf.num_bytes;
        int num_to_read = MIN(max_read, num_bytes - num_read);
        memcpy(proxy->in_buf.memory + proxy->in_buf.num_bytes,
            (uint8*)event->memory + num_read, num_to_read);
        proxy->in_buf.num_bytes += num_to_read;
        num_read += num_to_read;
        int r;
        if (!authed)
        {
            r = _read_unauthed_proxy_packet(proxy);
            if (!r)
                continue;
            if (r < 0)
            {
                _disconnect_proxy(proxy, 1);
                break;
            }
            authed = 1;
        } else if (_read_proxy_packet(proxy))
        {
            _disconnect_proxy(proxy, 1);
            break;
        }
    }
    mutex_lock(&_segpool_mutex);
    segpool_free(&_segpool, event->memory);
    mutex_unlock(&_segpool_mutex);
}

static void
_disconnect_proxy(proxy_t *proxy, bool32 del_from_netpoll)
{
    if (proxy->flags & PROXY_FLAG_WILL_DISCONNECT)
    {
        LOG_EXT("Already disconnected!");
        return;
    }
    proxy->flags |= PROXY_FLAG_WILL_DISCONNECT;
    com_event_t event;
    event.type = COM_EVENT_DISCONNECT_PROXY;
    event.disconnect_proxy.proxy_index = PROXY_INDEX(proxy);
    com_push_events_no_wait(&event, 1);
}

static int
_auth_proxy(proxy_t *proxy)
{
    muta_assert(!(proxy->flags & PROXY_FLAG_AUTHED));
    uint8 buf[PROXY_MSGTSZ + CRYPT_MSG_ADDITIONAL_BYTES +
        TPROXY_MSG_LOGIN_RESULT_SZ];
    bbuf_t                      bb = BBUF_INITIALIZER(buf, sizeof(buf));
    tproxy_msg_login_result_t   r_msg;
    r_msg.result            = 0;
    r_msg.shard_name_len    = (uint8)strlen(com_config.shard_name);
    memcpy(r_msg.shard_name, com_config.shard_name, r_msg.shard_name_len);
    int r = tproxy_msg_login_result_write_const_encrypted(&bb,
        &proxy->cryptchan, &r_msg);
    muta_assert(cryptchan_is_encrypted(&proxy->cryptchan));
    muta_assert(!r);
    if (net_send_all(proxy->socket, bb.mem, bb.num_bytes) <= 0)
    {
        LOG("Failed to send auth message to proxy.");
        return 2;
    }
    _authed_proxies[_num_authed_proxies++] = proxy;
    proxy->flags |= PROXY_FLAG_AUTHED;
    LOG("Authed proxy!");
    return 0;
}

static int
_read_proxy_packet(proxy_t *proxy)
{
    bbuf_t bb = BBUF_INITIALIZER(proxy->in_buf.memory, proxy->in_buf.num_bytes);
    int                 incomplete  = 0;
    int                 dc          = 0;
    proxy_msg_type_t    msg_type;
    while (BBUF_FREE_SPACE(&bb) >= PROXY_MSGTSZ && !incomplete && !dc)
    {
        BBUF_READ(&bb, &msg_type);
        switch (msg_type)
        {
        case FPROXY_MSG_OPENED_SOCKET_RESULT:
        {
            LOG_DEBUG("FPROXY_MSG_OPENED_SOCKET_RESULT");
            fproxy_msg_opened_socket_result_t s;
            incomplete = fproxy_msg_opened_socket_result_read_const_encrypted(
                &bb, &proxy->cryptchan, &s);
            if (!incomplete)
                dc = _handle_fproxy_msg_opened_socket_result(proxy, &s);
        }
            break;
        case FPROXY_MSG_PLAYER_CRYPTCHAN:
        {
            LOG_DEBUG("FPROXY_MSG_PLAYER_CRYPTCHAN");
            fproxy_msg_player_cryptchan_t s;
            incomplete = fproxy_msg_player_cryptchan_read_const_encrypted(&bb,
                &proxy->cryptchan, &s);
            if (!incomplete)
                dc = _handle_fproxy_msg_player_cryptchan(proxy, &s);
        }
            break;
        case FPROXY_MSG_PLAYER_PACKET:
        {
            fproxy_msg_player_packet_t s;
            incomplete = fproxy_msg_player_packet_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fproxy_msg_player_packet(proxy, &s);
        }
            break;
        case FPROXY_MSG_CLOSE_SOCKET:
        {
            LOG_DEBUG("FPROXY_MSG_CLOSE_SOCKET");
            fproxy_msg_close_socket_t s;
            incomplete = fproxy_msg_close_socket_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fproxy_msg_close_socket(proxy, &s);
        }
            break;
        default:
            LOG_DEBUG_EXT("Unknown message type %d.", msg_type);
            dc = 1;
            break;
        }
    }
    if (dc || incomplete < 0)
    {
        LOG_DEBUG_EXT("msg_type %u, dc %d, incomplete %d", msg_type, dc,
            incomplete);
        return 1;
    }
    uint32 free_space = BBUF_FREE_SPACE(&bb);
    void *src = proxy->in_buf.memory + proxy->in_buf.num_bytes - free_space;
    memmove(proxy->in_buf.memory, src, free_space);
    proxy->in_buf.num_bytes = free_space;
    return 0;
}

static int
_read_unauthed_proxy_packet(proxy_t *proxy)
{
    bbuf_t bb = BBUF_INITIALIZER(proxy->in_buf.memory, proxy->in_buf.num_bytes);
    int                 incomplete  = 0;
    int                 dc          = 0;
    bool32              authed      = 0;
    proxy_msg_type_t    msg_type;
    while (BBUF_FREE_SPACE(&bb) >= PROXY_MSGTSZ && !incomplete && !dc &&
        !authed)
    {
        BBUF_READ(&bb, &msg_type);
        switch (msg_type)
        {
        case PROXY_MSG_PUB_KEY:
        {
            LOG_DEBUG_EXT("PROXY_MSG_PUB_KEY");
            if (cryptchan_is_encrypted(&proxy->cryptchan))
            {
                LOG_DEBUG_EXT("cryptchan is already encrypted!");
                dc = 1;
                break;
            }
            proxy_msg_pub_key_t s;
            incomplete = proxy_msg_pub_key_read(&bb, &s);
            if (!incomplete)
                dc = _handle_proxy_msg_pub_key(proxy, &s);
        }
            break;
        case PROXY_MSG_STREAM_HEADER:
        {
            LOG_DEBUG_EXT("PROXY_MSG_STREAM_HEADER");
            proxy_msg_stream_header_t s;
            incomplete = proxy_msg_stream_header_read(&bb, &s);
            if (!incomplete)
                dc = _handle_proxy_msg_stream_header(proxy, &s);
        }
            break;
        case FPROXY_MSG_LOGIN:
        {
            LOG_DEBUG_EXT("PROXY_MSG_LOGIN");
            fproxy_msg_login_t s;
            incomplete = fproxy_msg_login_read_var_encrypted(&bb,
                &proxy->cryptchan, &s);
            if (!incomplete)
            {
                dc = _handle_proxy_msg_login(proxy, &s);
                if (!dc)
                    authed = 1;
            }
        }
            break;
        default:
            LOG_DEBUG_EXT("Unknown message type %d.", msg_type);
            dc = 1;
            break;
        }
    }
    if (dc || incomplete < 0)
    {
        LOG_DEBUG_EXT("msg_type %u, dc %d, incomplete %d", msg_type, dc,
            incomplete);
        return -1;
    }
    uint32 free_space = BBUF_FREE_SPACE(&bb);
    void *src = proxy->in_buf.memory + proxy->in_buf.num_bytes - free_space;
    memmove(proxy->in_buf.memory, src, free_space);
    proxy->in_buf.num_bytes = free_space;
    return authed ? 1 : 0;
}

static int
_handle_proxy_msg_pub_key(proxy_t *proxy, proxy_msg_pub_key_t *s)
{
    proxy_msg_stream_header_t   out_header;
    proxy_msg_pub_key_t         out_key;
    if (cryptchan_is_initialized(&proxy->cryptchan))
        return 1;
    if (cryptchan_init(&proxy->cryptchan, out_key.key))
        return 2;
    if (cryptchan_sv_store_pub_key(&proxy->cryptchan, s->key,
        out_header.header))
        return 3;
    uint8   out_buf[MUTA_MTU];
    bbuf_t  bb = BBUF_INITIALIZER(out_buf, sizeof(out_buf));
    /* Send public key and header forward */
    proxy_msg_pub_key_write(&bb, &out_key);
    proxy_msg_stream_header_write(&bb, &out_header);
    if (net_send_all(proxy->socket, bb.mem, bb.num_bytes) < 0)
        return 3;
    LOG_DEBUG("Sent public key and stream header to proxy.");
    return 0;
}

static int
_handle_proxy_msg_stream_header(proxy_t *proxy, proxy_msg_stream_header_t *s)
{
    if (cryptchan_store_stream_header(&proxy->cryptchan, s->header))
        return 1;
    muta_assert(cryptchan_is_encrypted(&proxy->cryptchan));
    LOG_DEBUG("Connection with proxy is now encrypted.");
    return 0;
}

static int
_handle_proxy_msg_login(proxy_t *proxy, fproxy_msg_login_t *s)
{
    int len = (int)strlen(_password);
    if (s->password_len != len)
        return 1;
    if (memcmp(s->password, _password, len))
        return 2;
    if (_auth_proxy(proxy))
        return 3;
    return 0;
}

static int
_handle_fproxy_msg_opened_socket_result(proxy_t *proxy,
    fproxy_msg_opened_socket_result_t *s)
{
    uint32 *socket_index = id_socket_table_find(&_sockets.id_table,
        s->socket_id);
    if (!socket_index)
    {
        LOG_EXT("Received invalid socket id!");
        return 1;
    }
    proxy_socket_t *ps = &_sockets.pool.all[*socket_index];
    if (ps->flags & PROXY_SOCKET_FLAG_CONFIRMED_BY_PROXY)
    {
        LOG_EXT("Socket was already confirmed by a proxy!");
        return 2;
    }
    if (ps->proxy != fixed_pool_index(&_proxies, proxy))
    {
        LOG_EXT("Socket is assigned to a different proxy!");
        return 3;
    }
    uint64 account_id       = ps->account_id;
    uint32 login_session_id = ps->login_session_id;
    if (s->result)
    {
        LOG("Proxy rejected socket creation.");
        _free_proxy_socket_resources(ps);
    } else
        ps->flags |= PROXY_SOCKET_FLAG_CONFIRMED_BY_PROXY;
    if (!ls_send_player_select_shard_result(account_id, login_session_id,
        s->result))
    {
        LOG("Informed login server of a client being accepted through a "
            "proxy.");
        return 0;
    }
    if (!s->result)
        _close_proxy_socket(ps);
    LOG("Failed to inform login server of a client being accepted through a "
        "proxy.");
    return 0;
}

static int
_handle_fproxy_msg_player_cryptchan(proxy_t *proxy,
    fproxy_msg_player_cryptchan_t *s)
{
    proxy_socket_t *ps = _get_socket_by_id(proxy, s->socket_id);
    if (!ps)
    {
        LOG_DEBUG("Warning: socket not found (handle this!)");
        return 1;
    }
    if (ps->client)
    {
        LOG_EXT("Client wasn't authed yet!");
        return 2;
    }
    client_t *client = cl_new(fixed_pool_index(&_sockets.pool, ps), s->wx,
        s->rx, s->sk, s->pk, s->read_key, s->read_nonce, s->write_key,
        s->write_nonce);
    if (!client)
        return 3;
    ps->client = client;
    return 0;
}

static int
_handle_fproxy_msg_player_packet(proxy_t *proxy, fproxy_msg_player_packet_t *s)
{
    proxy_socket_t *ps = _get_socket_by_id(proxy, s->socket_id);
    if (!ps)
    {
        LOG_EXT("Socket not found!");
        return 0;
    }
    if (!(ps->flags & PROXY_SOCKET_FLAG_CONFIRMED_BY_PROXY))
        return 1;
    if (!ps->client)
    {
        LOG_DEBUG_EXT("Socket has no client assigned!");
        return 2;
    }
    cl_read(ps->client, s->data, s->data_len);
    return 0;
}

static int
_handle_fproxy_msg_close_socket(proxy_t *proxy, fproxy_msg_close_socket_t *s)
{
    proxy_socket_t *ps = _get_socket_by_id(proxy, s->socket_id);
    if (!ps)
    {
        LOG_EXT("Socket %u not found.", s->socket_id);
        return 0;
    }
    if (ps->client)
        cl_disconnect(ps->client);
    else
        _close_proxy_socket(ps);
    return 0;
}

static bbuf_t
_send_msg_to_proxy(proxy_t *proxy, int size)
{
    muta_assert(proxy->socket != KSYS_INVALID_SOCKET);
    bbuf_t ret = {0};
    int req_size = PROXY_MSGTSZ + size;
    muta_assert(req_size < PROXY_OUT_BUF_SZ);
    if (req_size > PROXY_OUT_BUF_SZ - proxy->out_buf.num_bytes &&
        _flush_proxy(proxy))
        return ret;
    BBUF_INIT(&ret, proxy->out_buf.memory + proxy->out_buf.num_bytes, req_size);
    proxy->out_buf.num_bytes += req_size;
    return ret;
}

static inline bbuf_t
_send_const_crypt_msg_to_proxy(proxy_t *proxy, int size)
    {return _send_msg_to_proxy(proxy, size + CRYPT_MSG_ADDITIONAL_BYTES);}

static inline bbuf_t
_send_var_crypt_msg_to_proxy(proxy_t *proxy, int size)
{
    return _send_msg_to_proxy(proxy, size + CRYPT_MSG_ADDITIONAL_BYTES +
        sizeof(msg_sz_t));
}

static int
_flush_proxy(proxy_t *proxy)
{
    int offset      = 0;
    int num_left    = proxy->out_buf.num_bytes;
    while (num_left)
    {
        int num_to_send = MIN(num_left, MUTA_MTU);
        if (net_send_all(proxy->socket, proxy->out_buf.memory + offset,
            num_to_send) <= 0)
            return 1;
        offset += num_to_send;
        num_left -= num_to_send;
    }
    proxy->out_buf.num_bytes = num_left;
    return 0;
}

static void
_free_proxy_socket_resources(proxy_socket_t *socket)
{
    muta_assert(socket->flags & PROXY_SOCKET_FLAG_IN_USE);
    _sockets.used[socket->used_index] = _sockets.used[--_sockets.num_used];
    account_socket_table_erase(&_sockets.account_table, socket->account_id);
    id_socket_table_erase(&_sockets.id_table, socket->id);
    fixed_pool_free(&_sockets.pool, socket);
    socket->flags &= ~PROXY_SOCKET_FLAG_IN_USE;
}

static void
_close_proxy_socket(proxy_socket_t *socket)
{
    LOG("Closing proxy socket.");
    proxy_t *proxy      = &_proxies.all[socket->proxy];
    uint32  socket_id   = socket->id;
    _free_proxy_socket_resources(socket);
    if (proxy->socket == KSYS_INVALID_SOCKET)
        return;
    bbuf_t bb = _send_const_crypt_msg_to_proxy(proxy,
        TPROXY_MSG_CLOSED_SOCKET_SZ);
    if (!bb.max_bytes)
        _disconnect_proxy(proxy, 1);
    tproxy_msg_closed_socket_t d_msg;
    d_msg.socket_id = socket_id;
    tproxy_msg_closed_socket_write_const_encrypted(&bb, &proxy->cryptchan,
        &d_msg);
}

static proxy_socket_t *
_get_socket_by_id(proxy_t *proxy, uint32 id)
{
    uint32 *socket_index = id_socket_table_find(&_sockets.id_table, id);
    if (!socket_index)
        return 0;
    proxy_socket_t *ps = &_sockets.pool.all[*socket_index];
    if (ps->proxy != fixed_pool_index(&_proxies, proxy))
        return 0;
    if (!(ps->flags & PROXY_SOCKET_FLAG_CONFIRMED_BY_PROXY))
        return 0;
    return ps;
}
