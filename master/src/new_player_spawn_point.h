#ifndef MUTA_MASTER_NEW_PLAYER_SPAWN_POINT_H
#define MUTA_MASTER_NEW_PLAYER_SPAWN_POINT_H

#include "../../shared/types.h"

typedef struct new_player_spawn_point_t new_player_spawn_point_t;

struct new_player_spawn_point_t
{
    uint32  id;
    uint32  shared_instance_id;
    int     position[3];
};

int
new_player_spawn_points_init(void);

void
new_player_spawn_points_destroy(void);

new_player_spawn_point_t *
new_player_spawn_points_find(uint32 spawn_point_id);

#endif /* MUTA_MASTER_NEW_PLAYER_SPAWN_POINT_H */
