#include "interest_list.h"
#include "player.h"
#include "common.h"
#include "../../shared/common_utils.h"

bool32
pil_should_cull(int position_a[3], int position_b[3])
{
    int max_distance_h = com_config.view_distance_h;
    int max_distance_v = com_config.view_distance_v;
    if (abs(position_a[0] - position_b[0]) > max_distance_h)
        return 1;
    if (abs(position_a[1] - position_b[1]) > max_distance_h)
        return 1;
    if (abs(position_a[2] - position_b[2]) > max_distance_v)
        return 1;
    return 0;
}

void
pil_follow_player(player_t *player, player_t *other)
{
#ifdef _MUTA_DEBUG
    {
        uint32 index = pl_get_index(other);
        for (uint32 i = 0; i < darr_num(player->interest_list.players); ++i)
            muta_assert(index != player->interest_list.players[i].player_index);
    }
#endif
    /* Add to players */
    pil_player_t pil_player;
    pil_player.player_index = pl_get_index(other);
    pil_player.index_in_player_followers = darr_num(
        other->interest_list.player_followers);
    darr_push(player->interest_list.players, pil_player);
    /* Add to player_followers */
    pil_player_follower_t follower;
    follower.player_index       = pl_get_index(player);
    follower.index_in_players   = darr_num(player->interest_list.players) - 1;
    darr_push(other->interest_list.player_followers, follower);
}

void
pil_stop_following_player(player_t *player, uint32 index_in_players)
{
    muta_assert(index_in_players < darr_num(player->interest_list.players));
    pil_player_t tmp = player->interest_list.players[index_in_players];
    /* Remove from players */
    {
        uint32 last = --_darr_head(player->interest_list.players)->num;
        if (last)
        {
            player->interest_list.players[index_in_players] = player->interest_list.players[last];
            player_t *third = pl_get(player->interest_list.players[index_in_players].player_index);
            uint32 index_in_player_followers = player->interest_list.players[index_in_players].index_in_player_followers;
            third->interest_list.player_followers[index_in_player_followers].index_in_players = index_in_players;
            DEBUG_PRINTFF("Swapped %u for %u, num now: %u\n", index_in_players,
                last, darr_num(player->interest_list.players));
        }
    }
    /* Remove from player_followers */
    {
        player_t *other = pl_get(tmp.player_index);
        uint32 last = --_darr_head(other->interest_list.player_followers)->num;
        if (last)
        {
            other->interest_list.player_followers[tmp.index_in_player_followers]
                = other->interest_list.player_followers[last];
            player_t *third = pl_get(other->interest_list.player_followers[
                tmp.index_in_player_followers].player_index);
            uint32 index_in_players = other->interest_list.player_followers[
                tmp.index_in_player_followers].index_in_players;
            third->interest_list.players[
                index_in_players].index_in_player_followers =
                tmp.index_in_player_followers;
        }
    }
}

void
pil_stop_being_followed(player_t *player, uint32 index_in_player_followers)
{
    pil_player_follower_t pil_follower =
        player->interest_list.player_followers[index_in_player_followers];
    player_t *other = pl_get(pil_follower.player_index);
    pil_stop_following_player(other, pil_follower.index_in_players);
}
