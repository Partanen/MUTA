/* sim_server.h
 * Connection handling to simulation servers (worldds). */

#ifndef MUTA_SERVER_SIM_SERVER_H
#define MUTA_SERVER_SIM_SERVER_H

#include "../../shared/common_utils.h"

#define SIM_INVALID_INDEX 0xFFFFFFFF

/* Forward declaration(s) */
typedef struct cryptchan_t cryptchan_t;

int
sim_init(void);

void
sim_destroy(void);

int
sim_start(void);

void
sim_stop(void);

bbuf_t
sim_send(uint32 sim_server_index, uint32 num_bytes);

bbuf_t
sim_send_const_encrypted(uint32 sim_server_index, uint32 num_bytes);

bbuf_t
sim_send_var_encrypted(uint32 sim_server_index, uint32 num_bytes);

cryptchan_t *
sim_get_cryptchan(uint32 sim_server_index);

void
sim_flush(void);

#endif /* MUTA_SERVER_SIM_SERVER_H */
