/* interest_list.h
 * Interest lists are used to track what objects a player receives updates
 * from, and what other players are tracking the player. */

#ifndef MUTA_SERVER_INTEREST_LIST_H
#define MUTA_SERVER_INTEREST_LIST_H

#include "../../shared/types.h"

#define pil_num_followers(player) \
    darr_num((player)->interest_list.player_followers)

#define pil_get_follower(player, index) \
    pl_get((player)->interest_list.player_followers[(index)].player_index)

#define pil_num_players(player) \
    darr_num((player)->interest_list.players)

#define pil_get_player(player, index) \
    pl_get((player)->interest_list.players[(index)].player_index)

/* Forward declaration(s) */
typedef struct player_t player_t;

/* Types defined here */
typedef struct pil_player_t             pil_player_t;
typedef struct pil_player_follower_t    pil_player_follower_t;
typedef struct pl_interest_list_t       pl_interest_list_t;

struct pil_player_t
{
    uint32 player_index;
    uint32 index_in_player_followers; /* Index in other player's player_followers list */
};

struct pil_player_follower_t
{
    uint32 player_index;
    uint32 index_in_players; /* Index in other player's players list */
};

struct pl_interest_list_t
{
    pil_player_follower_t   *player_followers; /* Players who follow us, darr */
    pil_player_t            *players; /* Plays we're following, darr */
};

bool32
pil_should_cull(int position_a[3], int position_b[3]);

void
pil_follow_player(player_t *player, player_t *other);

void
pil_stop_following_player(player_t *player, uint32 index_in_players);

void
pil_stop_being_followed(player_t *player, uint32 index_in_player_followers);

#endif /* MUTA_SERVER_INTEREST_LIST_H */
