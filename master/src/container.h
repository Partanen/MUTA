#ifndef MUTA_MASTER_CONTAINER_H
#define MUTA_MASTER_CONTAINER_H

#include "../../shared/common_defs.h"

typedef struct container_item_t container_item_t;
typedef struct container_t      container_t;

struct container_item_t
{
    uint32  dobj_index;
    uint8   x;
    uint8   y;
    uint8   w;
    uint8   h;
};

struct container_t
{
    container_item_t    items[MAX_BAG_SIZE];
    uint32              num_items;
    uint8               w;
    uint8               h;
};

void
container_init(container_t *container, uint8 w, uint8 h);

bool32
container_can_fit(container_t *container, uint32 x, uint32 y, uint32 obj_w,
    uint32 obj_h);

bool32
container_can_move(container_t *container, uint32 dobj_index, uint32 x,
    uint32 y, uint32 obj_w, uint32 obj_h);


void
container_add_item(container_t *container, container_item_t item);
/* Assumes can_fit() has been used to check for fitting. */

void
container_remove_item(container_t *container, uint32 index);

container_item_t *
container_find_item(container_t *container, uint32 dobj_index);

#endif /* MUTA_MASTER_CONTAINER_H */
