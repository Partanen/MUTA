#ifndef MUTA_MASTER_DATA_H
#define MUTA_MASTER_DATA_H

#include "../../shared/types.h"

typedef struct data_config_t data_config_t;

struct data_config_t
{
    dobj_type_id_t player_starter_bag_dobj_type_id;
};

extern data_config_t data_config;

int
data_init(void);

void
data_destroy(void);

#endif /* MUTA_MASTER_DATA_H */
