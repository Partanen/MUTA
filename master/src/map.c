#include "map.h"
#include "common.h"
#include "../../shared/muta_map_format.h"
#include "../../shared/common_utils.h"

typedef map_data_t                  map_data_darr_t;

static muta_map_db_t    _map_db;
static const char       *_map_db_path       = "data/common/maps.def";
static map_data_darr_t  *_map_datas;

int
map_init(void)
{
    if (muta_map_db_load(&_map_db, _map_db_path))
    {
        LOG("Failed to read maps from %s.\n", _map_db_path);
        return 1;
    }
    int     ret             = 0;
    uint32  num_entries     = muta_map_db_num_entries(&_map_db);
    dchar   *spawn_db_path  = dstr_create_empty(128);;
    for (uint32 i = 0; i < num_entries; ++i)
    {
        muta_map_db_entry_t *entry = &_map_db.entries[i];
        map_data_t map_data =
        {
            .id     = entry->id,
            .name   = entry->name
        };
        muta_map_file_t map_file;
        if (muta_map_file_load(&map_file, entry->file_path))
        {
            LOG_ERROR("Error loading map file %s from path %s.",
                map_data.name, entry->file_path);
            ret = 1;
            break;
        }
        map_data.w_in_tiles = muta_map_file_tw(&map_file);
        map_data.h_in_tiles = muta_map_file_th(&map_file);
        muta_map_file_destroy(&map_file);
        dstr_set(&spawn_db_path, entry->directory_path);
        dstr_append(&spawn_db_path, "/spawns.mdb");
        if (spawn_db_open(&map_data.spawn_db, spawn_db_path))
        {
            LOG_ERROR("Error loading spawn db from path %s.", spawn_db_path);
            ret = 1;
            break;
        }
        darr_push(_map_datas, map_data);
    }
    dstr_free(&spawn_db_path);
    if (ret)
        map_destroy();
    return ret;
}

void
map_destroy(void)
{
    muta_map_db_destroy(&_map_db);
    darr_free(_map_datas);
}

map_data_t *
map_find(uint32 map_id)
{
    uint32 num_map_datas = darr_num(_map_datas);
    for (uint32 i = 0; i < num_map_datas; ++i)
        if (map_id == _map_datas[i].id)
            return &_map_datas[i];
    return 0;
}
