#ifndef MUTA_SERVER_INTEREST_AREA_H
#define MUTA_SERVER_INTEREST_AREA_H

#include "../../shared/types.h"

#define IA_W 22
#define IA_T 8

#define IA_FOR_EACH_PLAYER(interest_area, ret_player) \
    for ((ret_player) = interest_area->players == 0xFFFFFFFF ? 0 : \
            pl_get(interest_area->players); \
        (ret_player); \
        (ret_player) = (ret_player)->interest_area_next == 0xFFFFFFFF ? 0 : \
            pl_get((ret_player)->interest_area_next))

#define IA_FOR_EACH_DYNAMIC_OBJECT(interest_area, ret_dynamic_object) \
    for ((ret_dynamic_object) = interest_area->dynamic_objects == 0xFFFFFFFF ? 0 : \
            dobj_get(interest_area->dynamic_objects); \
        (ret_dynamic_object); \
        (ret_dynamic_object) = (ret_dynamic_object)->in.world.interest_area_next == 0xFFFFFFFF ? 0 : \
            dobj_get((ret_dynamic_object)->in.world.interest_area_next))

#define IA_FOR_EACH_CREATURE(interest_area, ret_creature) \
    for ((ret_creature) = interest_area->creatures == 0xFFFFFFFF ? 0 : \
            creature_get(interest_area->creatures); \
        (ret_creature); \
        (ret_creature) = (ret_creature)->interest_area_next == 0xFFFFFFFF ? 0 : \
            creature_get((ret_creature)->interest_area_next))

/* Forward declaration(s) */
typedef struct player_t         player_t;
typedef struct dynamic_object_t dynamic_object_t;
typedef struct creature_t       creature_t;

/* Types defined here */
typedef struct interest_area_t interest_area_t;

struct interest_area_t
{
    uint32 players;
    uint32 dynamic_objects;
    uint32 creatures;
};

interest_area_t *
ia_init_instance(uint32 w_in_tiles, uint32 h_in_tiles);

interest_area_t *
ia_find(interest_area_t *interest_areas, uint32 w_in_tiles, uint32 h_in_tiles,
    int x, int y, int z);
/* ia_find()
 * Find the correct interest area for the given position from the array passed
 * as an argument. Return value is the index of the interest area. */

void
ia_add_player(interest_area_t *interest_areas, uint32 interest_area_index,
    player_t *player);

void
ia_add_dynamic_object(interest_area_t *interest_areas,
    uint32 interest_area_index, dynamic_object_t *obj);

void
ia_add_creature(interest_area_t *interest_areas, uint32 interest_area_index,
    creature_t *creature);

void
ia_del_player(player_t *player);

void
ia_del_dynamic_object(dynamic_object_t *obj);

void
ia_del_creature(creature_t *creature);

#endif /* MUTA_SERVER_INTEREST_AREA_H */
