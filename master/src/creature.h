#ifndef MUTA_MASTER_CREATURE_H
#define MUTA_MASTER_CREATURE_H

#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct svmsg_add_creature_t svmsg_add_creature_t;

/* Types defined here */
typedef struct creature_t creature_t;


enum creature_flag
{
    CREATURE_FLAG_SPAWN_CONFIRMED   = (1 << 0),
    CREATURE_FLAG_DEAD              = (1 << 1)
};

struct creature_t
{
    creature_runtime_id_t   runtime_id;
    creature_type_id_t      type_id;
    uint32                  instance_index;
    uint32                  sim_index;
    int                     position[3];
    uint32                  interest_area_index;
    uint32                  interest_area_prev;
    uint32                  interest_area_next;
    struct
    {
        uint32 current;
        uint32 max;
    } health;
    uint8                   direction;
    uint8                   sex;
    uint8                   flags;
};

int
creature_init(void);

void
creature_destroy(void);

uint32
creature_get_index(creature_t *creature);

creature_t *
creature_get(uint32 index);

creature_t *
creature_find(creature_runtime_id_t id);

creature_t *
creature_request_spawn(creature_type_id_t type_id, uint32 instance_id,
    int position[3], int direction, int sex, uint32 health_current,
    uint32 health_max);

int
creature_confirm_spawn(creature_t *creature, uint32 instance_part_id,
    int x, int y, int z, int direction);
/* Returns non-zero if the sim server confirmed an already spawned object. */

int
creature_spawn_fail(creature_t *creature);
/* Returns non-zero if the sim server did something illegal. */

void
creature_on_sim_disconnected(uint32 sim_index);

void
creature_serialize_to_msg(creature_t *creature, svmsg_add_creature_t *ret_s);

void
creature_die(creature_t *creature);

#endif /* MUTA_MASTER_CREATURE_H */
