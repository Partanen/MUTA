#include "new_player_spawn_point.h"
#include "common.h"
#include "instance.h"
#include "map.h"
#include "../../shared/hashtable.h"
#include "../../shared/common_utils.h"
#include "../../shared/common_defs.h"
#include "../../shared/entities.h"

typedef struct new_player_spawn_point_parse_context_t
    new_player_spawn_point_parse_context_t;

hashtable_define(new_player_spawn_point_table, uint32,
    new_player_spawn_point_t);
typedef struct new_player_spawn_point_table new_player_spawn_point_table_t;

struct new_player_spawn_point_parse_context_t
{
    bool32                      should_be_inserted;
    bool32                      have_position[3];
    bool32                      have_shared_instance_id;
    new_player_spawn_point_t    point;
};

new_player_spawn_point_table_t _table;

static int
_on_def(parse_def_file_context_t *ctx, const char *def, const char *val);

static int
_on_opt(parse_def_file_context_t *ctx, const char *opt, const char *val);

static int
_insert_spawn_point_from_context(
    new_player_spawn_point_parse_context_t *context);

static void
_check_player_race_start_area_id(player_race_def_t *def, void *user_data);

int
new_player_spawn_points_init(void)
{
    new_player_spawn_point_table_einit(&_table, 16);
    new_player_spawn_point_parse_context_t context = {0};
    const char *path = "data/server/new_player_spawn_points.def";
    if (parse_def_file(path, _on_def, _on_opt, &context) ||
        _insert_spawn_point_from_context(&context))
    {
        LOG_ERROR("Failed to parse new player spawn points from '%s'.", path);
        return 1;
    }
    bool32 ok = 1;
    ent_for_each_player_race_def(_check_player_race_start_area_id, &ok);
    if (!ok)
        return 2;
    return 0;
}

void
new_player_spawn_points_destroy(void)
    {new_player_spawn_point_table_destroy(&_table);}

new_player_spawn_point_t *
new_player_spawn_points_find(uint32 spawn_point_id)
    {return new_player_spawn_point_table_find(&_table, spawn_point_id);}

static int
_on_def(parse_def_file_context_t *ctx, const char *def, const char *val)
{
    int ret = 0;
    new_player_spawn_point_parse_context_t *context = ctx->user_data;
    if (context->should_be_inserted &&
        _insert_spawn_point_from_context(context))
        {ret = 1; goto out;}
    if (!streq(def, "spawn_point"))
    {
        LOG_ERROR("Undefined keyword '%s' in new player spawn points file.",
            def);
        ret = 2;goto out;
    }
    long long v = strtoll(val, 0, 10);
    if (v < 0 || v > 0xFFFFFFFF)
    {
        LOG_ERROR("New player spawnpoint must be uint32.");
        ret = 3;
        goto out;
    }
    uint32 id = (uint32)v;
    if (new_player_spawn_point_table_exists(&_table, id))
    {
        LOG("New player spawn point %u defined more than once.", id);
        ret = 4;
        goto out;
    }
    out:;
        if (ret)
            LOG_ERROR("Error at line %zu when parsing new player spawn "
                "points.");
        else
            context->should_be_inserted = 1;
        return ret;
}

static int
_on_opt(parse_def_file_context_t *ctx, const char *opt, const char *val)
{
    new_player_spawn_point_parse_context_t *context = ctx->user_data;
    if (streq(opt, "x"))
    {
        context->point.position[0]  = atoi(val);
        context->have_position[0]   = 1;
    } else
    if (streq(opt, "y"))
    {
        context->point.position[1]  = atoi(val);
        context->have_position[1]   = 1;
    } else
    if (streq(opt, "z"))
    {
        context->point.position[2]  = atoi(val);
        context->have_position[2]   = 1;
    } else if (streq(opt, "shared_instance_id"))
    {
        context->point.shared_instance_id   = str_to_uint32(val);
        context->have_shared_instance_id    = 1;
    } else
    {
        LOG_ERROR("Error at line %zu when parsing new player spawn points: "
            "undefined keyuword '%s'.\n", ctx->line, opt);
        return 1;
    }
    return 0;
}

static int
_insert_spawn_point_from_context(
    new_player_spawn_point_parse_context_t *context)
{
    if (!context->have_position[0] || !context->have_position[1] ||
        !context->have_position[2])
    {
        LOG_ERROR("x, y and z not all defined for a new player spawn point.");
        return 1;
    }
    if (!context->have_shared_instance_id)
    {
        LOG_ERROR("have_shared_instance_id not defined for a new player spawn "
            "point.");
        return 2;
    }
    instance_t *inst = inst_find(context->point.shared_instance_id);
    if (!inst)
    {
        LOG_ERROR("New player spawn point set to shared instance id '%u', but "
            "instance does not exist.");
        return 3;
    }
    if (context->point.position[0] < 0 || context->point.position[1] < 0 ||
        context->point.position[2] < 0 ||
        context->point.position[0] >= inst->map_data->w_in_tiles ||
        context->point.position[1] >= inst->map_data->w_in_tiles ||
        context->point.position[2] >= MAP_CHUNK_T)
    {
        LOG_ERROR("New player spawn point out of map range (min 0, max x %u, "
            "max y %u.\n", inst->map_data->w_in_tiles,
            inst->map_data->h_in_tiles);
        return 4;
    }
    new_player_spawn_point_table_einsert(&_table, context->point.id,
        context->point);
    return 0;
}

static void
_check_player_race_start_area_id(player_race_def_t *def, void *user_data)
{
    if (new_player_spawn_point_table_exists(&_table, def->start_area_id))
        return;
    LOG_ERROR("Invalid start area id defined for player race %u.", def->name);
    *(bool32*)user_data = 0;
}
