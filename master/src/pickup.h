/* pickup.h
 * Procedures for objects picking up other objects. */

#ifndef MUTA_MASTER_PICKUP_H
#define MUTA_MASTER_PICKUP_H

#include "../../shared/types.h"
#include "../../shared/common_defs.h"

/* Forward declaration(s) */
typedef struct player_t         player_t;
typedef struct dynamic_object_t dynamic_object_t;

/* Types defined here. */
typedef struct pickup_state_t pickup_state_t;

int
pickup_request_player_pick_up_dynamic_object(player_t *player,
    dynamic_object_t *dobj, enum equipment_slot equipment_slot, uint32 x,
    uint32 y);

int
pickup_on_player_owner_replied(player_t *player,
    dynamic_object_t *dobj, uint32 bag_index, uint32 x, uint32 y, int error);

int
pickup_on_dobj_owner_replied(player_t *player, dynamic_object_t *dobj,
    uint32 bag_index, uint32 x, uint32 y, int error);
/* Returns non-zero if the sim server did something illegal. */

#endif /* MUTA_MASTER_PICKUP_H */
