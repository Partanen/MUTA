#include "creature.h"
#include "common.h"
#include "instance.h"
#include "sim_server.h"
#include "player.h"
#include "client.h"
#include "../../shared/common_utils.h"
#include "../../shared/hashtable.h"
#include "../../shared/sim_packets.h"
#include "../../shared/client_packets.h"

typedef struct creature_table creature_table_t;

hashtable_define(creature_table, creature_runtime_id_t, uint32);

static fixed_pool(creature_t)   _pool;
static creature_table_t         _table;
static creature_runtime_id_t    _running_runtime_id;

static inline creature_runtime_id_t
_next_runtime_id(void);

static void
_enter_new_instance(creature_t *creature, instance_t *inst, int x, int y,
    int z);

int
creature_init(void)
{
    fixed_pool_init(&_pool, com_config.max_creatures);
    creature_table_einit(&_table,
        (size_t)(1.5f * (float)com_config.max_creatures));
    return 0;
}

void
creature_destroy(void)
{
    fixed_pool_destroy(&_pool);
    creature_table_destroy(&_table);
}

uint32
creature_get_index(creature_t *creature)
    {return fixed_pool_index(&_pool, creature);}

creature_t *
creature_get(uint32 index)
    {return &_pool.all[index];}

creature_t *
creature_find(creature_runtime_id_t id)
{
    uint32 *index = creature_table_find(&_table, id);
    if (!index)
        return 0;
    return &_pool.all[*index];
}

creature_t *
creature_request_spawn(creature_type_id_t type_id, uint32 instance_id,
    int position[3], int direction, int sex, uint32 health_current,
    uint32 health_max)
{
    instance_t *inst = inst_find(instance_id);
    if (!inst)
    {
        LOG_ERROR("Cannot spawn creature: too many already spawned.");
        return 0;
    }
    instance_part_t *part = inst_find_part_by_position(inst, position[0],
        position[1]);
    uint32 sim_index = part->sim_index;
    if (sim_index == SIM_INVALID_INDEX)
    {
        LOG_ERROR("Can't spawn dynamic object: instance has no sim server.");
        return 0;
    }
    creature_t *creature = fixed_pool_new(&_pool);
    if (!creature)
    {
        LOG_ERROR("Cannot spawn creature: too many already spawned.");
        return 0;
    }
    creature_runtime_id_t runtime_id = _next_runtime_id();
    size_t runtime_id_hash = hashtable_hash(&runtime_id, sizeof(runtime_id));
    uint32 index = fixed_pool_index(&_pool, creature);
    hashtable_einsert(_table, runtime_id, runtime_id_hash, index);
    creature->runtime_id            = runtime_id;
    creature->flags                 = 0;
    creature->instance_index        = inst_get_index(inst);
    creature->type_id               = type_id;
    creature->sim_index             = part->sim_index;
    creature->sex                   = (uint8)sex;
    creature->direction             = (uint8)direction;
    creature->interest_area_index   = 0xFFFFFFFF;
    creature->interest_area_prev    = 0xFFFFFFFF;
    creature->interest_area_next    = 0xFFFFFFFF;
    creature->health.current        = health_current;
    creature->health.max            = health_max;
    for (int i = 0; i < 3; ++i)
        creature->position[i] = position[i];
    tsimmsg_request_spawn_creature_t s =
    {
        .runtime_id     = runtime_id,
        .type_id        = type_id,
        .instance_id    = instance_id,
        .x              = position[0],
        .y              = position[1],
        .z              = (uint8)position[2],
        .direction      = (uint8)direction,
        .sex            = (uint8)sex,
        .health_current = health_current,
        .health_max     = health_max
    };
    bbuf_t bb = sim_send(sim_index, TSIMMSG_REQUEST_SPAWN_CREATURE_SZ);
    if (!bb.max_bytes)
    {
        LOG_ERROR("Cannot spawn creature: failed to send message to sim "
            "server.");
        goto fail;
    }
    tsimmsg_request_spawn_creature_write(&bb, &s);
    LOG("Requested spawning new creature of type %u, runtime ID %u.",
        type_id, runtime_id);
    return creature;
    fail:
        hashtable_erase(_table, runtime_id, runtime_id_hash);
        fixed_pool_free(&_pool, creature);
        return 0;
}

int
creature_confirm_spawn(creature_t *creature, uint32 instance_part_id,
    int x, int y, int z, int direction)
{
    if (creature->flags & CREATURE_FLAG_SPAWN_CONFIRMED)
    {
        LOG_ERROR("Sim server confirmed creature spawn more than once!");
        return 1;
    }
    instance_part_t *part = inst_find_part_by_id(instance_part_id);
    if (!part)
    {
        LOG_ERROR("Cannot confirm spawn of creature %u: instance part %u "
            "not found.", creature->runtime_id, instance_part_id);
        return 1;
    }
    if (creature->sim_index != part->sim_index)
    {
        LOG_ERROR("Sim confirmed spawn of creature %u not owned by it!",
            creature->runtime_id);
        return 1;
    }
    creature->direction = direction;
    creature->flags |= CREATURE_FLAG_SPAWN_CONFIRMED;
    instance_t *inst = inst_get(part->instance_index);
    LOG("Sim confirmed spawn of creature %u.", creature->runtime_id);
    _enter_new_instance(creature, inst, x, y, z);
    return 0;
}

int
creature_spawn_fail(creature_t *creature)
{
    if (creature->flags & CREATURE_FLAG_SPAWN_CONFIRMED)
    {
        LOG_ERROR("Sim server sent fail message for dynamic object that was "
            "already spawned successfully!");
        return 1;
    }
    LOG_ERROR("Sim server failed to spawn creature %u.", creature->runtime_id);
    creature_runtime_id_t runtime_id = creature->runtime_id;
    hashtable_erase(_table, creature->runtime_id,
        hashtable_hash(&runtime_id, sizeof(runtime_id)));
    fixed_pool_free(&_pool, creature);
    return 0;
}

void
creature_on_sim_disconnected(uint32 sim_index)
{
    creature_runtime_id_t   key;
    uint32                  value;
    hashtable_for_each_pair(_table, key, value)
    {
        DEBUG_PRINTFF("Implement me!\n");
    }
}

void
creature_serialize_to_msg(creature_t *creature, svmsg_add_creature_t *ret_s)
{
    ret_s->runtime_id       = creature->runtime_id;
    ret_s->type_id          = creature->type_id;
    ret_s->sex              = creature->sex;
    ret_s->direction        = creature->direction;
    ret_s->x                = creature->position[0];
    ret_s->y                = creature->position[1];
    ret_s->z                = (uint8)creature->position[2];
    ret_s->health_current   = 100;
    ret_s->health_max       = 100;
    ret_s->dead             = (creature->flags & CREATURE_FLAG_DEAD) ? 1 : 0;
}

void
creature_die(creature_t *creature)
{
    IMPLEMENTME();
    muta_assert(!(creature->flags & CREATURE_FLAG_DEAD));
    creature->flags |= CREATURE_FLAG_DEAD;
}

static inline creature_runtime_id_t
_next_runtime_id(void)
    {return _running_runtime_id++;}

static void
_enter_new_instance(creature_t *creature, instance_t *inst, int x, int y,
    int z)
{
    creature->position[0]       = x;
    creature->position[1]       = y;
    creature->position[2]       = z;
    creature->instance_index    = inst_get_index(inst);
    interest_area_t *ia = ia_find(inst->interest_areas, inst_w_in_tiles(inst),
        inst_h_in_tiles(inst), x, y, z);
    ia_add_creature(inst->interest_areas,
        (uint32)(ia - inst->interest_areas), creature);
    svmsg_add_creature_t s;
    creature_serialize_to_msg(creature, &s);
    interest_area_t *interest_area;
#ifdef _MUTA_DEBUG
    int num_iters = 0;
#endif
    INST_FOR_EACH_RELEVANT_INTEREST_AREA(inst, x, y, z, interest_area)
    {
        player_t *player;
        IA_FOR_EACH_PLAYER(interest_area, player)
        {
            /* CHECK PL_IS_RELEVANT */
            /* if (!PL_IS_RELEVANT(player))
                continue; */
            if (pil_should_cull(player->position, creature->position))
                continue;
            bbuf_t bb = cl_send(player->client, SVMSG_ADD_CREATURE_SZ);
            svmsg_add_creature_write(&bb, &s);
        }
#ifdef _MUTA_DEBUG
        num_iters++;
#endif
    }
    muta_assert(num_iters <= 27);
}
