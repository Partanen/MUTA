#include "poll.h"
#include "common.h"
#include "../../shared/netpoll.h"

static netpoll_t    _netpoll;
static thread_t     _thread;
static mutex_t      _mutex;
static bool32       _running;

static thread_ret_t
_main(void *args);

int
poll_init()
{
    if (thread_init(&_thread))
        return 1;
    mutex_init(&_mutex);
    return 0;
}

void
poll_destroy()
{
    thread_destroy(&_thread);
}

int
poll_start()
{
    if (netpoll_init(&_netpoll))
        return 1;
    _running = 1;
    if (!thread_create(&_thread, _main, 0))
        return 0;
    _running = 0;
    return 2;
}

void
poll_stop()
{
    interlocked_decrement_int32(&_running);
    netpoll_destroy(&_netpoll);
    thread_join(&_thread);
}

int
poll_add(socket_t socket, poll_data_t *data)
{
    netpoll_event_t netpoll_event;
    netpoll_event.events    = NETPOLL_READ | NETPOLL_ET;
    netpoll_event.data.ptr  = data;
    return netpoll_add(&_netpoll, socket, &netpoll_event);
}

void
poll_del(socket_t socket)
    {netpoll_del(&_netpoll, socket);}

static thread_ret_t
_main(void *args)
{
    netpoll_event_t events[64];
    while (interlocked_compare_exchange_int32(&_running, -1, -1))
    {
        int num_events = netpoll_wait(&_netpoll, events, 64, 250);
        if (!interlocked_compare_exchange_int32(&_running, -1, -1))
            return 0;
        for (int i = 0; i < num_events; ++i)
        {
            poll_data_t *data = events[i].data.ptr;
            data->callback(data->user_data);
        }
    }
    return 0;
}
