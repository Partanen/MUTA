#ifndef MUTA_SERVER_COMMON_H
#define MUTA_SERVER_COMMON_H

#include "../../shared/log.h"
#include "../../shared/common_utils.h"
#include "../../shared/common_defs.h"
#include "../../shared/net.h"
#include "../../shared/svchan_client.h"
#include "../../shared/svchan_server.h"
#include "../../shared/wdb_connector.h"

typedef struct com_config_t                 com_config_t;
typedef struct com_event_read_proxy_t       com_event_read_proxy_t;
typedef struct com_event_disconnect_proxy_t com_event_disconnect_proxy_t;
typedef struct com_event_accept_proxy_t     com_event_accept_proxy_t;
typedef struct com_event_svchan_client_t    com_event_svchan_client_t;
typedef struct com_event_svchan_server_t    com_event_svchan_server_t;
typedef struct com_event_wdbc_t             com_event_wdbc_t;
typedef struct com_event_t                  com_event_t;

#define _LOG(category, fmt, ...) \
    log_printf(&com_log, category, fmt, ##__VA_ARGS__)

#define _LOG_EXT(category, fmt, ...) \
    log_printf(&com_log, category, "[%s, %s] " fmt, __FILE__, __func__, \
        ##__VA_ARGS__)

#define LOG(fmt, ...) \
    _LOG(LOG_CATEGORY_INFO, fmt, ##__VA_ARGS__)
/* LOG()
 * Write general informatoin to the server log. */

#define LOG_ERROR(fmt, ...) \
    _LOG(LOG_CATEGORY_ERROR, fmt, ##__VA_ARGS__)
/* LOG_ERROR()
 * Write an error to the server log. */

#define LOG_WORLD(fmt, ...) \
    _LOG(LOG_CATEGORY_WORLD, fmt, ##__VA_ARGS__)

#define LOG_EXT(fmt, ...) \
    _LOG_EXT(LOG_CATEGORY_INFO, fmt, ##__VA_ARGS__)
/* LOG_EXT()
 * Write to the server log, but also include the function and file names. */

#define LOG_WORLD_EXT(fmt, ...) \
    _LOG_EXT(LOG_CATEGORY_WORLD, fmt, ##__VA_ARGS__)

#ifdef _MUTA_DEBUG
    #define LOG_DEBUG(fmt, ...) \
        _LOG(LOG_CATEGORY_DEBUG, fmt, ##__VA_ARGS__)
    #define LOG_DEBUG_EXT(fmt, ...) \
        _LOG_EXT(LOG_CATEGORY_DEBUG, fmt, ##__VA_ARGS__)
#else
    #define LOG_DEBUG(fmt, ...) ((void)0)
    #define LOG_DEBUG_EXT(fmt, ...) ((void)0)
#endif

enum com_event
{
    COM_EVENT_READ_PROXY,
    COM_EVENT_ACCEPT_PROXY,
    COM_EVENT_DISCONNECT_PROXY,
    COM_EVENT_READ_LOGIN_SERVER,
    COM_EVENT_LOGIN_SERVER_CONNECTED,
    COM_EVENT_SVCHAN_CLIENT,
    COM_EVENT_SVCHAN_SERVER,
    COM_EVENT_DB_DISCONNECTED,
    COM_EVENT_WDBC,
    COM_EVENT_DESPAWN_PLAYERS,
    COM_EVENT_DISCONNECT_CLIENTS,
};

enum log_category
{
    LOG_CATEGORY_INFO,
    LOG_CATEGORY_DEBUG,
    LOG_CATEGORY_ERROR,
    LOG_CATEGORY_WORLD,
    LOG_CATEGORY_WDB_INFO,
    LOG_CATEGORY_WDB_ERROR,
    LOG_CATEGORY_WDB_DEBUG,
    NUM_LOG_CATEGORIES
};

struct com_config_t
{
    uint32  max_clients;
    uint32  max_sims;
    uint32  max_online_players;
    uint32  max_dynamic_objects;
    uint32  max_creatures;
    uint16  *sim_ports;
    dchar   *sim_username;
    dchar   *sim_password;
    dchar   *world_db_name;
    uint8   world_db_address[4];
    uint16  world_db_port;
#if defined(__unix__)
    dchar   *world_db_unix_socket; /* Optional */
#endif
    dchar   *world_db_username;
    dchar   *world_db_password;
    uint16  proxy_port;
    uint8   login_address[4];
    uint16  login_port;
    char    shard_name[MAX_SHARD_NAME_LEN + 1];
    int32   view_distance_h; /* In tiles. */
    int32   view_distance_v;
};

struct com_event_read_proxy_t
{
    uint32  proxy_index;
    uint32  proxy_id;
    uint8   *memory;
    int     num_bytes;
};

struct com_event_disconnect_proxy_t
{
    uint32 proxy_index;
};

struct com_event_accept_proxy_t
{
    socket_t socket;
};

struct com_event_t
{
    int type;
    union
    {
        com_event_read_proxy_t          read_proxy;
        com_event_accept_proxy_t        accept_proxy;
        svchan_client_event_t           svchan_client;
        svchan_server_event_t           svchan_server;
        wdbc_event_t                    wdbc;
        com_event_disconnect_proxy_t    disconnect_proxy;
    };
};
/* event_t
 * Used across the program to pass commands and data from other threads to the
 * main thread via com_push_events(). */

extern com_config_t com_config;
extern log_t        com_log;
extern segpool_t    com_main_thread_segpool;

int
com_init(const char *config_path_override);

void
com_destroy(void);

void *
com_event_malloc(uint32 num_bytes);

void
com_event_free(void *ptr);

void
com_push_events(com_event_t *events, int num_events);

void
com_push_events_no_wait(com_event_t *events, int num_events);

int
com_wait_events(com_event_t *ret_events, int32 max, int timeout_ms);

#endif /* MUTA_SERVER_COMMON_H */
