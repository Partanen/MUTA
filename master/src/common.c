#include "common.h"
#include "../../shared/sv_common_defs.h"

com_config_t    com_config;
log_t           com_log;
segpool_t       com_main_thread_segpool;

static segpool_t        _segpool;
static mutex_t          _segpool_mutex;
static event_buf_t      _event_buf;
static log_category_t   _log_cats[NUM_LOG_CATEGORIES] =
{
    {LOG_CATEGORY_INFO, "INFO"},
    {LOG_CATEGORY_DEBUG, "DEBUG"},
    {LOG_CATEGORY_ERROR, "ERROR"},
    {LOG_CATEGORY_WORLD, "WORLD"},
    {LOG_CATEGORY_WDB_INFO, "WDB-INFO"},
    {LOG_CATEGORY_WDB_ERROR, "WDB-ERROR"},
    {LOG_CATEGORY_WDB_DEBUG, "WDB-DEBUG"}
};

static void
_parse_cfg_callback(void *ctx, const char *opt, const char *val);

int
com_init(const char *config_path_override)
{
    if (log_init(&com_log, ".", "muta_master.log", _log_cats,
        NUM_LOG_CATEGORIES))
        return 1;
    com_config.max_clients          = 4096;
    com_config.max_online_players   = 4096;
    com_config.max_dynamic_objects  = 4096;
    com_config.max_creatures        = 4096;
    com_config.max_sims             = 32;
    com_config.login_address[0]     = 127;
    com_config.login_address[1]     = 0;
    com_config.login_address[2]     = 0;
    com_config.login_address[3]     = 1;
    com_config.login_port           = DEFAULT_SHARD_TO_LOGIN_PORT;
    com_config.world_db_address[0]  = 127;
    com_config.world_db_address[1]  = 0;
    com_config.world_db_address[2]  = 0;
    com_config.world_db_address[3]  = 1;
    com_config.world_db_port        = 3306; /* mariadb default port */
    com_config.view_distance_h      = 26;
    com_config.view_distance_v      = 20;
    com_config.proxy_port           = DEFAULT_PROXY_PORT;
    if (!config_path_override)
        config_path_override = "config.cfg";
    parse_cfg_file(config_path_override, _parse_cfg_callback, 0);
    if (!darr_num(com_config.sim_ports))
    {
        LOG("At least 1 sim_port must be defined (default: %d).",
            DEFAULT_SIM_PORT);
        return 1;
    }
    if (!com_config.sim_username || !dstr_len(com_config.sim_username))
    {
        LOG("sim_username must be defined in config.");
        return 1;
    }
    if (!com_config.sim_password || !dstr_len(com_config.sim_password))
    {
        LOG("sim_password must be defined in config.");
        return 1;
    }
    if (!com_config.world_db_username)
    {
        LOG("world_db_username must be defined in config.");
        return 1;
    }
    if (!com_config.world_db_password)
    {
        LOG("world_db_password must be defined in config.");
        return 1;
    }
    if (!com_config.world_db_name)
    {
        LOG("world_db_name must be defined in config.");
        return 1;
    }
    if (strlen(com_config.shard_name) < MIN_SHARD_NAME_LEN ||
        strlen(com_config.shard_name) > MAX_SHARD_NAME_LEN)
    {
        LOG("shard_name must be provided in config, and it must be %d to %d "
            "in length.", MIN_SHARD_NAME_LEN, MAX_SHARD_NAME_LEN);
        return 1;
    }
    segpool_init(&_segpool);
    segpool_init(&com_main_thread_segpool);
    mutex_init(&_segpool_mutex);
    event_init(&_event_buf, sizeof(com_event_t), 4096);
    return 0;
}

void
com_destroy(void)
{
    event_destroy(&_event_buf);
    segpool_destroy(&com_main_thread_segpool);
    segpool_destroy(&_segpool);
    mutex_destroy(&_segpool_mutex);
    log_destroy(&com_log);
}

void *
com_event_malloc(uint32 num_bytes)
{
    mutex_lock(&_segpool_mutex);
    void *ret = segpool_malloc(&_segpool, num_bytes);
    mutex_unlock(&_segpool_mutex);
    return ret;
}

void
com_event_free(void *ptr)
{
    mutex_lock(&_segpool_mutex);
    segpool_free(&_segpool, ptr);
    mutex_unlock(&_segpool_mutex);
}

void
com_push_events(com_event_t *events, int32 num_events)
    {event_push(&_event_buf, events, num_events);}

void
com_push_events_no_wait(com_event_t *events, int num_events)
    {event_push_no_wait(&_event_buf, events, num_events);}

int
com_wait_events(com_event_t *ret_events, int32 max, int timeout_ms)
    {return event_wait(&_event_buf, ret_events, max, timeout_ms);}

static void
_parse_cfg_callback(void *ctx, const char *opt, const char *val)
{
    if (streq(opt, "max_clients"))
    {
        com_config.max_clients = str_to_uint32(val);
        if (!com_config.max_clients)
            muta_panic_print("config: max_clients must be at least 1.");
    } else
    if (streq(opt, "max_sims"))
    {
        com_config.max_sims = str_to_uint32(val);
        if (!com_config.max_sims)
            muta_panic_print("config: max_sims must be at least 1.");
    } else
    if (streq(opt, "max_online_players"))
    {
        com_config.max_online_players = str_to_uint32(val);
        if (!com_config.max_online_players)
            muta_panic_print("config: max_online_players must be at least 1.");
    } else
    if (streq(opt, "max_dynamic_objects"))
    {
        com_config.max_dynamic_objects = str_to_uint32(val);
        if (!com_config.max_dynamic_objects)
            muta_panic_print("config: max_dynamic_objects must be at least 1.");
    } else
    if (streq(opt, "max_creatures"))
    {
        com_config.max_creatures = str_to_uint32(val);
        if (!com_config.max_creatures)
            muta_panic_print("config: max_creatures must be at least 1.");
    } else
    if (streq(opt, "world_db_address"))
    {
        if (sscanf(val, "%hhu.%hhu.%hhu.%hhu", &com_config.world_db_address[0],
            &com_config.world_db_address[1], &com_config.world_db_address[2],
            &com_config.world_db_address[3]) != 4)
            muta_panic_print("config: %s must be of format 0.0.0.0", opt);
    } else
    if (streq(opt, "world_db_port"))
        com_config.world_db_port = (uint16)str_to_uint32(val);
    else if (streq(opt, "world_db_username"))
        dstr_set(&com_config.world_db_username, val);
    else if (streq(opt, "world_db_password"))
        dstr_set(&com_config.world_db_password, val);
    else if (streq(opt, "proxy_port"))
    {
        if (sscanf(val, "%hhu.%hhu.%hhu.%hhu", &com_config.world_db_address[0],
            &com_config.world_db_address[1], &com_config.world_db_address[2],
            &com_config.world_db_address[3]) != 4)
            muta_panic_print("config: %s must be of format 0.0.0.0", opt);
    } else
    if (streq(opt, "shard_name"))
    {
        size_t len = strlen(val);
        if (len < MIN_SHARD_NAME_LEN || len >= MAX_SHARD_NAME_LEN ||
            !str_is_ascii(val))
            muta_panic_print("config: %s is illegal. Min length: %d, max "
                "length: %d, all characters must be ascii.", opt,
                MIN_SHARD_NAME_LEN, MAX_SHARD_NAME_LEN);
        strcpy(com_config.shard_name, val);
    } else
    if (streq(opt, "view_distance_h"))
    {
        int32 v = atoi(val);
        if (v < 0)
            muta_panic_print("config: %s must be >= 0", opt);
        com_config.view_distance_h = v;
    } else
    if (streq(opt, "view_distance_v"))
    {
        int32 v = atoi(val);
        if (v < 0)
            muta_panic_print("config: %s must be >= 0", opt);
        com_config.view_distance_v = v;
    } else
    if (streq(opt, "sim_port"))
    {
        uint32 v = str_to_uint32(val);
        if (v > UINT16_MAX)
            muta_panic_print("sim_ports must be < %u.", UINT16_MAX);
        for (uint32 i = 0; i < darr_num(com_config.sim_ports); ++i)
            if (com_config.sim_ports[i] == v)
                return;
        darr_push(com_config.sim_ports, (uint16)v);
    } else
    if (streq(opt, "sim_username"))
        dstr_set(&com_config.sim_username, val);
    else if (streq(opt, "sim_password"))
        dstr_set(&com_config.sim_password, val);
#if defined(__unix__)
    else if (streq(opt, "world_db_unix_socket"))
        dstr_set(&com_config.world_db_unix_socket, val);
#endif
    else if (streq(opt, "world_db_name"))
        dstr_set(&com_config.world_db_name, val);
    else
        muta_panic_print("Unknown config option '%s'.\n", opt);
}
