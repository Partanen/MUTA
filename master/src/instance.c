#include "instance.h"
#include "common.h"
#include "map.h"
#include "player.h"
#include "sim_server.h"
#include "interest_area.h"
#include "dynamic_object.h"
#include "creature.h"
#include "../../shared/sv_common_defs.h"
#include "../../shared/common_defs.h"
#include "../../shared/entities.h"

static instance_t               *_instances; /* darr */
static instance_runtime_id_t    _running_instance_part_id;

static int
_on_instance_def(parse_def_file_context_t *ctx, const char *def,
    const char *val);

static int
_on_instance_opt(parse_def_file_context_t *ctx, const char *opt,
    const char *val);

int
inst_init(void)
{
    int err = 0;
    if (parse_def_file("instance_config.def", _on_instance_def,
        _on_instance_opt, 0))
    {
        LOG_ERROR("Errors reading instance_config.def.");
        err = 1;
        goto fail;
    }
    /* Check parsed instances are OK and initialize them. */
    for (uint32 i = 0; i < darr_num(_instances); ++i)
    {
        instance_t *instance = &_instances[i];
        /* Check there's enough instancep parts assigned */
        if (!instance->num_parts)
        {
            LOG_ERROR("Not enough parts defined for an object in "
                "instance_config.def (must be at least 1).");
            err = 2;
            goto fail;
        }
        /* Check the id of the instance is unique */
        for (uint32 j = 0; j < darr_num(_instances); ++j)
            if (i != j && _instances[i].id != _instances[j].id)
            {
                LOG_ERROR("Duplicate id %u in instance_config.def.",
                    _instances[i].id);
                err = 3;
                goto fail;
            }
        /* Initialize parts */
        instance->parts = ecalloc(
            instance->num_parts * sizeof(instance_part_t));
        for (uint32 j = 0; j < instance->num_parts; ++j)
        {
            /* Initialize current sim server to none - we'll find one later. */
            instance->parts[j].sim_index = 0xFFFFFFFF;
            /* Divide parts into a grid */
            uint32 x_parts = instance->num_parts / 2;
            uint32 y_parts = x_parts + instance->num_parts % 2;
            if (!x_parts)
                x_parts = 1;
            if (!y_parts)
                y_parts = 1;
            /* Clamp to map width/height in chunks */
            uint32 w_in_chunks = instance->map_data->w_in_tiles / MAP_CHUNK_W;
            uint32 h_in_chunks = instance->map_data->h_in_tiles / MAP_CHUNK_W;
            if (x_parts > w_in_chunks)
                x_parts = w_in_chunks;
            if (y_parts > h_in_chunks)
                y_parts = h_in_chunks;
            instance->w_in_parts = x_parts;
            instance->h_in_parts = y_parts;
            for (uint32 x = 0; x < x_parts; ++x)
                for (uint32 y = 0; y < y_parts; ++y)
                {
                    instance_part_t *part = &instance->parts[y * x_parts + x];
                    part->id                = _running_instance_part_id++;
                    part->instance_index    = i;
                    part->x_in_chunks       = x;
                    part->y_in_chunks       = y;
                    part->w_in_chunks       = w_in_chunks / x_parts;
                    part->h_in_chunks       = h_in_chunks / y_parts;
                    part->flags             = 0;
                }
        }
        instance->interest_areas = ia_init_instance(
            instance->map_data->w_in_tiles, instance->map_data->h_in_tiles);
    }
    return 0;
    fail:
        return err;
}

void
inst_destroy(void)
{
}

instance_t *
inst_find(uint32 id)
{
    uint32 num_instances = darr_num(_instances);
    for (uint32 i = 0; i < num_instances; ++i)
        if (_instances[i].id == id)
            return &_instances[i];
    return 0;
}

uint32
inst_get_index(instance_t *instance)
    {return (uint32)(instance - _instances);}

uint32
inst_get_num_instances(void)
    {return darr_num(_instances);}

uint32
inst_find_unsimulated_part(uint32 instance_index)
{
    instance_t *instance = inst_get(instance_index);
    for (uint32 i = 0; i < instance->num_parts; ++i)
        if (instance->parts[i].sim_index == SIM_INVALID_INDEX)
            return i;
    return INST_INVALID_INDEX;
}

instance_t *
inst_get(uint32 instance_index)
{
    muta_assert(instance_index < darr_num(_instances));
    return &_instances[instance_index];
}

instance_part_t *
inst_find_part_by_position(instance_t *instance, int x, int y)
{
    x = CLAMP(x, 0, (int)instance->w_in_chunks * MAP_CHUNK_W - 1);
    y = CLAMP(y, 0, (int)instance->h_in_chunks * MAP_CHUNK_W - 1);
    x = x % instance->w_in_parts;
    y = y % instance->h_in_parts;
    int index = y * instance->w_in_parts + x;
    muta_assert(index < instance->w_in_parts * instance->h_in_parts);
    return &instance->parts[index];
}

instance_part_t *
inst_find_part_by_id(uint32 instance_part_id)
{
    uint32 num_instances = darr_num(_instances);
    for (uint32 i = 0; i < num_instances; ++i)
    {
        uint32 num_parts = _instances[i].num_parts;
        for (uint32 j = 0; j < num_parts; ++j)
            if (_instances[i].parts[j].id == instance_part_id)
                return &_instances[i].parts[j];
    }
    return 0;
}

instance_part_t *
inst_get_part(instance_t *instance, uint32 part_index)
    {return &instance->parts[part_index];}

uint32
inst_w_in_tiles(instance_t *instance)
    {return instance->map_data->w_in_tiles;}

uint32
inst_h_in_tiles(instance_t *instance)
    {return instance->map_data->h_in_tiles;}

uint32
inst_map_id(instance_t *instance)
    {return instance->map_data->id;}

interest_area_t *
inst_find_interest_area(instance_t *instance, int x, int y, int z)
{
    return ia_find(instance->interest_areas, inst_w_in_tiles(instance),
        inst_h_in_tiles(instance), x, y, z);
}

interest_area_t *
inst_get_interest_area(instance_t *instance, uint32 interest_area_index)
    {return &instance->interest_areas[interest_area_index];}

void
inst_part_on_connected(instance_part_t *part)
{
    instance_t  *inst       = inst_get(part->instance_index);
    uint32      inst_id     = inst->id;
    spawn_db_t  *spawn_db   = &inst->map_data->spawn_db;
    uint32 num_spawns = spawn_db_num_rows(spawn_db);
    for (uint32 i = 0; i < num_spawns; ++i)
    {
        mdb_row_t row = spawn_db_get_row_by_index(spawn_db, i);
        int position[3] =
        {
            spawn_db_get_x(spawn_db, row),
            spawn_db_get_y(spawn_db, row)
        };
        if (inst_find_part_by_position(inst, position[0], position[1]) != part)
            continue;
        position[2] = (int)spawn_db_get_z(spawn_db, row);
        uint8 spawn_type = spawn_db_get_spawn_type(spawn_db, row);
        if (spawn_type == SPAWN_DB_SPAWN_TYPE_DYNAMIC_OBJECT)
        {
            dobj_type_id_t type_id = spawn_db_get_type_id(spawn_db, row);
            enum iso_dir direction = spawn_db_get_direction(spawn_db, row);
            dobj_request_spawn(type_id, inst_id, position, direction);
        } else
        if (spawn_type == SPAWN_DB_SPAWN_TYPE_CREATURE)
        {
            creature_type_id_t type_id = spawn_db_get_type_id(spawn_db, row);
            enum iso_dir direction = spawn_db_get_direction(spawn_db, row);
            enum ent_sex sex = spawn_db_get_sex(spawn_db, row);
            creature_request_spawn(type_id, inst_id, position, direction, sex,
                100, 100);
        }
    }
}

void
inst_part_on_disconnect(instance_part_t *part)
{
    part->sim_index = SIM_INVALID_INDEX;
    part->flags &= ~INST_PART_PLAYABLE;
    /* TODO: despawn creatures etc. */
}

uint32
inst_get_relevant_non_controlling_sims_of_position(instance_t *inst, int x,
    int y, uint32 ret_sims[8])
{
    IMPLEMENTME();
    return 0;
}

interest_area_t *
_inst_for_each_relevant_interest_area_get(instance_t *inst, int x, int y, int z)
{
    uint32 w_in_ias = inst_w_in_tiles(inst) / IA_W;
    uint32 h_in_ias = inst_h_in_tiles(inst) / IA_W;
    uint32 index    = z * (w_in_ias * h_in_ias) + y * w_in_ias + x;
    return &inst->interest_areas[index];
}

static int
_on_instance_def(parse_def_file_context_t *ctx, const char *def,
    const char *val)
{
    if (!streq(def, "instance"))
        return 1;
    instance_t instance = {0};
    darr_push(_instances, instance);
    return 0;
}

static int
_on_instance_opt(parse_def_file_context_t *ctx, const char *opt,
    const char *val)
{
    instance_t *instance = &_instances[darr_num(_instances) - 1];
    if (streq(opt, "id"))
        instance->id = str_to_uint32(val);
    else if (streq(opt, "map_id"))
    {
        uint32 map_id = str_to_uint32(val);
        instance->map_data = map_find(map_id);
        if (!instance->map_data)
        {
            LOG_ERROR("Invalid map id defined in instance_config.def (%u).",
                map_id);
            return 1;
        }
        instance->w_in_chunks = instance->map_data->w_in_tiles / MAP_CHUNK_W;
        instance->h_in_chunks = instance->map_data->h_in_tiles / MAP_CHUNK_W;
    } else
    if (streq(opt, "num_parts"))
        instance->num_parts = str_to_uint32(val);
    return 0;
}
