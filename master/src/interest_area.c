#include "interest_area.h"
#include "player.h"
#include "dynamic_object.h"
#include "creature.h"
#include "instance.h"
#include "../../shared/muta_map_format.h"
#include "../../shared/common_utils.h"

interest_area_t *
ia_init_instance(uint32 w_in_tiles, uint32 h_in_tiles)
{
    uint32 w_ias    = w_in_tiles / IA_W;
    uint32 h_ias    = h_in_tiles / IA_W;
    uint32 t_ias    = MAP_CHUNK_T / IA_T;
    uint32 num_ias  = w_ias * h_ias * t_ias;
    interest_area_t *interest_areas = emalloc(
        num_ias * sizeof(interest_area_t));
    for (uint32 i = 0; i < num_ias; ++i)
    {
        interest_areas[i].players           = 0xFFFFFFFF;
        interest_areas[i].dynamic_objects   = 0xFFFFFFFF;
        interest_areas[i].creatures         = 0xFFFFFFFF;
    }
    DEBUG_PRINTFF("Initialized %d interest areas.\n", num_ias);
    return interest_areas;
}

interest_area_t *
ia_find(interest_area_t *interest_areas, uint32 w_in_tiles, uint32 h_in_tiles,
    int x, int y, int z)
{
    uint32 ux       = CLAMP((uint32)x, 0, w_in_tiles - 1);
    uint32 uy       = CLAMP((uint32)y, 0, h_in_tiles - 1);
    uint32 uz       = CLAMP((uint32)z, 0, MAP_CHUNK_T - 1);
    uint32 iax      = ux / IA_W;
    uint32 iay      = uy / IA_W;
    uint32 iaz      = uz / IA_T;
    uint32 w_in_ias = w_in_tiles / IA_W;
    uint32 h_in_ias = h_in_tiles / IA_W;
    uint32 index    = iaz * (w_in_ias * h_in_ias) + iay * w_in_ias + iax;
    return &interest_areas[index];
}

void
ia_add_player(interest_area_t *interest_areas, uint32 interest_area_index,
    player_t *player)
{
    interest_area_t *interest_area = &interest_areas[interest_area_index];
    uint32 player_index = pl_get_index(player);
#ifdef _MUTA_DEBUG
    {
        uint32 index = interest_area->players;
        for (; index != 0xFFFFFFFF; index = pl_get(index)->interest_area_next)
            if (index == player_index)
            {
                DEBUG_PRINTFF("Player already added, index %u\n", index);
                muta_assert(0);
            }
    }
#endif
    uint32 next_index = interest_area->players;
    player->interest_area_index = interest_area_index;
    player->interest_area_next  = next_index;
    player->interest_area_prev  = 0xFFFFFFFF;
    if (next_index != 0xFFFFFFFF)
    {
        player_t *other_player = pl_get(next_index);
        other_player->interest_area_prev = player_index;
    }
    interest_area->players = player_index;
}

void
ia_add_dynamic_object(interest_area_t *interest_areas,
    uint32 interest_area_index, dynamic_object_t *obj)
{
    interest_area_t *interest_area = &interest_areas[interest_area_index];
    uint32 obj_index = dobj_get_index(obj);
#ifdef _MUTA_DEBUG
    {
        uint32 index = interest_area->dynamic_objects;
        for (; index != 0xFFFFFFFF;
            index = dobj_get(index)->in.world.interest_area_next)
            if (index == obj_index)
            {
                DEBUG_PRINTFF("Dynamic object already added, index %u\n",
                    index);
                muta_assert(0);
            }
    }
#endif
    uint32 next_index = interest_area->dynamic_objects;
    obj->in.world.interest_area_index   = interest_area_index;
    obj->in.world.interest_area_next    = next_index;
    obj->in.world.interest_area_prev    = 0xFFFFFFFF;
    if (next_index != 0xFFFFFFFF)
    {
        dynamic_object_t *other_obj = dobj_get(next_index);
        other_obj->in.world.interest_area_prev = obj_index;
    }
    interest_area->dynamic_objects = obj_index;
}

void
ia_add_creature(interest_area_t *interest_areas, uint32 interest_area_index,
    creature_t *creature)
{
    interest_area_t *interest_area = &interest_areas[interest_area_index];
    uint32          creature_index = creature_get_index(creature);
#ifdef _MUTA_DEBUG
    {
        uint32 index = interest_area->creatures;
        for (; index != 0xFFFFFFFF;
            index = creature_get(index)->interest_area_next)
            if (index == creature_index)
            {
                DEBUG_PRINTFF("Dynamic creatureect already added, index %u\n",
                    index);
                muta_assert(0);
            }
    }
#endif
    uint32 next_index = interest_area->creatures;
    creature->interest_area_index   = interest_area_index;
    creature->interest_area_next    = next_index;
    creature->interest_area_prev    = 0xFFFFFFFF;
    if (next_index != 0xFFFFFFFF)
    {
        creature_t *other_creature = creature_get(next_index);
        other_creature->interest_area_prev = creature_index;
    }
    interest_area->creatures = creature_index;
}

void
ia_del_player(player_t *player)
{
    muta_assert(player->interest_area_index != 0xFFFFFFFF);
    instance_t      *inst           = inst_get(player->instance_index);
    interest_area_t *interest_area  =
        &inst->interest_areas[player->interest_area_index];
    uint32 prev_index = player->interest_area_prev;
    uint32 next_index = player->interest_area_next;
    if (prev_index != 0xFFFFFFFF)
    {
        player_t *prev_player = pl_get(prev_index);
        prev_player->interest_area_next = next_index;
    } else
        interest_area->players = next_index;
    if (next_index != 0xFFFFFFFF)
    {
        player_t *next_player = pl_get(next_index);
        next_player->interest_area_prev = prev_index;
    }
    player->interest_area_index = 0xFFFFFFFF;
    player->interest_area_prev  = 0xFFFFFFFF;
    player->interest_area_next  = 0xFFFFFFFF;
}

void
ia_del_dynamic_object(dynamic_object_t *obj)
{
    muta_assert(obj->in.world.interest_area_index != 0xFFFFFFFF);
    instance_t      *inst           = inst_get(obj->in.world.instance_index);
    interest_area_t *interest_area  =
        &inst->interest_areas[obj->in.world.interest_area_index];
    uint32 prev_index = obj->in.world.interest_area_prev;
    uint32 next_index = obj->in.world.interest_area_next;
    if (prev_index != 0xFFFFFFFF)
    {
        dynamic_object_t *prev_obj = dobj_get(prev_index);
        prev_obj->in.world.interest_area_next = next_index;
    } else
        interest_area->dynamic_objects = next_index;
    if (next_index != 0xFFFFFFFF)
    {
        dynamic_object_t *next_obj = dobj_get(next_index);
        next_obj->in.world.interest_area_prev = prev_index;
    }
    obj->in.world.interest_area_index   = 0xFFFFFFFF;
    obj->in.world.interest_area_prev    = 0xFFFFFFFF;
    obj->in.world.interest_area_next    = 0xFFFFFFFF;
}

void
ia_del_creature(creature_t *creature)
{
    muta_assert(creature->interest_area_index != 0xFFFFFFFF);
    instance_t      *inst           = inst_get(creature->instance_index);
    interest_area_t *interest_area  =
        &inst->interest_areas[creature->interest_area_index];
    uint32 prev_index = creature->interest_area_prev;
    uint32 next_index = creature->interest_area_next;
    if (prev_index != 0xFFFFFFFF)
    {
        creature_t *prev_creature = creature_get(prev_index);
        prev_creature->interest_area_next = next_index;
    } else
        interest_area->creatures = next_index;
    if (next_index != 0xFFFFFFFF)
    {
        creature_t *next_creature = creature_get(next_index);
        next_creature->interest_area_prev = prev_index;
    }
    creature->interest_area_index   = 0xFFFFFFFF;
    creature->interest_area_prev    = 0xFFFFFFFF;
    creature->interest_area_next    = 0xFFFFFFFF;
}
