#include "data.h"
#include "common.h"
#include "../../shared/entities.h"

data_config_t data_config;

int
data_init(void)
{
    config_option_t opts[] =
    {
        {
            .name       = "player_starter_bag_dobj_type_id",
            .type       = CONFIG_OPTION_TYPE_UINT32,
            .optional   = 0,
            .u32.value  = &data_config.player_starter_bag_dobj_type_id
        }
    };
    enum config_parse_error error = parse_cfg_file_with_option_list(
        "data/server/data_config.cfg", opts,
        (uint32)(sizeof(opts) / sizeof(opts[0])));
    if (error == CONFIG_PARSE_ERROR_PARSING_FAILED)
    {
        LOG_ERROR("Failed to parse data_config.cfg: file doesn't exist or is "
            "invalid.");
        return 1;
    }
    if (error == CONFIG_PARSE_ERROR_MISSING_OPTIONS)
    {
        LOG_ERROR("Failed to parse data.cfg: missing required options.");
        return 1;
    }
    /* Validate data. */
    dynamic_object_def_t *def = ent_get_dynamic_object_def(
        data_config.player_starter_bag_dobj_type_id);
    if (!def)
    {
        LOG_ERROR("Definition for player starter bag dynamic object not "
            "found.");
        return 1;
    }
    if (!def->is_container)
    {
        LOG_ERROR("Player starter bag dynamic object is not a container.");
        return 1;
    }
    return 0;
}

void
data_destroy(void)
{
}
