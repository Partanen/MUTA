/* map.h
 * Storage for maps. A map bay be referred to by multiple different instances.
 * */

#ifndef MUTA_SERVER_MAP_H
#define MUTA_SERVER_MAP_H

#include "../../shared/spawn_db.h"

typedef struct map_data_t map_data_t;

struct map_data_t
{
    map_id_t    id;
    dchar       *name;
    uint32      w_in_tiles;
    uint32      h_in_tiles;
    spawn_db_t  spawn_db;
};

int
map_init(void);

void
map_destroy(void);

map_data_t *
map_find(uint32 map_id);

#endif /* MUTA_SERVER_MAP_H */
