/* login_server.h
 * Connection handling to the login server */

#ifndef MUTA_SERVER_LOGIN_SERVER_H
#define MUTA_SERVER_LOGIN_SERVER_H

#include "../../shared/types.h"

int
ls_init(void);

void
ls_destroy(void);

int
ls_connect(void);

void
ls_disconnect(void);

bool32
ls_is_connected(void);

void
ls_flush(void);

int
ls_send_player_select_shard_result(uint64 account_id, uint32 login_session_id,
    int result);

#endif /* MUTA_SERVER_LOGIN_SERVER_H */
