#include <stdio.h>
#include <signal.h>
#include "client.h"
#include "login_server.h"
#include "proxy.h"
#include "common.h"
#include "map.h"
#include "sim_server.h"
#include "instance.h"
#include "poll.h"
#include "player.h"
#include "dynamic_object.h"
#include "creature.h"
#include "new_player_spawn_point.h"
#include "data.h"
#include "../../shared/common_defs.h"
#include "../../shared/sv_time.c"
#include "../../shared/common_utils.h"
#include "../../shared/get_opt.h"
#include "../../shared/crypt.h"
#include "../../shared/entities.h"
#include "../../shared/abilities.h"
#include "../../shared/wdb_connector.h"

#define MAX_EVENTS 128

static int32                _running = 1; /* Atomic */
static get_opt_context_t    _opts = GET_OPT_CONTEXT_INITIALIZER;
static bool32               _daemonize;
static com_event_t          _events[MAX_EVENTS];
static bool32               _db_ok = 0;

static void
_sig_handler(int sig);

static void
_post_wdb_event(wdbc_event_t *event);

int main(int argc, char **argv)
{
    const char  *config_path_override = 0;
    int         opt;
    while ((opt = get_opt(&_opts, argc, argv, "dc:")) != -1)
    {
        switch (opt)
        {
        case 'd':
            _daemonize = 1;
            break;
        case 'c':
            config_path_override = _opts.arg;
            break;
        case '?':
            puts("Unknown command line option.");
            return 0;
        }
    }
    signal(SIGINT, _sig_handler);
    if (_daemonize && muta_daemonize())
    {
        puts("Running in daemon mode failed.");
        return 1;
    }
    init_time();
    if (init_socket_api())
    {
        fprintf(stderr, "init_socket_api() failed.\n");
        return 1;
    }
    srand((uint)time(0));
    if (log_api_init(64))
    {
        fprintf(stderr, "log_api_init() failed.\n");
        return 1;
    }
    if (com_init(config_path_override))
    {
        fprintf(stderr, "com_init() failed.\n");
        return 1;
    }
    LOG("MUTA Master Server v. " MUTA_VERSION_STR);
    if (crypt_init())
    {
        fprintf(stderr, "crypt_init() failed.\n");
        return 1;
    }
    if (cl_init())
    {
        fprintf(stderr, "cl_init() failed.\n");
        return 1;
    }
    if (poll_init())
    {
        fprintf(stderr, "poll_init() failed.\n");
        return 1;
    }
#if defined(__unix__)
    const char *unix_socket = com_config.world_db_unix_socket;
#else
    const char *unix_socket = 0;
#endif
    dchar *world_db_ip = dstr_create_empty(128);
    dstr_setf(&world_db_ip, "%u.%u.%u.%u", (uint)com_config.world_db_address[0],
        (uint)com_config.world_db_address[1],
        (uint)com_config.world_db_address[2],
        (uint)com_config.world_db_address[3]);
    int wdbc_success = wdbc_init(world_db_ip, com_config.world_db_port,
        unix_socket, com_config.world_db_name, com_config.world_db_username,
        com_config.world_db_password, 256, &com_log, LOG_CATEGORY_WDB_INFO,
        LOG_CATEGORY_WDB_ERROR, LOG_CATEGORY_WDB_DEBUG, _post_wdb_event);
    dstr_free(&world_db_ip);
    if (wdbc_success)
    {
        fprintf(stderr, "wdbc_init() failed.\n");
        return 1;
    }
    if (ls_init())
    {
        fprintf(stderr, "ls_init() failed.\n");
        return 1;
    }
    if (ab_load())
    {
        fprintf(stderr, "ab_load() failed.\n");
        return 1;
    }
    if (ent_load(0))
    {
        fprintf(stderr, "ent_load() failed.\n");
        return 1;
    }
    if (data_init())
    {
        fprintf(stderr, "data_init() failed.\n");
        return 1;
    }
    if (map_init())
    {
        fprintf(stderr, "map_init() failed.\n");
        return 1;
    }
    if (inst_init())
    {
        fprintf(stderr, "inst_init() failed.\n");
        return 1;
    }
    if (new_player_spawn_points_init())
    {
        fprintf(stderr, "new_player_spawn_points_init() failed.\n");
        return 1;
    }
    if (sim_init())
    {
        fprintf(stderr, "sim_init() failed.\n");
        return 1;
    }
    if (proxy_init())
    {
        fprintf(stderr, "proxy_init() failed.\n");
        return 1;
    }
    if (pl_init())
    {
        fprintf(stderr, "pl_init() failed.\n");
        return 1;
    }
    if (dobj_init())
    {
        fprintf(stderr, "dobj_init() failed.\n");
        return 1;
    }
    if (creature_init())
    {
        fprintf(stderr, "creature_init() failed.\n");
        return 1;
    }
    if (poll_start())
    {
        fprintf(stderr, "poll_start() failed.\n");
        return 1;
    }
    if (wdbc_connect())
    {
        fprintf(stderr, "wdbc_connect() failed.\n");
        return 1;
    }
    if (ls_connect())
    {
        fprintf(stderr, "ls_connect() failed.\n");
        return 1;
    }
    if (proxy_start())
    {
        fprintf(stderr, "proxy_start() failed.\n");
        return 1;
    }
    if (sim_start())
    {
        fprintf(stderr, "sim_start() failed.\n");
        return 1;
    }
    uint64 last_tick        = get_program_ticks_ms();
    uint64 target_delta     = (uint64)(1.0f / 60.f * 1000.f);
    while (interlocked_compare_exchange_int32(&_running, -1, -1))
    {
        uint64 current_tick = get_program_ticks_ms();
        uint64 delta        = current_tick - last_tick;
        uint64 max_sleep    = delta < target_delta ? target_delta - delta : 0;
        int num_events = com_wait_events(_events, MAX_EVENTS, (int)max_sleep);
        for (int i = 0; i < num_events; ++i)
        {
            switch (_events[i].type)
            {
            case COM_EVENT_READ_PROXY:
                proxy_read(&_events[i].read_proxy);
                break;
            case COM_EVENT_ACCEPT_PROXY:
                proxy_accept(&_events[i].accept_proxy);
                break;
            case COM_EVENT_DISCONNECT_PROXY:
                proxy_disconnect(&_events[i].disconnect_proxy);
                break;
            case COM_EVENT_SVCHAN_CLIENT:
                svchan_client_handle_event(&_events[i].svchan_client);
                break;
            case COM_EVENT_SVCHAN_SERVER:
                svchan_server_handle_event(&_events[i].svchan_server);
                break;
            case COM_EVENT_WDBC:
                wdbc_handle_event(&_events[i].wdbc);
                if (_events[i].wdbc.type == WDBC_EVENT_CONNECTED)
                    _db_ok = 1;
                else if (_events[i].wdbc.type == WDBC_EVENT_DISCONNECTED)
                    _db_ok = 0; /* What to do here? */
                break;
            case COM_EVENT_DESPAWN_PLAYERS:
                pl_complete_despawn_requests();
                break;
            case COM_EVENT_DISCONNECT_CLIENTS:
                cl_complete_disconnects();
                break;
            default:
                muta_assert(0);
            }
        }
        proxy_flush();
        ls_flush();
        sim_flush();
        if (delta < target_delta)
            continue;
        cl_update();
        last_tick = current_tick;
    }
    LOG("Disconnecting from login server...");
    ls_disconnect();
    LOG("Stopping proxy server connections...");
    proxy_stop();
    LOG("Stopping sim server connections...");
    sim_stop();
    LOG("Stopping world database connection...");
    wdbc_destroy();
    poll_stop();
    LOG("Exiting MUTA master.");
    creature_destroy();
    dobj_destroy();
    pl_destroy();
    proxy_destroy();
    sim_destroy();
    new_player_spawn_points_destroy();
    inst_destroy();
    map_destroy();
    data_destroy();
    ent_destroy();
    ab_destroy();
    ls_destroy();
    poll_destroy();
    cl_destroy();
    com_destroy();
    log_destroy(&com_log);
    log_api_destroy();
    return 0;
}

static void
_sig_handler(int sig)
    {interlocked_decrement_int32(&_running);}

static void
_post_wdb_event(wdbc_event_t *event)
{
    com_event_t com_event = {.type = COM_EVENT_WDBC, .wdbc = *event};
    com_push_events(&com_event, 1);
}
