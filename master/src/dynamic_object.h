#ifndef MUTA_MASTER_DYNAMIC_OBJECT_H
#define MUTA_MASTER_DYNAMIC_OBJECT_H

#include "container.h"
#include "../../shared/common_defs.h"
#include "../../shared/wdb_connector.h"

#define DOBJ_INVALID_INDEX 0xFFFFFFFF

/* Forward declaration(s) */
typedef struct dynamic_object_def_t         dynamic_object_def_t;
typedef struct svmsg_add_dynamic_object_t   svmsg_add_dynamic_object_t;
typedef struct player_t                     player_t;
typedef struct instance_t                   instance_t;

/* Types defined here */
typedef struct dynamic_object_t dynamic_object_t;

enum dobj_flag
{
    DOBJ_FLAG_SPAWN_CONFIRMED       = (1 << 0),
    DOBJ_FLAG_IN_CONTAINER          = (1 << 1),
    DOBJ_FLAG_EQUIPPED              = (1 << 2),
    DOBJ_FLAG_IN_DB                 = (1 << 3),
    DOBJ_FLAG_UNCONFIRMED_DB_STATE  = (1 << 4),
    DOBJ_FLAG_IS_CONTAINER          = (1 << 5),
    DOBJ_FLAG_UNCONFIRMED_SIM_STATE = (1 << 6)
};

struct dynamic_object_t
{
    dobj_runtime_id_t   runtime_id;
    dobj_type_id_t      type_id;
    uuid16_t            db_uuid; /* Relevant with DOBJ_FLAG_IN_DB */
    union {
        /* Data if spawned in world. */
        struct {
            uint32  sim_index;
            uint32  instance_index;
            int     position[3];
            uint32  interest_area_index;
            uint32  interest_area_next;
            uint32  interest_area_prev;
            uint8   direction;
        } world;
        /* Data if spawned inside a bag. */
        struct {
            uint32  dobj_index;
            uint8   position[2];
        } container;
        struct {
            uint32              player_index;
            equipment_slot_id_t equipment_slot;
        } equipped_by_player;
    } in; /* Union data depends if object is in a bag or in the world. */
    container_t     container;
    uint8           flags;
};

int
dobj_init(void);

void
dobj_destroy(void);

dynamic_object_t *
dobj_request_spawn(dobj_type_id_t type_id, uint32 instance_id,
    int position[3], int direction);
/* Dynamic objects can be spawned even if their sim server is down. When
 * the sim comes back up, a request is sent to actually spawn the object. */

void
dobj_despawn_container_on_owner_spawn_failed(dynamic_object_t *obj);

int
dobj_confirm_spawn(dynamic_object_t *obj, uint32 instance_part_id, int x, int y,
    int z, int direction);
/* Returns non-zero if the sim server confirmed an already spawned object. */

int
dobj_spawn_fail(dynamic_object_t *obj);
/* Returns non-zero if the sim server did something illegal. */

dynamic_object_t *
dobj_spawn_equipped_by_player(player_t *pl, uuid16_t db_uuid,
    dobj_type_id_t type_id, enum equipment_slot equipment_slot);

dynamic_object_t *
dobj_spawn_in_container(dynamic_object_t *container_dobj, uuid16_t db_uuid,
    dobj_type_id_t type_id, uint8 x, uint8 y);
/* Call for spawning a dynamic object that does not spawn directly in the world,
 * but inside a container, for example a bag. */

dynamic_object_t *
dobj_find(dobj_runtime_id_t id);

uint32
dobj_get_index(dynamic_object_t *obj);

dynamic_object_t *
dobj_get(uint32 index);

void
dobj_on_sim_disonnected(uint32 sim_index);
/* Called when a sim server disconnects. Despawns entities on the sim. */

void
dobj_serialize_to_msg(dynamic_object_t *obj, svmsg_add_dynamic_object_t *ret_s);

bool32
dobj_is_in_world(dynamic_object_t *obj);
/* Check if object is in the open world rather than inside a bag. */

uint32
dobj_get_relevant_noncontrolling_sims(dynamic_object_t *obj,
    uint32 ret_sim_indices[8]);
/* Return value is number of sims written in ret_sim_indices */

bool32
dobj_is_container(dynamic_object_t *obj);

player_t *
dobj_get_owning_player(dynamic_object_t *obj);
/* Returns 0 if not owned by player. */

void
dobj_on_enter_new_instance(dynamic_object_t *obj, instance_t *inst, int x,
    int y, int z);

#endif /* MUTA_MASTER_DYNAMIC_OBJECT_H */
