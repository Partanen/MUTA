#include "login_server.h"
#include "common.h"
#include "proxy.h"
#include "../../shared/netpoll.h"
#include "../../shared/net.h"
#include "../../shared/login_packets.h"
#include "../../shared/common_defs.h"
#include "../../shared/account_rules.h"
#include "../../shared/svchan_client.h"

static svchan_client_t _svchan;

static void
_svchan_post_event(svchan_client_event_t *event);

static void
_svchan_on_authed(svchan_client_t *client);

static int
_svchan_on_read_packet(svchan_client_t *client, uint8 *memory, int num_bytes);

static void
_svchan_on_disconnect(svchan_client_t *client);

static void
_svchan_on_connection_failed(svchan_client_t *client,
    enum svchan_client_connection_error reason);

static void
_svchan_send_still_here_msg(svchan_client_t *client);

static bbuf_t
_send_msg_to_login_server(int num_bytes);

static inline bbuf_t
_send_const_crypt_msg_to_login_server(int num_bytes);

static inline bbuf_t
_send_var_crypt_msg_to_login_server(int num_bytes);

static int
_handle_flmsg_player_select_shard(flmsg_player_select_shard_t *s);

static int
_handle_flmsg_player_cancel_select_shard(flmsg_player_cancel_select_shard_t *s);

int
ls_init(void)
{
    int ret = 0;
    svchan_client_callbacks_t callbacks = {0};
    callbacks.post_event            = _svchan_post_event;
    callbacks.on_authed             = _svchan_on_authed;
    callbacks.on_read_packet        = _svchan_on_read_packet;
    callbacks.on_disconnect         = _svchan_on_disconnect;
    callbacks.on_connection_failed  = _svchan_on_connection_failed;
    callbacks.send_still_here_msg   = _svchan_send_still_here_msg;
    if (svchan_client_init(&_svchan, &callbacks, 0, MUTA_MTU * 4, MUTA_MTU * 4,
        1))
        {ret = 1; goto fail;}
    return ret;
    fail:
        LOG_DEBUG_EXT("Failed with code %d.\n", ret);
        return ret;
}

void
ls_destroy(void)
{
    svchan_client_destroy(&_svchan);
}

int
ls_connect(void)
{
    int ret = 0;
    addr_t address = create_addr(com_config.login_address[0],
        com_config.login_address[1], com_config.login_address[2],
        com_config.login_address[3], com_config.login_port);
    if (svchan_client_connect(&_svchan, &address, "admin", "password"))
        {ret = 1; goto fail;}
    return ret;
    fail:
        LOG_DEBUG_EXT("Failed with code %d.\n", ret);
        return ret;
}

void
ls_disconnect(void)
    {svchan_client_disconnect(&_svchan);}

void
ls_flush(void)
    {svchan_client_flush(&_svchan);}

int
ls_send_player_select_shard_result(uint64 account_id, uint32 login_session_id,
    int result)
{
    if (!svchan_client_is_connected(&_svchan))
        return 1;
    tlmsg_player_select_shard_result_t r_msg;
    r_msg.account_id        = account_id;
    r_msg.login_session_id  = login_session_id;
    r_msg.result            = (uint8)result;
    bbuf_t bb = _send_const_crypt_msg_to_login_server(
        TLMSG_PLAYER_SELECT_SHARD_RESULT_SZ);
    if (!bb.max_bytes)
    {
        ls_disconnect();
        return 2;
    }
    int r = tlmsg_player_select_shard_result_write(&bb, &_svchan.cryptchan,
        &r_msg);
    return r ? 3 : 0;
}

static void
_svchan_post_event(svchan_client_event_t *event)
{
    com_event_t new_event;
    new_event.type          = COM_EVENT_SVCHAN_CLIENT;
    new_event.svchan_client = *event;
    com_push_events(&new_event, 1);
}

static void
_svchan_on_authed(svchan_client_t *client)
{
    LOG("Successfully authenticated with the login server.");
    bbuf_t bb = _send_msg_to_login_server(0);
    if (!bb.max_bytes)
    {
        ls_disconnect();
        return;
    }
    tlmsg_went_online_for_players_write(&bb);
    LOG("Sent went_online_for_players message to login server.");
}

static int
_svchan_on_read_packet(svchan_client_t *client, uint8 *memory, int num_bytes)
{
    bbuf_t  bb          = BBUF_INITIALIZER(memory, num_bytes);
    int     dc          = 0;
    int     incomplete  = 0;
    lmsg_t  msg_type;
    while (BBUF_FREE_SPACE(&bb) >= sizeof(msg_type) && !incomplete && !dc)
    {
        BBUF_READ(&bb, &msg_type);
        switch (msg_type)
        {
        case FLMSG_PLAYER_SELECT_SHARD:
        {
            LOG_DEBUG("FLMSG_PLAYER_SELECT_SHARD");
            flmsg_player_select_shard_t s;
            incomplete = flmsg_player_select_shard_read(&bb, &_svchan.cryptchan,
                &s);
            if (!incomplete)
                dc = _handle_flmsg_player_select_shard(&s);
        }
            break;
        case FLMSG_PLAYER_CANCEL_SELECT_SHARD:
        {
            LOG_DEBUG("FLMSG_PLAYER_CANCEL_SELECT_SHARD");
            flmsg_player_cancel_select_shard_t s;
            incomplete = flmsg_player_cancel_select_shard_read(&bb, &s);
            if (!incomplete)
                dc = _handle_flmsg_player_cancel_select_shard(&s);
        }
            break;
        default:
            return -1;
        }
    }
    if (dc || incomplete < 0)
    {
        LOG_DEBUG_EXT("msg_type: %u, dc: %d, incomplete: %d", msg_type, dc,
            incomplete);
        return -1;
    }
    if (incomplete)
        return BBUF_FREE_SPACE(&bb) + sizeof(msg_type);
    return 0;
}

static void
_svchan_on_disconnect(svchan_client_t *client)
{
    int err;
    if ((err = ls_connect()))
        muta_panic_print("ls_connect() failed with error %d\n", err);
}

static void _svchan_on_connection_failed(svchan_client_t *client,
    enum svchan_client_connection_error reason)
{
    int err;
    if ((err = ls_connect()))
        muta_panic_print("ls_connect() failed, error %d.\n", err);
}

static void
_svchan_send_still_here_msg(svchan_client_t *client)
{
    bbuf_t bb = _send_msg_to_login_server(0);
    if (bb.max_bytes)
        tlmsg_still_here_write(&bb);
}

static bbuf_t
_send_msg_to_login_server(int num_bytes)
    {return svchan_client_send(&_svchan, sizeof(lmsg_t) + num_bytes);}

static inline bbuf_t
_send_const_crypt_msg_to_login_server(int num_bytes)
{
    return _send_msg_to_login_server(CRYPT_MSG_ADDITIONAL_BYTES + num_bytes);
}

static inline bbuf_t
_send_var_crypt_msg_to_login_server(int num_bytes)
{
    return _send_msg_to_login_server(sizeof(msg_sz_t) +
        CRYPT_MSG_ADDITIONAL_BYTES + num_bytes);
}

static int
_handle_flmsg_player_select_shard(flmsg_player_select_shard_t *s)
{
    char account_name[MAX_ACC_NAME_LEN + 1];
    muta_assert(s->account_name.len <= MAX_ACC_NAME_LEN);
    memcpy(account_name, s->account_name.data, s->account_name.len);
    account_name[s->account_name.len] = 0;
    if (ar_check_player_character_name(account_name, s->account_name.len))
        return 1;
    uint32 socket = proxy_new_socket(account_name, s->account_id,
        s->login_session_id, s->ip, s->token);
    if (socket != INVALID_PROXY_SOCKET)
        return 0;
    tlmsg_player_select_shard_result_t r_msg;
    r_msg.result            = 1;
    r_msg.account_id        = s->account_id;
    r_msg.login_session_id  = s->login_session_id;
    bbuf_t bb = _send_const_crypt_msg_to_login_server(
        TLMSG_PLAYER_SELECT_SHARD_RESULT_SZ);
    if (!bb.max_bytes)
        return 3;
    tlmsg_player_select_shard_result_write(&bb, &_svchan.cryptchan, &r_msg);
    return 0;
}

static int
_handle_flmsg_player_cancel_select_shard(flmsg_player_cancel_select_shard_t *s)
{
    proxy_cancel_socket(s->account_id, s->login_session_id);
    return 0;
}
