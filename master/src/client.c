#include <inttypes.h>
#include <ctype.h>
#include "client.h"
#include "proxy.h"
#include "common.h"
#include "player.h"
#include "dynamic_object.h"
#include "new_player_spawn_point.h"
#include "pickup.h"
#include "data.h"
#include "drop.h"
#include "../../shared/common_defs.h"
#include "../../shared/common_utils.h"
#include "../../shared/rwbits.inl"
#include "../../shared/client_packets.h"
#include "../../shared/containers.h"
#include "../../shared/sv_time.h"
#include "../../shared/hashtable.h"
#include "../../shared/common_defs.h"
#include "../../shared/entities.h"

#define MAX_DB_QUERIES_PER_CLIENT   1
#define CL_BUF_SZ                   MUTA_MTU
#define CL_BUF_SZ                   MUTA_MTU
#define CLIENT_TIMEOUT              30000

enum cl_flag
{
    CL_FLAG_RESERVED        = (1 << 0),
    CL_FLAG_DISCONNECTING   = (1 << 1) /* Disconnecting at the end of frame */
};

typedef struct cl_timer_t cl_timer_t;

enum client_state
{
    CL_STATE_WAITING_FOR_CHARACTER_SELECTION_DATA,
    CL_STATE_CHARACTER_SELECTION,
    CL_STATE_FETCHING_CHARACTER_DATA,
    CL_STATE_ENTERING_WORLD,
    CL_STATE_IN_WORLD
};

struct client_t
{
    uint32              id; /* Runtime id used in async ops. */
    uint32              flags;
    uint32              timer_index; /* Timeout timer */
    uint32              proxy_socket_index;
    uint32              player_index; /* Relevant if state CL_STATE_IN_WORLD */
    enum client_state   state;
    cryptchan_t         cryptchan;
    wdbc_query_id_t     db_queries[MAX_DB_QUERIES_PER_CLIENT];
    struct
    {
        uint8   memory[CL_BUF_SZ];
        uint32  num;
    } in_buf;
    uint8 num_db_queries;
};

struct cl_timer_t
{
    client_t    *client;
    uint32      runtime_id;
    uint64      timeout;
};

static fixed_pool(client_t)     _clients;
static uint32                   _running_id;
static cl_timer_t               *_timeout_timers; /* darr */
static uint64                   _next_timeout_check;

static struct
{
    uint32 *items;
    uint32 num;
} _disconnected; /* Disconnects are deferred until the end of the frame */

static void
_check_timeouts(uint64 program_ticks_ms);

static void
_save_received_timestamp(client_t *cl, uint64 program_ticks_ms);

static int
_read_packet(client_t *cl, uint8 *pckt, int pckt_len);
/* Returns the amount of bytes left in the buffer after the read, or < 0 if
 * the client should be disconnected. */

static bool32
_try_execute_dot_cmd(client_t *cl, const char *msg, uint32 msg_len);
/* Returns true if a command was executed */

static int
_new_query(client_t *cl, wdbc_query_id_t query_id);
/* _new_query()
 * Database queries regarding specific clients are stored in the client struct.
 * This function attempts to store a query_id for the client, and returns
 * non-zero if there are too many queries pending. */

static void
_del_query(client_t *cl, wdbc_query_id_t query_id);

static void
_on_complete_query_player_character_list(wdbc_query_id_t query_id, void *user_data,
    int error, wdbc_player_character_t *characters, uint num_characters);

static void
_on_complete_query_create_player_character(wdbc_query_id_t query_id,
    void *user_data, int error, wdbc_player_character_t *character);

static void
_on_complete_query_player_character(wdbc_query_id_t query_id, void *user_data,
    int error, wdbc_player_character_t *character,
    wdbc_dynamic_object_in_db_t *items, uint32 num_items);

static int
_handle_clmsg_create_character(client_t *cl, clmsg_create_character_t *s);

static int
_handle_clmsg_enter_world(client_t *cl, clmsg_enter_world_t *s);

static int
_handle_clmsg_chat_message(client_t *cl, clmsg_chat_message_t *s);

static int
_handle_clmsg_move_in_direction(client_t *cl, clmsg_move_in_direction_t *s);

static int
_handle_clmsg_find_path(client_t *cl, clmsg_find_path_t *s);

static int
_handle_clmsg_emote_at_player(client_t *cl, clmsg_emote_at_player_t *s);

static int
_handle_clmsg_pick_up_dynamic_object(client_t *cl,
    clmsg_pick_up_dynamic_object_t *s);

static int
_handle_clmsg_move_inventory_item(client_t *cl, clmsg_move_inventory_item_t *s);

static int
_handle_clmsg_drop_inventory_item(client_t *cl, clmsg_drop_inventory_item_t *s);

static int
_handle_clmsg_use_ability(client_t *cl, clmsg_use_ability_t *s);

static int
_handle_clmsg_stop_ability_charge(client_t *cl);

int
cl_init(void)
{
    if (!com_config.max_clients)
    {
        LOG_ERROR("Max clients must be > 0!");
        return 1;
    }
    fixed_pool_init(&_clients, com_config.max_clients);
    darr_reserve(_timeout_timers, com_config.max_clients);
    _disconnected.items = emalloc(com_config.max_clients * sizeof(uint32));
    _disconnected.num   = 0;
    _next_timeout_check = get_program_ticks_ms();
    _running_id         = 0;
    return 0;
}

void
cl_destroy(void)
{
    fixed_pool_destroy(&_clients);
    darr_free(_timeout_timers);
}

client_t *
cl_new(uint32 proxy_socket_index, uint8 *wx, uint8 *rx, uint8 *sk, uint8 *pk,
    uint8 *read_key, uint8 *read_nonce, uint8 *write_key, uint8 *write_nonce)
{
    int err = 0;
    client_t *cl = fixed_pool_new(&_clients);
    if (!cl)
        return 0;
    uint32 runtime_id = _running_id++;
    cl->flags                   = CL_FLAG_RESERVED;
    cl->id                      = runtime_id;
    cl->state                   = CL_STATE_WAITING_FOR_CHARACTER_SELECTION_DATA;
    cl->in_buf.num              = 0;
    cl->num_db_queries          = 0;
    cl->proxy_socket_index      = proxy_socket_index;
    cryptchan_init_from_keys(&cl->cryptchan, wx, rx, sk, pk, read_key,
        read_nonce, write_key, write_nonce);
    cl->timer_index = darr_num(_timeout_timers);
    cl_timer_t timer =
    {
        .client     = cl,
        .runtime_id = runtime_id,
        .timeout    = get_program_ticks_ms() + CLIENT_TIMEOUT
    };
    darr_push(_timeout_timers, timer);
    wdbc_query_id_t query_id = wdbc_query_player_character_list(
        _on_complete_query_player_character_list, cl,
        proxy_socket_get_account_id(cl->proxy_socket_index));
    if (_new_query(cl, query_id))
        {err = 2; goto fail;}
    LOG("New client with runtime ID %u created.", runtime_id);
    return cl;
    fail:
        LOG_ERROR("failed, code %d\n", err);
        cl_disconnect(cl);
        return 0;
}

void
cl_disconnect(client_t *cl)
{
    muta_assert(cl->flags & CL_FLAG_RESERVED);
    if (cl->flags & CL_FLAG_DISCONNECTING)
        return;
    cl->flags |= CL_FLAG_DISCONNECTING;
    uint32 num_disconnected = _disconnected.num++;
    _disconnected.items[num_disconnected] = fixed_pool_index(&_clients, cl);
    if (cl->state == CL_STATE_IN_WORLD)
    {
        /* Despawn player character if we can. Otherwise, set it's client to 0.
         * */
        player_t *pl = pl_get(cl->player_index);
        if (pl->num_unfinished_async_actions == 0)
            pl_request_despawn(pl_get(cl->player_index));
        else
            pl->client = 0;
    }
    proxy_delete_socket(cl->proxy_socket_index);
    for (uint32 i = 0; i < (uint32)cl->num_db_queries; ++i)
        wdbc_cancel_query(cl->db_queries[i]);
    if (!num_disconnected)
    {
        com_event_t event;
        event.type = COM_EVENT_DISCONNECT_CLIENTS;
        com_push_events_no_wait(&event, 1);
        LOG("Disconnecting client id %u.", cl->id);
    }
}

void
cl_disconnect_for_illegal_action(client_t *cl)
{
    LOG_DEBUG_EXT("Player did something illegal.");
    cl_disconnect(cl);
}

void
cl_read(client_t *client, const void *memory, int num_bytes)
{
    muta_assert(client->flags & CL_FLAG_RESERVED);
    int num_read = 0;
    while (num_read < num_bytes)
    {
        int num_to_read = MIN(num_bytes - num_read,
            CL_BUF_SZ - (int)client->in_buf.num);
        memcpy(client->in_buf.memory + client->in_buf.num,
            (uint8*)memory + num_read, num_to_read);
        client->in_buf.num += num_to_read;
        int bytes_left = _read_packet(client,
            client->in_buf.memory, client->in_buf.num);
        if (bytes_left < 0)
        {
            cl_disconnect(client);
            break;
        }
        memmove(client->in_buf.memory,
            client->in_buf.memory + client->in_buf.num - bytes_left,
            bytes_left);
        client->in_buf.num = bytes_left;
        if (client->in_buf.num == CL_BUF_SZ)
        {
            cl_disconnect(client);
            break;
        }
        num_read += num_to_read;
    }
}

bbuf_t
cl_send(client_t *cl, uint32 size)
{
    if (cl->flags & CL_FLAG_DISCONNECTING)
    {
        bbuf_t ret = {0};
        return ret;
    }
    return proxy_forward_msg(cl->proxy_socket_index, sizeof(clmsg_t) + size);
}
/* Not disconnecting the client here in case of failure because the proxy
 * will module will do it automatically. */

bbuf_t
cl_send_const_encrypted(client_t *cl, uint32 size)
    {return cl_send(cl, CRYPT_MSG_ADDITIONAL_BYTES + size);}

bbuf_t
cl_send_var_encrypted(client_t *cl, uint32 size)
    {return cl_send(cl, sizeof(msg_sz_t) + CRYPT_MSG_ADDITIONAL_BYTES + size);}

void
cl_update(void)
    {_check_timeouts(get_program_ticks_ms());}

cryptchan_t *
cl_get_cryptchan(client_t *cl)
    {return &cl->cryptchan;}

void
cl_on_spawn_player_failed(client_t *cl)
{
    muta_assert(cl->state == CL_STATE_ENTERING_WORLD);
    cl->state = CL_STATE_CHARACTER_SELECTION;
}

void
cl_complete_disconnects(void)
{
    uint32 num_disconnected = _disconnected.num;
    for (uint32 i = 0; i < num_disconnected; ++i)
    {
        client_t *cl = &_clients.all[_disconnected.items[i]];
        /* Swap the last timer in the array with the timer of this one. */
        muta_assert(_darr_head(_timeout_timers)->num);
        uint32 last = --_darr_head(_timeout_timers)->num;
        if (last != cl->timer_index)
        {
            _timeout_timers[cl->timer_index] = _timeout_timers[last];
            _timeout_timers[cl->timer_index].client->timer_index =
                cl->timer_index;
        }
        fixed_pool_free(&_clients, cl);
        cl->flags &= ~CL_FLAG_RESERVED;
    }
    num_disconnected = 0;
}

void
cl_confirm_entered_world(client_t *cl)
{
    muta_assert(cl->state == CL_STATE_ENTERING_WORLD);
    cl->state = CL_STATE_IN_WORLD;
}

static void
_check_timeouts(uint64 program_ticks_ms)
{
    if (program_ticks_ms < _next_timeout_check)
        return;
    uint64 next_timeout_check = _next_timeout_check + CLIENT_TIMEOUT;
    cl_timer_t  *timers     = _timeout_timers;
    uint32      num_timers  = darr_num(timers);
    uint64      timeout;
    cl_timer_t  *timer;
    for (uint32 i = 0; i < num_timers; ++i)
    {
        timer   = &timers[i];
        timeout = timer->timeout;
        if (program_ticks_ms >= timeout)
        {
            LOG("client %u timed out. %" PRIu64 ", %" PRIu64 ".",
                timer->client->id, timeout, program_ticks_ms);
            cl_disconnect(timer->client);
        } else if (timeout < next_timeout_check)
            next_timeout_check = timeout;
    }
    _next_timeout_check = next_timeout_check;
}

static void
_save_received_timestamp(client_t *cl, uint64 program_ticks_ms)
{
    muta_assert(cl->flags & CL_FLAG_RESERVED);
    cl_timer_t  *timer      = &_timeout_timers[cl->timer_index];
    uint64      addition    = CLIENT_TIMEOUT;
    uint64      timeout     = program_ticks_ms + addition;
    timer->timeout = program_ticks_ms + addition;
    if (timeout < _next_timeout_check)
        _next_timeout_check = timeout;
}

/* Returns the amount of bytes left in the buffer after the read, or < 0 if
 * the client should be disconnected. */
static int
_read_packet(client_t *cl, uint8 *pckt, int pckt_len)
{
    byte_buf_t  bb          = BBUF_INITIALIZER(pckt, pckt_len);
    int         incomplete  = 0;
    int         dc          = 0;
    clmsg_t     msg_type;
    while (BBUF_FREE_SPACE(&bb) >= sizeof(clmsg_t))
    {
        BBUF_READ(&bb, &msg_type);
        switch (msg_type)
        {
        case CLMSG_KEEP_ALIVE:
            break;
        case CLMSG_CREATE_CHARACTER:
        {
            clmsg_create_character_t s;
            incomplete = clmsg_create_character_read(&bb, &s);
            if (!incomplete)
                dc = _handle_clmsg_create_character(cl, &s);
        }
            break;
        case CLMSG_ENTER_WORLD:
        {
            clmsg_enter_world_t s;
            incomplete = clmsg_enter_world_read(&bb, &s);
            if (!incomplete)
                dc = _handle_clmsg_enter_world(cl, &s);
        }
            break;
        case CLMSG_CHAT_MESSAGE:
        {
            clmsg_chat_message_t s;
            incomplete = clmsg_chat_message_read(&bb, &s);
            if (!incomplete)
                dc = _handle_clmsg_chat_message(cl, &s);
        }
            break;
        case CLMSG_MOVE_IN_DIRECTION:
        {
            clmsg_move_in_direction_t s;
            incomplete = clmsg_move_in_direction_read(&bb, &s);
            if (!incomplete)
                dc = _handle_clmsg_move_in_direction(cl, &s);
        }
            break;
        case CLMSG_FIND_PATH:
        {
            clmsg_find_path_t s;
            incomplete = clmsg_find_path_read(&bb, &s);
            if (!incomplete)
                dc = _handle_clmsg_find_path(cl, &s);
        }
            break;
        case CLMSG_EMOTE_AT_PLAYER:
        {
            clmsg_emote_at_player_t s;
            incomplete = clmsg_emote_at_player_read(&bb, &s);
            if (!incomplete)
                dc = _handle_clmsg_emote_at_player(cl, &s);
        }
            break;
        case CLMSG_PICK_UP_DYNAMIC_OBJECT:
        {
            clmsg_pick_up_dynamic_object_t s;
            incomplete = clmsg_pick_up_dynamic_object_read(&bb, &s);
            if (!incomplete)
                dc = _handle_clmsg_pick_up_dynamic_object(cl, &s);
        }
            break;
        case CLMSG_MOVE_INVENTORY_ITEM:
        {
            clmsg_move_inventory_item_t s;
            incomplete = clmsg_move_inventory_item_read(&bb, &s);
            if (!incomplete)
                dc = _handle_clmsg_move_inventory_item(cl, &s);
        }
            break;
        case CLMSG_DROP_INVENTORY_ITEM:
        {
            clmsg_drop_inventory_item_t s;
            incomplete = clmsg_drop_inventory_item_read(&bb, &s);
            if (!incomplete)
                dc = _handle_clmsg_drop_inventory_item(cl, &s);
        }
            break;
        case CLMSG_USE_ABILITY:
        {
            clmsg_use_ability_t s;
            incomplete = clmsg_use_ability_read(&bb, &s);
            if (!incomplete)
                dc = _handle_clmsg_use_ability(cl, &s);
        }
            break;
        case CLMSG_STOP_ABILITY_CHARGE:
        {
            dc = _handle_clmsg_stop_ability_charge(cl);
            break;
        }
        }
        if (dc || incomplete < 0)
        {
            LOG_DEBUG_EXT("Bad message: type %d, dc %d, incomplete %d.",
                (int)msg_type, dc, incomplete);
            return -1;
        }
        if (incomplete)
        {
            bb.num_bytes -= sizeof(clmsg_t);
            break;
        }
    }
    _save_received_timestamp(cl, get_program_ticks_ms());
    return BBUF_FREE_SPACE(&bb);
}

static bool32
_try_execute_dot_cmd(client_t *cl, const char *msg, uint32 msg_len)
{
#if 0
    /* TODO: check gm level here */
    char buf[33]    = {0};
    char *dst       = buf;
    for (const char *c = cmd;
         dst < buf + 32 && *c && *c != ' ' && *c != '\t';
         ++c)
        *(dst++) = (char)tolower(*c);
    *dst = 0;
    if (streq(buf, ".teleport"))
    {
        int pos[3];
        if (sscanf(cmd, ".teleport %d %d %d", &pos[0], &pos[1], &pos[2]) == 3)
            w_teleport_player(cl_get_character_props(cl)->id, pos[0],
                pos[1], pos[2]);
        return 1;
    } else
    if (streq(buf, ".spawn"))
    {
        uint32  type_id;
        wpos_t  pos;
        if (sscanf(cmd + 6, " creature %u %d %d %c", &type_id, &pos.x, &pos.y,
            &pos.z) == 4)
            w_spawn_creature(0, type_id, pos, 0, 0);
        DEBUG_PRINTFF("spawning a creature.\n");
        return 1;
    } else
    if (streq(buf, ".reload_script"))
    {
        DEBUG_PRINTFF(".reload_script\n");
        const char *script_name = cmd + 14;
        while (*script_name && *script_name == ' ')
            script_name++;
        if (!*script_name)
            return 0;
        w_reload_entity_script(script_name);
        return 1;
    }
    /* TODO: send "invalid arguments" message if necessary */
#endif
    return 0;
}

static int
_new_query(client_t *cl, wdbc_query_id_t query_id)
{
    if (cl->num_db_queries == MAX_DB_QUERIES_PER_CLIENT)
    {
        wdbc_cancel_query(query_id);
        return 2;
    }
    cl->db_queries[cl->num_db_queries++] = query_id;
    return 0;
}

static void
_del_query(client_t *cl, wdbc_query_id_t query_id)
{
    uint32 num_queries = cl->num_db_queries;
    for (uint32 i = 0; i < num_queries; ++i)
        if (cl->db_queries[i] == query_id)
        {
            cl->db_queries[i] = cl->db_queries[--cl->num_db_queries];
            return;
        }
    muta_assert(0);
}

static void
_on_complete_query_player_character_list(wdbc_query_id_t query_id,
    void *user_data, int error, wdbc_player_character_t *characters,
    uint32 num_characters)
{
    client_t *cl = user_data;
    muta_assert(cl->flags & CL_FLAG_RESERVED);
    muta_assert(cl->state == CL_STATE_WAITING_FOR_CHARACTER_SELECTION_DATA);
    _del_query(cl, query_id);
    if (error)
    {
        LOG("Query account characters failed.");
        cl_disconnect(cl);
        return;
    }
    cl->state = CL_STATE_CHARACTER_SELECTION;
    /* Send the characters */
    svmsg_character_list_t s =
    {
        .characters.len = (uint8)num_characters
    };
    for (uint32 i = 0; i < num_characters; ++i)
    {
        svmsg_character_list_item_t *c = &s.characters.data[i];
        c->id           = characters[i].id;
        c->name.len     = (uint8)strlen(characters[i].name);
        c->race         = characters[i].race;
        c->sex          = characters[i].sex;;
        memcpy(c->name.data, characters[i].name, c->name.len);
    }
    bbuf_t bb = cl_send(cl, svmsg_character_list_compute_sz(&s));
    if (!bb.max_bytes)
        return;
    svmsg_character_list_write(&bb, &s);
    LOG_DEBUG_EXT("Completed query for player character list, set client state "
        "to CL_STATE_CHARACTER_SELECTION.");
}

static void
_on_complete_query_create_player_character(wdbc_query_id_t query_id,
    void *user_data, int error, wdbc_player_character_t *character)
{
    client_t *cl = user_data;
    muta_assert(cl->flags & CL_FLAG_RESERVED);
    _del_query(cl, query_id);
    if (error)
    {
        LOG_ERROR("Character creation for player failed.");
        svmsg_create_character_fail_t s =
        {
            .reason = CREATE_CHARACTER_FAIL_UNKNOWN
        };
        bbuf_t bb = cl_send(cl, SVMSG_CREATE_CHARACTER_FAIL_SZ);
        if (!bb.max_bytes)
        {
            cl_disconnect(cl);
            return;
        };
        svmsg_create_character_fail_write(&bb, &s);
        return;
    }
    svmsg_new_character_created_t s =
    {
        .id         = character->id,
        .name.len   = (uint8)strlen(character->name),
        .race       = character->race,
        .sex        = character->sex
    };
    memcpy(s.name.data, character->name, s.name.len);
    bbuf_t bb = cl_send(cl, svmsg_new_character_created_compute_sz(&s));
    if (!bb.max_bytes)
    {
        cl_disconnect(cl);
        return;
    }
    svmsg_new_character_created_write(&bb, &s);
}

static void
_on_complete_query_player_character(wdbc_query_id_t query_id, void *user_data,
    int error, wdbc_player_character_t *character,
    wdbc_dynamic_object_in_db_t *items, uint32 num_items)
{
    client_t *cl = user_data;
    _del_query(cl, query_id);
    muta_assert(cl->state == CL_STATE_FETCHING_CHARACTER_DATA);
    if (error)
    {
        cl_disconnect(cl);
        return;
    }
    LOG_DEBUG_EXT("Successfully finished querying player character, set state "
        "to CL_STATE_ENTERING_WORLD.");
    int position[3] = {character->x, character->y, character->z};
    player_t *player = pl_request_spawn(cl, character->id, character->name,
        character->race, character->sex, character->instance_id, position,
        character->direction, ENT_BASE_MOVE_SPEED, items, num_items);
    if (!player) /* pl_request_spawn() sends error msg. */
    {
        cl->state = CL_STATE_CHARACTER_SELECTION;
        return;
    }
    cl->player_index    = pl_get_index(player);
    cl->state           = CL_STATE_ENTERING_WORLD;
}

static int
_handle_clmsg_create_character(client_t *cl, clmsg_create_character_t *s)
{
    if (cl->state != CL_STATE_CHARACTER_SELECTION)
        return 1;
    LOG_DEBUG_EXT("FIXME: this function isn't fully implemented.");
    player_race_def_t *race = ent_get_player_race_def(s->race);
    if (!race)
    {
        LOG_DEBUG_EXT("Invalid race.");
        return 2;
    }
    char name[MAX_CHARACTER_NAME_LEN + 1];
    memcpy(name, s->name.data, s->name.len);
    name[s->name.len] = 0;
    DEBUG_PRINTFF("Attempting to create character %s.\n", name);
    new_player_spawn_point_t *spawn_point = new_player_spawn_points_find(
        race->start_area_id);
    muta_assert(spawn_point);
    uint32 INSTANCE_ID_FIXME = 0;
    wdbc_dynamic_object_t items[] =
    {
        {
            .type_id = data_config.player_starter_bag_dobj_type_id,
            .owner_type = WDBC_DYNAMIC_OBJECT_OWNER_PLAYER_EQUIPPED,
            .owner.player_equipped =
            {
                .equipment_slot = EQUIPMENT_SLOT_BAG1,
            }
        }
    };
    wdbc_query_id_t query_id = wdbc_query_create_player_character(
        _on_complete_query_create_player_character, cl,
        proxy_socket_get_account_id(cl->proxy_socket_index), name, s->race,
        s->sex, INSTANCE_ID_FIXME, spawn_point->position, ISODIR_SOUTH_EAST,
        items, (uint32)(sizeof(items) / sizeof(items[0])));
    if (_new_query(cl, query_id))
    {
        svmsg_create_character_fail_t s;
        s.reason = CREATE_CHARACTER_FAIL_DB_BUSY;
        bbuf_t bb = cl_send(cl, SVMSG_CREATE_CHARACTER_FAIL_SZ);
        if (!bb.max_bytes)
            return 4;
        svmsg_create_character_fail_write(&bb, &s);
        LOG_EXT("Character creation failed!");
    }
    return 0;
}

static int
_handle_clmsg_enter_world(client_t *cl, clmsg_enter_world_t *s)
{
    if (cl->state != CL_STATE_CHARACTER_SELECTION)
        return 1;
    cl->state = CL_STATE_FETCHING_CHARACTER_DATA;
    wdbc_query_id_t query_id = wdbc_query_player_character(
        _on_complete_query_player_character, cl,
        proxy_socket_get_account_id(cl->proxy_socket_index), s->character_id);
    if (query_id == WDBC_INVALID_QUERY_ID)
    {
        LOG_ERROR("wdbc_query_player_character() failed.");
        return 2;
    }
    if (_new_query(cl, query_id))
    {
        LOG_ERROR("Could not create a new query.");
        return 3;
    }
    LOG_DEBUG_EXT("Player attempting to log in, set client state to "
        "CL_STATE_FETCHING_CHARACTER_DATA.");
    return 0;
}

static int
_handle_clmsg_chat_message(client_t *cl, clmsg_chat_message_t *s)
{
    if (cl->state != CL_STATE_IN_WORLD)
        return 1;
    if (!is_chat_msg_legaln(s->message.data, s->message.len))
        return 0;
    if (s->message.data[0] == '.' &&
        _try_execute_dot_cmd(cl, s->message.data, s->message.len))
        return 0;
    pl_request_chat_message(pl_get(cl->player_index), s->message.data,
        s->message.len);
    return 0;
}

static int
_handle_clmsg_move_in_direction(client_t *cl, clmsg_move_in_direction_t *s)
{
    if (cl->state != CL_STATE_IN_WORLD)
        return 1;
    pl_request_move_in_direction(pl_get(cl->player_index), s->direction);
    return 0;
}

static int
_handle_clmsg_find_path(client_t *cl, clmsg_find_path_t *s)
{
    if (cl->state != CL_STATE_IN_WORLD)
        return 1;
    pl_request_find_path(pl_get(cl->player_index), s->x, s->y, s->z);
    return 0;
}

static int
_handle_clmsg_emote_at_player(client_t *cl, clmsg_emote_at_player_t *s)
{
    if (cl->state != CL_STATE_IN_WORLD)
        return 1;
    pl_request_emote(pl_get(cl->player_index), s->emote_id);
    return 0;
}

static int
_handle_clmsg_pick_up_dynamic_object(client_t *cl,
    clmsg_pick_up_dynamic_object_t *s)
{
    if (cl->state != CL_STATE_IN_WORLD)
    {
        LOG_DEBUG_EXT("Client not in world.");
        return 1;
    }
    dynamic_object_t *dobj = dobj_find(s->dobj_runtime_id);
    if (!dobj)
    {
        LOG_DEBUG_EXT("Dynamic object %u not found.", s->dobj_runtime_id);
        return 2;
    }
    player_t *pl = pl_get(cl->player_index);
    if (pickup_request_player_pick_up_dynamic_object(pl, dobj,
        s->equipment_slot, s->x_in_bag, s->y_in_bag))
    {
        LOG_DEBUG_EXT("pickup_request_player_pick_up_dynamic_object() failed.");
        return 3;
    }
    return 0;
}

static int
_handle_clmsg_move_inventory_item(client_t *cl, clmsg_move_inventory_item_t *s)
{
    if (cl->state != CL_STATE_IN_WORLD)
    {
        LOG_DEBUG_EXT("Client not in world.");
        return 1;
    }
    if (pl_request_move_inventory_item(pl_get(cl->player_index),
        s->dobj_runtime_id, s->new_equipment_slot, s->new_x, s->new_y))
    {
        LOG_DEBUG_EXT("pl_request_move_inventory_item() failed.");
        return 2;
    }
    return 0;
}

static int
_handle_clmsg_drop_inventory_item(client_t *cl, clmsg_drop_inventory_item_t *s)
{
    if (cl->state != CL_STATE_IN_WORLD)
    {
        LOG_DEBUG_EXT("Client not in world.");
        return 1;
    }
    if (drop_request_player_drop_item(pl_get(cl->player_index),
        s->dobj_runtime_id, s->x, s->y, s->z))
    {
        LOG_DEBUG_EXT("pl_request_drop_item() failed.");
        return 2;
    }
    return 0;
}

static int
_handle_clmsg_use_ability(client_t *cl, clmsg_use_ability_t *s)
{
    if (cl->state != CL_STATE_IN_WORLD)
    {
        LOG_DEBUG_EXT("Client not in world.");
        return 1;
    }
    player_t *pl = pl_get(cl->player_index);
    if (pl_request_use_ability(pl, s->ability_id, s->target_type, s->target_id))
    {
        LOG_DEBUG_EXT("pl_request_use_ability() failed.");
        return 2;
    }
    return 0;
}

static int
_handle_clmsg_stop_ability_charge(client_t *cl)
{
    if (cl->state != CL_STATE_IN_WORLD)
    {
        LOG_DEBUG_EXT("Client not in world.");
        return 1;
    }
    player_t *pl = pl_get(cl->player_index);
    if (pl_request_stop_ability_charge(pl))
    {
        LOG_DEBUG_EXT("pl_request_stop_ability_charge() failed.");
        return 2;
    }
    return 0;
}
