#ifndef MUTA_MASTER_DROP_H
#define MUTA_MASTER_DROP_H

#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct player_t         player_t;
typedef struct dynamic_object_t dynamic_object_t;

int
drop_request_player_drop_item(player_t *pl, dobj_runtime_id_t dobj_runtime_id,
    int x, int y, int z);
/* Returns 0 if the player did something that was not allowed. */

int
drop_on_sim_confirmed_player_drop_item(dynamic_object_t *dobj,
    uint32 sim_index, int32 x, int32 y, uint8 z, uint8 direction);

int
drop_on_sim_failed_player_drop_item(dynamic_object_t *dobj, player_t *pl,
    bool32 disconnect_player);
/* Assumes it has been checked that player owns the object. */

#endif /* MUTA_MASTER_DROP_H */
