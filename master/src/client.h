/* client.h
 * Client state storage. */

#ifndef MUTA_CLIENT_H
#define MUTA_CLIENT_H

#include "../../shared/common_utils.h"

/* Forward declaration(s) */
typedef struct cryptchan_t cryptchan_t;

/* Types defined here */
typedef struct client_t client_t;

int
cl_init(void);

void
cl_destroy(void);

client_t *
cl_new(uint32 proxy_socket_index, uint8 *wx, uint8 *rx, uint8 *sk, uint8 *pk,
    uint8 *read_key, uint8 *read_nonce, uint8 *write_key, uint8 *write_nonce);
/* cl_new()
 * Create a new client instance for a new connection. Normally called when a
 * proxy informs the master of a new client.
 * wx, rx, sk, pk, read_key, read_nonce, write_key, write_nonce are cryptchan
 * member variables sent by the proxy, as the proxy has already negotiated an
 * encryption scheme with the client. */

void
cl_disconnect(client_t *cl);

void
cl_disconnect_for_illegal_action(client_t *cl);

void
cl_read(client_t *client, const void *memory, int num_bytes);
/* Read an incoming packet. Normally called by the proxy module. */

bbuf_t
cl_send(client_t *cl, uint32 size);

bbuf_t
cl_send_const_encrypted(client_t *cl, uint32 size);

bbuf_t
cl_send_var_encrypted(client_t *cl, uint32 size);

void
cl_update(void);

cryptchan_t *
cl_get_cryptchan(client_t *cl);

void
cl_on_spawn_player_failed(client_t *cl);

/* Event handlers */

void
cl_complete_disconnects(void);

void
cl_confirm_entered_world(client_t *cl);

#endif /* MUTA_CLIENT_H */
