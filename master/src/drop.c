#include <math.h>
#include "drop.h"
#include "dynamic_object.h"
#include "player.h"
#include "instance.h"
#include "common.h"
#include "sim_server.h"
#include "client.h"
#include "../../shared/sim_packets.h"
#include "../../shared/client_packets.h"

typedef struct request_data_t request_data_t;

struct request_data_t
{
    uint32                  dobj_index;
    instance_runtime_id_t   instance_id;
    uint32                  sim_index;
    int32                   x;
    int32                   y;
    uint8                   z;
    uint8                   direction;
};

static void
_on_completed_query_remove_dynamic_object_from_player_container(
    wdbc_query_id_t query_id, void *user_data, int error, uuid16_t dobj_db_id);

int
drop_request_player_drop_item(player_t *pl, dobj_runtime_id_t dobj_runtime_id,
    int x, int y, int z)
{
    int x_diff = abs(x - pl->position[0]);
    int y_diff = abs(y - pl->position[1]);
    int z_diff = z - pl->position[2];
    int distance = (int)roundf(
        sqrtf((float)(x_diff * x_diff + y_diff * y_diff)));
    if (distance > MAX_ITEM_THROW_DISTANCE || z_diff > MAX_ITEM_THROW_HEIGHT)
    {
        LOG_ERROR("Player cannot drop dynamic object: target tile too far "
            "away.");
        return 1;
    }
    dynamic_object_t *dobj = dobj_find(dobj_runtime_id);
    if (!dobj)
    {
        LOG_ERROR("Player cannot drop dynamic object: dynamic object %u not "
            "found.", dobj_runtime_id);
        return 1;
    }
    if (dobj->flags & DOBJ_FLAG_UNCONFIRMED_DB_STATE)
    {
        LOG_ERROR("Player cannot drop dynamic object: object has unconfirmed "
            "db state.");
    }
    if (dobj_get_owning_player(dobj) != pl)
    {
        LOG_ERROR("Player cannot drop dynamic object: dynamic object %u not "
            "owned by player.", dobj_runtime_id);
        return 1;
    }
    muta_assert(!dobj_is_in_world(dobj));
    muta_assert((dobj->flags & DOBJ_FLAG_IN_CONTAINER) ||
        (dobj->flags & DOBJ_FLAG_EQUIPPED));
    if (dobj->flags & DOBJ_FLAG_UNCONFIRMED_DB_STATE)
    {
        LOG_ERROR("Can't drop object: dynamic object has unconfirmed db "
            "state.");
        return 1;
    }
    if (dobj->flags & DOBJ_FLAG_UNCONFIRMED_SIM_STATE)
    {
        LOG_ERROR("Can't drop object: dynamic object has unconfirmed sim "
            "state.");
        return 1;
    }
    instance_t      *inst = inst_get(pl->instance_index);
    instance_part_t *part = inst_find_part_by_position(inst, x, y);
    uint32 sim_index = part->sim_index;
    if (sim_index == SIM_INVALID_INDEX)
    {
        LOG_ERROR("Can't drop dynamic object: instance has no sim server.");
        svmsg_drop_inventory_item_fail_t s =
        {
            .dobj_runtime_id = dobj_runtime_id
        };
        bbuf_t bb = cl_send(pl->client, SVMSG_DROP_INVENTORY_ITEM_FAIL_SZ);
        if (bb.max_bytes)
            svmsg_drop_inventory_item_fail_write(&bb, &s);
        return 0;
    }
    /* This spawns the object in an inactive state. */
    tsimmsg_request_player_drop_dynamic_object_t s =
    {
        .dobj_runtime_id    = dobj->runtime_id,
        .player_runtime_id  = pl->world_session_id,
        .type_id            = dobj->type_id,
        .instance_id        = inst->id,
        .x                  = x,
        .y                  = y,
        .z                  = (uint8)z,
        .direction          = (uint8)ISODIR_NORTH
    };
    bbuf_t bb = sim_send(part->sim_index,
        TSIMMSG_REQUEST_PLAYER_DROP_DYNAMIC_OBJECT_SZ);
    if (!bb.max_bytes)
        return 1;
    tsimmsg_request_player_drop_dynamic_object_write(&bb, &s);
    dobj->flags |= DOBJ_FLAG_UNCONFIRMED_SIM_STATE;
    return 0;
}

int
drop_on_sim_confirmed_player_drop_item(dynamic_object_t *dobj,
    uint32 sim_index, int32 x, int32 y, uint8 z, uint8 direction)
{
    muta_assert(dobj->flags & DOBJ_FLAG_UNCONFIRMED_SIM_STATE);
    player_t *pl = dobj_get_owning_player(dobj);
    muta_assert(pl);
    request_data_t *user_data = segpool_malloc(&com_main_thread_segpool,
        sizeof(request_data_t));
    user_data->dobj_index   = dobj_get_index(dobj);
    user_data->instance_id  = inst_get(pl->instance_index)->id;
    user_data->sim_index    = sim_index;
    user_data->x            = x;
    user_data->y            = y;
    user_data->z            = z;
    user_data->direction    = direction;
    wdbc_query_id_t query_id =
        wdbc_query_remove_dynamic_object_from_player_container(
            _on_completed_query_remove_dynamic_object_from_player_container,
            user_data, dobj->db_uuid);
    if (query_id == WDBC_INVALID_QUERY_ID)
    {
        LOG_ERROR("wdbc_query_remove_dynamic_object_from_player_container() "
            "failed.");
        return 1;
    }
    dobj->flags &= ~DOBJ_FLAG_UNCONFIRMED_SIM_STATE;
    dobj->flags |= DOBJ_FLAG_UNCONFIRMED_DB_STATE;
    return 0;
}

int
drop_on_sim_failed_player_drop_item(dynamic_object_t *dobj, player_t *pl,
    bool32 disconnect_player)
{
    dobj->flags &= ~DOBJ_FLAG_UNCONFIRMED_SIM_STATE;
    if (disconnect_player)
    {
        cl_disconnect(pl->client);
        return 0;
    }
    svmsg_drop_inventory_item_fail_t s =
    {
        .dobj_runtime_id = dobj->runtime_id
    };
    bbuf_t bb = cl_send(pl->client, SVMSG_DROP_INVENTORY_ITEM_FAIL_SZ);
    if (bb.max_bytes)
        svmsg_drop_inventory_item_fail_write(&bb, &s);
    return 0;
}

static void
_on_completed_query_remove_dynamic_object_from_player_container(
    wdbc_query_id_t query_id, void *user_data, int error, uuid16_t dobj_db_id)
{
    request_data_t request_data = *(request_data_t*)user_data;
    segpool_free(&com_main_thread_segpool, user_data);
    if (error)
    {
        LOG_ERROR("Query failed with error %d.", error);
        return;
    }
    instance_t *inst = inst_find(request_data.instance_id);
    if (!inst)
    {
        LOG_DEBUG("Completed query to remove object from player container to "
            "drop it in the world, but instance went down during query.",
            error);
        return;
    }
    dynamic_object_t *dobj = dobj_get(request_data.dobj_index);
    if (!dobj)
    {
        LOG_DEBUG("Sim went down during query.");
        return;
    }
    player_t *pl = dobj_get_owning_player(dobj);
    muta_assert(pl);
    muta_assert(!memcmp(&dobj->db_uuid, &dobj_db_id, sizeof(dobj_db_id)));
    dobj->flags &= ~DOBJ_FLAG_UNCONFIRMED_DB_STATE;
    dobj->flags &= ~DOBJ_FLAG_IN_CONTAINER;
    dobj->flags &= ~DOBJ_FLAG_EQUIPPED;
    dobj->in.world.direction = request_data.direction;
    dobj->in.world.sim_index = request_data.sim_index;
    dobj_on_enter_new_instance(dobj, inst, request_data.x, request_data.y,
        request_data.z);
    tsimmsg_make_dynamic_object_active_t s1 =
    {
        .dobj_runtime_id = dobj->runtime_id
    };
    bbuf_t bb = sim_send(dobj->in.world.sim_index,
        TSIMMSG_MAKE_DYNAMIC_OBJECT_ACTIVE_SZ);
    if (bb.max_bytes)
        tsimmsg_make_dynamic_object_active_write(&bb, &s1);
    /* Send messages to surrounding sims to spawn an unowned object. */
    uint32 sims[8];
    uint32 num_sims = dobj_get_relevant_noncontrolling_sims(dobj, sims);
    tsimmsg_spawn_noncontrolled_dynamic_object_t s2 =
    {
        .runtime_id  = dobj->runtime_id,
        .type_id     = dobj->type_id,
        .instance_id = inst->id,
        .x           = dobj->in.world.position[0],
        .y           = dobj->in.world.position[1],
        .z           = (uint8)dobj->in.world.position[2],
        .direction   = dobj->in.world.direction
    };
    for (uint32 i = 0; i < num_sims; ++i)
    {
        bbuf_t bb = sim_send(sims[i],
            TSIMMSG_SPAWN_NONCONTROLLED_DYNAMIC_OBJECT_SZ);
        if (bb.max_bytes)
            tsimmsg_spawn_noncontrolled_dynamic_object_write(&bb, &s2);
    }
    svmsg_drop_inventory_item_success_t s3 =
    {
        .dobj_runtime_id = dobj->runtime_id
    };
    bb = cl_send(pl->client, SVMSG_DROP_INVENTORY_ITEM_SUCCESS_SZ);
    if (bb.max_bytes)
        svmsg_drop_inventory_item_success_write(&bb, &s3);
}
