#include "dynamic_object.h"
#include "common.h"
#include "instance.h"
#include "sim_server.h"
#include "player.h"
#include "client.h"
#include "../../shared/common_utils.h"
#include "../../shared/entities.h"
#include "../../shared/hashtable.h"
#include "../../shared/sim_packets.h"
#include "../../shared/client_packets.h"

typedef struct dynamic_object_t dynamic_object_t;

static fixed_pool(dynamic_object_t)                     _pool;
static hashtable(dobj_runtime_id_t, uint32)   _table;
static dobj_runtime_id_t                      _running_runtime_id;

static dobj_runtime_id_t
_next_runtime_id(void);

static dynamic_object_t *
_create_dobj(dobj_type_id_t type_id, dynamic_object_def_t **def);

int
dobj_init(void)
{
    fixed_pool_init(&_pool, com_config.max_dynamic_objects);
    hashtable_einit(_table,
        (uint32)((float)com_config.max_dynamic_objects / 7.f * 10.f));
    return 0;
}

void
dobj_destroy(void)
{
    hashtable_destroy(_table, 0);
    fixed_pool_destroy(&_pool);
}

dynamic_object_t *
dobj_request_spawn(dobj_type_id_t type_id, uint32 instance_id,
    int position[3], int direction)
{
    instance_t *instance = inst_find(instance_id);
    if (!instance)
    {
        LOG_ERROR("Cannot spawn dynamic object: too many already spawned.");
        return 0;
    }
    instance_part_t *part = inst_find_part_by_position(instance, position[0],
        position[1]);
    uint32 sim_index = part->sim_index;
    if (sim_index == SIM_INVALID_INDEX)
    {
        LOG_ERROR("Can't spawn dynamic object: instance has no sim server.");
        return 0;
    }
    dynamic_object_def_t *def;
    dynamic_object_t *obj = _create_dobj(type_id, &def);
    if (!obj)
        return 0;
    /* in.world properties */
    for (int i = 0; i < 3; ++i)
        obj->in.world.position[i] = position[i];
    obj->in.world.direction             = (uint8)direction;
    obj->in.world.instance_index        = inst_get_index(instance);
    obj->in.world.sim_index             = sim_index;
    obj->in.world.interest_area_index   = 0xFFFFFFFF;
    obj->in.world.interest_area_prev    = 0xFFFFFFFF;
    obj->in.world.interest_area_next    = 0xFFFFFFFF;
    tsimmsg_request_spawn_dynamic_object_t s =
    {
        .runtime_id     = obj->runtime_id,
        .type_id        = type_id,
        .instance_id    = instance->id,
        .x              = position[0],
        .y              = position[1],
        .z              = (uint8)position[2],
        .direction      = (uint8)direction
    };
    bbuf_t bb = sim_send(sim_index, TSIMMSG_REQUEST_SPAWN_DYNAMIC_OBJECT_SZ);
    if (!bb.max_bytes)
    {
        LOG_ERROR("Cannot spawn dynamic object: failed to send message to sim "
            "server.");
        goto fail;
    }
    tsimmsg_request_spawn_dynamic_object_write(&bb, &s);
    LOG("Requested spawning new dynamic object of type %u, runtime ID %u.",
        type_id, obj->runtime_id);
    return obj;
    fail:
        hashtable_erase(_table, obj->runtime_id,
            hashtable_hash(&obj->runtime_id, sizeof(obj->runtime_id)));
        fixed_pool_free(&_pool, obj);
        return 0;
}

void
dobj_despawn_container_on_owner_spawn_failed(dynamic_object_t *obj)
{
    if (obj->flags & DOBJ_FLAG_IN_CONTAINER)
        for (uint32 i = 0; i < obj->container.num_items; ++i)
        {
            dynamic_object_t *item_obj = dobj_get(
                obj->container.items[i].dobj_index);
            hashtable_erase(_table, item_obj->runtime_id,
                hashtable_hash(&item_obj->runtime_id,
                    sizeof(item_obj->runtime_id)));
            fixed_pool_free(&_pool, item_obj);
        }
    hashtable_erase(_table, obj->runtime_id,
        hashtable_hash(&obj->runtime_id, sizeof(obj->runtime_id)));
    fixed_pool_free(&_pool, obj);
}

int
dobj_confirm_spawn(dynamic_object_t *obj, uint32 instance_part_id, int x, int y,
    int z, int direction)
{
    if (obj->flags & DOBJ_FLAG_SPAWN_CONFIRMED)
    {
        LOG_ERROR("Sim server confirmed dynamic object spawn more than once!");
        return 1;
    }
    instance_part_t *part = inst_find_part_by_id(instance_part_id);
    if (!part)
    {
        LOG_ERROR("Cannot confirm spawn of dynamic object %u: instance part %u "
            "not found.", obj->runtime_id, instance_part_id);
        return 1;
    }
    if (obj->in.world.sim_index != part->sim_index)
    {
        LOG_ERROR("Sim confirmed spawn of dynamic object %u not owned by it!",
            obj->runtime_id);
        return 1;
    }
    obj->flags |= DOBJ_FLAG_SPAWN_CONFIRMED;
    obj->in.world.direction = direction;
    instance_t *inst = inst_get(part->instance_index);
    LOG("Sim confirmed spawn of dynamic object %u.", obj->runtime_id);
    dobj_on_enter_new_instance(obj, inst, x, y, z);
    return 0;
}

int
dobj_spawn_fail(dynamic_object_t *obj)
{
    if (obj->flags & DOBJ_FLAG_SPAWN_CONFIRMED)
    {
        LOG_ERROR("Sim server sent fail message for dynamic object that was "
            "already spawned successfully!");
        return 1;
    }
    LOG_ERROR("Sim server failed to spawn dynamic object %u.", obj->runtime_id);
    dobj_runtime_id_t runtime_id = obj->runtime_id;
    hashtable_erase(_table, runtime_id,
        hashtable_hash(&runtime_id, sizeof(runtime_id)));
    fixed_pool_free(&_pool, obj);
    return 0;
}

dynamic_object_t *
dobj_spawn_equipped_by_player(player_t *pl, uuid16_t db_uuid,
    dobj_type_id_t type_id, enum equipment_slot equipment_slot)
{
    dynamic_object_def_t    *def;
    dynamic_object_t        *obj = _create_dobj(type_id, &def);
    if (!obj)
        return 0;
    obj->flags |= DOBJ_FLAG_EQUIPPED;
    obj->in.equipped_by_player.player_index = pl_get_index(pl);
    obj->in.equipped_by_player.equipment_slot =
        (equipment_slot_id_t)equipment_slot;
    return obj;
}

dynamic_object_t *
dobj_spawn_in_container(dynamic_object_t *container_dobj, uuid16_t db_uuid,
    dobj_type_id_t type_id, uint8 x, uint8 y)
{
    dynamic_object_def_t    *def;
    dynamic_object_t        *obj = _create_dobj(type_id, &def);
    if (!obj)
        return 0;
    obj->db_uuid = db_uuid;
    obj->flags |= DOBJ_FLAG_IN_CONTAINER;
    obj->in.container.position[0]   = x;
    obj->in.container.position[1]   = y;
    obj->in.container.dobj_index    = dobj_get_index(container_dobj);
    container_item_t *item =
        &container_dobj->container.items[container_dobj->container.num_items++];
    item->dobj_index    = dobj_get_index(obj);
    item->x             = x;
    item->y             = y;
    item->w             = def->width_in_container;
    item->h             = def->height_in_container;
    LOG_DEBUG("Spawned dynamic object %u in player container.",
        obj->runtime_id);
    return obj;
}

dynamic_object_t *
dobj_find(dobj_runtime_id_t id)
{
    uint32 *index = hashtable_find(_table, id, hashtable_hash(&id, sizeof(id)));
    if (!index)
        return 0;
    return &_pool.all[*index];
}

uint32
dobj_get_index(dynamic_object_t *obj)
    {return fixed_pool_index(&_pool, obj);}

dynamic_object_t *
dobj_get(uint32 index)
{
    muta_assert(index < _pool.max);
    return &_pool.all[index];
}

void
dobj_on_sim_disonnected(uint32 sim_index)
{
    dobj_runtime_id_t key;
    dynamic_object_t            val;
    hashtable_for_each_pair(_table, key, val)
    {
        DEBUG_PRINTFF("Implement me!\n");
    }
}

void
dobj_serialize_to_msg(dynamic_object_t *obj, svmsg_add_dynamic_object_t *ret_s)
{
    ret_s->runtime_id   = obj->runtime_id;
    ret_s->type_id      = obj->type_id;
    ret_s->direction    = obj->in.world.direction;
    ret_s->x            = obj->in.world.position[0];
    ret_s->y            = obj->in.world.position[1];
    ret_s->z            = (uint8)obj->in.world.position[2];
}

bool32
dobj_is_in_world(dynamic_object_t *obj)
{
    return !(obj->flags & DOBJ_FLAG_IN_CONTAINER) &&
        !(obj->flags & DOBJ_FLAG_EQUIPPED);
}

bool32
dobj_have_unconfirmed_db_state(dynamic_object_t *obj)
    {return obj->flags & DOBJ_FLAG_UNCONFIRMED_DB_STATE;}

uint32
dobj_get_relevant_noncontrolling_sims(dynamic_object_t *obj,
    uint32 ret_sim_indices[8])
{
    IMPLEMENTME();
    return 0;
}

bool32
dobj_is_container(dynamic_object_t *obj)
    {return obj->flags & DOBJ_FLAG_IS_CONTAINER;}

player_t *
dobj_get_owning_player(dynamic_object_t *obj)
{
    if (obj->flags & DOBJ_FLAG_EQUIPPED)
        return pl_get(obj->in.equipped_by_player.player_index);
    if (!(obj->flags & DOBJ_FLAG_IN_CONTAINER))
        return 0;
    dynamic_object_t *container_obj = dobj_get(obj->in.container.dobj_index);
    muta_assert(container_obj->flags & DOBJ_FLAG_EQUIPPED);
    return pl_get(container_obj->in.equipped_by_player.player_index);
}

void
dobj_on_enter_new_instance(dynamic_object_t *obj, instance_t *inst, int x,
    int y, int z)
{
    obj->in.world.position[0]       = x;
    obj->in.world.position[1]       = y;
    obj->in.world.position[2]       = z;
    obj->in.world.instance_index    = inst_get_index(inst);
    interest_area_t *ia = ia_find(inst->interest_areas, inst_w_in_tiles(inst),
        inst_h_in_tiles(inst), x, y, z);
    ia_add_dynamic_object(inst->interest_areas,
        (uint32)(ia - inst->interest_areas), obj);
    svmsg_add_dynamic_object_t s;
    dobj_serialize_to_msg(obj, &s);
    interest_area_t *interest_area;
#ifdef _MUTA_DEBUG
    int num_iters = 0;
#endif
    INST_FOR_EACH_RELEVANT_INTEREST_AREA(inst, x, y, z, interest_area)
    {
        player_t *player;
        IA_FOR_EACH_PLAYER(interest_area, player)
        {
            /* CHECK PL_IS_RELEVANT */
            /* if (!PL_IS_RELEVANT(player))
                continue; */
            if (pil_should_cull(player->position, obj->in.world.position))
                continue;
            bbuf_t bb = cl_send(player->client, SVMSG_ADD_DYNAMIC_OBJECT_SZ);
            if (bb.max_bytes)
                svmsg_add_dynamic_object_write(&bb, &s);
        }
#ifdef _MUTA_DEBUG
        num_iters++;
#endif
    }
    muta_assert(num_iters <= 27);
}

static dobj_runtime_id_t
_next_runtime_id(void)
    {return _running_runtime_id++;}

static dynamic_object_t *
_create_dobj(dobj_type_id_t type_id, dynamic_object_def_t **def)
{
    *def = ent_get_dynamic_object_def(type_id);
    if (!*def)
    {
        LOG_ERROR("Cannot create dynamic object of type %u: no such dynamic "
            "object definition.");
        return 0;
    }
    dynamic_object_t *obj = fixed_pool_new(&_pool);
    if (!obj)
    {
        LOG_ERROR("Cannot spawn dynamic object: too many already spawned.");
        return 0;
    }
    dobj_runtime_id_t runtime_id = _next_runtime_id();
    size_t runtime_id_hash  = hashtable_hash(&runtime_id, sizeof(runtime_id));
    uint32 index            = fixed_pool_index(&_pool, obj);
    hashtable_einsert(_table, runtime_id, runtime_id_hash, index);
    obj->flags          = 0;
    obj->runtime_id     = runtime_id;
    obj->type_id        = type_id;
    if ((*def)->is_container)
    {
        obj->flags |= DOBJ_FLAG_IS_CONTAINER;
        container_init(&obj->container, (*def)->container_width,
            (*def)->container_height);
    }
    return obj;
}
