/* instance.h
 * Instances are loaded maps that can contain players and other entities. Each
 * instance can be divided into multiple parts as a grid of sorts, and each
 * individual piece of the grid may be simulated by a different simulation
 * server. */

#ifndef MUTA_SERVER_INSTANCE_H
#define MUTA_SERVER_INSTANCE_H

#include "../../shared/types.h"
#include "../../shared/common_utils.h"
#include "../../shared/sv_common_defs.h"
#include "interest_area.h"
#include "common.h"

#define INST_INVALID_INDEX 0xFFFFFFFF

#define INST_FOR_EACH_RELEVANT_INTEREST_AREA(instance, x, y, z, ret_interest_area) \
    for (int x_ = MAX(x / IA_W - 1, 0), \
        start_y_ = MAX(y / IA_W - 1, 0), \
        start_z_ = MAX(z / IA_T - 1, 0), \
        end_x_ = MIN(x / IA_W + 1, (int32)inst_w_in_tiles(instance) / IA_W - 1), \
        end_y_ = MIN(y / IA_W + 1, (int32)inst_h_in_tiles(instance) / IA_W - 1), \
        end_z_ = MIN(z / IA_T + 1, MAP_CHUNK_T / IA_T - 1), \
        z_; \
        x_ < end_x_; \
        ++x_) \
        for (int y_ = start_y_; y_ < end_y_; ++y_) \
            for (z_ = start_z_, (ret_interest_area) = _inst_for_each_relevant_interest_area_get(instance, x_, y_, z_); \
                z_ < end_z_; \
                ++z_, (ret_interest_area) = _inst_for_each_relevant_interest_area_get(instance, x_, y_, z_))
/* Iterate through each interest area relevant to position (x, y, z), meaning up
 * to 27 different interest areas. Example:
 *
 * interest_area_t *ia;
 * INST_FOR_EACH_RELEVANT_INTEREST_AREA(instance, 30, 42, 21, ia)
 * {
 *     ... Do something with ia ...
 * }
 */

/* Forward declaration(s) */
typedef struct client_t         client_t;
typedef struct map_data_t       map_data_t;
typedef struct interest_area_t  interest_area_t;

/* Types defined here */
typedef struct instance_part_t  instance_part_t;
typedef struct instance_t       instance_t;

enum instance_part_flag
{
    INST_PART_PLAYABLE = (1 << 0)
};

struct instance_part_t
{
    uint32          id;
    uint32          instance_index;
    uint32          sim_index;
    /* sim_index
     * Index of the sim server doing the simulation for the part. 0xFFFFFFFF if
     * none currently. */
    uint32          x_in_chunks;
    uint32          y_in_chunks;
    uint32          w_in_chunks;
    uint32          h_in_chunks;
    uint8           flags;
};

struct instance_t
{
    instance_runtime_id_t   id;
    uint32                  num_parts;
    map_data_t              *map_data;
    uint32                  w_in_chunks;
    uint32                  h_in_chunks;
    instance_part_t         *parts;
    /* parts
     * Instance parts, arranged as a 2D grid. */
    uint32                  w_in_parts;
    uint32                  h_in_parts;
    interest_area_t         *interest_areas;
    /* interest_areas
     * A 3D grid of interest areas, each one IA_W * IA_W * IA_T in size. */
};

int
inst_init(void);

void
inst_destroy(void);

instance_t *
inst_find(uint32 id);

uint32
inst_get_index(instance_t *instance);

uint32
inst_get_num_instances(void);

uint32
inst_find_unsimulated_part(uint32 instance_index);
/* inst_get_unsimulated_part()
 * Find a part from an instance with no simulation server assigned (sim_index is
 * 0xFFFFFFFF). On failure, INST_INVALID_INDEX is returned. */

instance_t *
inst_get(uint32 instance_index);

instance_part_t *
inst_find_part_by_position(instance_t *instance, int x, int y);
/* inst_find_part_by_position()
 * Position is in measured in tiles. */

instance_part_t *
inst_find_part_by_id(uint32 instance_part_id);
/* inst_find_part_by_id()
 * All instance parts can be identified by an ID unique even across different
 * instances. */

instance_part_t *
inst_get_part(instance_t *instance, uint32 part_index);

uint32
inst_w_in_tiles(instance_t *instance);

uint32
inst_h_in_tiles(instance_t *instance);

uint32
inst_map_id(instance_t *instance);

interest_area_t *
inst_find_interest_area(instance_t *instance, int x, int y, int z);

interest_area_t *
inst_get_interest_area(instance_t *instance, uint32 interest_area_index);

void
inst_part_on_connected(instance_part_t *part);

void
inst_part_on_disconnect(instance_part_t *part);

uint32
inst_get_relevant_non_controlling_sims_of_position(instance_t *inst, int x,
    int y, uint32 ret_sims[8]);
/* Get sims that should know about this position, but that do not control
 * entities in it.
 * Return value is the number of sims written in ret_sims. */

interest_area_t *
_inst_for_each_relevant_interest_area_get(instance_t *inst, int x, int y,
    int z);

#endif /* MUTA_SERVER_INSTANCE_H */
