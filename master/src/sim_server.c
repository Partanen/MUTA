#include <inttypes.h>
#include "sim_server.h"
#include "common.h"
#include "instance.h"
#include "player.h"
#include "dynamic_object.h"
#include "creature.h"
#include "map.h"
#include "drop.h"
#include "instance.h"
#include "client.h"
#include "../../shared/svchan_server.h"
#include "../../shared/common_defs.h"
#include "../../shared/sim_packets.h"
#include "../../shared/client_packets.h"

typedef struct sim_t                sim_t;
typedef struct instance_part_data_t instance_part_data_t;

struct sim_t
{
    svchan_server_client_t  *svchan_client;
    instance_part_data_t    *instance_parts; /* darr */
};

struct instance_part_data_t
{
    uint32 instance_index;
    uint32 part_index;
};
/* instance_part_data_t
 * Instances and instance parts are referred to by index in the instance API.
 * The indices, in this struct, are stored inside the sim_t that's
 * simulating the part. */

static svchan_server_t      _svchan_server;
static fixed_pool(sim_t)    _pool;

static void
_sim_server_add_part(sim_t *sim_server, uint32 instance_index,
    uint32 part_index);

static void
_svchan_post_event(svchan_server_event_t *event);

static int
_svchan_on_authed(svchan_server_client_t *client);

static int
_svchan_on_read_packet(svchan_server_client_t *client, uint8 *memory,
    int num_bytes);

static void
_svchan_on_disconnect(svchan_server_client_t *client, int reason);

static inline sim_t *
_get_sim_server(uint32 index);

static player_t *
_find_player_by_world_session_id(uint32 world_session_id, const char *func);

static inline bbuf_t
_send(sim_t *sim, uint32 num_bytes);

static inline bbuf_t
_send_const_encrypted(sim_t *sim, uint32 num_bytes);

static inline bbuf_t
_send_var_encrypted(sim_t *sim, uint32 num_bytes);

static int
_handle_fsimmsg_confirm_spawn_player(sim_t *sim,
    fsimmsg_confirm_spawn_player_t *s);

static int
_handle_fsimmsg_confirm_despawn_player(sim_t *sim,
    fsimmsg_confirm_despawn_player_t *s);

static int
_handle_fsimmsg_confirm_move_player(sim_t *sim,
    fsimmsg_confirm_move_player_t *s);

static int
_handle_fsimmsg_confirm_set_player_direction(sim_t *sim,
    fsimmsg_confirm_set_player_direction_t *s);

static int
_handle_fsimmsg_confirm_teleport_player_within_current_instance(
    sim_t *sim,
    fsimmsg_confirm_teleport_player_within_current_instance_t *s);

static int
_handle_fsimmsg_spawn_player_fail(sim_t *sim,
    fsimmsg_spawn_player_fail_t *s);

static int
_handle_fsimmsg_load_instance_part_result(sim_t *sim,
    fsimmsg_load_instance_part_result_t *s);

static int
_handle_fsimmsg_confirm_spawn_dynamic_object(sim_t *sim,
    fsimmsg_confirm_spawn_dynamic_object_t *s);

static int
_handle_fsimmsg_confirm_despawn_dynamic_object(sim_t *sim,
    fsimmsg_confirm_despawn_dynamic_object_t *s);

static int
_handle_fsimmsg_spawn_dynamic_object_fail(sim_t *sim,
    fsimmsg_spawn_dynamic_object_fail_t *s);

static int
_handle_fsimmsg_confirm_spawn_creature(sim_t *sim,
    fsimmsg_confirm_spawn_creature_t *s);

static int
_handle_fsimmsg_confirm_despawn_creature(sim_t *sim,
    fsimmsg_confirm_despawn_creature_t *s);

static int
_handle_fsimmsg_spawn_creature_fail(sim_t *sim,
    fsimmsg_spawn_creature_fail_t *s);

static int
_handle_fsimmsg_confirm_player_drop_dynamic_object(sim_t *sim,
    fsimmsg_confirm_player_drop_dynamic_object_t *s);

static int
_handle_fsimmsg_player_drop_dynamic_object_fail(sim_t *sim,
    fsimmsg_player_drop_dynamic_object_fail_t *s);

static int
_handle_fsimmsg_player_use_ability_result_read(sim_t *sim,
    fsimmsg_player_use_ability_result_t *s);

static int
_handle_fsimmsg_player_finished_ability_charge(sim_t *sim,
    fsimmsg_player_finished_ability_charge_t *s);

static int
_handle_fsimmsg_player_stop_charge_ability_fail(sim_t *sim,
    fsimmsg_player_stop_charge_ability_fail_t *s);

static int
_handle_fsimmsg_ability_script_request_deal_damage(sim_t *sim,
    fsimmsg_ability_script_request_deal_damage_t *s);

int
sim_init(void)
{
    int                         err;
    svchan_server_callbacks_t   callbacks;
    callbacks.post_event        = _svchan_post_event;
    callbacks.on_authed         = _svchan_on_authed;
    callbacks.on_read_packet    = _svchan_on_read_packet;
    callbacks.on_disconnect     = _svchan_on_disconnect;
    if (svchan_server_init(&_svchan_server, &callbacks,
        com_config.max_sims, 16384, 4 * MUTA_MTU))
        {err = 2; goto fail;}
    for (uint32 i = 0; i < darr_num(com_config.sim_ports); ++i)
        svchan_server_add_port(&_svchan_server, com_config.sim_ports[i]);
    svchan_server_add_account_info(&_svchan_server,
        com_config.sim_username, com_config.sim_password);
    svchan_server_add_address(&_svchan_server,
        uint32_ip_from_uint8s(127, 0, 0, 1));
    fixed_pool_init(&_pool, com_config.max_sims);
    return 0;
    fail:
        return err;
}

void
sim_destroy(void)
    {svchan_server_destroy(&_svchan_server);}

int
sim_start(void)
    {return svchan_server_start(&_svchan_server);}

void
sim_stop(void)
    {svchan_server_stop(&_svchan_server);}

bbuf_t
sim_send(uint32 sim_server_index, uint32 num_bytes)
    {return _send(_get_sim_server(sim_server_index), num_bytes);}

bbuf_t
sim_send_const_encrypted(uint32 sim_server_index, uint32 num_bytes)
    {return _send_const_encrypted(_get_sim_server(sim_server_index), num_bytes);}

bbuf_t
sim_send_var_encrypted(uint32 sim_server_index, uint32 num_bytes)
    {return _send_var_encrypted(_get_sim_server(sim_server_index), num_bytes);}

cryptchan_t *
sim_get_cryptchan(uint32 sim_server_index)
{
    return svchan_server_client_get_cryptchan(
        _get_sim_server(sim_server_index)->svchan_client);
}

void
sim_flush(void)
    {svchan_server_flush(&_svchan_server);}


static void
_sim_server_add_part(sim_t *sim, uint32 instance_index, uint32 part_index)
{
    instance_t      *inst = inst_get(instance_index);
    instance_part_t *part = &inst->parts[part_index];
    muta_assert(part->sim_index == SIM_INVALID_INDEX);
    part->sim_index = fixed_pool_index(&_pool, sim);
    instance_part_data_t data;
    data.instance_index = instance_index;
    data.part_index     = part_index;
    darr_push(sim->instance_parts, data);
    bbuf_t bb = _send(sim, TSIMMSG_LOAD_INSTANCE_PART_SZ);
    if (!bb.max_bytes)
        return;
    tsimmsg_load_instance_part_t s =
    {
        s.map_id        = inst->map_data->id,
        s.part_id       = part->id,
        s.instance_id   = inst->id,
        s.chunk_x       = part->x_in_chunks,
        s.chunk_y       = part->y_in_chunks,
        s.chunk_w       = part->w_in_chunks,
        s.chunk_h       = part->h_in_chunks
    };
    tsimmsg_load_instance_part_write(&bb, &s);
}

static void
_svchan_post_event(svchan_server_event_t *event)
{
    com_event_t new_event;
    new_event.type          = COM_EVENT_SVCHAN_SERVER;
    new_event.svchan_server = *event;
    com_push_events(&new_event, 1);
}

static int
_svchan_on_authed(svchan_server_client_t *client)
{
    sim_t *sim = fixed_pool_new(&_pool);
    if (!sim)
    {
        LOG_ERROR("A sim attempted to connect, but max sims (%u) already "
            "connected.\n", _pool.max);
    }
    sim->svchan_client = client;
    svchan_server_client_set_user_data(client, sim);
    darr_clear(sim->instance_parts);
    // Find some unsimulated instance parts. Have this sim server simulate them.
    uint32 num_instances = inst_get_num_instances();
    for (uint32 i = 0; i < num_instances; ++i)
    {
        uint32 part_index = inst_find_unsimulated_part(i);
        if (part_index != INST_INVALID_INDEX)
            _sim_server_add_part(sim, i, part_index);
    }
    return 0;
}

static int
_svchan_on_read_packet(svchan_server_client_t *client, uint8 *memory,
    int num_bytes)
{
    sim_t           *sim        = svchan_server_client_get_user_data(client);
    bbuf_t          bb          = BBUF_INITIALIZER(memory, num_bytes);
    int             incomplete  = 0;
    int             dc          = 0;
    simmsg_t        msg_type;
    while (BBUF_FREE_SPACE(&bb) >= sizeof(simmsg_t))
    {
        BBUF_READ(&bb, &msg_type);
        switch (msg_type)
        {
        case SIMMSG_KEEP_ALIVE:
            break;
        case FSIMMSG_LOAD_INSTANCE_PART_RESULT:
        {
            fsimmsg_load_instance_part_result_t s;
            incomplete = fsimmsg_load_instance_part_result_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fsimmsg_load_instance_part_result(sim, &s);
        }
            break;
        case FSIMMSG_CONFIRM_SPAWN_PLAYER:
        {
            fsimmsg_confirm_spawn_player_t s;
            incomplete = fsimmsg_confirm_spawn_player_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fsimmsg_confirm_spawn_player(sim, &s);
        }
            break;
        case FSIMMSG_CONFIRM_DESPAWN_PLAYER:
        {
            fsimmsg_confirm_despawn_player_t s;
            incomplete = fsimmsg_confirm_despawn_player_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fsimmsg_confirm_despawn_player(sim, &s);
        }
            break;
        case FSIMMSG_CONFIRM_MOVE_PLAYER:
        {
            fsimmsg_confirm_move_player_t s;
            incomplete = fsimmsg_confirm_move_player_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fsimmsg_confirm_move_player(sim, &s);
        }
            break;
        case FSIMMSG_SPAWN_PLAYER_FAIL:
        {
            fsimmsg_spawn_player_fail_t s;
            incomplete = fsimmsg_spawn_player_fail_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fsimmsg_spawn_player_fail(sim, &s);
        }
            break;
        case FSIMMSG_CONFIRM_SET_PLAYER_DIRECTION:
        {
            fsimmsg_confirm_set_player_direction_t s;
            incomplete = fsimmsg_confirm_set_player_direction_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fsimmsg_confirm_set_player_direction(sim, &s);
        }
            break;
        case FSIMMSG_CONFIRM_TELEPORT_PLAYER_WITHIN_CURRENT_INSTANCE:
        {
            fsimmsg_confirm_teleport_player_within_current_instance_t s;
            incomplete =
                fsimmsg_confirm_teleport_player_within_current_instance_read(
                    &bb, &s);
            if (!incomplete)
                dc =_handle_fsimmsg_confirm_teleport_player_within_current_instance(
                    sim, &s);
        }
            break;
        case FSIMMSG_CONFIRM_SPAWN_DYNAMIC_OBJECT:
        {
            fsimmsg_confirm_spawn_dynamic_object_t s;
            incomplete = fsimmsg_confirm_spawn_dynamic_object_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fsimmsg_confirm_spawn_dynamic_object(sim, &s);
        }
            break;
        case FSIMMSG_CONFIRM_DESPAWN_DYNAMIC_OBJECT:
        {
            fsimmsg_confirm_despawn_dynamic_object_t s;
            incomplete = fsimmsg_confirm_despawn_dynamic_object_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fsimmsg_confirm_despawn_dynamic_object(sim, &s);
        }
            break;
        case FSIMMSG_SPAWN_DYNAMIC_OBJECT_FAIL:
        {
            fsimmsg_spawn_dynamic_object_fail_t s;
            incomplete = fsimmsg_spawn_dynamic_object_fail_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fsimmsg_spawn_dynamic_object_fail(sim, &s);
        }
            break;
        case FSIMMSG_CONFIRM_SPAWN_CREATURE:
        {
            fsimmsg_confirm_spawn_creature_t s;
            incomplete = fsimmsg_confirm_spawn_creature_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fsimmsg_confirm_spawn_creature(sim, &s);
        }
            break;
        case FSIMMSG_CONFIRM_DESPAWN_CREATURE:
        {
            fsimmsg_confirm_despawn_creature_t s;
            incomplete = fsimmsg_confirm_despawn_creature_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fsimmsg_confirm_despawn_creature(sim, &s);
        }
            break;
        case FSIMMSG_SPAWN_CREATURE_FAIL:
        {
            fsimmsg_spawn_creature_fail_t s;
            incomplete = fsimmsg_spawn_creature_fail_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fsimmsg_spawn_creature_fail(sim, &s);
        }
            break;
        case FSIMMSG_CONFIRM_PLAYER_DROP_DYNAMIC_OBJECT:
        {
            fsimmsg_confirm_player_drop_dynamic_object_t s;
            incomplete = fsimmsg_confirm_player_drop_dynamic_object_read(&bb,
                &s);
            if (!incomplete)
                dc = _handle_fsimmsg_confirm_player_drop_dynamic_object(sim,
                    &s);
        }
            break;
        case FSIMMSG_PLAYER_DROP_DYNAMIC_OBJECT_FAIL:
        {
            fsimmsg_player_drop_dynamic_object_fail_t s;
            incomplete = fsimmsg_player_drop_dynamic_object_fail_read(&bb,
                &s);
            if (!incomplete)
                dc = _handle_fsimmsg_player_drop_dynamic_object_fail(sim, &s);
        }
            break;
        case FSIMMSG_PLAYER_USE_ABILITY_RESULT:
        {
            fsimmsg_player_use_ability_result_t s;
            incomplete = fsimmsg_player_use_ability_result_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fsimmsg_player_use_ability_result_read(sim, &s);
        }
            break;
        case FSIMMSG_PLAYER_FINISHED_ABILITY_CHARGE:
        {
            fsimmsg_player_finished_ability_charge_t s;
            incomplete = fsimmsg_player_finished_ability_charge_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fsimmsg_player_finished_ability_charge(sim, &s);
        }
            break;
        case FSIMMSG_PLAYER_STOP_CHARGE_ABILITY_FAIL:
        {
            fsimmsg_player_stop_charge_ability_fail_t s;
            incomplete = fsimmsg_player_stop_charge_ability_fail_read(&bb, &s);
            if (!incomplete)
                dc = _handle_fsimmsg_player_stop_charge_ability_fail(sim, &s);
        }
            break;
        case FSIMMSG_ABILITY_SCRIPT_REQUEST_DEAL_DAMAGE:
        {
            fsimmsg_ability_script_request_deal_damage_t s;
            incomplete = fsimmsg_ability_script_request_deal_damage_read(&bb,
                &s);
            if (!incomplete)
                dc = _handle_fsimmsg_ability_script_request_deal_damage(sim,
                    &s);
        }
            break;
        default:
            LOG_ERROR("Unknown sim server message type %u.", (uint)msg_type);
            dc = 1;
            break;
        }
        if (dc || incomplete < 0)
        {
            LOG_ERROR("msg_type: %u, dc:  %d, incomplete: %d", msg_type, dc,
                incomplete);
            return -1;
        }
        if (incomplete)
        {
            bb.num_bytes -= sizeof(simmsg_t);
            break;
        }
    }
    return BBUF_FREE_SPACE(&bb);
}

static void
_svchan_on_disconnect(svchan_server_client_t *client, int reason)
{
    LOG("Sim disconnected.");
    sim_t *sim = svchan_server_client_get_user_data(client);
    uint32 sim_index = fixed_pool_index(&_pool, sim);
    pl_on_sim_disconnected(sim_index);
    dobj_on_sim_disonnected(sim_index);
    for (uint32 i = 0; i < darr_num(sim->instance_parts); ++i)
    {
        instance_part_t *part = inst_get_part(
            inst_get(sim->instance_parts[i].instance_index),
            sim->instance_parts[i].part_index);
        inst_part_on_disconnect(part);
    }
    sim->svchan_client = 0;
    darr_clear(sim->instance_parts);
    fixed_pool_free(&_pool, (sim_t*)svchan_server_client_get_user_data(client));
}

static inline sim_t *
_get_sim_server(uint32 index)
{
    muta_assert(index < _pool.max);
    sim_t *ret = &_pool.all[index];
    return ret;
}

static player_t *
_find_player_by_world_session_id(uint32 world_session_id, const char *func)
{
    player_t *player = pl_find_by_world_session_id(world_session_id);
    if (player)
        return player;
    LOG("%s(): world session %u not found.", func, world_session_id);
    return 0;
}

static inline bbuf_t
_send(sim_t *sim, uint32 num_bytes)
{
    if (!sim->svchan_client)
        return (bbuf_t){0};
    return svchan_server_client_send(sim->svchan_client,
        sizeof(tsimmsg_t) + num_bytes);
}

static inline bbuf_t
_send_const_encrypted(sim_t *sim, uint32 num_bytes)
{
    if (!sim->svchan_client)
        return (bbuf_t){0};
    return svchan_server_client_send_const_encrypted(sim->svchan_client,
        sizeof(tsimmsg_t) + num_bytes);
}

static inline bbuf_t
_send_var_encrypted(sim_t *sim, uint32 num_bytes)
{
    if (!sim->svchan_client)
        return (bbuf_t){0};
    return svchan_server_client_send_var_encrypted(sim->svchan_client,
        sizeof(tsimmsg_t) + num_bytes);
}

static int
_handle_fsimmsg_confirm_spawn_player(sim_t *sim,
    fsimmsg_confirm_spawn_player_t *s)
{
    player_t *player = pl_find_by_world_session_id(s->world_session_id);
    if (!player)
    {
        LOG_EXT("Received for unexistant player %u.", s->world_session_id);
        return 0;
    }
    if (pl_confirm_spawn(player, s->db_id, s->instance_part_id, s->x, s->y,
        s->z, s->abilities.data, s->abilities.len))
        return 1;
    return 0;
}

static int
_handle_fsimmsg_confirm_despawn_player(sim_t *sim,
    fsimmsg_confirm_despawn_player_t *s)
{
    player_t *player = pl_find_by_world_session_id(s->world_session_id);
    if (!player)
    {
        LOG_EXT("Received confirm despawn unexistant playuer %u\n",
            s->world_session_id);
        return 1;
    }
    if (player->sim_index != fixed_pool_index(&_pool, sim))
    {
        LOG_EXT("Sim %u confirmed despawn of player id %u, but player is not "
            "owned by sim (owned by sim %u).", fixed_pool_index(&_pool, sim),
            player->world_session_id, player->sim_index);
        return 2;
    }
    if (!pl_despawning(player))
    {
        LOG_EXT("Sim %u confirmed despawn of player id %u, but despawn was not "
            "requested.", fixed_pool_index(&_pool, sim),
            player->world_session_id);
        return 3;
    }
    pl_confirm_despawn(player);
    return 0;
}

static int
_handle_fsimmsg_confirm_move_player(sim_t *sim,
    fsimmsg_confirm_move_player_t *s)
{
    player_t *player = _find_player_by_world_session_id(s->world_session_id,
        __func__);
    if (!player)
        return 1;
    if (pl_confirm_move_player(player, s->x, s->y, s->z))
        return 2;
    return 0;
}

static int
_handle_fsimmsg_confirm_set_player_direction(sim_t *sim,
    fsimmsg_confirm_set_player_direction_t *s)
{
    player_t *player = _find_player_by_world_session_id(s->world_session_id,
        __func__);
    if (!player)
        return 1;
    if (pl_confirm_set_player_direction(player, s->direction))
        return 2;
    return 0;
}

static int
_handle_fsimmsg_confirm_teleport_player_within_current_instance(sim_t *sim,
    fsimmsg_confirm_teleport_player_within_current_instance_t *s)
{
    player_t *player = _find_player_by_world_session_id(s->world_session_id,
        __func__);
    if (!player)
        return 1;
    if (pl_confirm_teleport_player_within_current_instance(player, s->x, s->y,
        s->z))
        return 2;
    return 0;
}

static int
_handle_fsimmsg_spawn_player_fail(sim_t *sim, fsimmsg_spawn_player_fail_t *s)
{
    player_t *player = _find_player_by_world_session_id(s->world_session_id,
        __func__);
    if (!player)
        return 0;
    if (player->db_id != s->db_id)
        return 1;
    if (pl_spawn_fail(player))
        return 2;
    return 0;
}

static int
_handle_fsimmsg_load_instance_part_result(sim_t *sim,
    fsimmsg_load_instance_part_result_t *s)
{
    instance_part_t *part = inst_find_part_by_id(s->part_id);
    if (!part)
    {
        LOG_ERROR("Sim sent load result for part %u, but part does not exist.",
            s->part_id);
        return 1;
    }
    if (&_pool.all[part->sim_index] != sim)
    {
        LOG_ERROR("Sim sent load result for part %u, but part is not owned by "
            "sim.", s->part_id);
        return 2;
    }
    if (part->flags & INST_PART_PLAYABLE)
    {
        LOG_ERROR("Sim sent load result for part %u, but part was already "
            "marked playable.", s->part_id);
        return 3;
    }
    if (!s->result)
    {
        part->flags |= INST_PART_PLAYABLE;
        inst_part_on_connected(part);
        LOG("Sim %u successfully loaded instance part id %u.",
            fixed_pool_index(&_pool, sim), part->id);
    }
    else
    {
        LOG_EXT("Failed to load instance part (code %" PRIu8 "). TODO: Handle "
            "this.");
        part->sim_index = SIM_INVALID_INDEX;
    }
    return 0;
}

static int
_handle_fsimmsg_confirm_spawn_dynamic_object(sim_t *sim,
    fsimmsg_confirm_spawn_dynamic_object_t *s)
{
    dynamic_object_t *obj = dobj_find(s->runtime_id);
    if (!obj)
    {
        LOG_ERROR("Sim confirmed dynamic object %u spawn, but no such object "
            "exists.", s->runtime_id);
        return 1;
    }
    if (dobj_confirm_spawn(obj, s->instance_part_id, s->x, s->y, s->z,
        s->direction))
        return 2;
    return 0;
}

static int
_handle_fsimmsg_confirm_despawn_dynamic_object(sim_t *sim,
    fsimmsg_confirm_despawn_dynamic_object_t *s)
{
}

static int
_handle_fsimmsg_spawn_dynamic_object_fail(sim_t *sim,
    fsimmsg_spawn_dynamic_object_fail_t *s)
{
}

static int
_handle_fsimmsg_confirm_spawn_creature(sim_t *sim, fsimmsg_confirm_spawn_creature_t *s)
{
    creature_t *creature = creature_find(s->runtime_id);
    if (!creature)
    {
        LOG_ERROR("Sim confirmed creature %u spawn, but no such creature "
            "exists.", s->runtime_id);
        return 1;
    }
    if (creature_confirm_spawn(creature, s->instance_part_id, s->x, s->y, s->z,
        s->direction))
        return 2;
    return 0;
}

static int
_handle_fsimmsg_confirm_despawn_creature(sim_t *sim,
    fsimmsg_confirm_despawn_creature_t *s)
{
}

static int
_handle_fsimmsg_spawn_creature_fail(sim_t *sim,
    fsimmsg_spawn_creature_fail_t *s)
{
}

static int
_handle_fsimmsg_confirm_player_drop_dynamic_object(sim_t *sim,
    fsimmsg_confirm_player_drop_dynamic_object_t *s)
{
    dynamic_object_t *dobj = dobj_find(s->runtime_id);
    if (!dobj)
    {
        LOG_ERROR("Sim confirmed spawn dropped dynamic object %u, but "
            "object not found.", s->runtime_id);
        return 1;
    }
    if (dobj_is_in_world(dobj))
    {
        LOG_ERROR("Sim confirmed spawn dropped dynamic object %u, but "
            "object was already in world.", s->runtime_id);
        return 2;
    }
    if (!(dobj->flags & DOBJ_FLAG_UNCONFIRMED_SIM_STATE))
    {
        LOG_ERROR("Sim confirmed spawn dropped dynamic object %u, but "
            "object is not owned by this sim.", s->runtime_id);
        return 4;
    }
    instance_part_t *part = inst_find_part_by_id(s->instance_part_id);
    if (!part)
    {
        LOG_ERROR("Instance part with ID %u not found.", s->instance_part_id);
        return 5;
    }
    if (part->sim_index != fixed_pool_index(&_pool, sim))
    {
        LOG_ERROR("Instance part with ID %u not owned by this sim.",
            s->instance_part_id);
        return 6;
    }
    player_t *pl = dobj_get_owning_player(dobj);
    /* WHAT IF THE PLAYER LOGGED OUT? */
    muta_assert(pl);
    if (pl->instance_index != part->instance_index)
    {
        LOG_ERROR("Instance part with ID %u does not belong to the player's "
            "instance.", s->instance_part_id);
        return 7;
    }
    if (drop_on_sim_confirmed_player_drop_item(dobj,
        fixed_pool_index(&_pool, sim), s->x, s->y, s->z, s->direction))
        return 8;
    return 0;
}

static int
_handle_fsimmsg_player_drop_dynamic_object_fail(sim_t *sim,
    fsimmsg_player_drop_dynamic_object_fail_t *s)
{
    dynamic_object_t *dobj = dobj_find(s->dobj_runtime_id);
    if (!dobj)
    {
        LOG_ERROR("Dynamic object %u not found.", s->dobj_runtime_id);
        return 1;
    }
    if (dobj_is_in_world(dobj))
    {
        LOG_ERROR("Sim failed to spawn dropped dynamic object %u, but "
            "object was already in world.", s->dobj_runtime_id);
        return 2;
    }
    if (!(dobj->flags & DOBJ_FLAG_UNCONFIRMED_SIM_STATE))
    {
        LOG_ERROR("Sim failed to spawn dropped dynamic object %u, but "
            "object is not owned by this sim.", s->dobj_runtime_id);
        return 3;
    }
    player_t *pl = pl_find_by_world_session_id(s->player_runtime_id);
    if (pl != dobj_get_owning_player(dobj))
    {
        LOG_ERROR("Dynamic object %u not owned by player %u.",
            s->dobj_runtime_id, pl->world_session_id);
        return 4;
    }
    if (drop_on_sim_failed_player_drop_item(dobj, pl, 0))
    {
        LOG_ERROR("drop_on_sim_failed_player_drop_item() failed.");
        return 5;
    }
    return 0;
}

static int
_handle_fsimmsg_player_use_ability_result_read(sim_t *sim,
    fsimmsg_player_use_ability_result_t *s)
{
    player_t *pl = pl_find_by_world_session_id(s->player_runtime_id);
    if (!pl)
    {
        LOG_DEBUG_EXT("Player no %u longer exists.", s->player_runtime_id);
        return 0;
    }
    if (pl_on_use_ability_result(pl, s->ability_id, s->target_type,
        s->target_runtime_id, s->result))
    {
        LOG_DEBUG_EXT("pl_on_use_ability_result() failed.");
        return 1;
    }
    return 0;
}

static int
_handle_fsimmsg_player_finished_ability_charge(sim_t *sim,
    fsimmsg_player_finished_ability_charge_t *s)
{
    player_t *pl = pl_find_by_world_session_id(s->player_runtime_id);
    if (!pl)
    {
        LOG_DEBUG_EXT("Player no %u longer exists.", s->player_runtime_id);
        return 0;
    }
    if (s->target_type != COMMON_ENTITY_PLAYER &&
        s->target_type != COMMON_ENTITY_CREATURE &&
        s->target_type != COMMON_ENTITY_DYNAMIC_OBJECT)
    {
        LOG_ERROR("Bad entity type %u.", (uint)s->target_type);
        return 1;
    }
    if (pl_on_player_finished_ability_charge(pl, s->ability_id,
        (enum common_entity_type)s->target_type, s->target_runtime_id,
        (enum common_ability_charge_result)s->result))
    {
        LOG_ERROR("pl_on_player_finished_ability_charge() failed.");
        return 2;
    }
    return 0;
}

static int
_handle_fsimmsg_player_stop_charge_ability_fail(sim_t *sim,
    fsimmsg_player_stop_charge_ability_fail_t *s)
{
    LOG_DEBUG_EXT("Is this message necessary?");
    return 0;
}

static int
_handle_fsimmsg_ability_script_request_deal_damage(sim_t *sim,
    fsimmsg_ability_script_request_deal_damage_t *s)
{
    int x, y, z;
    instance_t *inst;
    uint32 health_current;
    switch (s->target_type)
    {
    case COMMON_ENTITY_PLAYER:
    {
        player_t *player = pl_find_by_world_session_id(s->target_runtime_id);
        if (!player)
        {
            LOG_ERROR("Could not find target player %u.", s->target_runtime_id);
            return 1;
        }
        player->health.current -= MIN(player->health.current, s->damage);
        inst = inst_get(player->instance_index);
        x = player->position[0];
        y = player->position[1];
        z = player->position[2];
        health_current = player->health.current;
    }
        break;
    case COMMON_ENTITY_CREATURE:
    {
        creature_t *creature = creature_find(s->target_runtime_id);
        if (!creature)
        {
            LOG_ERROR("Could not find target creature %u.",
                s->target_runtime_id);
            return 2;
        }
        creature->health.current -= MIN(creature->health.current, s->damage);
        inst = inst_get(creature->instance_index);
        x = creature->position[0];
        y = creature->position[1];
        z = creature->position[2];
        health_current = creature->health.current;
    }
        break;
    default:
        LOG_ERROR("Bad entity type %d.", (int)s->entity_type);
        return 3;
    }
    /* Inform nearby non-controlling sims. */
    tsimmsg_entity_dealt_damage_t sim_msg =
    {
        .entity_type        = s->entity_type,
        .entity_runtime_id  = s->entity_runtime_id,
        .target_type        = s->target_type,
        .target_runtime_id  = s->target_runtime_id,
        .damage             = s->damage,
        .health_current     = health_current
    };
    bbuf_t sim_bb = _send(sim, TSIMMSG_ENTITY_DEALT_DAMAGE_SZ);
    if (sim_bb.max_bytes)
        tsimmsg_entity_dealt_damage_write(&sim_bb, &sim_msg);
    uint32 sims[8];
    uint32 num_sims = inst_get_relevant_non_controlling_sims_of_position(inst,
        x, y, sims);
    for (uint32 i = 0; i < num_sims; ++i)
    {
        bbuf_t bb = sim_send(sims[i], TSIMMSG_ENTITY_DEALT_DAMAGE_SZ);
        if (!bb.max_bytes)
            continue;
        tsimmsg_entity_dealt_damage_write(&bb, &sim_msg);
    }
    /* Inform nearby players. */
    svmsg_entity_dealt_damage_t player_msg =
    {
        .entity_type        = s->entity_type,
        .entity_runtime_id  = s->entity_runtime_id,
        .target_type        = s->target_type,
        .target_runtime_id  = s->target_runtime_id,
        .damage             = s->damage,
        .health_current     = health_current
    };
    LOG_DEBUG_EXT("entity_type: %d, entity_runtime_id: %d, target_type: %d, target_runtime_id: %d",
        (int)player_msg.entity_type, (int)player_msg.entity_runtime_id, (int)player_msg.target_type, (int)player_msg.target_runtime_id);
    interest_area_t *interest_area;
    INST_FOR_EACH_RELEVANT_INTEREST_AREA(inst, x, y, z, interest_area)
    {
        player_t *player;
        IA_FOR_EACH_PLAYER(interest_area, player)
        {
            if (!(player->flags & PL_RELEVANT))
                continue;
            bbuf_t bb = cl_send(player->client, SVMSG_ENTITY_DEALT_DAMAGE_SZ);
            if (bb.max_bytes)
                svmsg_entity_dealt_damage_write(&bb, &player_msg);
        }
    }
    return 0;
}
