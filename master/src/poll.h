#ifndef MUTA_SERVER_POLL_H
#define MUTA_SERVER_POLL_H

#include "../../shared/ksys.h"

typedef struct poll_data_t poll_data_t;

struct poll_data_t
{
    void *user_data;
    void (*callback)(void *data);
};

int
poll_init();

void
poll_destroy();

int
poll_start();

void
poll_stop();

int
poll_add(socket_t socket, poll_data_t *data);

void
poll_del(socket_t socket);

#endif /* MUTA_SERVER_POLL_H */

