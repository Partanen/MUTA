#include <inttypes.h>
#include "player.h"
#include "common.h"
#include "instance.h"
#include "client.h"
#include "sim_server.h"
#include "dynamic_object.h"
#include "creature.h"
#include "pickup.h"
#include "new_player_spawn_point.h"
#include "../../shared/hashtable.h"
#include "../../shared/common_utils.h"
#include "../../shared/client_packets.h"
#include "../../shared/sim_packets.h"
#include "../../shared/entities.h"
#include "../../shared/abilities.h"

#define PL_IS_RELEVANT(player) ((player)->flags & PL_RELEVANT)

typedef struct save_dynamic_object_position_in_player_inventory_query_data_t
    save_dynamic_object_position_in_player_inventory_query_data_t;
typedef struct player_table player_table_t;

struct save_dynamic_object_position_in_player_inventory_query_data_t
{
    player_runtime_id_t player_runtime_id;
    dobj_runtime_id_t   dobj_runtime_id;
    player_db_guid_t    player_db_id;
};

hashtable_define(player_table, uint32, uint32);

static fixed_pool(player_t) _players;
static player_table_t       _table;
/* player_table contains all players keyed by world_session_id */
static player_runtime_id_t          _running_world_session_id;
static uint32               *_despawned;
/* _despawned
 * Indices to despawned players, despawns deferred to the end of the frame. */

static void
_fill_new_player_msg(svmsg_add_player_t *s, player_t *player);

static void
_clear_interest_list_of_culled(player_t *player);

static void
_add_unculled_to_interest_list(player_t *player, int old_position[3]);
/* Called when position changes in the same instance.
 * The function sends messages of (and in the case of players, to) any objects
 * that also came into visible range after the move. */

static void
_clear_followeds(player_t *player);

static int
_change_position_within_instance(player_t *player, int x, int y, int z);

static void
_teleport_within_current_instance_and_sim(player_t *player,
    uint32 instance_index, int x, int y, int z);

static void
_teleport_to_different_instance_within_current_sim(player_t *player,
    instance_part_t *part, int x, int y, int z);

static void
_teleport_to_new_instance_and_sim(player_t *player, instance_part_t *part,
    int x, int y, int z);

static void
_leave_current_instance(player_t *player);
/* Clear the player's interest list, remove it from any interest areas and send
 * removal messages to nearby players. */

static void
_enter_new_instance(player_t *player, instance_t *inst, int x, int y, int z);
/* Initialize player's interest_list, add to interest areas and send necessary
 * messages as if the player had just spawned. Assumes the player is not in any
 * instance yet.
 * Does NOT send info of the player character to the player. */

static bbuf_t
_send(player_t *pl, uint32 size);

static inline bbuf_t
_send_const_encrypted(player_t *pl, uint32 size);

static inline bbuf_t
_send_var_encrypted(player_t *pl, uint32 size);

static void
_on_complete_query_save_player_character_position_and_direction(
    wdbc_query_id_t query_id, void *user_data, int error,
    player_db_guid_t player_db_id, uint32 instance_id, uint8 direction,
    int32 x, int32 y, uint8 z);

static void
_on_complete_query_save_dynamic_object_position_in_player_inventory(
    wdbc_query_id_t query_id, void *user_data, int error, uuid16_t dobj_db_uuid,
    equipment_slot_id_t equipment_slot_id, uint8 x, uint8 y);

int
pl_init(void)
{
    fixed_pool_init(&_players, com_config.max_online_players);
    player_table_einit(&_table, com_config.max_online_players);
    darr_reserve(_despawned, com_config.max_online_players);
    return 0;
}

void
pl_destroy(void)
{
    fixed_pool_destroy(&_players);
    player_table_destroy(&_table);
    darr_free(_despawned);
}

void
pl_on_sim_disconnected(uint32 sim_index)
{
    LOG("Disconnecting all players for sim index %u.", sim_index);
    player_runtime_id_t world_session_id;
    uint32      player_index;
    hashtable_for_each_pair(_table, world_session_id, player_index)
    {
        player_t *player = &_players.all[player_index];
        if (player->sim_index == sim_index && player->client)
            cl_disconnect(player->client);
    }
}

player_t *
pl_request_spawn(client_t *client, player_db_guid_t db_id, const char *name,
    int race, int sex, uint32 instance_id, int xyz[3], int direction,
    uint8 move_speed, wdbc_dynamic_object_in_db_t *items, uint32 num_items)
{
    bbuf_t  bb;
    int     position[3];
    for (int i = 0; i < 3; ++i)
        position[i] = xyz[i];
    instance_t *instance = inst_find(instance_id);
    if (!instance)
    {
        player_race_def_t *race_def = ent_get_player_race_def(race);
        if (!race_def)
            muta_panic_print("Invalid race %d.", race);
        new_player_spawn_point_t *spawn_point = new_player_spawn_points_find(
            race_def->start_area_id);
        if (!spawn_point)
            muta_panic_print("Invalid spawn point %u.", race_def->start_area_id);
        for (int i = 0; i < 3; ++i)
            position[i] = spawn_point->position[i];
        instance = inst_find(spawn_point->shared_instance_id);
    }
    if (!instance)
    {
        LOG_EXT("Instance %u not found.", instance_id);
        svmsg_enter_world_fail_t s;
        s.reason = ENTER_WORLD_ERROR_INSTANCE_DOWN;
        bbuf_t bb = cl_send_const_encrypted(client, SVMSG_ENTER_WORLD_FAIL_SZ);
        if (bb.max_bytes)
            svmsg_enter_world_fail_write(&bb, cl_get_cryptchan(client), &s);
        return 0;
    }
    instance_part_t *part = inst_find_part_by_position(instance, position[0],
        position[1]);
    if (part->sim_index == SIM_INVALID_INDEX)
    {
        LOG_EXT("Instance part is down.");
        svmsg_enter_world_fail_t s;
        s.reason = ENTER_WORLD_ERROR_INSTANCE_PART_DOWN;
        bbuf_t bb = cl_send_const_encrypted(client, SVMSG_ENTER_WORLD_FAIL_SZ);
        if (bb.max_bytes)
            svmsg_enter_world_fail_write(&bb, cl_get_cryptchan(client), &s);
        return 0;
    }
    uint32      instance_index  = inst_get_index(instance);
    player_t    *player         = fixed_pool_new(&_players);
    if (!player)
    {
        LOG_EXT("No free player slots available.");
        svmsg_enter_world_fail_t s;
        s.reason = ENTER_WORLD_ERROR_FULL;
        bbuf_t bb = cl_send_const_encrypted(client, SVMSG_ENTER_WORLD_FAIL_SZ);
        if (bb.max_bytes)
            svmsg_enter_world_fail_write(&bb, cl_get_cryptchan(client), &s);
        return 0;
    }
    muta_assert(!(player->flags & PL_RESERVED));
    muta_assert(!(darr_num(player->interest_list.players)));
    muta_assert(!(darr_num(player->interest_list.player_followers)));
    player->db_id               = db_id;
    player->world_session_id    = _running_world_session_id++;
    player->client              = client;
    player->position[0]         = position[0];
    player->position[1]         = position[1];
    player->position[2]         = position[2];
    player->sim_index           = part->sim_index;
    player->interest_area_index = 0xFFFFFFFF;
    player->interest_area_prev  = 0xFFFFFFFF;
    player->interest_area_next  = 0xFFFFFFFF;
    player->flags               = 0;
    player->instance_index      = instance_index;
    player->race                = race;
    player->sex                 = sex;
    player->direction           = CLAMP(direction, 0, NUM_ISODIRS - 1);
    player->move_speed          = move_speed;
    player->health.current      = 100;
    player->health.max          = 100;
    player->num_unfinished_async_actions = 0;
    for (int i = 0; i < NUM_EQUIPMENTS_SLOTS; ++i)
        player->equipment[i] = DOBJ_INVALID_INDEX;
    strncpy(player->name, name, sizeof(player->name) - 1);
    strncpy(player->display_name, name, sizeof(player->display_name) - 1);
    player_table_einsert(&_table, player->world_session_id,
        pl_get_index(player));
    /* Inventory
     * First find equipped items. They will contain the rest of the items. */
    dynamic_object_t *bag_dobjs[MAX_BAGS_PER_PLAYER] = {0};
    for (uint32 i = 0; i < num_items; ++i)
    {
        if (items[i].data.owner_type !=
            WDBC_DYNAMIC_OBJECT_OWNER_PLAYER_EQUIPPED)
            continue;
        equipment_slot_id_t equipment_slot =
            items[i].data.owner.player_equipped.equipment_slot;
        if (equipment_slot < BAG_SLOT1 || equipment_slot >= MAX_BAG_SLOT_ID)
            continue;
        dynamic_object_t *dobj = dobj_spawn_equipped_by_player(player,
            items[i].id, items[i].data.type_id, equipment_slot);
        if (!dobj)
        {
            LOG_ERROR("Can't spawn player: failed to spawn dynamic object as "
                "equpment for player.");
            goto fail;
        }
        int bag_index = equipment_slot - BAG_SLOT1;
        muta_assert(!bag_dobjs[bag_index]);
        bag_dobjs[equipment_slot - BAG_SLOT1]   = dobj;
        player->equipment[equipment_slot]       = dobj_get_index(dobj);
    }
    /* Add items to bags */
    for (uint32 i = 0; i < num_items; ++i)
    {
        if (items[i].data.owner_type !=
            WDBC_DYNAMIC_OBJECT_OWNER_PLAYER_CONTAINER)
            continue;
        equipment_slot_id_t slot_id =
            items[i].data.owner.player_container.equipment_slot;
        muta_assert(slot_id < NUM_EQUIPMENTS_SLOTS);
        if (player->equipment[slot_id] == DOBJ_INVALID_INDEX)
        {
            LOG_ERROR("Cannot add item to container because player's equipment "
                "slot %u is not in use.", (int)slot_id);
            goto fail;
        }
        dynamic_object_t *container_dobj = dobj_get(player->equipment[slot_id]);
        if (!dobj_is_container(container_dobj))
        {
            LOG_ERROR("Cannot add item to player's equipment slot %d because "
                "slot is not a container.", (int)slot_id);
            goto fail;
        }
        dynamic_object_def_t *item_def = ent_get_dynamic_object_def(
            items[i].data.type_id);
        uint32 w = item_def->width_in_container;
        uint32 h = item_def->height_in_container;
        if (!container_can_fit(&container_dobj->container,
            items[i].data.owner.player_container.x,
            items[i].data.owner.player_container.y, w, h))
        {
            LOG_ERROR("Cannot add item to player's equipment slot %d because "
                "it collides with another item.");
            goto fail;
        }
        dynamic_object_t *item_dobj = dobj_spawn_in_container(container_dobj,
            items[i].id, items[i].data.type_id,
            items[i].data.owner.player_container.x,
            items[i].data.owner.player_container.y);
        muta_assert(item_dobj);
    }
    /* Notify sim server of the spawn. */
    tsimmsg_request_spawn_player_t s =
    {
        .db_id              = db_id,
        .world_session_id   = player->world_session_id,
        .instance_id        = instance->id,
        .x                  = position[0],
        .y                  = position[1],
        .z                  = (uint8)position[2],
        .race               = (uint8)race,
        .direction          = (uint8)direction,
        .sex                = (uint8)sex,
        .name.len           = (uint8)strlen(player->name),
        .display_name.len   = (uint8)strlen(player->display_name),
        .move_speed         = move_speed
    };
    s.abilities.data[0] = 0; /* Temp: add auto attack ability. */
    s.abilities.len     = 1;
    memcpy(s.name.data, player->name, s.name.len);
    memcpy(s.display_name.data, player->display_name, s.display_name.len);
    bb = sim_send(part->sim_index,
        tsimmsg_request_spawn_player_compute_sz(&s));
    if (!bb.max_bytes)
    {
        LOG_EXT("Failed to send to sim.");
        player_table_erase(&_table, player->world_session_id);
        fixed_pool_free(&_players, player);
        return 0;
    }
    player->flags = PL_RESERVED;
    tsimmsg_request_spawn_player_write(&bb, &s);
    LOG("Requested to spawn player, name: %s, db_id: %" PRIu64 ", "
        "world_session_id: %u, position: (%d, %d, %d), race: %u, sex: %u, "
        "direction: %u, move_speed: %u.", player->name, player->db_id,
        player->world_session_id, player->position[0], player->position[1],
        player->position[2], (uint)player->race, (uint)player->sex,
        (uint)player->direction, (uint)player->move_speed);
    return player;
    fail:;
        svmsg_enter_world_fail_t fail_msg;
        fail_msg.reason = ENTER_WORLD_ERROR_INTERNAL;
        bb = cl_send_const_encrypted(client, SVMSG_ENTER_WORLD_FAIL_SZ);
        if (bb.max_bytes)
            svmsg_enter_world_fail_write(&bb, cl_get_cryptchan(client),
                &fail_msg);
        for (int i = 0; i < NUM_EQUIPMENTS_SLOTS; ++i)
        {
            if (player->equipment[i] == DOBJ_INVALID_INDEX)
                continue;
            dynamic_object_t *dobj = dobj_get(player->equipment[i]);
            for (uint32 i = 0; i < dobj->container.num_items; ++i)
                dobj_despawn_container_on_owner_spawn_failed(dobj);
        }
        player_table_erase(&_table, player->world_session_id);
        fixed_pool_free(&_players, player);
        return 0;
}

void
pl_request_despawn(player_t *player)
{
    /* This call is deferred to be handled as an event in
     * pl_complete_despawn_requests()
     * so that for-loops etc. iterating on players do not fail. */
    if (!(player->flags & PL_RELEVANT))
    {
        LOG_DEBUG_EXT("Already despawned.");
        return;
    }
    player->flags |= PL_DESPAWNING;
    player->flags &= ~PL_RELEVANT;
    player->client = 0;
    darr_push(_despawned, fixed_pool_index(&_players, player));
    if (darr_num(_despawned) - 1) /* Event already created */
        return;
    com_event_t event;
    event.type = COM_EVENT_DESPAWN_PLAYERS;
    com_push_events_no_wait(&event, 1);
}

uint32
pl_get_index(player_t *player)
    {return fixed_pool_index(&_players, player); }

player_t *
pl_get(uint32 player_index)
{
    muta_assert(player_index < _players.max);
    muta_assert(_players.all[player_index].flags & PL_RESERVED);
    return &_players.all[player_index];
}

player_t *
pl_find_by_world_session_id(player_runtime_id_t world_session_id)
{
    uint32 *index = player_table_find(&_table, world_session_id);
    if (!index)
        return 0;
    return &_players.all[*index];
}

player_t *
pl_find_by_account_id(account_db_id_t account_id)
{
    muta_assert(0);
}

int
pl_confirm_spawn(player_t *player, player_db_guid_t db_id,
    uint32 instance_part_id, int x, int y, int z,
    ability_id_t abilities[MAX_PLAYER_BAG_INDEX], uint32 num_abilities)
{
    cl_confirm_entered_world(player->client);
    if (player->db_id != db_id)
    {
        LOG_WORLD_EXT("Error: player database id was incorrect! "
            "Wanted: " PRIu64 ", received: " PRIu64 ".", player->db_id, db_id);
        return 1;
    }
    if (player->flags & PL_SPAWN_CONFIRMED)
    {
        LOG_WORLD_EXT("Spawn was already confirmed!");
        return 2;
    }
    instance_part_t *part = inst_find_part_by_id(instance_part_id);
    if (!part)
    {
        LOG_WORLD_EXT("Received invalid instance part id from world!");
        return 3;
    }
    if (part->sim_index != player->sim_index)
    {
        LOG_WORLD_EXT("Sim server spawned player in a part not owned by it!");
        return 4;
    }
    instance_t *instance = inst_get(part->instance_index);
    player->flags |= PL_SPAWN_CONFIRMED;
    player->flags |= PL_RELEVANT;
    player->instance_index  = part->instance_index;
    /* Send init data to player */
    bbuf_t bb = _send(player, SVMSG_ENTER_WORLD_START_SZ);
    if (!bb.max_bytes)
        return 5;
    svmsg_enter_world_start_write(&bb);
    svmsg_enter_world_end_t enter_world_end_msg =
    {
        .world_session_id   = player->world_session_id,
        .name.len           = (uint8)strlen(player->display_name),
        .map_id             = inst_map_id(instance),
        .x                  = x,
        .y                  = y,
        .z                  = z,
        .race               = player->race,
        .direction          = player->direction,
        .sex                = player->sex,
        .view_distance_h    = com_config.view_distance_h,
        .view_distance_v    = com_config.view_distance_v,
        .move_speed         = player->move_speed,
        .health_current     = 100,
        .health_max         = 100,
        .dead               = (player->flags & PL_DEAD) ? 1 : 0
    };
    memcpy(enter_world_end_msg.name.data, player->display_name,
        enter_world_end_msg.name.len);
    memcpy(enter_world_end_msg.abilities.data, abilities,
        num_abilities * sizeof(abilities[0]));
    enter_world_end_msg.abilities.len = (uint8)num_abilities;
    for (int i = 0; i < NUM_EQUIPMENTS_SLOTS; ++i)
    {
        if (player->equipment[i] == DOBJ_INVALID_INDEX)
            continue;
        dynamic_object_t *dobj = dobj_get(player->equipment[i]);
        svmsg_equip_on_me_t s1 =
        {
            .runtime_id     = dobj->runtime_id,
            .type_id        = dobj->type_id,
            .equipment_slot = (uint8)i
        };
        bb = _send(player, SVMSG_EQUIP_ON_ME_SZ);
        if (!bb.max_bytes)
            return 6;
        svmsg_equip_on_me_write(&bb, &s1);
        uint32 num_items = dobj->container.num_items;
        LOG_DEBUG("Sending %u items", num_items);
        for (uint32 j = 0; j < num_items; ++j)
        {
            container_item_t *item      = &dobj->container.items[j];
            dynamic_object_t *item_dobj = dobj_get(item->dobj_index);
            svmsg_add_inventory_item_t s2 =
            {
                .item =
                {
                    .runtime_id     = item_dobj->runtime_id,
                    .type_id        = item_dobj->type_id,
                    .x              = item->x,
                    .y              = item->y,
                    .equipment_slot = (uint8)i
                }
            };
            bb = _send(player, SVMSG_ADD_INVENTORY_ITEM_SZ);
            if (!bb.max_bytes)
                return 7;
            svmsg_add_inventory_item_write(&bb, &s2);
        }
    }
    bb = _send_var_encrypted(player,
        svmsg_enter_world_end_compute_sz(&enter_world_end_msg));
    if (!bb.max_bytes)
        return 8;
    svmsg_enter_world_end_write(&bb, cl_get_cryptchan(player->client),
        &enter_world_end_msg);
    _enter_new_instance(player, instance, x, y, z);
    LOG("Player spawn confirmed by sim, name: %s, db_id: %" PRIu64 ","
        "world_session_id: %u, position: (%d, %d, %d), race: %u, sex: %u, "
        "direction: %u, move_speed: %u.", player->name, player->db_id,
        player->world_session_id, player->position[0], player->position[1],
        player->position[2], (uint)player->race, (uint)player->sex,
        (uint)player->direction, (uint)player->move_speed);
    return 0;
}

int
pl_spawn_fail(player_t *player)
{
    if (player->flags & PL_SPAWN_CONFIRMED)
        return 1;
    svmsg_enter_world_fail_t s;
    s.reason = ENTER_WORLD_ERROR_UNKNOWN;
    bbuf_t bb = _send(player, SVMSG_ENTER_WORLD_FAIL_SZ);
    if (bb.max_bytes)
        svmsg_enter_world_fail_write(&bb, cl_get_cryptchan(player->client), &s);
    cl_on_spawn_player_failed(player->client);
    return 0;
}

void
pl_confirm_despawn(player_t *player)
{
    LOG("Sim confirmed despawn of player %u.", player->world_session_id);
    muta_assert(player->flags & PL_RESERVED);
    player->flags &= ~PL_RESERVED;
    player_table_erase(&_table, player->world_session_id);
    fixed_pool_free(&_players, player);
}

int
pl_confirm_move_player(player_t *player, int x, int y, int z)
{
    svmsg_move_me_t move_me_msg = {.x = x, .y = y, .z = (int8)z};
    bbuf_t bb = _send(player, SVMSG_MOVE_ME_SZ);
    if (bb.max_bytes)
        svmsg_move_me_write(&bb, &move_me_msg);
    /* Send move messages to followers */
    svmsg_move_player_t move_player_msg = {
        .world_session_id = player->world_session_id, .x = x, .y = y,
        .z = (int8)z};
    uint32 num_followers = pil_num_followers(player);
    for (uint32 i = 0; i < num_followers; ++i)
    {
        player_t *other = pil_get_follower(player, i);
        bbuf_t bb = cl_send(other->client, SVMSG_MOVE_PLAYER_SZ);
        if (bb.max_bytes)
            svmsg_move_player_write(&bb, &move_player_msg);
    }
    player->direction = vec2_to_iso_dir(x - player->position[0],
        y - player->position[1]);
    int ret = _change_position_within_instance(player, x, y, z);
    if (!ret)
    {
        wdbc_query_save_player_character_position_and_direction(
            _on_complete_query_save_player_character_position_and_direction,
            0, player->db_id, inst_get(player->instance_index)->id,
            player->direction, player->position[0], player->position[1],
            (uint8)player->position[2]);
    }
    return ret;
}

int
pl_confirm_teleport_player_within_current_instance(player_t *player, int x,
    int y, int z)
{
    int new_position[3] = {x, y, z};
    svmsg_update_my_position_t s = {.x = x, .y = y, .z = (int8)z};
    bbuf_t bb = _send(player, SVMSG_UPDATE_MY_POSITION_SZ);
    if (bb.max_bytes)
        svmsg_update_my_position_write(&bb, &s);
    /* Send update position messages to followers, or remove player messages if
     * the player became culled so as not to tell those clients the exact
     * position of the teleporting player. */
    svmsg_update_player_position_t update_msg = {
        .world_session_id = player->world_session_id, .x = x, .y = y,
        .z = (int8)z};
    svmsg_remove_player_t remove_msg = {
        .world_session_id = player->world_session_id};
    uint32 num_followers = pil_num_followers(player);
    for (uint32 i = 0; i < num_followers; ++i)
    {
        player_t *other = pil_get_follower(player, i);
        if (pil_should_cull(new_position, other->position))
        {
            bbuf_t bb = _send(player, SVMSG_REMOVE_PLAYER_SZ);
            if (bb.max_bytes)
                svmsg_remove_player_write(&bb, &remove_msg);
        } else
        {
            bbuf_t bb = _send(player,
                SVMSG_UPDATE_PLAYER_POSITION_SZ);
            if (bb.max_bytes)
                svmsg_update_player_position_write(&bb, &update_msg);
        }
    }
    return _change_position_within_instance(player, x, y, z);
}

int
pl_confirm_set_player_direction(player_t *player, int direction)
{
    muta_assert(direction >= 0 && direction < NUM_ISODIRS);
    if (player->direction == (uint8)direction)
        return 0 ;
    player->direction = (uint8)direction;
    /* Inform player */
    svmsg_update_my_direction_t my_dir_msg;
    my_dir_msg.direction = (uint8)direction;
    bbuf_t bb = _send(player, SVMSG_UPDATE_MY_DIRECTION_SZ);
    if (bb.max_bytes)
        svmsg_update_my_direction_write(&bb, &my_dir_msg);
    /* Inform others */
    svmsg_update_player_direction_t s;
    s.world_session_id  = player->world_session_id;
    s.direction         = (uint8)direction;
    uint32 num_followers = pil_num_followers(player);
    for (uint32 i = 0; i < num_followers; ++i)
    {
        player_t *other = pil_get_follower(player, i);
        bbuf_t bb = cl_send(other->client, SVMSG_UPDATE_PLAYER_DIRECTION_SZ);
        if (bb.max_bytes)
            svmsg_update_player_direction_write(&bb, &s);
    }
    return 0;
}

int
pl_confirm_teleport_player_to_another_instance(player_t *player,
    uint32 instance_id, int x, int y, int z)
{
    instance_t *inst = inst_find(instance_id);
    if (!inst)
        return 1;
    instance_part_t *part = inst_find_part_by_position(inst, x, y);
    if (player->sim_index != part->sim_index)
        return 2;
    _leave_current_instance(player);
    _enter_new_instance(player, inst, x, y, z);
    return 0;
}

int
pl_on_use_ability_result(player_t *player, ability_id_t ability_id,
    enum common_entity_type target_type, uint32 target_id,
    enum sim_player_use_ability_result result)
{
    svmsg_use_ability_result_t player_msg =
    {
        .ability_id  = ability_id,
        .target_type = target_type,
        .target_id   = target_id
    };
    switch (result)
    {
    case SIM_PLAYER_USE_ABILITY_RESULT_SUCCESS:
        player_msg.success = 1;
        break;
    case SIM_PLAYER_USE_ABILITY_RESULT_FAIL:
        player_msg.success = 0;
        break;
    case SIM_PLAYER_USE_ABILITY_RESULT_PLAYER_HAS_NO_SPELL:
        LOG_ERROR("Player has no spell %" PRIu64 ".", ability_id);
        cl_disconnect_for_illegal_action(player->client);
        return 0;
    case SIM_PLAYER_USE_ABILITY_RESULT_ALREADY_CASTING:
        LOG_ERROR("Player is already casting.");
        cl_disconnect_for_illegal_action(player->client);
        return 0;
    default:
        return 1;
    }
    bbuf_t bb = cl_send(player->client, SVMSG_USE_ABILITY_RESULT_SZ);
    if (bb.max_bytes)
        svmsg_use_ability_result_write(&bb, &player_msg);
    if (result == SIM_PLAYER_USE_ABILITY_RESULT_SUCCESS)
    {
        svmsg_player_used_ability_t follower_msg =
        {
            .ability_id          = ability_id,
            .player_runtime_id   = player->world_session_id,
            .target_runtime_id   = target_id,
            .target_type         = target_type
        };
        uint32 num_followers = pil_num_followers(player);
        for (uint32 i = 0; i < num_followers; ++i)
        {
            player_t *follower = pil_get_follower(player, i);
            bb = _send(follower, SVMSG_PLAYER_USED_ABILITY_SZ);
            if (bb.max_bytes)
                svmsg_player_used_ability_write(&bb, &follower_msg);
        }
    }
    return 0;
}

int
pl_on_player_finished_ability_charge(player_t *player, ability_id_t ability_id,
    enum common_entity_type target_type, uint32 target_runtime_id,
    enum common_ability_charge_result result)
{
    svmsg_player_finished_ability_charge_t s =
    {
        .ability_id          = ability_id,
        .result              = (uint8)result,
        .player_runtime_id   = player->world_session_id,
        .target_runtime_id   = target_runtime_id,
        .target_type         = (uint8)target_type
    };
    bbuf_t bb = _send(player, SVMSG_PLAYER_FINISHED_ABILITY_CHARGE_SZ);
    if (bb.max_bytes)
        svmsg_player_finished_ability_charge_write(&bb, &s);
    uint32 num_followers = pil_num_followers(player);
    for (uint32 i = 0; i < num_followers; ++i)
    {
        player_t *follower = pil_get_follower(player, i);
        bb = _send(follower, SVMSG_PLAYER_FINISHED_ABILITY_CHARGE_SZ);
        if (bb.max_bytes)
            svmsg_player_finished_ability_charge_write(&bb, &s);
    }
    return 0;
}

void
pl_complete_despawn_requests(void)
{
    uint32 num = darr_num(_despawned);
    for (uint32 i = 0; i < num; ++i)
    {
        player_t *player = &_players.all[_despawned[i]];
        muta_assert(player->flags & PL_DESPAWNING);
        uint32 world_session_id = player->world_session_id;
        tsimmsg_request_despawn_player_t s;
        s.world_session_id = player->world_session_id;
        if (player->flags & PL_SPAWN_CONFIRMED)
        {
            _clear_followeds(player);
            /* Send despawn messages to other players */
            pil_player_follower_t **followers =
                &player->interest_list.player_followers;
            svmsg_remove_player_t s;
            s.world_session_id = world_session_id;
            while (darr_num(*followers))
            {
                /* Inform follower that the player despawned. */
                player_t *other = pl_get((*followers)[0].player_index);
                bbuf_t bb = cl_send(other->client, SVMSG_REMOVE_PLAYER_SZ);
                if (bb.max_bytes)
                    svmsg_remove_player_write(&bb, &s);
                pil_stop_being_followed(player, 0);
            }
            /* Delete from interest area */
            ia_del_player(player);
        }
        bbuf_t bb = sim_send(player->sim_index,
            TSIMMSG_REQUEST_DESPAWN_PLAYER_SZ);
        if (bb.max_bytes)
            tsimmsg_request_despawn_player_write(&bb, &s);
        muta_assert(!darr_num(player->interest_list.players));
        muta_assert(!darr_num(player->interest_list.player_followers));
    }
    darr_clear(_despawned);
}

bool32
pl_despawning(player_t *player)
    {return player->flags & PL_DESPAWNING;}

void
pl_request_find_path(player_t *player, int x, int y, int z)
{
    if (player->position[0] == x && player->position[1] == y &&
        player->position[2] == z)
        return;
    bbuf_t bb = sim_send(player->sim_index,
        TSIMMSG_REQUEST_FIND_PATH_PLAYER_SZ);
    if (!bb.max_bytes)
        return;
    tsimmsg_request_find_path_player_t s;
    s.world_session_id  = player->world_session_id;
    s.x                 = x;
    s.y                 = y;
    s.z                 = z;
    tsimmsg_request_find_path_player_write(&bb, &s);
}

void
pl_request_move_in_direction(player_t *player, int direction)
{
    muta_assert(direction >= 0 && direction <= NUM_ISODIRS);
    bbuf_t bb = sim_send(player->sim_index,
        TSIMMSG_REQUEST_MOVE_PLAYER_IN_DIRECTION_SZ);
    if (!bb.max_bytes)
        return;
    tsimmsg_request_move_player_in_direction_t s;
    s.world_session_id  = player->world_session_id;
    s.direction         = (uint8)direction;
    tsimmsg_request_move_player_in_direction_write(&bb, &s);
}

void
pl_request_teleport(player_t *player, uint32 instance_index, int x, int y,
    int z)
{
    instance_t      *inst = inst_get(player->instance_index);
    instance_part_t *part = inst_find_part_by_position(inst, x, y);
    if (part->sim_index == player->sim_index)
    {
        if (instance_index == player->instance_index)
            _teleport_within_current_instance_and_sim(player, instance_index, x, y,
                z);
        else
            _teleport_to_different_instance_within_current_sim(player, part,
                x, y, z);
    } else
        _teleport_to_new_instance_and_sim(player, part, x, y, z);
}

void
pl_request_chat_message(player_t *player, const char *msg, uint32 msg_len)
{
    svmsg_global_chat_broadcast_t s;
    strncpy((char*)s.name.data, player->name, sizeof(s.name) - 1);
    s.name.len = strlen(player->name);
    muta_assert(msg_len <= sizeof(s.message.data));
    memcpy(s.message.data, msg, msg_len);
    s.message.len = (uint8)msg_len;
    int size = svmsg_global_chat_broadcast_compute_sz(&s);
    uint32 a, b;
    hashtable_for_each_pair(_table, a, b)
    {
        player_t *player = &_players.all[b];
        if (!(player->flags & PL_RELEVANT))
            continue;
        bbuf_t bb = _send(player, size);
        if (bb.max_bytes)
            svmsg_global_chat_broadcast_write(&bb, &s);
    }
}

void
pl_request_emote(player_t *player, uint16 emote_id)
{
    IMPLEMENTME();
}

int
pl_request_use_ability(player_t *player, ability_id_t ability_id,
    enum common_entity_type entity_type, uint32 target_id)
{
    if (entity_type != COMMON_ENTITY_PLAYER &&
        entity_type != COMMON_ENTITY_DYNAMIC_OBJECT &&
        entity_type != COMMON_ENTITY_CREATURE)
    {
        LOG_DEBUG_EXT("Bad target type.");
        return 1;
    }
    tsimmsg_request_player_use_ability_t s =
    {
        .player_runtime_id   = player->world_session_id,
        .target_runtime_id   = target_id,
        .target_type         = (uint8)entity_type
    };
    bbuf_t bb = sim_send(player->sim_index,
        TSIMMSG_REQUEST_PLAYER_USE_ABILITY_SZ);
    if (!bb.max_bytes)
        return 0;
    tsimmsg_request_player_use_ability_write(&bb, &s);
    return 0;
}

dynamic_object_t *
pl_get_equipment_slot(player_t *player, enum equipment_slot equipment_slot)
{
    uint32 dobj_index = player->equipment[equipment_slot];
    if (dobj_index == DOBJ_INVALID_INDEX)
        return 0;
    return dobj_get(dobj_index);
}

int
pl_request_move_inventory_item(player_t *pl, dobj_runtime_id_t dobj_runtime_id,
    enum equipment_slot new_equipment_slot, int new_x, int new_y)
{
    if (pl->equipment[new_equipment_slot] == DOBJ_INVALID_INDEX)
    {
        LOG_ERROR("Player cannot move inventory item: target equipment slot %d "
            "not equipped.", (int)new_equipment_slot);
        return 1;
    }
    dynamic_object_t *container_dobj = dobj_get(
        pl->equipment[new_equipment_slot]);
    if (!dobj_is_container(container_dobj))
    {
        LOG_ERROR("Player cannot move inventory item: target equipment slot %d "
            "is not a container.", dobj_runtime_id);
        return 1;
    }
    dynamic_object_t *dobj_to_move = dobj_find(dobj_runtime_id);
    if (!dobj_to_move)
    {
        LOG_ERROR("Player cannot move inventory item: dynamic object with "
            "runtime id %u does not exist.", dobj_runtime_id);
        return 1;
    }
    if (dobj_to_move->flags & DOBJ_FLAG_UNCONFIRMED_DB_STATE)
    {
        LOG_ERROR("Player cannot move inventory item: dynamic object %u has "
            "unconfirmed database state.", dobj_runtime_id);
        goto move_fail;
    }
    if (!(dobj_to_move->flags & DOBJ_FLAG_IN_CONTAINER))
    {
        LOG_ERROR("Player cannot move inventory item: dynamic object %u is not "
            "in a container.", dobj_runtime_id);
        return 1;
    }
    uint32 player_index = pl_get_index(pl);
    if (dobj_to_move->in.container.dobj_index == player_index)
    {
        LOG_ERROR("Player cannot move inventory item: dynamic object %u not "
            "owned by this player.", dobj_runtime_id);
        return 1;
    }
    if (dobj_to_move->in.container.dobj_index == dobj_get_index(container_dobj)
        && dobj_to_move->in.container.position[0] == (uint8)new_x
        && dobj_to_move->in.container.position[1] == (uint8)new_y)
    {
        LOG_DEBUG("Player attempted to move inventory item, but already at "
            "given position.");
        return 0;
    }
    dynamic_object_def_t *def = ent_get_dynamic_object_def(
        dobj_to_move->type_id);
    container_t *container = &container_dobj->container;
    if (!container_can_move(container, dobj_get_index(dobj_to_move),
            (uint32)new_x, (uint32)new_y, def->width_in_container,
            def->height_in_container))
    {
        LOG_ERROR("Player cannot move inventory item: container cannot fit "
            "dynamic object %u.", dobj_runtime_id);
        return 1;
    }
    save_dynamic_object_position_in_player_inventory_query_data_t query_data =
    {
        .player_runtime_id  = pl->world_session_id,
        .dobj_runtime_id    = dobj_runtime_id
    };
    void *user_data = segpool_malloc(&com_main_thread_segpool,
        sizeof(query_data));
    memcpy(user_data, &query_data, sizeof(query_data));
    wdbc_query_id_t query_id =
        wdbc_query_save_dynamic_object_position_in_player_inventory(
            _on_complete_query_save_dynamic_object_position_in_player_inventory,
            user_data, dobj_to_move->db_uuid,
            container_dobj->in.equipped_by_player.equipment_slot, (uint8)new_x,
            (uint8)new_y);
    if (query_id == WDBC_INVALID_QUERY_ID)
    {
        LOG_ERROR("Player cannot move inventory item: failed to save changes "
            "to database.", dobj_runtime_id);
        goto move_fail;
    }
    uint32 dobj_to_move_index = dobj_get_index(dobj_to_move);
    dynamic_object_t *previous_container_dobj =
        dobj_get(dobj_to_move->in.container.dobj_index);
    container_item_t *old_item = container_find_item(
        &previous_container_dobj->container, dobj_to_move_index);
    muta_assert(old_item);
    if (previous_container_dobj != container_dobj)
    {
        container_remove_item(&previous_container_dobj->container,
            (uint32)(old_item - previous_container_dobj->container.items));
        container_item_t new_item =
        {
            .dobj_index = dobj_to_move_index,
            .x          = (uint8)new_x,
            .y          = (uint8)new_y,
            .w          = def->width_in_container,
            .h          = def->height_in_container
        };
        container_add_item(&container_dobj->container, new_item);
    } else
    {
        old_item->x = (uint8)new_x;
        old_item->y = (uint8)new_y;
    }
    dobj_to_move->in.container.dobj_index   = dobj_get_index(container_dobj);
    dobj_to_move->in.container.position[0]  = (uint8)new_x;
    dobj_to_move->in.container.position[1]  = (uint8)new_y;
    return 0;
    move_fail:;
        svmsg_move_inventory_item_fail_t s =
        {
            .dobj_runtime_id = dobj_runtime_id
        };
        bbuf_t bb = cl_send(pl->client, SVMSG_MOVE_INVENTORY_ITEM_FAIL_SZ);
        if (bb.max_bytes)
            svmsg_move_inventory_item_fail_write(&bb, &s);
        return 0;
}

int
pl_request_stop_ability_charge(player_t *pl)
{
    tsimmsg_request_player_stop_ability_charge_t s =
    {
        .player_runtime_id = pl->world_session_id
    };
    bbuf_t bb = sim_send(pl->sim_index,
        TSIMMSG_REQUEST_PLAYER_STOP_ABILITY_CHARGE_SZ);
    if (bb.max_bytes)
        tsimmsg_request_player_stop_ability_charge_write(&bb, &s);
    return 0;
}

void
pl_on_did_illegal_action_according_to_sim(player_t *pl, const char *reason)
{
    LOG_ERROR("Player %u did something illegal according to sim server: %s.",
        reason);
    cl_disconnect(pl->client);
}

void
pl_die(player_t *pl)
{
    IMPLEMENTME();
    muta_assert(!(pl->flags & PL_DEAD));
    pl->flags |= PL_DEAD;
}

dynamic_object_t *
pl_get_bag(player_t *player, uint32 bag_index)
{
    muta_assert(bag_index < MAX_BAGS_PER_PLAYER);
    uint32 equipment_index  = BAG_SLOT1 + bag_index;
    uint32 dobj_index       = player->equipment[equipment_index];
    if (dobj_index == DOBJ_INVALID_INDEX)
        return 0;
    return dobj_get(dobj_index);
}

static void
_fill_new_player_msg(svmsg_add_player_t *s, player_t *player)
{
    s->world_session_id = player->world_session_id;
    s->display_name.len = (uint8)strlen(player->display_name);
    memcpy(s->display_name.data, player->display_name, s->display_name.len);
    s->race             = player->race;
    s->x                = player->position[0];
    s->y                = player->position[1];
    s->z                = player->position[2];
    s->direction        = player->direction;
    s->sex              = player->sex;
    s->move_speed       = player->move_speed;
    s->health_current   = 100;
    s->health_max       = 100;
    s->dead             = player->flags & PL_DEAD ? 1 : 0;
}

static void
_clear_interest_list_of_culled(player_t *player)
{
    int position[3];
    for (int i = 0; i < 3; ++i)
        position[i] = player->position[i];
    for (uint32 i = 0; i < pil_num_followers(player); ++i)
        if (pil_should_cull(position, pil_get_follower(player, i)->position))
        {
            pil_stop_being_followed(player, i--);
            if (!pil_num_followers(player))
                break;
        }
    for (uint32 i = 0; i < pil_num_players(player); ++i)
        if (pil_should_cull(position, pil_get_player(player, i)->position))
        {
            pil_stop_following_player(player, i--);
            if (!pil_num_players(player))
                break;
        }
}

static void
_add_unculled_to_interest_list(player_t *player, int old_position[3])
{
    svmsg_add_player_t s1, s2;
    _fill_new_player_msg(&s1, player);
    int s1_size = svmsg_add_player_compute_sz(&s1);
    int position[3];
    for (int i = 0; i < 3; ++i)
        position[i] = player->position[i];
    instance_t      *instance = inst_get(player->instance_index);
    interest_area_t *interest_area;
    INST_FOR_EACH_RELEVANT_INTEREST_AREA(instance, position[0], position[1],
        position[2], interest_area)
    {
        player_t *other;
        IA_FOR_EACH_PLAYER(interest_area, other)
        {
            if (other == player)
                continue;
            if (pil_should_cull(position, other->position))
                continue;
            if (!pil_should_cull(old_position, other->position))
                continue;
            pil_follow_player(player, other);
            pil_follow_player(other, player);
            /* Send info of other to player */
            _fill_new_player_msg(&s2, other);
            bbuf_t bb = _send(player,
                svmsg_add_player_compute_sz(&s2));
            if (bb.max_bytes)
                svmsg_add_player_write(&bb, &s2);
            /* Send info of the player to other */
            bb = cl_send(other->client, s1_size);
            if (bb.max_bytes)
                svmsg_add_player_write(&bb, &s1);
        }
        dynamic_object_t *obj;
        IA_FOR_EACH_DYNAMIC_OBJECT(interest_area, obj)
        {
            if (pil_should_cull(player->position, obj->in.world.position))
                continue;
            if (!pil_should_cull(old_position, obj->in.world.position))
                continue;
            svmsg_add_dynamic_object_t s;
            dobj_serialize_to_msg(obj, &s);
            bbuf_t bb = _send(player, SVMSG_ADD_DYNAMIC_OBJECT_SZ);
            if (bb.max_bytes)
                svmsg_add_dynamic_object_write(&bb, &s);
        }
        creature_t *creature;
        IA_FOR_EACH_CREATURE(interest_area, creature)
        {
            if (pil_should_cull(player->position, creature->position))
                continue;
            if (!pil_should_cull(old_position, creature->position))
                continue;
            svmsg_add_creature_t s;
            creature_serialize_to_msg(creature, &s);
            bbuf_t bb = _send(player, SVMSG_ADD_CREATURE_SZ);
            if (bb.max_bytes)
                svmsg_add_creature_write(&bb, &s);
        }
    }
}

static void
_clear_followeds(player_t *player)
{
    while (darr_num(player->interest_list.players))
        pil_stop_following_player(player, 0);
}

static int
_change_position_within_instance(player_t *player, int x, int y, int z)
{
    int old_position[3];
    for (int i = 0; i < 3; ++i)
        old_position[i] = player->position[i];
    instance_t      *inst = inst_get(player->instance_index);
    instance_part_t *part = inst_find_part_by_position(inst, x, y);
    if (part->sim_index != player->sim_index)
        return 1;
    player->position[0] = x;
    player->position[1] = y;
    player->position[2] = z;
    interest_area_t *cur_ia =
        &inst->interest_areas[player->interest_area_index];
    interest_area_t *new_ia = inst_find_interest_area(inst, x, y, z);
    if (cur_ia != new_ia)
    {
        ia_del_player(player);
        ia_add_player(inst->interest_areas,
            (uint32)(new_ia - inst->interest_areas), player);
    }
    _clear_interest_list_of_culled(player);
    _add_unculled_to_interest_list(player, old_position);
    return 0;
}

static void
_teleport_within_current_instance_and_sim(player_t *player,
    uint32 instance_index, int x, int y, int z)
{
    if (player->position[0] == x && player->position[1] == y &&
        player->position[2] == z)
        return;
    tsimmsg_request_teleport_player_within_current_instance_t s;
    s.world_session_id  = player->world_session_id;
    s.x                 = x;
    s.y                 = y;
    s.z                 = z;
    bbuf_t bb = _send(player,
        TSIMMSG_REQUEST_TELEPORT_PLAYER_WITHIN_CURRENT_INSTANCE_SZ);
    if (bb.max_bytes)
        tsimmsg_request_teleport_player_within_current_instance_write(&bb, &s);
}

static void
_teleport_to_different_instance_within_current_sim(player_t *player,
    instance_part_t *part, int x, int y, int z)
{
    muta_assert(part->instance_index != player->instance_index);
    tsimmsg_request_teleport_player_to_another_instance_t s;
    s.world_session_id  = player->world_session_id;
    s.instance_part_id  = part->id;
    s.x                 = x;
    s.y                 = y;
    s.z                 = z;
    bbuf_t bb = _send(player,
        TSIMMSG_REQUEST_TELEPORT_PLAYER_TO_ANOTHER_INSTANCE_SZ);
    if (!bb.max_bytes)
        return;
    tsimmsg_request_teleport_player_to_another_instance_write(&bb, &s);
}

static void
_teleport_to_new_instance_and_sim(player_t *player, instance_part_t *part,
    int x, int y, int z)
{
    muta_assert(part->sim_index != player->sim_index);
    if (part->sim_index == SIM_INVALID_INDEX)
    {
        LOG_EXT("Target instance part id %u's sim server is offline!",
            part->id);
        return;
    }
    tsimmsg_request_change_player_sim_t s;
    s.world_session_id = player->world_session_id;
    bbuf_t bb = _send(player, TSIMMSG_REQUEST_CHANGE_PLAYER_SIM_SZ);
    if (bb.max_bytes)
        tsimmsg_request_change_player_sim_write(&bb, &s);
}

static void
_leave_current_instance(player_t *player)
{
    ia_del_player(player);
    svmsg_remove_player_t s;
    s.world_session_id = player->world_session_id;
    /* Send despawn messages of player to followers. */
    while (darr_num(player->interest_list.player_followers))
    {
        bbuf_t bb = cl_send(pl_get(
            player->interest_list.player_followers[0].player_index)->client,
            SVMSG_REMOVE_PLAYER_SZ);
        if (bb.max_bytes)
            svmsg_remove_player_write(&bb, &s);
        pil_stop_being_followed(player, 0);
    }
    _clear_followeds(player);
    player->instance_index = 0xFFFFFFFF;
}

static void
_enter_new_instance(player_t *player, instance_t *inst, int x, int y, int z)
{
    player->position[0]     = x;
    player->position[1]     = y;
    player->position[2]     = z;
    player->instance_index  = inst_get_index(inst);
    interest_area_t *ia = ia_find(inst->interest_areas, inst_w_in_tiles(inst),
        inst_h_in_tiles(inst), x, y, z);
    ia_add_player(inst->interest_areas, (uint32)(ia - inst->interest_areas),
        player);
    svmsg_add_player_t new_player_msg;
    _fill_new_player_msg(&new_player_msg, player);
    int msg_size = svmsg_add_player_compute_sz(&new_player_msg);
    interest_area_t *interest_area;
#ifdef _MUTA_DEBUG
    int num_iters = 0;
#endif
    INST_FOR_EACH_RELEVANT_INTEREST_AREA(inst, x, y, z, interest_area)
    {
        player_t *other;
        IA_FOR_EACH_PLAYER(interest_area, other)
        {
            if (other == player)
                continue;
            if (!PL_IS_RELEVANT(other))
                continue;
            if (pil_should_cull(player->position, other->position))
                continue;
            pil_follow_player(player, other);
            pil_follow_player(other, player);
            bbuf_t bb = cl_send(other->client, msg_size);
            svmsg_add_player_write(&bb, &new_player_msg);
            svmsg_add_player_t other_new_player_msg;
            _fill_new_player_msg(&other_new_player_msg, other);
            bb = _send(player, svmsg_add_player_compute_sz(
                &other_new_player_msg));
            if (bb.max_bytes)
                svmsg_add_player_write(&bb, &other_new_player_msg);
        }
        dynamic_object_t *obj;
        IA_FOR_EACH_DYNAMIC_OBJECT(interest_area, obj)
        {
            if (pil_should_cull(player->position, obj->in.world.position))
                continue;
            svmsg_add_dynamic_object_t s;
            dobj_serialize_to_msg(obj, &s);
            bbuf_t bb = _send(player, SVMSG_ADD_DYNAMIC_OBJECT_SZ);
            if (bb.max_bytes)
                svmsg_add_dynamic_object_write(&bb, &s);
        }
        creature_t *creature;
        IA_FOR_EACH_CREATURE(interest_area, creature)
        {
            if (pil_should_cull(player->position, creature->position))
                continue;
            svmsg_add_creature_t s;
            creature_serialize_to_msg(creature, &s);
            bbuf_t bb = _send(player, SVMSG_ADD_CREATURE_SZ);
            if (bb.max_bytes)
                svmsg_add_creature_write(&bb, &s);
        }
#ifdef _MUTA_DEBUG
        num_iters++;
#endif
    }
    muta_assert(num_iters <= 27);
}

static bbuf_t
_send(player_t *pl, uint32 size)
{
    if (!pl->client)
    {
        bbuf_t bb = {0};
        return bb;
    }
    return cl_send(pl->client, size);
}

static inline bbuf_t
_send_const_encrypted(player_t *pl, uint32 size)
{
    if (!pl->client)
    {
        bbuf_t bb = {0};
        return bb;
    }
    return cl_send_const_encrypted(pl->client, size);
}

static inline bbuf_t
_send_var_encrypted(player_t *pl, uint32 size)
{
    if (!pl->client)
    {
        bbuf_t bb = {0};
        return bb;
    }
    return cl_send_var_encrypted(pl->client, size);
}

static void
_on_complete_query_save_player_character_position_and_direction(
    wdbc_query_id_t query_id, void *user_data, int error,
    player_db_guid_t player_db_id, uint32 instance_id, uint8 direction,
    int32 x, int32 y, uint8 z)
{
    if (!error)
        LOG_DEBUG_EXT("Successfully saved player character position and "
            "direction");
    else
        LOG_DEBUG_EXT("Failed to save player character position and direction, "
            "error %d.", error);
}

static void
_on_complete_query_save_dynamic_object_position_in_player_inventory(
    wdbc_query_id_t query_id, void *user_data, int error, uuid16_t dobj_db_uuid,
    equipment_slot_id_t equipment_slot_id, uint8 x, uint8 y)
{
    save_dynamic_object_position_in_player_inventory_query_data_t query_data;
    memcpy(&query_data, user_data, sizeof(query_data));
    segpool_free(&com_main_thread_segpool, user_data);
    if (error)
    {
        LOG_DEBUG_EXT("Failed to save dynamic object position in player "
            "inventory, error %d.", error);
        player_t *pl = pl_find_by_world_session_id(query_data.player_runtime_id);
        if (pl)
            cl_disconnect(pl->client);
        return;
    }
    LOG_DEBUG_EXT("Successfully saved dynamic object position in player "
        "inventory");
    player_t *pl = pl_find_by_world_session_id(query_data.player_runtime_id);
    if (!pl)
    {
        LOG_DEBUG_EXT("Player with runtime id %u is no longer online.",
            query_data.player_runtime_id);
        return;
    }
    svmsg_move_inventory_item_t s =
    {
        .dobj_runtime_id    = query_data.dobj_runtime_id,
        .new_equipment_slot = equipment_slot_id,
        .new_x              = x,
        .new_y              = y
    };
    bbuf_t bb = cl_send(pl->client, SVMSG_MOVE_INVENTORY_ITEM_SZ);
    if (bb.max_bytes)
        svmsg_move_inventory_item_write(&bb, &s);
}
