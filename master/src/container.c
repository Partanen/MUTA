#include "container.h"
#include "common.h"
#include "../../shared/common_utils.h"

void
container_init(container_t *container, uint8 w, uint8 h)
{
    container->num_items    = 0;
    container->w            = w;
    container->h            = h;
}

bool32
container_can_fit(container_t *container, uint32 x, uint32 y, uint32 obj_w,
    uint32 obj_h)
{
    muta_assert(container->w);
    muta_assert(container->h);
    if (container->num_items == MAX_BAG_SIZE)
    {
        LOG_DEBUG_EXT("Container is full.");
        return 0;
    }
    if (x + obj_w > container->w)
    {
        LOG_DEBUG_EXT("X coordinate out of range (object x and width: %u, %u, "
            "container width: %u.", x, obj_w, (uint)container->w);
        return 0;
    }
    if (y + obj_h > container->h)
    {
        LOG_DEBUG_EXT("Y coordinate out of range (object y and height: %u, %u, "
            "container width: %u.", y, obj_h, (uint)container->h);
        return 0;
    }
    uint32 num_items = container->num_items;
    for (uint32 i = 0; i < num_items; ++i)
    {
        container_item_t *item = &container->items[i];
        if (x < item->x + item->w && x + obj_w > item->x &&
            y < item->y + item->h && y + obj_h > item->y)
        {
            LOG_DEBUG_EXT("Object collides with another object in container. "
                "Existing object xywh: %u, %u, %u, %u, added item's xywh: %u, "
                "%u, %u %u.", (uint)item->x, (uint)item->y, (uint)item->w,
                (uint)item->h, x, y, obj_w, obj_h);
            return 0;
        }
    }
    return 1;
}

bool32
container_can_move(container_t *container, uint32 dobj_index, uint32 x,
    uint32 y, uint32 obj_w, uint32 obj_h)
{
    muta_assert(container->w);
    muta_assert(container->h);
    if (container->num_items == MAX_BAG_SIZE)
    {
        LOG_DEBUG_EXT("Container is full.");
        return 0;
    }
    if (x + obj_w > container->w)
    {
        LOG_DEBUG_EXT("X coordinate out of range (object x and width: %u, %u, "
            "container width: %u.", x, obj_w, (uint)container->w);
        return 0;
    }
    if (y + obj_h > container->h)
    {
        LOG_DEBUG_EXT("Y coordinate out of range (object y and height: %u, %u, "
            "container width: %u.", y, obj_h, (uint)container->h);
        return 0;
    }
    uint32 num_items = container->num_items;
    for (uint32 i = 0; i < num_items; ++i)
    {
        container_item_t *item = &container->items[i];
        if (item->dobj_index == dobj_index)
            continue;
        if (x < item->x + item->w && x + obj_w > item->x &&
            y < item->y + item->h && y + obj_h > item->y)
        {
            LOG_DEBUG_EXT("Object collides with another object in container. "
                "Existing object xywh: %u, %u, %u, %u, added item's xywh: %u, "
                "%u, %u %u.", (uint)item->x, (uint)item->y, (uint)item->w,
                (uint)item->h, x, y, obj_w, obj_h);
            return 0;
        }
    }
    return 1;
}

void
container_add_item(container_t *container, container_item_t item)
{
    muta_assert(container->num_items < MAX_BAG_SIZE);
    muta_assert(item.w);
    muta_assert(item.h);
    container->items[container->num_items++] = item;
}

void
container_remove_item(container_t *container, uint32 index)
{
    muta_assert(container->num_items);
    container->items[index] = container->items[--container->num_items];
}

container_item_t *
container_find_item(container_t *container, uint32 dobj_index)
{
    uint32 num_items = container->num_items;
    for (uint32 i = 0; i < num_items; ++i)
        if (container->items[i].dobj_index == dobj_index)
            return &container->items[i];
    return 0;
}
