/* player.h
 * Player character control on the sim server. Communicating with sim servers
 * regarding player characters is mostly accomplished through this API. */

#ifndef MUTA_SERVER_PLAYER_H
#define MUTA_SERVER_PLAYER_H

#include "../../shared/common_defs.h"
#include "../../shared/sv_common_defs.h"
#include "interest_list.h"

/* Forward declaration(s) */
typedef struct client_t                     client_t;
typedef struct wdbc_dynamic_object_in_db_t  wdbc_dynamic_object_in_db_t;
typedef struct dynamic_object_t             dynamic_object_t;

/* Types defined here */
typedef struct player_t player_t;

enum player_flag
{
    PL_SPAWN_CONFIRMED  = (1 << 0),
    /* ^ True once sim confirms spawn */
    PL_DESPAWNING       = (1 << 1),
    /* ^ Will despawn at the end of frame */
    PL_RELEVANT         = (1 << 2),
    PL_RESERVED         = (1 << 3),
    PL_DEAD             = (1 << 4)
};

struct player_t
{
    player_db_guid_t    db_id;
    player_runtime_id_t world_session_id;
    /* ^ Id generated every time pl_request_spawn() is called. It's used to
     * communicate about the player between the world and the master server, as
     * well as between master and clients. */
    client_t            *client;
    /* ^ The client that controls this player character. Should always be valid
     * during a player_t's lifetime. */
    int                 position[3];
    uint32              sim_index;
    uint32              instance_index;
    uint32              num_unfinished_async_actions;
    /* Can't log out before all async actions such as DB requests are finished.
     * (The client can log out, but the player character will stay online). */
    char                name[MAX_CHARACTER_NAME_LEN + 1];
    char                display_name[MAX_DISPLAY_NAME_LEN + 1];
    /* ^ The name currently shown to other players or other objects. May differ
     * from the character's actual name under special circumstances. */
    uint32              interest_area_index;
    uint32              interest_area_next;
    uint32              interest_area_prev;
    /* ^ Indices to other players. Linked list of players in the current
     * interest area. 0xFFFFFFFF if none. */
    pl_interest_list_t  interest_list;
    uint32              equipment[NUM_EQUIPMENTS_SLOTS];
    struct
    {
        uint32 current;
        uint32 max;
    } health;
    /* ^ Relevant if correct flag is set. */
    uint8               flags;
    uint8               race;
    uint8               sex;
    uint8               direction;
    uint8               move_speed;
};

int
pl_init(void);

void
pl_destroy(void);

void
pl_on_sim_disconnected(uint32 sim_index);
/* Disconnect all players currently simulated by sim at sim_index. */

player_t *
pl_request_spawn(client_t *client, player_db_guid_t id, const char *name,
    int race, int sex, uint32 instance_id, int position[3], int direction,
    uint8 move_speed, wdbc_dynamic_object_in_db_t *items, uint32 num_items);

void
pl_request_despawn(player_t *player);
/* This function sets the player clientless. */

void
pl_free(player_t *player);

uint32
pl_get_index(player_t *player);

player_t *
pl_get(uint32 player_index);

player_t *
pl_find_by_world_session_id(player_runtime_id_t world_session_id);

player_t *
pl_find_by_account_id(account_db_id_t account_id);

/* The pl_confirm class of functions are called when a sim_server confirms (or
 * completes by itself) an action.
 * Return values of non-zero indicate something unexpected happened, indicating
 * a possible bug in the values given by the sim_server. */

int
pl_confirm_spawn(player_t *player, player_db_guid_t db_id,
    uint32 instance_part_id, int x, int y, int z,
    ability_id_t abilities[MAX_PLAYER_BAG_INDEX], uint32 num_abilities);

int
pl_spawn_fail(player_t *player);

void
pl_confirm_despawn(player_t *player);

int
pl_confirm_move_player(player_t *player, int x, int y, int z);

int
pl_confirm_teleport_player_within_current_instance(player_t *player, int x,
    int y, int z);
/* pl_confirm_teleport_player_within_current_instance()
 * Called when the sim and instance of the player remained the same after the
 * teleport. */

int
pl_confirm_set_player_direction(player_t *player, int direction);

int
pl_confirm_teleport_player_to_another_instance(player_t *player,
    uint32 instance_id, int x, int y, int z);
/* pl_confirm_teleport_player_to_another_instance()
 * Called when a player moves to a different instance but remains on it's old
 * sim. */

int
pl_on_use_ability_result(player_t *player, ability_id_t ability_id,
    enum common_entity_type target_type, uint32 target_id,
    enum sim_player_use_ability_result result);

int
pl_on_player_finished_ability_charge(player_t *player, ability_id_t ability_id,
    enum common_entity_type target_type, uint32 target_runtime_id,
    enum common_ability_charge_result result);

void
pl_complete_despawn_requests(void);
/* pl_complete_despawns()
 * Called on COM_EVENT_DESPAWN_PLAYERS. */

bool32
pl_despawning(player_t *player);

void
pl_request_find_path(player_t *player, int x, int y, int z);

void
pl_request_move_in_direction(player_t *player, int direction);

void
pl_request_teleport(player_t *player, uint32 dst_instance_index, int x, int y,
    int z);

void
pl_request_chat_message(player_t *player, const char *msg, uint32 msg_len);

void
pl_request_emote(player_t *player, uint16 emote_id);

int
pl_request_use_ability(player_t *player, ability_id_t ability_id,
    enum common_entity_type entity_type, uint32 target_id);
/* Expects entity_type has already checked to be either creature, player or
 * dynamic object. */

dynamic_object_t *
pl_get_equipment_slot(player_t *player, enum equipment_slot equipment_slot);
/* Returns null if doesn't exist. */

int
pl_request_move_inventory_item(player_t *pl, dobj_runtime_id_t dobj_runtime_id,
    enum equipment_slot new_equipment_slot, int new_x, int new_y);
/* Returns non-zero if player did something illegal. */

int
pl_request_stop_ability_charge(player_t *pl);

void
pl_on_did_illegal_action_according_to_sim(player_t *pl, const char *reason);
/* Disconnects the player. */

void
pl_die(player_t *pl);

#endif /* MUTA_SERVER_PLAYER_H */
