#include "pickup.h"
#include "player.h"
#include "dynamic_object.h"
#include "sim_server.h"
#include "common.h"
#include "client.h"
#include "container.h"
#include "interest_area.h"
#include "instance.h"
#include "../../shared/sim_packets.h"
#include "../../shared/client_packets.h"
#include "../../shared/entities.h"

typedef struct query_data_t query_data_t;

struct query_data_t
{
    player_runtime_id_t world_session_id;
    dobj_runtime_id_t   dobj_runtime_id;
};

static void
_on_complete_query_create_dynamic_object_in_player_container(
    wdbc_query_id_t query_id, void *user_data, int error,
    wdbc_dynamic_object_in_db_t *dobj);

int
pickup_request_player_pick_up_dynamic_object(player_t *pl,
    dynamic_object_t *dobj, enum equipment_slot equipment_slot, uint32 x,
    uint32 y)
{
    const char *err_msg = 0;
    if (dobj->flags & DOBJ_FLAG_UNCONFIRMED_DB_STATE)
    {
        err_msg = "object has unconfirmed database state.";
        goto pickup_fail;
    }
    if (!dobj_is_in_world(dobj))
    {
        err_msg = "object is already inside a container";
        goto pickup_fail;
    }
    dynamic_object_def_t *def = ent_get_dynamic_object_def(dobj->type_id);
    if (def->flags & ENT_DYNAMIC_OBJECT_DISABLE_PICK_UP)
    {
        LOG_DEBUG_EXT("Player %u cannot pick up dynamic object %u: object "
            "pickup disabled.", pl->world_session_id, dobj->runtime_id);
        return 1;
    }
    int32 x_distance = abs(pl->position[0] - dobj->in.world.position[0]);
    int32 y_distance = abs(pl->position[1] - dobj->in.world.position[1]);
    int32 z_distance = pl->position[2] - dobj->in.world.position[2];
    if (x_distance > 1 || y_distance > 1 || z_distance != 0)
    {
        err_msg = "object is too far away";
        goto pickup_fail;
    }
    if (dobj->in.world.instance_index != pl->instance_index)
    {
        err_msg = "object is in a different instance";
        goto pickup_fail;
    }
    dynamic_object_t *bag_dobj = pl_get_equipment_slot(pl, equipment_slot);
    if (!bag_dobj)
    {
        LOG_DEBUG_EXT("Player %u cannot pick up dynamic object %u: bag %u not "
            "in use.", pl->world_session_id, dobj->runtime_id);
        goto pickup_fail;
    }
    if (!dobj_is_container(bag_dobj))
    {
        LOG_DEBUG_EXT("Player %u cannot pick up dynamic object %u: target is "
            "not a container.", pl->world_session_id, dobj->runtime_id);
        return 1;
    }
    uint8 w = def->width_in_container;
    uint8 h = def->height_in_container;
    if (!container_can_fit(&bag_dobj->container, x, y, w, h))
    {
        err_msg = "cannot fit object in container";
        goto pickup_fail;
    }
    /* Send database request */
    query_data_t *user_data = segpool_malloc(&com_main_thread_segpool,
        sizeof(query_data_t));
    user_data->world_session_id = pl->world_session_id;
    user_data->dobj_runtime_id  = dobj->runtime_id;
    wdbc_query_id_t query_id =
        wdbc_query_create_dynamic_object_in_player_container(
        _on_complete_query_create_dynamic_object_in_player_container, user_data,
        dobj->type_id, pl->db_id, equipment_slot, x, y);
    if (query_id == WDBC_INVALID_QUERY_ID)
    {
        err_msg = "wdbc_query_create_dynamic_object_in_player_container() "
            "failed.";
        goto pickup_fail;
    }
    /* Move object to player bag */
    container_item_t container_item =
    {
        .dobj_index = dobj_get_index(dobj),
        .x          = x,
        .y          = y,
        .w          = w,
        .h          = h
    };
    container_add_item(&bag_dobj->container, container_item);
    muta_assert(!(dobj->flags & DOBJ_FLAG_IN_CONTAINER));
    muta_assert(!(dobj->flags & DOBJ_FLAG_EQUIPPED));
    /* Delete from ia */
    ia_del_dynamic_object(dobj);
    /* Inform nearby players of object being picked up. */
    instance_t *inst = inst_get(dobj->in.world.instance_index);
    interest_area_t *interest_area;
    int dobj_xyz[3];
    for (int i = 0; i < 3; ++i)
        dobj_xyz[i] = dobj->in.world.position[i];
    INST_FOR_EACH_RELEVANT_INTEREST_AREA(inst, dobj_xyz[0], dobj_xyz[1],
        dobj_xyz[2], interest_area)
    {
        player_t *player;
        IA_FOR_EACH_PLAYER(interest_area, player)
        {
            if (pil_should_cull(player->position, dobj_xyz))
                continue;
            svmsg_remove_dynamic_object_t s = {.runtime_id = dobj->runtime_id};
            bbuf_t bb = cl_send(player->client, SVMSG_REMOVE_DYNAMIC_OBJECT_SZ);
            if (bb.max_bytes)
                svmsg_remove_dynamic_object_write(&bb, &s);
        }
    }
    uint32 dobj_sim_index = dobj->in.world.sim_index;
    dobj->flags |= DOBJ_FLAG_IN_CONTAINER;
    dobj->flags |= DOBJ_FLAG_UNCONFIRMED_DB_STATE;
    dobj->in.container.dobj_index   = dobj_get_index(bag_dobj);
    dobj->in.container.position[0]  = x;
    dobj->in.container.position[1]  = y;
    /* Tell sim to despawn object. */
    tsimmsg_request_despawn_dynamic_object_t s1 =
    {
        .runtime_id = dobj->runtime_id
    };
    bbuf_t bb = sim_send(dobj_sim_index,
        TSIMMSG_REQUEST_DESPAWN_DYNAMIC_OBJECT_SZ);
    if (bb.max_bytes)
        tsimmsg_request_despawn_dynamic_object_write(&bb, &s1);
    tsimmsg_despawn_noncontrolled_dynamic_object_t s2 =
    {
        .runtime_id = dobj->runtime_id
    };
    uint32 sim_indices[8];
    uint32 num_sims = dobj_get_relevant_noncontrolling_sims(dobj, sim_indices);
    for (uint32 i = 0; i < num_sims; ++i)
    {
        bb = sim_send(sim_indices[i],
            TSIMMSG_DESPAWN_NONCONTROLLED_DYNAMIC_OBJECT_SZ);
        if (bb.max_bytes)
            tsimmsg_despawn_noncontrolled_dynamic_object_write(&bb, &s2);
    }
    return 0;
    pickup_fail:
        if (err_msg)
            LOG_DEBUG_EXT("Player cannot pick up dynamic object %u: %s.",
                dobj->runtime_id, err_msg);
        svmsg_pick_up_dynamic_object_fail_t s =
        {
            .runtime_id = dobj->runtime_id
        };
        bb = cl_send(pl->client,
            SVMSG_PICK_UP_DYNAMIC_OBJECT_FAIL_SZ);
        if (bb.max_bytes)
            svmsg_pick_up_dynamic_object_fail_write(&bb, &s);
        return 0;
}

static void
_on_complete_query_create_dynamic_object_in_player_container(wdbc_query_id_t query_id,
    void *user_data, int error, wdbc_dynamic_object_in_db_t *obj_data)
{
    query_data_t data;
    memcpy(&data, user_data, sizeof(data));
    segpool_free(&com_main_thread_segpool, user_data);
    if (error)
    {
        LOG_ERROR("Query failed. TODO: what to do in this case?");
        return;
    }
    player_t *pl = pl_find_by_world_session_id(data.world_session_id);
    if (!pl)
    {
        /* If the player was despawned, the dynamic object must have been too. */
        return;
    }
    dynamic_object_t *dobj = dobj_find(data.dobj_runtime_id);
    muta_assert(dobj);
    muta_assert(dobj->flags & DOBJ_FLAG_UNCONFIRMED_DB_STATE);
    muta_assert(!dobj_is_in_world(dobj));
    dobj->flags &= ~DOBJ_FLAG_UNCONFIRMED_DB_STATE;
    dobj->db_uuid = obj_data->id;
    LOG_EXT("Completed dynamic object creation query successfully.", error);
    LOG_DEBUG_EXT("Received db in a callback: should this happen immediately?");
    dynamic_object_t *container_dobj = dobj_get(dobj->in.container.dobj_index);
    equipment_slot_id_t equipment_slot =
        container_dobj->in.equipped_by_player.equipment_slot;
    svmsg_pick_up_dynamic_object_success_t s =
    {
        .runtime_id     = dobj->runtime_id,
        .type_id        = dobj->type_id,
        .x              = dobj->in.container.position[0],
        .y              = dobj->in.container.position[1],
        .equipment_slot = equipment_slot
    };
    bbuf_t bb = cl_send(pl->client, SVMSG_PICK_UP_DYNAMIC_OBJECT_SUCCESS_SZ);
    if (bb.max_bytes)
        svmsg_pick_up_dynamic_object_success_write(&bb, &s);
}
