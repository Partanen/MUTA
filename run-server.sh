#!/bin/sh

# Quick script to run the server components in separate terminal emulator
# windows. Only st and urxvt support right now!

print_usage()
{
    echo "Usage: $0 [OPTIONS]"
    echo "    h Print this text"
    echo "    d [BOOLEAN] Debug with GDB"
    echo "    t [TERMINAL] Terminal emulator, either urxvt or st"
}

term="st"
debug="false"

while getopts ":t:dh:" opt; do
    case "${opt}" in
    t)
        term=${OPTARG}
        ;;
    d)
        debug="true"
        ;;
    h)
        print_usage
        exit 0
        ;;
    *)
        print_usage
        exit 1
        ;;
    esac
done

if ! [ "$term" = "st" ] && ! [ "$term" = "urxvt" ]; then
    echo "Invalid terminal emulator, must be st or urxvt"
    exit 1
fi

if ! [ "$debug" = "true" ] && ! [ "$debug" = "false" ]; then
    echo "Debug must be true or false (default: false)"
    exit 1
fi

if [ "$debug" = "false" ]; then
    run_file_name=run.sh
else
    run_file_name=run-debug.sh
fi

cd proxy/rundir && $term -e sh -c "./$run_file_name" &
cd master/rundir && $term -e sh -c "./$run_file_name" &
cd login/rundir && $term -e sh -c "./$run_file_name" &
cd sim/rundir && $term -e sh -c "./$run_file_name" &
