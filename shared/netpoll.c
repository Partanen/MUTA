#include "netpoll.h"
#include "net.h"

/*-- Linux --*/

#if defined(__linux__)

int
netpoll_init(netpoll_t *np)
    {return (np->fd = epoll_create(1)) == -1;}

void
netpoll_destroy(netpoll_t *np)
    {close(np->fd);}

int
netpoll_wait(netpoll_t *np, netpoll_event_t *ret_events, int max_events,
    int timeout_ms)
    {return epoll_wait(np->fd, ret_events, max_events, timeout_ms);}

int
netpoll_add(netpoll_t *np, socket_t s, netpoll_event_t *e)
{
    struct epoll_event ie = *e;
    ie.events = EPOLLRDHUP | EPOLLHUP | EPOLLET | e->events;
    return epoll_ctl(np->fd, EPOLL_CTL_ADD, s, &ie);
}

int
netpoll_del(netpoll_t *np, socket_t s)
    {return epoll_ctl(np->fd, EPOLL_CTL_DEL, s, 0);}

/*-- Windows --*/

#elif defined(_WIN32)

#include "common_utils.h"

typedef struct netpoll_t netpoll_t;

thread_ret_t
_connect_loopback(void *args)
{
    netpoll_t   *np     = args;
    addr_t      address = create_addr(127, 0, 0, 1, NETPOLL_LOOPBACK_PORT);
    net_connect(np->loopback_write, address);
    return 0;
}

int
netpoll_init(netpoll_t *np)
{
    np->fds     = 0;
    np->datas   = 0;
    np->added   = 0;
    np->deleted = 0;
    mutex_init(&np->add_mutex);
    mutex_init(&np->del_mutex);
    np->loopback_write  = net_tcp_ipv4_sock();
    socket_t tmp = KSYS_INVALID_SOCKET;
    for (;;)
    {
        socket_t tmp = net_tcp_ipv4_listen_sock(NETPOLL_LOOPBACK_PORT, 1);
        if (tmp == KSYS_INVALID_SOCKET)
        {
            sleep_ms(16);
            continue;
        }
        thread_t thread;
        thread_init(&thread);
        thread_create(&thread, _connect_loopback, np);
        addr_t ret_addr;
        np->loopback_read = net_accept(tmp, &ret_addr);
        thread_join(&thread);
        thread_destroy(&thread);
        make_socket_non_block(np->loopback_read);
        close_socket(tmp);
        break;
    }
    WSAPOLLFD fd;
    fd.fd       = np->loopback_read;
    fd.events   = NETPOLL_READ;
    fd.revents  = 0;
    union netpoll_data_t empty_data = {0};
    darr_push(np->fds, fd);
    darr_push(np->datas, empty_data);
    np->num_events  = 0;
    np->index       = 1;
    return 0;
}

void
netpoll_destroy(netpoll_t *np)
{
    darr_free(np->fds);
    darr_free(np->datas);
    darr_free(np->added);
    darr_free(np->deleted);
    mutex_destroy(&np->add_mutex);
    close_socket(np->loopback_write);
    close_socket(np->loopback_read);
}

static bool32
_is_socket_registered(netpoll_t *np, socket_t s)
{
    uint32 num_fds = darr_num(np->fds);
    for (uint32 i = 0; i < num_fds; ++i)
        if (np->fds[i].fd == s)
            return 1;
    return 0;
}

static void
_del_socket(netpoll_t *np, socket_t s)
{
    WSAPOLLFD   *fds    = np->fds;
    uint32      num     = darr_num(fds);
    for (uint32 i = 0; i < num; ++i)
    {
        if (fds[i].fd != s)
            continue;
        darr_erase(np->fds, i);
        darr_erase(np->datas, i);
        return;
    }
}

static void
_register_added_sockets(netpoll_t *np)
{
    WSAPOLLFD fd;
    mutex_lock(&np->add_mutex);
    uint32 num_added = darr_num(np->added);
    for (uint32 i = 0; i < num_added; ++i)
    {
        if (_is_socket_registered(np, np->added[i].socket))
            continue;
        fd.fd       = np->added[i].socket;
        fd.events   = np->added[i].event.events;
        darr_push(np->fds, fd);
        darr_push(np->datas, np->added[i].event.data);
    }
    darr_clear(np->added);
    mutex_unlock(&np->add_mutex);
}

static void
_unregister_deleted_sockets(netpoll_t *np)
{
    mutex_lock(&np->del_mutex);
    uint32 num_deleted = darr_num(np->deleted);
    for (uint32 i = 0; i < num_deleted; ++i)
        _del_socket(np, np->deleted[i]);
    mutex_unlock(&np->del_mutex);
    darr_clear(np->deleted);
}

static void
_wake_netpoll(netpoll_t *np)
{
    uint8   msg = 0;
    int     n   = net_send_all(np->loopback_write, &msg, sizeof(msg));
}

static int
_write_events(netpoll_t *np, netpoll_event_t *ret_events, uint32 max_events)
{
    uint32 num_events   = np->num_events;
    uint32 num_fds      = darr_num(np->fds);
    uint32  i           = np->index;
    uint32  j           = 0;
    for (; num_events && i < num_fds && j < max_events; ++i)
    {
        if (!np->fds[i].revents)
            continue;
        ret_events[j].events    = (uint32)np->fds[i].revents;
        ret_events[j].data      = np->datas[i];
        num_events--;
        j++;
    }
    np->num_events  = num_events;
    np->index       = num_events ? i : 1;
    return (int)j;
}

int
netpoll_wait(netpoll_t *np, netpoll_event_t *ret_events, int max_events,
    int timeout_ms)
{
    if (!np->num_events)
    {
        _unregister_deleted_sockets(np);
        _register_added_sockets(np);
        uint32 num_fds      = darr_num(np->fds);
        uint32 num_datas    = darr_num(np->datas);
        muta_assert(num_fds == num_datas);
        np->num_events = WSAPoll(np->fds, darr_num(np->fds), timeout_ms);
        if (!np->num_events)
            return 0;
        if (np->fds[0].revents) /* Check loopback */
        {
            uint8 bytes[256];
            net_recv(np->fds[0].fd, bytes, sizeof(bytes));
            np->num_events--;
        }
        if (!np->num_events)
            return 0;
    }
    return _write_events(np, ret_events, (uint32)max_events);
}

int
netpoll_add(netpoll_t *np, socket_t s, netpoll_event_t *e)
{
    mutex_lock(&np->add_mutex);
    netpoll_added_socket_t as;
    as.socket   = s;
    as.event    = *e;
    darr_push(np->added, as);
    mutex_unlock(&np->add_mutex);
    _wake_netpoll(np);
    return 0;
}

int
netpoll_del(netpoll_t *np, socket_t s)
{
    mutex_lock(&np->del_mutex);
    darr_push(np->deleted, s);
    mutex_unlock(&np->del_mutex);
    _wake_netpoll(np);
    return 0;
}

#endif /* __linux__, _WIN32 */
