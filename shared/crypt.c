#include <string.h>
#include "crypt.h"
#include "common_utils.h"

enum cryptchan_flag_t
{
    CRYPTCHAN_HAVE_SK       = (1 << 0),
    CRYPTCHAN_HAVE_PK       = (1 << 1),
    CRYPTCHAN_HAVE_HEADER   = (1 << 2)
};

#define CRYPTCHAN_VALID_FLAGS \
    (CRYPTCHAN_HAVE_SK | CRYPTCHAN_HAVE_PK | CRYPTCHAN_HAVE_HEADER)

int
cryptchan_init(cryptchan_t *c, uint8 *ret_pub_key)
{
    c->flags = 0;
    int r = crypto_kx_keypair(c->pk, c->sk);
    if (!r)
    {
        if (ret_pub_key)
            memcpy(ret_pub_key, c->pk, CRYPTCHAN_PUB_KEY_SZ);
        c->flags |= CRYPTCHAN_HAVE_SK;
        return 0;
    }
    return -2;
}

void
cryptchan_init_from_keys(cryptchan_t *c, uint8 *wx, uint8 *rx, uint8 *sk,
    uint8 *pk, uint8 *read_key, uint8 *read_nonce, uint8 *write_key,
    uint8 *write_nonce)
{
    c->flags = 0;
    memcpy(c->wx, wx, sizeof(c->wx));
    memcpy(c->rx, rx, sizeof(c->rx));
    memcpy(c->sk, sk, sizeof(c->sk));
    memcpy(c->pk, pk, sizeof(c->pk));
    memcpy(c->rstream.k, read_key, sizeof(c->rstream.k));
    memcpy(c->rstream.nonce, read_nonce, sizeof(c->rstream.nonce));
    memcpy(c->wstream.k, write_key, sizeof(c->wstream.k));
    memcpy(c->wstream.nonce, write_nonce, sizeof(c->wstream.nonce));
    c->flags = CRYPTCHAN_HAVE_SK | CRYPTCHAN_HAVE_PK | CRYPTCHAN_HAVE_HEADER;
}

bool32
cryptchan_is_encrypted(cryptchan_t *c)
    {return c->flags == CRYPTCHAN_VALID_FLAGS;}

bool32
cryptchan_is_initialized(cryptchan_t *c)
    {return c->flags & CRYPTCHAN_HAVE_SK;}

int
cryptchan_cl_store_pub_key(cryptchan_t *c,
    const uint8 sv_pub_key[CRYPTCHAN_PUB_KEY_SZ],
    uint8 ret_header[CRYPTCHAN_STREAM_HEADER_SZ])
{
    if (!(c->flags & CRYPTCHAN_HAVE_SK))    return -1;
    if (c->flags & CRYPTCHAN_HAVE_PK)       return -2;
    if (c->flags & CRYPTCHAN_HAVE_HEADER)   return -3;
    if (crypto_kx_client_session_keys(c->rx, c->wx, c->pk, c->sk, sv_pub_key))
        return -4;
    if (crypto_secretstream_xchacha20poly1305_init_push(&c->wstream,
        ret_header, c->wx))
        return -5;
    c->flags |= CRYPTCHAN_HAVE_PK; /* Note: should destroy sk here? */
    return 0;
}

int
cryptchan_sv_store_pub_key(cryptchan_t *c,
    const uint8 cl_pub_key[CRYPTCHAN_PUB_KEY_SZ],
    uint8 ret_header[CRYPTCHAN_STREAM_HEADER_SZ])
{
    if (!(c->flags & CRYPTCHAN_HAVE_SK))
        return -1;
    if (c->flags & CRYPTCHAN_HAVE_PK)
        return -2;
    if (c->flags & CRYPTCHAN_HAVE_HEADER)
        return -3;
    if (crypto_kx_server_session_keys(c->rx, c->wx, c->pk, c->sk, cl_pub_key))
        return -4;
    if (crypto_secretstream_xchacha20poly1305_init_push(&c->wstream,
        ret_header, c->wx))
        return -5;
    c->flags |= CRYPTCHAN_HAVE_PK;
    return 0;
}

int
cryptchan_store_stream_header(cryptchan_t *c,
    const uint8 header[CRYPTCHAN_STREAM_HEADER_SZ])
{
    if (!(c->flags & CRYPTCHAN_HAVE_SK))
        return -1;
    if (!(c->flags & CRYPTCHAN_HAVE_PK))
        return -2;
    if (c->flags & CRYPTCHAN_HAVE_HEADER)
        return -3;
    if (!header)
        return -4;
    if (crypto_secretstream_xchacha20poly1305_init_pull(&c->rstream, header,
        c->rx))
        return -5;
    c->flags |= CRYPTCHAN_HAVE_HEADER;
    return 0;
}

int
cryptchan_encrypt(cryptchan_t *c, uint8 *dst, const uint8 *src, int src_len)
{
    if (crypto_secretstream_xchacha20poly1305_push(&c->wstream, dst, 0, src,
        src_len, 0, 0, 0))
        return -1;
    return 0;
}

int
cryptchan_decrypt(cryptchan_t *c, uint8 *dst, const uint8 *src, int src_len)
{
    uint8 tag;
    if (crypto_secretstream_xchacha20poly1305_pull(&c->rstream, dst, 0, &tag,
        src, src_len, 0, 0))
        return -1;
    if (tag != crypto_secretstream_xchacha20poly1305_TAG_FINAL)
        return 0;
    return 1;
}

int
crypt_storable_pw_hash(const char *pw, int pw_len, char *ret_hash, int strength)
{
    int memlimit;
    int opslimit;
    switch (strength)
    {
    case CRYPT_PW_STRENGTH_LOW:
        memlimit = crypto_pwhash_MEMLIMIT_MIN;
        opslimit = crypto_pwhash_OPSLIMIT_MIN;
        break;
    case CRYPT_PW_STRENGTH_MEDIUM:
        opslimit = crypto_pwhash_OPSLIMIT_INTERACTIVE;
        memlimit = crypto_pwhash_MEMLIMIT_INTERACTIVE;
        break;
    case CRYPT_PW_STRENGTH_STRONG:
        memlimit = crypto_pwhash_MEMLIMIT_SENSITIVE;
        opslimit = crypto_pwhash_OPSLIMIT_SENSITIVE;
        break;
    default:
        return -1;
    }
    return crypto_pwhash_str(ret_hash, pw, pw_len, opslimit, memlimit);
}
