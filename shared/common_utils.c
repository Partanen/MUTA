#include <ctype.h>
#include <inttypes.h>
#include "common_utils.h"
#define STB_SPRINTF_IMPLEMENTATION
#include "stb/stb_sprintf.h"
#include "net.h"
#include "common_defs.h"

typedef struct parse_cfg_file_with_option_list_context_t
    parse_cfg_file_with_option_list_context_t;

struct parse_cfg_file_with_option_list_context_t
{
    config_option_t *opts;
    uint32          num_opts;
    bool8           *have_opts;
    uint32          num_errors;
};

#define ARG_STR_DEFAULT_ARG_LEN 64

static char _legal_chat_symbols[] =
{
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
    'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
    ',', ';', '.', ':', ',', '-', '_', '!', '"', '#', '%', '&', '/', '(', ')',
     '[', ']', '{', '}', '+', '?', '\\', '*', '^', ' ', '\''
};

static void (*_panic_callback)(const char *);

static void
_obj_pool_alloc_more(obj_pool_t *op, uint32 num_items);

static void
_parse_cfg_file_with_option_list_on_opt(void *ctx, const char *opt,
    const char *val);

int
copy_byte_buf(byte_buf_t *dst, byte_buf_t *src)
{
    if (!dst || !src)                       return 1;
    if (dst->max_bytes < src->num_bytes)    return 2;
    dst->num_bytes = src->num_bytes;
    memcpy(dst->mem, src->mem, src->num_bytes);
    return 0;
}

int
dynamic_bbuf_init(dynamic_bbuf_t *buf, int sz)
{
    muta_assert(sz >= 0);
    uint8 *m = (uint8*)malloc(sz);
    if (!m)
        return 1;
    BBUF_INIT(buf, m, sz);
    return 0;
}

void
dynamic_bbuf_destroy(dynamic_bbuf_t *buf)
{
    free(buf->mem);
    memset(buf, 0, sizeof(dynamic_bbuf_t));
}

int
double_cmd_buf_init(double_cmd_buf_t *q, int initial_max, int item_sz)
{
    if (initial_max <= 0)   return 1;
    if (item_sz <= 0)       return 2;
    void *mem_a = malloc(initial_max * item_sz);
    void *mem_b = malloc(initial_max * item_sz);
    if (!mem_a || !mem_b)
    {
        free(mem_a);
        free(mem_b);
        return 3;
    }
    q->wbuf.mem = mem_a;
    q->wbuf.num = 0;
    q->wbuf.max = initial_max;
    q->rbuf.mem = mem_b;
    q->rbuf.num = 0;
    q->rbuf.max = initial_max;
    q->item_sz  = item_sz;
    spinlock_init(&q->spinlock);
    return 0;
}

void
double_cmd_buf_destroy(double_cmd_buf_t *q)
{
    free(q->wbuf.mem);
    free(q->rbuf.mem);
    memset(q, 0, sizeof(double_cmd_buf_t));
}

int
double_cmd_buf_push(double_cmd_buf_t *q, void *data)
{
    spinlock_lock(&q->spinlock);
    int num     = q->wbuf.num;
    int max     = q->wbuf.max;
    int item_sz = q->item_sz;
    if (num == max)
    {
        int     new_max = MAX(max * 105 / 100, max + 4);
        void    *mem    = realloc(q->wbuf.mem, new_max * q->item_sz);
        if (!mem) {spinlock_unlock(&q->spinlock); return 1;}
        q->wbuf.mem = mem;
        q->wbuf.max = new_max;
    }
    memcpy((char*)q->wbuf.mem + num * item_sz, data, item_sz);
    q->wbuf.num = num + 1;
    spinlock_unlock(&q->spinlock);
    return 0;
}

int
double_cmd_buf_push_static(double_cmd_buf_t *q, void *data)
{
    spinlock_lock(&q->spinlock);
    int num     = q->wbuf.num;
    int max     = q->wbuf.max;
    int item_sz = q->item_sz;
    if (num == max)
        {spinlock_unlock(&q->spinlock); return 1;}
    memcpy((char*)q->wbuf.mem + num * item_sz, data, item_sz);
    q->wbuf.num = num + 1;
    spinlock_unlock(&q->spinlock);
    return 0;
}

double_cmd_buf_arr_t *
double_cmd_buf_swap(double_cmd_buf_t *q)
{
    /* TODO: copy possible unread cmds */
    spinlock_lock(&q->spinlock);
    double_cmd_buf_arr_t tmp    = q->wbuf;
    q->wbuf                     = q->rbuf;
    q->rbuf                     = tmp;
    spinlock_unlock(&q->spinlock);
    return &q->rbuf;
}

int
double_msg_buf_init(double_msg_buf_t *buf, int num_initial_bytes)
{
    if (dynamic_bbuf_init(&buf->wbuf, num_initial_bytes))
        return 1;
    if (dynamic_bbuf_init(&buf->rbuf, num_initial_bytes))
        {dynamic_bbuf_destroy(&buf->wbuf); return 2;}
    mutex_init(&buf->mtx);
    return 0;
}

void
double_msg_buf_destroy(double_msg_buf_t *buf)
{
    dynamic_bbuf_destroy(&buf->wbuf);
    dynamic_bbuf_destroy(&buf->rbuf);
    mutex_destroy(&buf->mtx);
    memset(buf, 0, sizeof(double_msg_buf_t));
}

byte_buf_t *
double_msg_buf_begin_write(double_msg_buf_t *buf, int num_bytes)
{
    muta_assert(num_bytes > 0);

    mutex_lock(&buf->mtx);

    if (BBUF_FREE_SPACE(&buf->wbuf) < num_bytes
    &&  _dynamic_bbuf_ensure_capacity_for_n_more(&buf->wbuf, num_bytes))
        {mutex_unlock(&buf->mtx); return 0;}

    return &buf->wbuf;
}

void
double_msg_buf_finalize_write(double_msg_buf_t *buf)
    {mutex_unlock(&buf->mtx);}

int
double_msg_buf_write(double_msg_buf_t *buf, const void *data, int num_bytes)
{
    int ret = 0;
    if (num_bytes <= 0) return ret;
    mutex_lock(&buf->mtx);
    if (dynamic_bbuf_write_bytes(&buf->wbuf, data, num_bytes))
        {ret = 1; goto out;}
    out:
    mutex_unlock(&buf->mtx);
    return ret;
}

byte_buf_t *
double_msg_buf_swap(double_msg_buf_t *buf)
{
    mutex_lock(&buf->mtx);

    if (buf->rbuf.num_bytes == 0)
    {
        dynamic_bbuf_t tmp  = buf->wbuf;
        buf->wbuf           = buf->rbuf;
        buf->rbuf           = tmp;

    } else
    {
        /* If the user left unread bytes in the read buffer, append any new
         * data to those bytes. This assures a continuous stream. */
        if (dynamic_bbuf_write_bytes(&buf->rbuf, buf->wbuf.mem,
            buf->wbuf.num_bytes))
        {
            muta_assert(0);
            mutex_unlock(&buf->mtx);
            return 0;
        }
    }

    BBUF_CLEAR(&buf->wbuf);
    mutex_unlock(&buf->mtx);
    return &buf->rbuf;
}

void
double_msg_buf_clear(double_msg_buf_t *buf)
{
    mutex_lock(&buf->mtx);
    BBUF_CLEAR(&buf->wbuf);
    BBUF_CLEAR(&buf->rbuf);
    mutex_unlock(&buf->mtx);
}

void
spinlock_init(spinlock_t volatile *sl) {*sl = 0;}

int
spinlock_lock(spinlock_t volatile *sl)
    {while (interlocked_compare_exchange_int32(sl, 1, 0)); return 0;}

int
spinlock_unlock(spinlock_t volatile *sl)
    {if (*sl) {interlocked_exchange_int32(sl, 0); return 0;} return 1;}

uint8 *
load_file_to_buffer(const char *path, size_t *ret_len)
{
    uint8       *buf;
    FILE        *file;
    long int    num_chars;
    file = fopen(path, "rb");
    if (!file)
        return 0;
    /* Get file length */
    fseek(file, 0L, SEEK_END);
    num_chars = ftell(file);
    rewind(file);
    buf = (uint8*)malloc(num_chars);
    fread(buf, sizeof(char), num_chars, file);
    fclose(file);
    if (ret_len)
        *ret_len = num_chars;
    return buf;
}

dchar *
load_text_file_to_dstr(const char *path)
{
    FILE *f = fopen(path, "r");
    if (!f)
        return 0;
    char    *str    = 0;
    uint    len     = 0;
    int     c;
    for (c = fgetc(f); c != EOF; c = fgetc(f))
        len++;
    if (!len)
        {goto out;}
    rewind(f);
    str = create_empty_dynamic_str(len);
    if (!str)
        goto out;
    char *rc = str;
    while ((*(rc++) = fgetc(f)) != EOF);
    *rc = 0;
    set_dynamic_str_len(str, len);
    out:
        fclose(f);
        return str;
}

void
str_strip_symbols(char *str, const char *symbols, int num_symbols)
{
    if (!num_symbols)
        return;
    int len = (int)strlen(str);
    const char *s, *ls;
    ls = symbols + num_symbols;
    for (char *c = str; *c; ++c)
    {
        for (s = symbols; s < ls; ++s)
        {
            if (*c == *s)
            {
                /* Why does strcpy not work here on gcc? */
                memmove(c, c + 1, len - (int)(c - str));
                c--;
                len--;
                break;
            }
        }
    }
}

int
str_strip_symbolsn(char *str, const char *symbols, int num_symbols,
    int str_len)
{
    if (!num_symbols || !str_len)
        return str_len;
    int         len = str_len;
    const char  *s, *ls, *lc;
    lc = str + len;
    ls = symbols + num_symbols;
    for (char *c = str; c < lc && *c; ++c)
    {
        for (s = symbols; s < ls; ++s)
        {
            if (*c == *s)
            {
                memcpy(c, c + 1, len - (int)(c - str));
                c--;
                len--;
                lc = str + len;
            }
        }
    }
    return len;
}

void
str_strip_undef_symbols(char *str, const char *symbols, int num_symbols)
{
    if (!num_symbols)
        return;
    const char  *s, *ls;
    bool32      is_sym_one_of;
    ls = symbols + num_symbols;
    for (char *c = str; *c; ++c)
    {
        is_sym_one_of = 0;
        for (s = symbols; s < ls; ++s)
        {
            if (*c == *s)
            {
                is_sym_one_of = 1;
                break;
            }
        }
        if (is_sym_one_of)
            continue;
        strcpy(c, c + 1);
        c--;
    }
}

bool32
str_contains_one_or_more_of_symbols(const char *str, const char *symbols,
    int num_symbols)
{
    if (!num_symbols)
        return 0;
    const char *s, *ls;
    ls = symbols + num_symbols;
    for (const char *c = str; *c; ++c)
        for (s = symbols; s < ls; ++s)
            if (*c == *s)
                return 1;
    return 0;
}

bool32
str_contains_one_or_more_of_symbolsn(const char *str, const char *symbols,
    int num_symbols, int str_len)
{
    if (!num_symbols || !str_len)
        return 0;
    const char  *sym, *ls, *lc;
    lc = str + str_len;
    ls = symbols + num_symbols;
    for (const char *c = str; c < lc && *c; ++c)
        for (sym = symbols; sym < ls; ++sym)
            if (*c == *sym)
                return 1;
    return 0;
}

bool32
str_contains_only_symbols(const char *str, const char *symbols,
    int num_symbols)
{
    if (!num_symbols)
        return 1;
    bool32      was_one_of = 0;
    const char  *s, *ls;
    ls = symbols + num_symbols;
    for (const char *c = str; *c; ++c)
    {
        was_one_of = 0;
        for (s = symbols; s < ls; ++s)
        {
            if (*c != *s)
                continue;
            was_one_of = 1;
            break;
        }
        if (!was_one_of)
            return 0;
    }
    return 1;
}

bool32
str_contains_only_symbolsn(const char *str, const char *symbols,
    int num_symbols, int str_len)
{
    if (!num_symbols || !str_len)
        return 1;
    const char  *sym, *lsym, *lc;
    bool32      was_one_of = 0;
    lc      = str + str_len;
    lsym    = symbols + num_symbols;
    for (const char *c = str; c < lc && *c; ++c)
    {
        was_one_of = 0;
        for (sym = symbols; sym < lsym; ++sym)
        {
            if (*c != *sym)
                continue;
            was_one_of = 1;
            break;
        }
        if (!was_one_of)
            return 0;
    }
    return 1;
}

int
str_insensitive_cmp(const char *str1, const char *str2)
{
    int a = (int)strlen(str1);
    int b = (int)strlen(str2);

    if (a != b)
        return 1;

    int k;

    for (k = 0; k < a; ++k)
    {
        if ((str1[k] | 32) != (str2[k] | 32))
            break;
    }

    if (k != a)
        return 1;

    return 0;
}

void
str_strip_trailing_spaces(char *str)
{
    if (!str)
        return;
    int len, i;
    len = (int)strlen(str);
    for (i = 0; i < len; ++i)
        if (str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
            break;
    len = len - i;
    memmove(str, str + i, len);
    str[len] = '\0';
    for (i = len - 1; i >= 0; --i)
    {
        if (str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
            break;
        str[i] = '\0';
    }
}

void
str_strip_non_numbers(char *str)
{
	static char numbers[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
	str_strip_undef_symbols(str, numbers, 10);
}

int
str_to_bool(const char *str, bool32 *ret_bool)
{
    if (!str) return 1;
    char ts[7];
    strncpy(ts, str, 6);
    str_to_lower(ts);
    int ret = 0;
    if (streq(ts, "false") || streq(ts, "0"))
        *ret_bool = 0;
    else if (streq(ts, "true") || streq(ts, "1"))
        *ret_bool = 1;
    else
        ret = 2;
    return ret;
}

void
str_to_upper(char *str)
    {for (char *c = str; *c; ++c) *c = (char)toupper(*c);}

void
str_to_lower(char *str)
    {for (char *c = str; *c; ++c) *c = (char)tolower(*c);}

char *
str_find(char *str, const char *key)
{
    char *ret = 0;
    if (!str || ! key)
        return ret;
    size_t len = strlen(key);
    if (!len)
        return ret;
    size_t  num_correct = 0;
    char    *potential = str;
    for (char *c = str; *c; ++c)
    {
        if (*c != key[num_correct])
        {
            num_correct = 0;
            potential = c + 1;
            continue;
        }
        if (++num_correct != len)
            continue;
        ret = potential;
        break;
    }
    return ret;
}

char *
str_insensitive_find(char *str, const char *key)
{
    char *ret = 0;
    if (!str || ! key)
        return ret;
    size_t len = strlen(key);
    if (!len)
        return ret;
    size_t  num_correct = 0;
    char    *potential = str;
    for (char *c = str; *c; ++c)
    {
        if (tolower(*c) != tolower(key[num_correct]))
        {
            num_correct = 0;
            potential = c + 1;
            continue;
        }
        if (++num_correct != len)
            continue;
        ret = potential;
        break;
    }
    return ret;
}

bool32
str_contains(char *str, const char *key)
    {return str_find(str, key) ? 1 : 0;}

bool32
str_insensitive_contains(char *str, const char *key)
    {return str_insensitive_find(str, key) ? 1 : 0;}

size_t
str_count_chars(const char *str, char c)
{
    size_t ret = 0;
    for (const char *o = str; *o; ++o)
        if (*o == c)
            ret++;
    return ret;
}

void
str_copy_and_null_terminate(char *dst, const char *src, size_t src_len)
{
    memcpy(dst, src, src_len);
    dst[src_len] = 0;
}

bool32
str_ends_with(const char *str, const char *end)
{
    size_t end_len = strlen(end);
    if (!end_len)
        return 1;
    size_t str_len = strlen(str);
    if (end_len > str_len)
        return 0;
    return !memcmp(str + str_len - end_len, end, end_len);
}

bool32
str_is_valid_file_name(const char *str)
{
    if (!str)
        return 0;
    if (!str[0])
        return 0;
    static char syms[] =
    {
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B',
        'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '.', '-', '_',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    };
    return str_contains_only_symbols(str, syms, sizeof(syms));
}

bool32
str_is_valid_file_path(const char *str)
{
    if (!str[0])
        return 0;
    static char syms[] =
    {
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B',
        'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '.', '-', '_', '/',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    };
    return str_contains_only_symbols(str, syms, sizeof(syms));
}

bool32
str_is_int(const char *str)
{
    if (!str[0])
        return 0;
    const char *c = str;
    if (*c == '-')
        c += 1;
    for (; *c; ++c)
        if (!isdigit(*c))
            return 0;
    return 1;
}

bool32
str_is_float(const char *str)
{
    if (!str[0])
        return 0;
    if (str[0] == '.')
        return 0;
    const char *dot = 0;
    const char *c   = str;
    if (*c == '-')
        c += 1;
    for (; *c; ++c)
    {
        if (*c == '.')
        {
            if (dot)
                return 1;
            dot = c;
            continue;
        }
        if (!isdigit(*c))
            return 0;
    }
    return dot && (c - 1) != dot;
}

bool32
str_is_ascii(const char *str)
{
    for (const char *c = str; *c; ++c)
        if (!isascii(*c)) return 0;
    return 1;
}

#define STRIP_STR(str, call) \
    char *ca = str; \
    char *cb = str; \
    for (;*cb;) {*ca = *(cb++); if (call(*ca)) ca++;} \
    *ca = 0; \
    return (int)(ca - str);

int
str_strip_non_ascii(char *str)
    {STRIP_STR(str, isascii);}

#define NOT_CTRL(c_) (!iscntrl(c_))

int
str_strip_ctrl_chars(char *str)
    {STRIP_STR(str, NOT_CTRL);}

typedef struct
{
    uint32 len, cap; /* Neither number includes the null terminator */
} dstr_header_t;

static dchar *
_create_empty_dynamic_str_inner(uint len)
{
    uint32          cap = len + len % 4;
    dstr_header_t   *h  = malloc(sizeof(dstr_header_t) + cap + 1);
    if (!h) return 0;
    h->len = 0;
    h->cap = cap;
    return (dchar*)(h + 1);
}

static dstr_header_t *
_grow_dynamic_str_cap(dstr_header_t *h, uint cap)
{
    uint32 max = cap * 150 / 100;
    uint32 req = cap + cap % 4;
    if (max < req)
        max = req;
    dstr_header_t *nh = realloc(h, sizeof(dstr_header_t) + max + 1);
    if (nh)
        nh->cap = max;
    return nh;
}

dchar *
create_dynamic_str(const char *txt)
{
    if (!txt)
        return 0;
    uint32  len     = (uint32)strlen(txt);
    dchar   *ret    = _create_empty_dynamic_str_inner(len);
    if (!ret)
        return ret;
    memcpy(ret, txt, len + 1);
    ((dstr_header_t*)ret - 1)->len = len;
    return ret;
}

dchar *
create_empty_dynamic_str(uint len)
{
    if (!len) return 0;
    return _create_empty_dynamic_str_inner(len);
}

void
free_dynamic_str(const dchar *str)
    {if (str) free((dstr_header_t*)str - 1);}

dchar *
set_dynamic_str(dchar *str, const char *txt)
{
    if (!str)
        return create_dynamic_str(txt);
    if (!txt)
        return 0;
    uint32 len = (uint32)strlen(txt);
    if (!len)
        return 0;
    dstr_header_t *h = (dstr_header_t*)str - 1;
    if (h->cap < len)
    {
        dstr_header_t *nh = _grow_dynamic_str_cap(h, len);
        if (!nh) {return 0;}
        h = nh;
    }
    h->len = len;
    char *ret = (char*)(h + 1);
    memcpy(ret, txt, len + 1);
    return ret;
}

uint
get_dynamic_str_len(const dchar *str)
    {if (str) return ((dstr_header_t*)str - 1)->len; return 0;}

uint
get_dynamic_str_cap(const dchar *str)
    {if (str) return ((dstr_header_t*)str - 1)->cap; return 0;}

dchar *
set_dynamic_str_len(dchar *str, uint len)
{
    if (!str)
        return 0;
    dstr_header_t *h = (dstr_header_t*)str - 1;
    if (len > h->cap)
    {
        dstr_header_t *nh = _grow_dynamic_str_cap(h, len);
        if (!nh) return 0;
        h = nh;
    }
    h->len = len;
    ((dchar*)(h + 1))[len] = 0;
    return (dchar*)(h + 1);
}

dchar *
append_to_dynamic_str(dchar *dstr, const char *s)
{
    if (!dstr)
        return create_dynamic_str(s);
    if (!s)
        return dstr;
    dstr_header_t *h    = (dstr_header_t*)dstr - 1;
    uint len            = (uint)strlen(s);
    uint req_cap        = h->len + len;
    if (req_cap > h->cap)
    {
        h = _grow_dynamic_str_cap(h, req_cap + req_cap % 4);
        if (!h)
            return 0;
    }
    char *ndstr = (char*)(h + 1);
    memcpy(ndstr + h->len, s, len + 1);
    h->len += len;
    return ndstr;
}

dchar *
set_dynamic_strvf(dchar *str, const char *fmt, va_list args)
{
    dchar *ret = 0;
    dchar *tmp = str;
    va_list cargs;
    va_copy(cargs, args);
    for (;;)
    {
        uint cap = get_dynamic_str_cap(tmp);
        if (!cap)
        {
            tmp = create_empty_dynamic_str((uint)strlen(fmt) + 16);
            if (!tmp)
                break;
            cap = get_dynamic_str_cap(tmp);
        }
        va_list cargs;
        va_copy(cargs, args);
        int len = vsnprintf(tmp, cap - 1, fmt, cargs);
        if (len < 0)
            break;
        if ((uint)len <= cap)
        {
            ret = tmp;
            ((dstr_header_t*)ret - 1)->len = len;
            break;
        }

        uint new_cap = (len + 1) + len % 4;
        if (tmp != str)
            free_dynamic_str(tmp);
        tmp = create_empty_dynamic_str(new_cap);
    }
    if (ret)
    {
        if (ret != str)
            free_dynamic_str(str);
    } else
    if (tmp != str)
        free_dynamic_str(tmp);
    return ret;

}

dchar *
set_dynamic_strf(dchar *str, const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    dchar *ret = set_dynamic_strvf(str, fmt, args);
    va_end(args);
    return ret;
}

dchar *
dstr_create(const char *s)
{
    dchar *ret = create_dynamic_str(s);
    if (!ret && s)
        muta_panic_print(__func__);
    return ret;
}

dchar *
dstr_create_empty(uint32 len)
{
    dchar *ret = create_empty_dynamic_str(len);
    if (!ret && len > 0)
        muta_panic_print(__func__);
    return ret;
}

void
dstr_free(dchar **dstr)
    {if (dstr) free_dynamic_str(*dstr);}

void
dstr_set(dchar **dstr, const char *s)
{
    if (!dstr)
        return;
    dchar *ret = set_dynamic_str(*dstr, s);
    if (!ret && s && s[0])
        muta_panic_print(__func__);
    *dstr = ret;
}

void
dstr_setf(dchar **dstr, const char *fmt, ...)
{
    if (!dstr)
        return;
    va_list args;
    va_start(args, fmt);
    dchar *ret = set_dynamic_strvf(*dstr, fmt, args);
    va_end(args);
    if (!ret && fmt && fmt[0])
        muta_panic_print(__func__);
    *dstr = ret;
}

void
dstr_setvf(dchar **dstr, const char *fmt, va_list args)
{
    va_list new_args;
    va_copy(new_args, args);
    dchar *ret = set_dynamic_strvf(*dstr, fmt, new_args);
    if (!ret && fmt && fmt[0])
        muta_panic_print(__func__);
    *dstr = ret;
}

void
dstr_append(dchar **dstr, const char *s)
{
    if (!dstr) return;
    dchar *ret = append_to_dynamic_str(*dstr, s);
    if (!ret && s && s[0])
        muta_panic_print(__func__);
    *dstr = ret;
}

void
dstr_append_char(dchar **dstr, char c)
{
    char s[2] = {c, '\0'};
    dstr_append(dstr, s);
}

void
dstr_set_len(dchar **dstr, uint32 len)
{
    if (dstr)
        *dstr = set_dynamic_str_len(*dstr, len);
}

void
dstr_reserve(dchar **dstr, uint32 len)
{
    if (!dstr)
        return;
    dstr_header_t *h = (dstr_header_t*)*dstr - 1;
    if (h->cap >= len)
        return;
    dstr_header_t *new_dstr = erealloc(h,
        sizeof(dstr_header_t) + len * sizeof(char) + 1);
    ((dstr_header_t*)new_dstr)->cap = len;
    *dstr = (dchar*)(new_dstr + 1);
}

#define CFG_PARSE_BUF_SZ 1024

enum parse_cfg_file_result
parse_cfg_file(const char *path,
    void (*callback)(void *ctx, const char *opt, const char *val),
    void *context)
{
    muta_assert(path);
    muta_assert(callback);
    FILE *f = fopen(path, "r");
    if (!f)
        return PARSE_CFG_FILE_FOPEN_FAILED;
    char    stack_buf[CFG_PARSE_BUF_SZ];
    int     ret         = PARSE_CFG_FILE_SUCCESS;
    char    *buf        = stack_buf;
    char    *val        = 0;
    uint    buf_max     = CFG_PARSE_BUF_SZ;
    int     i           = 0;
    bool32  have_first  = 0;
    bool32  eof;
    for (;;)
    {
        buf[i]  = fgetc(f);
        eof     = buf[i] == EOF || !buf[i];
        if (buf[i] == '\n' || eof)
        {
            if (val)
            {
                buf[i]      = 0;
                char *opt   = buf;
                str_strip_trailing_spaces(opt);
                str_strip_trailing_spaces(val);
                if (opt[0] && val[0])
                    callback(context, opt, val);
            }
            i           = 0,
            val         = 0;
            have_first  = 0;
            if (eof)
                break;
            else
                continue;
        }
        if (!val)
        {
            if (!have_first && !(buf[i] == ' ' || buf[i] == '\t'))
            {
                if (buf[i] == '#') /* Line is a comment */
                {
                    int c = fgetc(f);;
                    for (; c != EOF && c != '\n' && c; c = fgetc(f));
                    i = 0;
                    continue;
                }
                have_first = 1;
            } else
            if (buf[i] == '=')
            {
                val     = buf + i + 1;
                buf[i]  = 0;
            }
        }
        if ((i++) != buf_max)
            continue;
        /* Buffer full - count the size required and attempt to heap alloc */
        long int    offset      = ftell(f);
        int         line_len    = i;
        int         c;
        while ((c = fgetc(f)) != EOF && c)
            line_len++;
        fseek(f, offset, SEEK_SET);
        int     buf_sz      = line_len * 110 / 100 + 1;
        size_t  val_offset  = (size_t)(val - buf);
        if (buf == stack_buf)
        {
            char *new_buf = emalloc(buf_sz);
            memcpy(new_buf, buf, i);
            buf = new_buf;
        } else
            buf = erealloc(buf, buf_sz);
        if (val)
            val = buf + val_offset;
        buf_max = buf_sz;
    }
    out:
        fclose(f);
        if (buf != stack_buf)
            free(buf);
        return ret;
}

bool32
is_chat_msg_legal(const char *msg)
{
    return str_contains_only_symbols(msg, _legal_chat_symbols,
        sizeof(_legal_chat_symbols));
}

bool32
is_chat_msg_legaln(const char *msg, int len)
{
    return str_contains_only_symbolsn(msg, _legal_chat_symbols,
        sizeof(_legal_chat_symbols), len);
}

void
strip_illegal_chat_msg_symbols(char *msg)
{
    str_strip_undef_symbols(msg, _legal_chat_symbols,
        sizeof(_legal_chat_symbols));
}

void
muta_panic(void)
{
    if (_panic_callback)
    {
        _panic_callback(0);
        return;
    }
    puts("Panic!");
    SET_BREAKPOINT();
    exit(1);
}

void
muta_panic_print(const char *fmt, ...)
{
    char buf[4096];
    if (fmt)
    {
        va_list args;
        va_start(args, fmt);
        vsnprintf(buf, sizeof(buf), fmt, args);
        va_end(args);
    } else
        buf[0] = 0;
    if (_panic_callback)
        _panic_callback(buf);
    else
    {
        printf("Panic! %s\n", buf);
        SET_BREAKPOINT();
    }
    exit(1);
}

void
set_muta_panic_callback(void (*callback)(const char *))
    {_panic_callback = callback;}

int
fwrite_zeros(FILE *f, size_t num)
{
    for (size_t i = 0; i < num; ++i)
        if (fputc(0, f) == EOF)
            return 1;
    return 0;
}

int
fwrite_uint8_arr(FILE *f, uint8 *val, size_t len)
    {return fwrite(val, sizeof(uint8), len, f) != len;}

int
fwrite_uint16_arr(FILE *f, uint16 *val, size_t len)
{
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    for (size_t i = 0; i < len; ++i)
        if (fwrite_uint16(f, val[i])
            return 1;
    return 0;
#else
    return fwrite(val, sizeof(uint16), len, f) != len;
#endif
}

int
fwrite_uint32_arr(FILE *f, uint32 *val, size_t len)
{
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    for (size_t i = 0; i < len; ++i)
        if (fwrite_uint32(f, val[i])
            return 1;
    return 0;
#else
    return fwrite(val, sizeof(uint32), len, f) != len;
#endif
}

int
fwrite_uint64_arr(FILE *f, uint64 *val, size_t len)
{
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    for (size_t i = 0; i < len; ++i)
        if (fwrite_uint64(f, val[i])
            return 1;
    return 0;
#else
    return fwrite(val, sizeof(uint64), len, f) != len;
#endif
}

int
fwrite_uint8(FILE *f, uint8 val)
    {return fwrite(&val, sizeof(uint8), 1, f) != 1;}

int
fwrite_int8(FILE *f, int8 val)
    {return fwrite(&val, sizeof(int8), 1, f) != 1;}

int
fwrite_int16(FILE *f, int16 val)
{
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    val = BSWAP16(val);
#endif
    return fwrite(&val, sizeof(int16), 1, f) != 1;
}

int
fwrite_uint16(FILE *f, uint16 val)
{
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    val = BSWAP16(val);
#endif
    return fwrite(&val, sizeof(uint16), 1, f) != 1;
}

int
fwrite_int32(FILE *f, int32 val)
{
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    val = BSWAP32(val);
#endif
    return fwrite(&val, sizeof(int32), 1, f) != 1;
}

int
fwrite_uint32(FILE *f, uint32 val)
{
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    val = BSWAP32(val);
#endif
    return fwrite(&val, sizeof(uint32), 1, f) != 1;
}

int
fwrite_int64(FILE *f, int64 val)
{
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    val = BSWAP64(val);
#endif
    return fwrite(&val, sizeof(int64), 1, f) != 1;
}

int
fwrite_uint64(FILE *f, uint64 val)
{
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    val = BSWAP64(val);
#endif
    return fwrite(&val, sizeof(uint64), 1, f) != 1;
}

int
fread_uint8_arr(FILE *f, uint8 *ret, size_t len)
    {return fread(ret, sizeof(uint8), len, f) != len;}

int
fread_uint16_arr(FILE *f, uint16 *ret, size_t len)
{
    size_t num = fread(ret, sizeof(uint16), len, f);
    if (num != len)
        return 1;
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    for (size_t i = 0; i < len; ++i)
        ret[i] = BSWAP16(ret[i]);
#endif
    return 0;
}

int
fread_uint32_arr(FILE *f, uint32 *ret, size_t len)
{
    size_t num = fread(ret, sizeof(uint32), len, f);
    if (num != len)
        return 1;
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    for (size_t i = 0; i < len; ++i)
        ret[i] = BSWAP32(ret[i]);
#endif
    return 0;
}

int
fread_int8(FILE *f, int8 *ret)
    {return fread(ret, sizeof(int8), 1, f) != 1;}

int
fread_uint8(FILE *f, uint8 *ret)
    {return fread(ret, sizeof(uint8), 1, f) != 1;}

int
fread_int16(FILE *f, int16 *ret_val)
{
    int ret = fread(ret_val, sizeof(int16), 1, f) != 1;
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    if (!ret)
        *ret_val = BSWAP16(value);
#endif
    return ret;
}

int
fread_uint16(FILE *f, uint16 *ret_val)
{
    int ret = fread(ret_val, sizeof(uint16), 1, f) != 1;
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    if (!ret)
        *ret_val = BSWAP16(value);
#endif
    return ret;
}

int
fread_int32(FILE *f, int32 *ret_val)
{
    int ret = fread(ret_val, sizeof(int32), 1, f) != 1;
#if  (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    if (!ret)
        *ret_val = BSWAP32(value);
#endif
    return ret;
}

int
fread_uint32(FILE *f, uint32 *ret_val)
{
    int ret = fread(ret_val, sizeof(uint32), 1, f) != 1;
#if  (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    if (!ret)
        *ret_val = BSWAP32(*ret_val);
#endif
    return ret;
}

int
fread_int64(FILE *f, int64 *ret_val)
{
    int ret = fread(ret_val, sizeof(int64), 1, f) != 1;
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    if (!ret)
        *ret_val = BSWAP64(*ret_val);
#endif
    return ret;
}

int
fread_uint64(FILE *f, uint64 *ret_val)
{
    int ret = fread(ret_val, sizeof(uint64), 1, f) != 1;
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    if (!ret)
        *ret_val = BSWAP64(*ret_val);
#endif
    return ret;
}

int
fread_uint64_arr(FILE *f, uint64 *ret_val, size_t len)
{
    int ret = fread(ret_val, sizeof(uint64), len, f) != len;
#if (MUTA_ENDIANNESS == MUTA_BIG_ENDIAN)
    if (!ret)
        for (size_t i = 0; i < len; ++i)
            ret_val[i] = BSWAP64(ret_val[i]);
#endif
    return ret;
}

void *
emalloc(size_t sz)
{
    void *ret = malloc(sz);
    if (!ret && sz)
        muta_panic_print("emalloc failed!");
    return ret;
}

void *
ecalloc(size_t sz)
{
    void *ret = calloc(sz, 1);
    if (!ret && sz)
        muta_panic_print(__func__);
    return ret;
}

void *
erealloc(void *p, size_t sz)
{
    void *ret = realloc(p, sz);
    if (!ret && sz)
        muta_panic_print(__func__);
    return ret;
}

void *
darr_ensure_growth_by(void *darr, uint32 grow_by, uint32 item_sz)
{
    if (!grow_by)
        return darr;
    darr_head_t *h = darr_head(darr);
    if (!h)
    {
        uint32 cap = grow_by > 4 ? grow_by : 4;
        h = emalloc(sizeof(darr_head_t) + cap * item_sz);
        h->num = 0;
        h->cap = cap;
    } else
    {
        uint32 req = h->num + grow_by;
        if (req <= h->cap)
            return darr;
        uint32 new_cap = h->cap * 2;
        new_cap = new_cap > req ? new_cap : req;
        h = erealloc(h, sizeof(darr_head_t) + new_cap * item_sz);
        h->cap = new_cap;
    }
    return h + 1;
}

void
darr_sized_erase(void *darr, uint32 index, uint32 item_sz)
{
    darr_head_t *h = _darr_head(darr);
    muta_assert(index < h->num);
    size_t  num_bytes   = item_sz * (h->num - (index + 1));
    char    *mem        = (char*)darr;
    char    *ind_pos    = mem + index * item_sz;
    h->num--;
    memmove(ind_pos, ind_pos + item_sz, num_bytes);
}

void *
dynamic_pool_grow(void *all, size_t type_size, uint32 **free, uint32 *num_free,
    uint32 *max)
{
    uint32 new_max = *max * 2;
    if (!new_max)
        new_max = 4;
    void *ret = erealloc(all, new_max * type_size);
    *free   = erealloc(*free, new_max * sizeof(uint32));
    for (uint32 i = *max; i < new_max; ++i)
        (*free)[(*num_free)++] = i;
    *max = new_max;
    return ret;
}

void
segfit_stack_init(segfit_stack_t *sfs, uint32 largest_block)
{
    memset(sfs, 0, sizeof(segfit_stack_t));
    largest_block       = next_pow2_uint32(largest_block);
    uint32 num_lists    = highest_bit_uint32(largest_block) + 1;
    darr_reserve(sfs->lists, num_lists);
    memset(sfs->lists, 0, num_lists * sizeof(segfit_stack_list_t));
}

void
segfit_stack_destroy(segfit_stack_t *sfs)
{
    uint32 num_allocs = darr_num(sfs->allocs);
    for (uint32 i = 0; i < num_allocs; ++i)
        free(sfs->allocs[i].mem);
    darr_free(sfs->allocs);
    uint32 num_lists = darr_num(sfs->lists);
    for (uint32 i = 0; i < num_lists; ++i)
    {
        darr_free(sfs->lists[i].res);
        darr_free(sfs->lists[i].free);
    }
    darr_free(sfs->lists);
    darr_free(sfs->allocs);
}

void *
segfit_stack_malloc(segfit_stack_t *sfs, uint32 num_bytes)
{
    if (!num_bytes)
        return 0;
    num_bytes = next_pow2_uint32(num_bytes);
    uint32 index = highest_bit_uint32(num_bytes);
    /* Create a new list for this byte size */
    if (index >= darr_num(sfs->lists))
    {
        uint32 num_lists        = darr_num(sfs->lists);
        uint32 new_num_lists    = index + 1;
        darr_reserve(sfs->lists, new_num_lists);
        memset(sfs->lists + num_lists, 0,
            (new_num_lists - num_lists) * sizeof(segfit_stack_list_t));
        _darr_head(sfs->lists)->num = new_num_lists;
    }
    segfit_stack_list_t *list = &sfs->lists[index];
    /* If there are no free blocks, allocate some more */
    if (!darr_num(list->free))
    {
        uint32 num_blocks = darr_num(list->res) * 2;
        if (!num_blocks)
            num_blocks = 2;
        uint32 num_alloc_bytes = num_blocks * num_bytes;
        segfit_stack_alloc_t alloc;
        alloc.mem   = emalloc(num_alloc_bytes);
        alloc.size  = num_alloc_bytes;
        darr_push(sfs->allocs, alloc);
        uint8 *mem = alloc.mem;
        for (uint32 i = 0; i < num_blocks; ++i)
            darr_push(list->free, mem + i * num_bytes);
    }
    uint32  darr_index  = darr_num(list->free) - 1;
    void    *ret        = list->free[darr_index];
    darr_erase_last(list->free);
    darr_push(list->res, ret);
    return ret;
}

void
segfit_stack_clear(segfit_stack_t *sfs)
{
    uint32 num_lists = darr_num(sfs->lists);
    for (uint32 i = 0; i < num_lists; ++i)
    {
        uint32 num_allocs = darr_num(sfs->lists[i].res);
        for (uint32 j = 0; j < num_allocs; ++j)
            darr_push(sfs->lists[i].free, sfs->lists[i].res[j]);
        darr_clear(sfs->lists[i].res);
    }
}

void
segpool_init(segpool_t *pool)
{
    pool->blocks = 0;
    pool->allocs = 0;
}

void
segpool_destroy(segpool_t *pool)
{
    darr_free(pool->blocks);
    uint32 num_allocs = darr_num(pool->allocs);
    for (uint32 i = 0; i < num_allocs; ++i)
        free(pool->allocs[i]);
    darr_free(pool->allocs);
}

void *
segpool_malloc(segpool_t *pool, uint32 num_bytes)
{
    if (!num_bytes)
        return 0;
    uint32              p2      = next_pow2_uint32(num_bytes);
    uint32              index   = highest_bit_uint32(p2);
    segpool_header_t    *header;
    if (index >= darr_num(pool->blocks))
    {
        uint32 num_blocks   = index + 1;
        uint32 diff         = num_blocks - darr_num(pool->blocks);
        darr_reserve(pool->blocks, num_blocks);
        memset(pool->blocks + darr_num(pool->blocks), 0,
            diff * sizeof(segpool_header_t**));
        _darr_head(pool->blocks)->num = num_blocks;
    }
    if (!darr_num(pool->blocks[index]))
    {
        uint32 num_new_free = darr_cap(pool->blocks[index]) * 2;
        if (!num_new_free)
            num_new_free = 8;
        darr_reserve(pool->blocks[index], num_new_free);
        uint32              unit_size   = p2 + sizeof(segpool_header_t);
        uint8               *memory     = emalloc(num_new_free * unit_size);
        darr_push(pool->allocs, memory);
        for (uint32 i = 0; i < num_new_free; ++i)
        {
            header = (segpool_header_t*)(memory + i * unit_size);
            header->num_bytes = p2;
            darr_push(pool->blocks[index], header);
        }
    }
    header = pool->blocks[index][--_darr_head(pool->blocks[index])->num];
    return header + 1;
}

void
segpool_free(segpool_t *pool, void *mem)
{
    if (!mem)
        return;
    segpool_header_t    *header = (segpool_header_t*)mem - 1;
    uint32              index   = highest_bit_uint32(header->num_bytes);
    darr_push(pool->blocks[index], header);
}

static void
_obj_pool_alloc_more(obj_pool_t *op, uint32 num_items)
{
    uint32 item_size = op->item_size;
    obj_pool_alloc_t alloc;
    alloc.mem = emalloc(num_items * item_size);
    alloc.num = num_items;
    darr_push(op->allocs, alloc);
    uint8 *item = (uint8*)alloc.mem;
    for (uint32 i = 0; i < num_items; ++i)
    {
        darr_push(op->free, (void*)item);
        item += item_size;
    }
    op->cap += num_items;
}

void
obj_pool_init(obj_pool_t *op, uint32 num_items, uint32 item_size)
{
    muta_assert(item_size);
    memset(op, 0, sizeof(obj_pool_t));
    darr_reserve(op->allocs, 16);
    op->item_size   = item_size;
    num_items       = next_pow2_uint32(num_items);
    if (num_items < 16)
        num_items = 16;
    _obj_pool_alloc_more(op, num_items);
}

void
obj_pool_destroy(obj_pool_t *op)
{
    for (uint32 i = 0; i < darr_num(op->allocs); ++i)
        free(op->allocs[i].mem);
    darr_free(op->allocs);
    darr_free(op->free);
    memset(op, 0, sizeof(obj_pool_t));
}

void
obj_pool_clear(obj_pool_t *op)
{
    darr_clear(op->free);
    uint32 item_sz = op->item_size;
    uint32 num_allocs = darr_num(op->allocs);
    for (uint32 i = 0; i < num_allocs; ++i)
    {
        uint32 num = op->allocs[i].num;
        for (uint32 j = 0; j < num; ++j)
            darr_push(op->free, (uint8*)op->allocs[i].mem + j * item_sz);
    }
}

void *
obj_pool_reserve(obj_pool_t *op)
{
    muta_assert(op->item_size);
    void    *ret;
    uint32  num_free = darr_num(op->free);
    if (!num_free)
    {
        uint32 num_new = next_pow2_uint32(op->cap) * 2;
        if (num_new < 16)
            num_new = 16;
        _obj_pool_alloc_more(op, num_new);
        num_free = _darr_head(op->free)->num;
    }
    ret = op->free[--num_free];
    _darr_head(op->free)->num = num_free;
    return ret;
}

void *
obj_pool_reserve_static(obj_pool_t *op)
{
    uint32 num_free = darr_num(op->free);
    if (!num_free)
        return 0;
    void *ret = op->free[--num_free];
    _darr_head(op->free)->num = num_free;
    return ret;
}

void
obj_pool_free(obj_pool_t *op, void *ptr)
    {darr_push(op->free, ptr);}

void
blocking_queue_init(blocking_queue_t *q, uint32 num, uint32 item_size)
{
    q->items        = emalloc(num * sizeof(item_size));
    q->num          = 0;
    q->max          = num;
    q->item_size    = item_size;
    mutex_init(&q->mtx);
    cond_var_init(&q->cv);
}

void
blocking_queue_destroy(blocking_queue_t *q)
{
    free(q->items);
    mutex_destroy(&q->mtx);
    cond_var_destroy(&q->cv);
}

void
blocking_queue_push(blocking_queue_t *q, void *item)
{
    mutex_lock(&q->mtx);
    while (q->num == q->max)
        cond_var_wait(&q->cv, &q->mtx);
    memcpy((uint8*)q->items + q->num * q->item_size, item, q->item_size);
    q->num++;
    mutex_unlock(&q->mtx);
}

void
blocking_queue_push_num(blocking_queue_t *q, void *items, uint32 num)
{
    uint32 num_added = 0;
    while (num_added < num)
    {
        mutex_lock(&q->mtx);
        while (q->num == q->max)
            cond_var_wait(&q->cv, &q->mtx);
        uint32 num_to_add = MIN(q->max - q->num, num - num_added);
        memcpy((uint8*)q->items + q->num * q->item_size,
            (uint8*)items + num_added * q->item_size,
            (num - num_added) * q->item_size);
        q->num += num_to_add;
        mutex_unlock(&q->mtx);
        num_added += num_to_add;
    }
}

uint32
blocking_queue_pop_num(blocking_queue_t *q, uint32 num, void *ret_array)
{
    mutex_lock(&q->mtx);
    uint32 num_to_pop = num < q->num ? num : q->num;
    memcpy(ret_array, q->items, num_to_pop * q->item_size);
    q->num -= num_to_pop;
    memmove(q->items, (uint8*)q->items + num_to_pop * q->item_size,
        q->num * q->item_size);
    mutex_unlock(&q->mtx);
    cond_var_signal_all(&q->cv);
    return num_to_pop;
}

void
blocking_queue_clear(blocking_queue_t *q)
{
    mutex_lock(&q->mtx);
    q->num = 0;
    mutex_unlock(&q->mtx);
}

void
event_init(event_buf_t *buf, uint32 item_size, int32 max)
{
    memset(buf, 0, sizeof(event_buf_t));
    buf->events     = emalloc(max * item_size);
    buf->max_events = max;
    buf->item_size = item_size;
    mutex_init(&buf->write_mutex);
    cond_var_init(&buf->write_cond_var);
    cond_var_init(&buf->read_cond_var);
}

void
event_destroy(event_buf_t *buf)
{
    cond_var_destroy(&buf->write_cond_var);
    cond_var_destroy(&buf->read_cond_var);
    mutex_destroy(&buf->write_mutex);
    free(buf->events);
}

void
event_clear(event_buf_t *buf)
{
    mutex_lock(&buf->write_mutex);
    buf->num_events = 0;
    mutex_unlock(&buf->write_mutex);
}

void
event_push(event_buf_t *buf, void *evs, int32 num)
{
    muta_assert(num >= 0);
    int32 num_pushed = 0;
    int32 num_to_push;
    while (num_pushed < num)
    {
        mutex_lock(&buf->write_mutex);
        while (buf->num_events == buf->max_events)
            cond_var_wait(&buf->write_cond_var, &buf->write_mutex);
        num_to_push = MIN(buf->max_events - buf->num_events, num - num_pushed);
        memcpy((uint8*)buf->events + buf->num_events * buf->item_size,
            (uint8*)evs + num_pushed * buf->item_size,
            num_to_push * buf->item_size);
        buf->num_events += num_to_push;
        num_pushed += num_to_push;
        mutex_unlock(&buf->write_mutex);
        cond_var_signal_all(&buf->read_cond_var);
    }
}

void
event_push_no_wait(event_buf_t *buf, void *evs, int32 num)
{
    muta_assert(num >= 0);
    mutex_lock(&buf->write_mutex);
    if (buf->num_events == buf->max_events)
    {
        int32 max = 2 * buf->max_events;
        if (max < buf->num_events + num)
            max = buf->num_events + num;
        buf->events     = erealloc(buf->events, max * buf->item_size);
        buf->max_events = max;
    }
    memcpy((uint8*)buf->events + buf->num_events * buf->item_size,
        evs, num * buf->item_size);
    buf->num_events += num;
    mutex_unlock(&buf->write_mutex);
}

int
event_wait(event_buf_t *buf, void *evs, int32 max, int timeout_ms)
{
    mutex_lock(&buf->write_mutex);
    while (!buf->num_events)
        if (cond_var_timed_wait(&buf->read_cond_var, &buf->write_mutex,
            timeout_ms))
        {
            mutex_unlock(&buf->write_mutex);
            return 0;
        }
    int num = MIN(buf->num_events, max);
    memcpy(evs, buf->events, num * buf->item_size);
    memmove(buf->events, (uint8*)buf->events + num * buf->item_size,
        (buf->num_events - num) * buf->item_size);
    buf->num_events -= num;
    mutex_unlock(&buf->write_mutex);
    cond_var_signal_all(&buf->write_cond_var);
    return num;
}

void
dynamic_stack_init(dynamic_stack_t *stack, size_t num_initial_bytes)
{
    stack->memory   = emalloc(num_initial_bytes);
    stack->num      = 0;
    stack->max      = num_initial_bytes;
}

void
dynamic_stack_destroy(dynamic_stack_t *stack)
{
    free(stack->memory);
    memset(stack, 0, sizeof(*stack));
}

void *
dynamic_stack_push(dynamic_stack_t *stack, size_t num_bytes)
{
    size_t offset = stack->num +
        (DYNAMIC_STACK_ALIGNMENT - stack->num % DYNAMIC_STACK_ALIGNMENT);
    size_t required = offset + num_bytes;
    if (stack->max < required)
    {
        stack->max = stack->max * 2;
        if (stack->max < required)
            stack->max = required +
                (DYNAMIC_STACK_ALIGNMENT - required % DYNAMIC_STACK_ALIGNMENT);
        stack->memory = erealloc(stack->memory, stack->max);
    }
    stack->num = required;
    return (char*)stack->memory + offset;
}

void *
dynamic_stack_push_static(dynamic_stack_t *stack, size_t num_bytes)
{
    size_t offset = stack->num;
    size_t required = offset + num_bytes +
        (DYNAMIC_STACK_ALIGNMENT - num_bytes % DYNAMIC_STACK_ALIGNMENT);
    if (stack->max < required)
        return 0;
    stack->num = required;
    return (char*)stack->memory + offset;
}

void
dynamic_stack_pop(dynamic_stack_t *stack, size_t offset)
{
    muta_assert(stack->num >= offset);
    stack->num = offset;
}

void
dynamic_stack_clear(dynamic_stack_t *stack)
    {stack->num = 0;}

size_t
dynamic_stack_offset(dynamic_stack_t *stack, void *memory)
    {return (size_t)((char*)memory - (char*)stack->memory);}

void *
dynamic_stack_at(dynamic_stack_t *stack, size_t offset)
{
    muta_assert(offset < stack->num);
    return (char*)stack->memory + offset;
}

int
copy_file(const char *src_path, const char *dst_path)
{
    FILE *src = fopen(src_path, "rb");
    if (!src)
        return 1;
    FILE *dst = fopen(dst_path, "wb+");
    if (!dst)
    {
        fclose(src);
        return 2;
    }
    uint8   buf[1024];
    int     ret = 0;
    for (;;)
    {
        size_t num_read = fread(buf, sizeof(buf[0]),
            sizeof(buf) / sizeof(buf[0]), src);
        if (fwrite(buf, sizeof(buf[0]), num_read, dst) != num_read)
        {
            ret = 3;
            break;

        }
        if (num_read != sizeof(buf))
        {
            if (!feof(src))
                ret = 4;
            break;
        }
    }
    fclose(dst);
    fclose(src);
    return ret;
}

enum parse_def_file_result
parse_def_file(const char *fp,
    int (*on_def)(parse_def_file_context_t *ctx, const char *def,
        const char *val),
    int (*on_opt)(parse_def_file_context_t *ctx, const char *opt,
        const char *val),
    void *user_data)
{
    #define FMT_ERR_IF(v_, ret_) \
    if ((v_)) \
    { \
        fmt_error = 1; \
        ret = ret_; \
        goto out; \
    }
    int                         ret         = 0;
    char                        *line       = 0;
    bool32                      fmt_error   = 0;
    parse_def_file_context_t    context     = {0, user_data};
    int             c;
    FILE *f = fopen(fp, "r");
    if (!f)
    {
        printf("Failed to open def file %s.\n", fp);
        return PARSE_DEF_FILE_FOPEN_FAILED;
    }
    darr_reserve(line, 256);
    for (;;)
    {
        darr_clear(line);
        /* Find the next non-whitespace */
        for (;;)
        {
            c = fgetc(f);
            if (c == EOF)
                goto out;
            if (c == '\n')
            {
                context.line++;
                continue;
            }
            if (c != ' ' && c != '\t')
                break;
        }
        fseek(f, ftell(f) - 1, SEEK_SET);
        /* Read the line into a dynamic buffer */
        while ((c = fgetc(f)) != EOF && c != '\n')
           darr_push(line, (char)c);
        if (c == '\n')
            context.line++;
        darr_push(line, 0); /* Zero-terminate */
        str_strip_trailing_spaces(line);
        _darr_head(line)->num = (uint32)strlen(line) + 1;
        if (line[0] == '#') /* Line is a comment */
            continue;
        /* Line begins a new definition or is a value? 0 = def, 1 = value */
        int line_type           = -1;
        int first_space_index   = -1;
        int line_len            = (int)darr_num(line);
        int marker_index;
        for (int i = 0; i < line_len; ++i)
        {
            if (line[i] == ':')
            {
                line_type       = 0;
                marker_index    = i;
                break;
            }
            if (line[i] == '=')
            {
                line_type       = 1;
                marker_index    = i;
                break;
            }
            if ((line[i] == ' ' || line[i] == '\t') && first_space_index < 0)
                first_space_index = i;
        }
        FMT_ERR_IF(line_type < 0, 5);
        /* The line creates a new definition */
        if (line_type == 0)
        {
            char *id = line + marker_index + 1;
            str_strip_trailing_spaces(id);
            _darr_head(line)->num = (uint32)strlen(line) + 1;
            line[first_space_index >= 0 ? first_space_index : marker_index] = 0;
            FMT_ERR_IF(!str_is_ascii(id), 6);
            FMT_ERR_IF(on_def(&context, line, id), 7);
            continue;
        }
        int term = first_space_index >= 0 ? first_space_index : marker_index;
        line[term] = 0;
        char    *value  = line + term + 1;
        int     j       = 0;
        first_space_index = -1;
        while (*value != '=' && *value != '\n' && *value)
        {
            if (*value == ' ' && first_space_index < 0)
                first_space_index = j;
            value++, j++;
        }
        if (*value != '=')
            return 2;
        str_strip_trailing_spaces(++value);
        _darr_head(line)->num = (uint32)strlen(line);
        FMT_ERR_IF(on_opt(&context, line, value), 8);
    }
    out:
        if (fmt_error)
            printf("Format error in def file %s. Line "
                "contents: '%s', line %zu, error %d.\n", fp, line, context.line,
                ret);
        if (ret)
            printf("Errors parsing def file %s, code %d.\n", fp, ret);
        fclose(f);
        darr_free(line);
        if (ret)
            return PARSE_DEF_FILE_SYNTAX_ERROR;
        return PARSE_DEF_FILE_SUCCESS;
    #undef FMT_ERR_IF
}

enum config_parse_error
parse_cfg_file_with_option_list(const char *path, config_option_t *opts,
    uint32 num_opts)
{
    if (!num_opts)
        return 0;
    parse_cfg_file_with_option_list_context_t context =
    {
        .opts       = opts,
        .num_opts   = num_opts,
        .have_opts  = ecalloc(num_opts * sizeof(bool8))
    };
    int err = parse_cfg_file(path, _parse_cfg_file_with_option_list_on_opt,
        &context);
    int ret = 0;
    if (err)
    {
        ret = CONFIG_PARSE_ERROR_PARSING_FAILED;
        goto out;
    }
    for (uint32 i = 0; i < num_opts; ++i)
        if (!opts[i].optional && !context.have_opts[i])
        {
            DEBUG_PRINTFF("Missing option %s in file '%s'.\n", opts[i].name,
                path);
            ret = CONFIG_PARSE_ERROR_MISSING_OPTIONS;
        }
    out:
        free(context.have_opts);
        return ret;

}

void
arg_str_init(arg_str_t *argv)
{
    memset(argv, 0, sizeof(*argv));
    uint32 num = 16;
    darr_reserve(argv->argv, num);
    for (uint32 i = 0; i < num; ++i)
        darr_push(argv->argv, dstr_create_empty(ARG_STR_DEFAULT_ARG_LEN));
}

void
arg_str_destroy(arg_str_t *argv)
{
    uint32 num = darr_num(argv->argv);
    for (uint32 i = 0; i < num; ++i)
        dstr_free(&argv->argv[i]);
    darr_free(argv->argv);
    memset(argv, 0, sizeof(*argv));
}

void
arg_str_parse(arg_str_t *argv, const char *str)
{
    /* Clear previous args. */
    uint32 num = darr_num(argv->argv);
    for (uint32 i = 0; i < num; ++i)
        dstr_set_len(&argv->argv[i], 0);
    argv->argc = 0;
    if (!str[0])
        return;
    bool32  escaped         = 0;
    bool32  double_quoted   = 0;
    bool32  single_quoted   = 0;
    for (const char *c = str; ; ++c)
    {
        switch (*c)
        {
        case '\\':
            if (escaped)
            {
                dstr_append_char(&argv->argv[argv->argc], *c);
                escaped = 0;
            } else
                escaped = 1;
            break;
        case '"':
            if (escaped)
            {
                dstr_append_char(&argv->argv[argv->argc], *c);
                escaped = 0;
            } else if (double_quoted)
                double_quoted = 0;
            else
                double_quoted = 1;
            break;
        case '\'':
            if (escaped)
            {
                dstr_append_char(&argv->argv[argv->argc], *c);
                escaped = 0;
            } else if (single_quoted)
                single_quoted = 0;
            else
                single_quoted = 1;
            break;
        case ' ':
            if (escaped)
                dstr_append_char(&argv->argv[argv->argc], *c);
            if (double_quoted || single_quoted)
                dstr_append_char(&argv->argv[argv->argc], *c);
            else if (dstr_len(argv->argv[argv->argc]))
            {
                argv->argc++;
                if (darr_num(argv->argv) < (uint32)argv->argc)
                    darr_push(argv->argv,
                        dstr_create_empty(ARG_STR_DEFAULT_ARG_LEN));
            }
            break;
        case '\0':
            if (dstr_len(argv->argv[argv->argc]))
                argv->argc++;
            return;
        default:
            dstr_append_char(&argv->argv[argv->argc], *c);
        }
    }
}

static void
_parse_cfg_file_with_option_list_on_opt(void *ctx, const char *opt,
    const char *val)
{
    parse_cfg_file_with_option_list_context_t *context = ctx;
    int num_opts    = (int)context->num_opts;
    int opt_index   = -1;
    for (int i = 0; i < num_opts; ++i)
        if (!strcmp(context->opts[i].name, opt))
        {
            opt_index = i;
            break;
        }
    if (opt_index == -1)
    {
        DEBUG_PRINTFF("Option '%s' not found.\n", opt);
        return;
    }
    DEBUG_PRINTFF("Option %s\n", opt);
    config_option_t *option = &context->opts[opt_index];
    switch (option->type)
    {
    case CONFIG_OPTION_TYPE_INT16:
    {
        int16 v;
        if (sscanf(val, "%hu", &v) != 1)
            break;
        *context->opts[opt_index].i16.value = v;
        context->have_opts[opt_index] = 1;
    }
        break;
    case CONFIG_OPTION_TYPE_UINT16:
    {
        uint16 v;
        if (sscanf(val, "%hu", &v) != 1)
            break;
        *context->opts[opt_index].u16.value = v;
        context->have_opts[opt_index] = 1;
    }
        break;
    case CONFIG_OPTION_TYPE_INT32:
    {
        int32 v;
        if (sscanf(val, "%" PRIi32, &v) != 1)
            break;
        *context->opts[opt_index].i32.value = v;
        context->have_opts[opt_index] = 1;
    }
        break;
    case CONFIG_OPTION_TYPE_UINT32:
    {
        uint32 v;
        if (sscanf(val, "%" PRIu32, &v) != 1)
            break;
        *context->opts[opt_index].u32.value = v;
        context->have_opts[opt_index] = 1;
    }
        break;
    case CONFIG_OPTION_TYPE_INT64:
    {
        int64 v;
        if (sscanf(val, "%" PRIi64, &v) != 1)
            break;
        *context->opts[opt_index].i64.value = v;
        context->have_opts[opt_index] = 1;
    }
        break;
    case CONFIG_OPTION_TYPE_UINT64:
    {
        uint64 v;
        if (sscanf(val, "%" PRIu64, &v) != 1)
            break;
        *context->opts[opt_index].u64.value = v;
        context->have_opts[opt_index] = 1;
    }
        break;
    case CONFIG_OPTION_TYPE_BOOL32:
    {
        bool32 v;
        if (str_to_bool(val, &v))
            break;
        *context->opts[opt_index].b32.value = v;
        context->have_opts[opt_index] = 1;
    }
        break;
    case CONFIG_OPTION_TYPE_STR:
        if (!context->have_opts[opt_index])
            *context->opts[opt_index].str.value = dstr_create(val);
        else
            dstr_set(context->opts[opt_index].str.value, val);
        context->have_opts[opt_index] = 1;
        break;
    case CONFIG_OPTION_TYPE_CUSTOM:
        if (!context->opts[opt_index].custom.callback(val,
            context->opts[opt_index].custom.value))
            context->have_opts[opt_index] = 1;
        break;
    }
}
