#ifndef MUTA_SHARED_STR_HASHTABLE_H
#define MUTA_SHARED_STR_HASHTABLE_H

#include <string.h>
#include "hashtable.h"

static inline size_t str_hashtable_compute_hash(const void *key, size_t size)
{
    (void)size;
    return hashtable_hash(*(const char**)key, strlen(*(const char**)key));
}

static inline int str_hashtable_compare_keys(const void *a, const void *b, size_t size)
    {return strcmp(*(const char**)a, *(const char**)b);}

static inline int str_hashtable_copy_key(void *dst, const void *src, size_t size)
{
    size_t len = strlen(*(const char**)src);
    *(char**)dst = malloc(len + 1);
    if (!*(char**)dst)
        return 1;
    memcpy(*(char**)dst, *(const char**)src, len + 1);
    return 0;
}

static inline void str_hashtable_free_key(void *key)
    {free(*(char**)key);}

#define STR_HASHTABLE_DEFINITION(table_name, value_type) \
    hashtable_define_ext(table_name, const char *, value_type, \
        str_hashtable_compute_hash, str_hashtable_compare_keys, \
        str_hashtable_copy_key, str_hashtable_free_key)

#endif /* MUTA_SHARED_STR_HASHTABLE_H */
