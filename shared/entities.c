#include "entities.h"
#include "common_utils.h"
#include "mdb.h"
#include "hashtable.h"

typedef struct creature_table       creature_table_t;
typedef struct static_object_table  static_object_table_t;
typedef struct dynamic_object_table dynamic_object_table_t;
typedef struct player_race_table    player_race_table_t;

hashtable_define(creature_table, creature_type_id_t, creature_def_t);
hashtable_define(static_object_table, sobj_type_id_t,
    static_object_def_t);
hashtable_define(dynamic_object_table, dobj_type_id_t,
    dynamic_object_def_t);
hashtable_define(player_race_table, player_race_id_t, player_race_def_t);

static uint32                   _load_flags; /* What flags were used to load? */
static creature_table_t         _creatures;
static static_object_table_t    _static_objects;
static dynamic_object_table_t   _dynamic_objects;
static player_race_table_t      _player_races;

static int
_load_creature_defs(void);

static int
_load_player_race_defs(void);

static int
_load_dynamic_object_defs(void);

static int
_load_static_objects(void);

static void
_destroy_player_race_defs(void);

static void
_destroy_creature_defs(void);

static void
_destroy_static_objects(void);

static void
_destroy_dynamic_object_defs(void);

int
ent_load(uint32 flags)
{
    int err = 0;
    if (!flags)
        flags = 0xFFFFFFFF;
    _load_flags = flags;
    if (flags & ENT_LOAD_CREATURES && _load_creature_defs())
        err |= ENT_LOAD_CREATURES;
    if (flags & ENT_LOAD_PLAYER_RACES && _load_player_race_defs())
        err |= ENT_LOAD_PLAYER_RACES;
    if (flags & ENT_LOAD_DYNAMIC_OBJECTS && _load_dynamic_object_defs())
        err |= ENT_LOAD_DYNAMIC_OBJECTS;
    if (flags & ENT_LOAD_STATIC_OBJECTS && _load_static_objects())
        err |= ENT_LOAD_STATIC_OBJECTS;
    if (err)
        ent_destroy();
    return err;
}

void
ent_destroy()
{
    if (_load_flags & ENT_LOAD_CREATURES)
        _destroy_creature_defs();
    if (_load_flags & ENT_LOAD_PLAYER_RACES)
        _destroy_player_race_defs();
    if (_load_flags & ENT_LOAD_DYNAMIC_OBJECTS)
        _destroy_dynamic_object_defs();
    if (_load_flags & ENT_LOAD_STATIC_OBJECTS)
        _destroy_static_objects();
    _load_flags = 0;
}

creature_def_t *
ent_get_creature_def(creature_type_id_t id)
    {return creature_table_find(&_creatures, id);}

static_object_def_t *
ent_get_static_object_def(sobj_type_id_t type_id)
    {return static_object_table_find(&_static_objects, type_id);}

dynamic_object_def_t *
ent_get_dynamic_object_def(dobj_type_id_t type_id)
    {return dynamic_object_table_find(&_dynamic_objects, type_id);}

player_race_def_t *
ent_get_player_race_def(player_race_id_t type_id)
    {return player_race_table_find(&_player_races, type_id);}

uint32
ent_num_creature_defs(void)
    {return (uint32)hashtable_num_values(_creatures);}

uint32
ent_num_static_object_defs(void)
    {return (uint32)hashtable_num_values(_static_objects);}

uint32
ent_num_dynamic_object_defs(void)
    {return (uint32)hashtable_num_values(_dynamic_objects);}

uint32
ent_num_player_race_defs(void)
    {return (uint32)hashtable_num_values(_player_races);}

void
ent_for_each_creature_def(
    void (*callback)(creature_def_t *def, void *user_data), void *user_data)
{
    creature_type_id_t  type_id;
    creature_def_t      def;
    hashtable_for_each_pair(_creatures, type_id, def)
        callback(&def, user_data);
}

void
ent_for_each_static_object_def(
    void (*callback)(static_object_def_t *def, void *user_data),
    void *user_data)
{
    sobj_type_id_t    type_id;
    static_object_def_t     def;
    hashtable_for_each_pair(_static_objects, type_id, def)
        callback(&def, user_data);
}

void
ent_for_each_dynamic_object_def(
    void (*callback)(dynamic_object_def_t *def, void *user_data),
    void *user_data)
{
    dobj_type_id_t   type_id;
    dynamic_object_def_t    def;
    hashtable_for_each_pair(_dynamic_objects, type_id, def)
        callback(&def, user_data);
}

void
ent_for_each_player_race_def(
    void (*callback)(player_race_def_t *def, void *user_data), void *user_data)
{
    dobj_type_id_t   type_id;
    player_race_def_t       def;
    hashtable_for_each_pair(_player_races, type_id, def)
        callback(&def, user_data);
}

float
ent_convert_move_speed_to_seconds(uint8 move_speed)
{
    return ENT_MOVE_MAX_SECONDS - (float)move_speed / 255.f *
        ENT_MOVE_MAX_SECONDS;
}

static int
_load_creature_defs(void)
{
    mdb_t mdb;
    if (mdb_open(&mdb, "data/common/creatures.mdb"))
        return 1;
    int     ret = 0;
    uint32  id_index;
    uint32  name_index;
    uint32  display_id_index;
    uint32  description_index;
    uint32  script_name_index;
    int     index;
    if ((index = mdb_get_col_index(&mdb, "id")) < 0)
        {ret = 2; goto out;}
    id_index = (uint32)index;
    if ((index = mdb_get_col_index(&mdb, "name")) < 0)
        {ret = 3; goto out;}
    name_index = (uint32)index;
    if ((index = mdb_get_col_index(&mdb, "display_id")) < 0)
        {ret = 4; goto out;}
    display_id_index = (uint32)index;
    if ((index = mdb_get_col_index(&mdb, "description")) < 0)
        {ret = 5; goto out;}
    description_index = (uint32)index;
    if ((index = mdb_get_col_index(&mdb, "script_name")) < 0)
        {ret = 6; goto out;}
    script_name_index = (uint32)index;
    uint32 num_rows = mdb_num_rows(&mdb);
    creature_table_einit(&_creatures, num_rows);
    for (uint32 i = 0; i < num_rows; ++i)
    {
        creature_def_t def = {0};
        mdb_row_t row = mdb_get_row_by_index(&mdb, i);
        def.id = *(uint32*)mdb_get_field(&mdb, row, id_index);
        def.name = dstr_create(*(dchar**)mdb_get_field(&mdb, row, name_index));
        def.description = dstr_create(*(dchar**)mdb_get_field(&mdb, row,
            description_index));
        def.script_name = dstr_create(*(dchar**)mdb_get_field(&mdb, row,
            script_name_index));
        def.display_id = *(uint32*)mdb_get_field(&mdb, row, display_id_index);
        creature_table_einsert(&_creatures, def.id, def);
    }
    out:
        mdb_close(&mdb);
        return ret;
}

static int
_load_player_race_defs(void)
{
    memset(&_player_races, 0, sizeof(_player_races));
    mdb_t mdb;
    if (mdb_open(&mdb, "data/common/player_races.mdb"))
        return 1;
    int ret = 0;
    int index;
    if ((index = mdb_get_col_index_with_type(&mdb, MDB_COL_UINT32, "id")) < 0)
        {ret = 2; goto out;}
    uint32 id_col_index = (uint32)index;
    if ((index = mdb_get_col_index_with_type(&mdb, MDB_COL_STR, "name")) < 0)
        {ret = 2; goto out;}
    uint32 name_col_index = (uint32)index;
    if ((index = mdb_get_col_index_with_type(&mdb, MDB_COL_STR, "description"))
        < 0)
        {ret = 2; goto out;}
    uint32 description_col_index = (uint32)index;
    if ((index = mdb_get_col_index_with_type(&mdb, MDB_COL_UINT32,
        "display_id")) < 0)
        {ret = 2; goto out;}
    uint32 display_id_col_index = (uint32)index;
    if ((index = mdb_get_col_index_with_type(&mdb, MDB_COL_UINT32,
        "start_area_id")) < 0)
        {ret = 2; goto out;}
    uint32 start_area_id_index  = (uint32)index;
    uint32 num_rows             = mdb_num_rows(&mdb);
    player_race_table_einit(&_player_races, num_rows);
    for (uint32 i = 0; i < num_rows; ++i)
    {
        mdb_row_t row = mdb_get_row_by_index(&mdb, i);
        uint8 id = *(uint8*)mdb_get_field(&mdb, row, id_col_index);
        player_race_def_t def = {0};
        def.id    = id;
        def.name = dstr_create(
            *(dchar**)mdb_get_field(&mdb, row, name_col_index));
        def.description = dstr_create(
            *(dchar**)mdb_get_field(&mdb, row, description_col_index));
        def.display_id = *(uint32*)mdb_get_field(&mdb, row,
            display_id_col_index);
        def.start_area_id = *(uint32*)mdb_get_field(&mdb, row,
            start_area_id_index);
        player_race_table_einsert(&_player_races, def.id, def);
    }
    out:
        mdb_close(&mdb);
        return ret;
}

static int
_load_dynamic_object_defs(void)
{
    mdb_t mdb;
    if (mdb_open(&mdb, "data/common/dynamic_objects.mdb"))
        return 1;
    int ret = 0;
    int id_col_index = mdb_get_col_index_with_type(&mdb, MDB_COL_UINT32, "id");
    if (id_col_index < 0)
        {ret = 1; goto out;}
    int name_col_index = mdb_get_col_index_with_type(&mdb, MDB_COL_STR, "name");
    if (name_col_index < 0)
        {ret = 2; goto out;}
    int description_col_index = mdb_get_col_index_with_type(&mdb, MDB_COL_STR,
        "description");
    if (description_col_index < 0)
        {ret = 3; goto out;}
    int display_id_col_index = mdb_get_col_index_with_type(&mdb, MDB_COL_UINT32,
        "display_id");
    if (display_id_col_index < 0)
        {ret = 4; goto out;}
    int is_container_col_index = mdb_get_col_index_with_type(&mdb,
        MDB_COL_UINT8, "is_container");
    if (is_container_col_index < 0)
        {ret = 5; goto out;}
    int container_width_col_index = mdb_get_col_index_with_type(&mdb,
        MDB_COL_UINT8, "container_width");
    if (container_width_col_index < 0)
        {ret = 6; goto out;}
    int container_height_col_index = mdb_get_col_index_with_type(&mdb,
        MDB_COL_UINT8, "container_height");
    if (container_height_col_index < 0)
        {ret = 7; goto out;}
    int width_in_container_col_index = mdb_get_col_index_with_type(&mdb,
        MDB_COL_UINT8, "width_in_container");
    if (width_in_container_col_index < 0)
        {ret = 7; goto out;}
    int height_in_container_col_index = mdb_get_col_index_with_type(&mdb,
        MDB_COL_UINT8, "height_in_container");
    if (height_in_container_col_index < 0)
        {ret = 8; goto out;}
    int disable_pickup_col_index = mdb_get_col_index_with_type(&mdb,
        MDB_COL_UINT8, "disable_pickup");
    uint32 num_rows = mdb_num_rows(&mdb);
    dynamic_object_table_einit(&_dynamic_objects, num_rows);
    for (uint32 i = 0; i < num_rows; ++i)
    {
        mdb_row_t row = mdb_get_row_by_index(&mdb, i);
        dynamic_object_def_t def = {0};
        def.id = *(uint32*)mdb_get_field(&mdb, row, id_col_index);
        def.name = dstr_create(
            *(dchar**)mdb_get_field(&mdb, row, name_col_index));
        def.description = dstr_create(
            *(dchar**)mdb_get_field(&mdb, row, description_col_index));
        def.display_id = *(uint32*)mdb_get_field(&mdb, row,
            display_id_col_index);
        def.is_container = *(uint8*)mdb_get_field(&mdb, row,
            is_container_col_index);
        def.container_width = *(uint8*)mdb_get_field(&mdb, row,
            container_width_col_index);
        def.container_height = *(uint8*)mdb_get_field(&mdb, row,
            container_height_col_index);
        def.width_in_container = *(uint8*)mdb_get_field(&mdb, row,
            width_in_container_col_index);
        def.height_in_container = *(uint8*)mdb_get_field(&mdb, row,
            height_in_container_col_index);
        if (def.width_in_container < 1 || def.height_in_container < 1)
        {
            DEBUG_PRINTFF("Dynamic object width and height in container must "
                "be at least 1.\n");
            _destroy_dynamic_object_defs();
            ret = 9;
            goto out;
        }
        def.flags = 0;
        if (*(uint8*)mdb_get_field(&mdb, row, disable_pickup_col_index))
            def.flags |= ENT_DYNAMIC_OBJECT_DISABLE_PICK_UP;
        dynamic_object_table_einsert(&_dynamic_objects, def.id, def);
    }
    out:
        if (ret)
            DEBUG_PRINTFF("Failed with error %d.\n", ret);
        mdb_close(&mdb);
        return ret;
}

static int
_load_static_objects(void)
{
    mdb_t mdb;
    if (mdb_open(&mdb, "data/common/static_objects.mdb"))
        return 1;
    int ret = 0;
    int index;
    if ((index = mdb_get_col_index(&mdb, "id")) < 0)
        {ret = 2; goto out;}
    uint32 id_col_index = (uint32)index;
    if ((index = mdb_get_col_index(&mdb, "name")) < 0)
        {ret = 3; goto out;}
    uint32 name_col_index = (uint32)index;
    if ((index = mdb_get_col_index(&mdb, "description")) < 0)
        {ret = 3; goto out;}
    uint32 description_col_index = (uint32)index;
    if ((index = mdb_get_col_index(&mdb, "display_id")) < 0)
        {ret = 3; goto out;}
    uint32 display_id_col_index = (uint32)index;
    if ((index = mdb_get_col_index(&mdb, "display_type")) < 0)
        {ret = 4; goto out;}
    uint32 display_type_col_index   = (uint32)index;
    uint32 num_rows                 = mdb_num_rows(&mdb);
    static_object_table_init(&_static_objects, num_rows);
    for (uint32 i = 0; i < num_rows; ++i)
    {
        static_object_def_t def;
        mdb_row_t row = mdb_get_row_by_index(&mdb, i);
        def.id = *(uint32*)mdb_get_field(&mdb, row, id_col_index);
        def.graphic_type = *(uint8*)mdb_get_field(&mdb, row,
            display_type_col_index);
        if (def.graphic_type > 2)
            {ret = 5; goto out;}
        switch (def.graphic_type)
        {
        case ENT_GRAPHIC_ISO_SPRITE:
            def.graphic_data.iso_sprite = *(uint32*)mdb_get_field(&mdb, row,
                display_id_col_index);
        }
        def.name = dstr_create(*(dchar**)mdb_get_field(&mdb, row,
            name_col_index));
        def.description = dstr_create(*(dchar**)mdb_get_field(&mdb, row,
            description_col_index));
        static_object_table_einsert(&_static_objects, def.id, def);
    }
    out:
        mdb_close(&mdb);
        return ret;
}

static void
_destroy_player_race_defs(void)
{
    player_race_id_t    race_id;
    player_race_def_t   def;
    hashtable_for_each_pair(_player_races, race_id, def)
    {
        dstr_free(&def.name);
        dstr_free(&def.description);
    }
    player_race_table_destroy(&_player_races);
}

static void
_destroy_creature_defs(void)
{
    creature_type_id_t  type_id;
    creature_def_t      def;
    hashtable_for_each_pair(_creatures, type_id, def)
    {
        dstr_free(&def.name);
        dstr_free(&def.description);
        dstr_free(&def.script_name);
    }
    creature_table_destroy(&_creatures);
}

static void
_destroy_static_objects(void)
{
    sobj_type_id_t    type_id;
    static_object_def_t     def;
    hashtable_for_each_pair(_static_objects, type_id, def)
    {
        dstr_free(&def.name);
        dstr_free(&def.description);
    }
    static_object_table_destroy(&_static_objects);
}

static void
_destroy_dynamic_object_defs(void)
{
    dobj_type_id_t   type_id;
    dynamic_object_def_t    def;
    hashtable_for_each_pair(_dynamic_objects, type_id, def)
    {
        dstr_free(&def.name);
        dstr_free(&def.description);
    }
    dynamic_object_table_destroy(&_dynamic_objects);
}
