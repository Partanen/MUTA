/* log.h
 * API for async logging to a text file. */

#ifndef MUTA_SHARED_LOG_H
#define MUTA_SHARED_LOG_H
#include "types.h"

typedef struct log_t            log_t;
typedef struct log_category_t   log_category_t;
typedef struct log_tag_t        log_tag_t;

struct log_t
{
    void        *file;
    log_tag_t   *tags;
    uint32      num_tags;
    int32       queued_count;
};

struct log_category_t
{
    int         id;
    /* Used for log_post. Must be unique, >= 0 and < num_categories. */

    const char  *tag;
    /* A tag string printed before each message of this type, making it easier
     * to grep for a given type of messages. */
};

int
log_api_init(uint32 queue_size);
/* log_api_init()
 * Call before using any other log functions. queue_size can be set to zero for
 * a default value: it's the size of the internal command buffer used by the
 * log_printf() function. */

void
log_api_destroy(void);
/* log_api_destroy()
 * Frees the API's internal structures. All existing log_t instances must be
 * destroyed before calling this. */

int
log_init(log_t *log, const char *directory_path, const char *base_file_name,
    log_category_t *cats, uint32 num_cats);

void
log_destroy(log_t *log);
/* log_destroy()
 * Destroys a log and closes its file. Make sure log_printf is not called after
 * this function has been called. */

void
log_printf(log_t *log, uint category, const char *fmt, ...);

#endif /* MUTA_SHARED_LOG_H */

