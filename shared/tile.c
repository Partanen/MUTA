#include "tile.h"
#include "common_utils.h"

typedef struct parse_context_t parse_context_t;

enum tile_ramp ramp_up_table[NUM_ISODIRS] =
{
    TILE_RAMP_NORTH, /* ISODIR_NORTH */
    TILE_RAMP_NONE,  /* ISODIR_NORTH_EAST */
    TILE_RAMP_EAST,  /* ISODIR_EAST */
    TILE_RAMP_NONE,  /* ISODIR_SOUTH_EAST */
    TILE_RAMP_SOUTH, /* ISODIR_SOUTH */
    TILE_RAMP_NONE,  /* ISODIR_SOUTH_WEST */
    TILE_RAMP_WEST,  /* ISODIR_WEST */
    TILE_RAMP_NONE   /* ISODIR_NORTH_WEST */
};
/* Required directions of movement per each ramp */

enum tile_ramp ramp_down_table[NUM_ISODIRS] =
{
    TILE_RAMP_SOUTH, /* ISODIR_NORTH */
    TILE_RAMP_NONE,  /* ISODIR_NORTH_EAST */
    TILE_RAMP_WEST,  /* ISODIR_EAST */
    TILE_RAMP_NONE,  /* ISODIR_SOUTH_EAST */
    TILE_RAMP_NORTH, /* ISODIR_SOUTH */
    TILE_RAMP_NONE,  /* ISODIR_SOUTH_WEST */
    TILE_RAMP_EAST,  /* ISODIR_WEST */
    TILE_RAMP_NONE   /* ISODIR_NORTH_WEST */
};
/* Required directions of movement per each ramp */

struct parse_context_t
{
    bool32  load_graphics;
    tile_t  tile_id;
    bool32  have_name;
    bool32  have_passthrough;
    bool32  have_ramp;
    bool32  have_clip;
    bool32  have_offset_x;
    bool32  have_offset_y;
};

tile_def_t           *_tile_defs;
tile_graphic_def_t   *_tile_graphic_defs;

static int
_on_def(parse_def_file_context_t *ctx, const char *def, const char *val);

static int
_on_opt(parse_def_file_context_t *ctx, const char *opt, const char *val);

int
tile_load(bool32 load_graphics)
{
    _tile_defs = ecalloc((TILE_MAX_TYPES) * sizeof(tile_def_t));
    if (load_graphics)
        _tile_graphic_defs = ecalloc(
            (TILE_MAX_TYPES) * sizeof(tile_graphic_def_t));
    /* Initialize all tiles to empty tile */
    for (uint i = 0; i < TILE_MAX_TYPES; ++i)
    {
        _tile_defs[i].name          = dstr_create("Empty");
        _tile_defs[i].passthrough   = 1;
        _tile_defs[i].ramp          = TILE_RAMP_NONE;
        _tile_defs[i].defined       = 0;
    }
    _tile_defs[0].defined = 1;
    /* init graffat tile nollaan */
    parse_context_t context = {.load_graphics  = load_graphics};
    if (parse_def_file("data/common/tiles.def", _on_def, _on_opt, &context))
    {
        tile_destroy();
        return 1;
    }
    return 0;
}

void
tile_destroy(void)
{
    for (uint i = 0; i < TILE_MAX_TYPES; ++i)
        dstr_free(&_tile_defs[i].name);
    free(_tile_defs);
    free(_tile_graphic_defs);
}

tile_def_t *
tile_get_def(tile_t tile)
    {return &_tile_defs[tile];}

tile_graphic_def_t *
tile_get_graphic_def(tile_t tile)
    {return &_tile_graphic_defs[tile];}

int
tile_check_ramp_up(int dir, enum tile_ramp ramp)
{
    switch (ramp)
    {
    case TILE_RAMP_NORTH:
        return (dir == ISODIR_NORTH || dir == ISODIR_NORTH_EAST ||
            dir == ISODIR_NORTH_WEST) ? 0 : 1;
    case TILE_RAMP_EAST:
        return (dir == ISODIR_EAST || dir == ISODIR_NORTH_EAST ||
            dir == ISODIR_SOUTH_EAST)  ? 0 : 1;
    case TILE_RAMP_SOUTH:
        return (dir == ISODIR_SOUTH || dir == ISODIR_SOUTH_EAST ||
            dir == ISODIR_SOUTH_WEST) ? 0 : 1;
    case TILE_RAMP_WEST:
        return (dir == ISODIR_WEST || dir == ISODIR_SOUTH_WEST ||
            dir == ISODIR_NORTH_WEST)  ? 0 : 1;
    case TILE_RAMP_NORTH_EAST:
        return (dir == ISODIR_NORTH || dir == ISODIR_NORTH_EAST ||
            dir == ISODIR_EAST) ? 0 : 1;
    case TILE_RAMP_NORTH_WEST:
        return (dir == ISODIR_WEST || dir == ISODIR_NORTH_WEST ||
            dir == ISODIR_WEST) ? 0 : 1;
    case TILE_RAMP_SOUTH_EAST:
        return (dir == ISODIR_SOUTH || dir == ISODIR_SOUTH_EAST ||
            dir == ISODIR_EAST) ? 0 : 1;
    case TILE_RAMP_SOUTH_WEST:
        return (dir == ISODIR_SOUTH || dir == ISODIR_SOUTH_WEST ||
            dir == ISODIR_WEST) ? 0 : 1;
    default:
        return 1;
    };
}

int
tile_check_ramp_down(int dir, enum tile_ramp ramp)
{
    switch (ramp)
    {
    case TILE_RAMP_SOUTH:
        return (dir == ISODIR_NORTH || dir == ISODIR_NORTH_EAST ||
            dir == ISODIR_NORTH_WEST) ? 0 : 1;
    case TILE_RAMP_WEST:
        return (dir == ISODIR_EAST || dir == ISODIR_NORTH_EAST ||
            dir == ISODIR_SOUTH_EAST)  ? 0 : 1;
    case TILE_RAMP_NORTH:
        return (dir == ISODIR_SOUTH || dir == ISODIR_SOUTH_EAST ||
            dir == ISODIR_SOUTH_WEST) ? 0 : 1;
    case TILE_RAMP_EAST:
        return (dir == ISODIR_WEST || dir == ISODIR_SOUTH_WEST ||
            dir == ISODIR_NORTH_WEST)  ? 0 : 1;
    case TILE_RAMP_SOUTH_WEST:
        return (dir == ISODIR_NORTH || dir == ISODIR_NORTH_EAST ||
            dir == ISODIR_EAST) ? 0 : 1;
    case TILE_RAMP_SOUTH_EAST:
        return (dir == ISODIR_WEST || dir == ISODIR_NORTH_WEST ||
            dir == ISODIR_WEST) ? 0 : 1;
    case TILE_RAMP_NORTH_WEST:
        return (dir == ISODIR_SOUTH || dir == ISODIR_SOUTH_EAST ||
            dir == ISODIR_EAST) ? 0 : 1;
    case TILE_RAMP_NORTH_EAST:
        return (dir == ISODIR_SOUTH || dir == ISODIR_SOUTH_WEST ||
            dir == ISODIR_WEST) ? 0 : 1;
    default:
        return 1;
    };
}

int
tile_ramp_is_diagonal(enum tile_ramp ramp)
{
    switch (ramp)
    {
    case TILE_RAMP_NORTH_EAST:
    case TILE_RAMP_NORTH_WEST:
    case TILE_RAMP_SOUTH_EAST:
    case TILE_RAMP_SOUTH_WEST:
        return 1;
    default:
        return 0;
    }
}

tile_t
tile_def_get_id(tile_def_t *def)
    {return (tile_t)(def - _tile_defs);}

static int
_on_def(parse_def_file_context_t *ctx, const char *def, const char *val)
{
    const char *err_str = 0;
    if (!streq(def, "tile"))
        {err_str = "invalid def name (must be 'tile')"; goto fail;}
    if (!str_is_int(val))
        {err_str = "bad tile id (must be integer)"; goto fail;}
    int tile_id = atoi(val);
    if (tile_id <= 0 || tile_id >= 0xFFFF) /* Zero and 0xFFFF are reserved. */
        {err_str = "bad tile id (must be > 0 and < 65535)"; goto fail;}
    parse_context_t *context = ctx->user_data;
    context->tile_id            = tile_id;
    context->have_name          = 0;
    context->have_passthrough   = 0;
    context->have_ramp          = 0;
    context->have_clip          = 0;
    context->have_offset_x      = 0;
    context->have_offset_y      = 0;
    _tile_defs[tile_id].defined = 1;
    return 0;
    fail:
        printf("Error parsing tiles.def, line %zu: %s\n", ctx->line, err_str);
        return 1;
}

static int
_on_opt(parse_def_file_context_t *ctx, const char *opt, const char *val)
{
    const char *err_str;
    parse_context_t *context = ctx->user_data;
    tile_def_t *def = &_tile_defs[context->tile_id];
    if (streq(opt, "name"))
    {
        if (context->have_name)
            {err_str = "name defined more than once"; goto fail;}
        dstr_set(&def->name, val);
        context->have_name = 1;
    } else
    if (streq(opt, "passthrough"))
    {
        if (context->have_passthrough)
            {err_str = "passthrough defined more than once"; goto fail;}
        bool32 v;
        if (str_to_bool(val, &v))
            {err_str = "passthrough must be a boolean value"; goto fail;}
        def->passthrough = v;
        context->have_passthrough = 1;
    } else
    if (streq(opt, "ramp"))
    {
        if (context->have_ramp)
            {err_str = "ramp defined more than once"; goto fail;}
        enum tile_ramp ramp;
        if (streq(val, "none"))
            ramp = TILE_RAMP_NONE;
        else if (streq(val, "north"))
            ramp = TILE_RAMP_NORTH;
        else if (streq(val, "north_east"))
            ramp = TILE_RAMP_NORTH_EAST;
        else if (streq(val, "east"))
            ramp = TILE_RAMP_EAST;
        else if (streq(val, "south_east"))
            ramp = TILE_RAMP_SOUTH_EAST;
        else if (streq(val, "south"))
            ramp = TILE_RAMP_SOUTH;
        else if (streq(val, "south_west"))
            ramp = TILE_RAMP_SOUTH_WEST;
        else if (streq(val, "west"))
            ramp = TILE_RAMP_WEST;
        else if (streq(val, "north_west"))
            ramp = TILE_RAMP_NORTH_WEST;
        else
        {
            err_str = "invalid ramp value";
            goto fail;
        }
        def->ramp = ramp;
        context->have_ramp = 1;
   } else
   if (streq(opt, "clip"))
   {
        if (context->load_graphics)
        {
            if (context->have_clip)
               {err_str = "clip defined more than once"; goto fail;}
            int v[4];
            if (sscanf(val, "%d, %d, %d, %d", &v[0], &v[1], &v[2], &v[3]) != 4)
                {err_str = "clip must be in format '0, 0, 0, 0'"; goto fail;}
            for (int i = 0; i < 4; ++i)
                _tile_graphic_defs[context->tile_id].clip[i] = (float)v[i];
            context->have_clip = 1;
        }
   } else
   if (streq(opt, "offset_x"))
   {
       if (context->load_graphics)
       {
       if (context->have_offset_x)
           {err_str = "offset_x defined more than once"; goto fail;}
        _tile_graphic_defs[context->tile_id].ox = (float)atoi(val);
        context->have_offset_x = 1;
       }
   } else
   if (streq(opt, "offset_y"))
   {
       if (context->load_graphics)
       {
           if (context->have_offset_y)
               {err_str = "offset_y defined more than once"; goto fail;}
            _tile_graphic_defs[context->tile_id].oy = (float)atoi(val);
            context->have_offset_y = 1;
       }
   } else
        {err_str = "unrecognized parameter"; goto fail;}
    return 0;
    fail:
        printf("Error parsing tiles.def, line %zu: %s\n", ctx->line, err_str);
        return 1;
}
