#ifndef MUTA_SV_TIME_H
#define MUTA_SV_TIME_H
#include "types.h"
#include "ksys.h"
/* Server time utilities */

typedef struct perf_clock_t perf_clock_t;

extern sys_time_t program_start_time;

struct perf_clock_t
{
    uint64  last_tick;  /* Milliseconds */
    uint32  delta_ms;   /* Milliseconds */
    double  delta_s;    /* Seconds */
    uint64  target_fps; /* Milliseconds */
    uint64  time;       /* Milliseconds */
};

int
init_time();

void
perf_clock_init(perf_clock_t *pc, uint32 target_fps);

void
perf_clock_tick(perf_clock_t *pc);

static inline uint64
get_program_ticks_ms();

static inline uint64
get_program_ticks_ms()
{
    sys_time_t time_now;
    if (get_monotonic_time(&time_now))
        return 0;
    uint64 prog_start_msec = program_start_time.sec * 1000 + \
        program_start_time.msec;
    uint64 now_msec = time_now.sec * 1000 + time_now.msec;
    return now_msec - prog_start_msec;
}

#endif /* MUTA_SV_TIME_H */
