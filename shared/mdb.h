#ifndef MUTA_GENERIC_DB
#define MUTA_GENERIC_DB

#include "types.h"
#include <string.h>
#include <stdlib.h>
#include "containers.h"

typedef struct mdb_col_t    mdb_col_t;
typedef mdb_col_t           mdb_col_darr_t;
typedef uint32              mdb_row_t;
typedef struct mdb_t        mdb_t;

#define MDB_MAX_COL_NAME_LEN      32
#define MDB_MAX_NAME_LEN          32
#define MDB_HEADER_PAD_BYTES      16

enum mdb_col_type
{
    MDB_COL_INT8 = 0,
    MDB_COL_UINT8,
    MDB_COL_INT16,
    MDB_COL_UINT16,
    MDB_COL_INT32,
    MDB_COL_UINT32,
    MDB_COL_STR,
    NUM_MDB_COL_TYPES
};

struct mdb_col_t
{
    dchar   *name;
    uint8   type;
    uint8   is_key;
    uint32  offset;
};

struct mdb_t
{
    /* Layout:
     * name         :char[MDB_MAX_NAME_LEN]
     * num_cols     :uint32
     * num_rows     :uint32
     * running_id   :uint32
     * version      :uint32
     * padding      :char[MDB_HEADER_PAD_BYTES]
     * cols
     * rows
     * strings */

    /* Strings are saved like so:
     * On the row: uint32 represeting offset to the spring block
     * In the spring block: uint32 (len) -> string (not null-terminated) */
    char                name[MDB_MAX_NAME_LEN + 1];
    uint32              running_id;
    uint32              version;
    uint32              num_rows;
    /* Padding */
    mdb_col_darr_t      *cols;
    void                *rows;
};

int
mdb_save(mdb_t *db, const char *fp);

int
mdb_open(mdb_t *db, const char *fp);

int
mdb_new(mdb_t *db);

void
mdb_close(mdb_t *db);

int
mdb_set_name(mdb_t *db, const char *name);

int
mdb_new_col(mdb_t *db, const char *name, int type, bool32 is_key);

int
mdb_del_col(mdb_t *db, uint32 col_index);

int
mdb_new_row(mdb_t *db, uint32 *ret_id);

int
mdb_del_row(mdb_t *db, mdb_row_t row);

mdb_row_t
mdb_get_row(mdb_t *db, uint32 id);
/* Return value: 0 indicates failure.
 * Any modifications to the db will make the row unsuitable for further use */

mdb_row_t
mdb_get_row_by_index(mdb_t *db, int index);

#define mdb_num_rows(db) ((db)->num_rows)

#define mdb_num_cols(db) (darr_num((db)->cols))

void *
mdb_get_field(mdb_t *db, mdb_row_t row, uint32 col);

int
mdb_set_field(mdb_t *db, mdb_row_t row, uint32 col, void *val);
/* Val must not be 0 */

int
mdb_create_and_set_field_by_name_with_type(mdb_t *mdb, mdb_row_t row,
    const char *col_name, enum mdb_col_type col_type, bool32 col_is_key,
    void *val);
/* col_is_key is not checked except if a new row is created. */

int
mdb_get_col_index(mdb_t *db, const char *name); /* < 0 if col not found */

enum mdb_col_type
mdb_get_col_type(mdb_t *db, uint32 col);

int
mdb_get_col_index_with_type(mdb_t *db, enum mdb_col_type type,
    const char *name);

int
mdb_get_or_create_col_index(mdb_t *db, enum mdb_col_type type, const char *name,
    bool32 col_is_key);
/* Get index, but if col doesn't exist, it is automatically created first. If
 * the column exists but is of the wrong type, < 0 is returned and no new column
 * is created. */

const char *
mdb_col_type_to_str(int type);

#endif
