#include "abilities.h"
#include "hashtable.h"
#include "common_utils.h"

hashtable_define(ability_id_ability_def_table, ability_id_t, ability_def_t);

typedef struct ability_id_ability_def_table ability_id_ability_def_table_t;
typedef struct parse_context_t parse_context_t;

#define PRINT_ERR(str, ...) \
    printf("abilities.def line %zu: " str "\n", context->line, ##__VA_ARGS__)

struct parse_context_t
{
    bool32          have_def;
    ability_def_t   def;
};

static ability_id_ability_def_table_t _table;

static int
_on_def(parse_def_file_context_t *context, const char *def, const char *val);

static int
_on_opt(parse_def_file_context_t *context, const char *opt, const char *val);

static int
_finalize_existing(parse_context_t *ctx);

static void
_destroy_def(ability_def_t *def);

int
ab_load(void)
{
    ability_id_ability_def_table_einit(&_table, 256);
    parse_context_t ctx = {0};
    const char *path = "data/common/abilities.def";
    enum parse_def_file_result parse_result = parse_def_file(path, _on_def,
        _on_opt, &ctx);
    switch (parse_result)
    {
    case PARSE_DEF_FILE_SUCCESS:
        break;
    case PARSE_DEF_FILE_FOPEN_FAILED:
        printf("'%s' not found.\n", path);
        goto fail;
    case PARSE_DEF_FILE_SYNTAX_ERROR:
        printf("abilities.def: syntax error.\n");
        goto fail;
    }
    if (_finalize_existing(&ctx))
        goto fail;
    return 0;
    fail:;
        ab_destroy();
        return 1;
}

void
ab_destroy(void)
{
    uint32          id;
    ability_def_t   def;
    hashtable_for_each_pair(_table, id, def)
    {
        _destroy_def(&def);
    };
    ability_id_ability_def_table_destroy(&_table);
}

ability_def_t *
ab_get(ability_id_t id)
    {return ability_id_ability_def_table_find(&_table, id);}

static int
_on_def(parse_def_file_context_t *context, const char *def, const char *val)
{
    parse_context_t *ctx = context->user_data;
    if (_finalize_existing(ctx))
        return 1;
    if (!streq(def, "ability"))
    {
        PRINT_ERR("Unknown definition type '%s'", def);
        return 1;
    }
    if (!str_is_int(val))
    {
        PRINT_ERR("Ability ID is not an integer.");
        return 1;
    }
    if (val[0] == '-')
    {
        PRINT_ERR("Ability ID is negative.");
        return 1;
    }
    uint32 id = str_to_uint32(val);
    if (ability_id_ability_def_table_exists(&_table, id))
    {
        PRINT_ERR("ID %u defined more than once", id);
        return 1;
    }
    memset(&ctx->def, 0, sizeof(ctx->def));
    ctx->def.id = id;
    return 0;
}

static int
_on_opt(parse_def_file_context_t *context, const char *opt, const char *val)
{
    parse_context_t *ctx = context->user_data;
    if (streq(opt, "name"))
    {
        if (ctx->def.name)
            goto defined_twice;
        ctx->def.name = dstr_create(val);
    }
    else if (streq(opt, "icon_id"))
    {
        if (ctx->def.icon_id)
            goto defined_twice;
        ctx->def.icon_id = dstr_create(val);
    }
    else
    {
        PRINT_ERR("Unknown option '%s'", opt);
        return 1;
    }
    return 0;
    defined_twice:;
        PRINT_ERR("Option '%s' defined more than once.", opt);
        return 1;
}

static int
_finalize_existing(parse_context_t *ctx)
{
    if (!ctx->have_def)
    {
        ctx->have_def = 1;
        return 0;
    }
    const char *param_name;
    if (!ctx->def.name)
    {
        param_name = "name";
        goto missing_parameter;
    }
    if (!ctx->def.icon_id)
        ctx->def.icon_id = dstr_create("question_mark");
    ability_id_ability_def_table_einsert(&_table, ctx->def.id, ctx->def);
    return 0;
    missing_parameter:;
        printf("abilities.def: Missing option '%s' for definition of ability "
            "id %u.\n", param_name, ctx->def.id);
        _destroy_def(&ctx->def);
        return 1;
}

static void
_destroy_def(ability_def_t *def)
{
    dstr_free(&def->name);
    dstr_free(&def->icon_id);
}
