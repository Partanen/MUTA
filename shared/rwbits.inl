#ifndef RW_BITS_INL
#define RW_BITS_INL

#include "common_utils.h"
#include "crypt.h"

/* This macro defines an encrypted message prep function for the given message
 * type. Example:
 * MSG_WRITE_PREP_DEFINITION(dbmsg, DBMSGTSZ, WRITE_DBMSG_TYPE);
 * ...results in the function:
 * static inline int
 * prep_dbmsg_write_var_encrypted(byte_buf_t *buf, int type, int sz); */
#define MSG_WRITE_PREP_DEFINITION(msg_type_name, msg_type_sz, \
    msg_type_write_macro) \
static inline uint8 * \
prep_##msg_type_name##_write_var_encrypted(byte_buf_t *buf, int type, int sz) \
{ \
    int tot_sz = msg_type_sz + sizeof(msg_sz_t) + CRYPT_MSG_ADDITIONAL_BYTES + \
        sz; \
    uint8 *ret = bbuf_reserve(buf, tot_sz); \
    if (ret) \
    { \
        msg_type_write_macro(ret, type); \
        WRITE_MSG_SZ(ret, sz); \
    } \
    return ret; \
}

static inline int
prep_msg_read_var_encrypted(byte_buf_t *buf, int max_size,
    msg_sz_t *ret_sz)
{
    if (BBUF_FREE_SPACE(buf) < sizeof(msg_sz_t) + CRYPT_MSG_ADDITIONAL_BYTES)
        return 1;
    BBUF_READ(buf, ret_sz);
    if (*ret_sz > max_size) return -1;
    (*ret_sz) += CRYPT_MSG_ADDITIONAL_BYTES;
    if (*ret_sz > BBUF_FREE_SPACE(buf)) return 1;
    return 0;
}

 /* Data is always sent in little endian order */

/* On message read-write functions:
 * The functions return 0 on successful write. Otherwise, non-zero will be
 * returned, indicating the bytebuffer did not have enough space for the
 * message.
 * Note: read functions will also write their type id into the message, so the
 * offset of the buffer must take this in count (don't write it manually).
 * For write functions, the buffer offset should begin directly AFTER the type
 * (because to read a message, it's type must be know first)! */

#if MUTA_ENDIANNESS == MUTA_LIL_ENDIAN
    #define WRITE_INT32(mem, val) \
        *(int32*)(mem) = (int32)(val); \
        (mem) = (uint8*)(mem) + sizeof(int32);

    #define WRITE_UINT32(mem, val) \
        *(uint32*)(mem) = (uint32)(val); \
        (mem) = (uint8*)(mem) + sizeof(uint32);

    #define WRITE_INT16(mem, val) \
        *(int16*)(mem) = (int16)(val); \
        (mem) = (uint8*)(mem) + sizeof(int16);

    #define WRITE_UINT16(mem, val) \
        *(uint16*)(mem) = (uint16)(val); \
        (mem) = (uint8*)(mem) + sizeof(uint16);

    #define WRITE_UINT64(mem, val) \
        *(uint64*)(mem) = (uint64)(val); \
        (mem) = (uint8*)(mem) + sizeof(uint64);

    #define WRITE_MSG_SZ(mem, val) \
        *(msg_sz_t*)(mem) = (msg_sz_t)(val); \
        (mem) = (uint8*)(mem) + sizeof(msg_sz_t);

    #define READ_INT32(mem, val) \
        *(val) = *(int32*)(mem); \
        (mem) = (uint8*)(mem) + sizeof(int32);

    #define READ_UINT32(mem, val) \
        *(val) = *(uint32*)(mem); \
        (mem) = (uint8*)(mem) + sizeof(uint32);

    #define READ_INT16(mem, val) \
        *(val) = *(int16*)(mem); \
        (mem) = (uint8*)(mem) + sizeof(int16);

    #define READ_UINT16(mem, val) \
        *(val) = *(uint16*)(mem); \
        (mem) = (uint8*)(mem) + sizeof(uint16);

    #define READ_UINT64(mem, val) \
        *(val) = *(uint64*)(mem); \
        (mem) = (uint8*)(mem) + sizeof(uint64);

    #define READ_VAL(mem, type, ret_val) \
        *(ret_val) = *(type*)(mem); \
        (mem) += sizeof(type);

    #define READ_MSG_SZ(mem, val) \
        *(val) = *(msg_sz_t*)(mem); \
        (mem) = (uint8*)(mem) + sizeof(msg_sz_t);

    #define WRITE_UINT16_VARARR(mem, arr, len) \
        memcpy((uint16*)(mem), (arr), (size_t)(sizeof(uint16) * (len))); \
        mem = (uint8*)(mem) + (sizeof(uint16) * (len));

    #define WRITE_UINT32_VARARR(mem, arr, len) \
        memcpy((uint32*)(mem), (arr), (size_t)(sizeof(uint32) * (len))); \
        mem = (uint8*)(mem) + (sizeof(uint32) * (len));

    #define WRITE_UINT64_VARARR(mem, arr, len) \
        memcpy((uint64*)(mem), (arr), (size_t)(sizeof(uint64) * (len))); \
        mem = (uint8*)(mem) + (sizeof(uint64) * (len));

    #define READ_INT16_VARARR(mem, arr, len) \
        (arr) = (int16*)(mem); \
        (mem) = (uint8*)(mem) + (sizeof(int16) * (len));

    #define READ_UINT16_VARARR(mem, arr, len) \
        (arr) = (uint16*)(mem); \
        (mem) = (uint8*)(mem) + (sizeof(uint16) * (len));

    #define READ_INT32_VARARR(mem, arr, len) \
        (arr) = (int32*)(mem); \
        (mem) = (uint8*)(mem) + (sizeof(int32) * (len));

    #define READ_UINT32_VARARR(mem, arr, len) \
        (arr) = (uint32*)(mem); \
        (mem) = (uint8*)(mem) + (sizeof(uint32) * (len));

    #define READ_INT64_VARARR(mem, arr, len) \
        (arr) = (int64*)(mem); \
        (mem) = (uint8*)(mem) + (sizeof(int64) * (len));

    #define READ_UINT64_VARARR(mem, arr, len) \
        (arr) = (uint64*)(mem); \
        (mem) = (uint8*)(mem) + (sizeof(uint64) * (len));

#elif MUTA_ENDIANNESS == MUTA_BIG_ENDIAN
#   error "Big endian unsupported"
#else
#   error "Endianness undefined"
#endif /* MUTA_LIL_ENDIAN */

#define WRITE_INT8(mem, val) \
    *(int8*)(mem) = (int8)(val); \
    (mem) = (uint8*)(mem) + sizeof(uint8);

#define WRITE_UINT8(mem, val) \
    *(uint8*)(mem) = (uint8)(val); \
    (mem) = (uint8*)(mem) + sizeof(uint8);

#define WRITE_STR(mem, str, len) \
    memcpy((char*)(mem), (str), (size_t)(len)); \
    mem = (uint8*)mem + (len);

#define WRITE_UINT8_FIXARR(mem, str, len) \
    memcpy((char*)(mem), (str), (size_t)(len)); \
    mem = (uint8*)mem + (len);

#define WRITE_UINT8_VARARR(mem, arr, len) \
    memcpy((uint8*)(mem), (arr), (size_t)(len)); \
    mem = (uint8*)(mem) + (sizeof(uint8) * (len));

#define WRITE_INT8_VARARR(mem, arr, len) \
    memcpy((int8*)(mem), (arr), (size_t)(len)); \
    mem = (uint8*)(mem) + (sizeof(int8) * (len));

#define WRITE_INT16_VARARR(mem, arr, len) \
    memcpy((int16*)(mem), (arr), (size_t)(sizeof(int16) * (len))); \
    mem = (uint8*)(mem) + (sizeof(int16) * (len));

#define WRITE_INT32_VARARR(mem, arr, len) \
    memcpy((int32*)(mem), (arr), (size_t)(sizeof(int32) * (len))); \
    mem = (uint8*)(mem) + (sizeof(int32) * (len));

#define WRITE_INT64_VARARR(mem, arr, len) \
    memcpy((int64*)(mem), (arr), (size_t)(sizeof(int64) * (len))); \
    mem = (uint8*)(mem) + (sizeof(int64) * (len));

#define READ_INT8(mem, val) \
    *(val) = *(int8*)(mem); \
    (mem) = (uint8*)(mem) + sizeof(int8);

#define READ_UINT8(mem, val) \
    *(val) = *(uint8*)(mem); \
    (mem) = (uint8*)(mem) + sizeof(uint8);

#define READ_STR(mem, str, len) \
    memcpy((str), (mem), (size_t)(len)); \
    (mem) = (uint8*)(mem) + (len);

#define READ_UINT8_FIXARR(mem, str, len) \
    memcpy((str), (mem), (size_t)(len)); \
    (mem) = (uint8*)(mem) + (len);

#define READ_CHAR_PTR(mem, str, len) \
    (str) = (char*)(mem); \
    (mem) = (uint8*)(mem) + (len);

#define READ_VARCHAR(mem, str, len) \
    (str) = (char*)(mem); \
    (mem) = (uint8*)(mem) + (len);

#define READ_UINT8_VARARR(mem, arr, len) \
    (arr) = (uint8*)(mem); \
    (mem) = (uint8*)(mem) + (sizeof(uint8) * (len));

#define READ_INT8_VARARR(mem, arr, len) \
    (arr) = (int8*)(mem); \
    (mem) = (uint8*)(mem) + (sizeof(int8) * (len));

#endif /* RW_BITS_INL */
