#ifndef MUTA_GENERIC_CONTAINERS_H
#define MUTA_GENERIC_CONTAINERS_H

#include "common_utils.h" /* For muta_panic() */

/* An object pool that will not grow nor shrink dynamically. type requires the
 * _t postfix, pool_name must not have it. */

#define STATIC_OBJ_POOL_DEFINITION(type, pool_name) \
typedef struct pool_name##_t pool_name##_t; \
struct pool_name##_t \
{ \
    type    *items, *res, *free; \
    int     max; \
}; \
\
static inline int \
pool_name##_init(pool_name##_t *p, type *mem, int max); \
\
static inline void \
pool_name##_clear(pool_name##_t *p); \
\
static inline int \
pool_name##_init(pool_name##_t *p, type *mem, int max) \
{ \
    if (max <= 0)   return 1; \
    if (!mem)       return 2; \
    for (int i = 0; i < max - 1; ++i) \
        mem[i].next = &mem[i] + 1; \
    mem[max - 1].next   = 0; \
    p->items            = mem; \
    p->free             = mem; \
    p->res              = 0; \
    p->max              = max; \
    return 0; \
} \
\
static inline void \
pool_name##_clear(pool_name##_t *p) \
{ \
    int num     = p->max - 1; \
    type *mem   = p->items; \
    for (int i = 0; i < num; ++i) \
        mem[i].next = &mem[i] + 1; \
    mem[num].next   = 0; \
    p->res          = 0; \
} \
\
static inline type * \
pool_name##_reserve(pool_name##_t *p) \
{ \
    type *ret = p->free; \
    if (!ret) return 0; \
    p->free     = ret->next; \
    ret->next   = p->res; \
    p->res      = ret; \
    return ret; \
} \
\
static inline void \
pool_name##_free(pool_name##_t *p, type *item) \
{ \
    if (!item) return; \
    for (type **oi = &p->res; *oi; oi = &(*oi)->next) \
    { \
        if (*oi != item) continue; \
        *oi = item->next; \
        break; \
    } \
    item->next  = p->free; \
    p->free     = item; \
}

/* A single-linked list of objects whose memory addresses will not change even
 * if the internal array is resized.
 * The type contained must have a "next" pointer as a member.
 * new_arr_sz is the number of new objects allocated if space is run out of */
#define DYNAMIC_OBJ_POOL_DEFINITION(type, pool_name, new_arr_sz) \
\
typedef struct pool_name##_t pool_name##_t; \
\
struct pool_name##_t \
{ \
    type    **item_arrs; \
    int     num_item_arrs; \
    int     max_item_arrs; \
    int     first_arr_sz; \
    type    *free, *res; \
}; \
\
static inline int \
pool_name##_init(pool_name##_t *p, int num, int num_arrs); \
\
static inline int \
pool_name##_init(pool_name##_t *p, int num, int num_arrs); \
\
static inline void \
pool_name##_destroy(pool_name##_t *p); \
\
static inline void \
pool_name##_clear(pool_name##_t *p); \
\
static inline type * \
pool_name##_first(pool_name##_t *p) \
    {return p->res;} \
\
static inline int \
pool_name##_init(pool_name##_t *p, int num, int num_arrs) \
{ \
    if (num <= 0)       return 1; \
    if (num_arrs <= 0)  return 2; \
\
    pool_name##_t tmp = {0}; \
\
    tmp.item_arrs = calloc(num_arrs, sizeof(type*)); \
    if (!tmp.item_arrs) return 3; \
\
    int ret = 0; \
\
    tmp.max_item_arrs   = num_arrs; \
    tmp.item_arrs[0]    = malloc(num * sizeof(type)); \
    tmp.first_arr_sz    = num; \
\
    if (!tmp.item_arrs[0]) \
    { \
        ret = 4; \
        goto cleanup; \
    } \
\
    tmp.num_item_arrs   = 1; \
    type *arr           = tmp.item_arrs[0]; \
\
    for (int i = 0; i < num - 1; ++i) \
        arr[i].next = &arr[i] + 1; \
\
    arr[num - 1].next   = 0; \
    tmp.free            = arr; \
\
    cleanup: \
\
    if (ret == 0) \
        *p = tmp; \
    else \
        pool_name##_destroy(&tmp); \
\
    return ret; \
} \
\
static inline void \
pool_name##_einit(pool_name##_t *p, int num, int num_arrs) \
    {if (pool_name##_init(p, num, num_arrs)) muta_panic_print(__func__);} \
\
static inline void \
pool_name##_destroy(pool_name##_t *p) \
{ \
    for (int i = 0; i < p->num_item_arrs; ++i) \
        free(p->item_arrs[i]); \
    free(p->item_arrs); \
    memset(p, 0, sizeof(pool_name##_t)); \
} \
\
static inline void \
pool_name##_clear(pool_name##_t *p) \
{ \
    int i, j, num_items; \
    type *arr, *last; \
    type **last_ptr = &p->free; \
 \
    for (i = 0; i < p->num_item_arrs; ++i) \
    { \
        arr         = p->item_arrs[i]; \
        num_items   = i == 0 ? p->first_arr_sz : new_arr_sz; \
 \
        for (j = 0; j < num_items; ++j) \
            arr[j].next = &arr[j] + 1; \
 \
        last        = &arr[num_items - 1]; \
        last->next  = 0; \
        *last_ptr   = arr; \
        last_ptr    = &last->next; \
    } \
    p->res = 0; \
} \
\
static inline type * \
pool_name##_reserve(pool_name##_t *p) \
{ \
    /* Allocate more */ \
    if (!p->free) \
    { \
        if (p->num_item_arrs == p->max_item_arrs) \
        { \
            int max_arrs = p->max_item_arrs + 4; \
            type **arrs = realloc(p->item_arrs, max_arrs * sizeof(type*)); \
            if (!arrs) return 0; \
\
            p->item_arrs        = arrs; \
            p->max_item_arrs    = max_arrs; \
        } \
\
        type *arr = malloc(new_arr_sz * sizeof(type)); \
        if (!arr) return 0; \
\
        p->item_arrs[p->num_item_arrs++] = arr; \
\
        for (int i = 0; i < new_arr_sz; ++i) \
            arr[i].next = &arr[i] + 1; \
\
        arr[new_arr_sz - 1].next    = 0; \
        p->free                     = arr; \
    } \
\
    type *ret   = p->free; \
    p->free     = ret->next; \
    ret->next   = p->res; \
    p->res      = ret; \
    return ret; \
} \
\
static inline type * \
pool_name##_ereserve(pool_name##_t *p) \
{ \
    type *ret = pool_name##_reserve(p); \
    if (!ret) muta_panic_print(__func__); \
    return ret; \
} \
\
static inline void \
pool_name##_free(pool_name##_t *p, type *item) \
{ \
    if (!item) return; \
    for (type **oi = &p->res; *oi; oi = &(*oi)->next) \
    { \
        if (*oi != item) continue; \
        *oi = item->next; \
        break; \
    } \
    item->next  = p->free; \
    p->free     = item; \
}

#endif /* MUTA_GENERIC_CONTAINERS_H */
