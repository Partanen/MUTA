#ifndef MUTA_SHARED_TILE_H
#define MUTA_SHARED_TILE_H

#include "types.h"
#include "common_defs.h"

#define TILE_MAX_TYPES          (0xFFFF + 1)
#define TILE_INVISIBLE_COLLIDER 0xFFFF

typedef struct tile_def_t           tile_def_t;
typedef struct tile_graphic_def_t   tile_graphic_def_t;

enum tile_ramp
{
    TILE_RAMP_NONE = 0,
    TILE_RAMP_NORTH,
    TILE_RAMP_EAST,
    TILE_RAMP_SOUTH,
    TILE_RAMP_WEST,
    TILE_RAMP_NORTH_EAST,
    TILE_RAMP_NORTH_WEST,
    TILE_RAMP_SOUTH_EAST,
    TILE_RAMP_SOUTH_WEST,
    NUM_TILE_RAMPS
};
/* Implies towards which direction a ramp goes up. */

struct tile_def_t
{
    bool32          defined;
    dchar           *name;
    bool32          passthrough;
    enum tile_ramp  ramp;
};
/* A definition of a single tile type. */

struct tile_graphic_def_t
{
    float   clip[4];
    float   ox, oy; /* Pixel offsets */
};
/* A definition of a the graphical representation of a single tile. All tile
 * graphics come from the same texture. */

int
tile_load(bool32 load_graphics);

void
tile_destroy(void);

uint32
tile_num_defs(void);

tile_def_t *
tile_get_def(tile_t tile);
/* tile_get_def()
 * Get the definition of a tile. */

tile_graphic_def_t *
tile_get_graphic_def(tile_t tile);
/* tile_get_graphic_def()
 * Get the definition of a tile's graphic. The caller must make sure the
 * parameter tile is < tile_num_defs(). */

int
tile_check_ramp_up(int dir, enum tile_ramp ramp);
/* Returns 0 if we can step on the ramp from the given direction. */

int
tile_check_ramp_down(int dir, enum tile_ramp ramp);

int
tile_ramp_is_diagonal(enum tile_ramp ramp);

tile_t
tile_def_get_id(tile_def_t *def);

#endif /* MUTA_SHARED_TILE_H */
