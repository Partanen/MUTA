/* svchan_client.h
 * Part of the server-to-server communication API. The client side API here is
 * the one who connects to the server (svchan_server.h). */

#ifndef MUTA_SHARED_SVCHAN_CLIENT_H
#define MUTA_SHARED_SVCHAN_CLIENT_H

#include "common_utils.h"
#include "crypt.h"
#include "net.h"

typedef struct svchan_client_callbacks_t    svchan_client_callbacks_t;
typedef struct svchan_client_buf_t          svchan_client_buf_t;
typedef struct svchan_client_event_t        svchan_client_event_t;
typedef struct svchan_client_t              svchan_client_t;

enum svchan_client_connection_error
{
    SVCHAN_CLIENT_ERROR_CONNECTION_REFUSED,
    SVCHAN_CLIENT_ERROR_SERVER_CLOSED_CONNECTION,
    SVCHAN_CLIENT_ERROR_BAD_USERNAME_OR_PASSWORD
};

struct svchan_client_callbacks_t
{
    void (*post_event)(svchan_client_event_t *event);
    /* post_event()
     * This callback should write the event it receives somewhere. This function
     * is called on the client's internal thread, so concurrency must be kept in
     * mind. Typically this will call event_push() (from common_utils.h)
     * internally. */

    void (*on_authed)(svchan_client_t *client);
    /* on_authed()
     * Called when client is successfully authenticated by the server. Before
     * this callback is called, it is not valid to send anything to the client.
     * This function is called from within svchan_handle_event(). */

    int (*on_read_packet)(svchan_client_t *client, uint8 *memory,
        int num_bytes);
    /* on_read_packet()
     * Called when data arrives from the server. Return value should be < 0 if
     * we should disconnect, or the number of unread bytes left in the buffer
     * after reading on success.
     * num_bytes will never be <= 0.
     * This function is called from within svchan_handle_event().
     * NOTE: never call svchan_client_disconnect() from within this function.
     * Rather, return an error code. */


    void (*on_disconnect)(svchan_client_t *client);
    /* on_disconnect()
     * Called when client is disconnected from the server. This function from
     * within svchan_handle_event(). */

    void (*on_connection_failed)(svchan_client_t *client,
        enum svchan_client_connection_error reason);
    /* on_connection_failed()
     * Called if the connection process failed. The reason is passed as a
     * parameter. */

    void (*send_still_here_msg)(svchan_client_t *client);
    /* send_still_here_msg()
     * Every n seconds, if no messages have been sent, a still here message must
     * be sent to the svchan_server. That's what this function should do. */

    void (*on_connected)(svchan_client_t *client);
    /* on_connected()
     * Called when a connection with the server is first established. The
     * authentication process begins after this.
     * This callback only needs to be provided if the client is ran in non-async
     * mode (but it is still always called if provided). In non-async mode, when
     * this function has been called, the client's socket should be registered
     * with a polling instance by the user. */
};

struct svchan_client_buf_t
{
    uint8   *memory;
    uint32  num;
    uint32  max;
};

struct svchan_client_event_t
{
    int             type;
    svchan_client_t *client;
    struct
    {
        uint8   *memory;
        int     num_bytes;
    } read;
};

struct svchan_client_t
{
    int32                       running;
    bool32                      async;
    int                         state;
    addr_t                      address;
    socket_t                    socket;
    thread_t                    thread;
    uint64                      last_sent_something;
    svchan_client_buf_t         in_buf;
    svchan_client_buf_t         out_buf;
    segpool_t                   segpool;
    mutex_t                     segpool_mutex; /* Only if async */
    svchan_client_callbacks_t   callbacks;
    dchar                       *name;
    dchar                       *password;
    void                        *user_data; /* Set this to identify a client */
    cryptchan_t                 cryptchan;
};

int
svchan_client_init(svchan_client_t *client,
    svchan_client_callbacks_t *callbacks, void *user_data, uint32 in_buf_size,
    uint32 out_buf_size, bool32 async);
/* svchan_client_init()
 * Initialize the client.
 * The async option signifies whether or not the client will create an internal
 * thread to read back from the server. If async is 0, the user must poll the
 * client's socket manually and call svchan_client_read() or
 * svchan_client_read_and_handle() when the socket is returned from
 * poll/epoll/netpoll().
 * The async option does not concern connecting, which is always asynchronous.
 * */

void
svchan_client_destroy(svchan_client_t *client);

int
svchan_client_connect(svchan_client_t *client, addr_t *address,
    const char *username, const char *password);
/* svchan_client_connect()
 * Connect to the server at the given address using the given username and
 * password. If an error occurs immediately, the return value is non-zero. If an
 * error occurs later while the connection is still being established (before
 * successful authentication), on_connection_failed() is called. */

void
svchan_client_disconnect(svchan_client_t *client);
/* svchan_client_disconnect()
 * Disconnect client from server. Never call from inside the on_read_packet()
 * callback! */

bool32
svchan_client_is_connecting(svchan_client_t *client);

bool32
svchan_client_is_connected(svchan_client_t *client);

void
svchan_client_handle_event(svchan_client_event_t *event);

void
svchan_client_flush(svchan_client_t *client);

bbuf_t
svchan_client_send(svchan_client_t *client, uint32 num_bytes);
/* svchan_client_send()
 * Send a message to the server. May only be called if client has been
 * authenticated.
 * NOTE: num_bytes MUST include the size of the message enumerator type. */

bbuf_t
svchan_client_send_const_encrypted(svchan_client_t *client, uint32 num_bytes);

bbuf_t
svchan_client_send_var_encrypted(svchan_client_t *client, uint32 num_bytes);

const char *
svchan_client_error_str(enum svchan_client_connection_error error);

void
svchan_client_read(svchan_client_t *client);
/* svchan_client_read()
 * Read incoming data from the client's socket and create an event for it. This
 * function can be called from another thread than the thread where the message
 * handling happens.
 * Only called if the client is ran in non-async mode. */

void
svchan_client_read_and_handle(svchan_client_t *client);
/* svchan_client_read_and_handle()
 * Read incoming data from the client's socket, but rather than creating an
 * event for it, handle the message immediately on the same thread.
 * Only called if the client is ran in non-async mode. */

#endif /* MUTA_SHARED_SVCHAN_CLIENT_H */
