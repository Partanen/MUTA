#ifndef MUTA_SHARED_ABILITIES_H
#define MUTA_SHARED_ABILITIES_H

#include "types.h"

typedef struct ability_def_t ability_def_t;

struct ability_def_t
{
    ability_id_t    id;
    dchar           *name;
    dchar           *icon_id;
};

int
ab_load(void);

void
ab_destroy(void);

ability_def_t *
ab_get(ability_id_t id);

#endif /* MUTA_SHARED_ABILITIES_H */
