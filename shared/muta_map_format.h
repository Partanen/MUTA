#ifndef MUTA_MAP_FORMAT_H
#define MUTA_MAP_FORMAT_H

#include "types.h"
#include "common_defs.h"

#define MUTA_CHUNK_FILE_NAME_LEN    32
#define MUTA_MAP_FILE_NAME_LEN      32

typedef struct muta_map_file_t      muta_map_file_t;
typedef muta_map_file_t             muta_map_file_darr_t;
typedef struct stored_static_obj_t  stored_static_obj_t;
typedef struct muta_chunk_file_t    muta_chunk_file_t;
typedef struct muta_map_db_entry_t  muta_map_db_entry_t;
typedef struct muta_map_db_t        muta_map_db_t;

/* This structure defines the macro structure of the map with a list of chunk
 * paths. Width and height are presented in chunks. */
struct muta_map_file_t
{
    struct
    {
        char    name[MUTA_MAP_FILE_NAME_LEN + 1];
        uint32  id;
        uint32  version;
        uint32  game_version[3];
        uint32  w, h; /* In chunks */
        /* 64 bytes of padding */
    } header;
    dchar **relative_chunk_paths;
    dchar **absolute_chunk_paths;
};

struct stored_static_obj_t
{
    sobj_type_id_t    type_id;
    uint8                   dir;
    uint8                   x;
    uint8                   y;
    uint8                   z;
};

/* A single map chunk, referenced by a muta_map_file_t in it's list of chunks */
struct muta_chunk_file_t
{
    struct
    {
        char    name[MUTA_CHUNK_FILE_NAME_LEN + 1];
        uint32  location[2]; /* In chunks */
        uint32  num_static_objs;
        /* 64 bytes of padding */
    } header;
    tile_t              *tiles;
    stored_static_obj_t *static_objs;
    uint32              max_static_objs;
};

struct muta_map_db_entry_t
{
    map_id_t    id;
    dchar       *name;
    dchar       *file_path;
    dchar       *directory_path;
};

struct muta_map_db_t
{
    muta_map_db_entry_t *entries;
};

int
muta_map_file_init_for_edit(muta_map_file_t *mf, const char *name,
    uint32 version, uint32 w, uint32 h);

int
muta_map_file_load(muta_map_file_t *mf, const char *path);
/* May not be called on an already loaded or initialized file. Destroy first. */

int
muta_map_file_save(muta_map_file_t *mf, const char *path);

void
muta_map_file_destroy(muta_map_file_t *mf);

int
muta_map_file_set_name(muta_map_file_t *mf, const char *name);

int
muta_map_file_set_chunk_path(muta_map_file_t *mf, uint32 x, uint32 y,
    const char *path);

char *
muta_map_file_get_chunk_relative_path(muta_map_file_t *mf, uint32 x, uint32 y);
/* Relative to location of the .map file. */

char *
muta_map_file_get_chunk_absolute_path(muta_map_file_t *mf, uint32 x, uint32 y);

char *
muta_map_file_get_chunk_name(muta_map_file_t *mf, uint32 x, uint32 y);

int
muta_map_file_set_chunk_path_pattern(muta_map_file_t *mf,
    const char *directory_path, const char *pattern);
/* All chunk paths will be set to [pat]x_y.dat where x and y are the position
 * of the chunk. Directory path is required for absolute paths. */

#define muta_map_file_tw(f) ((f)->header.w * MAP_CHUNK_W)
#define muta_map_file_th(f) ((f)->header.h * MAP_CHUNK_W)

int
muta_chunk_file_init(muta_chunk_file_t *mch);

void
muta_chunk_file_destroy(muta_chunk_file_t *mch);

int
muta_chunk_file_load(muta_chunk_file_t *mch, const char *path);
/* It's safe to call this on an already loaded chunk - the memory will be
 * re-used. */

int
muta_chunk_file_check(muta_chunk_file_t *mch, uint32 num_tile_types);
/* muta_chunk_file_check()
 * Check that the file contains only tile ids < num_tile_types. */

int
muta_chunk_file_save(muta_chunk_file_t *mch, const char *dir_path);
/* A name for the file will be autogenerated */

int
muta_chunk_file_set_name(muta_chunk_file_t *mch, const char *name);

void
muta_chunk_file_clear_static_objects(muta_chunk_file_t *mch);

int
muta_chunk_file_push_static_obj(muta_chunk_file_t *mch,
    sobj_type_id_t type_id, uint8 dir, uint8 x, uint8 y, int8 z);

void
muta_chunk_file_erase_static_obj(muta_chunk_file_t * mch, uint32 index);

int
muta_map_db_load(muta_map_db_t *db, const char *fp);

int
muta_map_db_save(muta_map_db_t *db, const char *fp);

void
muta_map_db_destroy(muta_map_db_t *db);

uint32
muta_map_db_num_entries(muta_map_db_t *db);

muta_map_db_entry_t *
muta_map_db_get_entry_by_name(muta_map_db_t *db, const char *name);

muta_map_db_entry_t *
muta_map_db_get_entry_by_id(muta_map_db_t *db, uint32 id);

#endif /* MUTA_MAP_FORMAT_H */
