#include <math.h>
#include <ctype.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include "gui.h"
#define STB_RECT_PACK_IMPLEMENTATION
#include "stb_rect_pack.h"
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

/* =============================================================================
 * Dynamic array
 * ===========================================================================*/

typedef struct darr_head_t darr_head_t;

struct darr_head_t
{
    uint32_t num;
    uint32_t cap;
};

#define _darr_head(darr) ((darr_head_t*)(darr) - 1)
#define darr_head(darr) ((darr) ? _darr_head(darr) : 0)
#define darr_num(darr)  ((darr) ? ((darr_head_t*)(darr) - 1)->num : 0)
#define darr_cap(darr)  ((darr) ? ((darr_head_t*)(darr) - 1)->cap : 0)
#define darr_reserve(darr, cap_) \
    ((darr) = darr_ensure_growth_by((darr), \
        darr_num(darr) < (cap_) ? (cap_) - darr_num(darr) : 0, \
        sizeof(*(darr))))
#define darr_free(darr) (gui_free(darr_head(darr)), (darr) = 0)
#define darr_push(darr, val) \
    ((darr) = darr_ensure_growth_by((darr), 1, sizeof(*(darr))), \
    (darr)[darr_head(darr)->num++] = (val))
#define darr_push_empty(darr) \
    ((darr) = darr_ensure_growth_by((darr), 1, sizeof(*(darr))), \
    &(darr)[darr_head(darr)->num++])
#define darr_clear(darr) \
    ((darr) ? _darr_head(darr)->num = 0 : 0)
#define darr_erase_last(darr) \
    ((darr) ? (_darr_head(darr)->num ? _darr_head(darr)->num-- : 0) : 0)
#define darr_erase(darr, index) \
    (darr_sized_erase(darr, index, sizeof(*(darr))))

/* =============================================================================
 * GUI
 * ===========================================================================*/

#define GUI_MAX_WINS                512
#define GUI_MAX_GUIDES              128
#define GUI_FNV_32_SEED             ((uint32_t)0x811C9DC5)
#define GUI_FNV_32_PRIME            ((uint32_t)0x01000193)
#if defined(MUTA_GUI_DEBUG)
    #define gui_assert(cond_) \
        ((cond_) ? (void)0 : \
        ((void)fprintf(stderr, \
        "GUI assert: %s, %s, line %d\n", __FILE__, \
        __func__, __LINE__), (void)fflush(stdout), \
        gui.init_config.panic_callback("Assertion failed!")))
    #define gui_debug_puts(str) printf("[GUI DEBUG] %s: " str "\n", __func__)
    #define gui_debug_printf(str, ...) \
        printf("[GUI DEBUG] %s: " str, __func__, ##__VA_ARGS__)
#else
    #define gui_assert(condition) ((void)0)
    #define gui_debug_puts(str)         ((void)0)
    #define gui_debug_printf(str, ...)  ((void)0)
#endif
#define GUI_MOUSE_BUTTON_DOWN(b) (gui.input_state.mouse_buttons & (b))
#define GUI_MOUSE_BUTTON_DOWN_NOW(b) \
    (gui.input_state.mouse_buttons & (b) && \
    !(gui.last_input_state.mouse_buttons & (b)))
#define GUI_MOUSE_BUTTON_UP_NOW(b) \
    (gui.last_input_state.mouse_buttons & (b) && \
    !(gui.input_state.mouse_buttons & (b)))
#define gui_malloc  malloc
#define gui_calloc  calloc
#define gui_realloc realloc
#define gui_free    free

typedef struct next_gui_win_t       next_gui_win_t;
typedef struct next_gui_button_t    next_gui_button_t;
typedef struct gui_draw_con_t       gui_draw_con_t;
typedef struct gui_state_t          gui_state_t;
typedef struct gui_win_t            gui_win_t;
typedef struct gui_text_input_t     gui_text_input_t;
typedef struct gui_draw_cmd_t       gui_draw_cmd_darr_t;
typedef gui_text_input_t            gui_text_input_darr_t;

struct next_gui_win_t
{
    int         index;
    uint32_t    last_active;
    gui_bool_t  is_override; /* Was set via a call like gui_set_active_win()? */
};

struct next_gui_button_t
{
    uint32_t id;
    uint32_t parent_id;
    uint32_t last_active;
    uint32_t mouse_btn;
};

struct gui_draw_con_t
{
    gui_draw_cmd_darr_t *draw_cmds;
    float               *vert_floats; /* Dynamic array */
};

struct gui_win_t
{
    uint32_t                id;
    uint32_t                last_hovered;
    uint32_t                last_active;
    uint32_t                last_drawn;
    int                     dim[4]; /* Origin for this is always top left */
    /* Relative x and y as they were passed to the gui_begin_win command: */
    int                     relative_x;
    int                     relative_y;
    gui_win_state_style_t   *style;
    int                     child_list_index;
    int                     parent_index;
    gui_draw_con_t          draw_con;
    int                     scroll[2];
    float                   scroll_percentage[2]; // Computed at end of frame
    int                     content_wh[2];
    gui_bool_t              is_parent_of_act_win;
    uint8_t                 flags;
};

struct gui_text_input_t
{
    uint32_t    id;
    int         cursor_index;
    int         left_index;
};

struct gui_state_t
{
    gui_bool_t                  began_update;
    uint32_t                    frame_count;
    enum gui_origin             origin;
    gui_font_t                  *font;
    uint8_t                     color[4];
    gui_bool_t                  hovered_this_frame;
    gui_bool_t                  hovered_last_frame;

    /*-- Text input --*/
    gui_bool_t                  next_have_active_text_input;
    uint32_t                    active_text_input;
    gui_text_input_darr_t       *text_inputs;
    float                       text_input_cursor_timer;
    float                       text_input_cursor_frequency;
    gui_bool_t                  show_text_input_cursor;
    gui_bool_t                  text_input_enter_pressed;
    gui_bool_t                  text_input_made_inactive;
    gui_bool_t                  text_input_made_active_now;
    /* True if gui_set_active_text_input() is called or a text input field is
     * successfully mouse-selected. */

    /*-- Last states for different gui object types --*/
    int                         last_text_origin;
    int                         last_text_dimensions[4];
    int                         last_button_origin;
    int                         last_button_dimensions[4];
    int                         last_texture_origin;
    int                         last_texture_dimensions[4];
    int                         last_text_input_origin;
    int                         last_text_input_dimensions[4];

    char                        *fmt_buf; /* For temporary text formatting.  */
    int                         fmt_buf_size;

    gui_win_t                   *wins;
    int                         win_num;
    int                         cur_win;
    int                         *wins_open_parents;
    int                         wins_open_parents_num;
    int                         wins_open_num;
    int                         *win_stack;
    int                         win_stack_num;

    int                         *guides; /* Array of int arrays of 4 slots */
    int                         guides_num;

    int                         **win_child_lists; /* Dynamic array */

    /*-- Active window --*/
    uint32_t                    active_win_id;
    uint32_t                    hovered_win_id;
    uint32_t                    current_win_id;

    next_gui_win_t              next_act_win;
    next_gui_win_t              next_hov_win;

    /*-- Buttons --*/
    next_gui_button_t           next_prs_btn;
    next_gui_button_t           next_hov_btn;
    uint32_t                    hovered_button;
    uint32_t                    last_drawn_button_id;
    uint32_t                    pressed_button_id, pressed_button_pid;
    uint32_t                    pressed_button_mbtn;
    uint32_t                    released_button_id;

    /*-- Input state --*/
    gui_input_state_t           input_state;
    gui_input_state_t           last_input_state;

    uint32_t                    mbtns_down_now;
    uint32_t                    mbtns_up_now;

    int                         clicked_this_frame;
    int                         released_this_frame;

    /* pressed_element_button_flags
     * Bitmask of the mouse buttons which have been used to press some gui
     * element */
    uint32_t                    pressed_element_button_flags;

    /* Draw lists
     * There's always an equal maximum number of draw lists to the number of
     * windows */
    gui_draw_list_t             *draw_lists;
    int                         num_draw_lists;

    gui_button_style_t          *button_style;
    gui_win_style_t             *win_style;
    gui_text_input_style_t      *text_input_style;
    gui_progress_bar_style_t    *progress_bar_style;
    gui_font_t                  default_font;
    gui_win_style_t             default_win_style;
    gui_button_style_t          default_button_style;
    gui_text_input_style_t      default_text_input_style;
    gui_progress_bar_style_t    default_progress_bar_style;

    gui_init_config_t           init_config;
};

static gui_win_t        _wins[GUI_MAX_WINS];
static int              _guides[GUI_MAX_GUIDES * 4];
static int              _win_stack[GUI_MAX_WINS];
static int              _wins_open_parents[GUI_MAX_WINS];
static gui_draw_list_t  _draw_lists[GUI_MAX_WINS];
static gui_state_t      gui;
static uint8_t          gui_color_white[4]  = {255, 255, 255, 255};
static const char       *_gui_base_win_name = "__GUI_BASE_WIN__";
static uint32_t         _gui_base_win_id;
static uint8_t          _gui_munro_ttf[28168];

static void *
darr_ensure_growth_by(void *darr, uint32_t grow_by, uint32_t item_sz);

static inline void
darr_sized_erase(void *darr, uint32_t index, uint32_t item_sz);

static inline uint32_t
_gui_fnv_hash32_from_str(const char *str);

static void
_gui_panic(const char *error, ...);

static uint32_t
_gui_graphic_button(uint32_t id, const char *title, int x, int y, int w, int h,
    uint32_t mbtn_mask);

static gui_win_t *
_gui_get_win_by_id(uint32_t id);

static gui_win_t *
_gui_create_win(uint32_t id);

static void
_gui_compute_quad_parented_xy(int *ret_x, int *ret_y, enum gui_origin o,
    int parent_dims[4], int x,  int y,  int w,  int h);

static gui_win_t *
_gui_begin_win_internal(const char *title, int x, int y, int w, int h,
    int flags);

static void
gui_end_win_internal(void);

static int
_gui_win_draw_quad(gui_win_t *win, int x, int y, int w, int h, void *tex,
    float *clip, uint8_t *color, enum gui_flip flip, float rot, int scissor[4]);

static int
_gui_win_draw_quad_f(gui_win_t *win, float x, float y, float w, float h, void *tex,
    float *clip, uint8_t *color, enum gui_flip flip, float rot,
    int scissor[4]);

static int
_gui_draw_con_draw_text(gui_draw_con_t *con, const char *txt, gui_font_t *font,
    float x, float y, int *parent_dim, int origin, int wrap,
    float sx, float sy, uint8_t *col, int *ret_rect, int scissor[4]);

static int
_gui_draw_con_draw_title_text(gui_draw_con_t *con, const char *text,
    gui_font_t *font, int x, int y, int parent_dim[4], int origin, float sx,
    float sy, uint8_t col[4], int scissor[4]);

static int
_gui_draw_con_draw_input_text(gui_draw_con_t *con, gui_text_input_t *ti,
    const char *txt, gui_bool_t is_active, gui_font_t *font, float offset_x,
    float offset_y, int *parent_dim, float sx, float sy, uint8_t *col,
    int scissor[4], int flags);

static void
_gui_write_quad_verts(float *verts, void *tex, float *clip, float x, float y,
    float w, float h, uint8_t *col, enum gui_flip flip);

/* Note: assumes children actually exist */
static void
_gui_sort_win_children(gui_win_t *win);

static void
_gui_update_and_add_win_to_draw_list(gui_win_t *win, gui_win_t *all_wins,
    gui_draw_list_t *lists, int *num_lists);

static inline gui_bool_t
_gui_test_next_active_win_parenthood(void);

static inline gui_bool_t
_gui_test_next_hovered_win_parenthood(void);

static int
_gui_draw_con_init(gui_draw_con_t *con, uint32_t num_cmds);

static inline void
_gui_draw_con_clear(gui_draw_con_t *con);

static float *
_gui_draw_con_push_verts(gui_draw_con_t *con, int num_verts);

static float *
_gui_draw_con_reserve_verts(gui_draw_con_t *con, int num_verts);

static void
_gui_draw_con_push_reserved_verts(gui_draw_con_t *con, int num_verts);

static int
_gui_draw_con_push_draw_cmd(gui_draw_con_t *con, int num_verts,
    void *tex, int scissor[4]);

static int
_gui_compute_title_text_line_w(const char *txt, gui_font_t *f, float x_scale);
/* Ignores double ## as they mark the ending of a visible title */

static void
_gui_compute_text_dimensions(const char *txt, gui_font_t *f, float wrap, float sx,
    float sy, int *ret_w, int *ret_h);

static inline int
_gui_strlen(const char *txt);
/* For calculating the number of quads required for a drawable string */

static inline int
_gui_title_strlen(const char *txt);
/* For calculating the number of quads required for a drawable title string */

static int
_gui_realloc_fmt_buf(int new_size);

static uint32_t
_gui_button_internal(uint32_t id, int x, int y, int w, int h,
    uint32_t mbtn_mask, int *ret_tx, int *ret_ty, gui_win_t **ret_parent,
    gui_bool_t save_last_button_dimensions);

static int
_gui_claim_win_child_list(void);
/* Returns < 0 on failure */

static inline void
_gui_get_win_inner_scissor(gui_win_t *win, int scissor[4]);

static gui_text_input_t *
_gui_get_text_input_by_id(uint32_t id);

static gui_text_input_t *
_gui_create_text_input(uint32_t id);

static int
_gui_find_cursor_right_jump_index(char *buf, int cursor_index);

static int
_gui_find_cursor_left_jump_index(char *buf, int cursor_index);

static void
_gui_draw_con_draw_tex_border(gui_draw_con_t *con, void *tex,
    float clips[8][4], uint8_t *color, int *rect, int *scissor);

static inline void
_gui_write_tex_quad_verts(float *_verts, void *tex, float *clip, float x,
    float y, float w, float h, uint8_t *color);

static void
_gui_write_flipped_tex_quad_verts(float *verts, void *tex, float *clip, float x,
    float y, float w, float h, uint8_t *color, int flip);

static inline void
_gui_get_tex_w_h(void *tex, float *ret_w, float *ret_h);

static uint8_t *
_gui_load_file_to_buffer(const char *path, size_t *ret_len);

static inline int
_gui_get_win_index(gui_win_t *win);

static inline gui_win_t *
_gui_get_win_stack_top(void);

static inline int *
_gui_get_guide_stack_top(void);

static inline void
_gui_compute_win_viewport_rect(gui_win_t *win, int ret_arr[4]);

static inline int **
_gui_get_win_child_indices(gui_win_t *win);

static inline gui_bool_t
_gui_test_mouse_rect(int x, int y, int w, int h);

static inline void
_gui_compute_quad_position(int *ret_x, int *ret_y, int origin, int x, int y,
    int w, int h);

static inline void
_gui_rotate_quad(float *verts, float x, float y, float w, float h, float rot);

static inline gui_glyph_t *
_gui_font_get_glyph(gui_font_t *font, int index);

static inline gui_glyph_t *
_gui_get_glyph(gui_glyph_t *glyphs, int num_glyphs, int index);

static void
_gui_save_parent_content_wh(gui_win_t *parent, int x, int y, int w, int h,
    int origin);

static void
_gui_compute_rect_by_edge(enum gui_edge edge, int tx, int ty, int w, int h,
    int ret_rect[4]);

static int
_compute_win_viewport_x(gui_win_t *win);

static int
_compute_win_viewport_y(gui_win_t *win);

static int
_compute_win_viewport_w(gui_win_t *win);

static int
_compute_win_viewport_h(gui_win_t *win);

int
gui_font_load_from_file(gui_font_t *font, const char *path, int height)
{
    uint8_t *file = 0;
    file = _gui_load_file_to_buffer(path, 0);
    if (!file)
        return 1;
    int ret = 0;
    if (gui_font_load_from_memory(font, file, height))
        ret = 2;
    gui_free(file);
    return ret;
}

int
gui_font_load_from_memory(gui_font_t *font, uint8_t *file, int height)
{
    int h_oversample = 2;
    int v_oversample = 2;
    struct
    {
        stbtt_fontinfo      info;
        uint8_t             *bitmap;
        uint8_t             *file;
        int                 bitmap_w, bitmap_h;
        stbtt_packedchar    *glyphs;
        int                 num_glyphs; /* If info.numGlyphs < 256, this is 256 */
        int                 pixel_height;
        float               scale;
        int                 ascent; /* Ascent, descent and linegap are unscaled */
        int                 descent;/* (for the moment at least). To use, scale */
        int                 linegap;/* by ttf_data.scale first. */
    } ttf_data = {0};
    int                 ret                 = 0;
    uint8_t             *tmp_pixels         = 0;
    stbtt_packedchar    *char_data          = 0;
    uint8_t             *transformed_pixels = 0;
    gui_glyph_t         *glyphs             = 0;
    stbtt_fontinfo font_info;
    if (!stbtt_InitFont(&font_info, file, 0))
        {ret = 2; goto out;}
    int num_glyphs = font_info.numGlyphs < 256 ? 256 : font_info.numGlyphs;
    /*-- Allocate everything --*/
    uint32_t num_char_data_bytes = sizeof(stbtt_packedchar) * num_glyphs;
    char_data = gui_calloc(1, num_char_data_bytes);
    if (!char_data)
        {ret = 3; goto out;}
    int     bmp_w = 0;
    gui_bool_t  baked = 0;
    /* Attempt to brute force into a suitable bitmap */
    for (bmp_w = 256; bmp_w < 2048; bmp_w = bmp_w * 2)
    {
        uint8_t *old_tmp_pixels = tmp_pixels;
        tmp_pixels = gui_realloc(tmp_pixels, bmp_w * bmp_w);
        if (!tmp_pixels)
        {
            gui_free(old_tmp_pixels);
            ret = 5;
            goto out;
        }
        memset(tmp_pixels, 0, bmp_w * bmp_w);
        stbtt_pack_context context;
        if (!stbtt_PackBegin(&context, tmp_pixels, bmp_w, bmp_w, 0, 1, 0))
            continue;
        if (h_oversample > 0 || v_oversample > 0)
            stbtt_PackSetOversampling(&context, 1, 1);
        if (!stbtt_PackFontRange(&context, file, 0,
            (float)STBTT_POINT_SIZE(height),
            0, num_glyphs, char_data))
        {
            stbtt_PackEnd(&context);
            continue;
        }
        stbtt_PackEnd(&context);
        baked = 1;
        break;
    }
    if (!baked)
        {ret = 6; goto out;}
    transformed_pixels = gui_calloc(1, bmp_w * bmp_w * 4);
    if (!transformed_pixels)
        {ret = 4; goto out;}
    for (int i = 0; i < bmp_w * bmp_w; ++i)
    {
        if (tmp_pixels[i])
        {
            transformed_pixels[i * 4 + 0] = 0xFF;
            transformed_pixels[i * 4 + 1] = 0xFF;
            transformed_pixels[i * 4 + 2] = 0xFF;
            transformed_pixels[i * 4 + 3] = tmp_pixels[i];
        } else
        {
            transformed_pixels[i * 4 + 0] = 0;
            transformed_pixels[i * 4 + 1] = 0;
            transformed_pixels[i * 4 + 2] = 0;
            transformed_pixels[i * 4 + 3] = 0;
        }
    }
    ttf_data.info          = font_info;
    ttf_data.bitmap        = transformed_pixels;
    ttf_data.bitmap_w      = bmp_w;
    ttf_data.bitmap_h      = bmp_w;
    ttf_data.glyphs        = char_data;
    ttf_data.pixel_height  = height;
    ttf_data.num_glyphs    = num_glyphs;
    ttf_data.file          = file;
    ttf_data.scale = stbtt_ScaleForPixelHeight(&font_info, (float)height);
    stbtt_GetFontVMetrics(&font_info, &ttf_data.ascent, &ttf_data.descent,
        &ttf_data.linegap);
    /*-- Write to the font structure --*/
    glyphs = gui_malloc(ttf_data.num_glyphs * sizeof(gui_glyph_t));
    if (!glyphs)
        {ret = 7; goto out;}
    gui_glyph_t *g;
    int tmp_advance, tmp_left_side_bearing;
    for (int i = 0; i < ttf_data.num_glyphs; ++i)
    {
        stbtt_GetGlyphHMetrics(&ttf_data.info, i, &tmp_advance,
            &tmp_left_side_bearing);
        g                       = &glyphs[i];
        g->clip[0]              = ttf_data.glyphs[i].x0;
        g->clip[1]              = ttf_data.glyphs[i].y0;
        g->clip[2]              = ttf_data.glyphs[i].x1;
        g->clip[3]              = ttf_data.glyphs[i].y1;
        g->advance              = roundf(ttf_data.glyphs[i].xadvance);
        g->left_side_bearing    = (float)tmp_left_side_bearing;
        g->x_offset             = roundf((float)ttf_data.glyphs[i].xoff2);
        g->y_offset             = roundf((float)ttf_data.glyphs[i].yoff2);
    }
    font->glyphs            = glyphs;
    font->num_glyphs        = ttf_data.num_glyphs;
    font->vertical_advance  = roundf(ttf_data.scale * ((float)ttf_data.ascent -
        (float)ttf_data.descent + (float)ttf_data.linegap));
    font->height            = height;
    font->bitmap            = ttf_data.bitmap;
    font->bitmap_w          = ttf_data.bitmap_w;
    font->bitmap_h          = ttf_data.bitmap_h;
    font->tex               = 0;
    out:
        gui_free(tmp_pixels);
        gui_free(char_data);
        if (ret)
        {
            gui_free(file);
            gui_free(transformed_pixels);
            gui_free(glyphs);
            memset(font, 0, sizeof(*font));
        }
        return ret;
}

void
gui_font_destroy(gui_font_t *font)
{
    gui_free(font->glyphs);
    gui_free(font->bitmap);
    memset(font, 0, sizeof(*font));
}

int
gui_init(gui_init_config_t *config)
{
    int ret = 0;
    gui_assert(config->get_texture_dimensions_callback);
    gui.init_config = *config;
    if (!config->panic_callback)
        gui.init_config.panic_callback = _gui_panic;
    if (gui_font_load_from_memory(&gui.default_font, _gui_munro_ttf, 10))
        {ret = 1; goto out;}
    gui.wins                = _wins;
    gui.guides              = _guides;
    gui.win_stack           = _win_stack;
    gui.wins_open_parents   = _wins_open_parents;
    gui.draw_lists          = _draw_lists;
    darr_reserve(gui.win_child_lists, GUI_MAX_WINS);
    darr_reserve(gui.text_inputs, 32);
    memset(gui.win_child_lists, 0, GUI_MAX_WINS * sizeof(int*));
    /* gui.win_child_lists     = _win_child_lists; */
    /* Format buffer */
    gui.fmt_buf         = gui_malloc(1024);
    gui.fmt_buf_size    = 1024;
    if (!gui.fmt_buf)
        {ret = 7; goto out;}
    gui.win_num                             = 0;
    gui.frame_count                         = 0;
    gui.began_update                        = 0;
    gui.hovered_win_id                      = 0;
    gui.active_win_id                       = 0;
    gui.input_state.coordinate_space[0]     = 0;
    gui.input_state.coordinate_space[1]     = 0;
    gui.input_state.mouse_scroll_speed[0]   = 8;
    gui.input_state.mouse_scroll_speed[1]   = 8;
    gui.pressed_button_id                   = 0;
    gui.hovered_button                      = 0;
    gui.active_text_input                   = 0;
    gui.button_style                        = &gui.default_button_style;
    gui.win_style                           = &gui.default_win_style;
    gui.text_input_style                    = &gui.default_text_input_style;
    gui.font                                = &gui.default_font;
    gui.show_text_input_cursor              = 1;
    gui.text_input_cursor_frequency         = 0.25f;
    gui.text_input_cursor_timer             = 0.f;
    gui.text_input_made_inactive            = 0;
    gui.default_win_style                   = gui_create_win_style();
    gui.default_button_style                = gui_create_button_style();
    gui.default_text_input_style            = gui_create_text_input_style();
    gui.default_progress_bar_style          = gui_create_progress_bar_style();
    gui.active_win_id                       = 0;
    _gui_base_win_id = _gui_fnv_hash32_from_str(_gui_base_win_name);
    out:
        if (ret)
            gui_destroy();
        return ret;
}

void
gui_destroy(void)
{
    gui_free(gui.fmt_buf);
    gui.fmt_buf = 0;
    for (int i = 0; i < GUI_MAX_WINS; ++i)
        darr_free(gui.win_child_lists[i]);
    for (int i = 0; i < GUI_MAX_WINS; ++i)
    {
        darr_free(gui.wins[i].draw_con.draw_cmds);
        darr_free(gui.wins[i].draw_con.vert_floats);
    }
    memset(_wins, 0, sizeof(_wins));
    darr_free(gui.win_child_lists);
    darr_free(gui.text_inputs);
    gui_font_destroy(&gui.default_font);
    memset(&gui, 0, sizeof(gui));
}

void
gui_clear(void)
{
    gui_assert(!gui.began_update);
    gui.win_num                     = 0;
    gui.text_input_cursor_timer     = 0.f;
    gui.text_input_made_inactive    = 0;
    gui.active_win_id               = 0;
    darr_clear(gui.text_inputs);
}

gui_input_state_t *
gui_get_input_state(void)
{
    gui_assert(!gui.began_update);
    return &gui.input_state;
}

void
gui_begin(void)
{
    gui_assert(!gui.began_update);
    gui_assert(!gui.win_stack_num);
    gui.next_act_win.index              = -1;
    gui.next_act_win.is_override        = 0;
    gui.next_hov_win.index              = -1;
    gui.next_hov_win.is_override        = 0;
    gui.next_prs_btn.id                 = 0;
    gui.next_hov_btn.id                 = 0;
    gui.win_stack_num                   = 0;
    gui.wins_open_num                   = 0;
    gui.wins_open_parents_num           = 0;
    gui.began_update                    = 1;
    gui.origin                          = GUI_TOP_LEFT;
    gui.released_button_id              = 0;
    gui.clicked_this_frame              = 0;
    gui.guides_num                      = 0;
    gui.current_win_id                  = 0;
    gui.text_input_enter_pressed        = 0;
    gui.text_input_made_active_now      = 0;
    gui.hovered_last_frame              = gui.hovered_this_frame;
    gui.hovered_this_frame              = 0;
    gui.next_have_active_text_input     = 0;
    darr_clear(gui.win_child_lists);
    for (int i = 0; i < 4; ++i)
        gui.last_text_dimensions[i] = 0;
    for (int i = 0; i < 4; ++i)
        gui.last_button_dimensions[i] = 0;
    for (int i = 0; i < 4; ++i)
        gui.last_texture_dimensions[i] = 0;
    gui.last_text_origin    = GUI_TOP_LEFT;
    gui.last_button_origin  = GUI_TOP_LEFT;
    gui.last_texture_origin = GUI_TOP_LEFT;
    gui.mbtns_down_now = gui.input_state.mouse_buttons & \
        (gui.input_state.mouse_buttons ^ gui.last_input_state.mouse_buttons);
    gui.mbtns_up_now = gui.last_input_state.mouse_buttons & \
        (gui.last_input_state.mouse_buttons ^ gui.input_state.mouse_buttons);
    /* Do this after updating inputs */
    if (gui.pressed_element_button_flags && !gui.input_state.mouse_buttons)
        gui.released_this_frame = 1;
    else
        gui.released_this_frame = 0;
    gui_color(255, 255, 255, 255);
    /*-- Set styles to default --*/
    gui.win_style           = &gui.default_win_style;
    gui.button_style        = &gui.default_button_style;
    gui.text_input_style    = &gui.default_text_input_style;
    gui.progress_bar_style  = &gui.default_progress_bar_style;
    gui.font                = &gui.default_font;
    gui_begin_empty_win(_gui_base_win_name, 0, 0,
        gui.input_state.coordinate_space[2],
        gui.input_state.coordinate_space[3], 0);
}

void
gui_end(void)
{
    gui_assert(gui.began_update);
    gui_assert(gui.win_stack_num == 1);
    gui_end_win_internal(); /* End root window. */
    if (!gui.next_have_active_text_input)
        gui.active_text_input = 0;
    uint32_t next_act_id;
    /* Set the active window */
    gui_win_t *next_act_win = gui.next_act_win.index < 0 ? 0 : \
        &gui.wins[gui.next_act_win.index];
    if (next_act_win)
    {
        /* Clear is_parent_of_act_win flags */
        for (int i = 0; i < gui.win_num; ++i)
            gui.wins[i].is_parent_of_act_win = 0;
        next_act_win->last_active   = gui.frame_count;
        next_act_id                 = next_act_win->id;
        /*-- Also make the window's parents active --*/
        /* NOTE: Should this happen if the parent is click-through? */
        gui_win_t *parent;
        for (int pi = next_act_win->parent_index;
             pi != -1;
             pi = parent->parent_index)
        {
            parent                          = &gui.wins[pi];
            parent->last_active             = gui.frame_count;
            parent->is_parent_of_act_win    = 1;
        }
    } else
        next_act_id = 0;
    if (next_act_id)
        gui.active_win_id = next_act_id;
    /*-- Set the hovered window --*/
    if (!gui.input_state.mouse_buttons)
    {
        if (gui.next_hov_win.index >= 0)
            gui.hovered_win_id = gui.wins[gui.next_hov_win.index].id;
        /* There can only be a hovered button if no mouse button is currently
         * pressed */
        if (!gui.pressed_button_id)
            gui.hovered_button = gui.next_hov_btn.id;
        gui.pressed_element_button_flags = 0;
        /*-- Set text input inactive if something else was clicked. --*/
        if ((GUI_MOUSE_BUTTON_UP_NOW(GUI_MOUSE_BUTTON_LEFT) ||
            GUI_MOUSE_BUTTON_UP_NOW(GUI_MOUSE_BUTTON_RIGHT)) &&
            !gui.text_input_made_active_now)
            gui_set_active_text_input(0);
    }
    if (!(gui.input_state.mouse_buttons && gui.pressed_button_id
        && gui.hovered_win_id == gui.pressed_button_pid))
    {
        gui.pressed_button_id   = gui.next_prs_btn.id;
        gui.pressed_button_pid  = gui.next_prs_btn.parent_id;
        gui.pressed_button_mbtn = gui.next_prs_btn.mouse_btn;
    }
    /*-- Quit here if we don't have any active windows anyway --*/
    if (gui.wins_open_num == 0)
        goto out;
    /*-- Window scrolling --*/
    gui_win_t *hov_win = _gui_get_win_by_id(gui.hovered_win_id);
    if (hov_win && hov_win->flags & GUI_WIN_SCROLLABLE)
        for (int i = 0; i < 2; ++i)
            hov_win->scroll[i] += gui.input_state.mouse_scroll[i] *
                gui.input_state.mouse_scroll_speed[i];
    /*-- Sort active windows --*/
    int         *active_parent_wins = gui.wins_open_parents;
    gui_bool_t  swapped             = 1;
    gui_win_t   *win, *owin;
    int         tmp;
    while (swapped)
    {
        swapped = 0;
        for (int i = 0; i < gui.wins_open_parents_num - 1; ++i)
        {
            win     = &gui.wins[active_parent_wins[i]];
            owin    = &gui.wins[active_parent_wins[i + 1]];
            if (owin->last_active >= win->last_active)
                continue;
            tmp = active_parent_wins[i];
            active_parent_wins[i]      = active_parent_wins[i + 1];
            active_parent_wins[i + 1]  = tmp;
            swapped = 1;
        }
    }
    /*-- Build draw lists and a list of lists --*/
    gui_draw_list_t *lists              = gui.draw_lists;
    int             num_lists           = 0;
    int             **child_list;
    for (int i = 0; i < gui.wins_open_parents_num; ++i)
    {
        win         = &gui.wins[active_parent_wins[i]];
        child_list  = _gui_get_win_child_indices(win);
        if (child_list && darr_num(*child_list) > 0)
            _gui_sort_win_children(win);
        _gui_update_and_add_win_to_draw_list(win, gui.wins, lists, &num_lists);
    }
    gui.num_draw_lists = num_lists;
    gui.began_update = 0;
    gui.frame_count++;
    gui.text_input_cursor_timer += gui.input_state.delta_time;
    if (gui.text_input_cursor_timer >= gui.text_input_cursor_frequency)
    {
        gui.show_text_input_cursor  = gui.show_text_input_cursor ? 0 : 1;
        gui.text_input_cursor_timer = 0.f;
    }
    out:
        gui.last_input_state = gui.input_state;
}

const char *
gui_format_id(const char *fmt, ...)
{
    va_list args1;
    va_start(args1, fmt);
    int len = vsnprintf(gui.fmt_buf, gui.fmt_buf_size, fmt, args1);
    va_end(args1);
    gui_assert(len >= 0);
    if (len > gui.fmt_buf_size - 1)
    {
        size_t new_size = (size_t)len + 1;
        if (new_size < 2 * gui.fmt_buf_size)
            new_size = 2 * gui.fmt_buf_size;
        if (_gui_realloc_fmt_buf((int)new_size))
            return 0;
        va_list args2;
        va_start(args2, fmt);
        vsnprintf(gui.fmt_buf, gui.fmt_buf_size, fmt, args2);
        va_end(args2);
    }
    return gui.fmt_buf;
}

gui_draw_list_t *
gui_get_draw_lists(int *ret_num)
{
    *ret_num = gui.num_draw_lists;
    return gui.draw_lists;
}

gui_font_t *
gui_get_default_font(void)
    {return &gui.default_font;}

gui_win_style_t *
gui_get_default_win_style(void)
    {return &gui.default_win_style;}

gui_button_style_t *
gui_get_default_button_style(void)
    {return &gui.default_button_style;}

gui_text_input_style_t *
gui_get_default_text_input_style(void)
    {return &gui.default_text_input_style;}

gui_progress_bar_style_t *
gui_get_default_progress_bar_style(void)
    {return &gui.default_progress_bar_style;}

uint32_t
gui_get_active_win_id(void)
    {return gui.active_win_id;}

void
gui_set_active_win(const char *title)
{
    if (!title)
        title = _gui_base_win_name;
    gui_win_t *win = _gui_get_win_by_id(_gui_fnv_hash32_from_str(title));
    if (!win)
        return;
    gui.next_act_win.index          = _gui_get_win_index(win);
    gui.next_act_win.is_override    = 1;
}

gui_bool_t
gui_is_any_element_pressed(void)
{
    return gui.clicked_this_frame ||
        (gui.input_state.mouse_buttons && gui.pressed_element_button_flags);
}

gui_bool_t
gui_is_any_element_hovered(void)
    {return gui.hovered_this_frame | gui.hovered_last_frame;}

gui_bool_t
gui_is_any_button_pressed(void)
    {return gui.pressed_button_id ? 1 : 0;}

gui_bool_t
gui_is_any_text_input_active(void)
{
    return gui.active_text_input ? 1 : 0;
    /* return !gui.text_input_made_inactive && gui.active_text_input; */
}

int
gui_get_current_win_x(void)
{
    gui_win_t *p = _gui_get_win_stack_top();
    return p ? p->relative_x : 0;
}

int
gui_get_current_win_y(void)
{
    gui_win_t *p = _gui_get_win_stack_top();
    return p ? p->relative_y : 0;
}

int
gui_get_current_win_w(void)
{
    gui_win_t *p = _gui_get_win_stack_top();
    return p ? p->dim[2] : 0;
}

int
gui_get_current_win_h(void)
{
    gui_win_t *p = _gui_get_win_stack_top();
    return p ? p->dim[3] : 0;
}

int
gui_get_current_win_viewport_x(void)
{
    gui_win_t *win = _gui_get_win_stack_top();
    if (!win)
        return 0;
    return _compute_win_viewport_x(win);
}

int
gui_get_current_win_viewport_y(void)
{
    gui_win_t *win = _gui_get_win_stack_top();
    if (!win)
        return 0;
    return _compute_win_viewport_y(win);
}

int
gui_get_current_win_viewport_w(void)
{
    gui_win_t *win = _gui_get_win_stack_top();
    if (!win)
        return 0;
    return _compute_win_viewport_w(win);
}

int
gui_get_current_win_viewport_h(void)
{
    gui_win_t *win = _gui_get_win_stack_top();
    if (!win)
        return 0;
    return _compute_win_viewport_h(win);
}

float
gui_get_current_win_scroll_x(void)
{
    gui_win_t *win = _gui_get_win_stack_top();
    return win ? win->scroll_percentage[0] : 0.f;
}

float
gui_get_current_win_scroll_y(void)
{
    gui_win_t *win = _gui_get_win_stack_top();
    return win ? win->scroll_percentage[1] : 0.f;
}

static void *
darr_ensure_growth_by(void *darr, uint32_t grow_by, uint32_t item_sz)
{
    if (!grow_by)
        return darr;
    darr_head_t *h = darr_head(darr);
    if (!h)
    {
        uint32_t cap = grow_by > 4 ? grow_by : 4;
        h = gui_malloc(sizeof(darr_head_t) + cap * item_sz);
        h->num = 0;
        h->cap = cap;
    } else
    {
        uint32_t req = h->num + grow_by;
        if (req <= h->cap)
            return darr;
        uint32_t new_cap = h->cap * 2;
        new_cap = new_cap > req ? new_cap : req;
        h = gui_realloc(h, sizeof(darr_head_t) + new_cap * item_sz);
        h->cap = new_cap;
    }
    return h + 1;
}

static inline void
darr_sized_erase(void *darr, uint32_t index, uint32_t item_sz)
{
    darr_head_t *h = _darr_head(darr);
    gui_assert(index < h->num);
    size_t  num_bytes   = item_sz * (h->num - (index + 1));
    char    *mem        = (char*)darr;
    char    *ind_pos    = mem + index * item_sz;
    h->num--;
    memmove(ind_pos, ind_pos + item_sz, num_bytes);
}

static inline uint32_t
_gui_fnv_hash32_from_str(const char *str)
{
    uint32_t hash = GUI_FNV_32_SEED;
    for (const char *c = str; *c; ++c)
        {hash ^= *c; hash *= GUI_FNV_32_PRIME;}
    return hash;
}

static void
_gui_panic(const char *error, ...)
{
    printf("[GUI] Panic! ");
    va_list args;
    va_start(args, error);
    vprintf(error, args);
    va_end(args);
    abort();
}

static void
_set_next_active_win(gui_win_t *win)
{
    gui.next_act_win.index          = _gui_get_win_index(win);
    gui.next_act_win.last_active    = win->last_active;
    if (win->id != _gui_base_win_id)
        gui.pressed_element_button_flags = gui.input_state.mouse_buttons;
}

static gui_win_t *
_gui_begin_win_internal(const char *title, int x, int y, int w, int h,
    int flags)
{
    uint32_t    id                  = _gui_fnv_hash32_from_str(title);
    gui_win_t   *win                = _gui_get_win_by_id(id);
    gui_bool_t  created_this_frame  = 0;
    gui_bool_t  opened_this_frame   = 0;
    if (!win )
    {
        if (!(win = _gui_create_win(id)))
        {
            gui_assert(0);
            return 0;
        }
        created_this_frame = 1;
    }
    gui.current_win_id = id;
    _gui_compute_quad_position(&win->dim[0], &win->dim[1], gui.origin, x, y, w,
        h);
    if (created_this_frame || win->last_drawn != gui.frame_count - 1)
        opened_this_frame = 1;
    win->flags              = (uint8_t)flags;
    win->dim[2]             = w;
    win->dim[3]             = h;
    win->last_drawn         = gui.frame_count;
    win->child_list_index   = -1;
    win->relative_x         = x;
    win->relative_y         = y;
    win->content_wh[0]      = 0;
    win->content_wh[1]      = 0;
    _gui_draw_con_clear(&win->draw_con);
    int win_index = _gui_get_win_index(win);
    /* Determine if this window is potentially the next hovered or active
     * window */
    if (opened_this_frame)
    {
        if (!(flags & GUI_WIN_CLICKTHROUGH) && (!gui.next_act_win.is_override ||
            _gui_test_next_active_win_parenthood()))
            _set_next_active_win(win);
    }
    if (_gui_test_mouse_rect(win->dim[0], win->dim[1], win->dim[2],
        win->dim[3]))
    {
        /* TODO: changed to any mouse button */
        if ((GUI_MOUSE_BUTTON_DOWN_NOW(GUI_MOUSE_BUTTON_LEFT) ||
                GUI_MOUSE_BUTTON_DOWN_NOW(GUI_MOUSE_BUTTON_RIGHT)) &&
            !(flags & GUI_WIN_CLICKTHROUGH))
        {
            if (id != _gui_base_win_id)
                gui.clicked_this_frame = 1;
            next_gui_win_t *nw = &gui.next_act_win;
            if (nw->index < 0 || ((nw->last_active <= win->last_active ||
                _gui_test_next_active_win_parenthood()) && !nw->is_override))
                _set_next_active_win(win);
        } else
        if (gui.next_hov_win.index < 0 ||
            (gui.next_hov_win.last_active <= win->last_active ||
            _gui_test_next_hovered_win_parenthood()))
        {
            gui.next_hov_win.index          = win_index;
            gui.next_hov_win.last_active    = win->last_active;
        }
        if (id != _gui_base_win_id)
            gui.hovered_this_frame = 1;
    } else
    if (!gui.active_win_id) /* This is the first frame */
    {
        gui.next_act_win.index          = win_index;
        gui.next_act_win.last_active    = win->last_active;
    }
    /* If this is a child window, push it to the child array of the parent */
    gui_win_t *p = _gui_get_win_stack_top();
    if (p)
    {
        win->parent_index = gui.win_stack[gui.win_stack_num - 1];
        int **child_list = _gui_get_win_child_indices(p);
        if (!child_list)
        {
            int list_index = _gui_claim_win_child_list();
            if (list_index < 0)
            {
                gui_assert(0);
                return 0;
            }
            p->child_list_index = list_index;
            child_list = _gui_get_win_child_indices(p);
        }
        darr_push(*child_list, win_index);
        gui_assert(win_index != _gui_get_win_index(p));
    } else /* Only push to open win list if win has no parent */
    {
        win->parent_index = -1;
        gui.wins_open_parents[gui.wins_open_parents_num++] = win_index;
    }
    gui.wins_open_num++;
    /* Push on to the win stack */
    gui.win_stack[gui.win_stack_num++] = win_index;
    gui_assert(win);
    return win;
}

static void
gui_end_win_internal(void)
{
    int win_stack_num = --gui.win_stack_num;
    gui.guides_num = 0;
    if (win_stack_num)
        gui.current_win_id = _gui_get_win_stack_top()->id;
    else
        gui.current_win_id = 0;
}

uint32_t
gui_begin_win(const char *title, int x, int y, int w, int h, int flags)
{
    if (!title)
        return 0;
    gui_win_t *win = _gui_begin_win_internal(title, x, y, w, h, flags);
    if (!win)
        return 0;
    uint32_t id = win->id;
    if (!(flags & GUI_WIN_SCROLLABLE))
    {
        win->scroll[0] = 0;
        win->scroll[1] = 0;
    }
    /* Render */
    gui_win_state_style_t *ss;
    if (id == gui.active_win_id || win->is_parent_of_act_win)
        ss = &gui.win_style->states[GUI_WIN_STATE_ACTIVE];
    else if (id == gui.hovered_win_id)
        ss = &gui.win_style->states[GUI_WIN_STATE_HOVERED];
    else
        ss = &gui.win_style->states[GUI_WIN_STATE_INACTIVE];
    win->style = ss;
    /* Border if untextured */
    if (!ss->border.tex)
        _gui_win_draw_quad(win, win->dim[0], win->dim[1],
            win->dim[2], win->dim[3], 0, 0, ss->border.color, GUI_FLIP_NONE, 0,
            win->dim);
    /* Title */
    _gui_draw_con_draw_title_text(&win->draw_con, title,
        ss->title_font, ss->title_offset[0], ss->title_offset[1], win->dim,
        ss->title_origin, ss->title_scale[0], ss->title_scale[1],
        ss->title_color, win->dim);
    /* Background */
    int     tx      = win->dim[0] + ss->border.widths[GUI_EDGE_LEFT];
    int     ty      = win->dim[1] + ss->border.widths[GUI_EDGE_TOP];
    int     tw      = win->dim[2] - ss->border.widths[GUI_EDGE_LEFT] -
        ss->border.widths[GUI_EDGE_RIGHT];
    int     th      = win->dim[3] - ss->border.widths[GUI_EDGE_TOP] -
        ss->border.widths[GUI_EDGE_BOTTOM];
    void   *tex    = ss->background_tex;
    float   tmp_clip[4];
    float   *clip;
    if (tex)
    {
        clip = ss->background_clip;
        if (!clip[0] && !clip[1] && !clip[2] && !clip[3])
        {
            float tex_w, tex_h;
            _gui_get_tex_w_h(tex, &tex_w, &tex_h);
            clip = tmp_clip;
            clip[0] = 0;
            clip[1] = 0;
            clip[2] = tex_w;
            clip[3] = tex_h;
        }
    } else
        clip = 0;
    _gui_win_draw_quad(win, tx, ty, tw, th, tex, clip, ss->background_color,
        ss->background_flip, 0, win->dim);
    /*  Border, if textured */
    if (ss->border.tex)
        _gui_draw_con_draw_tex_border(&win->draw_con, ss->border.tex,
            ss->border.clips, ss->border.color, win->dim, win->dim);
    return id;
}

uint32_t
gui_begin_empty_win(const char *title, int x, int y, int w, int h, int flags)
{
    gui_win_t *win = _gui_begin_win_internal(title, x, y, w, h, flags);
    if (!win)
        return 0;
    win->style = 0;
    return win->id;
}

int
gui_end_win(void)
{
    gui_assert(gui.win_stack_num > 1);
    if (gui.win_stack_num <= 1)
        return 1;
    gui_end_win_internal();
    return 0;
}

int
gui_begin_guide(int x, int y, int w, int h)
{
    if (gui.guides_num == GUI_MAX_GUIDES)
        return 1;
    int *g = &gui.guides[gui.guides_num * 4];
    _gui_compute_quad_position(&g[0], &g[1], gui.origin, x, y, w, h);
    g[2] = w;
    g[3] = h;
    gui.guides_num++;
    return 0;
}

void
gui_end_guide(void)
{
    gui_assert(gui.guides_num > 0);
    gui.guides_num--;
}

uint32_t
gui_repeat_button(const char *title, int x, int y, int w, int h, uint32_t mbtn_mask)
{
    uint32_t id = _gui_fnv_hash32_from_str(title);
    _gui_graphic_button(id, title, x, y, w, h, mbtn_mask);
    if (gui.pressed_button_id == id)
        return gui.pressed_button_mbtn;
    return 0;
}

uint32_t
gui_invisible_repeat_button(const char *title, int x, int y, int w, int h,
    uint32_t mbtn_mask)
{
    uint32_t id = _gui_fnv_hash32_from_str(title);
    _gui_button_internal(id, x, y, w, h, mbtn_mask, 0, 0, 0, 1);
    if (gui.pressed_button_id == id)
        return gui.pressed_button_mbtn;
    return 0;
}

uint32_t
gui_button(const char *title, int x, int y, int w, int h, uint32_t mbtn_mask)
{
    uint32_t id = _gui_fnv_hash32_from_str(title);
    return _gui_graphic_button(id, title, x, y, w, h, mbtn_mask);
}

uint32_t
gui_invisible_button(const char *title, int x, int y, int w, int h,
    uint32_t mbtn_mask)
{
    uint32_t id = _gui_fnv_hash32_from_str(title);
    return _gui_button_internal(id, x, y, w, h, mbtn_mask, 0, 0, 0, 1);
}

void
gui_text(const char *text, int wrap, int x, int y)
    {gui_text_s(text, wrap, x, y, 1.f);}

void
gui_textf(const char *fmt, int wrap, int x, int y, ...)
{
    if (!fmt)
        return;
    int res;
    va_list args;
    va_start(args, y);
    res = vsnprintf(gui.fmt_buf, gui.fmt_buf_size, fmt, args);
    va_end(args);
    if (res >= gui.fmt_buf_size)
    {
        _gui_realloc_fmt_buf(res + 1);
        va_list args;
        va_start(args, y);
        vsnprintf(gui.fmt_buf, gui.fmt_buf_size, fmt, args);
        va_end(args);
    }
    gui_text(gui.fmt_buf, wrap, x, y);
}

void
gui_text_s(const char *text, int wrap, int x, int y, float s)
{
    if (!text)
        return;
    gui_win_t *win = _gui_get_win_stack_top();
    int content_rect[4];
    _gui_compute_win_viewport_rect(win, content_rect);
    int scissor[4];
    _gui_get_win_inner_scissor(win, scissor);
    _gui_draw_con_draw_text(&win->draw_con, text, gui.font,
        (float)(x + win->scroll[0]), (float)(y + win->scroll[1]), content_rect,
        gui.origin, wrap, s, s, gui.color, gui.last_text_dimensions, scissor);
    gui.last_text_origin = gui.origin;
    _gui_save_parent_content_wh(win, x, y, gui.last_text_dimensions[2],
        gui.last_text_dimensions[3], gui.origin);
}

void
gui_textf_s(const char *fmt, int wrap, int x, int y, float s, ...)
{
    if (!fmt)
        return;
    int res;
    va_list args;
    va_start(args, s);
    res = vsnprintf(gui.fmt_buf, gui.fmt_buf_size, fmt, args);
    va_end(args);
    if (res >= gui.fmt_buf_size)
    {
        _gui_realloc_fmt_buf(res + 1);
        va_list args;
        va_start(args, s);
        vsnprintf(gui.fmt_buf, gui.fmt_buf_size, fmt, args);
        va_end(args);
    }
    gui_text_s(gui.fmt_buf, wrap, x, y, s);
}

gui_bool_t
gui_text_input_ext(const char *title, char *buf, uint32_t buf_size, int x,
    int y, int w, int h, int flags,
    int (*callback)(const gui_text_input_event_t *event,
        union gui_text_input_callback_return_values *ret_values),
    void *user_data)
{
    gui.text_input_enter_pressed = 0;
    if (!title || !buf || !buf_size)
        return 0;
    gui_text_input_event_t  event   = {.user_data = user_data};
    uint32_t                id      = _gui_fnv_hash32_from_str(title);
    gui_text_input_t        *ti     = _gui_get_text_input_by_id(id);
    if (!ti)
    {
        ti                  = _gui_create_text_input(id);
        ti->cursor_index    = (int)strlen(buf);
    }
    gui_win_t                       *win;
    int                             tx, ty, scissor[4];
    gui_text_input_state_style_t    *ss;
    uint32_t clicked = _gui_button_internal(id, x, y, w, h, 0, &tx, &ty, &win,
        0);
    if (clicked)
    {
        gui.active_text_input           = id;
        gui.text_input_made_inactive    = 0;
        gui.text_input_made_active_now  = 1;
    }
    gui_bool_t is_active = !gui.text_input_made_inactive &&
        gui.active_text_input == id && gui.active_win_id == win->id;
    if (is_active)
        ss = &gui.text_input_style->states[GUI_TEXT_INPUT_STATE_ACTIVE];
    else
        ss = &gui.text_input_style->states[GUI_TEXT_INPUT_STATE_INACTIVE];
    _gui_get_win_inner_scissor(win, scissor);
    /*-- Border -*/
    if (ss->border.color[3])
        _gui_win_draw_quad(win, tx, ty, w, h, 0, 0, ss->border.color,
            GUI_FLIP_NONE, 0, scissor);
    int text_rect[4] = {
        tx + ss->border.widths[GUI_EDGE_LEFT],
        ty + ss->border.widths[GUI_EDGE_TOP],
        w - ss->border.widths[GUI_EDGE_RIGHT] -
            ss->border.widths[GUI_EDGE_LEFT],
        h - ss->border.widths[GUI_EDGE_BOTTOM] -
            ss->border.widths[GUI_EDGE_TOP]};
    /*-- Background --*/
    if (ss->background_color[3])
        _gui_win_draw_quad(win, text_rect[0], text_rect[1], text_rect[2],
            text_rect[3], 0, 0, ss->background_color, GUI_FLIP_NONE, 0,
            scissor);
    int len = (int)strlen(buf);
    if (is_active)
    {
        int         i               = len;
        gui_bool_t  buffer_modified = 0; /* For event. */
        if (ti->cursor_index > len)
            ti->cursor_index = len;
        for (char *c = gui.input_state.text_input; *c; ++c)
        {
            switch (*c)
            {
            case GUI_CHAR_BACKSPACE:
            {
                if (!ti->cursor_index)
                    break;
                if (ti->cursor_index == len)
                    buf[len - 1] = 0;
                else
                {
                    char *base = buf + ti->cursor_index;
                    memmove(base - 1, base, len - ti->cursor_index + 1);
                }
                ti->cursor_index--;
                buffer_modified = 1;
            }
                break;
            case GUI_CHAR_DELETE:
            {
                if (ti->cursor_index == len)
                    break;
                char *base = buf + ti->cursor_index;
                memmove(base, base + 1, len + 1 - ti->cursor_index);
                len--;
                buffer_modified = 1;
            }
                break;
            case GUI_CHAR_LEFT:
                if (ti->cursor_index == 0)
                    break;
                ti->cursor_index--;
                gui.text_input_cursor_timer = 0;
                gui.show_text_input_cursor  = 1;
                break;
            case GUI_CHAR_RIGHT:
                if (ti->cursor_index >= len)
                    break;
                ti->cursor_index++;
                gui.text_input_cursor_timer = 0;
                gui.show_text_input_cursor  = 1;
                break;
            case GUI_CHAR_LONG_LEFT:
                ti->cursor_index = _gui_find_cursor_left_jump_index(buf,
                    ti->cursor_index);
                gui.text_input_cursor_timer = 0;
                gui.show_text_input_cursor  = 1;
                break;
            case GUI_CHAR_LONG_RIGHT:
                ti->cursor_index = _gui_find_cursor_right_jump_index(buf,
                    ti->cursor_index);
                gui.text_input_cursor_timer = 0;
                gui.show_text_input_cursor  = 1;
                break;
            case GUI_CHAR_LONG_BACKSPACE:
            {
                int cursor_index    = ti->cursor_index;
                int index = _gui_find_cursor_left_jump_index(buf, cursor_index);
                int num_moved_chars       = len - cursor_index + 1;
                memmove(buf + index, buf + cursor_index, num_moved_chars);
                int num_del_chars = cursor_index - index;
                len -= num_del_chars;
                if (ti->cursor_index != index)
                    buffer_modified = 1;
                ti->cursor_index = index;
            }
                break;
            case GUI_CHAR_LONG_DELETE:
            {
                int cursor_index    = ti->cursor_index;
                int index           = _gui_find_cursor_right_jump_index(buf,
                    cursor_index);
                int num_moved_chars       = len - index + 1;
                memmove(buf + cursor_index, buf + index, num_moved_chars);
                int num_del_chars = index - cursor_index;
                len -= num_del_chars;
                if (num_del_chars > 0)
                    buffer_modified = 1;
            }
                break;
            case GUI_CHAR_LINE_BEGIN:
                ti->cursor_index = 0;
                break;
            case GUI_CHAR_LINE_END:
                ti->cursor_index = len;
                break;
            case '\n':
                gui.text_input_enter_pressed = 1;
                break;
            default:
                if (!isascii(*c))
                    continue;
                if (i == buf_size - 1)
                {
                    if (flags & GUI_TEXT_INPUT_FLAG_CALLBACK_MAX_SIZE_REACHED &&
                        callback)
                    {
                        event.type = GUI_TEXT_INPUT_EVENT_MAX_SIZE_REACHED;
                        event.max_size_reached.buf      = buf;
                        event.max_size_reached.buf_size = buf_size;
                        event.max_size_reached.min_new_buf_size =
                            buf_size + GUI_TEXT_INPUT_CHARS_PER_FRAME + 1;
                        union gui_text_input_callback_return_values user_ret;
                        if (callback(&event, &user_ret))
                            break; /* TODO: Just break? Or do what? */
                        if (user_ret.max_size_reached.new_buf_size <
                            event.max_size_reached.min_new_buf_size)
                            break; /* TODO Same as above. */
                        buf         = user_ret.max_size_reached.new_buf;
                        buf_size    = user_ret.max_size_reached.new_buf_size;
                    } else
                        break;
                }
                if (flags & GUI_TEXT_INPUT_FLAG_POSITIVE_INTEGER &&
                    !isdigit(*c))
                    break;
                char *base = buf + ti->cursor_index;
                memmove(base + 1, base, (++len) - ti->cursor_index);
                buf[ti->cursor_index++] = *c;
                buffer_modified = 1;
            }
        }
        if (buffer_modified &&
            flags & GUI_TEXT_INPUT_FLAG_CALLBACK_BUFFER_MODIFIED && callback)
        {
            event.type = GUI_TEXT_INPUT_EVENT_BUFFER_MODIFIED;
            callback(&event, 0);
        }
    }
    /* Check for non-allowed characters in case user inserted them through other
     * means than the GUI. */
    if (flags & GUI_TEXT_INPUT_FLAG_POSITIVE_INTEGER)
    {
        char *a = buf;
        for (char *b = buf; *b; b++)
            if (isdigit(*b) || !*b)
                *(a++) = *b;
        *a = 0;
        len = (int)(a - buf);
        if (ti->cursor_index > len)
            ti->cursor_index = len;
    }
    _gui_draw_con_draw_input_text(&win->draw_con, ti, buf, is_active,
        ss->input_font, (float)ss->input_pixel_offset[0],
        (float)ss->input_pixel_offset[1], text_rect, ss->input_scale,
        ss->input_scale, ss->input_color, scissor, flags);
    int rect[4];
    _gui_compute_rect_by_edge(ss->title_edge, tx, ty, w, h, rect);
    _gui_draw_con_draw_title_text(&win->draw_con, title, ss->title_font,
        ss->title_pixel_offset[0], ss->title_pixel_offset[1], rect,
        ss->title_origin, ss->title_scale, ss->title_scale, ss->title_color,
        scissor);
    gui.last_text_input_origin          = gui.origin;
    gui.last_text_input_dimensions[0]   = x;
    gui.last_text_input_dimensions[1]   = y;
    gui.last_text_input_dimensions[2]   = w;
    gui.last_text_input_dimensions[3]   = h;
    if (is_active)
        gui.next_have_active_text_input = 1;
    return is_active;
}

gui_bool_t
gui_text_input(const char *title, char *buf, uint32_t buf_size, int x,
    int y, int w, int h, int flags)
    {return gui_text_input_ext(title, buf, buf_size, x, y, w, h, flags, 0, 0);}

gui_bool_t
gui_text_input_enter_pressed()
    {return gui.text_input_enter_pressed;}

void
gui_set_active_text_input(const char *title)
{
    if (title)
    {
        gui.active_text_input           = _gui_fnv_hash32_from_str(title);
        gui.text_input_made_inactive    = 0;
        gui.text_input_made_active_now  = 1;
        gui.next_have_active_text_input = 1;
    } else
    {
        gui.active_text_input           = 0;
        gui.text_input_made_inactive    = 1;
    }
}

gui_bool_t
gui_is_text_input_active(const char *title)
{
    if (!gui_is_any_text_input_active())
        return 0;
    if (title)
        return _gui_fnv_hash32_from_str(title) == gui.active_text_input;
    else
        return !gui.active_text_input;
}

int
gui_texture(void *tex, float *clip, int x, int y)
{
    return gui_texture_scfr(tex, clip, x, y, 1.0f, 1.0f, gui_color_white,
           GUI_FLIP_NONE, 0.0f);
}

int
gui_texture_s(void *tex, float *clip, int x, int y, float sx, float sy)
{
    return gui_texture_scfr(tex, clip, x, y, sx, sy, gui_color_white,
           GUI_FLIP_NONE, 0.0f);
}

int
gui_texture_c(void *tex, float *clip, int x, int y, uint8_t col[4])
{
    return gui_texture_scfr(tex, clip, x, y, 1.0f, 1.0f, col,
           GUI_FLIP_NONE, 0.0f);
}

int
gui_texture_f(void *tex, float *clip, int x, int y, enum gui_flip flip)
{
    return gui_texture_scfr(tex, clip, x, y, 1.0f, 1.0f, gui_color_white,
           flip, 0.0f);
}

int
gui_texture_r(void *tex, float *clip, int x, int y, float rot)
{
    return gui_texture_scfr(tex, clip, x, y, 1.0f, 1.0f, gui_color_white,
           GUI_FLIP_NONE, rot);
}

int
gui_texture_sc(void *tex, float *clip, int x, int y,
    float sx, float sy, uint8_t col[4])
{
    return gui_texture_scfr(tex, clip, x, y, sx, sy, col,
           GUI_FLIP_NONE, 0.0f);
}

int
gui_texture_sf(void *tex, float *clip, int x, int y, float sx, float sy,
    enum gui_flip flip)
{
    return gui_texture_scfr(tex, clip, x, y, sx, sy, gui_color_white,
           flip, 0.0f);
}

int
gui_texture_sr(void *tex, float *clip, int x, int y, float sx, float sy,
    float rot)
{
    return gui_texture_scfr(tex, clip, x, y, sx, sy, gui_color_white,
        GUI_FLIP_NONE, rot);
}

int
gui_texture_cf(void *tex, float *clip, int x, int y, uint8_t col[4],
    enum gui_flip flip)
{
    return gui_texture_scfr(tex, clip, x, y, 1.0f, 1.0f, col,
           flip, 0.0f);
}

int
gui_texture_cr(void *tex, float *clip, int x, int y, uint8_t col[4],
    float rot)
{
    return gui_texture_scfr(tex, clip, x, y, 1.0f, 1.0f, col,
        GUI_FLIP_NONE, rot);
}

int
gui_texture_scf(void *tex, float *clip, int x, int y,
    float sx, float sy, uint8_t col[4], enum gui_flip flip)
{
    return gui_texture_scfr(tex, clip, x, y, sx, sy, col, flip, 0.0f);
}

int
gui_texture_scfr(void *tex, float *clip, int x, int y,
    float sx, float sy, uint8_t col[4], enum gui_flip flip, float rot)
{
    float *uclip;
    float tmp_clip[4];
    if (clip)
        uclip = clip;
    else
    {
        float tex_w, tex_h;
        _gui_get_tex_w_h(tex, &tex_w, &tex_h);
        tmp_clip[0] = 0.f;
        tmp_clip[1] = 0.f;
        tmp_clip[2] = tex_w;
        tmp_clip[3] = tex_h;
        uclip       = tmp_clip;
    }
    gui_win_t *p = _gui_get_win_stack_top();
    int sw = (int)(sx * (float)(uclip[2] - uclip[0]));
    int sh = (int)(sy * (float)(uclip[3] - uclip[1]));
    int tx, ty;
    _gui_compute_quad_position(&tx, &ty, gui.origin, x, y, sw, sh);
    int scissor[4];
    _gui_get_win_inner_scissor(p, scissor);
    int r = _gui_win_draw_quad(p, tx, ty, sw, sh, tex, uclip, col, flip, rot,
        scissor);
    if (!r)
    {
        gui.last_texture_origin         = gui.origin;
        gui.last_texture_dimensions[0]  = x;
        gui.last_texture_dimensions[1]  = y;
        gui.last_texture_dimensions[2]  = sw;
        gui.last_texture_dimensions[3]  = sh;
    }
    return r;
}

int
gui_rectangle(int x, int y, int w, int h)
{
    gui_win_t *p = _gui_get_win_stack_top();
    int crect[4];
    _gui_compute_win_viewport_rect(p, crect);
    int tx, ty;
    _gui_compute_quad_parented_xy(&tx, &ty, gui.origin, crect, x,  y,  w, h);
    int scissor[4];
    _gui_get_win_inner_scissor(p, scissor);
    return _gui_win_draw_quad(p, tx, ty, w, h, 0, 0, gui.color, GUI_FLIP_NONE,
        0, scissor);
}

uint32_t
gui_progress_bar_int(const char *title, int x, int y, int w, int h,
    enum gui_progress_direction direction, uint32_t mouse_button_mask,
    int value1, int value2)
{
    int         tx, ty;
    gui_win_t   *win;
    gui_bool_t save_last_button_dimensions  = 1;
    uint32_t    id                          = _gui_fnv_hash32_from_str(title);
    uint32_t    button_state = _gui_button_internal(id, x, y, w, h,
        mouse_button_mask, &tx, &ty, &win, save_last_button_dimensions);
    int scissor[4];
    _gui_get_win_inner_scissor(win, scissor);
    gui_progress_bar_style_t        *style  = gui.progress_bar_style;
    gui_progress_bar_state_style_t  *state_style;
    if (gui.pressed_button_id == id)
        state_style = &style->states[GUI_PROGRESS_BAR_STATE_PRESSED];
    else if (gui.hovered_button == id)
        state_style = &style->states[GUI_PROGRESS_BAR_STATE_HOVERED];
    else
        state_style = &style->states[GUI_PROGRESS_BAR_STATE_NORMAL];
    /* Border */
    if(!state_style->border.tex)
        _gui_win_draw_quad(win, tx, ty, w, h, 0, 0, state_style->border.color,
            GUI_FLIP_NONE, 0, scissor);
    /* Background */
    _gui_win_draw_quad(win, tx + state_style->border.widths[GUI_EDGE_LEFT],
        ty + state_style->border.widths[GUI_EDGE_TOP],
        w - state_style->border.widths[GUI_EDGE_LEFT] -
            state_style->border.widths[GUI_EDGE_RIGHT],
        h - state_style->border.widths[GUI_EDGE_TOP] -
            state_style->border.widths[GUI_EDGE_BOTTOM],
        0, 0, state_style->background.color, GUI_FLIP_NONE, 0, scissor);
    /* Progress */
    if (value1 > 0 && value2 > 0)
    {
        int v1 = value1;
        int v2 = value2;
        if (v1 >  v2)
            v1 = v2;
        float percentage = (float)v1 / (float)v2;
        int fill_x;
        int fill_y;
        int fill_w;
        int fill_h;
        switch (direction)
        {
        case GUI_PROGRESS_DIRECTION_LEFT_TO_RIGHT:
        {
            fill_x = tx + state_style->border.widths[GUI_EDGE_LEFT];
            fill_y = ty + state_style->border.widths[GUI_EDGE_TOP];
            fill_w = (int)(percentage *
                (w - state_style->border.widths[GUI_EDGE_LEFT] -
                    state_style->border.widths[GUI_EDGE_RIGHT]));
            fill_h = h - state_style->border.widths[GUI_EDGE_TOP] -
                state_style->border.widths[GUI_EDGE_BOTTOM];
        }
            break;
        case GUI_PROGRESS_DIRECTION_RIGHT_TO_LEFT:
        {
            fill_w = (int)(percentage *
                (w - state_style->border.widths[GUI_EDGE_LEFT] -
                    state_style->border.widths[GUI_EDGE_RIGHT]));
            fill_h = h - state_style->border.widths[GUI_EDGE_TOP] -
                state_style->border.widths[GUI_EDGE_BOTTOM];
            fill_x = tx + w - state_style->border.widths[GUI_EDGE_RIGHT] -
                fill_w;
            fill_y = ty + state_style->border.widths[GUI_EDGE_TOP];
        }
            break;
        case GUI_PROGRESS_DIRECTION_BOTTOM_TO_TOP:
        {
            fill_w = w - state_style->border.widths[GUI_EDGE_LEFT] -
                state_style->border.widths[GUI_EDGE_RIGHT];
            fill_h = (int)(percentage *
                (h - state_style->border.widths[GUI_EDGE_TOP] -
                    state_style->border.widths[GUI_EDGE_BOTTOM]));
            fill_x = tx + state_style->border.widths[GUI_EDGE_LEFT];
            fill_y = ty + h - state_style->border.widths[GUI_EDGE_BOTTOM] -
                fill_h;
        }
            break;
        case GUI_PROGRESS_DIRECTION_TOP_TO_BOTTOM:
        {
            fill_x = tx + state_style->border.widths[GUI_EDGE_LEFT];
            fill_y = ty + state_style->border.widths[GUI_EDGE_TOP];
            fill_w = w - state_style->border.widths[GUI_EDGE_LEFT] -
                state_style->border.widths[GUI_EDGE_RIGHT];
            fill_h = (int)(percentage *
                (h - state_style->border.widths[GUI_EDGE_TOP] -
                    state_style->border.widths[GUI_EDGE_BOTTOM]));
        }
            break;
        }
        _gui_win_draw_quad(win, fill_x, fill_y, fill_w, fill_h,
            state_style->fill.tex, 0, state_style->fill.color, GUI_FLIP_NONE,
            0, scissor);
        if (state_style->border.tex)
            _gui_draw_con_draw_tex_border(&win->draw_con,
                state_style->border.tex, state_style->border.clips,
                state_style->border.color, win->dim, win->dim);
    }
    /* Border, if textured */
    if (state_style->border.tex)
        _gui_draw_con_draw_tex_border(&win->draw_con, state_style->border.tex,
            state_style->border.clips, state_style->border.color, win->dim,
            win->dim);
    /* Title */
    int rect[4] =
    {
        tx + state_style->border.widths[GUI_EDGE_LEFT],
        ty + state_style->border.widths[GUI_EDGE_TOP],
        w - state_style->border.widths[GUI_EDGE_RIGHT] -
            state_style->border.widths[GUI_EDGE_LEFT],
        h - state_style->border.widths[GUI_EDGE_BOTTOM] -
            state_style->border.widths[GUI_EDGE_TOP]
    };
    _gui_draw_con_draw_title_text(&win->draw_con, title,
        state_style->title_font, state_style->title_pixel_offset[0],
        state_style->title_pixel_offset[1], rect, state_style->title_origin,
        state_style->title_scale, state_style->title_scale,
        state_style->title_color, scissor);
    char progress[128];
    switch (state_style->progress_style)
    {
    case GUI_PROGRESS_BAR_PROGRESS_STYLE_PERCENTAGE:
    {
        int percentage;
        if (value2 != 0)
            percentage = (int)((float)value1 / (float)value2 * 100.f);
        else
            percentage = 100;
        snprintf(progress, sizeof(progress), "%d%%", percentage);
    }
        break;
    case GUI_PROGRESS_BAR_PROGRESS_STYLE_X_SLASH_Y:
        snprintf(progress, sizeof(progress), "%d/%d", value1, value2);
        break;
    }
    _gui_draw_con_draw_title_text(&win->draw_con, progress,
        state_style->progress_font, state_style->progress_pixel_offset[0],
        state_style->progress_pixel_offset[1], rect,
        state_style->progress_origin, state_style->progress_scale,
        state_style->progress_scale, state_style->progress_color, scissor);
    return button_state;
}

void
gui_origin(enum gui_origin origin)
    {gui.origin = origin;}

void
gui_font(gui_font_t *font)
    {gui.font = font ? font : &gui.default_font;}

void
gui_color(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
    gui.color[0] = r;
    gui.color[1] = g;
    gui.color[2] = b;
    gui.color[3] = a;
}

void
gui_win_style(gui_win_style_t *style)
    {gui.win_style = style ? style : &gui.default_win_style;}

void
gui_button_style(gui_button_style_t *style)
    {gui.button_style = style ? style : &gui.default_button_style;}

void
gui_text_input_style(gui_text_input_style_t *style)
    {gui.text_input_style = style ? style : &gui.default_text_input_style;}

void
gui_progress_bar_style(gui_progress_bar_style_t *style)
    {gui.progress_bar_style = style ? style : &gui.default_progress_bar_style;}

gui_bool_t
gui_is_window_active()
    {return gui.current_win_id == gui.active_win_id;}

gui_bool_t
gui_is_window_hovered()
    {return gui.current_win_id == gui.hovered_win_id;}

gui_bool_t
gui_is_button_hovered(void)
    {return gui.last_drawn_button_id == gui.hovered_button;}

int
gui_get_origin(void)
    {return gui.origin;}

int
gui_get_last_text_origin(void)
    {return gui.last_text_origin;}

void gui_get_last_text_dimensions(int rect[4])
{
    for (int i = 0; i < 4; ++i)
        rect[i]= gui.last_text_dimensions[i];
}

int
gui_get_last_text_x(void)
    {return gui.last_text_dimensions[0];}

int
gui_get_last_text_y(void)
    {return gui.last_text_dimensions[1];}

int
gui_get_last_text_w(void)
    {return gui.last_text_dimensions[2];}

int
gui_get_last_text_h(void)
    {return gui.last_text_dimensions[3];}

int
gui_get_last_button_origin(void)
    {return gui.last_button_origin;}

void
gui_get_last_button_dimensions(int rect[4])
{
    for (int i = 0; i < 4; ++i)
        rect[i] = gui.last_button_dimensions[i];
}

int
gui_get_last_button_x(void)
    {return gui.last_button_dimensions[0];}

int
gui_get_last_button_y(void)
    {return gui.last_button_dimensions[1];}

int
gui_get_last_button_w(void)
    {return gui.last_button_dimensions[2];}

int
gui_get_last_button_h(void)
    {return gui.last_button_dimensions[3];}

int
gui_get_last_texture_origin(void)
    {return gui.last_texture_origin;}

void
gui_get_last_texture_dimensions(int rect[4])
{
    for (int i = 0; i < 4; ++i)
        rect[i] = gui.last_texture_dimensions[i];
}

int
gui_get_last_texture_x(void)
    {return gui.last_texture_dimensions[0];}

int
gui_get_last_texture_y(void)
    {return gui.last_texture_dimensions[1];}

int
gui_get_last_texture_w(void)
    {return gui.last_texture_dimensions[2];}

int
gui_get_last_texture_h(void)
    {return gui.last_texture_dimensions[3];}

int
gui_get_last_text_input_origin(void)
    {return gui.last_text_input_origin;}

int
gui_get_last_text_input_x(void)
    {return gui.last_text_input_dimensions[0];}

int
gui_get_last_text_input_y(void)
    {return gui.last_text_input_dimensions[1];}

int
gui_get_last_text_input_w(void)
    {return gui.last_text_input_dimensions[2];}

int
gui_get_last_text_input_h(void)
    {return gui.last_text_input_dimensions[3];}

int gui_get_num_open_wins(void)
    {return gui.wins_open_num ? gui.wins_open_num - 1 : 0;}

gui_win_style_t
gui_create_win_style(void)
{
    gui_win_style_t style;
    for (int i = 0; i < 3; ++i)
    {
        style.states[i].border.widths[GUI_EDGE_TOP]     = 15;
        style.states[i].border.widths[GUI_EDGE_BOTTOM]  = 2;
        style.states[i].border.widths[GUI_EDGE_LEFT]    = 2;
        style.states[i].border.widths[GUI_EDGE_RIGHT]   = 2;
        style.states[i].title_origin                    = GUI_TOP_LEFT;
        style.states[i].title_offset[0]                 = 2;
        style.states[i].title_offset[1]                 = 0;
        style.states[i].title_scale[0]                  = 1.f;
        style.states[i].title_scale[1]                  = 1.f;
        style.states[i].title_font                      = &gui.default_font;
        style.states[i].border.tex                      = 0;
        style.states[i].background_tex                  = 0;
        style.states[i].background_flip                 = GUI_FLIP_NONE;
        gui_set_color_array(style.states[i].background_color, 5, 5, 5, 255);
    }
    for (int i = 0; i < 2; ++i)
    {
        memset(style.states[i].title_color, 128, 4);
        memset(style.states[i].background_clip, 0,
            sizeof(style.states[i].background_clip));
        gui_set_color_array(style.states[i].border.color,
            23,  23, 23, 255);
    }
    gui_set_color_array(style.states[GUI_WIN_STATE_ACTIVE].border.color,
        33, 33, 33, 255);
    memset(style.states[GUI_WIN_STATE_ACTIVE].title_color, 0xFF, 4);
    memset(style.states[GUI_WIN_STATE_ACTIVE].background_clip, 0,
        sizeof(style.states[GUI_WIN_STATE_ACTIVE].background_clip));
    return style;
}

gui_button_style_t
gui_create_button_style(void)
{
    gui_button_style_t style;
    gui_set_color_array(style.states[GUI_BUTTON_STATE_NORMAL].background_color,
        13, 13, 13, 255);
    gui_set_color_array(style.states[GUI_BUTTON_STATE_HOVERED].background_color,
        13, 28, 28, 255);
    gui_set_color_array(style.states[GUI_BUTTON_STATE_PRESSED].background_color,
        13, 43, 43, 255);
    gui_set_color_array(style.states[GUI_BUTTON_STATE_NORMAL].border.color,
        23, 23, 23, 255);
    gui_set_color_array(style.states[GUI_BUTTON_STATE_HOVERED].border.color,
        33, 33, 33, 255);
    gui_set_color_array(style.states[GUI_BUTTON_STATE_PRESSED].border.color,
        43, 43, 43, 255);
    for (int i = 0; i < 3; ++i)
    {
        style.states[i].border.widths[GUI_EDGE_TOP]     = 1;
        style.states[i].border.widths[GUI_EDGE_BOTTOM]  = 1;
        style.states[i].border.widths[GUI_EDGE_LEFT]    = 1;
        style.states[i].border.widths[GUI_EDGE_RIGHT]   = 1;
        style.states[i].title_origin                    = GUI_CENTER_CENTER;
        style.states[i].title_offset[0]                 = 0;
        style.states[i].title_offset[1]                 = -3;
        style.states[i].title_scale[0]                  = 1.f;
        style.states[i].title_scale[1]                  = 1.f;
        style.states[i].font                            = &gui.default_font;
        style.states[i].tex                             = 0;
        style.states[i].tex_flip                        = GUI_FLIP_NONE;
        gui_set_color_array(style.states[i].title_color, 255, 255, 255, 255);
        memset(style.states[i].tex_clip, 0, sizeof(style.states[i].tex_clip));
    }
    return style;
}

gui_text_input_style_t
gui_create_text_input_style(void)
{
    gui_text_input_style_t s;
    gui_set_color_array(s.states[GUI_TEXT_INPUT_STATE_INACTIVE].background_color,
        5,   5,  5, 255);
    gui_set_color_array(s.states[GUI_TEXT_INPUT_STATE_INACTIVE].border.color,
        13, 13, 13, 255);
    gui_set_color_array(s.states[GUI_TEXT_INPUT_STATE_HOVERED].background_color,
        8, 8, 8, 255);
    gui_set_color_array(s.states[GUI_TEXT_INPUT_STATE_HOVERED].border.color,
        23, 23, 23, 255);
    gui_set_color_array(s.states[GUI_TEXT_INPUT_STATE_ACTIVE].background_color,
        13, 13, 13, 255);
    gui_set_color_array(s.states[GUI_TEXT_INPUT_STATE_ACTIVE].border.color,
        33,  33, 33, 255);
    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 4; ++j)
            s.states[i].border.widths[j]    = 2;
        s.states[i].title_origin            = GUI_CENTER_LEFT;
        s.states[i].title_font              = &gui.default_font;
        s.states[i].input_font              = &gui.default_font;
        s.states[i].title_scale             = 1.f;
        s.states[i].input_scale             = 1.f;
        s.states[i].input_pixel_offset[0]   = 2;
        s.states[i].input_pixel_offset[1]   = 0;
        s.states[i].title_edge              = GUI_EDGE_TOP;
        s.states[i].title_origin            = GUI_BOTTOM_LEFT;
        s.states[i].title_pixel_offset[0]   = 2;
        s.states[i].title_pixel_offset[1]   = 4;
    }
    for (int i = 0; i < 2; ++i)
    {
        gui_set_color_array(s.states[i].title_color, 128, 128, 128, 255);
        gui_set_color_array(s.states[i].input_color, 128, 128, 128, 255);
    }
    gui_set_color_array(s.states[GUI_TEXT_INPUT_STATE_ACTIVE].title_color,
        255, 255, 255, 255);
    gui_set_color_array(s.states[GUI_TEXT_INPUT_STATE_ACTIVE].input_color,
        255, 255, 255, 255);
    return s;
}

gui_progress_bar_style_t
gui_create_progress_bar_style(void)
{
    gui_progress_bar_style_t s = {0};
    for (int i = 0; i < 3; ++i)
    {
        gui_progress_bar_state_style_t *ss = &s.states[i];
        gui_set_color_array(ss->border.color, 0, 0, 0, 255);
        for (int j = 0; j < 4; ++j)
            ss->border.widths[j] = 2;
        gui_set_color_array(ss->border.color, 23, 23, 23, 255);
        gui_set_color_array(ss->background.color, 5, 5, 5, 255);
        ss->title_font                  = &gui.default_font;
        ss->progress_font               = &gui.default_font;
        ss->title_scale                 = 1.0f;
        ss->title_origin                = GUI_CENTER_CENTER;
        ss->progress_scale              = 1.0f;
        ss->progress_pixel_offset[0]    = 0;
        ss->progress_pixel_offset[1]    = 0;
        ss->progress_origin             = GUI_BOTTOM_LEFT;
        ss->progress_style = GUI_PROGRESS_BAR_PROGRESS_STYLE_X_SLASH_Y;
        gui_set_color_array(ss->fill.color, 0, 255, 0, 255);
        gui_set_color_array(ss->title_color, 255, 255, 255, 255);
        gui_set_color_array(ss->progress_color, 255, 255, 255, 255);
    }
    return s;
}

void
gui_compute_text_size(int *w, int *h, const char *text, gui_font_t *font,
    float scale, int wrap, int window_w, int window_h)
{
    int rw, rh;
    if (text && font)
    {
        float q_wrap = (float)(wrap && wrap < window_w ? wrap : window_w);
        _gui_compute_text_dimensions(text, font, q_wrap, scale, scale, &rw, &rh);
    } else
    {
        rw = 0;
        rh = 0;
    }
    if  (w)
        *w = rw;
    if (h)
        *h = rh;
}

void
gui_compute_window_viewport_size(gui_win_state_style_t *state_style,
    int win_w, int win_h, int *ret_w, int *ret_h)
{
    if (!state_style)
    {
        *ret_w = win_w;
        *ret_h = win_h;
        return;
    }
    *ret_w = win_w - state_style->border.widths[GUI_EDGE_LEFT] -
        state_style->border.widths[GUI_EDGE_RIGHT];
    *ret_h = win_h - state_style->border.widths[GUI_EDGE_TOP] -
        state_style->border.widths[GUI_EDGE_BOTTOM];
}

static uint32_t
_gui_graphic_button(uint32_t id, const char *title, int x, int y, int w, int h,
    uint32_t mbtn_mask)
{
    gui_win_t   *p;
    int         tx, ty;
    uint32_t ret = _gui_button_internal(id, x, y, w, h, mbtn_mask, &tx, &ty, &p,
        1);
    /*-- Render --*/
    gui_button_state_style_t *ss;
    if (gui.pressed_button_id == id)
        ss = &gui.button_style->states[GUI_BUTTON_STATE_PRESSED];
    else if (gui.hovered_button == id)
        ss = &gui.button_style->states[GUI_BUTTON_STATE_HOVERED];
    else
        ss = &gui.button_style->states[GUI_BUTTON_STATE_NORMAL];
    int scissor[4];
    _gui_get_win_inner_scissor(p, scissor);
    /*-- Border --*/
    _gui_win_draw_quad(p, tx, ty, w, h, 0, 0, ss->border.color, GUI_FLIP_NONE,
        0, scissor);
    /*-- Background --*/
    _gui_win_draw_quad(p, tx + ss->border.widths[GUI_EDGE_LEFT],
        ty + ss->border.widths[GUI_EDGE_TOP],
        w - ss->border.widths[GUI_EDGE_LEFT] - ss->border.widths[GUI_EDGE_RIGHT],
        h - ss->border.widths[GUI_EDGE_TOP] - ss->border.widths[GUI_EDGE_BOTTOM],
        0, 0, ss->background_color, GUI_FLIP_NONE, 0, scissor);
    if (ss->tex)
        _gui_win_draw_quad(p, tx, ty, w, h, ss->tex, ss->tex_clip,
            gui_color_white, ss->tex_flip, 0, scissor);
    /*-- Title --*/
    int rect[4] =
    {
        tx + ss->border.widths[GUI_EDGE_LEFT],
        ty + ss->border.widths[GUI_EDGE_TOP],
        w - ss->border.widths[GUI_EDGE_RIGHT] - ss->border.widths[GUI_EDGE_LEFT],
        h - ss->border.widths[GUI_EDGE_BOTTOM] - ss->border.widths[GUI_EDGE_TOP]
    };
    _gui_draw_con_draw_title_text(&p->draw_con, title, ss->font,
        ss->title_offset[0], ss->title_offset[1], rect, ss->title_origin,
        ss->title_scale[0], ss->title_scale[1], ss->title_color, scissor);
    return ret;
}

static gui_win_t *
_gui_get_win_by_id(uint32_t id)
{
    gui_win_t *wins = gui.wins;
    for (int i = 0; i < gui.win_num; ++i)
        if (wins[i].id == id)
            return &wins[i];
    return 0;
}

static gui_win_t *
_gui_create_win(uint32_t id)
{
    if (gui.win_num == GUI_MAX_WINS)
        return 0;
    gui_win_t *win = &gui.wins[gui.win_num];
    if (_gui_draw_con_init(&win->draw_con, 16))
        return 0;
    win->last_hovered           = 0;
    win->last_active            = 0;
    win->is_parent_of_act_win   = 0;
    win->content_wh[0]          = 0;
    win->content_wh[1]          = 0;
    win->scroll_percentage[0]   = 0.f;
    win->scroll_percentage[1]   = 0.f;
    win->scroll[0]              = 0;
    win->scroll[1]              = 0;
    win->id                     = id;
    gui.win_num++;
    return win;
}

static void
_gui_compute_quad_parented_xy(int *ret_x, int *ret_y, enum gui_origin o,
    int p[4], int x,  int y,  int w,  int h)
{
    switch (o)
    {
    case GUI_TOP_LEFT:
        *ret_x = p[0] + x;
        *ret_y = p[1] + y;
        break;
    case GUI_TOP_RIGHT:
        *ret_x = p[0] + p[2] - w - x;
        *ret_y = p[1] + y;
        break;
    case GUI_TOP_CENTER:
        *ret_x = p[0] + p[2] / 2 - w / 2 + x;
        *ret_y = p[1] + y;
        break;
    case GUI_BOTTOM_LEFT:
        *ret_x = p[0] + x;
        *ret_y = p[1] + p[3] - y - h;
        break;
    case GUI_BOTTOM_RIGHT:
        *ret_x = p[0] + p[2] - w - x;
        *ret_y = p[1] + p[3] - y - h;
        break;
    case GUI_BOTTOM_CENTER:
        *ret_x = p[0] + p[2] / 2 - w / 2 + x;
        *ret_y = p[1] + p[3] - y - h;
        break;
    case GUI_CENTER_LEFT:
        *ret_x = p[0] + x;
        *ret_y = p[1] + p[3] / 2 - h / 2 + y;
        break;
    case GUI_CENTER_RIGHT:
        *ret_x = p[0] + p[2] - w - x;
        *ret_y = p[1] + p[3] / 2 - h / 2 + y;
        break;
    case GUI_CENTER_CENTER:
        *ret_x = p[0] + p[2] / 2 - w / 2 + x;
        *ret_y = p[1] + p[3] / 2 - h / 2 + y;
        break;
    default:
        gui_assert(0);
    }
}

static int
_gui_win_draw_quad(gui_win_t *win, int x, int y, int w, int h, void *tex,
    float *clip, uint8_t *color, enum gui_flip flip, float rot, int scissor[4])
{
    return _gui_win_draw_quad_f(win, (float)x, (float)y, (float)w, (float)h,
        tex, clip, color, flip, rot, scissor);
}

static int
_gui_win_draw_quad_f(gui_win_t *win, float x, float y, float w, float h,
    void *tex, float *clip, uint8_t *color, enum gui_flip flip, float rot,
    int scissor[4])
{
    if (!color[3])
        return 0;
    float *verts = _gui_draw_con_push_verts(&win->draw_con, 4);
    if (!verts)
        return 1;
    _gui_write_quad_verts(verts, tex, clip, x, y, w, h, color, flip);
    _gui_rotate_quad(verts, x, y, w, h, rot);
    if (_gui_draw_con_push_draw_cmd(&win->draw_con, 4, tex, scissor))
        return 2;
    return 0;
}

static int
_gui_draw_con_draw_text(gui_draw_con_t *con, const char *txt, gui_font_t *font,
    float x, float y, int *parent_dim, int origin, int wrap,
    float sx, float sy, uint8_t *col, int *ret_rect, int scissor[4])
{
    int len = _gui_strlen(txt);
    if (_gui_draw_con_push_draw_cmd(con, len * 4, font->tex, scissor))
        return 2;
    float *verts = _gui_draw_con_push_verts(con, len * 4);
    if (!verts)
        return 1;
    float bx, by;
    if (origin == GUI_TOP_LEFT)
    {
        bx = (float)parent_dim[0] + x;
        by = (float)parent_dim[1] + y;
    } else
    {
        /* If there's no wrapping, wrap to parent dimensions */
        float quad_wrap = (float)(wrap && wrap < parent_dim[2] ? wrap :
            parent_dim[2]);
        int w, h, tmp_x, tmp_y;
        _gui_compute_text_dimensions(txt, font, quad_wrap, sx, sy, &w, &h);
        _gui_compute_quad_parented_xy(&tmp_x, &tmp_y, origin,
            parent_dim, (int)x, (int)y,  w, h);
        bx = (float)tmp_x;
        by = (float)tmp_y;
    }
    gui_glyph_t *g;
    float       rx, ry, w, h;
    void        *tex    = font->tex;
    float       max_x   = wrap && wrap < parent_dim[2] ? bx + (float)wrap :
        bx + (float)parent_dim[2];
    float           *write_verts    = verts;
    int             num_drawn       = 0;
    float           px              = bx;
    float           py              = by;
    float           v_adv           = font->vertical_advance * sy;
    float           rect_w          = 0.f;
    float           font_height     = sy * (float)font->height;
    for (const char *c = txt; *c; ++c)
    {
        if (*c == '\n')
        {
            if (px > rect_w)
                rect_w = px;
            px = bx;
            py += v_adv;
            /* Break out if max vertical height is reached */
            /* Note: can't do this before we can resize vert containers */
            /*if (py + v_adv > max_y)*/
                /*break;*/
            continue;
        }
        g   = _gui_font_get_glyph(font, (int)*c);
        w   = sx * (g->clip[2] - g->clip[0]);
        h   = sy * (g->clip[3] - g->clip[1]);
        if (c != txt && px + (sx * g->advance) > max_x)
        {
            if (px > rect_w)
                rect_w = px;
            px = bx;
            py += v_adv;
        }
        rx  = px;
        ry  = (py + (font_height - h) + g->y_offset * sy);
        float tw, th;
        _gui_get_tex_w_h(tex, &tw, &th);
        _gui_write_tex_quad_verts(write_verts, tex, g->clip, rx, ry, w, h, col);
        px += sx * g->advance;
        num_drawn++;
        write_verts = verts + (num_drawn * GUI_NUM_FLOATS_PER_QUAD);
    }
    gui_assert(num_drawn == len);
    if (ret_rect)
    {
        ret_rect[0] = (int)x;
        ret_rect[1] = (int)y;
        ret_rect[2] = wrap ? wrap : (int)rect_w;
        ret_rect[3] = (int)(py + v_adv - by);
    }
    return 0;
}

static int
_gui_draw_con_draw_title_text(gui_draw_con_t *con, const char *text,
    gui_font_t *font, int x, int y, int parent_dim[4], int origin, float sx,
    float sy, uint8_t col[4], int scissor[4])
{
    if (!font || !col[3]) /* Transparent or non-existing font */
        return 0;
    int     len     = _gui_title_strlen(text);
    float   *verts  = _gui_draw_con_reserve_verts(con, len * 4);
    float   bx, by;
    if (origin == GUI_TOP_LEFT)
    {
        bx = (float)parent_dim[0] + (float)x;
        by = (float)parent_dim[1] + (float)y;
    } else
    {
        int w, h, tmp_x, tmp_y;
        w = _gui_compute_title_text_line_w(text, font, sx);
        h = (int)(sy * (float)font->vertical_advance);
        _gui_compute_quad_parented_xy(&tmp_x, &tmp_y, origin, parent_dim, x, y,
             w, h);
        bx = (float)tmp_x;
        by = (float)tmp_y;
    }
    void            *tex            = font->tex;
    float           *write_verts    = verts;
    float           px              = bx;
    float           py              = by;
    int             num_drawn       = 0;
    float           font_h          = sy * (float)font->height;
    float           rx, ry, w, h;
    gui_glyph_t     *g;
    for (const char *c = text;
         *c && *c != '\n' && !(*c == '#' && *(c + 1) == '#');
         ++c)
    {
        g   = _gui_font_get_glyph(font, (int)*c);
        w   = sx * (g->clip[2] - g->clip[0]);
        h   = sy * (g->clip[3] - g->clip[1]);
        rx  = px;
        ry  = py + (font_h - h) + g->y_offset * sy;
        _gui_write_tex_quad_verts(write_verts, tex, g->clip, rx, ry, w, h, col);
        write_verts = verts + (++num_drawn) * GUI_NUM_FLOATS_PER_QUAD;
        px += sx * g->advance;
    }
    _gui_draw_con_push_reserved_verts(con, num_drawn * 4);
    _gui_draw_con_push_draw_cmd(con, num_drawn * 4, font->tex, scissor);
    return 0;
}

static int
_gui_draw_con_draw_input_text(gui_draw_con_t *con, gui_text_input_t *ti,
    const char *txt, gui_bool_t is_active, gui_font_t *font, float offset_x,
    float offset_y, int *parent_dim, float sx, float sy, uint8_t *col,
    int scissor[4], int flags)
{
    int     len     = (int)strlen(txt); /* Add one for cursor. */
    float   *verts  = _gui_draw_con_reserve_verts(con, (len + 1) * 4);
    float   bx      = (float)parent_dim[0] + offset_x;
    float   by      = (float)(parent_dim[1] + parent_dim[3] / 2) - sy * 0.5f *
        font->vertical_advance + offset_y;
    void            *tex            = font->tex;
    float           *write_verts    = verts;
    float           px              = bx;
    float           py              = by;
    int             num_drawn       = 0;
    float           min_x           = (float)parent_dim[0];
    float           max_x           = (float)(parent_dim[0] + parent_dim[2]);
    float           cursor_px       = bx;
    int             cursor_index    = ti->cursor_index;;
    float           rx, ry, w, h;
    float           font_h          = sy * (float)font->height;
    gui_glyph_t     *g;
    char            c;
    gui_glyph_t     *glyphs;
    int             num_glyphs;
    if (flags & GUI_TEXT_INPUT_FLAG_PASSWORD)
    {
        glyphs      = _gui_font_get_glyph(font, (int)'*');
        num_glyphs  = 1;
    } else
    {
        glyphs      = font->glyphs;
        num_glyphs  = font->num_glyphs;
    }
    /*-- Make sure the cursor is visible --*/
    if (ti->left_index > cursor_index)
    {
        float   quarter     = 0.25f * (float)parent_dim[2];
        float   num_px      = 0;
        int     left_index  = ti->left_index;
        while (num_px < quarter && left_index)
        {
            c = txt[--left_index];
            g = _gui_get_glyph(glyphs, num_glyphs, (int)c);
            num_px += (g->advance * sx);
        }
        ti->left_index = left_index;
    }
    for (int i = ti->left_index; i < cursor_index; ++i)
    {
        c = txt[i];
        g = _gui_get_glyph(glyphs, num_glyphs, (int)c);
        cursor_px += (g->advance * sx);
    }
    if (cursor_px < min_x)
    {
        cursor_px       = min_x;
        ti->left_index  = cursor_index;
    } else
    if (cursor_px > max_x)
    {
        float   sub = cursor_px - max_x;
        float   cur = 0;
        int     i           = 0;
        for (i = ti->left_index; i < cursor_index && cur < sub; ++i)
        {
            c = txt[i];
            g = _gui_get_glyph(glyphs, num_glyphs, (int)c);
            cur += (g->advance * sx);
        }
        ti->left_index = i;
    }
    for (int i = ti->left_index; i < len; ++i)
    {
        c   = txt[i];
        g   = _gui_get_glyph(glyphs, num_glyphs, (int)c);
        w   = (g->clip[2] - g->clip[0]) * sx;
        if (px + w > max_x)
            break;
        h   = (g->clip[3] - g->clip[1]) * sy;
        rx  = px;
        ry  = (py + (font_h - h) + g->y_offset * sy);
        if (rx >= min_x)
        {
            _gui_write_tex_quad_verts(write_verts, tex, g->clip, rx, ry, w, h,
                col);
            write_verts = verts + (++num_drawn * GUI_NUM_FLOATS_PER_QUAD);
        }
        px += (g->advance * sx);
    }
    if (is_active && gui.show_text_input_cursor)
    {
        g   = _gui_font_get_glyph(font, (int)'|');
        w   = (g->clip[2] - g->clip[0]) * sx;
        h   = (g->clip[3] - g->clip[1]) * sy;
        ry  = (py + (font_h - h) + g->y_offset * sy);
        if (cursor_px - w * 0.5f < min_x)
            cursor_px = min_x + w * 0.5f;
        else if (cursor_px + w * 0.5f > max_x)
            cursor_px = max_x - w * 0.5f;
        else
            cursor_px = cursor_px - w * 0.5f;
        _gui_write_tex_quad_verts(write_verts, tex, g->clip, cursor_px,
            ry, w, h, col);
        num_drawn++;
    }
    _gui_draw_con_push_reserved_verts(con, num_drawn * 4);
    _gui_draw_con_push_draw_cmd(con, num_drawn * 4, font->tex, scissor);
    return 0;
}

static void
_gui_write_quad_verts(float *verts, void *tex, float *tex_clip,
    float x, float y, float w, float h, uint8_t *color, enum gui_flip flip)
{
    uint8_t *col = color ? color : gui_color_white;
    if (tex)
    {
        float full_clip[4];
        float *clip;
        if (tex_clip)
            clip = tex_clip;
        else
        {
            float tex_w, tex_h;
            _gui_get_tex_w_h(tex, &tex_w, &tex_h);
            full_clip[0] = x;
            full_clip[1] = y;
            full_clip[2] = tex_w;
            full_clip[3] = tex_h;
            clip = full_clip;
        }
        _gui_write_flipped_tex_quad_verts(verts, tex, clip, x, y, w, h, col,
            flip);
    } else
    {
        verts[ 0] = x;
        verts[ 1] = y;
        verts[ 5] = x + w;
        verts[ 6] = y;
        verts[10] = x;
        verts[11] = y + h;
        verts[15] = x + w;
        verts[16] = y + h;
        uint8_t *bverts = (uint8_t*)verts;
        bverts[16] = col[0];
        bverts[17] = col[1];
        bverts[18] = col[2];
        bverts[19] = col[3];
        bverts[36] = col[0];
        bverts[37] = col[1];
        bverts[38] = col[2];
        bverts[39] = col[3];
        bverts[56] = col[0];
        bverts[57] = col[1];
        bverts[58] = col[2];
        bverts[59] = col[3];
        bverts[76] = col[0];
        bverts[77] = col[1];
        bverts[78] = col[2];
        bverts[79] = col[3];
    }
}

static void
_gui_sort_win_children(gui_win_t *win)
{
    gui_assert(win->child_list_index >= 0);
    int         **child_list     = &gui.win_child_lists[win->child_list_index];
    int         **child_indices  = child_list;

    int         num_children    = (int)darr_num(*child_list);
    int         end             = num_children - 1;
    gui_bool_t      swapped         = 1;
    int         i, tmp;
    gui_win_t   *c, *oc;
    while (swapped)
    {
        swapped = 0;
        for (i = 0; i < end; ++i)
        {
            c   = &gui.wins[(*child_list)[i]];
            oc  = &gui.wins[(*child_list)[i + 1]];
            gui_assert(c != oc);
            if (oc->last_active >= c->last_active)
                continue;
            tmp                     = (*child_list)[i];
            (*child_list)[i]        = (*child_list)[i + 1];
            (*child_list)[i + 1]    = tmp;
            swapped                 = 1;
        }
    }
    for (i = 0; i < num_children; ++i)
    {
        c = &gui.wins[(*child_indices)[i]];
        gui_assert(c != win);
        child_list = _gui_get_win_child_indices(c);
        if (child_list && darr_num(*child_list) > 1)
            _gui_sort_win_children(c);
    }
}

static void
_gui_update_and_add_win_to_draw_list(gui_win_t *win, gui_win_t *all_wins,
    gui_draw_list_t *lists, int *num_lists)
{
    /* Update scroll percentage */
    if (win->content_wh[0])
        win->scroll_percentage[0] = (float)-win->scroll[0] /
            (float)(-win->content_wh[0] + _compute_win_viewport_w(win));
    else
        win->scroll_percentage[0] = 0.f;
    if (win->content_wh[1])
        win->scroll_percentage[1] = (float)-win->scroll[1] /
            (float)-(-win->content_wh[1] + _compute_win_viewport_h(win));
    else
        win->scroll_percentage[1] = 0.f;
    for (int i = 0; i < 2; ++i) /* Clamp values */
    {
        if (win->scroll_percentage[i] < 0.f)
            win->scroll_percentage[i] = 0.f;
        if (win->scroll_percentage[i] > 1.f)
            win->scroll_percentage[i] = 1.f;
    }
    /* Clamp scrolling to window content size. */
    if (win->flags & GUI_WIN_SCROLLABLE)
    {
        for (int i = 0; i < 2; ++i)
            if (win->scroll[i] > 0)
                win->scroll[i] = 0;
        int viewport[4];
        _gui_compute_win_viewport_rect(win, viewport);
        int min_scroll[2] = {
            -(win->content_wh[0] - viewport[2]),
            -(win->content_wh[1] - viewport[3])};
        for (int i = 0; i < 2; ++i)
            if (min_scroll[i] > 0)
                min_scroll[i] = 0;
        for (int i = 0; i < 2; ++i)
            if (win->scroll[i] < min_scroll[i])
                win->scroll[i] = min_scroll[i];
    }
    /* Add to draw list */
    int index = *num_lists;
    if (darr_num(win->draw_con.vert_floats) > 0)
    {
        gui_win_state_style_t *s = win->style;
        int border[4];
        if (s)
        {
            border[0] = s->border.widths[GUI_EDGE_LEFT];
            border[1] = s->border.widths[GUI_EDGE_TOP];
            border[2] = s->border.widths[GUI_EDGE_RIGHT];
            border[3] = s->border.widths[GUI_EDGE_BOTTOM];
        } else
            memset(border, 0, sizeof(border));
        lists[index].cmds           = win->draw_con.draw_cmds;
        lists[index].num_cmds       = darr_num(win->draw_con.draw_cmds);
        lists[index].verts          = win->draw_con.vert_floats;
        lists[index].num_verts      =
            darr_num(win->draw_con.vert_floats) / GUI_NUM_FLOATS_PER_QUAD;
        *num_lists = index + 1;
    }
    int **child_list = _gui_get_win_child_indices(win);
    if (!child_list)
        return;
    uint32_t num = darr_num(*child_list);
    for (uint32_t i = 0; i < num; ++i)
        _gui_update_and_add_win_to_draw_list(&all_wins[(*child_list)[i]],
            all_wins, lists, num_lists);
}

static inline gui_bool_t
_gui_test_next_active_win_parenthood(void)
{
    for (int i = 0; i < gui.win_stack_num; ++i)
        if (gui.win_stack[i] == gui.next_act_win.index)
            return 1;
    return 0;
}

static inline gui_bool_t
_gui_test_next_hovered_win_parenthood(void)
{
    for (int i = 0; i < gui.win_stack_num; ++i)
        if (gui.win_stack[i] == gui.next_hov_win.index)
            return 1;
    return 0;
}

int
_gui_draw_con_init(gui_draw_con_t *con, uint32_t num_cmds)
{
    darr_reserve(con->draw_cmds, num_cmds);
    darr_reserve(con->vert_floats, num_cmds * 16 * GUI_NUM_FLOATS_PER_QUAD);
    return 0;
}

static inline void
_gui_draw_con_clear(gui_draw_con_t *con)
{
    darr_clear(con->draw_cmds);
    darr_clear(con->vert_floats);
}

static float *
_gui_draw_con_push_verts(gui_draw_con_t *con, int num_verts)
{
    int index   = darr_num(con->vert_floats);
    int num_new = num_verts * GUI_NUM_FLOATS_PER_VERT;
    int new_num = index + num_new;
    darr_reserve(con->vert_floats, (uint32_t)new_num);
    _darr_head(con->vert_floats)->num = new_num;
    return con->vert_floats + index;
}

static float *
_gui_draw_con_reserve_verts(gui_draw_con_t *con, int num_verts)
{
    uint32_t index    = darr_num(con->vert_floats);
    uint32_t num_new  = num_verts * GUI_NUM_FLOATS_PER_VERT;
    uint32_t new_num = index + num_new;
    darr_reserve(con->vert_floats, new_num);
    return con->vert_floats + index;

}

static void
_gui_draw_con_push_reserved_verts(gui_draw_con_t *con, int num_verts)
{
    uint32_t num_now = _darr_head(con->vert_floats)->num;
    uint32_t num_new = num_verts * GUI_NUM_FLOATS_PER_VERT;
    _darr_head(con->vert_floats)->num = num_now + num_new;
    gui_assert(_darr_head(con->vert_floats)->num <=
        _darr_head(con->vert_floats)->cap);
}

static int
_gui_draw_con_push_draw_cmd(gui_draw_con_t *con, int num_verts, void *tex,
    int scissor[4])
{
    /* Can always count on there being room for at least one */
    if (!darr_num(con->draw_cmds))
    {
        gui_draw_cmd_t cmd;
        cmd.tex         = tex;
        cmd.num_verts   = num_verts;
        memcpy(cmd.scissor, scissor, 4 * sizeof(int));
        darr_push(con->draw_cmds, cmd);
        return 0;
    }
    int index           = darr_num(con->draw_cmds) - 1;
    int scissor_is_same = 1;
    for (int i = 0; i < 4; ++i)
    {
        if (scissor[i] == con->draw_cmds[index].scissor[i])
            continue;
        scissor_is_same = 0;
        break;
    }

    if (con->draw_cmds[index].tex == tex && scissor_is_same)
    {
        con->draw_cmds[index].num_verts += num_verts;
        return 0;
    }
    gui_draw_cmd_t cmd;
    cmd.tex         = tex;
    cmd.num_verts   = num_verts;
    memcpy(cmd.scissor, scissor, 4 * sizeof(int));
    darr_push(con->draw_cmds, cmd);
    return 0;
}

static int
_gui_compute_title_text_line_w(const char *txt, gui_font_t *f, float x_scale)
{
    float w = 0;
    for (const char *c = txt;
         *c && *c != '\n' && !(*c == '#' && c > txt && *(c - 1) == '#');
         ++c)
        w += x_scale * _gui_font_get_glyph(f, (int)*c)->advance;
    return (int)w;
}

static void
_gui_compute_text_dimensions(const char *txt, gui_font_t *font, float wrap,
    float sx, float sy, int *ret_w, int *ret_h)
{
    float       v_adv         = sy * font->vertical_advance;
    float       longest_line  = 0;
    float       w             = 0;
    float       h             = txt[0] ? v_adv : 0;
    float       h_adv;
    gui_glyph_t *g;
    for (const char *c = txt; *c; ++c)
    {
        if (*c == '\n')
        {
            if (w > longest_line)
                longest_line = w;
            w = 0;
            h += v_adv;
            continue;
        }
        g       = _gui_font_get_glyph(font, (int)*c);
        h_adv   = g->advance * sx;
        if (w + h_adv > wrap && c != txt)
        {
            if (w > longest_line)
                longest_line = w;
            w = 0;
            h += v_adv;
        }
        w += h_adv;
    }
    *ret_w = (int)(longest_line > w ? longest_line : w);
    *ret_h = (int)h;
}

static inline int
_gui_strlen(const char *txt)
{
    int len = 0;
    if (txt) {for (const char *c = txt; *c; ++c) {if (*c != '\n') ++len;}}
    return len;
}

static inline int
_gui_title_strlen(const char *txt)
{
    int len = 0;
    if (!txt)
        return len;
    for (const char *c = txt;
         *c && *c != '\n' && !(*c == '#' && *(c + 1) == '#');
         ++c)
        ++len;
    return len;
}

static int
_gui_realloc_fmt_buf(int new_size)
{
    int suggested_sz = gui.fmt_buf_size * 2;
    if (suggested_sz > new_size)
        new_size = suggested_sz;
    gui_assert(new_size > gui.fmt_buf_size);
    char *m = gui_realloc(gui.fmt_buf, new_size);
    if (!m)
        return 1;
    gui.fmt_buf = m;
    gui.fmt_buf_size = new_size;
    return 0;
}

static uint32_t
_gui_button_internal(uint32_t id, int x, int y, int w, int h,
    uint32_t mbtn_mask, int *ret_tx, int *ret_ty, gui_win_t **ret_parent,
    gui_bool_t save_last_button_dimensions)
{
    int tx, ty;
    _gui_compute_quad_position(&tx, &ty, gui.origin, x, y, w, h);
    gui_win_t *p = _gui_get_win_stack_top();
    uint32_t ret = 0;
    if (_gui_test_mouse_rect(p->dim[0], p->dim[1], p->dim[2], p->dim[3]) && _gui_test_mouse_rect(tx, ty, w, h))
    {
        uint32_t t_mbtn_mask = mbtn_mask == 0 ? GUI_MOUSE_BUTTON_LEFT : mbtn_mask;
        /*-- Mouse was released on top of this button --*/
        if (gui.pressed_button_id == id &&
            GUI_MOUSE_BUTTON_UP_NOW(gui.pressed_button_mbtn))
        {
            ret = gui.mbtns_up_now & gui.pressed_button_mbtn;
            gui.released_button_id = id;
        } else /* Mouse went down on top of this button */
        if ((t_mbtn_mask & gui.mbtns_down_now)
        &&  (gui.next_prs_btn.id == 0
             || gui.next_prs_btn.last_active <= p->last_active))
        {
            gui.next_prs_btn.id                 = id;
            gui.next_prs_btn.parent_id          = p->id;
            gui.next_prs_btn.last_active        = p->last_active;
            gui.next_prs_btn.mouse_btn          = t_mbtn_mask;
            gui.clicked_this_frame              = 1;
            gui.pressed_element_button_flags    = gui.input_state.mouse_buttons;
        } else
        if (gui.next_hov_btn.id == 0
        || (gui.next_hov_btn.last_active <= p->last_active))
        {
            gui.next_hov_btn.id             = id;
            gui.next_hov_btn.parent_id      = p->id;
            gui.next_hov_btn.last_active    = p->last_active;
        }
        gui.hovered_this_frame = 1;
    }
    if (ret_tx)
        *ret_tx = tx;
    if (ret_ty)
        *ret_ty = ty;
    if (ret_parent)
        *ret_parent = p;
    if (save_last_button_dimensions)
    {
        gui.last_button_origin          = gui.origin;
        gui.last_button_dimensions[0]   = x;
        gui.last_button_dimensions[1]   = y;
        gui.last_button_dimensions[2]   = w;
        gui.last_button_dimensions[3]   = h;
    }
    _gui_save_parent_content_wh(p, x, y, w, h, gui.origin);
    gui.last_drawn_button_id = id;
    return ret;
}

/* Returns < 0 on failure */
static int
_gui_claim_win_child_list(void)
{
    int num = darr_num(gui.win_child_lists);
    if (num == GUI_MAX_WINS)
        return -1;
    darr_clear(gui.win_child_lists[num]);
    _darr_head(gui.win_child_lists)->num = num + 1;
    return num;
}

static inline void
_gui_get_win_inner_scissor(gui_win_t *win, int scissor[4])
{
    gui_win_state_style_t *s = win->style;
    if (win->style)
    {
        scissor[0] = win->dim[0] + s->border.widths[GUI_EDGE_LEFT];
        scissor[1] = win->dim[1] + s->border.widths[GUI_EDGE_TOP];
        scissor[2] = win->dim[2] - s->border.widths[GUI_EDGE_LEFT] - s->border.widths[GUI_EDGE_RIGHT];
        scissor[3] = win->dim[3] - s->border.widths[GUI_EDGE_TOP] - s->border.widths[GUI_EDGE_BOTTOM];
    } else
        memcpy(scissor, win->dim, 4 * sizeof(int));
}

static gui_text_input_t *
_gui_get_text_input_by_id(uint32_t id)
{
    gui_text_input_t *inputs = gui.text_inputs;
    uint32_t num = darr_num(inputs);
    for (uint32_t i = 0; i < num; ++i)
        if (inputs[i].id == id)
            return &inputs[i];
    return 0;
}

static gui_text_input_t *
_gui_create_text_input(uint32_t id)
{
    gui_text_input_t ti;
    ti.id           = id;
    ti.cursor_index = 0;
    ti.left_index   = 0;
    darr_push(gui.text_inputs, ti);
    return &gui.text_inputs[_darr_head(gui.text_inputs)->num - 1];
}

static int
_gui_find_cursor_right_jump_index(char *buf, int cursor_index)
{
    if (!buf[cursor_index])
        return cursor_index;
    int     i       = cursor_index;
    gui_bool_t  spaces  = buf[cursor_index] == ' ' || buf[cursor_index] == '\t';
    if (spaces)
    {
        for (i = cursor_index; buf[i]; ++i)
            if (!(buf[i] == ' ' || buf[i] == '\t'))
                break;
    } else
        for (i = cursor_index; buf[i]; ++i)
            if (buf[i] == ' ' || buf[i] == '\t')
                break;
    return i;
}

static int
_gui_find_cursor_left_jump_index(char *buf, int cursor_index)
{
    if (!cursor_index)
        return cursor_index;
    int     i       = cursor_index;
    gui_bool_t  spaces  = buf[cursor_index - 1] == ' ' ||
        buf[cursor_index - 1] == '\t';
    if (spaces)
    {
        for (i = cursor_index - 1; i > 0; --i)
            if (!(buf[i - 1] == ' ' || buf[i - 1] == '\t'))
                break;
    } else
        for (i = cursor_index - 1; i > 0; --i)
            if (buf[i - 1] == ' ' || buf[i - 1] == '\t')
                break;
    return i;
}

static inline void
_gui_draw_single_border(float **verts, int num_blits, void *tex, float *clip,
    uint8_t *color, float x, float y, float cw, float ch, float x_increment,
    float y_increment)
{
    for (int i = 0; i < num_blits; ++i)
    {
        _gui_write_tex_quad_verts(*verts, tex, clip, x, y, cw, ch, color);
        x += x_increment;
        y += y_increment;
        (*verts) += GUI_NUM_FLOATS_PER_QUAD;
    }
}


static void
_gui_draw_con_draw_tex_border(gui_draw_con_t *con, void *tex,
    float clips[8][4], uint8_t *color, int *rect, int *scissor)
{
    float top_left_w      = clips[GUI_BORDER_TOP_LEFT][2] -
        clips[GUI_BORDER_TOP_LEFT][0];
    float top_right_w     = clips[GUI_BORDER_TOP_RIGHT][2] -
        clips[GUI_BORDER_TOP_RIGHT][0];
    float bottom_left_w   = clips[GUI_BORDER_BOTTOM_LEFT][2] -
        clips[GUI_BORDER_BOTTOM_LEFT][0];
    float bottom_right_w  = clips[GUI_BORDER_BOTTOM_RIGHT][2] -
        clips[GUI_BORDER_BOTTOM_RIGHT][0];
    float top_left_h      = clips[GUI_BORDER_TOP_LEFT][3] -
        clips[GUI_BORDER_TOP_LEFT][1];
    float bottom_left_h   = clips[GUI_BORDER_BOTTOM_LEFT][3] -
        clips[GUI_BORDER_BOTTOM_LEFT][1];
    float top_right_h     = clips[GUI_BORDER_TOP_RIGHT][3] -
        clips[GUI_BORDER_TOP_RIGHT][1];
    float bottom_right_h  = clips[GUI_BORDER_BOTTOM_RIGHT][3] -
        clips[GUI_BORDER_BOTTOM_RIGHT][1];
    float top_w       = clips[GUI_BORDER_TOP][2] - clips[GUI_BORDER_TOP][0];
    float top_h       = clips[GUI_BORDER_TOP][3] - clips[GUI_BORDER_TOP][1];
    float bottom_w = clips[GUI_BORDER_BOTTOM][2] - clips[GUI_BORDER_BOTTOM][0];
    float bottom_h = clips[GUI_BORDER_BOTTOM][3] - clips[GUI_BORDER_BOTTOM][1];
    float left_w      = clips[GUI_BORDER_LEFT][2] - clips[GUI_BORDER_LEFT][0];
    float left_h      = clips[GUI_BORDER_LEFT][3] - clips[GUI_BORDER_LEFT][1];
    float right_w     = clips[GUI_BORDER_RIGHT][2] - clips[GUI_BORDER_RIGHT][0];
    float right_h     = clips[GUI_BORDER_RIGHT][3] - clips[GUI_BORDER_RIGHT][1];
    float lt                = rect[2] - top_left_w - top_right_w;
    float lb                = rect[2] - bottom_left_w - bottom_right_w;
    float ll                = rect[3] - top_left_h - bottom_left_h;
    float lr                = rect[3] - top_right_h - bottom_right_h;
    float top_over_px       = 0.f;
    float left_over_px      = 0.f;
    float right_over_px     = 0.f;
    float bottom_over_px    = 0.f;
    int num_blits_top       = 0;
    int num_blits_bottom    = 0;
    int num_blits_left      = 0;
    int num_blits_right     = 0;
    int num_verts           = 16;
    if (top_w)
    {
        num_blits_top = (int)(lt / top_w);
        num_verts += num_blits_top * 4;
        if ((top_over_px = lt - (float)num_blits_top * top_w) > 0.f)
            num_verts += 4;
    }
    if (bottom_w)
    {
        num_blits_bottom = (int)(lb / bottom_w);
        num_verts += num_blits_bottom * 4;
        if ((bottom_over_px = lb - (float)num_blits_bottom * bottom_w) > 0.f)
            num_verts += 4;
    }
    if (left_h)
    {
        num_blits_left = (int)(ll / left_h);
        num_verts += num_blits_left * 4;
        if ((left_over_px = ll - (float)num_blits_left * left_h) > 0.f)
            num_verts += 4;
    }
    if (right_h)
    {
        num_blits_right = (int)(lr / right_h);
        num_verts += num_blits_right * 4;
        if ((right_over_px = lr - (float)num_blits_right * right_h) > 0.f)
            num_verts += 4;
    }
    _gui_draw_con_push_draw_cmd(con, num_verts, tex, scissor);
    float *verts = _gui_draw_con_push_verts(con, num_verts);
    _gui_draw_single_border(&verts, num_blits_top, tex, clips[GUI_BORDER_TOP],
        color, (float)(rect[0] + top_left_w), (float)rect[1], top_w,
        top_h, top_w, 0.f);
    if (top_over_px > 0.f)
    {
        float tmp_clip[4] = {clips[GUI_BORDER_TOP][0], clips[GUI_BORDER_TOP][1],
            clips[GUI_BORDER_TOP][2] - top_over_px, clips[GUI_BORDER_TOP][3]};
        float bx = (float)rect[0] + top_left_w + (float)num_blits_top * top_w;
        _gui_write_tex_quad_verts(verts, tex, tmp_clip, bx, (float)rect[1],
            top_w, top_h, color);
        verts += GUI_NUM_FLOATS_PER_QUAD;
    }
    _gui_draw_single_border(&verts, num_blits_left, tex, clips[GUI_BORDER_LEFT],
        color, (float)rect[0], (float)(rect[1] + top_left_h), (float)left_w,
        (float)left_h, 0, (float)left_h);
    if (left_over_px > 0.f)
    {
        float tmp_clip[4] = {clips[GUI_BORDER_LEFT][0],
            clips[GUI_BORDER_LEFT][1], clips[GUI_BORDER_LEFT][2],
            clips[GUI_BORDER_LEFT][3] - left_over_px};
        float by = (float)rect[1] + top_left_h + (float)num_blits_left * left_h;
        _gui_write_tex_quad_verts(verts, tex, tmp_clip, (float)rect[0], by,
            left_w, left_h, color);
        verts += GUI_NUM_FLOATS_PER_QUAD;
    }
    _gui_draw_single_border(&verts, num_blits_right, tex,
        clips[GUI_BORDER_RIGHT], color, (float)(rect[0] + rect[2] - right_w),
        (float)(rect[1] + top_right_h), (float)right_w, (float)right_h, 0,
        (float)right_h);
    if (right_over_px > 0.f)
    {
        float tmp_clip[4] = {clips[GUI_BORDER_RIGHT][0],
            clips[GUI_BORDER_RIGHT][1], clips[GUI_BORDER_RIGHT][2],
            clips[GUI_BORDER_RIGHT][3] - right_over_px};
        float by = (float)rect[1] + top_right_h + (float)num_blits_right *
            right_h;
        _gui_write_tex_quad_verts(verts, tex, tmp_clip,
            (float)(rect[0] + rect[2]) - right_w, by, right_w, right_h,
            color);
        verts += GUI_NUM_FLOATS_PER_QUAD;
    }
    _gui_draw_single_border(&verts, num_blits_bottom, tex,
        clips[GUI_BORDER_BOTTOM], color, (float)(rect[0] + bottom_left_w),
        (float)(rect[1] + rect[3] - bottom_right_h), (float)bottom_w,
        (float)bottom_h, (float)bottom_w, 0);
    if (bottom_over_px)
    {
        float tmp_clip[4] = {clips[GUI_BORDER_BOTTOM][0],
            clips[GUI_BORDER_BOTTOM][1],
            clips[GUI_BORDER_BOTTOM][2] - bottom_over_px,
            clips[GUI_BORDER_BOTTOM][3]};
        float bx = (float)rect[0] + bottom_left_w + (float)num_blits_bottom *
            bottom_w;
        _gui_write_tex_quad_verts(verts, tex, tmp_clip,
            bx, (float)(rect[1] + rect[3]) - bottom_h, bottom_w, bottom_h,
            color);
        verts += GUI_NUM_FLOATS_PER_QUAD;
    }
    _gui_write_tex_quad_verts(verts, tex, clips[GUI_BORDER_TOP_LEFT],
        (float)rect[0], (float)rect[1], (float)top_left_w, (float)top_left_h,
        color);
    verts += GUI_NUM_FLOATS_PER_QUAD;
    _gui_write_tex_quad_verts(verts, tex, clips[GUI_BORDER_TOP_RIGHT],
        (float)(rect[0] + rect[2] - top_right_w), (float)rect[1],
        (float)top_right_w, (float)top_right_h, color);
    verts += GUI_NUM_FLOATS_PER_QUAD;
    _gui_write_tex_quad_verts(verts, tex, clips[GUI_BORDER_BOTTOM_LEFT],
        (float)rect[0], (float)(rect[1] + rect[3] - bottom_left_h),
        (float)bottom_left_w, (float)bottom_left_h, color);
    verts += GUI_NUM_FLOATS_PER_QUAD;
    _gui_write_tex_quad_verts(verts, tex, clips[GUI_BORDER_BOTTOM_RIGHT],
        (float)(rect[0] + rect[2] - top_right_w),
        (float)(rect[1] + rect[3] - bottom_left_h), (float)top_right_w,
        (float)top_right_h, color);
}

static inline void
_gui_write_tex_quad_verts(float *verts, void *tex, float *clip, float x,
    float y, float w, float h, uint8_t *color)
{
    float tex_w;
    float tex_h;
    _gui_get_tex_w_h(tex, &tex_w, &tex_h);
    verts[ 0] = x;
    verts[ 1] = y;
    verts[ 5] = x + w;
    verts[ 6] = y;
    verts[10] = x;
    verts[11] = y + h;
    verts[15] = x + w;
    verts[16] = y + h;
    verts[ 2] = clip[0] / tex_w;
    verts[ 3] = clip[1] / tex_h;
    verts[ 7] = clip[2] / tex_w;
    verts[ 8] = clip[1] / tex_h;
    verts[12] = clip[0] / tex_w;
    verts[13] = clip[3] / tex_h;
    verts[17] = clip[2] / tex_w;
    verts[18] = clip[3] / tex_h;
    uint8_t* bverts = (uint8_t*)verts;
    bverts[16] = color[0];
    bverts[17] = color[1];
    bverts[18] = color[2];
    bverts[19] = color[3];
    bverts[36] = color[0];
    bverts[37] = color[1];
    bverts[38] = color[2];
    bverts[39] = color[3];
    bverts[56] = color[0];
    bverts[57] = color[1];
    bverts[58] = color[2];
    bverts[59] = color[3];
    bverts[76] = color[0];
    bverts[77] = color[1];
    bverts[78] = color[2];
    bverts[79] = color[3];
}

static void
_gui_write_flipped_tex_quad_verts(float *verts, void *tex, float *clip, float x,
    float y, float w, float h, uint8_t *color, int flip)
{
    float tex_w;
    float tex_h;
    _gui_get_tex_w_h(tex, &tex_w, &tex_h);
    switch (flip)
    {
        default:
        case GUI_FLIP_NONE:
            _gui_write_tex_quad_verts(verts, tex, clip, x, y, w, h, color);
            return;
        case GUI_FLIP_H:
            verts[ 0] = x + w;
            verts[ 1] = y;
            verts[ 5] = x;
            verts[ 6] = y;
            verts[10] = x + w;
            verts[11] = y + h;
            verts[15] = x;
            verts[16] = y + h;
            break;
        case GUI_FLIP_V:
            verts[ 0] = x;
            verts[ 1] = y + h;
            verts[ 5] = x + w;
            verts[ 6] = y + h;
            verts[10] = x;
            verts[11] = y;
            verts[15] = x + w;
            verts[16] = y;
            break;
        case GUI_FLIP_BOTH:
            verts[ 0] = x + w;
            verts[ 1] = y + h;
            verts[ 5] = x;
            verts[ 6] = y + h;
            verts[10] = x + w;
            verts[11] = y;
            verts[15] = x;
            verts[16] = y;
            break;
    }
    verts[ 2] = clip[0] / tex_w;
    verts[ 3] = clip[1] / tex_h;
    verts[ 7] = clip[2] / tex_w;
    verts[ 8] = clip[1] / tex_h;
    verts[12] = clip[0] / tex_w;
    verts[13] = clip[3] / tex_h;
    verts[17] = clip[2] / tex_w;
    verts[18] = clip[3] / tex_h;
    uint8_t *bverts = (uint8_t*)verts;
    bverts[16] = color[0];
    bverts[17] = color[1];
    bverts[18] = color[2];
    bverts[19] = color[3];
    bverts[36] = color[0];
    bverts[37] = color[1];
    bverts[38] = color[2];
    bverts[39] = color[3];
    bverts[56] = color[0];
    bverts[57] = color[1];
    bverts[58] = color[2];
    bverts[59] = color[3];
    bverts[76] = color[0];
    bverts[77] = color[1];
    bverts[78] = color[2];
    bverts[79] = color[3];
}

static inline void
_gui_get_tex_w_h(void *tex, float *ret_w, float *ret_h)
{
    int w, h;
    gui.init_config.get_texture_dimensions_callback(tex, &w, &h);
    *ret_w = (float)w;
    *ret_h = (float)h;
}

static uint8_t *
_gui_load_file_to_buffer(const char *path, size_t *ret_len)
{
    uint8_t       *buf        = 0;
    long int    num_chars   = 0;
    FILE        *file;
    file = fopen(path, "rb");
    if (!file)
        return buf;
    fseek(file, 0L, SEEK_END);
    num_chars = ftell(file);
    rewind(file);
    buf = (uint8_t*)gui_malloc(num_chars);
    if (buf)
    {
        fread(buf, sizeof(uint8_t), num_chars, file);
        fclose(file);
    }
    if (ret_len)
        *ret_len = num_chars;
    return buf;
}

static inline int
_gui_get_win_index(gui_win_t *win)
    {return (int)(size_t)(win - gui.wins);}

static inline gui_win_t *
_gui_get_win_stack_top()
{
    gui_win_t *ret = 0;
    if (gui.win_stack_num)
        ret = &gui.wins[gui.win_stack[gui.win_stack_num - 1]];
    return ret;
}

static inline int *
_gui_get_guide_stack_top()
    {return gui.guides_num ? &gui.guides[gui.guides_num - 1] : 0;}

static inline void
_gui_compute_win_viewport_rect(gui_win_t *win, int ret_arr[4])
{
    if (win->style)
    {
        ret_arr[0] = win->dim[0] + win->style->border.widths[GUI_EDGE_LEFT];
        ret_arr[1] = win->dim[1] + win->style->border.widths[GUI_EDGE_TOP];
        ret_arr[2] = win->dim[2] - win->style->border.widths[GUI_EDGE_LEFT] -
            win->style->border.widths[GUI_EDGE_RIGHT];
        ret_arr[3] = win->dim[3] - win->style->border.widths[GUI_EDGE_TOP] -
            win->style->border.widths[GUI_EDGE_BOTTOM];
        return;
    }
    for (int i = 0; i < 4; ++i)
        ret_arr[i] = win->dim[i];
}

static inline int **
_gui_get_win_child_indices(gui_win_t *win)
{
    if (win->child_list_index < 0)
        return 0;
    return &gui.win_child_lists[win->child_list_index];
}

static inline gui_bool_t
_gui_test_mouse_rect(int x, int y, int w, int h)
{
    if (gui.input_state.mouse_position[0] < x)
        return 0;
    if (gui.input_state.mouse_position[0] > x + w)
        return 0;
    if (gui.input_state.mouse_position[1] < y)
        return 0;
    if (gui.input_state.mouse_position[1] >= y + h)
        return 0;
    return 1;
}

static inline void
_gui_compute_quad_position(int *ret_x, int *ret_y, int origin, int x, int y,
    int w, int h)
{
    int         dimensions[4];
    int         *guide = _gui_get_guide_stack_top();
    gui_win_t   *p;
    if (guide)
    {
        for (int i = 0; i < 4; ++i)
            dimensions[i] = guide[i];
        if ((p = _gui_get_win_stack_top()))
        {
            dimensions[0] += p->scroll[0];
            dimensions[1] += p->scroll[1];
        }
    } else
    if ((p = _gui_get_win_stack_top()))
    {
        gui_win_t *p = _gui_get_win_stack_top();
        for (int i = 0; i < 4; ++i)
            dimensions[i] = p->dim[i];
        dimensions[0] += p->scroll[0];
        dimensions[1] += p->scroll[1];
        if (p->style)
        {
            dimensions[0] += p->style->border.widths[GUI_EDGE_LEFT];
            dimensions[1] += p->style->border.widths[GUI_EDGE_TOP];
            dimensions[2] = dimensions[2] -
                p->style->border.widths[GUI_EDGE_LEFT] -
                p->style->border.widths[GUI_EDGE_RIGHT];
            dimensions[3] = dimensions[3] -
                p->style->border.widths[GUI_EDGE_TOP] -
                p->style->border.widths[GUI_EDGE_BOTTOM];
        }
    } else
        for (int i = 0; i < 4; ++i)
            dimensions[i] = gui.input_state.coordinate_space[i];
    _gui_compute_quad_parented_xy(ret_x, ret_y, origin, dimensions, x, y, w, h);
}

static inline void
_gui_rotate_quad(float *verts, float x, float y, float w, float h, float rot)
{
    if (!rot)
        return;
    float tx, ty, rx, ry;
    float ox = (0.5f * w) + x;
    float oy = (0.5f * h) + y;
    for (int i = 0; i < 4; ++i)
    {
        tx = verts[(i * 5) + 0] - ox;
        ty = verts[(i * 5) + 1] - oy;
        rx = tx * cosf(rot) - ty * sinf(rot);
        ry = tx * sinf(rot) + ty * cosf(rot);
        verts[(i * 5) + 0] = rx + ox;
        verts[(i * 5) + 1] = ry + oy;
    }
}

static inline gui_glyph_t *
_gui_font_get_glyph(gui_font_t *font, int index)
    {return &font->glyphs[index % font->num_glyphs];}

static inline gui_glyph_t *
_gui_get_glyph(gui_glyph_t *glyphs, int num_glyphs, int index)
    {return &glyphs[index % num_glyphs];}

static void
_gui_save_parent_content_wh(gui_win_t *parent, int x, int y, int w, int h,
    int origin)
{
    int dims[2];
    switch (origin)
    {
    case GUI_TOP_LEFT:
        dims[0] = x;
        dims[1] = y;
        break;
    case GUI_TOP_RIGHT:
        dims[0] = parent->dim[2] - w - x;
        dims[1] = y;
        break;
    case GUI_TOP_CENTER:
        dims[0] = parent->dim[2] / 2 - w / 2 + x;
        dims[1] = y;
        break;
    case GUI_BOTTOM_LEFT:
        dims[0] = x;
        dims[1] = parent->dim[3] - y - h;
        break;
    case GUI_BOTTOM_RIGHT:
        dims[0] = parent->dim[2] - w - x;
        dims[1] = parent->dim[3] - y - h;
        break;
    case GUI_BOTTOM_CENTER:
        dims[0] = parent->dim[2] / 2 - w / 2 + x;
        dims[1] = parent->dim[3] - y - h;
        break;
    case GUI_CENTER_LEFT:
        dims[0] = x;
        dims[1] = parent->dim[3] / 2 - h / 2 + y;
        break;
    case GUI_CENTER_RIGHT:
        dims[0] = parent->dim[2] - w - x;
        dims[1] = parent->dim[3] / 2 - h / 2 + y;
        break;
    case GUI_CENTER_CENTER:
        dims[0] = parent->dim[2] / 2 - w / 2 + x;
        dims[1] = parent->dim[3] / 2 - h / 2 + y;
        break;
    default:
        gui_assert(0);
    }
    dims[0] += w;
    dims[1] += h;
    for (int i = 0; i < 2; ++i)
        if (dims[i] > parent->content_wh[i])
            parent->content_wh[i] = dims[i];
}

static void
_gui_compute_rect_by_edge(enum gui_edge edge, int tx, int ty, int w, int h,
    int ret_rect[4])
{
    switch (edge)
    {
    case GUI_EDGE_LEFT:
        ret_rect[0] = tx;
        ret_rect[1] = ty;
        ret_rect[2] = 0;
        ret_rect[3] = h;
        break;
    case GUI_EDGE_RIGHT:
        ret_rect[0] = tx + w;
        ret_rect[1] = ty;
        ret_rect[2] = 0;
        ret_rect[3] = h;
        break;
    case GUI_EDGE_TOP:
        ret_rect[0] = tx;
        ret_rect[1] = ty;
        ret_rect[2] = w;
        ret_rect[3] = 0;
        break;
    case GUI_EDGE_BOTTOM:
        ret_rect[0] = tx;
        ret_rect[1] = ty + h;
        ret_rect[2] = w;
        ret_rect[3] = 0;
        break;
    }
}

static int
_compute_win_viewport_x(gui_win_t *win)
{
    int ret = win->dim[0];
    if (win->style)
        ret += win->style->border.widths[GUI_EDGE_LEFT];
    return ret;
}

static int
_compute_win_viewport_y(gui_win_t *win)
{
    int ret = win->dim[1];
    if (win->style)
        ret += win->style->border.widths[GUI_EDGE_TOP];
    return ret;
}

static int
_compute_win_viewport_w(gui_win_t *win)
{
    if (!win->style)
        return win->dim[2];
    return win->dim[2] - win->style->border.widths[GUI_EDGE_LEFT] -
        win->style->border.widths[GUI_EDGE_RIGHT];
}

static int
_compute_win_viewport_h(gui_win_t *win)
{
    if (!win->style)
        return win->dim[3];
    return win->dim[3] - win->style->border.widths[GUI_EDGE_TOP] -
        win->style->border.widths[GUI_EDGE_BOTTOM];
}

static uint8_t _gui_munro_ttf[28168] =
{
0, 1, 0, 0, 0, 13, 0, 128, 0, 3, 0, 80, 79, 83, 47, 50, 57, 9, 251, 173, 0, 0,
1, 88, 0, 0, 0, 96, 99, 109, 97, 112, 148, 85, 172, 41, 0, 0, 3, 132, 0, 0, 3,
14, 99, 118, 116, 32, 5, 5, 9, 96, 0, 0, 8, 124, 0, 0, 0, 26, 102, 112, 103,
109, 6, 89, 156, 55, 0, 0, 6, 148, 0, 0, 1, 115, 103, 108, 121, 102, 64, 83,
178, 94, 0, 0, 8, 152, 0, 0, 95, 216, 104, 101, 97, 100, 247, 188, 137, 228,
0, 0, 0, 220, 0, 0, 0, 54, 104, 104, 101, 97, 14, 64, 6, 197, 0, 0, 1, 20, 0,
0, 0, 36, 104, 109, 116, 120, 150, 10, 1, 250, 0, 0, 1, 184, 0, 0, 1, 204, 108,
111, 99, 97, 74, 231, 98, 82, 0, 0, 104, 112, 0, 0, 0, 232, 109, 97, 120, 112,
2, 142, 3, 95, 0, 0, 1, 56, 0, 0, 0, 32, 110, 97, 109, 101, 17, 49, 7, 226, 0,
0, 105, 88, 0, 0, 3, 132, 112, 111, 115, 116, 215, 242, 82, 17, 0, 0, 108, 220,
0, 0, 1, 42, 112, 114, 101, 112, 213, 66, 175, 24, 0, 0, 8, 8, 0, 0, 0, 114,
0, 1, 0, 0, 0, 1, 0, 0, 233, 82, 148, 121, 95, 15, 60, 245, 0, 9, 8, 0, 0, 0,
0, 0, 195, 106, 119, 0, 0, 0, 0, 0, 205, 252, 200, 111, 255, 56, 254, 111, 8,
0, 7, 8, 0, 0, 0, 9, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 211, 254, 81, 1,
51, 8, 0, 255, 56, 0, 0, 8, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 115, 0, 1, 0, 0, 0, 115, 0, 65, 0, 13, 0, 8, 0, 2, 0, 1, 0, 0, 0, 0, 0, 10,
0, 0, 2, 0, 3, 20, 0, 1, 0, 1, 0, 3, 3, 143, 1, 144, 0, 5, 0, 8, 2, 188, 2, 138,
0, 0, 0, 140, 2, 188, 2, 138, 0, 0, 1, 221, 0, 50, 0, 250, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 128, 0, 0, 131, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 72, 76,
32, 32, 0, 0, 0, 0, 32, 172, 5, 211, 254, 81, 1, 51, 7, 62, 1, 178, 0, 0, 0,
1, 0, 0, 0, 0, 3, 232, 5, 120, 0, 0, 0, 32, 0, 0, 4, 0, 0, 100, 0, 0, 0, 0, 1,
252, 0, 0, 2, 88, 0, 0, 1, 144, 0, 1, 3, 32, 0, 0, 4, 176, 255, 255, 3, 32, 0,
0, 6, 64, 0, 0, 5, 120, 0, 0, 1, 144, 0, 0, 2, 88, 0, 0, 2, 88, 0, 0, 3, 32,
0, 0, 4, 176, 0, 0, 1, 144, 0, 0, 3, 32, 0, 0, 1, 144, 0, 0, 3, 32, 0, 1, 3,
232, 0, 0, 2, 88, 0, 0, 3, 32, 0, 0, 3, 32, 0, 0, 3, 232, 0, 0, 3, 32, 0, 0,
3, 232, 0, 0, 3, 32, 0, 0, 3, 232, 0, 0, 3, 232, 0, 0, 1, 144, 0, 0, 1, 144,
0, 0, 3, 246, 0, 87, 3, 246, 0, 87, 3, 246, 0, 87, 3, 32, 0, 0, 6, 64, 0, 0,
3, 232, 0, 0, 3, 232, 0, 0, 3, 32, 0, 0, 3, 232, 0, 0, 3, 32, 0, 0, 3, 32, 0,
0, 3, 232, 0, 0, 3, 232, 0, 0, 1, 144, 0, 0, 2, 88, 0, 0, 3, 232, 0, 0, 3, 32,
0, 0, 4, 176, 0, 0, 3, 232, 0, 0, 3, 232, 0, 0, 3, 232, 0, 0, 3, 232, 0, 0, 3,
232, 0, 0, 3, 32, 0, 0, 3, 32, 0, 0, 3, 232, 0, 0, 4, 176, 0, 0, 4, 176, 0, 0,
4, 176, 0, 0, 4, 176, 0, 0, 3, 32, 0, 0, 2, 88, 0, 0, 3, 32, 0, 1, 2, 88, 0,
0, 3, 32, 0, 0, 4, 176, 0, 0, 3, 232, 0, 0, 3, 232, 0, 0, 3, 32, 0, 0, 3, 232,
0, 0, 3, 232, 0, 0, 2, 88, 0, 0, 3, 232, 0, 0, 3, 232, 0, 0, 1, 144, 0, 0, 1,
144, 255, 56, 3, 232, 0, 0, 1, 144, 0, 0, 6, 64, 0, 0, 3, 232, 0, 0, 3, 232,
0, 0, 3, 232, 0, 0, 3, 232, 0, 0, 3, 32, 0, 0, 3, 32, 0, 0, 3, 32, 0, 0, 3, 232,
0, 0, 4, 176, 0, 0, 6, 64, 0, 0, 4, 176, 0, 0, 3, 232, 0, 0, 3, 32, 0, 0, 3,
32, 0, 0, 3, 32, 0, 200, 3, 32, 0, 0, 4, 176, 0, 0, 3, 232, 0, 0, 1, 144, 0,
0, 1, 144, 0, 0, 3, 32, 0, 0, 3, 32, 0, 0, 4, 84, 0, 56, 8, 0, 0, 0, 2, 88, 0,
0, 3, 232, 0, 0, 6, 64, 0, 0, 4, 176, 0, 0, 3, 246, 0, 87, 3, 32, 0, 0, 6, 64,
0, 0, 3, 32, 0, 0, 4, 176, 0, 0, 4, 176, 0, 0, 1, 144, 0, 0, 0, 0, 0, 3, 0, 0,
0, 3, 0, 0, 2, 118, 0, 1, 0, 0, 0, 0, 0, 28, 0, 3, 0, 1, 0, 0, 1, 222, 0, 6,
1, 194, 0, 0, 0, 0, 0, 220, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 3, 0, 4, 0,
5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0, 11, 0, 12, 0, 13, 0, 14, 0, 15, 0, 16, 0,
17, 0, 18, 0, 19, 0, 20, 0, 21, 0, 22, 0, 23, 0, 24, 0, 25, 0, 26, 0, 27, 0,
28, 0, 29, 0, 30, 0, 31, 0, 32, 0, 33, 0, 34, 0, 35, 0, 36, 0, 37, 0, 38, 0,
39, 0, 40, 0, 41, 0, 42, 0, 43, 0, 44, 0, 45, 0, 46, 0, 47, 0, 48, 0, 49, 0,
50, 0, 51, 0, 52, 0, 53, 0, 54, 0, 55, 0, 56, 0, 57, 0, 58, 0, 59, 0, 60, 0,
61, 0, 62, 0, 63, 0, 64, 0, 65, 0, 66, 0, 0, 0, 67, 0, 68, 0, 69, 0, 70, 0, 71,
0, 72, 0, 73, 0, 74, 0, 75, 0, 76, 0, 77, 0, 78, 0, 79, 0, 80, 0, 81, 0, 82,
0, 83, 0, 84, 0, 85, 0, 86, 0, 87, 0, 88, 0, 89, 0, 90, 0, 91, 0, 92, 0, 93,
0, 94, 0, 95, 0, 96, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 111,
0, 0, 0, 105, 0, 0, 0, 0, 0, 0, 0, 0, 0, 110, 0, 106, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 108, 0, 0, 0, 0, 0, 0, 0, 0, 0,
107, 0, 112, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 102, 0, 103, 0, 100,
0, 101, 0, 98, 0, 99, 0, 113, 0, 0, 0, 0, 0, 0, 0, 0, 0, 97, 0, 4, 0, 152, 0,
0, 0, 34, 0, 32, 0, 4, 0, 2, 0, 0, 0, 13, 0, 95, 0, 126, 0, 160, 0, 163, 0, 169,
0, 174, 0, 176, 0, 187, 0, 247, 3, 126, 32, 20, 32, 25, 32, 29, 32, 172, 255,
255, 0, 0, 0, 0, 0, 13, 0, 32, 0, 97, 0, 160, 0, 163, 0, 169, 0, 171, 0, 176,
0, 187, 0, 247, 3, 126, 32, 19, 32, 24, 32, 28, 32, 172, 255, 255, 0, 1, 255,
245, 255, 227, 255, 226, 255, 200, 255, 198, 255, 193, 255, 192, 255, 191, 255,
181, 255, 122, 252, 244, 224, 83, 224, 74, 224, 72, 223, 181, 0, 1, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 4, 0, 152, 0, 0, 0, 34, 0, 32, 0, 4, 0, 2, 0, 0, 0, 13, 0,
95, 0, 126, 0, 160, 0, 163, 0, 169, 0, 174, 0, 176, 0, 187, 0, 247, 3, 126, 32,
20, 32, 25, 32, 29, 32, 172, 255, 255, 0, 0, 0, 0, 0, 13, 0, 32, 0, 97, 0, 160,
0, 163, 0, 169, 0, 171, 0, 176, 0, 187, 0, 247, 3, 126, 32, 19, 32, 24, 32, 28,
32, 172, 255, 255, 0, 1, 255, 245, 255, 227, 255, 226, 255, 200, 255, 198, 255,
193, 255, 192, 255, 191, 255, 181, 255, 122, 252, 244, 224, 83, 224, 74, 224,
72, 223, 181, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 184, 0, 0, 44, 75, 184, 0,
9, 80, 88, 177, 1, 1, 142, 89, 184, 1, 255, 133, 184, 0, 68, 29, 185, 0, 9, 0,
3, 95, 94, 45, 184, 0, 1, 44, 32, 32, 69, 105, 68, 176, 1, 96, 45, 184, 0, 2,
44, 184, 0, 1, 42, 33, 45, 184, 0, 3, 44, 32, 70, 176, 3, 37, 70, 82, 88, 35,
89, 32, 138, 32, 138, 73, 100, 138, 32, 70, 32, 104, 97, 100, 176, 4, 37, 70,
32, 104, 97, 100, 82, 88, 35, 101, 138, 89, 47, 32, 176, 0, 83, 88, 105, 32,
176, 0, 84, 88, 33, 176, 64, 89, 27, 105, 32, 176, 0, 84, 88, 33, 176, 64, 101,
89, 89, 58, 45, 184, 0, 4, 44, 32, 70, 176, 4, 37, 70, 82, 88, 35, 138, 89, 32,
70, 32, 106, 97, 100, 176, 4, 37, 70, 32, 106, 97, 100, 82, 88, 35, 138, 89,
47, 253, 45, 184, 0, 5, 44, 75, 32, 176, 3, 38, 80, 88, 81, 88, 176, 128, 68,
27, 176, 64, 68, 89, 27, 33, 33, 32, 69, 176, 192, 80, 88, 176, 192, 68, 27,
33, 89, 89, 45, 184, 0, 6, 44, 32, 32, 69, 105, 68, 176, 1, 96, 32, 32, 69, 125,
105, 24, 68, 176, 1, 96, 45, 184, 0, 7, 44, 184, 0, 6, 42, 45, 184, 0, 8, 44,
75, 32, 176, 3, 38, 83, 88, 176, 64, 27, 176, 0, 89, 138, 138, 32, 176, 3, 38,
83, 88, 35, 33, 176, 128, 138, 138, 27, 138, 35, 89, 32, 176, 3, 38, 83, 88,
35, 33, 184, 0, 192, 138, 138, 27, 138, 35, 89, 32, 176, 3, 38, 83, 88, 35, 33,
184, 1, 0, 138, 138, 27, 138, 35, 89, 32, 176, 3, 38, 83, 88, 35, 33, 184, 1,
64, 138, 138, 27, 138, 35, 89, 32, 184, 0, 3, 38, 83, 88, 176, 3, 37, 69, 184,
1, 128, 80, 88, 35, 33, 184, 1, 128, 35, 33, 27, 176, 3, 37, 69, 35, 33, 35,
33, 89, 27, 33, 89, 68, 45, 184, 0, 9, 44, 75, 83, 88, 69, 68, 27, 33, 33, 89,
45, 0, 184, 0, 0, 43, 0, 186, 0, 1, 0, 2, 0, 2, 43, 1, 186, 0, 3, 0, 2, 0, 2,
43, 1, 191, 0, 3, 0, 57, 0, 47, 0, 36, 0, 26, 0, 16, 0, 0, 0, 8, 43, 191, 0,
4, 0, 14, 0, 11, 0, 9, 0, 7, 0, 4, 0, 0, 0, 8, 43, 0, 191, 0, 1, 0, 57, 0, 47,
0, 36, 0, 26, 0, 16, 0, 0, 0, 8, 43, 191, 0, 2, 0, 29, 0, 24, 0, 18, 0, 13, 0,
8, 0, 0, 0, 8, 43, 0, 186, 0, 5, 0, 4, 0, 7, 43, 184, 0, 0, 32, 69, 125, 105,
24, 68, 0, 0, 0, 42, 0, 200, 1, 144, 0, 200, 3, 72, 0, 0, 0, 0, 254, 112, 0,
0, 3, 232, 0, 0, 5, 120, 0, 2, 0, 0, 0, 4, 0, 100, 0, 0, 3, 156, 5, 154, 0, 3,
0, 7, 0, 44, 0, 64, 0, 196, 186, 0, 7, 0, 1, 0, 3, 43, 186, 0, 18, 0, 23, 0,
3, 43, 187, 0, 8, 0, 3, 0, 27, 0, 4, 43, 186, 0, 0, 0, 4, 0, 3, 43, 65, 27, 0,
6, 0, 18, 0, 22, 0, 18, 0, 38, 0, 18, 0, 54, 0, 18, 0, 70, 0, 18, 0, 86, 0, 18,
0, 102, 0, 18, 0, 118, 0, 18, 0, 134, 0, 18, 0, 150, 0, 18, 0, 166, 0, 18, 0,
182, 0, 18, 0, 198, 0, 18, 0, 13, 93, 65, 5, 0, 213, 0, 18, 0, 229, 0, 18, 0,
2, 93, 65, 5, 0, 90, 0, 27, 0, 106, 0, 27, 0, 2, 93, 65, 11, 0, 9, 0, 27, 0,
25, 0, 27, 0, 41, 0, 27, 0, 57, 0, 27, 0, 73, 0, 27, 0, 5, 93, 186, 0, 35, 0,
1, 0, 0, 17, 18, 57, 0, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 5,
62, 89, 186, 0, 3, 0, 5, 0, 3, 43, 186, 0, 40, 0, 30, 0, 3, 43, 184, 0, 0, 16,
184, 0, 4, 220, 48, 49, 41, 1, 17, 33, 3, 17, 33, 17, 1, 20, 7, 14, 1, 7, 14,
3, 21, 20, 23, 35, 38, 53, 52, 55, 54, 53, 52, 38, 35, 34, 7, 14, 1, 7, 53, 54,
55, 54, 51, 50, 30, 2, 3, 20, 15, 1, 6, 35, 34, 47, 1, 38, 53, 52, 63, 1, 54,
51, 50, 31, 1, 22, 3, 156, 252, 200, 3, 56, 50, 253, 44, 2, 37, 23, 11, 44, 32,
32, 50, 35, 18, 24, 32, 35, 81, 82, 66, 58, 37, 32, 13, 28, 15, 31, 27, 52, 60,
46, 75, 54, 29, 146, 10, 56, 14, 7, 11, 9, 61, 7, 11, 57, 10, 10, 9, 13, 56,
9, 5, 154, 250, 152, 5, 54, 250, 202, 3, 228, 48, 51, 25, 64, 40, 40, 65, 54,
48, 24, 39, 94, 99, 49, 75, 143, 142, 75, 57, 66, 17, 8, 19, 13, 45, 28, 15,
30, 25, 45, 65, 252, 249, 12, 10, 62, 14, 10, 71, 8, 10, 9, 13, 58, 10, 11, 60,
11, 0, 2, 0, 1, 0, 0, 0, 201, 5, 121, 0, 3, 0, 7, 0, 69, 187, 0, 0, 0, 3, 0,
1, 0, 4, 43, 184, 0, 0, 16, 184, 0, 4, 208, 184, 0, 1, 16, 184, 0, 5, 208, 0,
184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 11, 62, 89, 184, 0, 0, 69,
88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 5, 62, 89, 185, 0, 6, 0, 1, 244, 48, 49,
19, 35, 17, 51, 17, 35, 53, 51, 201, 200, 200, 200, 200, 1, 144, 3, 233, 250,
135, 200, 0, 0, 0, 0, 2, 0, 0, 4, 176, 2, 88, 6, 64, 0, 3, 0, 7, 0, 61, 184,
0, 8, 47, 184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 8, 16, 184, 0, 5, 208,
184, 0, 5, 47, 185, 0, 4, 0, 3, 244, 0, 187, 0, 3, 0, 2, 0, 0, 0, 4, 43, 184,
0, 0, 16, 184, 0, 4, 208, 184, 0, 3, 16, 184, 0, 6, 208, 48, 49, 1, 35, 17, 51,
1, 35, 17, 51, 2, 88, 200, 200, 254, 112, 200, 200, 4, 176, 1, 144, 254, 112,
1, 144, 0, 0, 0, 0, 5, 255, 255, 0, 200, 3, 231, 4, 176, 0, 3, 0, 7, 0, 11, 0,
15, 0, 19, 0, 84, 184, 0, 20, 47, 184, 0, 5, 47, 185, 0, 4, 0, 3, 244, 184, 0,
20, 16, 184, 0, 9, 208, 184, 0, 9, 47, 185, 0, 8, 0, 3, 244, 184, 0, 16, 208,
184, 0, 9, 16, 184, 0, 17, 208, 0, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185,
0, 2, 0, 9, 62, 89, 187, 0, 15, 0, 1, 0, 12, 0, 4, 43, 184, 0, 2, 16, 185, 0,
0, 0, 1, 244, 48, 49, 1, 33, 53, 33, 3, 35, 17, 51, 1, 35, 17, 51, 1, 33, 53,
33, 1, 35, 17, 51, 3, 231, 252, 24, 3, 232, 200, 200, 200, 254, 112, 200, 200,
2, 88, 252, 24, 3, 232, 253, 168, 200, 200, 3, 32, 200, 252, 224, 3, 232, 252,
24, 3, 232, 252, 224, 200, 254, 112, 3, 232, 0, 0, 3, 0, 0, 255, 55, 2, 88, 6,
64, 0, 3, 0, 23, 0, 27, 0, 143, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43, 184, 0, 0, 16,
184, 0, 5, 208, 184, 0, 0, 16, 184, 0, 9, 208, 184, 0, 1, 16, 184, 0, 11, 208,
184, 0, 1, 16, 184, 0, 15, 208, 184, 0, 1, 16, 184, 0, 19, 208, 184, 0, 0, 16,
184, 0, 21, 208, 184, 0, 0, 16, 184, 0, 24, 208, 184, 0, 1, 16, 184, 0, 25, 208,
0, 187, 0, 6, 0, 1, 0, 24, 0, 4, 43, 187, 0, 3, 0, 1, 0, 0, 0, 4, 43, 187, 0,
21, 0, 1, 0, 10, 0, 4, 43, 184, 0, 21, 16, 184, 0, 12, 208, 184, 0, 0, 16, 184,
0, 16, 208, 184, 0, 10, 16, 184, 0, 23, 208, 184, 0, 6, 16, 184, 0, 26, 208,
184, 0, 26, 47, 48, 49, 1, 35, 53, 51, 19, 35, 21, 33, 53, 33, 17, 35, 53, 35,
17, 51, 53, 33, 21, 33, 17, 51, 21, 51, 3, 35, 53, 51, 1, 144, 200, 200, 200,
200, 254, 112, 1, 144, 200, 200, 200, 1, 144, 254, 112, 200, 200, 200, 200, 200,
5, 120, 200, 250, 136, 200, 200, 1, 144, 200, 1, 144, 200, 200, 254, 112, 200,
252, 223, 200, 0, 0, 5, 0, 0, 0, 0, 5, 120, 5, 120, 0, 11, 0, 39, 0, 51, 0, 55,
0, 59, 3, 20, 187, 0, 2, 0, 3, 0, 3, 0, 4, 43, 187, 0, 14, 0, 3, 0, 15, 0, 4,
43, 184, 0, 2, 16, 185, 0, 0, 0, 3, 244, 184, 0, 3, 16, 185, 0, 5, 0, 3, 244,
184, 0, 3, 16, 184, 0, 7, 208, 184, 0, 4, 16, 184, 0, 8, 208, 184, 0, 2, 16,
184, 0, 9, 208, 184, 0, 1, 16, 184, 0, 10, 208, 184, 0, 14, 16, 185, 0, 12, 0,
3, 244, 184, 0, 0, 16, 185, 0, 34, 0, 3, 244, 184, 0, 17, 208, 184, 0, 0, 16,
184, 0, 19, 208, 184, 0, 2, 16, 184, 0, 21, 208, 184, 0, 1, 16, 184, 0, 22, 208,
184, 0, 3, 16, 184, 0, 23, 208, 184, 0, 4, 16, 184, 0, 24, 208, 184, 0, 5, 16,
184, 0, 25, 208, 184, 0, 3, 16, 184, 0, 27, 208, 184, 0, 4, 16, 184, 0, 28, 208,
184, 0, 2, 16, 184, 0, 29, 208, 184, 0, 1, 16, 184, 0, 30, 208, 184, 0, 0, 16,
184, 0, 31, 208, 184, 0, 15, 16, 184, 0, 35, 208, 184, 0, 14, 16, 184, 0, 37,
208, 184, 0, 13, 16, 184, 0, 38, 208, 184, 0, 12, 16, 184, 0, 40, 208, 184, 0,
14, 16, 184, 0, 41, 208, 184, 0, 13, 16, 184, 0, 42, 208, 184, 0, 15, 16, 184,
0, 43, 208, 184, 0, 34, 16, 184, 0, 45, 208, 184, 0, 15, 16, 184, 0, 47, 208,
184, 0, 14, 16, 184, 0, 49, 208, 184, 0, 13, 16, 184, 0, 50, 208, 184, 0, 2,
16, 184, 0, 52, 208, 184, 0, 1, 16, 184, 0, 53, 208, 184, 0, 3, 16, 184, 0, 54,
208, 184, 0, 4, 16, 184, 0, 55, 208, 184, 0, 14, 16, 184, 0, 56, 208, 184, 0,
13, 16, 184, 0, 57, 208, 184, 0, 15, 16, 184, 0, 58, 208, 0, 184, 0, 0, 69, 88,
184, 0, 8, 47, 27, 185, 0, 8, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 38, 47,
27, 185, 0, 38, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0,
0, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 9, 62, 89,
184, 0, 0, 69, 88, 184, 0, 14, 47, 27, 185, 0, 14, 0, 9, 62, 89, 184, 0, 0, 69,
88, 184, 0, 34, 47, 27, 185, 0, 34, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0,
52, 47, 27, 185, 0, 52, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 24, 47, 27,
185, 0, 24, 0, 5, 62, 89, 184, 0, 0, 69, 88, 184, 0, 42, 47, 27, 185, 0, 42,
0, 5, 62, 89, 187, 0, 19, 0, 1, 0, 20, 0, 4, 43, 184, 0, 52, 16, 185, 0, 2, 0,
1, 244, 184, 0, 3, 208, 184, 0, 0, 16, 184, 0, 5, 208, 184, 0, 8, 16, 185, 0,
6, 0, 1, 244, 184, 0, 10, 208, 184, 0, 11, 208, 184, 0, 7, 208, 184, 0, 11, 16,
184, 0, 12, 208, 184, 0, 13, 208, 184, 0, 0, 16, 184, 0, 15, 208, 184, 0, 3,
16, 184, 0, 16, 208, 184, 0, 17, 208, 185, 0, 18, 0, 1, 244, 184, 0, 24, 16,
185, 0, 22, 0, 1, 244, 185, 0, 21, 0, 1, 244, 184, 0, 22, 16, 184, 0, 26, 208,
184, 0, 27, 208, 184, 0, 21, 16, 184, 0, 28, 208, 184, 0, 20, 16, 184, 0, 29,
208, 184, 0, 19, 16, 184, 0, 30, 208, 184, 0, 18, 16, 184, 0, 31, 208, 184, 0,
17, 16, 184, 0, 32, 208, 184, 0, 33, 208, 184, 0, 0, 16, 184, 0, 35, 208, 184,
0, 13, 16, 184, 0, 36, 208, 184, 0, 37, 208, 184, 0, 27, 16, 184, 0, 40, 208,
184, 0, 41, 208, 184, 0, 44, 208, 184, 0, 45, 208, 184, 0, 21, 16, 184, 0, 46,
208, 184, 0, 20, 16, 184, 0, 47, 208, 184, 0, 19, 16, 184, 0, 48, 208, 184, 0,
18, 16, 184, 0, 49, 208, 184, 0, 21, 16, 184, 0, 50, 208, 184, 0, 20, 16, 184,
0, 51, 208, 184, 0, 37, 16, 184, 0, 53, 208, 184, 0, 54, 208, 184, 0, 0, 16,
184, 0, 55, 208, 184, 0, 45, 16, 184, 0, 56, 208, 184, 0, 21, 16, 184, 0, 57,
208, 184, 0, 20, 16, 184, 0, 58, 208, 184, 0, 56, 16, 184, 0, 59, 208, 48, 49,
1, 35, 21, 35, 53, 35, 53, 51, 53, 51, 21, 51, 33, 35, 21, 35, 21, 35, 21, 35,
21, 35, 21, 35, 21, 35, 53, 51, 53, 51, 53, 51, 53, 51, 53, 51, 53, 51, 53, 51,
17, 35, 21, 35, 53, 35, 53, 51, 53, 51, 21, 51, 1, 53, 35, 21, 1, 53, 35, 21,
2, 88, 200, 200, 200, 200, 200, 200, 3, 32, 200, 200, 200, 200, 200, 200, 200,
200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 252, 24, 200,
3, 232, 200, 3, 232, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200,
200, 200, 200, 200, 200, 200, 251, 80, 200, 200, 200, 200, 200, 2, 88, 200, 200,
252, 224, 200, 200, 0, 0, 0, 0, 3, 0, 0, 0, 0, 4, 176, 5, 120, 0, 31, 0, 35,
0, 43, 2, 5, 187, 0, 6, 0, 3, 0, 7, 0, 4, 43, 187, 0, 22, 0, 3, 0, 11, 0, 4,
43, 187, 0, 2, 0, 3, 0, 3, 0, 4, 43, 184, 0, 2, 16, 185, 0, 0, 0, 3, 244, 184,
0, 6, 16, 184, 0, 9, 208, 184, 0, 6, 16, 184, 0, 13, 208, 184, 0, 11, 16, 184,
0, 15, 208, 184, 0, 22, 16, 184, 0, 17, 208, 184, 0, 22, 16, 184, 0, 38, 208,
184, 0, 38, 47, 184, 0, 18, 208, 184, 0, 18, 47, 184, 0, 3, 16, 184, 0, 19, 208,
184, 0, 22, 16, 185, 0, 24, 0, 3, 244, 184, 0, 20, 208, 184, 0, 3, 16, 184, 0,
23, 208, 184, 0, 2, 16, 184, 0, 25, 208, 184, 0, 1, 16, 184, 0, 26, 208, 184,
0, 0, 16, 184, 0, 27, 208, 184, 0, 2, 16, 184, 0, 29, 208, 184, 0, 1, 16, 184,
0, 30, 208, 184, 0, 22, 16, 184, 0, 32, 208, 184, 0, 38, 16, 184, 0, 33, 208,
184, 0, 33, 47, 184, 0, 11, 16, 184, 0, 34, 208, 184, 0, 3, 16, 184, 0, 36, 208,
184, 0, 11, 16, 184, 0, 40, 208, 184, 0, 40, 47, 184, 0, 6, 16, 184, 0, 42, 208,
0, 184, 0, 0, 69, 88, 184, 0, 16, 47, 27, 185, 0, 16, 0, 11, 62, 89, 184, 0,
0, 69, 88, 184, 0, 12, 47, 27, 185, 0, 12, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184,
0, 20, 47, 27, 185, 0, 20, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 32, 47, 27,
185, 0, 32, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0,
5, 62, 89, 184, 0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 5, 62, 89, 184,
0, 0, 16, 185, 0, 2, 0, 1, 244, 184, 0, 3, 208, 184, 0, 6, 208, 184, 0, 7, 208,
184, 0, 12, 16, 185, 0, 10, 0, 1, 244, 185, 0, 9, 0, 1, 244, 184, 0, 16, 16,
185, 0, 14, 0, 1, 244, 184, 0, 12, 16, 185, 0, 15, 0, 1, 244, 184, 0, 14, 16,
184, 0, 18, 208, 184, 0, 19, 208, 184, 0, 12, 16, 184, 0, 21, 208, 184, 0, 10,
16, 184, 0, 22, 208, 184, 0, 23, 208, 184, 0, 9, 16, 184, 0, 24, 208, 184, 0,
23, 16, 184, 0, 26, 208, 184, 0, 27, 208, 184, 0, 9, 16, 184, 0, 28, 208, 184,
0, 7, 16, 184, 0, 30, 208, 184, 0, 31, 208, 184, 0, 19, 16, 184, 0, 33, 208,
184, 0, 34, 208, 184, 0, 12, 16, 184, 0, 35, 208, 184, 0, 31, 16, 184, 0, 36,
208, 184, 0, 9, 16, 184, 0, 37, 208, 184, 0, 27, 16, 184, 0, 39, 208, 184, 0,
40, 208, 184, 0, 9, 16, 184, 0, 41, 208, 184, 0, 36, 16, 184, 0, 43, 208, 48,
49, 33, 35, 53, 35, 21, 33, 53, 35, 17, 51, 53, 51, 53, 35, 53, 51, 53, 51, 21,
51, 21, 35, 21, 51, 21, 51, 53, 51, 21, 35, 17, 51, 1, 53, 35, 21, 1, 17, 35,
53, 35, 21, 35, 17, 4, 176, 200, 200, 253, 168, 200, 200, 200, 200, 200, 200,
200, 200, 200, 200, 200, 200, 200, 253, 168, 200, 1, 144, 199, 200, 201, 200,
200, 200, 1, 144, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 254, 112,
3, 32, 200, 200, 252, 224, 1, 144, 200, 200, 254, 112, 0, 0, 1, 0, 0, 4, 176,
0, 200, 6, 64, 0, 3, 0, 23, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43, 0, 187, 0, 3, 0,
2, 0, 0, 0, 4, 43, 48, 49, 19, 35, 17, 51, 200, 200, 200, 4, 176, 1, 144, 0,
3, 0, 0, 255, 55, 1, 144, 6, 64, 0, 3, 0, 7, 0, 11, 0, 163, 187, 0, 4, 0, 3,
0, 5, 0, 4, 43, 184, 0, 4, 16, 185, 0, 0, 0, 3, 244, 184, 0, 4, 16, 184, 0, 1,
208, 184, 0, 0, 16, 184, 0, 8, 208, 184, 0, 4, 16, 184, 0, 9, 208, 184, 0, 1,
16, 184, 0, 10, 208, 0, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 11,
62, 89, 184, 0, 0, 69, 88, 184, 0, 6, 47, 27, 185, 0, 6, 0, 11, 62, 89, 184,
0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 5, 62, 89, 184, 0, 0, 69, 88,
184, 0, 10, 47, 27, 185, 0, 10, 0, 5, 62, 89, 187, 0, 11, 0, 1, 0, 8, 0, 4, 43,
184, 0, 0, 16, 185, 0, 3, 0, 1, 244, 184, 0, 11, 16, 184, 0, 5, 208, 184, 0,
5, 47, 184, 0, 0, 16, 184, 0, 7, 208, 48, 49, 1, 35, 53, 51, 3, 35, 17, 51, 19,
35, 53, 51, 1, 144, 200, 200, 200, 200, 200, 200, 200, 200, 5, 120, 200, 249,
192, 5, 120, 249, 191, 200, 0, 0, 0, 0, 3, 0, 0, 255, 55, 1, 144, 6, 64, 0, 3,
0, 7, 0, 11, 0, 155, 187, 0, 7, 0, 3, 0, 0, 0, 4, 43, 184, 0, 0, 16, 185, 0,
1, 0, 3, 244, 184, 0, 0, 16, 184, 0, 5, 208, 184, 0, 0, 16, 184, 0, 8, 208, 184,
0, 1, 16, 184, 0, 9, 208, 0, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0,
0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 6, 47, 27, 185, 0, 6, 0, 11, 62, 89,
184, 0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 5, 62, 89, 184, 0, 0, 69,
88, 184, 0, 10, 47, 27, 185, 0, 10, 0, 5, 62, 89, 187, 0, 11, 0, 1, 0, 8, 0,
4, 43, 184, 0, 0, 16, 185, 0, 3, 0, 1, 244, 184, 0, 11, 16, 184, 0, 5, 208, 184,
0, 5, 47, 184, 0, 0, 16, 184, 0, 7, 208, 48, 49, 19, 35, 53, 51, 19, 35, 17,
51, 3, 35, 53, 51, 200, 200, 200, 200, 200, 200, 200, 200, 200, 5, 120, 200,
249, 192, 5, 120, 249, 191, 200, 0, 1, 0, 0, 3, 232, 2, 88, 6, 64, 0, 18, 1,
10, 184, 0, 19, 47, 184, 0, 4, 208, 184, 0, 4, 47, 184, 0, 2, 220, 65, 3, 0,
111, 0, 2, 0, 1, 93, 65, 3, 0, 112, 0, 2, 0, 1, 93, 184, 0, 0, 220, 65, 3, 0,
111, 0, 0, 0, 1, 93, 65, 3, 0, 112, 0, 0, 0, 1, 93, 184, 0, 2, 16, 185, 0, 1,
0, 3, 244, 184, 0, 4, 16, 185, 0, 3, 0, 3, 244, 184, 0, 6, 208, 184, 0, 4, 16,
184, 0, 8, 208, 184, 0, 3, 16, 184, 0, 10, 208, 184, 0, 0, 16, 185, 0, 18, 0,
3, 244, 184, 0, 12, 208, 184, 0, 1, 16, 184, 0, 14, 208, 184, 0, 1, 16, 184,
0, 16, 208, 184, 0, 18, 16, 184, 0, 20, 220, 0, 184, 0, 0, 69, 88, 184, 0, 7,
47, 27, 185, 0, 7, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 11, 47, 27, 185,
0, 11, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 15, 47, 27, 185, 0, 15, 0, 11,
62, 89, 185, 0, 1, 0, 1, 244, 185, 0, 0, 0, 1, 244, 184, 0, 1, 16, 184, 0, 2,
208, 184, 0, 0, 16, 184, 0, 3, 208, 184, 0, 2, 16, 184, 0, 5, 208, 184, 0, 6,
208, 184, 0, 7, 16, 185, 0, 10, 0, 1, 244, 184, 0, 7, 16, 184, 0, 12, 208, 184,
0, 10, 16, 184, 0, 13, 208, 184, 0, 6, 16, 184, 0, 16, 208, 184, 0, 17, 208,
48, 49, 1, 53, 35, 21, 35, 53, 51, 53, 35, 53, 51, 21, 33, 53, 35, 29, 1, 51,
21, 1, 144, 200, 200, 200, 200, 200, 1, 144, 200, 200, 3, 232, 200, 200, 200,
200, 200, 200, 200, 200, 200, 200, 0, 0, 0, 1, 0, 0, 0, 200, 3, 232, 4, 176,
0, 11, 0, 55, 187, 0, 2, 0, 3, 0, 3, 0, 4, 43, 184, 0, 3, 16, 184, 0, 7, 208,
184, 0, 2, 16, 184, 0, 9, 208, 0, 187, 0, 11, 0, 1, 0, 0, 0, 4, 43, 184, 0, 0,
16, 184, 0, 4, 208, 184, 0, 11, 16, 184, 0, 6, 208, 48, 49, 1, 33, 17, 35, 17,
33, 53, 33, 17, 51, 17, 33, 3, 232, 254, 112, 200, 254, 112, 1, 144, 200, 1,
144, 2, 88, 254, 112, 1, 144, 200, 1, 144, 254, 112, 0, 1, 0, 0, 255, 55, 0,
200, 0, 199, 0, 3, 0, 23, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43, 0, 187, 0, 3, 0, 2,
0, 0, 0, 4, 43, 48, 49, 23, 35, 17, 51, 200, 200, 200, 201, 1, 144, 0, 0, 1,
0, 0, 1, 144, 2, 88, 2, 88, 0, 3, 0, 23, 187, 0, 0, 0, 4, 0, 1, 0, 4, 43, 0,
187, 0, 3, 0, 1, 0, 0, 0, 4, 43, 48, 49, 1, 33, 53, 33, 2, 88, 253, 168, 2, 88,
1, 144, 200, 0, 0, 0, 1, 0, 0, 0, 0, 0, 200, 0, 200, 0, 3, 0, 36, 187, 0, 0,
0, 3, 0, 1, 0, 4, 43, 0, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0,
5, 62, 89, 185, 0, 2, 0, 1, 244, 48, 49, 51, 35, 53, 51, 200, 200, 200, 200,
0, 0, 0, 1, 0, 1, 255, 56, 2, 89, 6, 64, 0, 11, 0, 139, 184, 0, 12, 47, 184,
0, 5, 208, 184, 0, 5, 47, 184, 0, 3, 220, 65, 3, 0, 111, 0, 3, 0, 1, 93, 65,
3, 0, 112, 0, 3, 0, 1, 93, 184, 0, 1, 220, 65, 3, 0, 111, 0, 1, 0, 1, 93, 65,
3, 0, 112, 0, 1, 0, 1, 93, 185, 0, 0, 0, 3, 244, 184, 0, 3, 16, 185, 0, 2, 0,
3, 244, 184, 0, 5, 16, 185, 0, 4, 0, 3, 244, 184, 0, 7, 208, 184, 0, 2, 16, 184,
0, 9, 208, 184, 0, 0, 16, 184, 0, 13, 220, 0, 184, 0, 0, 69, 88, 184, 0, 0, 47,
27, 185, 0, 0, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8,
0, 9, 62, 89, 48, 49, 1, 35, 17, 35, 17, 35, 17, 51, 17, 51, 17, 51, 2, 89, 200,
200, 200, 200, 200, 200, 3, 232, 253, 168, 253, 168, 2, 88, 2, 88, 2, 88, 0,
0, 0, 0, 2, 0, 0, 0, 0, 3, 32, 5, 120, 0, 11, 0, 15, 0, 165, 184, 0, 16, 47,
184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 16, 16, 184, 0, 5, 208, 184, 0,
5, 47, 185, 0, 4, 0, 3, 244, 184, 0, 7, 208, 184, 0, 1, 16, 184, 0, 9, 208, 184,
0, 1, 16, 184, 0, 12, 208, 184, 0, 4, 16, 184, 0, 14, 208, 184, 0, 0, 16, 184,
0, 17, 220, 0, 184, 0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8, 0, 11, 62, 89,
184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 185, 0, 0, 0,
1, 244, 184, 0, 4, 208, 184, 0, 5, 208, 184, 0, 8, 16, 185, 0, 6, 0, 1, 244,
184, 0, 10, 208, 184, 0, 11, 208, 184, 0, 5, 16, 184, 0, 12, 208, 184, 0, 11,
16, 184, 0, 13, 208, 184, 0, 14, 208, 184, 0, 12, 16, 184, 0, 15, 208, 48, 49,
37, 35, 21, 33, 53, 35, 17, 51, 53, 33, 21, 51, 3, 17, 33, 17, 3, 32, 200, 254,
112, 200, 200, 1, 144, 200, 200, 254, 112, 200, 200, 200, 3, 232, 200, 200, 252,
24, 3, 232, 252, 24, 0, 0, 1, 0, 0, 0, 0, 1, 144, 5, 120, 0, 7, 0, 65, 187, 0,
0, 0, 3, 0, 1, 0, 4, 43, 184, 0, 1, 16, 184, 0, 5, 208, 0, 184, 0, 0, 69, 88,
184, 0, 6, 47, 27, 185, 0, 6, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47,
27, 185, 0, 0, 0, 5, 62, 89, 187, 0, 5, 0, 1, 0, 2, 0, 4, 43, 48, 49, 33, 35,
17, 35, 53, 51, 53, 51, 1, 144, 200, 200, 200, 200, 3, 232, 200, 200, 0, 0, 0,
0, 1, 0, 0, 0, 0, 2, 88, 5, 119, 0, 17, 0, 197, 184, 0, 18, 47, 184, 0, 1, 208,
184, 0, 1, 47, 184, 0, 3, 220, 65, 3, 0, 111, 0, 3, 0, 1, 93, 65, 3, 0, 112,
0, 3, 0, 1, 93, 184, 0, 5, 220, 65, 3, 0, 111, 0, 5, 0, 1, 93, 65, 3, 0, 112,
0, 5, 0, 1, 93, 185, 0, 12, 0, 3, 244, 184, 0, 0, 208, 184, 0, 1, 16, 184, 0,
7, 208, 184, 0, 3, 16, 185, 0, 14, 0, 3, 244, 184, 0, 9, 208, 184, 0, 1, 16,
185, 0, 16, 0, 3, 244, 184, 0, 12, 16, 184, 0, 19, 220, 0, 184, 0, 0, 69, 88,
184, 0, 8, 47, 27, 185, 0, 8, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47,
27, 185, 0, 0, 0, 5, 62, 89, 187, 0, 4, 0, 1, 0, 3, 0, 4, 43, 184, 0, 8, 16,
185, 0, 6, 0, 1, 244, 184, 0, 10, 208, 184, 0, 11, 208, 184, 0, 4, 16, 184, 0,
12, 208, 184, 0, 3, 16, 184, 0, 14, 208, 184, 0, 0, 16, 185, 0, 16, 0, 1, 244,
48, 49, 41, 1, 17, 51, 53, 51, 17, 33, 53, 33, 21, 51, 17, 35, 21, 35, 17, 33,
2, 88, 253, 168, 200, 200, 254, 112, 1, 144, 200, 200, 200, 1, 144, 2, 87, 200,
1, 144, 200, 200, 254, 112, 200, 254, 113, 0, 1, 0, 0, 0, 0, 2, 88, 5, 120, 0,
19, 0, 149, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43, 184, 0, 1, 16, 184, 0, 5, 208, 184,
0, 1, 16, 184, 0, 9, 208, 184, 0, 1, 16, 184, 0, 13, 208, 184, 0, 0, 16, 184,
0, 15, 208, 184, 0, 1, 16, 184, 0, 17, 208, 0, 184, 0, 0, 69, 88, 184, 0, 12,
47, 27, 185, 0, 12, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185,
0, 2, 0, 5, 62, 89, 187, 0, 9, 0, 1, 0, 6, 0, 4, 43, 184, 0, 2, 16, 185, 0, 0,
0, 1, 244, 184, 0, 4, 208, 184, 0, 5, 208, 184, 0, 12, 16, 185, 0, 10, 0, 1,
244, 184, 0, 14, 208, 184, 0, 15, 208, 184, 0, 9, 16, 184, 0, 16, 208, 184, 0,
6, 16, 184, 0, 18, 208, 48, 49, 37, 35, 21, 33, 53, 33, 17, 33, 53, 33, 17, 33,
53, 33, 21, 51, 17, 35, 21, 51, 2, 88, 200, 254, 112, 1, 144, 254, 112, 1, 144,
254, 112, 1, 144, 200, 200, 200, 200, 200, 200, 1, 144, 200, 1, 144, 200, 200,
254, 112, 200, 0, 0, 0, 1, 0, 0, 0, 0, 3, 32, 5, 120, 0, 13, 0, 128, 184, 0,
14, 47, 184, 0, 3, 47, 185, 0, 2, 0, 3, 244, 184, 0, 14, 16, 184, 0, 5, 208,
184, 0, 5, 47, 185, 0, 8, 0, 3, 244, 184, 0, 3, 16, 184, 0, 9, 208, 184, 0, 2,
16, 184, 0, 11, 208, 0, 184, 0, 0, 69, 88, 184, 0, 6, 47, 27, 185, 0, 6, 0, 11,
62, 89, 184, 0, 0, 69, 88, 184, 0, 10, 47, 27, 185, 0, 10, 0, 9, 62, 89, 184,
0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 187, 0, 13, 0, 1, 0,
0, 0, 4, 43, 184, 0, 0, 16, 184, 0, 4, 208, 184, 0, 13, 16, 184, 0, 8, 208, 48,
49, 1, 35, 17, 35, 17, 33, 17, 51, 17, 51, 17, 51, 17, 51, 3, 32, 200, 200, 254,
112, 200, 200, 200, 200, 1, 144, 254, 112, 1, 144, 3, 232, 252, 224, 1, 144,
254, 112, 0, 1, 0, 0, 0, 0, 2, 88, 5, 119, 0, 15, 0, 147, 184, 0, 16, 47, 184,
0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 16, 16, 184, 0, 7, 208, 184, 0, 7, 47,
184, 0, 3, 208, 184, 0, 1, 16, 184, 0, 5, 208, 184, 0, 0, 16, 184, 0, 9, 208,
184, 0, 7, 16, 185, 0, 12, 0, 3, 244, 184, 0, 1, 16, 184, 0, 13, 208, 0, 184,
0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8, 0, 11, 62, 89, 184, 0, 0, 69, 88,
184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 187, 0, 13, 0, 1, 0, 6, 0, 4, 43,
184, 0, 2, 16, 185, 0, 0, 0, 1, 244, 184, 0, 4, 208, 184, 0, 5, 208, 184, 0,
8, 16, 185, 0, 10, 0, 1, 244, 184, 0, 6, 16, 184, 0, 15, 208, 48, 49, 37, 35,
21, 33, 53, 33, 17, 33, 17, 33, 21, 33, 17, 51, 21, 51, 2, 88, 200, 254, 112,
1, 144, 254, 112, 2, 88, 254, 112, 200, 200, 200, 200, 200, 1, 144, 3, 31, 200,
254, 113, 200, 0, 0, 2, 0, 0, 0, 0, 3, 32, 5, 120, 0, 15, 0, 19, 0, 191, 184,
0, 20, 47, 184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 20, 16, 184, 0, 5, 208,
184, 0, 5, 47, 185, 0, 4, 0, 3, 244, 184, 0, 7, 208, 184, 0, 1, 16, 184, 0, 9,
208, 184, 0, 4, 16, 184, 0, 11, 208, 184, 0, 1, 16, 184, 0, 13, 208, 184, 0,
1, 16, 184, 0, 16, 208, 184, 0, 4, 16, 184, 0, 18, 208, 184, 0, 0, 16, 184, 0,
21, 220, 0, 184, 0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8, 0, 11, 62, 89, 184,
0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 187, 0, 13, 0, 1, 0,
14, 0, 4, 43, 184, 0, 2, 16, 185, 0, 0, 0, 1, 244, 184, 0, 4, 208, 184, 0, 5,
208, 184, 0, 8, 16, 185, 0, 6, 0, 1, 244, 184, 0, 10, 208, 184, 0, 11, 208, 184,
0, 5, 16, 184, 0, 16, 208, 184, 0, 14, 16, 184, 0, 17, 208, 184, 0, 16, 16, 184,
0, 19, 208, 48, 49, 37, 35, 21, 33, 53, 35, 17, 51, 53, 33, 21, 33, 17, 33, 21,
51, 3, 17, 33, 17, 3, 32, 200, 254, 112, 200, 200, 1, 144, 254, 112, 1, 144,
200, 200, 254, 112, 200, 200, 200, 3, 232, 200, 200, 254, 112, 200, 254, 112,
1, 144, 254, 112, 0, 1, 0, 0, 0, 2, 2, 88, 5, 120, 0, 13, 0, 136, 184, 0, 14,
47, 184, 0, 5, 208, 184, 0, 5, 47, 184, 0, 3, 220, 65, 3, 0, 111, 0, 3, 0, 1,
93, 65, 3, 0, 112, 0, 3, 0, 1, 93, 184, 0, 1, 220, 65, 3, 0, 111, 0, 1, 0, 1,
93, 65, 3, 0, 112, 0, 1, 0, 1, 93, 185, 0, 0, 0, 3, 244, 184, 0, 3, 16, 185,
0, 2, 0, 3, 244, 184, 0, 5, 16, 185, 0, 4, 0, 3, 244, 184, 0, 7, 208, 184, 0,
2, 16, 184, 0, 9, 208, 184, 0, 5, 16, 184, 0, 11, 208, 184, 0, 0, 16, 184, 0,
15, 220, 0, 184, 0, 0, 69, 88, 184, 0, 12, 47, 27, 185, 0, 12, 0, 11, 62, 89,
185, 0, 10, 0, 1, 244, 48, 49, 1, 35, 17, 35, 17, 35, 17, 51, 17, 51, 17, 33,
53, 33, 2, 88, 200, 200, 200, 200, 200, 254, 112, 2, 88, 3, 34, 254, 112, 254,
112, 1, 144, 1, 144, 1, 142, 200, 0, 3, 0, 0, 0, 0, 3, 32, 5, 120, 0, 19, 0,
23, 0, 27, 1, 15, 184, 0, 28, 47, 184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0,
28, 16, 184, 0, 5, 208, 184, 0, 5, 47, 185, 0, 4, 0, 3, 244, 184, 0, 7, 208,
184, 0, 5, 16, 184, 0, 9, 208, 184, 0, 4, 16, 184, 0, 11, 208, 184, 0, 1, 16,
184, 0, 13, 208, 184, 0, 0, 16, 184, 0, 15, 208, 184, 0, 1, 16, 184, 0, 17, 208,
184, 0, 1, 16, 184, 0, 20, 208, 184, 0, 4, 16, 184, 0, 22, 208, 184, 0, 1, 16,
184, 0, 24, 208, 184, 0, 4, 16, 184, 0, 26, 208, 184, 0, 0, 16, 184, 0, 29, 220,
0, 184, 0, 0, 69, 88, 184, 0, 12, 47, 27, 185, 0, 12, 0, 11, 62, 89, 184, 0,
0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 187, 0, 17, 0, 1, 0, 25,
0, 4, 43, 184, 0, 2, 16, 185, 0, 0, 0, 1, 244, 184, 0, 4, 208, 184, 0, 4, 47,
184, 0, 5, 208, 184, 0, 5, 47, 184, 0, 25, 16, 184, 0, 6, 208, 184, 0, 6, 47,
184, 0, 17, 16, 184, 0, 8, 208, 184, 0, 12, 16, 185, 0, 10, 0, 1, 244, 184, 0,
14, 208, 184, 0, 15, 208, 184, 0, 25, 16, 184, 0, 19, 208, 184, 0, 19, 47, 184,
0, 17, 16, 184, 0, 20, 208, 184, 0, 15, 16, 184, 0, 21, 208, 184, 0, 22, 208,
184, 0, 5, 16, 184, 0, 24, 208, 184, 0, 27, 208, 48, 49, 37, 35, 21, 33, 53,
35, 17, 51, 53, 35, 17, 51, 53, 33, 21, 51, 17, 35, 21, 51, 39, 17, 33, 17, 1,
17, 33, 17, 3, 32, 200, 254, 112, 200, 200, 200, 200, 1, 144, 200, 200, 200,
200, 254, 112, 1, 144, 254, 112, 200, 200, 200, 1, 144, 200, 1, 144, 200, 200,
254, 112, 200, 200, 1, 144, 254, 112, 253, 167, 1, 144, 254, 112, 0, 0, 0, 2,
0, 0, 0, 0, 3, 32, 5, 120, 0, 15, 0, 19, 0, 183, 184, 0, 20, 47, 184, 0, 1, 47,
185, 0, 0, 0, 3, 244, 184, 0, 20, 16, 184, 0, 9, 208, 184, 0, 9, 47, 185, 0,
8, 0, 3, 244, 184, 0, 3, 208, 184, 0, 1, 16, 184, 0, 5, 208, 184, 0, 8, 16, 184,
0, 11, 208, 184, 0, 1, 16, 184, 0, 13, 208, 184, 0, 1, 16, 184, 0, 16, 208, 184,
0, 8, 16, 184, 0, 18, 208, 184, 0, 0, 16, 184, 0, 21, 220, 0, 184, 0, 0, 69,
88, 184, 0, 12, 47, 27, 185, 0, 12, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0,
2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 187, 0, 16, 0, 1, 0, 6, 0, 4, 43, 184, 0,
2, 16, 185, 0, 0, 0, 1, 244, 184, 0, 4, 208, 184, 0, 5, 208, 184, 0, 16, 16,
184, 0, 8, 208, 184, 0, 12, 16, 185, 0, 10, 0, 1, 244, 184, 0, 14, 208, 184,
0, 15, 208, 184, 0, 17, 208, 184, 0, 18, 208, 48, 49, 37, 35, 21, 33, 53, 33,
17, 33, 53, 35, 17, 51, 53, 33, 21, 51, 3, 17, 33, 17, 3, 32, 200, 254, 112,
1, 144, 254, 112, 200, 200, 1, 144, 200, 200, 254, 112, 200, 200, 200, 1, 144,
200, 1, 144, 200, 200, 254, 112, 1, 144, 254, 112, 0, 2, 0, 0, 0, 0, 0, 200,
3, 32, 0, 3, 0, 7, 0, 66, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43, 184, 0, 0, 16, 184,
0, 4, 208, 184, 0, 1, 16, 184, 0, 5, 208, 0, 184, 0, 0, 69, 88, 184, 0, 4, 47,
27, 185, 0, 4, 0, 5, 62, 89, 187, 0, 3, 0, 1, 0, 0, 0, 4, 43, 184, 0, 4, 16,
185, 0, 6, 0, 1, 244, 48, 49, 19, 35, 53, 51, 17, 35, 53, 51, 200, 200, 200,
200, 200, 2, 88, 200, 252, 224, 200, 0, 0, 0, 0, 2, 0, 0, 255, 56, 0, 200, 3,
33, 0, 3, 0, 7, 0, 39, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43, 184, 0, 0, 16, 184, 0,
4, 208, 184, 0, 1, 16, 184, 0, 5, 208, 0, 187, 0, 3, 0, 1, 0, 0, 0, 4, 43, 48,
49, 19, 35, 53, 51, 17, 35, 17, 51, 200, 200, 200, 200, 200, 2, 89, 200, 252,
23, 1, 144, 0, 0, 1, 0, 87, 0, 193, 3, 159, 4, 217, 0, 6, 0, 21, 0, 186, 0, 3,
0, 0, 0, 3, 43, 186, 0, 5, 0, 0, 0, 3, 17, 18, 57, 48, 49, 37, 1, 53, 1, 21,
9, 1, 3, 159, 252, 184, 3, 72, 253, 143, 2, 113, 193, 1, 225, 83, 1, 228, 167,
254, 153, 254, 157, 0, 0, 2, 0, 87, 1, 235, 3, 159, 3, 175, 0, 3, 0, 7, 0, 23,
0, 187, 0, 7, 0, 1, 0, 4, 0, 4, 43, 187, 0, 3, 0, 1, 0, 0, 0, 4, 43, 48, 49,
1, 33, 53, 33, 17, 33, 53, 33, 3, 159, 252, 184, 3, 72, 252, 184, 3, 72, 3, 35,
140, 254, 60, 140, 0, 0, 1, 0, 87, 0, 193, 3, 159, 4, 217, 0, 6, 0, 21, 0, 186,
0, 5, 0, 1, 0, 3, 43, 186, 0, 3, 0, 1, 0, 5, 17, 18, 57, 48, 49, 9, 1, 53, 9,
1, 53, 1, 3, 159, 252, 184, 2, 113, 253, 143, 3, 72, 2, 162, 254, 31, 167, 1,
99, 1, 103, 167, 254, 28, 0, 2, 0, 0, 0, 0, 2, 88, 5, 120, 0, 11, 0, 15, 0, 141,
187, 0, 2, 0, 3, 0, 3, 0, 4, 43, 184, 0, 2, 16, 185, 0, 0, 0, 3, 244, 184, 0,
2, 16, 184, 0, 5, 208, 184, 0, 1, 16, 184, 0, 6, 208, 184, 0, 2, 16, 184, 0,
9, 208, 184, 0, 1, 16, 184, 0, 10, 208, 184, 0, 2, 16, 184, 0, 12, 208, 184,
0, 3, 16, 184, 0, 13, 208, 184, 0, 1, 16, 184, 0, 15, 208, 0, 184, 0, 0, 69,
88, 184, 0, 8, 47, 27, 185, 0, 8, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 12,
47, 27, 185, 0, 12, 0, 5, 62, 89, 184, 0, 8, 16, 185, 0, 6, 0, 1, 244, 184, 0,
10, 208, 184, 0, 11, 208, 184, 0, 12, 16, 185, 0, 14, 0, 1, 244, 48, 49, 1, 35,
17, 35, 17, 51, 17, 33, 53, 33, 21, 51, 3, 35, 53, 51, 2, 88, 200, 200, 200,
254, 112, 1, 144, 200, 200, 200, 200, 3, 32, 254, 112, 1, 144, 1, 144, 200, 200,
251, 80, 200, 0, 0, 13, 0, 0, 0, 0, 5, 120, 5, 120, 0, 3, 0, 7, 0, 11, 0, 15,
0, 19, 0, 23, 0, 27, 0, 31, 0, 35, 0, 39, 0, 43, 0, 47, 0, 51, 2, 89, 187, 0,
36, 0, 3, 0, 37, 0, 4, 43, 187, 0, 14, 0, 3, 0, 20, 0, 4, 43, 187, 0, 19, 0,
3, 0, 12, 0, 4, 43, 187, 0, 11, 0, 3, 0, 0, 0, 4, 43, 184, 0, 19, 16, 184, 0,
1, 208, 184, 0, 19, 16, 184, 0, 4, 208, 184, 0, 20, 16, 184, 0, 5, 208, 184,
0, 0, 16, 184, 0, 9, 208, 184, 0, 12, 16, 184, 0, 17, 208, 184, 0, 36, 16, 184,
0, 21, 208, 184, 0, 0, 16, 184, 0, 24, 208, 184, 0, 19, 16, 184, 0, 25, 208,
184, 0, 14, 16, 184, 0, 28, 208, 184, 0, 20, 16, 184, 0, 29, 208, 184, 0, 12,
16, 184, 0, 32, 208, 184, 0, 14, 16, 184, 0, 33, 208, 184, 0, 19, 16, 184, 0,
40, 208, 184, 0, 20, 16, 184, 0, 41, 208, 184, 0, 20, 16, 184, 0, 44, 208, 184,
0, 36, 16, 184, 0, 45, 208, 184, 0, 20, 16, 184, 0, 48, 208, 184, 0, 36, 16,
184, 0, 49, 208, 184, 0, 11, 16, 184, 0, 53, 220, 0, 184, 0, 0, 69, 88, 184,
0, 6, 47, 27, 185, 0, 6, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27,
185, 0, 0, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 10, 47, 27, 185, 0, 10, 0,
9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 14, 47, 27, 185, 0, 14, 0, 9, 62, 89, 184,
0, 0, 69, 88, 184, 0, 20, 47, 27, 185, 0, 20, 0, 9, 62, 89, 184, 0, 0, 69, 88,
184, 0, 38, 47, 27, 185, 0, 38, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 48,
47, 27, 185, 0, 48, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 40, 47, 27, 185,
0, 40, 0, 5, 62, 89, 187, 0, 35, 0, 1, 0, 17, 0, 4, 43, 184, 0, 6, 16, 185, 0,
2, 0, 1, 244, 184, 0, 0, 16, 185, 0, 3, 0, 1, 244, 184, 0, 2, 16, 184, 0, 4,
208, 184, 0, 5, 208, 184, 0, 17, 16, 184, 0, 8, 208, 184, 0, 40, 16, 185, 0,
24, 0, 1, 244, 185, 0, 9, 0, 1, 244, 184, 0, 0, 16, 184, 0, 11, 208, 184, 0,
14, 16, 185, 0, 12, 0, 1, 244, 184, 0, 35, 16, 184, 0, 28, 208, 185, 0, 13, 0,
1, 244, 184, 0, 0, 16, 184, 0, 15, 208, 184, 0, 9, 16, 184, 0, 16, 208, 184,
0, 12, 16, 184, 0, 18, 208, 184, 0, 19, 208, 184, 0, 0, 16, 184, 0, 21, 208,
184, 0, 5, 16, 184, 0, 22, 208, 184, 0, 23, 208, 184, 0, 9, 16, 184, 0, 26, 208,
184, 0, 17, 16, 184, 0, 27, 208, 184, 0, 19, 16, 184, 0, 30, 208, 184, 0, 31,
208, 184, 0, 9, 16, 184, 0, 32, 208, 184, 0, 17, 16, 184, 0, 33, 208, 184, 0,
9, 16, 184, 0, 36, 208, 184, 0, 17, 16, 184, 0, 37, 208, 184, 0, 0, 16, 184,
0, 39, 208, 184, 0, 24, 16, 184, 0, 42, 208, 184, 0, 43, 208, 184, 0, 44, 208,
184, 0, 45, 208, 184, 0, 9, 16, 184, 0, 46, 208, 184, 0, 17, 16, 184, 0, 47,
208, 184, 0, 0, 16, 184, 0, 49, 208, 184, 0, 23, 16, 184, 0, 50, 208, 184, 0,
51, 208, 48, 49, 1, 35, 53, 51, 35, 33, 53, 33, 1, 35, 17, 51, 5, 35, 53, 51,
19, 35, 17, 51, 37, 35, 53, 51, 1, 35, 53, 51, 37, 35, 53, 51, 19, 35, 53, 51,
5, 35, 17, 51, 1, 33, 53, 41, 1, 35, 53, 51, 17, 35, 53, 51, 4, 176, 200, 200,
200, 253, 168, 2, 88, 1, 144, 200, 200, 253, 168, 200, 200, 200, 200, 200, 253,
168, 200, 200, 3, 32, 200, 200, 253, 168, 200, 200, 200, 200, 200, 253, 168,
200, 200, 3, 32, 253, 168, 2, 88, 253, 168, 200, 200, 200, 200, 3, 232, 200,
200, 252, 24, 2, 88, 200, 200, 253, 168, 1, 144, 200, 200, 252, 24, 200, 200,
200, 254, 112, 200, 200, 2, 88, 252, 24, 200, 200, 2, 88, 200, 0, 2, 0, 0, 0,
0, 3, 32, 5, 120, 0, 11, 0, 15, 0, 158, 184, 0, 16, 47, 184, 0, 1, 47, 185, 0,
0, 0, 3, 244, 184, 0, 16, 16, 184, 0, 5, 208, 184, 0, 5, 47, 185, 0, 4, 0, 3,
244, 184, 0, 7, 208, 184, 0, 1, 16, 184, 0, 9, 208, 184, 0, 1, 16, 184, 0, 12,
208, 184, 0, 4, 16, 184, 0, 14, 208, 184, 0, 0, 16, 184, 0, 17, 220, 0, 184,
0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8, 0, 11, 62, 89, 184, 0, 0, 69, 88,
184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62, 89, 184, 0, 0, 69, 88, 184, 0, 4, 47,
27, 185, 0, 4, 0, 5, 62, 89, 187, 0, 12, 0, 1, 0, 2, 0, 4, 43, 184, 0, 8, 16,
185, 0, 6, 0, 1, 244, 184, 0, 10, 208, 184, 0, 11, 208, 184, 0, 13, 208, 184,
0, 14, 208, 48, 49, 33, 35, 17, 33, 17, 35, 17, 51, 53, 33, 21, 51, 3, 17, 33,
17, 3, 32, 200, 254, 112, 200, 200, 1, 144, 200, 200, 254, 112, 2, 88, 253, 168,
4, 176, 200, 200, 254, 112, 1, 144, 254, 112, 0, 0, 0, 0, 3, 0, 0, 0, 0, 3, 32,
5, 120, 0, 11, 0, 15, 0, 19, 0, 199, 184, 0, 20, 47, 184, 0, 1, 47, 185, 0, 0,
0, 3, 244, 184, 0, 20, 16, 184, 0, 3, 208, 184, 0, 3, 47, 184, 0, 1, 16, 184,
0, 5, 208, 184, 0, 0, 16, 184, 0, 7, 208, 184, 0, 1, 16, 184, 0, 9, 208, 184,
0, 1, 16, 184, 0, 12, 208, 184, 0, 3, 16, 185, 0, 19, 0, 3, 244, 184, 0, 14,
208, 184, 0, 1, 16, 184, 0, 16, 208, 184, 0, 0, 16, 184, 0, 21, 220, 0, 184,
0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 11, 62, 89, 184, 0, 0, 69, 88,
184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 187, 0, 9, 0, 1, 0, 10, 0, 4, 43,
184, 0, 2, 16, 185, 0, 0, 0, 1, 244, 184, 0, 4, 16, 185, 0, 6, 0, 1, 244, 184,
0, 9, 16, 184, 0, 12, 208, 184, 0, 6, 16, 184, 0, 13, 208, 184, 0, 14, 208, 184,
0, 0, 16, 184, 0, 16, 208, 184, 0, 10, 16, 184, 0, 17, 208, 184, 0, 16, 16, 184,
0, 19, 208, 48, 49, 37, 35, 21, 33, 17, 33, 21, 51, 17, 35, 21, 51, 39, 17, 33,
17, 1, 17, 33, 17, 3, 32, 200, 253, 168, 2, 88, 200, 200, 200, 200, 254, 112,
1, 144, 254, 112, 200, 200, 5, 120, 200, 254, 112, 200, 200, 1, 144, 254, 112,
253, 168, 1, 144, 254, 112, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 88, 5, 120, 0, 11,
0, 99, 187, 0, 2, 0, 3, 0, 3, 0, 4, 43, 184, 0, 2, 16, 184, 0, 5, 208, 184, 0,
2, 16, 184, 0, 9, 208, 0, 184, 0, 0, 69, 88, 184, 0, 6, 47, 27, 185, 0, 6, 0,
11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62, 89, 185,
0, 2, 0, 1, 244, 184, 0, 6, 16, 185, 0, 4, 0, 1, 244, 184, 0, 8, 208, 184, 0,
9, 208, 184, 0, 2, 16, 184, 0, 10, 208, 184, 0, 11, 208, 48, 49, 41, 1, 53, 35,
17, 51, 53, 33, 21, 33, 17, 33, 2, 88, 254, 112, 200, 200, 1, 144, 254, 112,
1, 144, 200, 3, 232, 200, 200, 252, 24, 0, 2, 0, 0, 0, 0, 3, 32, 5, 120, 0, 7,
0, 11, 0, 141, 184, 0, 12, 47, 184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 12,
16, 184, 0, 3, 208, 184, 0, 3, 47, 184, 0, 1, 16, 184, 0, 5, 208, 184, 0, 1,
16, 184, 0, 8, 208, 184, 0, 3, 16, 185, 0, 11, 0, 3, 244, 184, 0, 0, 16, 184,
0, 13, 220, 0, 184, 0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 11, 62, 89,
184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 185, 0, 0, 0,
1, 244, 184, 0, 4, 16, 185, 0, 6, 0, 1, 244, 184, 0, 0, 16, 184, 0, 8, 208, 184,
0, 6, 16, 184, 0, 9, 208, 184, 0, 10, 208, 184, 0, 8, 16, 184, 0, 11, 208, 48,
49, 37, 35, 21, 33, 17, 33, 21, 51, 3, 17, 33, 17, 3, 32, 200, 253, 168, 2, 88,
200, 200, 254, 112, 200, 200, 5, 120, 200, 252, 24, 3, 232, 252, 24, 0, 0, 1,
0, 0, 0, 0, 2, 88, 5, 120, 0, 11, 0, 85, 187, 0, 10, 0, 3, 0, 1, 0, 4, 43, 184,
0, 10, 16, 184, 0, 5, 208, 0, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2,
0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62, 89,
187, 0, 7, 0, 1, 0, 8, 0, 4, 43, 184, 0, 2, 16, 185, 0, 4, 0, 1, 244, 184, 0,
0, 16, 185, 0, 10, 0, 1, 244, 48, 49, 41, 1, 17, 33, 21, 33, 17, 33, 21, 33,
17, 33, 2, 88, 253, 168, 2, 88, 254, 112, 1, 144, 254, 112, 1, 144, 5, 120, 200,
254, 112, 200, 254, 112, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 88, 5, 120, 0, 9, 0, 75,
187, 0, 2, 0, 3, 0, 3, 0, 4, 43, 184, 0, 2, 16, 184, 0, 7, 208, 0, 184, 0, 0,
69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184,
0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 187, 0, 9, 0, 1, 0, 0, 0, 4, 43, 184,
0, 4, 16, 185, 0, 6, 0, 1, 244, 48, 49, 1, 33, 17, 35, 17, 33, 21, 33, 17, 33,
2, 88, 254, 112, 200, 2, 88, 254, 112, 1, 144, 2, 88, 253, 168, 5, 120, 200,
254, 112, 0, 0, 1, 0, 0, 0, 0, 3, 32, 5, 122, 0, 17, 0, 163, 184, 0, 18, 47,
184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 18, 16, 184, 0, 5, 208, 184, 0,
5, 47, 185, 0, 4, 0, 3, 244, 184, 0, 7, 208, 184, 0, 1, 16, 184, 0, 9, 208, 184,
0, 4, 16, 184, 0, 11, 208, 184, 0, 1, 16, 184, 0, 13, 208, 184, 0, 0, 16, 184,
0, 19, 220, 0, 184, 0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8, 0, 11, 62, 89,
184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 187, 0, 16, 0,
1, 0, 15, 0, 4, 43, 184, 0, 2, 16, 185, 0, 0, 0, 1, 244, 184, 0, 4, 208, 184,
0, 5, 208, 184, 0, 8, 16, 185, 0, 6, 0, 1, 244, 184, 0, 10, 208, 184, 0, 11,
208, 184, 0, 5, 16, 184, 0, 12, 208, 184, 0, 13, 208, 48, 49, 37, 35, 21, 33,
53, 35, 17, 51, 53, 33, 21, 33, 17, 33, 17, 35, 53, 33, 3, 32, 200, 254, 112,
200, 200, 1, 144, 254, 112, 1, 144, 200, 1, 144, 200, 200, 200, 3, 234, 200,
202, 252, 24, 1, 144, 200, 0, 0, 1, 0, 0, 0, 0, 3, 32, 5, 120, 0, 11, 0, 133,
184, 0, 12, 47, 184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 12, 16, 184, 0,
5, 208, 184, 0, 5, 47, 185, 0, 4, 0, 3, 244, 184, 0, 7, 208, 184, 0, 1, 16, 184,
0, 9, 208, 184, 0, 0, 16, 184, 0, 13, 220, 0, 184, 0, 0, 69, 88, 184, 0, 6, 47,
27, 185, 0, 6, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 10, 47, 27, 185, 0,
10, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62,
89, 184, 0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 5, 62, 89, 187, 0, 9,
0, 1, 0, 2, 0, 4, 43, 48, 49, 33, 35, 17, 33, 17, 35, 17, 51, 17, 33, 17, 51,
3, 32, 200, 254, 112, 200, 200, 1, 144, 200, 2, 88, 253, 168, 5, 120, 253, 168,
2, 88, 0, 0, 1, 0, 0, 0, 0, 0, 200, 5, 120, 0, 3, 0, 47, 187, 0, 0, 0, 3, 0,
1, 0, 4, 43, 0, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 11, 62, 89,
184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62, 89, 48, 49, 51, 35,
17, 51, 200, 200, 200, 5, 120, 0, 0, 0, 1, 0, 0, 0, 0, 1, 144, 5, 120, 0, 9,
0, 105, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43, 184, 0, 1, 16, 185, 0, 3, 0, 3, 244,
184, 0, 1, 16, 184, 0, 5, 208, 184, 0, 2, 16, 184, 0, 6, 208, 184, 0, 3, 16,
184, 0, 7, 208, 0, 184, 0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8, 0, 11, 62,
89, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 185, 0, 0,
0, 1, 244, 184, 0, 4, 208, 184, 0, 5, 208, 184, 0, 8, 16, 185, 0, 6, 0, 1, 244,
48, 49, 37, 35, 21, 35, 53, 51, 17, 35, 53, 33, 1, 144, 200, 200, 200, 200, 1,
144, 200, 200, 200, 3, 232, 200, 0, 0, 1, 0, 0, 0, 0, 3, 32, 5, 120, 0, 23, 1,
17, 187, 0, 6, 0, 3, 0, 7, 0, 4, 43, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43, 184, 0,
1, 16, 185, 0, 3, 0, 3, 244, 184, 0, 6, 16, 184, 0, 9, 208, 184, 0, 3, 16, 184,
0, 11, 208, 184, 0, 1, 16, 184, 0, 13, 208, 184, 0, 2, 16, 184, 0, 14, 208, 184,
0, 0, 16, 184, 0, 15, 208, 184, 0, 1, 16, 184, 0, 17, 208, 184, 0, 2, 16, 184,
0, 18, 208, 184, 0, 3, 16, 184, 0, 19, 208, 184, 0, 1, 16, 184, 0, 21, 208, 184,
0, 2, 16, 184, 0, 22, 208, 0, 184, 0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8,
0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 14, 47, 27, 185, 0, 14, 0, 11, 62,
89, 184, 0, 0, 69, 88, 184, 0, 12, 47, 27, 185, 0, 12, 0, 9, 62, 89, 184, 0,
0, 69, 88, 184, 0, 16, 47, 27, 185, 0, 16, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184,
0, 0, 47, 27, 185, 0, 0, 0, 5, 62, 89, 184, 0, 0, 69, 88, 184, 0, 6, 47, 27,
185, 0, 6, 0, 5, 62, 89, 187, 0, 21, 0, 1, 0, 2, 0, 4, 43, 184, 0, 21, 16, 184,
0, 4, 208, 184, 0, 12, 16, 185, 0, 10, 0, 1, 244, 184, 0, 4, 16, 185, 0, 11,
0, 1, 244, 184, 0, 10, 16, 184, 0, 18, 208, 184, 0, 19, 208, 184, 0, 2, 16, 184,
0, 23, 208, 48, 49, 33, 35, 17, 35, 53, 35, 17, 35, 17, 51, 17, 51, 53, 51, 17,
51, 17, 35, 21, 35, 21, 51, 21, 51, 3, 32, 200, 200, 200, 200, 200, 200, 200,
200, 200, 200, 200, 200, 1, 144, 200, 253, 168, 5, 120, 253, 168, 200, 1, 144,
254, 112, 200, 200, 200, 0, 0, 0, 1, 0, 0, 0, 0, 2, 88, 5, 120, 0, 5, 0, 53,
187, 0, 4, 0, 3, 0, 1, 0, 4, 43, 0, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185,
0, 2, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62,
89, 185, 0, 4, 0, 1, 244, 48, 49, 41, 1, 17, 51, 17, 33, 2, 88, 253, 168, 200,
1, 144, 5, 120, 251, 80, 0, 1, 0, 0, 0, 0, 3, 232, 5, 120, 0, 19, 1, 3, 184,
0, 20, 47, 184, 0, 9, 208, 184, 0, 9, 47, 184, 0, 5, 220, 65, 3, 0, 64, 0, 5,
0, 1, 93, 65, 3, 0, 144, 0, 5, 0, 1, 93, 184, 0, 1, 220, 65, 3, 0, 64, 0, 1,
0, 1, 93, 65, 3, 0, 144, 0, 1, 0, 1, 93, 185, 0, 0, 0, 3, 244, 184, 0, 5, 16,
185, 0, 4, 0, 3, 244, 184, 0, 9, 16, 185, 0, 8, 0, 3, 244, 184, 0, 11, 208, 184,
0, 5, 16, 184, 0, 13, 208, 184, 0, 4, 16, 184, 0, 15, 208, 184, 0, 1, 16, 184,
0, 17, 208, 184, 0, 0, 16, 184, 0, 21, 220, 0, 184, 0, 0, 69, 88, 184, 0, 10,
47, 27, 185, 0, 10, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 18, 47, 27, 185,
0, 18, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 12, 47, 27, 185, 0, 12, 0, 9,
62, 89, 184, 0, 0, 69, 88, 184, 0, 16, 47, 27, 185, 0, 16, 0, 9, 62, 89, 184,
0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62, 89, 184, 0, 0, 69, 88,
184, 0, 8, 47, 27, 185, 0, 8, 0, 5, 62, 89, 187, 0, 3, 0, 1, 0, 4, 0, 4, 43,
184, 0, 16, 16, 185, 0, 2, 0, 1, 244, 184, 0, 6, 208, 184, 0, 7, 208, 184, 0,
14, 208, 184, 0, 15, 208, 48, 49, 33, 35, 17, 35, 21, 35, 53, 35, 17, 35, 17,
51, 17, 51, 21, 51, 53, 51, 17, 51, 3, 232, 200, 200, 200, 200, 200, 200, 200,
200, 200, 200, 3, 32, 200, 200, 252, 224, 5, 120, 254, 112, 200, 200, 1, 144,
0, 0, 1, 0, 0, 0, 0, 3, 32, 5, 120, 0, 15, 0, 172, 184, 0, 16, 47, 184, 0, 1,
47, 185, 0, 0, 0, 3, 244, 184, 0, 16, 16, 184, 0, 7, 208, 184, 0, 7, 47, 185,
0, 6, 0, 3, 244, 184, 0, 9, 208, 184, 0, 1, 16, 184, 0, 13, 208, 184, 0, 0, 16,
184, 0, 17, 220, 0, 184, 0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8, 0, 11, 62,
89, 184, 0, 0, 69, 88, 184, 0, 14, 47, 27, 185, 0, 14, 0, 11, 62, 89, 184, 0,
0, 69, 88, 184, 0, 10, 47, 27, 185, 0, 10, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184,
0, 0, 47, 27, 185, 0, 0, 0, 5, 62, 89, 184, 0, 0, 69, 88, 184, 0, 6, 47, 27,
185, 0, 6, 0, 5, 62, 89, 184, 0, 10, 16, 185, 0, 4, 0, 1, 244, 184, 0, 12, 208,
184, 0, 13, 208, 185, 0, 2, 0, 1, 244, 184, 0, 13, 16, 184, 0, 5, 208, 48, 49,
33, 35, 17, 35, 53, 35, 17, 35, 17, 51, 17, 51, 21, 51, 17, 51, 3, 32, 200, 200,
200, 200, 200, 200, 200, 200, 2, 88, 200, 252, 224, 5, 120, 254, 112, 200, 2,
88, 0, 2, 0, 0, 0, 0, 3, 32, 5, 120, 0, 11, 0, 15, 0, 165, 184, 0, 16, 47, 184,
0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 16, 16, 184, 0, 5, 208, 184, 0, 5, 47,
185, 0, 4, 0, 3, 244, 184, 0, 7, 208, 184, 0, 1, 16, 184, 0, 9, 208, 184, 0,
1, 16, 184, 0, 12, 208, 184, 0, 4, 16, 184, 0, 14, 208, 184, 0, 0, 16, 184, 0,
17, 220, 0, 184, 0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8, 0, 11, 62, 89, 184,
0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 185, 0, 0, 0, 1, 244,
184, 0, 4, 208, 184, 0, 5, 208, 184, 0, 8, 16, 185, 0, 6, 0, 1, 244, 184, 0,
10, 208, 184, 0, 11, 208, 184, 0, 5, 16, 184, 0, 12, 208, 184, 0, 11, 16, 184,
0, 13, 208, 184, 0, 14, 208, 184, 0, 12, 16, 184, 0, 15, 208, 48, 49, 37, 35,
21, 33, 53, 35, 17, 51, 53, 33, 21, 51, 3, 17, 33, 17, 3, 32, 200, 254, 112,
200, 200, 1, 144, 200, 200, 254, 112, 200, 200, 200, 3, 232, 200, 200, 252, 24,
3, 232, 252, 24, 0, 0, 2, 0, 0, 0, 0, 3, 32, 5, 120, 0, 9, 0, 13, 0, 141, 184,
0, 14, 47, 184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 14, 16, 184, 0, 5, 208,
184, 0, 5, 47, 185, 0, 4, 0, 3, 244, 184, 0, 1, 16, 184, 0, 7, 208, 184, 0, 1,
16, 184, 0, 10, 208, 184, 0, 4, 16, 184, 0, 12, 208, 184, 0, 0, 16, 184, 0, 15,
220, 0, 184, 0, 0, 69, 88, 184, 0, 6, 47, 27, 185, 0, 6, 0, 11, 62, 89, 184,
0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 5, 62, 89, 187, 0, 1, 0, 1, 0,
2, 0, 4, 43, 184, 0, 6, 16, 185, 0, 8, 0, 1, 244, 184, 0, 1, 16, 184, 0, 10,
208, 184, 0, 8, 16, 184, 0, 11, 208, 184, 0, 12, 208, 48, 49, 1, 35, 21, 33,
17, 35, 17, 33, 21, 51, 3, 17, 33, 17, 3, 32, 200, 254, 112, 200, 2, 88, 200,
200, 254, 112, 3, 32, 200, 253, 168, 5, 120, 200, 254, 112, 1, 144, 254, 112,
0, 0, 0, 0, 2, 0, 0, 0, 0, 3, 32, 5, 120, 0, 15, 0, 23, 0, 204, 187, 0, 4, 0,
3, 0, 9, 0, 4, 43, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43, 184, 0, 4, 16, 185, 0, 20,
0, 3, 244, 184, 0, 6, 208, 184, 0, 4, 16, 184, 0, 7, 208, 184, 0, 4, 16, 184,
0, 11, 208, 184, 0, 1, 16, 184, 0, 13, 208, 184, 0, 0, 16, 184, 0, 16, 208, 184,
0, 1, 16, 184, 0, 17, 208, 184, 0, 1, 16, 184, 0, 21, 208, 0, 184, 0, 0, 69,
88, 184, 0, 12, 47, 27, 185, 0, 12, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0,
6, 47, 27, 185, 0, 6, 0, 5, 62, 89, 184, 0, 0, 69, 88, 184, 0, 16, 47, 27, 185,
0, 16, 0, 5, 62, 89, 184, 0, 12, 16, 185, 0, 2, 0, 1, 244, 184, 0, 6, 16, 185,
0, 4, 0, 1, 244, 184, 0, 8, 208, 184, 0, 9, 208, 184, 0, 2, 16, 184, 0, 10, 208,
184, 0, 11, 208, 184, 0, 14, 208, 184, 0, 15, 208, 184, 0, 9, 16, 184, 0, 18,
208, 184, 0, 19, 208, 184, 0, 22, 208, 184, 0, 23, 208, 48, 49, 1, 35, 17, 33,
17, 51, 21, 35, 53, 35, 17, 51, 53, 33, 21, 51, 17, 35, 53, 35, 53, 51, 21, 51,
3, 32, 200, 254, 112, 200, 200, 200, 200, 1, 144, 200, 200, 200, 200, 200, 1,
144, 3, 32, 252, 24, 200, 200, 3, 232, 200, 200, 251, 80, 200, 200, 200, 0, 0,
0, 0, 3, 0, 0, 0, 0, 3, 32, 5, 120, 0, 9, 0, 13, 0, 17, 0, 182, 184, 0, 18, 47,
184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 18, 16, 184, 0, 5, 208, 184, 0,
5, 47, 185, 0, 4, 0, 3, 244, 184, 0, 1, 16, 184, 0, 7, 208, 184, 0, 0, 16, 184,
0, 10, 208, 184, 0, 1, 16, 184, 0, 11, 208, 184, 0, 1, 16, 184, 0, 14, 208, 184,
0, 4, 16, 184, 0, 16, 208, 184, 0, 0, 16, 184, 0, 19, 220, 0, 184, 0, 0, 69,
88, 184, 0, 6, 47, 27, 185, 0, 6, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 4,
47, 27, 185, 0, 4, 0, 5, 62, 89, 184, 0, 0, 69, 88, 184, 0, 10, 47, 27, 185,
0, 10, 0, 5, 62, 89, 187, 0, 1, 0, 1, 0, 2, 0, 4, 43, 184, 0, 6, 16, 185, 0,
8, 0, 1, 244, 184, 0, 2, 16, 184, 0, 12, 208, 184, 0, 1, 16, 184, 0, 14, 208,
184, 0, 8, 16, 184, 0, 15, 208, 184, 0, 16, 208, 48, 49, 1, 35, 21, 33, 17, 35,
17, 33, 21, 51, 17, 35, 17, 51, 39, 17, 33, 17, 3, 32, 200, 254, 112, 200, 2,
88, 200, 200, 200, 200, 254, 112, 3, 32, 200, 253, 168, 5, 120, 200, 251, 80,
2, 88, 200, 1, 144, 254, 112, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 88, 5, 120, 0, 19,
0, 229, 184, 0, 20, 47, 184, 0, 9, 208, 184, 0, 9, 47, 184, 0, 7, 220, 184, 0,
9, 16, 185, 0, 7, 0, 3, 244, 65, 3, 0, 111, 0, 7, 0, 1, 93, 65, 3, 0, 112, 0,
7, 0, 1, 93, 184, 0, 1, 220, 65, 3, 0, 111, 0, 1, 0, 1, 93, 65, 3, 0, 112, 0,
1, 0, 1, 93, 185, 0, 0, 0, 3, 244, 184, 0, 9, 16, 184, 0, 3, 208, 184, 0, 7,
16, 185, 0, 6, 0, 3, 244, 184, 0, 7, 16, 184, 0, 11, 208, 184, 0, 0, 16, 184,
0, 13, 208, 184, 0, 7, 16, 184, 0, 15, 208, 184, 0, 6, 16, 184, 0, 18, 208, 184,
0, 0, 16, 184, 0, 21, 220, 0, 184, 0, 0, 69, 88, 184, 0, 12, 47, 27, 185, 0,
12, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62,
89, 187, 0, 17, 0, 1, 0, 6, 0, 4, 43, 184, 0, 2, 16, 185, 0, 0, 0, 1, 244, 184,
0, 4, 208, 184, 0, 5, 208, 184, 0, 17, 16, 184, 0, 8, 208, 184, 0, 12, 16, 185,
0, 10, 0, 1, 244, 184, 0, 14, 208, 184, 0, 15, 208, 184, 0, 6, 16, 184, 0, 19,
208, 48, 49, 37, 35, 21, 33, 53, 33, 17, 35, 53, 35, 17, 51, 53, 33, 21, 33,
17, 51, 21, 51, 2, 88, 200, 254, 112, 1, 144, 200, 200, 200, 1, 144, 254, 112,
200, 200, 200, 200, 200, 1, 144, 200, 1, 144, 200, 200, 254, 112, 200, 0, 1,
0, 0, 0, 0, 2, 88, 5, 120, 0, 7, 0, 65, 187, 0, 2, 0, 3, 0, 3, 0, 4, 43, 0, 184,
0, 0, 69, 88, 184, 0, 6, 47, 27, 185, 0, 6, 0, 11, 62, 89, 184, 0, 0, 69, 88,
184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 184, 0, 6, 16, 185, 0, 0, 0, 1, 244,
184, 0, 4, 208, 184, 0, 5, 208, 48, 49, 1, 35, 17, 35, 17, 35, 53, 33, 2, 88,
200, 200, 200, 2, 88, 4, 176, 251, 80, 4, 176, 200, 0, 0, 0, 0, 1, 0, 0, 0, 0,
3, 32, 5, 120, 0, 11, 0, 128, 184, 0, 12, 47, 184, 0, 1, 47, 185, 0, 0, 0, 3,
244, 184, 0, 12, 16, 184, 0, 5, 208, 184, 0, 5, 47, 185, 0, 4, 0, 3, 244, 184,
0, 7, 208, 184, 0, 1, 16, 184, 0, 9, 208, 184, 0, 0, 16, 184, 0, 13, 220, 0,
184, 0, 0, 69, 88, 184, 0, 6, 47, 27, 185, 0, 6, 0, 11, 62, 89, 184, 0, 0, 69,
88, 184, 0, 10, 47, 27, 185, 0, 10, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0,
2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 185, 0, 0, 0, 1, 244, 184, 0, 4, 208, 184,
0, 5, 208, 184, 0, 8, 208, 184, 0, 9, 208, 48, 49, 37, 35, 21, 33, 53, 35, 17,
51, 17, 33, 17, 51, 3, 32, 200, 254, 112, 200, 200, 1, 144, 200, 200, 200, 200,
4, 176, 251, 80, 4, 176, 0, 0, 0, 0, 1, 0, 0, 0, 0, 3, 232, 5, 120, 0, 19, 0,
168, 187, 0, 8, 0, 3, 0, 9, 0, 4, 43, 187, 0, 4, 0, 3, 0, 5, 0, 4, 43, 187, 0,
0, 0, 3, 0, 1, 0, 4, 43, 184, 0, 4, 16, 185, 0, 2, 0, 3, 244, 184, 0, 8, 16,
185, 0, 6, 0, 3, 244, 184, 0, 8, 16, 184, 0, 11, 208, 184, 0, 7, 16, 184, 0,
12, 208, 184, 0, 5, 16, 184, 0, 13, 208, 184, 0, 6, 16, 184, 0, 14, 208, 184,
0, 4, 16, 184, 0, 15, 208, 184, 0, 3, 16, 184, 0, 16, 208, 184, 0, 1, 16, 184,
0, 17, 208, 184, 0, 2, 16, 184, 0, 18, 208, 0, 184, 0, 0, 69, 88, 184, 0, 10,
47, 27, 185, 0, 10, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 18, 47, 27, 185,
0, 18, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 5,
62, 89, 48, 49, 1, 35, 17, 35, 17, 35, 17, 35, 17, 35, 17, 51, 17, 51, 17, 51,
17, 51, 17, 51, 3, 232, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 3,
32, 254, 112, 254, 112, 1, 144, 1, 144, 2, 88, 253, 168, 254, 112, 1, 144, 2,
88, 0, 0, 0, 1, 0, 0, 0, 0, 3, 232, 5, 120, 0, 19, 0, 241, 184, 0, 20, 47, 184,
0, 9, 208, 184, 0, 9, 47, 184, 0, 5, 220, 65, 3, 0, 64, 0, 5, 0, 1, 93, 65, 3,
0, 144, 0, 5, 0, 1, 93, 184, 0, 1, 220, 65, 3, 0, 64, 0, 1, 0, 1, 93, 65, 3,
0, 144, 0, 1, 0, 1, 93, 185, 0, 0, 0, 3, 244, 184, 0, 5, 16, 185, 0, 4, 0, 3,
244, 184, 0, 9, 16, 185, 0, 8, 0, 3, 244, 184, 0, 11, 208, 184, 0, 5, 16, 184,
0, 13, 208, 184, 0, 4, 16, 184, 0, 15, 208, 184, 0, 1, 16, 184, 0, 17, 208, 184,
0, 0, 16, 184, 0, 21, 220, 0, 184, 0, 0, 69, 88, 184, 0, 10, 47, 27, 185, 0,
10, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 18, 47, 27, 185, 0, 18, 0, 11,
62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62, 89, 184, 0,
0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8, 0, 5, 62, 89, 187, 0, 17, 0, 1, 0, 2,
0, 4, 43, 184, 0, 17, 16, 184, 0, 4, 208, 184, 0, 2, 16, 184, 0, 6, 208, 184,
0, 17, 16, 184, 0, 12, 208, 184, 0, 4, 16, 184, 0, 13, 208, 184, 0, 4, 16, 185,
0, 15, 0, 1, 244, 48, 49, 33, 35, 17, 35, 53, 35, 21, 35, 17, 35, 17, 51, 17,
51, 53, 51, 21, 51, 17, 51, 3, 232, 200, 200, 200, 200, 200, 200, 200, 200, 200,
200, 1, 144, 200, 200, 254, 112, 5, 120, 252, 224, 200, 200, 3, 32, 0, 0, 0,
0, 1, 0, 0, 0, 0, 3, 232, 5, 120, 0, 35, 1, 201, 187, 0, 8, 0, 3, 0, 9, 0, 4,
43, 187, 0, 4, 0, 3, 0, 5, 0, 4, 43, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43, 184, 0,
4, 16, 185, 0, 2, 0, 3, 244, 184, 0, 8, 16, 185, 0, 6, 0, 3, 244, 184, 0, 8,
16, 184, 0, 11, 208, 184, 0, 7, 16, 184, 0, 12, 208, 184, 0, 5, 16, 184, 0, 13,
208, 184, 0, 6, 16, 184, 0, 14, 208, 184, 0, 8, 16, 184, 0, 15, 208, 184, 0,
7, 16, 184, 0, 16, 208, 184, 0, 9, 16, 184, 0, 17, 208, 184, 0, 8, 16, 184, 0,
19, 208, 184, 0, 7, 16, 184, 0, 20, 208, 184, 0, 5, 16, 184, 0, 21, 208, 184,
0, 6, 16, 184, 0, 22, 208, 184, 0, 4, 16, 184, 0, 23, 208, 184, 0, 3, 16, 184,
0, 24, 208, 184, 0, 1, 16, 184, 0, 25, 208, 184, 0, 2, 16, 184, 0, 26, 208, 184,
0, 0, 16, 184, 0, 27, 208, 184, 0, 1, 16, 184, 0, 29, 208, 184, 0, 2, 16, 184,
0, 30, 208, 184, 0, 4, 16, 184, 0, 31, 208, 184, 0, 3, 16, 184, 0, 32, 208, 184,
0, 1, 16, 184, 0, 33, 208, 184, 0, 2, 16, 184, 0, 34, 208, 0, 184, 0, 0, 69,
88, 184, 0, 18, 47, 27, 185, 0, 18, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0,
26, 47, 27, 185, 0, 26, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 16, 47, 27,
185, 0, 16, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 20, 47, 27, 185, 0, 20,
0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 24, 47, 27, 185, 0, 24, 0, 9, 62, 89,
184, 0, 0, 69, 88, 184, 0, 28, 47, 27, 185, 0, 28, 0, 9, 62, 89, 184, 0, 0, 69,
88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62, 89, 184, 0, 0, 69, 88, 184, 0, 8,
47, 27, 185, 0, 8, 0, 5, 62, 89, 187, 0, 33, 0, 1, 0, 2, 0, 4, 43, 184, 0, 33,
16, 184, 0, 4, 208, 184, 0, 2, 16, 184, 0, 6, 208, 184, 0, 2, 16, 184, 0, 10,
208, 184, 0, 33, 16, 184, 0, 12, 208, 184, 0, 4, 16, 184, 0, 13, 208, 184, 0,
16, 16, 185, 0, 14, 0, 1, 244, 184, 0, 22, 208, 184, 0, 23, 208, 184, 0, 15,
208, 184, 0, 23, 16, 184, 0, 30, 208, 184, 0, 31, 208, 184, 0, 2, 16, 184, 0,
35, 208, 48, 49, 33, 35, 17, 35, 53, 35, 21, 35, 17, 35, 17, 51, 53, 51, 53,
35, 53, 35, 17, 51, 17, 51, 21, 51, 53, 51, 17, 51, 17, 35, 21, 35, 21, 51, 21,
51, 3, 232, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200,
200, 200, 200, 200, 200, 1, 144, 200, 200, 254, 112, 1, 144, 200, 200, 200, 1,
144, 254, 112, 200, 200, 1, 144, 254, 112, 200, 200, 200, 0, 0, 1, 0, 0, 0, 0,
3, 232, 5, 120, 0, 19, 1, 10, 187, 0, 8, 0, 3, 0, 9, 0, 4, 43, 187, 0, 4, 0,
3, 0, 5, 0, 4, 43, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43, 184, 0, 4, 16, 185, 0, 2,
0, 3, 244, 184, 0, 8, 16, 185, 0, 6, 0, 3, 244, 184, 0, 8, 16, 184, 0, 11, 208,
184, 0, 7, 16, 184, 0, 12, 208, 184, 0, 5, 16, 184, 0, 13, 208, 184, 0, 6, 16,
184, 0, 14, 208, 184, 0, 4, 16, 184, 0, 15, 208, 184, 0, 3, 16, 184, 0, 16, 208,
184, 0, 1, 16, 184, 0, 17, 208, 184, 0, 2, 16, 184, 0, 18, 208, 0, 184, 0, 0,
69, 88, 184, 0, 10, 47, 27, 185, 0, 10, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184,
0, 18, 47, 27, 185, 0, 18, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27,
185, 0, 0, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8, 0,
9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 12, 47, 27, 185, 0, 12, 0, 9, 62, 89, 184,
0, 0, 69, 88, 184, 0, 16, 47, 27, 185, 0, 16, 0, 9, 62, 89, 184, 0, 0, 69, 88,
184, 0, 4, 47, 27, 185, 0, 4, 0, 5, 62, 89, 184, 0, 0, 16, 185, 0, 2, 0, 1, 244,
184, 0, 3, 208, 184, 0, 6, 208, 184, 0, 7, 208, 184, 0, 14, 208, 184, 0, 15,
208, 48, 49, 1, 35, 21, 35, 17, 35, 17, 35, 53, 35, 17, 51, 17, 51, 21, 51, 53,
51, 17, 51, 3, 232, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 3, 232,
200, 252, 224, 3, 32, 200, 1, 144, 254, 112, 200, 200, 1, 144, 0, 1, 0, 0, 0,
0, 2, 88, 5, 120, 0, 15, 0, 193, 184, 0, 16, 47, 184, 0, 1, 208, 184, 0, 1, 47,
184, 0, 3, 220, 65, 3, 0, 111, 0, 3, 0, 1, 93, 65, 3, 0, 112, 0, 3, 0, 1, 93,
184, 0, 5, 220, 65, 3, 0, 111, 0, 5, 0, 1, 93, 65, 3, 0, 112, 0, 5, 0, 1, 93,
185, 0, 10, 0, 3, 244, 184, 0, 0, 208, 184, 0, 1, 16, 184, 0, 7, 208, 184, 0,
3, 16, 185, 0, 12, 0, 3, 244, 184, 0, 1, 16, 185, 0, 14, 0, 3, 244, 184, 0, 10,
16, 184, 0, 17, 220, 0, 184, 0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 9,
62, 89, 184, 0, 0, 69, 88, 184, 0, 10, 47, 27, 185, 0, 10, 0, 9, 62, 89, 184,
0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8, 0, 11, 62, 89, 184, 0, 0, 69, 88,
184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62, 89, 184, 0, 8, 16, 185, 0, 6, 0, 1, 244,
184, 0, 0, 16, 185, 0, 14, 0, 1, 244, 48, 49, 41, 1, 17, 51, 17, 51, 53, 33,
53, 33, 17, 35, 17, 35, 21, 33, 2, 88, 253, 168, 200, 200, 254, 112, 2, 88, 200,
200, 1, 144, 1, 144, 2, 88, 200, 200, 254, 112, 253, 168, 200, 0, 3, 0, 0, 254,
111, 1, 144, 7, 8, 0, 3, 0, 7, 0, 11, 0, 121, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43,
184, 0, 1, 16, 184, 0, 4, 208, 184, 0, 1, 16, 185, 0, 5, 0, 3, 244, 184, 0, 0,
16, 184, 0, 8, 208, 184, 0, 1, 16, 184, 0, 9, 208, 184, 0, 4, 16, 184, 0, 10,
208, 0, 184, 0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 7, 62, 89, 184, 0,
0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8, 0, 7, 62, 89, 187, 0, 3, 0, 1, 0, 0,
0, 4, 43, 184, 0, 3, 16, 184, 0, 6, 208, 184, 0, 4, 16, 185, 0, 10, 0, 1, 244,
184, 0, 11, 208, 48, 49, 1, 35, 53, 51, 3, 35, 17, 51, 19, 35, 53, 51, 1, 144,
200, 200, 200, 200, 200, 200, 200, 200, 6, 64, 200, 247, 104, 8, 152, 247, 103,
200, 0, 0, 1, 0, 1, 255, 56, 2, 89, 6, 64, 0, 11, 0, 139, 184, 0, 12, 47, 184,
0, 5, 208, 184, 0, 5, 47, 184, 0, 3, 220, 65, 3, 0, 111, 0, 3, 0, 1, 93, 65,
3, 0, 112, 0, 3, 0, 1, 93, 184, 0, 1, 220, 65, 3, 0, 111, 0, 1, 0, 1, 93, 65,
3, 0, 112, 0, 1, 0, 1, 93, 185, 0, 0, 0, 3, 244, 184, 0, 3, 16, 185, 0, 2, 0,
3, 244, 184, 0, 5, 16, 185, 0, 4, 0, 3, 244, 184, 0, 7, 208, 184, 0, 2, 16, 184,
0, 9, 208, 184, 0, 0, 16, 184, 0, 13, 220, 0, 184, 0, 0, 69, 88, 184, 0, 4, 47,
27, 185, 0, 4, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8,
0, 9, 62, 89, 48, 49, 5, 35, 17, 35, 17, 35, 17, 51, 17, 51, 17, 51, 2, 89, 200,
200, 200, 200, 200, 200, 200, 2, 88, 2, 88, 2, 88, 253, 168, 253, 168, 0, 3,
0, 0, 254, 111, 1, 144, 7, 8, 0, 3, 0, 7, 0, 11, 0, 117, 187, 0, 0, 0, 3, 0,
1, 0, 4, 43, 184, 0, 0, 16, 184, 0, 5, 208, 184, 0, 0, 16, 185, 0, 7, 0, 3, 244,
184, 0, 0, 16, 184, 0, 8, 208, 184, 0, 1, 16, 184, 0, 9, 208, 184, 0, 3, 16,
184, 0, 11, 208, 0, 184, 0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 7, 62,
89, 184, 0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8, 0, 7, 62, 89, 187, 0, 3,
0, 1, 0, 0, 0, 4, 43, 184, 0, 3, 16, 184, 0, 6, 208, 184, 0, 8, 16, 185, 0, 10,
0, 1, 244, 48, 49, 19, 35, 53, 51, 19, 35, 17, 51, 3, 35, 53, 51, 200, 200, 200,
200, 200, 200, 200, 200, 200, 6, 64, 200, 247, 104, 8, 152, 247, 103, 200, 0,
0, 0, 3, 0, 0, 4, 176, 2, 88, 6, 64, 0, 3, 0, 7, 0, 11, 0, 200, 184, 0, 12, 47,
184, 0, 9, 208, 184, 0, 9, 47, 184, 0, 1, 220, 65, 3, 0, 111, 0, 1, 0, 1, 93,
65, 3, 0, 112, 0, 1, 0, 1, 93, 184, 0, 0, 220, 184, 0, 1, 16, 185, 0, 0, 0, 3,
244, 65, 3, 0, 111, 0, 0, 0, 1, 93, 65, 3, 0, 112, 0, 0, 0, 1, 93, 184, 0, 5,
208, 184, 0, 0, 16, 185, 0, 7, 0, 3, 244, 184, 0, 9, 16, 185, 0, 8, 0, 3, 244,
184, 0, 7, 16, 184, 0, 13, 220, 0, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185,
0, 0, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 6, 47, 27, 185, 0, 6, 0, 11,
62, 89, 184, 0, 0, 69, 88, 184, 0, 10, 47, 27, 185, 0, 10, 0, 11, 62, 89, 184,
0, 0, 16, 185, 0, 3, 0, 1, 244, 184, 0, 0, 16, 185, 0, 4, 0, 1, 244, 184, 0,
0, 16, 184, 0, 7, 208, 184, 0, 4, 16, 184, 0, 8, 208, 184, 0, 9, 208, 184, 0,
0, 16, 184, 0, 11, 208, 48, 49, 1, 35, 53, 51, 19, 35, 53, 51, 5, 35, 53, 51,
1, 144, 200, 200, 200, 200, 200, 254, 112, 200, 200, 5, 120, 200, 254, 112, 200,
200, 200, 0, 0, 0, 0, 1, 0, 0, 255, 56, 3, 232, 0, 0, 0, 3, 0, 13, 0, 187, 0,
3, 0, 1, 0, 0, 0, 4, 43, 48, 49, 5, 33, 53, 33, 3, 232, 252, 24, 3, 232, 200,
200, 0, 0, 2, 0, 0, 0, 0, 3, 32, 3, 232, 0, 13, 0, 17, 0, 177, 184, 0, 18, 47,
184, 0, 8, 47, 184, 0, 18, 16, 184, 0, 3, 208, 184, 0, 3, 47, 185, 0, 2, 0, 3,
244, 184, 0, 5, 208, 184, 0, 2, 16, 184, 0, 9, 208, 184, 0, 8, 16, 184, 0, 11,
208, 184, 0, 8, 16, 185, 0, 13, 0, 3, 244, 184, 0, 8, 16, 184, 0, 14, 208, 184,
0, 2, 16, 184, 0, 16, 208, 184, 0, 13, 16, 184, 0, 19, 220, 0, 184, 0, 0, 69,
88, 184, 0, 10, 47, 27, 185, 0, 10, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0,
0, 47, 27, 185, 0, 0, 0, 5, 62, 89, 185, 0, 2, 0, 1, 244, 185, 0, 5, 0, 1, 244,
185, 0, 6, 0, 1, 244, 184, 0, 10, 16, 185, 0, 8, 0, 1, 244, 184, 0, 12, 208,
184, 0, 13, 208, 184, 0, 2, 16, 184, 0, 14, 208, 184, 0, 5, 16, 184, 0, 15, 208,
184, 0, 14, 16, 184, 0, 17, 208, 48, 49, 41, 1, 53, 35, 53, 51, 53, 33, 53, 33,
53, 33, 21, 51, 3, 53, 33, 21, 3, 32, 253, 168, 200, 200, 1, 144, 254, 112, 1,
144, 200, 200, 254, 112, 200, 200, 200, 200, 200, 200, 253, 168, 200, 200, 0,
0, 0, 0, 2, 0, 0, 0, 0, 3, 32, 5, 120, 0, 9, 0, 13, 0, 158, 184, 0, 14, 47, 184,
0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 14, 16, 184, 0, 3, 208, 184, 0, 3, 47,
185, 0, 13, 0, 3, 244, 184, 0, 5, 208, 184, 0, 1, 16, 184, 0, 7, 208, 184, 0,
1, 16, 184, 0, 10, 208, 184, 0, 0, 16, 184, 0, 15, 220, 0, 184, 0, 0, 69, 88,
184, 0, 4, 47, 27, 185, 0, 4, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 6, 47,
27, 185, 0, 6, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2,
0, 5, 62, 89, 185, 0, 0, 0, 1, 244, 184, 0, 6, 16, 185, 0, 8, 0, 1, 244, 184,
0, 0, 16, 184, 0, 10, 208, 184, 0, 8, 16, 184, 0, 11, 208, 184, 0, 12, 208, 184,
0, 10, 16, 184, 0, 13, 208, 48, 49, 37, 35, 21, 33, 17, 51, 17, 33, 21, 51, 3,
17, 33, 17, 3, 32, 200, 253, 168, 200, 1, 144, 200, 200, 254, 112, 200, 200,
5, 120, 254, 112, 200, 253, 168, 2, 88, 253, 168, 0, 0, 0, 0, 1, 0, 0, 0, 0,
2, 88, 3, 232, 0, 11, 0, 99, 187, 0, 2, 0, 3, 0, 3, 0, 4, 43, 184, 0, 2, 16,
184, 0, 5, 208, 184, 0, 2, 16, 184, 0, 9, 208, 0, 184, 0, 0, 69, 88, 184, 0,
6, 47, 27, 185, 0, 6, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185,
0, 0, 0, 5, 62, 89, 185, 0, 2, 0, 1, 244, 184, 0, 6, 16, 185, 0, 4, 0, 1, 244,
184, 0, 8, 208, 184, 0, 9, 208, 184, 0, 2, 16, 184, 0, 10, 208, 184, 0, 11, 208,
48, 49, 41, 1, 53, 35, 17, 51, 53, 33, 21, 33, 17, 33, 2, 88, 254, 112, 200,
200, 1, 144, 254, 112, 1, 144, 200, 2, 88, 200, 200, 253, 168, 0, 2, 0, 0, 0,
0, 3, 32, 5, 120, 0, 9, 0, 13, 0, 162, 184, 0, 14, 47, 184, 0, 8, 47, 184, 0,
14, 16, 184, 0, 3, 208, 184, 0, 3, 47, 185, 0, 2, 0, 3, 244, 184, 0, 5, 208,
184, 0, 8, 16, 185, 0, 9, 0, 3, 244, 184, 0, 8, 16, 184, 0, 10, 208, 184, 0,
2, 16, 184, 0, 12, 208, 184, 0, 9, 16, 184, 0, 15, 220, 0, 184, 0, 0, 69, 88,
184, 0, 8, 47, 27, 185, 0, 8, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 6, 47,
27, 185, 0, 6, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0,
0, 5, 62, 89, 185, 0, 2, 0, 1, 244, 184, 0, 6, 16, 185, 0, 4, 0, 1, 244, 184,
0, 2, 16, 184, 0, 10, 208, 184, 0, 4, 16, 184, 0, 11, 208, 184, 0, 12, 208, 184,
0, 10, 16, 184, 0, 13, 208, 48, 49, 41, 1, 53, 35, 17, 51, 53, 33, 17, 51, 3,
17, 33, 17, 3, 32, 253, 168, 200, 200, 1, 144, 200, 200, 254, 112, 200, 2, 88,
200, 1, 144, 251, 80, 2, 88, 253, 168, 0, 2, 0, 0, 0, 0, 3, 32, 3, 232, 0, 13,
0, 17, 0, 167, 184, 0, 18, 47, 184, 0, 3, 47, 185, 0, 0, 0, 3, 244, 184, 0, 18,
16, 184, 0, 7, 208, 184, 0, 7, 47, 185, 0, 2, 0, 3, 244, 184, 0, 5, 208, 184,
0, 2, 16, 184, 0, 9, 208, 184, 0, 3, 16, 184, 0, 11, 208, 184, 0, 3, 16, 184,
0, 14, 208, 184, 0, 2, 16, 184, 0, 16, 208, 184, 0, 0, 16, 184, 0, 19, 220, 0,
184, 0, 0, 69, 88, 184, 0, 10, 47, 27, 185, 0, 10, 0, 9, 62, 89, 184, 0, 0, 69,
88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 5, 62, 89, 187, 0, 14, 0, 1, 0, 0, 0, 4,
43, 184, 0, 4, 16, 185, 0, 2, 0, 1, 244, 184, 0, 6, 208, 184, 0, 7, 208, 184,
0, 10, 16, 185, 0, 8, 0, 1, 244, 184, 0, 12, 208, 184, 0, 13, 208, 184, 0, 15,
208, 184, 0, 16, 208, 48, 49, 1, 33, 21, 33, 21, 33, 53, 35, 17, 51, 53, 33,
21, 51, 7, 53, 33, 21, 3, 32, 253, 168, 1, 144, 254, 112, 200, 200, 1, 144, 200,
200, 254, 112, 1, 144, 200, 200, 200, 2, 88, 200, 200, 200, 200, 200, 0, 0, 0,
0, 1, 0, 0, 0, 0, 2, 88, 5, 120, 0, 11, 0, 130, 187, 0, 6, 0, 3, 0, 7, 0, 4,
43, 184, 0, 6, 16, 184, 0, 1, 208, 184, 0, 6, 16, 185, 0, 3, 0, 3, 244, 184,
0, 6, 16, 184, 0, 9, 208, 184, 0, 2, 16, 184, 0, 10, 208, 0, 184, 0, 0, 69, 88,
184, 0, 10, 47, 27, 185, 0, 10, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 2,
47, 27, 185, 0, 2, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 6, 47, 27, 185, 0,
6, 0, 5, 62, 89, 184, 0, 10, 16, 185, 0, 0, 0, 1, 244, 184, 0, 2, 16, 185, 0,
4, 0, 1, 244, 184, 0, 0, 16, 184, 0, 8, 208, 184, 0, 9, 208, 48, 49, 1, 33, 21,
51, 21, 35, 17, 35, 17, 51, 53, 33, 2, 88, 254, 112, 200, 200, 200, 200, 1, 144,
4, 176, 200, 200, 252, 224, 4, 176, 200, 0, 0, 2, 0, 0, 254, 112, 3, 32, 3, 232,
0, 15, 0, 19, 0, 212, 184, 0, 20, 47, 184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184,
0, 20, 16, 184, 0, 9, 208, 184, 0, 9, 47, 185, 0, 8, 0, 3, 244, 184, 0, 3, 208,
184, 0, 1, 16, 184, 0, 5, 208, 184, 0, 8, 16, 184, 0, 11, 208, 184, 0, 1, 16,
184, 0, 13, 208, 184, 0, 1, 16, 184, 0, 16, 208, 184, 0, 8, 16, 184, 0, 18, 208,
184, 0, 0, 16, 184, 0, 21, 220, 0, 184, 0, 0, 69, 88, 184, 0, 12, 47, 27, 185,
0, 12, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 7, 62,
89, 184, 0, 0, 69, 88, 184, 0, 6, 47, 27, 185, 0, 6, 0, 5, 62, 89, 184, 0, 2,
16, 185, 0, 0, 0, 1, 244, 184, 0, 4, 208, 184, 0, 5, 208, 184, 0, 6, 16, 185,
0, 8, 0, 1, 244, 184, 0, 12, 16, 185, 0, 10, 0, 1, 244, 184, 0, 14, 208, 184,
0, 15, 208, 184, 0, 8, 16, 184, 0, 16, 208, 184, 0, 15, 16, 184, 0, 17, 208,
184, 0, 18, 208, 184, 0, 16, 16, 184, 0, 19, 208, 48, 49, 5, 35, 21, 33, 53,
33, 53, 33, 53, 35, 17, 51, 53, 33, 21, 51, 3, 17, 33, 17, 3, 32, 200, 254, 112,
1, 144, 254, 112, 200, 200, 1, 144, 200, 200, 254, 112, 200, 200, 200, 200, 200,
2, 88, 200, 200, 253, 168, 2, 88, 253, 168, 0, 1, 0, 0, 0, 0, 3, 32, 5, 120,
0, 11, 0, 141, 184, 0, 12, 47, 184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 12,
16, 184, 0, 5, 208, 184, 0, 5, 47, 185, 0, 4, 0, 3, 244, 184, 0, 7, 208, 184,
0, 1, 16, 184, 0, 9, 208, 184, 0, 0, 16, 184, 0, 13, 220, 0, 184, 0, 0, 69, 88,
184, 0, 6, 47, 27, 185, 0, 6, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 8, 47,
27, 185, 0, 8, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0,
0, 5, 62, 89, 184, 0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 5, 62, 89,
184, 0, 8, 16, 185, 0, 2, 0, 1, 244, 184, 0, 10, 208, 184, 0, 11, 208, 48, 49,
33, 35, 17, 33, 17, 35, 17, 51, 17, 33, 21, 51, 3, 32, 200, 254, 112, 200, 200,
1, 144, 200, 3, 32, 252, 224, 5, 120, 254, 112, 200, 0, 0, 0, 2, 0, 0, 0, 0,
0, 200, 5, 120, 0, 3, 0, 7, 0, 90, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43, 184, 0, 0,
16, 184, 0, 4, 208, 184, 0, 1, 16, 184, 0, 5, 208, 0, 184, 0, 0, 69, 88, 184,
0, 6, 47, 27, 185, 0, 6, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27,
185, 0, 2, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0,
5, 62, 89, 184, 0, 2, 16, 185, 0, 0, 0, 1, 244, 48, 49, 19, 35, 53, 51, 17, 35,
17, 51, 200, 200, 200, 200, 200, 4, 176, 200, 250, 136, 3, 232, 0, 0, 0, 3, 255,
56, 254, 112, 0, 200, 5, 120, 0, 3, 0, 7, 0, 11, 0, 130, 187, 0, 0, 0, 3, 0,
1, 0, 4, 43, 184, 0, 0, 16, 184, 0, 4, 208, 184, 0, 1, 16, 184, 0, 5, 208, 184,
0, 1, 16, 184, 0, 8, 208, 184, 0, 6, 208, 184, 0, 1, 16, 185, 0, 9, 0, 3, 244,
0, 184, 0, 0, 69, 88, 184, 0, 6, 47, 27, 185, 0, 6, 0, 9, 62, 89, 184, 0, 0,
69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184,
0, 8, 47, 27, 185, 0, 8, 0, 7, 62, 89, 184, 0, 2, 16, 185, 0, 0, 0, 1, 244, 184,
0, 8, 16, 185, 0, 4, 0, 1, 244, 184, 0, 10, 208, 184, 0, 11, 208, 48, 49, 19,
35, 53, 51, 17, 35, 17, 51, 3, 35, 53, 51, 200, 200, 200, 200, 200, 200, 200,
200, 4, 176, 200, 249, 192, 4, 176, 250, 136, 200, 0, 0, 0, 1, 0, 0, 0, 0, 3,
32, 5, 120, 0, 23, 1, 5, 187, 0, 6, 0, 3, 0, 7, 0, 4, 43, 187, 0, 2, 0, 3, 0,
3, 0, 4, 43, 184, 0, 2, 16, 185, 0, 0, 0, 3, 244, 184, 0, 6, 16, 184, 0, 9, 208,
184, 0, 3, 16, 184, 0, 11, 208, 184, 0, 2, 16, 184, 0, 13, 208, 184, 0, 1, 16,
184, 0, 14, 208, 184, 0, 0, 16, 184, 0, 15, 208, 184, 0, 2, 16, 184, 0, 17, 208,
184, 0, 1, 16, 184, 0, 18, 208, 184, 0, 3, 16, 184, 0, 19, 208, 184, 0, 2, 16,
184, 0, 21, 208, 184, 0, 1, 16, 184, 0, 22, 208, 0, 184, 0, 0, 69, 88, 184, 0,
8, 47, 27, 185, 0, 8, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 14, 47, 27, 185,
0, 14, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62,
89, 184, 0, 0, 69, 88, 184, 0, 6, 47, 27, 185, 0, 6, 0, 5, 62, 89, 184, 0, 0,
16, 185, 0, 2, 0, 1, 244, 184, 0, 3, 208, 184, 0, 2, 16, 185, 0, 21, 0, 1, 244,
184, 0, 4, 208, 185, 0, 11, 0, 1, 244, 184, 0, 14, 16, 185, 0, 12, 0, 1, 244,
184, 0, 16, 208, 184, 0, 17, 208, 184, 0, 11, 16, 184, 0, 18, 208, 184, 0, 4,
16, 184, 0, 20, 208, 184, 0, 3, 16, 184, 0, 22, 208, 184, 0, 23, 208, 48, 49,
33, 35, 53, 35, 53, 35, 17, 35, 17, 51, 17, 51, 53, 51, 53, 51, 21, 35, 21, 35,
21, 51, 21, 51, 3, 32, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200,
200, 200, 200, 254, 112, 5, 120, 252, 224, 200, 200, 200, 200, 200, 200, 0, 0,
1, 0, 0, 0, 0, 0, 200, 5, 120, 0, 3, 0, 47, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43,
0, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 11, 62, 89, 184, 0, 0,
69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62, 89, 48, 49, 51, 35, 17, 51, 200,
200, 200, 5, 120, 0, 0, 0, 1, 0, 0, 0, 0, 5, 120, 3, 232, 0, 19, 1, 10, 184,
0, 20, 47, 184, 0, 9, 208, 184, 0, 9, 47, 184, 0, 5, 220, 65, 3, 0, 223, 0, 5,
0, 1, 93, 65, 3, 0, 160, 0, 5, 0, 1, 93, 65, 3, 0, 48, 0, 5, 0, 1, 93, 184, 0,
1, 220, 65, 3, 0, 223, 0, 1, 0, 1, 93, 65, 3, 0, 160, 0, 1, 0, 1, 93, 65, 3,
0, 48, 0, 1, 0, 1, 93, 185, 0, 0, 0, 3, 244, 184, 0, 5, 16, 185, 0, 4, 0, 3,
244, 184, 0, 9, 16, 185, 0, 8, 0, 3, 244, 184, 0, 11, 208, 184, 0, 5, 16, 184,
0, 13, 208, 184, 0, 4, 16, 184, 0, 15, 208, 184, 0, 1, 16, 184, 0, 17, 208, 184,
0, 0, 16, 184, 0, 21, 220, 0, 184, 0, 0, 69, 88, 184, 0, 12, 47, 27, 185, 0,
12, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 16, 47, 27, 185, 0, 16, 0, 9, 62,
89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62, 89, 184, 0, 0,
69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 5, 62, 89, 184, 0, 0, 69, 88, 184, 0,
8, 47, 27, 185, 0, 8, 0, 5, 62, 89, 184, 0, 16, 16, 185, 0, 2, 0, 1, 244, 184,
0, 6, 208, 184, 0, 7, 208, 184, 0, 10, 208, 184, 0, 11, 208, 184, 0, 14, 208,
184, 0, 15, 208, 184, 0, 18, 208, 184, 0, 19, 208, 48, 49, 33, 35, 17, 33, 17,
35, 17, 33, 17, 35, 17, 51, 53, 33, 21, 51, 53, 33, 21, 51, 5, 120, 200, 254,
112, 200, 254, 112, 200, 200, 1, 144, 200, 1, 144, 200, 3, 32, 252, 224, 3, 32,
252, 224, 3, 32, 200, 200, 200, 200, 0, 0, 0, 1, 0, 0, 0, 0, 3, 32, 3, 232, 0,
11, 0, 132, 184, 0, 12, 47, 184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 12,
16, 184, 0, 5, 208, 184, 0, 5, 47, 185, 0, 4, 0, 3, 244, 184, 0, 7, 208, 184,
0, 1, 16, 184, 0, 9, 208, 184, 0, 0, 16, 184, 0, 13, 220, 0, 184, 0, 0, 69, 88,
184, 0, 8, 47, 27, 185, 0, 8, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47,
27, 185, 0, 0, 0, 5, 62, 89, 184, 0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4,
0, 5, 62, 89, 184, 0, 8, 16, 185, 0, 2, 0, 1, 244, 184, 0, 6, 208, 184, 0, 7,
208, 184, 0, 10, 208, 184, 0, 11, 208, 48, 49, 33, 35, 17, 33, 17, 35, 17, 51,
53, 33, 21, 51, 3, 32, 200, 254, 112, 200, 200, 1, 144, 200, 3, 32, 252, 224,
3, 32, 200, 200, 0, 2, 0, 0, 0, 0, 3, 32, 3, 232, 0, 11, 0, 15, 0, 165, 184,
0, 16, 47, 184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 16, 16, 184, 0, 5, 208,
184, 0, 5, 47, 185, 0, 4, 0, 3, 244, 184, 0, 7, 208, 184, 0, 1, 16, 184, 0, 9,
208, 184, 0, 1, 16, 184, 0, 12, 208, 184, 0, 4, 16, 184, 0, 14, 208, 184, 0,
0, 16, 184, 0, 17, 220, 0, 184, 0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0, 8, 0,
9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 185,
0, 0, 0, 1, 244, 184, 0, 4, 208, 184, 0, 5, 208, 184, 0, 8, 16, 185, 0, 6, 0,
1, 244, 184, 0, 10, 208, 184, 0, 11, 208, 184, 0, 5, 16, 184, 0, 12, 208, 184,
0, 11, 16, 184, 0, 13, 208, 184, 0, 14, 208, 184, 0, 12, 16, 184, 0, 15, 208,
48, 49, 37, 35, 21, 33, 53, 35, 17, 51, 53, 33, 21, 51, 3, 17, 33, 17, 3, 32,
200, 254, 112, 200, 200, 1, 144, 200, 200, 254, 112, 200, 200, 200, 2, 88, 200,
200, 253, 168, 2, 88, 253, 168, 0, 0, 2, 0, 0, 254, 112, 3, 32, 3, 232, 0, 10,
0, 14, 0, 183, 184, 0, 15, 47, 184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 15,
16, 184, 0, 5, 208, 184, 0, 5, 47, 185, 0, 4, 0, 3, 244, 184, 0, 7, 208, 184,
0, 1, 16, 184, 0, 8, 208, 184, 0, 1, 16, 184, 0, 11, 208, 184, 0, 4, 16, 184,
0, 13, 208, 184, 0, 0, 16, 184, 0, 16, 220, 0, 184, 0, 0, 69, 88, 184, 0, 6,
47, 27, 185, 0, 6, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 8, 47, 27, 185, 0,
8, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 4, 47, 27, 185, 0, 4, 0, 7, 62, 89,
184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 185, 0, 0, 0,
1, 244, 184, 0, 8, 16, 185, 0, 9, 0, 1, 244, 184, 0, 0, 16, 184, 0, 11, 208,
184, 0, 9, 16, 184, 0, 12, 208, 184, 0, 13, 208, 184, 0, 11, 16, 184, 0, 14,
208, 48, 49, 37, 35, 21, 33, 17, 35, 17, 51, 33, 21, 51, 3, 17, 33, 17, 3, 32,
200, 254, 112, 200, 200, 1, 144, 200, 200, 254, 112, 200, 200, 254, 112, 5, 120,
200, 253, 168, 2, 88, 253, 168, 0, 2, 0, 0, 254, 112, 3, 232, 3, 232, 0, 13,
0, 17, 0, 188, 184, 0, 18, 47, 184, 0, 1, 47, 184, 0, 18, 16, 184, 0, 5, 208,
184, 0, 5, 47, 185, 0, 4, 0, 3, 244, 184, 0, 7, 208, 184, 0, 1, 16, 184, 0, 9,
208, 184, 0, 1, 16, 185, 0, 12, 0, 3, 244, 184, 0, 1, 16, 184, 0, 14, 208, 184,
0, 4, 16, 184, 0, 16, 208, 184, 0, 12, 16, 184, 0, 19, 220, 0, 184, 0, 0, 69,
88, 184, 0, 8, 47, 27, 185, 0, 8, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0,
47, 27, 185, 0, 0, 0, 7, 62, 89, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0,
2, 0, 5, 62, 89, 185, 0, 4, 0, 1, 244, 184, 0, 8, 16, 185, 0, 6, 0, 1, 244, 184,
0, 10, 208, 184, 0, 11, 208, 184, 0, 0, 16, 185, 0, 12, 0, 1, 244, 184, 0, 4,
16, 184, 0, 14, 208, 184, 0, 11, 16, 184, 0, 15, 208, 184, 0, 16, 208, 184, 0,
14, 16, 184, 0, 17, 208, 48, 49, 1, 33, 17, 33, 53, 35, 17, 51, 53, 33, 21, 51,
17, 51, 1, 17, 33, 17, 3, 232, 254, 112, 254, 112, 200, 200, 1, 144, 200, 200,
254, 112, 254, 112, 254, 112, 1, 144, 200, 2, 88, 200, 200, 252, 24, 1, 144,
2, 88, 253, 168, 0, 0, 1, 0, 0, 0, 0, 2, 88, 3, 232, 0, 7, 0, 73, 187, 0, 2,
0, 3, 0, 3, 0, 4, 43, 184, 0, 2, 16, 184, 0, 5, 208, 0, 184, 0, 0, 69, 88, 184,
0, 6, 47, 27, 185, 0, 6, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27,
185, 0, 2, 0, 5, 62, 89, 184, 0, 6, 16, 185, 0, 0, 0, 1, 244, 184, 0, 4, 208,
184, 0, 5, 208, 48, 49, 1, 33, 17, 35, 17, 51, 53, 33, 2, 88, 254, 112, 200,
200, 1, 144, 3, 32, 252, 224, 3, 32, 200, 0, 0, 0, 1, 0, 0, 0, 0, 2, 88, 3, 232,
0, 19, 0, 245, 184, 0, 20, 47, 184, 0, 4, 208, 184, 0, 4, 47, 184, 0, 7, 220,
184, 0, 4, 16, 185, 0, 7, 0, 3, 244, 65, 3, 0, 111, 0, 7, 0, 1, 93, 65, 3, 0,
112, 0, 7, 0, 1, 93, 184, 0, 1, 220, 65, 3, 0, 111, 0, 1, 0, 1, 93, 65, 3, 0,
112, 0, 1, 0, 1, 93, 185, 0, 0, 0, 3, 244, 184, 0, 7, 16, 185, 0, 6, 0, 3, 244,
184, 0, 4, 16, 184, 0, 9, 208, 184, 0, 7, 16, 184, 0, 11, 208, 184, 0, 0, 16,
184, 0, 13, 208, 184, 0, 7, 16, 184, 0, 15, 208, 184, 0, 6, 16, 184, 0, 17, 208,
184, 0, 0, 16, 184, 0, 21, 220, 0, 184, 0, 0, 69, 88, 184, 0, 12, 47, 27, 185,
0, 12, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62,
89, 185, 0, 0, 0, 1, 244, 184, 0, 4, 208, 184, 0, 5, 208, 184, 0, 0, 16, 185,
0, 19, 0, 1, 244, 184, 0, 6, 208, 185, 0, 17, 0, 1, 244, 184, 0, 8, 208, 184,
0, 12, 16, 185, 0, 10, 0, 1, 244, 184, 0, 8, 16, 185, 0, 11, 0, 1, 244, 184,
0, 10, 16, 184, 0, 14, 208, 184, 0, 15, 208, 184, 0, 8, 16, 184, 0, 16, 208,
48, 49, 37, 35, 21, 33, 53, 33, 53, 35, 53, 35, 53, 51, 53, 33, 21, 33, 21, 51,
21, 51, 2, 88, 200, 254, 112, 1, 144, 200, 200, 200, 1, 144, 254, 112, 200, 200,
200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 0, 0, 0, 0, 1, 0, 0, 0, 0,
2, 88, 5, 120, 0, 11, 0, 108, 187, 0, 2, 0, 3, 0, 3, 0, 4, 43, 184, 0, 2, 16,
184, 0, 5, 208, 184, 0, 2, 16, 184, 0, 9, 208, 0, 184, 0, 0, 69, 88, 184, 0,
4, 47, 27, 185, 0, 4, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 6, 47, 27, 185,
0, 6, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62,
89, 185, 0, 2, 0, 1, 244, 184, 0, 6, 16, 185, 0, 8, 0, 1, 244, 184, 0, 2, 16,
184, 0, 10, 208, 184, 0, 11, 208, 48, 49, 41, 1, 53, 35, 17, 51, 17, 51, 21,
35, 17, 33, 2, 88, 254, 112, 200, 200, 200, 200, 1, 144, 200, 4, 176, 254, 112,
200, 253, 168, 0, 1, 0, 0, 0, 0, 3, 32, 3, 232, 0, 11, 0, 128, 184, 0, 12, 47,
184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 12, 16, 184, 0, 5, 208, 184, 0,
5, 47, 185, 0, 4, 0, 3, 244, 184, 0, 7, 208, 184, 0, 1, 16, 184, 0, 9, 208, 184,
0, 0, 16, 184, 0, 13, 220, 0, 184, 0, 0, 69, 88, 184, 0, 6, 47, 27, 185, 0, 6,
0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 10, 47, 27, 185, 0, 10, 0, 9, 62, 89,
184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 185, 0, 0, 0,
1, 244, 184, 0, 4, 208, 184, 0, 5, 208, 184, 0, 8, 208, 184, 0, 9, 208, 48, 49,
37, 35, 21, 33, 53, 35, 17, 51, 17, 33, 17, 51, 3, 32, 200, 254, 112, 200, 200,
1, 144, 200, 200, 200, 200, 3, 32, 252, 224, 3, 32, 0, 0, 0, 0, 1, 0, 0, 0, 0,
3, 232, 3, 232, 0, 19, 0, 190, 187, 0, 8, 0, 3, 0, 9, 0, 4, 43, 187, 0, 2, 0,
3, 0, 3, 0, 4, 43, 184, 0, 2, 16, 185, 0, 0, 0, 3, 244, 184, 0, 3, 16, 185, 0,
5, 0, 3, 244, 184, 0, 8, 16, 185, 0, 6, 0, 3, 244, 184, 0, 8, 16, 184, 0, 11,
208, 184, 0, 7, 16, 184, 0, 12, 208, 184, 0, 6, 16, 184, 0, 13, 208, 184, 0,
5, 16, 184, 0, 14, 208, 184, 0, 3, 16, 184, 0, 15, 208, 184, 0, 4, 16, 184, 0,
16, 208, 184, 0, 2, 16, 184, 0, 17, 208, 184, 0, 1, 16, 184, 0, 18, 208, 0, 184,
0, 0, 69, 88, 184, 0, 10, 47, 27, 185, 0, 10, 0, 9, 62, 89, 184, 0, 0, 69, 88,
184, 0, 18, 47, 27, 185, 0, 18, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 4, 47,
27, 185, 0, 4, 0, 5, 62, 89, 185, 0, 2, 0, 1, 244, 184, 0, 6, 208, 184, 0, 7,
208, 184, 0, 14, 208, 184, 0, 15, 208, 48, 49, 1, 35, 17, 35, 21, 35, 53, 35,
17, 35, 17, 51, 17, 51, 17, 51, 17, 51, 17, 51, 3, 232, 200, 200, 200, 200, 200,
200, 200, 200, 200, 200, 2, 88, 254, 112, 200, 200, 1, 144, 1, 144, 254, 112,
254, 112, 1, 144, 1, 144, 0, 0, 0, 1, 0, 0, 0, 0, 5, 120, 3, 232, 0, 19, 1, 10,
184, 0, 20, 47, 184, 0, 9, 208, 184, 0, 9, 47, 184, 0, 5, 220, 65, 3, 0, 223,
0, 5, 0, 1, 93, 65, 3, 0, 160, 0, 5, 0, 1, 93, 65, 3, 0, 48, 0, 5, 0, 1, 93,
184, 0, 1, 220, 65, 3, 0, 223, 0, 1, 0, 1, 93, 65, 3, 0, 160, 0, 1, 0, 1, 93,
65, 3, 0, 48, 0, 1, 0, 1, 93, 185, 0, 0, 0, 3, 244, 184, 0, 5, 16, 185, 0, 4,
0, 3, 244, 184, 0, 9, 16, 185, 0, 8, 0, 3, 244, 184, 0, 11, 208, 184, 0, 5, 16,
184, 0, 13, 208, 184, 0, 4, 16, 184, 0, 15, 208, 184, 0, 1, 16, 184, 0, 17, 208,
184, 0, 0, 16, 184, 0, 21, 220, 0, 184, 0, 0, 69, 88, 184, 0, 10, 47, 27, 185,
0, 10, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 14, 47, 27, 185, 0, 14, 0, 9,
62, 89, 184, 0, 0, 69, 88, 184, 0, 18, 47, 27, 185, 0, 18, 0, 9, 62, 89, 184,
0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 184, 0, 0, 69, 88,
184, 0, 6, 47, 27, 185, 0, 6, 0, 5, 62, 89, 184, 0, 2, 16, 185, 0, 0, 0, 1, 244,
184, 0, 4, 208, 184, 0, 5, 208, 184, 0, 8, 208, 184, 0, 9, 208, 184, 0, 12, 208,
184, 0, 13, 208, 184, 0, 16, 208, 184, 0, 17, 208, 48, 49, 37, 35, 21, 33, 53,
35, 21, 33, 53, 35, 17, 51, 17, 33, 17, 51, 17, 33, 17, 51, 5, 120, 200, 254,
112, 200, 254, 112, 200, 200, 1, 144, 200, 1, 144, 200, 200, 200, 200, 200, 200,
3, 32, 252, 224, 3, 32, 252, 224, 3, 32, 0, 0, 1, 0, 0, 0, 0, 3, 232, 3, 232,
0, 35, 1, 189, 187, 0, 6, 0, 3, 0, 7, 0, 4, 43, 187, 0, 2, 0, 3, 0, 3, 0, 4,
43, 184, 0, 2, 16, 185, 0, 0, 0, 3, 244, 184, 0, 6, 16, 185, 0, 4, 0, 3, 244,
184, 0, 7, 16, 185, 0, 9, 0, 3, 244, 184, 0, 7, 16, 184, 0, 11, 208, 184, 0,
8, 16, 184, 0, 12, 208, 184, 0, 6, 16, 184, 0, 13, 208, 184, 0, 5, 16, 184, 0,
14, 208, 184, 0, 7, 16, 184, 0, 15, 208, 184, 0, 8, 16, 184, 0, 16, 208, 184,
0, 9, 16, 184, 0, 17, 208, 184, 0, 7, 16, 184, 0, 19, 208, 184, 0, 8, 16, 184,
0, 20, 208, 184, 0, 6, 16, 184, 0, 21, 208, 184, 0, 5, 16, 184, 0, 22, 208, 184,
0, 3, 16, 184, 0, 23, 208, 184, 0, 4, 16, 184, 0, 24, 208, 184, 0, 2, 16, 184,
0, 25, 208, 184, 0, 1, 16, 184, 0, 26, 208, 184, 0, 0, 16, 184, 0, 27, 208, 184,
0, 2, 16, 184, 0, 29, 208, 184, 0, 1, 16, 184, 0, 30, 208, 184, 0, 3, 16, 184,
0, 31, 208, 184, 0, 4, 16, 184, 0, 32, 208, 184, 0, 2, 16, 184, 0, 33, 208, 184,
0, 1, 16, 184, 0, 34, 208, 0, 184, 0, 0, 69, 88, 184, 0, 18, 47, 27, 185, 0,
18, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 26, 47, 27, 185, 0, 26, 0, 9, 62,
89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62, 89, 184, 0, 0,
69, 88, 184, 0, 8, 47, 27, 185, 0, 8, 0, 5, 62, 89, 184, 0, 0, 16, 185, 0, 2,
0, 1, 244, 185, 0, 33, 0, 1, 244, 184, 0, 4, 208, 184, 0, 2, 16, 184, 0, 6, 208,
184, 0, 7, 208, 184, 0, 10, 208, 184, 0, 11, 208, 184, 0, 33, 16, 184, 0, 12,
208, 184, 0, 4, 16, 184, 0, 13, 208, 184, 0, 4, 16, 185, 0, 23, 0, 1, 244, 184,
0, 14, 208, 184, 0, 18, 16, 185, 0, 16, 0, 1, 244, 184, 0, 20, 208, 184, 0, 21,
208, 184, 0, 17, 208, 184, 0, 21, 16, 184, 0, 24, 208, 184, 0, 25, 208, 184,
0, 28, 208, 184, 0, 29, 208, 184, 0, 23, 16, 184, 0, 30, 208, 184, 0, 14, 16,
184, 0, 31, 208, 184, 0, 4, 16, 184, 0, 32, 208, 184, 0, 11, 16, 184, 0, 34,
208, 184, 0, 35, 208, 48, 49, 33, 35, 53, 35, 53, 35, 21, 35, 21, 35, 53, 51,
53, 51, 53, 35, 53, 35, 53, 51, 21, 51, 21, 51, 53, 51, 53, 51, 21, 35, 21, 35,
21, 51, 21, 51, 3, 232, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200,
200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200,
200, 200, 200, 200, 200, 200, 200, 200, 0, 1, 0, 0, 254, 112, 3, 32, 3, 232,
0, 15, 0, 175, 184, 0, 16, 47, 184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 16,
16, 184, 0, 9, 208, 184, 0, 9, 47, 185, 0, 8, 0, 3, 244, 184, 0, 3, 208, 184,
0, 1, 16, 184, 0, 5, 208, 184, 0, 8, 16, 184, 0, 11, 208, 184, 0, 1, 16, 184,
0, 13, 208, 184, 0, 0, 16, 184, 0, 17, 220, 0, 184, 0, 0, 69, 88, 184, 0, 10,
47, 27, 185, 0, 10, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 14, 47, 27, 185,
0, 14, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 7, 62,
89, 184, 0, 0, 69, 88, 184, 0, 6, 47, 27, 185, 0, 6, 0, 5, 62, 89, 184, 0, 2,
16, 185, 0, 0, 0, 1, 244, 184, 0, 4, 208, 184, 0, 5, 208, 184, 0, 6, 16, 185,
0, 8, 0, 1, 244, 184, 0, 12, 208, 184, 0, 13, 208, 48, 49, 5, 35, 21, 33, 53,
33, 53, 33, 53, 35, 17, 51, 17, 33, 17, 51, 3, 32, 200, 254, 112, 1, 144, 254,
112, 200, 200, 1, 144, 200, 200, 200, 200, 200, 200, 3, 32, 252, 224, 3, 32,
0, 0, 0, 1, 0, 0, 0, 0, 2, 88, 3, 232, 0, 17, 0, 193, 184, 0, 18, 47, 184, 0,
2, 208, 184, 0, 2, 47, 184, 0, 4, 220, 65, 3, 0, 111, 0, 4, 0, 1, 93, 65, 3,
0, 112, 0, 4, 0, 1, 93, 184, 0, 6, 220, 65, 3, 0, 111, 0, 6, 0, 1, 93, 65, 3,
0, 112, 0, 6, 0, 1, 93, 185, 0, 12, 0, 3, 244, 184, 0, 0, 208, 184, 0, 2, 16,
184, 0, 8, 208, 184, 0, 12, 16, 184, 0, 10, 208, 184, 0, 4, 16, 185, 0, 14, 0,
3, 244, 184, 0, 2, 16, 185, 0, 16, 0, 3, 244, 184, 0, 12, 16, 184, 0, 19, 220,
0, 184, 0, 0, 69, 88, 184, 0, 9, 47, 27, 185, 0, 9, 0, 9, 62, 89, 184, 0, 0,
69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 5, 62, 89, 187, 0, 5, 0, 1, 0, 4, 0,
4, 43, 184, 0, 9, 16, 185, 0, 7, 0, 1, 244, 184, 0, 5, 16, 184, 0, 12, 208, 184,
0, 4, 16, 184, 0, 14, 208, 184, 0, 0, 16, 185, 0, 16, 0, 1, 244, 48, 49, 41,
1, 61, 1, 51, 53, 51, 53, 33, 53, 33, 29, 1, 35, 21, 35, 21, 33, 2, 88, 253,
168, 200, 200, 254, 112, 2, 88, 200, 200, 1, 144, 200, 200, 200, 200, 200, 200,
200, 200, 200, 0, 1, 0, 0, 255, 56, 2, 88, 6, 64, 0, 19, 1, 15, 184, 0, 20, 47,
184, 0, 5, 208, 184, 0, 5, 47, 184, 0, 3, 220, 65, 3, 0, 111, 0, 3, 0, 1, 93,
65, 3, 0, 112, 0, 3, 0, 1, 93, 184, 0, 1, 220, 65, 3, 0, 111, 0, 1, 0, 1, 93,
65, 3, 0, 112, 0, 1, 0, 1, 93, 185, 0, 0, 0, 3, 244, 184, 0, 3, 16, 185, 0, 2,
0, 3, 244, 184, 0, 5, 16, 185, 0, 4, 0, 3, 244, 184, 0, 3, 16, 184, 0, 7, 208,
184, 0, 1, 16, 184, 0, 9, 208, 184, 0, 0, 16, 184, 0, 12, 208, 184, 0, 2, 16,
184, 0, 13, 208, 184, 0, 4, 16, 184, 0, 15, 208, 184, 0, 2, 16, 184, 0, 17, 208,
184, 0, 0, 16, 184, 0, 21, 220, 0, 184, 0, 0, 69, 88, 184, 0, 8, 47, 27, 185,
0, 8, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 12, 47, 27, 185, 0, 12, 0, 11,
62, 89, 184, 0, 0, 69, 88, 184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 184, 0,
0, 69, 88, 184, 0, 18, 47, 27, 185, 0, 18, 0, 5, 62, 89, 187, 0, 19, 0, 1, 0,
0, 0, 4, 43, 187, 0, 10, 0, 1, 0, 9, 0, 4, 43, 187, 0, 7, 0, 1, 0, 4, 0, 4, 43,
184, 0, 19, 16, 184, 0, 3, 208, 184, 0, 9, 16, 184, 0, 13, 208, 184, 0, 7, 16,
184, 0, 14, 208, 184, 0, 4, 16, 184, 0, 16, 208, 48, 49, 5, 35, 53, 35, 17, 35,
53, 51, 17, 51, 53, 51, 21, 35, 17, 35, 21, 51, 17, 51, 2, 88, 200, 200, 200,
200, 200, 200, 200, 200, 200, 200, 200, 200, 2, 88, 200, 2, 88, 200, 200, 253,
168, 200, 253, 168, 0, 0, 1, 0, 200, 254, 112, 1, 144, 7, 8, 0, 3, 0, 30, 187,
0, 0, 0, 3, 0, 1, 0, 4, 43, 0, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0,
0, 0, 7, 62, 89, 48, 49, 1, 35, 17, 51, 1, 144, 200, 200, 254, 112, 8, 152, 0,
1, 0, 0, 255, 56, 2, 88, 6, 64, 0, 19, 1, 11, 184, 0, 20, 47, 184, 0, 5, 208,
184, 0, 5, 47, 184, 0, 3, 220, 65, 3, 0, 111, 0, 3, 0, 1, 93, 65, 3, 0, 112,
0, 3, 0, 1, 93, 184, 0, 1, 220, 65, 3, 0, 111, 0, 1, 0, 1, 93, 65, 3, 0, 112,
0, 1, 0, 1, 93, 185, 0, 0, 0, 3, 244, 184, 0, 3, 16, 185, 0, 2, 0, 3, 244, 184,
0, 5, 16, 185, 0, 4, 0, 3, 244, 184, 0, 7, 208, 184, 0, 2, 16, 184, 0, 9, 208,
184, 0, 4, 16, 184, 0, 11, 208, 184, 0, 5, 16, 184, 0, 13, 208, 184, 0, 4, 16,
184, 0, 15, 208, 184, 0, 2, 16, 184, 0, 17, 208, 184, 0, 0, 16, 184, 0, 21, 220,
0, 184, 0, 0, 69, 88, 184, 0, 12, 47, 27, 185, 0, 12, 0, 11, 62, 89, 184, 0,
0, 69, 88, 184, 0, 16, 47, 27, 185, 0, 16, 0, 11, 62, 89, 184, 0, 0, 69, 88,
184, 0, 2, 47, 27, 185, 0, 2, 0, 5, 62, 89, 184, 0, 0, 69, 88, 184, 0, 6, 47,
27, 185, 0, 6, 0, 5, 62, 89, 187, 0, 3, 0, 1, 0, 4, 0, 4, 43, 187, 0, 19, 0,
1, 0, 0, 0, 4, 43, 184, 0, 3, 16, 184, 0, 7, 208, 184, 0, 0, 16, 184, 0, 8, 208,
184, 0, 19, 16, 184, 0, 10, 208, 184, 0, 12, 16, 185, 0, 15, 0, 1, 244, 184,
0, 12, 16, 184, 0, 17, 208, 48, 49, 1, 35, 17, 35, 21, 35, 53, 51, 17, 51, 53,
35, 17, 35, 53, 51, 21, 51, 17, 51, 2, 88, 200, 200, 200, 200, 200, 200, 200,
200, 200, 200, 2, 88, 253, 168, 200, 200, 2, 88, 200, 2, 88, 200, 200, 253, 168,
0, 5, 0, 0, 0, 200, 3, 232, 3, 32, 0, 3, 0, 7, 0, 11, 0, 15, 0, 19, 0, 183, 187,
0, 16, 0, 3, 0, 17, 0, 4, 43, 187, 0, 11, 0, 3, 0, 4, 0, 4, 43, 187, 0, 0, 0,
3, 0, 1, 0, 4, 43, 184, 0, 16, 16, 184, 0, 5, 208, 184, 0, 4, 16, 184, 0, 9,
208, 184, 0, 1, 16, 184, 0, 12, 208, 184, 0, 11, 16, 184, 0, 13, 208, 0, 187,
0, 1, 0, 1, 0, 12, 0, 4, 43, 184, 0, 1, 16, 185, 0, 3, 0, 1, 244, 184, 0, 4,
208, 184, 0, 2, 208, 184, 0, 4, 16, 185, 0, 7, 0, 1, 244, 184, 0, 1, 16, 184,
0, 8, 208, 184, 0, 0, 16, 184, 0, 9, 208, 184, 0, 3, 16, 184, 0, 10, 208, 184,
0, 4, 16, 184, 0, 11, 208, 184, 0, 1, 16, 184, 0, 14, 208, 184, 0, 0, 16, 184,
0, 15, 208, 184, 0, 1, 16, 184, 0, 16, 208, 184, 0, 0, 16, 184, 0, 17, 208, 184,
0, 3, 16, 184, 0, 18, 208, 184, 0, 4, 16, 184, 0, 19, 208, 48, 49, 1, 35, 53,
51, 33, 35, 53, 51, 19, 35, 53, 51, 19, 35, 53, 51, 33, 35, 53, 51, 3, 232, 200,
200, 253, 168, 200, 200, 200, 200, 200, 200, 200, 200, 253, 168, 200, 200, 1,
144, 200, 200, 254, 112, 200, 254, 112, 200, 200, 0, 0, 0, 1, 0, 0, 0, 0, 3,
32, 5, 120, 0, 23, 0, 241, 187, 0, 4, 0, 3, 0, 5, 0, 4, 43, 184, 0, 4, 16, 185,
0, 2, 0, 3, 244, 184, 0, 4, 16, 184, 0, 7, 208, 184, 0, 3, 16, 184, 0, 8, 208,
184, 0, 2, 16, 184, 0, 9, 208, 184, 0, 2, 16, 184, 0, 13, 208, 184, 0, 4, 16,
184, 0, 15, 208, 184, 0, 3, 16, 184, 0, 16, 208, 184, 0, 4, 16, 184, 0, 19, 208,
184, 0, 3, 16, 184, 0, 20, 208, 184, 0, 2, 16, 184, 0, 21, 208, 0, 184, 0, 0,
69, 88, 184, 0, 6, 47, 27, 185, 0, 6, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0,
14, 47, 27, 185, 0, 14, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 10, 47, 27,
185, 0, 10, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0, 0,
5, 62, 89, 187, 0, 17, 0, 1, 0, 18, 0, 4, 43, 184, 0, 0, 16, 185, 0, 2, 0, 1,
244, 185, 0, 21, 0, 1, 244, 184, 0, 4, 208, 184, 0, 10, 16, 185, 0, 8, 0, 1,
244, 185, 0, 7, 0, 1, 244, 184, 0, 8, 16, 184, 0, 12, 208, 184, 0, 13, 208, 184,
0, 7, 16, 184, 0, 15, 208, 184, 0, 2, 16, 184, 0, 22, 208, 184, 0, 23, 208, 48,
49, 41, 1, 53, 35, 53, 35, 17, 51, 53, 51, 53, 33, 21, 33, 21, 35, 21, 33, 21,
33, 21, 51, 21, 33, 3, 32, 254, 112, 200, 200, 200, 200, 1, 144, 254, 112, 200,
1, 144, 254, 112, 200, 1, 144, 200, 200, 2, 88, 200, 200, 200, 200, 200, 200,
200, 200, 0, 0, 1, 0, 0, 4, 176, 0, 200, 6, 64, 0, 3, 0, 23, 187, 0, 0, 0, 3,
0, 1, 0, 4, 43, 0, 187, 0, 3, 0, 2, 0, 0, 0, 4, 43, 48, 49, 19, 35, 17, 51, 200,
200, 200, 4, 176, 1, 144, 0, 1, 0, 0, 4, 176, 0, 200, 6, 64, 0, 3, 0, 23, 187,
0, 0, 0, 3, 0, 1, 0, 4, 43, 0, 187, 0, 3, 0, 2, 0, 0, 0, 4, 43, 48, 49, 19, 35,
17, 51, 200, 200, 200, 4, 176, 1, 144, 0, 2, 0, 0, 4, 176, 2, 88, 6, 64, 0, 3,
0, 7, 0, 61, 184, 0, 8, 47, 184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 8, 16,
184, 0, 5, 208, 184, 0, 5, 47, 185, 0, 4, 0, 3, 244, 0, 187, 0, 3, 0, 2, 0, 0,
0, 4, 43, 184, 0, 0, 16, 184, 0, 4, 208, 184, 0, 3, 16, 184, 0, 6, 208, 48, 49,
1, 35, 17, 51, 1, 35, 17, 51, 2, 88, 200, 200, 254, 112, 200, 200, 4, 176, 1,
144, 254, 112, 1, 144, 0, 0, 0, 0, 2, 0, 0, 4, 176, 2, 88, 6, 64, 0, 3, 0, 7,
0, 61, 184, 0, 8, 47, 184, 0, 1, 47, 185, 0, 0, 0, 3, 244, 184, 0, 8, 16, 184,
0, 5, 208, 184, 0, 5, 47, 185, 0, 4, 0, 3, 244, 0, 187, 0, 3, 0, 2, 0, 0, 0,
4, 43, 184, 0, 0, 16, 184, 0, 4, 208, 184, 0, 3, 16, 184, 0, 6, 208, 48, 49,
1, 35, 17, 51, 1, 35, 17, 51, 2, 88, 200, 200, 254, 112, 200, 200, 4, 176, 1,
144, 254, 112, 1, 144, 0, 0, 0, 0, 1, 0, 56, 1, 163, 4, 28, 2, 27, 0, 3, 0, 13,
0, 187, 0, 3, 0, 1, 0, 0, 0, 4, 43, 48, 49, 1, 33, 53, 33, 4, 28, 252, 28, 3,
228, 1, 163, 120, 0, 1, 0, 0, 1, 163, 8, 0, 2, 27, 0, 3, 0, 13, 0, 187, 0, 3,
0, 1, 0, 0, 0, 4, 43, 48, 49, 1, 33, 53, 33, 8, 0, 248, 0, 8, 0, 1, 163, 120,
255, 255, 0, 0, 0, 0, 0, 0, 0, 0, 2, 6, 0, 3, 0, 0, 0, 1, 0, 0, 0, 0, 3, 32,
5, 120, 0, 15, 0, 113, 187, 0, 14, 0, 3, 0, 3, 0, 4, 43, 184, 0, 14, 16, 184,
0, 5, 208, 184, 0, 14, 16, 184, 0, 9, 208, 0, 184, 0, 0, 69, 88, 184, 0, 6, 47,
27, 185, 0, 6, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27, 185, 0, 0,
0, 5, 62, 89, 187, 0, 11, 0, 1, 0, 12, 0, 4, 43, 184, 0, 0, 16, 185, 0, 2, 0,
1, 244, 184, 0, 6, 16, 185, 0, 4, 0, 1, 244, 184, 0, 8, 208, 184, 0, 9, 208,
184, 0, 2, 16, 184, 0, 14, 208, 184, 0, 15, 208, 48, 49, 41, 1, 53, 51, 17, 51,
53, 33, 21, 33, 17, 51, 21, 35, 17, 33, 3, 32, 252, 224, 200, 200, 1, 144, 254,
112, 200, 200, 1, 144, 200, 3, 232, 200, 200, 254, 112, 200, 254, 112, 0, 0,
12, 0, 0, 0, 0, 5, 120, 5, 120, 0, 3, 0, 7, 0, 11, 0, 15, 0, 19, 0, 23, 0, 27,
0, 31, 0, 35, 0, 39, 0, 43, 0, 47, 2, 53, 187, 0, 32, 0, 3, 0, 33, 0, 4, 43,
187, 0, 14, 0, 3, 0, 20, 0, 4, 43, 187, 0, 11, 0, 3, 0, 0, 0, 4, 43, 184, 0,
0, 16, 185, 0, 1, 0, 3, 244, 184, 0, 4, 208, 184, 0, 20, 16, 184, 0, 5, 208,
184, 0, 0, 16, 184, 0, 9, 208, 184, 0, 1, 16, 184, 0, 12, 208, 184, 0, 0, 16,
184, 0, 16, 208, 184, 0, 1, 16, 184, 0, 17, 208, 184, 0, 32, 16, 184, 0, 21,
208, 184, 0, 1, 16, 184, 0, 24, 208, 184, 0, 14, 16, 184, 0, 25, 208, 184, 0,
14, 16, 184, 0, 28, 208, 184, 0, 20, 16, 184, 0, 29, 208, 184, 0, 1, 16, 184,
0, 36, 208, 184, 0, 20, 16, 184, 0, 37, 208, 184, 0, 20, 16, 184, 0, 40, 208,
184, 0, 32, 16, 184, 0, 41, 208, 184, 0, 20, 16, 184, 0, 44, 208, 184, 0, 32,
16, 184, 0, 45, 208, 184, 0, 11, 16, 184, 0, 49, 220, 0, 184, 0, 0, 69, 88, 184,
0, 6, 47, 27, 185, 0, 6, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 0, 47, 27,
185, 0, 0, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 10, 47, 27, 185, 0, 10, 0,
9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 14, 47, 27, 185, 0, 14, 0, 9, 62, 89, 184,
0, 0, 69, 88, 184, 0, 20, 47, 27, 185, 0, 20, 0, 9, 62, 89, 184, 0, 0, 69, 88,
184, 0, 34, 47, 27, 185, 0, 34, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 44,
47, 27, 185, 0, 44, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 36, 47, 27, 185,
0, 36, 0, 5, 62, 89, 184, 0, 6, 16, 185, 0, 2, 0, 1, 244, 184, 0, 0, 16, 185,
0, 3, 0, 1, 244, 184, 0, 2, 16, 184, 0, 4, 208, 184, 0, 5, 208, 184, 0, 36, 16,
185, 0, 16, 0, 1, 244, 185, 0, 9, 0, 1, 244, 184, 0, 18, 208, 184, 0, 8, 208,
184, 0, 0, 16, 184, 0, 11, 208, 184, 0, 0, 16, 185, 0, 12, 0, 1, 244, 184, 0,
13, 208, 184, 0, 0, 16, 184, 0, 15, 208, 184, 0, 0, 16, 184, 0, 21, 208, 184,
0, 5, 16, 184, 0, 22, 208, 184, 0, 23, 208, 184, 0, 9, 16, 184, 0, 24, 208, 184,
0, 18, 16, 184, 0, 25, 208, 184, 0, 13, 16, 185, 0, 26, 0, 1, 244, 184, 0, 18,
16, 185, 0, 27, 0, 1, 244, 184, 0, 28, 208, 184, 0, 26, 16, 184, 0, 29, 208,
184, 0, 13, 16, 184, 0, 30, 208, 184, 0, 31, 208, 184, 0, 9, 16, 184, 0, 32,
208, 184, 0, 18, 16, 184, 0, 33, 208, 184, 0, 0, 16, 184, 0, 35, 208, 184, 0,
16, 16, 184, 0, 38, 208, 184, 0, 39, 208, 184, 0, 40, 208, 184, 0, 41, 208, 184,
0, 9, 16, 184, 0, 42, 208, 184, 0, 18, 16, 184, 0, 43, 208, 184, 0, 0, 16, 184,
0, 45, 208, 184, 0, 23, 16, 184, 0, 46, 208, 184, 0, 47, 208, 48, 49, 1, 35,
53, 51, 35, 33, 53, 33, 1, 35, 17, 51, 5, 33, 53, 33, 19, 35, 53, 51, 1, 35,
53, 51, 1, 33, 53, 41, 1, 35, 53, 51, 1, 35, 17, 51, 1, 33, 53, 41, 1, 35, 53,
51, 17, 35, 53, 51, 4, 176, 200, 200, 200, 253, 168, 2, 88, 1, 144, 200, 200,
254, 112, 254, 112, 1, 144, 200, 200, 200, 252, 224, 200, 200, 2, 88, 254, 112,
1, 144, 254, 112, 200, 200, 254, 112, 200, 200, 3, 32, 253, 168, 2, 88, 253,
168, 200, 200, 200, 200, 3, 232, 200, 200, 252, 24, 2, 88, 200, 200, 252, 224,
200, 2, 88, 200, 252, 224, 200, 200, 254, 112, 2, 88, 252, 24, 200, 200, 2, 88,
200, 0, 6, 0, 0, 0, 200, 3, 232, 3, 32, 0, 3, 0, 7, 0, 11, 0, 15, 0, 19, 0, 23,
0, 229, 187, 0, 12, 0, 3, 0, 13, 0, 4, 43, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43, 184,
0, 1, 16, 184, 0, 4, 208, 184, 0, 1, 16, 185, 0, 5, 0, 3, 244, 184, 0, 0, 16,
184, 0, 8, 208, 184, 0, 1, 16, 184, 0, 9, 208, 184, 0, 4, 16, 184, 0, 10, 208,
184, 0, 13, 16, 184, 0, 16, 208, 184, 0, 13, 16, 185, 0, 17, 0, 3, 244, 184,
0, 12, 16, 184, 0, 20, 208, 184, 0, 13, 16, 184, 0, 21, 208, 184, 0, 16, 16,
184, 0, 22, 208, 0, 187, 0, 11, 0, 1, 0, 8, 0, 4, 43, 187, 0, 3, 0, 1, 0, 0,
0, 4, 43, 184, 0, 11, 16, 184, 0, 4, 208, 185, 0, 1, 0, 1, 244, 184, 0, 0, 16,
184, 0, 6, 208, 184, 0, 0, 16, 184, 0, 12, 208, 184, 0, 1, 16, 184, 0, 13, 208,
184, 0, 3, 16, 184, 0, 14, 208, 184, 0, 11, 16, 184, 0, 16, 208, 184, 0, 4, 16,
184, 0, 17, 208, 184, 0, 0, 16, 184, 0, 18, 208, 184, 0, 1, 16, 184, 0, 19, 208,
184, 0, 8, 16, 184, 0, 20, 208, 184, 0, 11, 16, 184, 0, 22, 208, 184, 0, 4, 16,
184, 0, 23, 208, 48, 49, 1, 35, 53, 51, 3, 35, 53, 51, 19, 35, 53, 51, 37, 35,
53, 51, 3, 35, 53, 51, 19, 35, 53, 51, 3, 232, 200, 200, 200, 200, 200, 200,
200, 200, 253, 168, 200, 200, 200, 200, 200, 200, 200, 200, 2, 88, 200, 254,
112, 200, 254, 112, 200, 200, 200, 254, 112, 200, 254, 112, 200, 0, 0, 0, 1,
0, 87, 1, 19, 3, 159, 2, 255, 0, 6, 0, 21, 186, 0, 0, 0, 2, 0, 3, 43, 0, 187,
0, 6, 0, 1, 0, 3, 0, 4, 43, 48, 49, 1, 7, 35, 17, 33, 53, 33, 3, 159, 1, 98,
253, 27, 3, 72, 1, 20, 1, 1, 136, 100, 255, 255, 0, 0, 1, 144, 2, 88, 2, 88,
2, 6, 0, 16, 0, 0, 0, 11, 0, 0, 0, 0, 5, 120, 5, 120, 0, 3, 0, 7, 0, 11, 0, 15,
0, 19, 0, 23, 0, 27, 0, 31, 0, 35, 0, 39, 0, 43, 1, 237, 187, 0, 28, 0, 3, 0,
29, 0, 4, 43, 187, 0, 14, 0, 3, 0, 20, 0, 4, 43, 187, 0, 11, 0, 3, 0, 0, 0, 4,
43, 184, 0, 0, 16, 185, 0, 1, 0, 3, 244, 184, 0, 4, 208, 184, 0, 20, 16, 184,
0, 5, 208, 184, 0, 0, 16, 184, 0, 9, 208, 184, 0, 1, 16, 184, 0, 12, 208, 184,
0, 0, 16, 184, 0, 16, 208, 184, 0, 1, 16, 184, 0, 17, 208, 184, 0, 28, 16, 184,
0, 21, 208, 184, 0, 14, 16, 184, 0, 24, 208, 184, 0, 20, 16, 184, 0, 25, 208,
184, 0, 1, 16, 184, 0, 32, 208, 184, 0, 20, 16, 184, 0, 33, 208, 184, 0, 20,
16, 184, 0, 36, 208, 184, 0, 28, 16, 184, 0, 37, 208, 184, 0, 20, 16, 184, 0,
40, 208, 184, 0, 28, 16, 184, 0, 41, 208, 184, 0, 11, 16, 184, 0, 45, 220, 0,
184, 0, 0, 69, 88, 184, 0, 6, 47, 27, 185, 0, 6, 0, 11, 62, 89, 184, 0, 0, 69,
88, 184, 0, 0, 47, 27, 185, 0, 0, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 10,
47, 27, 185, 0, 10, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 14, 47, 27, 185,
0, 14, 0, 9, 62, 89, 184, 0, 0, 69, 88, 184, 0, 20, 47, 27, 185, 0, 20, 0, 9,
62, 89, 184, 0, 0, 69, 88, 184, 0, 30, 47, 27, 185, 0, 30, 0, 9, 62, 89, 184,
0, 0, 69, 88, 184, 0, 40, 47, 27, 185, 0, 40, 0, 9, 62, 89, 184, 0, 0, 69, 88,
184, 0, 32, 47, 27, 185, 0, 32, 0, 5, 62, 89, 184, 0, 6, 16, 185, 0, 2, 0, 1,
244, 184, 0, 0, 16, 185, 0, 3, 0, 1, 244, 184, 0, 2, 16, 184, 0, 4, 208, 184,
0, 5, 208, 184, 0, 32, 16, 185, 0, 16, 0, 1, 244, 185, 0, 9, 0, 1, 244, 184,
0, 0, 16, 184, 0, 11, 208, 184, 0, 0, 16, 185, 0, 12, 0, 1, 244, 184, 0, 13,
208, 184, 0, 0, 16, 184, 0, 15, 208, 184, 0, 9, 16, 184, 0, 18, 208, 184, 0,
0, 16, 184, 0, 21, 208, 184, 0, 5, 16, 184, 0, 22, 208, 184, 0, 23, 208, 184,
0, 9, 16, 184, 0, 24, 208, 184, 0, 13, 16, 184, 0, 26, 208, 184, 0, 27, 208,
184, 0, 9, 16, 184, 0, 28, 208, 184, 0, 0, 16, 184, 0, 31, 208, 184, 0, 16, 16,
184, 0, 34, 208, 184, 0, 35, 208, 184, 0, 36, 208, 184, 0, 37, 208, 184, 0, 9,
16, 184, 0, 38, 208, 184, 0, 0, 16, 184, 0, 41, 208, 184, 0, 23, 16, 184, 0,
42, 208, 184, 0, 43, 208, 48, 49, 1, 35, 53, 51, 35, 33, 53, 33, 1, 35, 17, 51,
5, 33, 53, 33, 19, 35, 53, 51, 1, 35, 53, 51, 19, 35, 17, 51, 1, 35, 17, 51,
1, 33, 53, 41, 1, 35, 53, 51, 17, 35, 53, 51, 4, 176, 200, 200, 200, 253, 168,
2, 88, 1, 144, 200, 200, 254, 112, 254, 112, 1, 144, 200, 200, 200, 252, 224,
200, 200, 200, 200, 200, 254, 112, 200, 200, 3, 32, 253, 168, 2, 88, 253, 168,
200, 200, 200, 200, 3, 232, 200, 200, 252, 24, 2, 88, 200, 200, 252, 224, 200,
2, 88, 200, 252, 224, 1, 144, 254, 112, 2, 88, 252, 24, 200, 200, 2, 88, 200,
0, 0, 2, 0, 0, 3, 232, 2, 88, 6, 64, 0, 11, 0, 15, 0, 246, 184, 0, 16, 47, 184,
0, 5, 208, 184, 0, 5, 47, 184, 0, 3, 220, 65, 3, 0, 111, 0, 3, 0, 1, 93, 65,
3, 0, 112, 0, 3, 0, 1, 93, 184, 0, 1, 220, 65, 3, 0, 111, 0, 1, 0, 1, 93, 65,
3, 0, 112, 0, 1, 0, 1, 93, 185, 0, 0, 0, 3, 244, 184, 0, 3, 16, 185, 0, 2, 0,
3, 244, 184, 0, 5, 16, 185, 0, 4, 0, 3, 244, 184, 0, 3, 16, 184, 0, 7, 208, 184,
0, 2, 16, 184, 0, 10, 208, 184, 0, 2, 16, 184, 0, 12, 208, 184, 0, 4, 16, 184,
0, 14, 208, 184, 0, 0, 16, 184, 0, 17, 220, 0, 184, 0, 0, 69, 88, 184, 0, 6,
47, 27, 185, 0, 6, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 10, 47, 27, 185,
0, 10, 0, 11, 62, 89, 184, 0, 0, 69, 88, 184, 0, 13, 47, 27, 185, 0, 13, 0, 11,
62, 89, 187, 0, 1, 0, 1, 0, 2, 0, 4, 43, 187, 0, 8, 0, 1, 0, 7, 0, 4, 43, 184,
0, 10, 16, 185, 0, 0, 0, 1, 244, 184, 0, 4, 208, 184, 0, 5, 208, 184, 0, 7, 16,
184, 0, 11, 208, 184, 0, 5, 16, 184, 0, 12, 208, 184, 0, 7, 16, 184, 0, 14, 208,
184, 0, 12, 16, 184, 0, 15, 208, 48, 49, 1, 35, 21, 35, 53, 35, 53, 51, 53, 51,
21, 51, 7, 53, 35, 21, 2, 88, 200, 200, 200, 200, 200, 200, 200, 200, 4, 176,
200, 200, 200, 200, 200, 200, 200, 200, 0, 0, 0, 6, 0, 0, 0, 200, 3, 232, 3,
32, 0, 3, 0, 7, 0, 11, 0, 15, 0, 19, 0, 23, 0, 225, 187, 0, 12, 0, 3, 0, 13,
0, 4, 43, 187, 0, 0, 0, 3, 0, 1, 0, 4, 43, 184, 0, 0, 16, 184, 0, 5, 208, 184,
0, 0, 16, 185, 0, 7, 0, 3, 244, 184, 0, 0, 16, 184, 0, 8, 208, 184, 0, 1, 16,
184, 0, 9, 208, 184, 0, 12, 16, 184, 0, 17, 208, 184, 0, 12, 16, 185, 0, 19,
0, 3, 244, 184, 0, 12, 16, 184, 0, 20, 208, 184, 0, 13, 16, 184, 0, 21, 208,
0, 187, 0, 5, 0, 1, 0, 8, 0, 4, 43, 187, 0, 3, 0, 1, 0, 0, 0, 4, 43, 184, 0,
0, 16, 184, 0, 6, 208, 184, 0, 5, 16, 185, 0, 7, 0, 1, 244, 184, 0, 5, 16, 184,
0, 10, 208, 184, 0, 4, 16, 184, 0, 11, 208, 184, 0, 0, 16, 184, 0, 12, 208, 184,
0, 7, 16, 184, 0, 13, 208, 184, 0, 3, 16, 184, 0, 14, 208, 184, 0, 5, 16, 184,
0, 16, 208, 184, 0, 4, 16, 184, 0, 17, 208, 184, 0, 0, 16, 184, 0, 18, 208, 184,
0, 7, 16, 184, 0, 19, 208, 184, 0, 8, 16, 184, 0, 20, 208, 184, 0, 5, 16, 184,
0, 22, 208, 184, 0, 4, 16, 184, 0, 23, 208, 48, 49, 1, 35, 53, 51, 19, 35, 53,
51, 3, 35, 53, 51, 37, 35, 53, 51, 19, 35, 53, 51, 3, 35, 53, 51, 3, 32, 200,
200, 200, 200, 200, 200, 200, 200, 253, 168, 200, 200, 200, 200, 200, 200, 200,
200, 2, 88, 200, 254, 112, 200, 254, 112, 200, 200, 200, 254, 112, 200, 254,
112, 200, 0, 0, 0, 3, 0, 0, 0, 200, 3, 232, 4, 176, 0, 11, 0, 15, 0, 19, 0, 127,
187, 0, 2, 0, 3, 0, 3, 0, 4, 43, 184, 0, 3, 16, 184, 0, 7, 208, 184, 0, 2, 16,
184, 0, 9, 208, 184, 0, 2, 16, 184, 0, 12, 208, 184, 0, 3, 16, 184, 0, 14, 208,
184, 0, 2, 16, 184, 0, 16, 208, 184, 0, 3, 16, 184, 0, 18, 208, 0, 187, 0, 16,
0, 1, 0, 2, 0, 4, 43, 187, 0, 9, 0, 1, 0, 13, 0, 4, 43, 187, 0, 11, 0, 1, 0,
0, 0, 4, 43, 184, 0, 0, 16, 184, 0, 4, 208, 184, 0, 11, 16, 184, 0, 6, 208, 184,
0, 11, 16, 184, 0, 12, 208, 184, 0, 0, 16, 184, 0, 17, 208, 184, 0, 17, 47, 48,
49, 1, 33, 17, 35, 17, 33, 53, 33, 17, 51, 17, 41, 1, 53, 35, 21, 19, 53, 35,
21, 3, 232, 254, 112, 200, 254, 112, 1, 144, 200, 1, 144, 254, 112, 200, 200,
200, 2, 88, 254, 112, 1, 144, 200, 1, 144, 254, 112, 200, 200, 254, 111, 200,
200, 0, 255, 255, 0, 0, 255, 56, 0, 200, 3, 33, 2, 6, 0, 30, 0, 0, 0, 0, 0, 196,
0, 196, 0, 196, 0, 196, 0, 250, 1, 46, 1, 130, 1, 246, 3, 206, 5, 12, 5, 36,
5, 144, 5, 246, 6, 152, 6, 204, 6, 228, 6, 254, 7, 28, 7, 122, 7, 234, 8, 28,
8, 156, 9, 8, 9, 98, 9, 200, 10, 74, 10, 168, 11, 94, 11, 220, 12, 16, 12, 54,
12, 86, 12, 118, 12, 150, 12, 250, 14, 126, 14, 236, 15, 118, 15, 190, 16, 30,
16, 98, 16, 158, 17, 14, 17, 104, 17, 140, 17, 212, 18, 128, 18, 170, 19, 74,
19, 186, 20, 42, 20, 142, 21, 26, 21, 152, 22, 42, 22, 94, 22, 182, 23, 44, 23,
196, 24, 216, 25, 124, 25, 248, 26, 78, 26, 170, 26, 254, 27, 124, 27, 144, 28,
8, 28, 116, 28, 188, 29, 40, 29, 156, 29, 244, 30, 128, 30, 222, 31, 30, 31,
120, 32, 28, 32, 64, 32, 230, 33, 62, 33, 174, 34, 38, 34, 166, 34, 222, 35,
120, 35, 196, 36, 28, 36, 156, 37, 66, 38, 76, 38, 192, 39, 60, 39, 226, 39,
254, 40, 162, 41, 34, 41, 190, 41, 214, 41, 238, 42, 34, 42, 86, 42, 106, 42,
126, 42, 134, 42, 218, 44, 72, 44, 230, 45, 2, 45, 10, 46, 78, 46, 228, 47, 128,
47, 228, 47, 236, 0, 0, 0, 22, 1, 14, 0, 1, 0, 0, 0, 0, 0, 0, 0, 53, 0, 0, 0,
1, 0, 0, 0, 0, 0, 1, 0, 5, 0, 53, 0, 1, 0, 0, 0, 0, 0, 2, 0, 7, 0, 58, 0, 1,
0, 0, 0, 0, 0, 3, 0, 16, 0, 65, 0, 1, 0, 0, 0, 0, 0, 4, 0, 5, 0, 81, 0, 1, 0,
0, 0, 0, 0, 5, 0, 47, 0, 86, 0, 1, 0, 0, 0, 0, 0, 6, 0, 5, 0, 133, 0, 1, 0, 0,
0, 0, 0, 8, 0, 10, 0, 138, 0, 1, 0, 0, 0, 0, 0, 9, 0, 10, 0, 148, 0, 1, 0, 0,
0, 0, 0, 11, 0, 27, 0, 158, 0, 1, 0, 0, 0, 0, 0, 12, 0, 25, 0, 185, 0, 3, 0,
1, 4, 9, 0, 0, 0, 106, 0, 210, 0, 3, 0, 1, 4, 9, 0, 1, 0, 10, 1, 60, 0, 3, 0,
1, 4, 9, 0, 2, 0, 14, 1, 70, 0, 3, 0, 1, 4, 9, 0, 3, 0, 32, 1, 84, 0, 3, 0, 1,
4, 9, 0, 4, 0, 10, 1, 116, 0, 3, 0, 1, 4, 9, 0, 5, 0, 94, 1, 126, 0, 3, 0, 1,
4, 9, 0, 6, 0, 10, 1, 220, 0, 3, 0, 1, 4, 9, 0, 8, 0, 20, 1, 230, 0, 3, 0, 1,
4, 9, 0, 9, 0, 20, 1, 250, 0, 3, 0, 1, 4, 9, 0, 11, 0, 54, 2, 14, 0, 3, 0, 1,
4, 9, 0, 12, 0, 50, 2, 68, 84, 121, 112, 101, 102, 97, 99, 101, 32, 169, 32,
40, 84, 101, 110, 32, 98, 121, 32, 84, 119, 101, 110, 116, 121, 41, 46, 32, 50,
48, 48, 55, 46, 32, 65, 108, 108, 32, 82, 105, 103, 104, 116, 115, 32, 82, 101,
115, 101, 114, 118, 101, 100, 77, 117, 110, 114, 111, 82, 101, 103, 117, 108,
97, 114, 49, 46, 48, 48, 48, 59, 72, 76, 32, 32, 59, 77, 117, 110, 114, 111,
77, 117, 110, 114, 111, 86, 101, 114, 115, 105, 111, 110, 32, 49, 46, 48, 48,
32, 78, 111, 118, 101, 109, 98, 101, 114, 32, 50, 50, 44, 32, 50, 48, 48, 55,
44, 32, 105, 110, 105, 116, 105, 97, 108, 32, 114, 101, 108, 101, 97, 115, 101,
77, 117, 110, 114, 111, 72, 105, 103, 104, 45, 76, 111, 103, 105, 99, 69, 100,
32, 77, 101, 114, 114, 105, 116, 116, 104, 116, 116, 112, 58, 47, 47, 119, 119,
119, 46, 116, 101, 110, 98, 121, 116, 119, 101, 110, 116, 121, 46, 99, 111, 109,
47, 104, 116, 116, 112, 58, 47, 47, 119, 119, 119, 46, 101, 100, 109, 101, 114,
114, 105, 116, 116, 46, 99, 111, 109, 47, 0, 84, 0, 121, 0, 112, 0, 101, 0, 102,
0, 97, 0, 99, 0, 101, 0, 32, 0, 169, 0, 32, 0, 40, 0, 84, 0, 101, 0, 110, 0,
32, 0, 98, 0, 121, 0, 32, 0, 84, 0, 119, 0, 101, 0, 110, 0, 116, 0, 121, 0, 41,
0, 46, 0, 32, 0, 50, 0, 48, 0, 48, 0, 55, 0, 46, 0, 32, 0, 65, 0, 108, 0, 108,
0, 32, 0, 82, 0, 105, 0, 103, 0, 104, 0, 116, 0, 115, 0, 32, 0, 82, 0, 101, 0,
115, 0, 101, 0, 114, 0, 118, 0, 101, 0, 100, 0, 77, 0, 117, 0, 110, 0, 114, 0,
111, 0, 82, 0, 101, 0, 103, 0, 117, 0, 108, 0, 97, 0, 114, 0, 49, 0, 46, 0, 48,
0, 48, 0, 48, 0, 59, 0, 72, 0, 76, 0, 32, 0, 32, 0, 59, 0, 77, 0, 117, 0, 110,
0, 114, 0, 111, 0, 77, 0, 117, 0, 110, 0, 114, 0, 111, 0, 86, 0, 101, 0, 114,
0, 115, 0, 105, 0, 111, 0, 110, 0, 32, 0, 49, 0, 46, 0, 48, 0, 48, 0, 32, 0,
78, 0, 111, 0, 118, 0, 101, 0, 109, 0, 98, 0, 101, 0, 114, 0, 32, 0, 50, 0, 50,
0, 44, 0, 32, 0, 50, 0, 48, 0, 48, 0, 55, 0, 44, 0, 32, 0, 105, 0, 110, 0, 105,
0, 116, 0, 105, 0, 97, 0, 108, 0, 32, 0, 114, 0, 101, 0, 108, 0, 101, 0, 97,
0, 115, 0, 101, 0, 77, 0, 117, 0, 110, 0, 114, 0, 111, 0, 72, 0, 105, 0, 103,
0, 104, 0, 45, 0, 76, 0, 111, 0, 103, 0, 105, 0, 99, 0, 69, 0, 100, 0, 32, 0,
77, 0, 101, 0, 114, 0, 114, 0, 105, 0, 116, 0, 116, 0, 104, 0, 116, 0, 116, 0,
112, 0, 58, 0, 47, 0, 47, 0, 119, 0, 119, 0, 119, 0, 46, 0, 116, 0, 101, 0, 110,
0, 98, 0, 121, 0, 116, 0, 119, 0, 101, 0, 110, 0, 116, 0, 121, 0, 46, 0, 99,
0, 111, 0, 109, 0, 47, 0, 104, 0, 116, 0, 116, 0, 112, 0, 58, 0, 47, 0, 47, 0,
119, 0, 119, 0, 119, 0, 46, 0, 101, 0, 100, 0, 109, 0, 101, 0, 114, 0, 114, 0,
105, 0, 116, 0, 116, 0, 46, 0, 99, 0, 111, 0, 109, 0, 47, 0, 2, 0, 0, 0, 0, 0,
0, 255, 39, 0, 150, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 115, 0, 0, 1, 2, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10,
0, 11, 0, 12, 0, 13, 0, 14, 0, 15, 0, 16, 0, 17, 0, 18, 0, 19, 0, 20, 0, 21,
0, 22, 0, 23, 0, 24, 0, 25, 0, 26, 0, 27, 0, 28, 0, 29, 0, 30, 0, 31, 0, 32,
0, 33, 0, 34, 0, 35, 0, 36, 0, 37, 0, 38, 0, 39, 0, 40, 0, 41, 0, 42, 0, 43,
0, 44, 0, 45, 0, 46, 0, 47, 0, 48, 0, 49, 0, 50, 0, 51, 0, 52, 0, 53, 0, 54,
0, 55, 0, 56, 0, 57, 0, 58, 0, 59, 0, 60, 0, 61, 0, 62, 0, 63, 0, 64, 0, 65,
0, 66, 0, 68, 0, 69, 0, 70, 0, 71, 0, 72, 0, 73, 0, 74, 0, 75, 0, 76, 0, 77,
0, 78, 0, 79, 0, 80, 0, 81, 0, 82, 0, 83, 0, 84, 0, 85, 0, 86, 0, 87, 0, 88,
0, 89, 0, 90, 0, 91, 0, 92, 0, 93, 0, 94, 0, 95, 0, 96, 0, 97, 1, 3, 0, 182,
0, 183, 0, 180, 0, 181, 0, 178, 0, 179, 1, 4, 0, 133, 0, 139, 0, 169, 0, 164,
1, 5, 0, 138, 0, 131, 0, 170, 0, 184, 1, 6, 4, 78, 85, 76, 76, 4, 69, 117, 114,
111, 7, 117, 110, 105, 48, 48, 65, 48, 7, 117, 110, 105, 48, 48, 65, 68, 7, 117,
110, 105, 48, 51, 55, 69, 0, 0
};
