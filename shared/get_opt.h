/* get_opt.h
 * Command line option parsing.
 *
 * Example:
 *
 * int main (int argc, char **argv)
 * {
 *     get_opt_context_t   ctx = GET_OPT_CONTEXT_INITIALIZER;
 *     int                 opt;
 *     const char          *opts = "hl:";
 *     while ((opt = get_opt(&opt_context, argc, argv, "hs:l")) != -1)
 *     {
 *         switch (opt)
 *         {
 *         case 'h':
 *             print_help();
 *             break;
 *         case 'l':
 *             set_log_file(opts.arg);
 *             break;
 *         case '?':
 *             puts("Unknown command line option.");
 *             break;
 *         }
 *     }
 *     ...
 * }
 */

#ifndef MUTA_SHARED_GET_OPT_H
#define MUTA_SHARED_GET_OPT_H

#define GET_OPT_CONTEXT_INITIALIZER {0, 1}

typedef struct get_opt_context_t get_opt_context_t;

struct get_opt_context_t
{
    char    *arg;
    int     ind;
};

void
get_opt_context_init(get_opt_context_t *context);

int
get_opt(get_opt_context_t *context, int argc, char **argv, const char *opts);

#endif /* MUTA_SHARED_GET_OPT_H */
