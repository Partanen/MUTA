#ifndef MUTA_SHARED_WORLD_DB_H
#define MUTA_SHARED_WORLD_DB_H

#include "wdb_packets.h"

enum wdb_error
{
    WDB_ERROR_MISC = 0,
    WDB_ERROR_DISCONNECTED,
    WDB_ERROR_BAD_PARAMETER,
    NUM_WDB_ERRORS
};
/* wdb_error
 * Query error codes returned by the world db application. */

#endif /* MUTA_SHARED_WORLD_DB_H */
