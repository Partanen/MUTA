#include <mariadb/mysql.h>
#include <mariadb/errmsg.h>
#include <mariadb/mysqld_error.h>
#include <inttypes.h>
#include "wdb_connector.h"
#include "common_utils.h"
#include "log.h"
#include "hashtable.h"
#include "account_rules.h"

typedef struct query_player_character_list_t    query_player_character_list_t;
typedef struct query_player_character_t         query_player_character_t;
typedef struct query_create_player_character_t  query_create_player_character_t;
typedef struct query_player_character_t         query_player_character_t;
typedef struct query_create_dynamic_object_in_player_container_t
    query_create_dynamic_object_in_player_container_t;
typedef struct query_save_player_character_position_and_direction_t
    query_save_player_character_position_and_direction_t;
typedef struct query_save_dynamic_object_position_in_player_inventory_t
    query_save_dynamic_object_position_in_player_inventory_t;
typedef struct query_remove_dynamic_object_from_player_container_t
    query_remove_dynamic_object_from_player_container_t;
typedef struct request_t                        request_t;

#define LOG(msg, ...) \
    log_printf(_log, _log_info_category, "%s: " msg, __func__, ##__VA_ARGS__)

#define LOG_ERROR(msg, ...) \
    log_printf(_log, _log_error_category, "%s: " msg, __func__, \
        ##__VA_ARGS__)

#define LOG_DEBUG(msg, ...) \
    log_printf(_log, _log_debug_category, "%s: " msg, __func__, \
        ##__VA_ARGS__)

#define DB_OK() (_connection_state == CONNECTION_CONNECTED)

enum connection_state
{
    CONNECTION_DISCONNECTED,
    CONNECTION_CONNECTING,
    CONNECTION_CONNECTED
};

enum request_type
{
    REQUEST_DISCONNECT,
    REQUEST_QUERY_PLAYER_CHARACTER_LIST,
    REQUEST_QUERY_PLAYER_CHARACTER,
    REQUEST_QUERY_CREATE_PLAYER_CHARACTER,
    REQUEST_QUERY_CREATE_DYNAMIC_OBJECT_IN_PLAYER_CONTAINER,
    REQUEST_QUERY_SAVE_PLAYER_CHARACTER_POSITION_AND_DIRECTION,
    REQUEST_QUERY_SAVE_DYNAMIC_OBJECT_POSITION_IN_PLAYER_INVENTORY,
    REQUEST_QUERY_REMOVE_DYNAMIC_OBJECT_FROM_PLAYER_CONTAINER
};

struct query_player_character_list_t
{
    wdbc_query_id_t query_id;
    account_db_id_t account_id;
    void            *user_data;
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        wdbc_player_character_t *characters, uint32 num_characters);
};

struct query_player_character_t
{
    wdbc_query_id_t     query_id;
    account_db_id_t     account_db_id;
    player_db_guid_t    player_db_id;
    void                *user_data;
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        wdbc_player_character_t *character,
        wdbc_dynamic_object_in_db_t *items, uint32 num_items);
};

struct query_create_player_character_t
{
    wdbc_query_id_t         query_id;
    account_db_id_t         account_id;
    char                    name[MAX_CHARACTER_NAME_LEN + 1];
    uint8                   race;
    uint8                   sex;
    uint8                   direction;
    uint32                  instance_id;
    wdbc_dynamic_object_t   *items; /* segpool allocated */
    uint32                  num_items;
    int32                   x;
    int32                   y;
    uint8                   z;
    void                    *user_data;
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        wdbc_player_character_t *character);
};

struct query_create_dynamic_object_in_player_container_t
{
    wdbc_query_id_t         query_id;
    player_db_guid_t        player_db_id;
    dobj_type_id_t          type_id;
    equipment_slot_id_t     equipment_slot;
    uint8                   x;
    uint8                   y;
    void                    *user_data;
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        wdbc_dynamic_object_in_db_t *dobj);
};

struct query_save_player_character_position_and_direction_t
{
    wdbc_query_id_t     query_id;
    player_db_guid_t    player_db_id;
    uint32              instance_id;
    int32               x;
    int32               y;
    uint8               z;
    uint8               direction;
    void                *user_data;
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        player_db_guid_t player_db_id, uint32 instance_id, uint8 direction,
        int32 x, int32 y, uint8 z);
};

struct query_save_dynamic_object_position_in_player_inventory_t
{
    wdbc_query_id_t     query_id;
    uuid16_t            dobj_uuid;
    equipment_slot_id_t equipment_slot;
    uint8               x;
    uint8               y;
    void                *user_data;
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        uuid16_t dobj_db_uuid, equipment_slot_id_t equipment_slot, uint8 x,
        uint8 y);
};

struct query_remove_dynamic_object_from_player_container_t
{
    wdbc_query_id_t query_id;
    uuid16_t        dobj_uuid;
    void            *user_data;
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        uuid16_t dobj_db_uuid);
};

struct request_t
{
    enum request_type type;
    union
    {
        query_player_character_list_t   query_player_character_list;
        query_create_player_character_t query_create_player_character;
        query_player_character_t        query_player_character;
        query_create_dynamic_object_in_player_container_t
            query_create_dynamic_object_in_player_container;
        query_save_player_character_position_and_direction_t
            query_save_player_character_position_and_direction;
        query_save_dynamic_object_position_in_player_inventory_t
            query_save_dynamic_object_position_in_player_inventory;
        query_remove_dynamic_object_from_player_container_t
            query_remove_dynamic_object_from_player_container;
    };
};

static MYSQL                    _mysql;
static log_t                    *_log;
static int                      _log_info_category;
static int                      _log_error_category;
static int                      _log_debug_category;
static enum connection_state    _connection_state;
static thread_t                 _thread;
static dchar                    *_shard_db_name;
static dchar                    *_username;
static dchar                    *_password;
static dchar                    *_ip;
static uint16                   _port;
#if defined __unix__
static dchar                    *_unix_socket;
#endif
static event_buf_t              _requests;
static void                     (*_post_event)(wdbc_event_t *event);
static hashtable(wdbc_query_id_t, request_t) _request_table;
static wdbc_query_id_t          _running_query_id;
static segpool_t                _segpool;
static mutex_t                  _segpool_mutex;

/* Pre-compiled statements */
MYSQL_STMT *_query_generate_uuid_stmt;
MYSQL_STMT *_query_player_character_list_stmt;
MYSQL_STMT *_query_player_character_stmt;
MYSQL_STMT *_query_create_player_character_stmt;
MYSQL_STMT *_query_create_dynamic_object_in_player_container_stmt;
MYSQL_STMT *_query_save_player_character_position_direction_stmt;
MYSQL_STMT *_query_player_dynamic_objects_stmt;
MYSQL_STMT *_query_save_dynamic_object_position_in_player_inventory_stmt;
MYSQL_STMT *_query_remove_dynamic_object_from_player_container_stmt;

static const char *_query_generate_uuid_str = "SELECT UuidToBin(UUID())";

static const char *_query_player_character_list_str =
   "SELECT id, name, race, sex, direction, instance_id, x, y, z FROM "
   "player_characters WHERE account_id = ? LIMIT ?";

static const char *_query_player_character_str =
   "SELECT id, name, race, sex, direction, instance_id, x, y, z FROM "
   "player_characters WHERE account_id = ? AND id = ? LIMIT 1";

static const char *_query_create_player_character_str =
    "INSERT INTO player_characters (account_id, name, race, sex, direction, "
    "instance_id, x, y, z) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

static const char *_query_create_dynamic_object_in_player_container_str =
    "INSERT INTO dynamic_objects (id, type, in_type, x, y, player_id, "
    "equipment_slot) VALUES "
    "(?, ?, ?, ?, ?, ?, ?)";

static const char *_query_save_player_character_position_direction_str =
    "UPDATE player_characters SET instance_id = ?, direction = ?, x = ?, "
    "y = ?, z = ? WHERE id = ?";

static const char *_query_player_dynamic_objects_str =
    "SELECT id, in_type, type, equipment_slot, x, y FROM "
    "dynamic_objects WHERE "
    "(in_type = 'player_container' OR in_type = 'player_equipped') AND "
    "player_id = ?";

static const char *_query_save_dynamic_object_position_in_player_inventory_str =
    "UPDATE dynamic_objects SET in_type = 'player_container', "
    "equipment_slot = ?, x = ?, y = ? WHERE id = ?";

static const char *_query_remove_dynamic_object_from_player_container_str =
    "DELETE FROM dynamic_objects WHERE id = ?";

static thread_ret_t
_main(void *args);

static inline wdbc_query_id_t
_next_query_id(void);

static MYSQL_STMT *
_prepare_statement(const char *query);
/* Panics on failure */

static inline MYSQL_BIND
_bind_account_id(account_db_id_t *buf);

static inline MYSQL_BIND
_bind_null_terminated_str(char *buf, unsigned long buf_len);
/* For null-terminated strings. buf_len must include the null terminator. */

static inline MYSQL_BIND
_bind_non_null_terminated_str(char *buf, unsigned long buf_len);

static inline MYSQL_BIND
_bind_int8(int8 *buf);

static inline MYSQL_BIND
_bind_uint8(uint8 *buf);

static inline MYSQL_BIND
_bind_int32(int32 *buf);

static inline MYSQL_BIND
_bind_uint32(uint32 *buf);

static inline MYSQL_BIND
_bind_int64(int64 *buf);

static inline MYSQL_BIND
_bind_uint64(uint64 *buf);

static inline MYSQL_BIND
_bind_equipment_slot(equipment_slot_id_t *slot_id);

static inline MYSQL_BIND
_bind_blob(uint8 *buf, unsigned long buf_len);

static int
_generate_uuid(uuid16_t * ret_uuid);

static int
_handle_request_query_player_character_list(
    query_player_character_list_t *query);

static int
_handle_request_query_player_character(query_player_character_t *query);

static int
_handle_request_query_create_player_character(
    query_create_player_character_t *query);

static int
_handle_request_query_create_dynamic_object_in_player_container(
    query_create_dynamic_object_in_player_container_t *query);

static int
_handle_request_query_save_player_character_position_and_direction(
    query_save_player_character_position_and_direction_t *query);

static int
_handle_request_query_save_dynamic_object_position_in_player_inventory(
    query_save_dynamic_object_position_in_player_inventory_t *query);

static int
_handle_request_query_remove_dynamic_object_from_player_container(
    query_remove_dynamic_object_from_player_container_t *query);

static int
_execute_statement_with_results(MYSQL_STMT *stmt, MYSQL_BIND *args,
    MYSQL_BIND *results);
/* Panics if something unexpected happens, but if stmt execution fails, assumes
 * the mysql server disconnected and returns non-zero instead. */

static int
_execute_statement_without_results(MYSQL_STMT *stmt, MYSQL_BIND *args);
/* Panics if something unexpected happens, but if stmt execution fails, assumes
 * the mysql server disconnected and returns non-zero instead. */

static void
_push_request(wdbc_query_id_t query_id, request_t *request);

static void
_free_stmts(void);

static void
_request_disconnect(const char *reason);

static inline int
_start_transaction(void);

static inline int
_commit_transaction(void);

static inline int
_rollback_transaction(void);

static void *
_malloc(uint32 num_bytes);

static void
_free(void *mem);

int
wdbc_init(const char *ip, uint16 port,
    const char *unix_socket,
    const char *shard_db_name,
    const char *username,
    const char *password,
    uint32 max_pending_queries, log_t *log,
    int log_info_category,
    int log_error_category,
    int log_debug_category,
    void (*post_event)(wdbc_event_t *event))
{
    if (thread_init(&_thread))
    {
        LOG("thread_init() failed.");
        muta_panic_print("Failed to init thread.");
    }
    event_init(&_requests, sizeof(request_t), max_pending_queries);
    segpool_init(&_segpool);
    mutex_init(&_segpool_mutex);
    hashtable_einit(_request_table, max_pending_queries * 2);
    if (ip)
        _ip = dstr_create(ip);
    else
        _ip = 0;
    _port               = port;
    _shard_db_name      = dstr_create(shard_db_name);
    _username           = dstr_create(username);
    _password           = dstr_create(password);
    _log                = log;
    _log_info_category  = log_info_category;
    _log_error_category = log_error_category;
    _log_debug_category = log_debug_category;
#if defined(__unix__)
    if (unix_socket)
        _unix_socket = dstr_create(unix_socket);
    else
        _unix_socket = 0;
#endif
    _post_event = post_event;
    return 0;
}

void
wdbc_destroy(void)
{
    if (_connection_state != CONNECTION_DISCONNECTED)
    {
        _request_disconnect("wdbc_destroy() called.");
        thread_join(&_thread);
    }
    dstr_free(&_shard_db_name);
    dstr_free(&_username);
    dstr_free(&_password);
#if defined(__unix__)
    dstr_free(&_unix_socket);
#endif
    segpool_destroy(&_segpool);
    hashtable_destroy(_request_table, 0);
    thread_destroy(&_thread);
    event_destroy(&_requests);
}

int
wdbc_connect(void)
{
    if (_connection_state != CONNECTION_DISCONNECTED)
        return 1;
    if (thread_create(&_thread, _main, 0))
    {
        LOG("thread_create() failed.");
        muta_panic_print("Failed to create db connection thread.");
    }
    _connection_state = CONNECTION_CONNECTING;
    return 0;
}

void
wdbc_handle_event(wdbc_event_t *event)
{
    switch (event->type)
    {
    case WDBC_EVENT_CONNECTED:
        muta_assert(_connection_state == CONNECTION_CONNECTING);
        LOG("World DB connected successfully.");
        _connection_state = CONNECTION_CONNECTED;
        break;
    case WDBC_EVENT_FAILED_TO_CONNECT:
        muta_assert(_connection_state == CONNECTION_CONNECTING);
        _connection_state = CONNECTION_DISCONNECTED;
        thread_join(&_thread);
        wdbc_connect(); /* Try again */
        break;
    case WDBC_EVENT_DISCONNECTED:
    {
        /* Fail any on-going queries. */
        wdbc_query_id_t key;
        request_t       value;
        hashtable_for_each_pair(_request_table, key, value)
        {
            switch (value.type)
            {
            case REQUEST_QUERY_PLAYER_CHARACTER_LIST:
            {
                query_player_character_list_t *query =
                    &value.query_player_character_list;
                query->on_complete(query->query_id, query->user_data, 1, 0, 0);
            }
                break;
            case REQUEST_QUERY_PLAYER_CHARACTER:
            {
                query_player_character_t *query = &value.query_player_character;
                query->on_complete(query->query_id, query->user_data, 1, 0, 0,
                    0);
            }
                break;
            case REQUEST_QUERY_CREATE_PLAYER_CHARACTER:
            {
                query_create_player_character_t *query =
                    &value.query_create_player_character;
                query->on_complete(query->query_id, query->user_data, 1, 0);
            }
                break;
            case REQUEST_QUERY_CREATE_DYNAMIC_OBJECT_IN_PLAYER_CONTAINER:
            {
                query_create_dynamic_object_in_player_container_t *query =
                    &value.query_create_dynamic_object_in_player_container;
                query->on_complete(query->query_id, query->user_data, 1, 0);
            }
                break;
            case REQUEST_QUERY_SAVE_PLAYER_CHARACTER_POSITION_AND_DIRECTION:
            {
                query_save_player_character_position_and_direction_t *query =
                    &value.query_save_player_character_position_and_direction;
                query->on_complete(query->query_id, query->user_data, 1,
                    query->player_db_id, 0, 0, 0, 0, 0);
            }
                break;
            case REQUEST_QUERY_SAVE_DYNAMIC_OBJECT_POSITION_IN_PLAYER_INVENTORY:
            {
                query_save_dynamic_object_position_in_player_inventory_t *query =
                    &value.query_save_dynamic_object_position_in_player_inventory;
                query->on_complete(query->query_id, query->user_data, 1,
                    query->dobj_uuid, 0, 0, 0);
            }
                break;
            case REQUEST_QUERY_REMOVE_DYNAMIC_OBJECT_FROM_PLAYER_CONTAINER:
            {
                query_remove_dynamic_object_from_player_container_t *query =
                    &value.query_remove_dynamic_object_from_player_container;
                query->on_complete(query->query_id, query->user_data, 1,
                    query->dobj_uuid);
            }
                break;
            case REQUEST_DISCONNECT: /* Not actually possible. */
                break;
            }
        }
        hashtable_clear(_request_table, 0);
        event_clear(&_requests);
        LOG("World DB disconnected.");
    }
        break;
    case WDBC_EVENT_COMPLETED_QUERY_PLAYER_CHARACTER_LIST:
    {
        wdbc_completed_query_player_character_list_event_t *event_data =
            &event->completed_query_player_character_list;
        size_t hash = hashtable_hash(&event_data->query_id,
            sizeof(event_data->query_id));
        request_t *request = hashtable_find(_request_table,
            event_data->query_id, hash);
        if (!request) /* Cancelled */
        {
            LOG_DEBUG("Event for player character list query completed, but "
                "query was cancelled.");
            break;
        }
        LOG("Event for player character list query completed, calling "
            "user callback.");
        query_player_character_list_t *request_data =
            &request->query_player_character_list;
        request_data->on_complete(event_data->query_id, request_data->user_data,
            event_data->error, event_data->characters,
            event_data->num_characters);
        hashtable_erase(_request_table, event_data->query_id, hash);
    }
        break;
    case WDBC_EVENT_COMPLETED_QUERY_PLAYER_CHARACTER:
    {
        wdbc_completed_query_player_character_t *event_data =
            &event->completed_query_player_character;
        size_t hash = hashtable_hash(&event_data->query_id,
            sizeof(event_data->query_id));
        request_t *request = hashtable_find(_request_table,
            event_data->query_id, hash);
        if (!request) /* Cancelled */
        {
            LOG_DEBUG("Event for player character query completed, but "
                "query was cancelled.");
            break;
        }
        query_player_character_t *request_data =
            &request->query_player_character;
        LOG_DEBUG("Event for player character query completed, calling "
            "user callback. Id: %" PRIu64 ", Name: %s, race: %d, sex: %d",
            event_data->character.id, event_data->character.name,
            event_data->character.race, event_data->character.sex);
        request_data->on_complete(event_data->query_id, request_data->user_data,
            event_data->error,
            &event_data->character,
            event_data->items, event_data->num_items);  /* items */
        _free(event_data->items);
        hashtable_erase(_request_table, event_data->query_id, hash);
    }
        break;
    case WDBC_EVENT_COMPLETED_QUERY_CREATE_PLAYER_CHARACTER:
    {
        wdbc_completed_query_create_player_character_t *event_data =
            &event->completed_query_create_player_character;
        size_t hash = hashtable_hash(&event_data->query_id,
            sizeof(event_data->query_id));
        request_t *request = hashtable_find(_request_table,
            event_data->query_id, hash);
        if (!request) /* Cancelled */
        {
            LOG_DEBUG("Event for create player character query completed, but "
                "query was cancelled.");
            break;
        }
        LOG("Event for create player character query completed, calling "
            "user callback.");
        query_create_player_character_t *request_data =
            &request->query_create_player_character;
        _free(request_data->items);
        request_data->on_complete(event_data->query_id, request_data->user_data,
            event_data->error, &event_data->character);
        hashtable_erase(_request_table, event_data->query_id, hash);
    }
        break;
    case WDBC_EVENT_COMPLETED_QUERY_CREATE_DYNAMIC_OBJECT_IN_PLAYER_CONTAINER:
    {
        wdbc_completed_query_create_dynamic_object_in_player_container_t *event_data =
            &event->completed_query_create_dynamic_object_in_player_container;
        size_t hash = hashtable_hash(&event_data->query_id,
            sizeof(event_data->query_id));
        request_t *request = hashtable_find(_request_table,
            event_data->query_id, hash);
        if (!request)
        {
            LOG_DEBUG("Event for create dynamic object in player bag query "
                "completed, but query was cancelled.");
            break;
        }
        query_create_dynamic_object_in_player_container_t *request_data =
            &request->query_create_dynamic_object_in_player_container;
        request_data->on_complete(event_data->query_id, request_data->user_data,
            event_data->error, &event_data->dobj);
        LOG("Created dynamic object in a player's bag in DB.");
    }
        break;
    case WDBC_EVENT_COMPLETED_QUERY_SAVE_PLAYER_CHARACTER_POSITION_AND_DIRECTION:
    {
        wdbc_completed_query_save_player_character_position_and_direction_t *event_data =
            &event->completed_query_save_player_character_position_and_direction;
        size_t hash = hashtable_hash(&event_data->query_id,
            sizeof(event_data->query_id));
        request_t *request = hashtable_find(_request_table,
            event_data->query_id, hash);
        if (!request)
        {
            LOG_DEBUG("Event for save player character position and direction "
                "query completed, but query was cancelled.");
            break;
        }
        query_save_player_character_position_and_direction_t *request_data =
            &request->query_save_player_character_position_and_direction;
        request_data->on_complete(event_data->query_id, request_data->user_data,
            event_data->error, request_data->player_db_id,
            event_data->instance_id, event_data->direction, event_data->x,
            event_data->y, event_data->z);
        LOG("Created dynamic object in a player's bag in DB.");
    }
        break;
    case WDBC_EVENT_COMPLETED_QUERY_SAVE_DYNAMIC_OBJECT_POSITION_IN_PLAYER_INVENTORY:
    {
        wdbc_completed_query_save_dynamic_object_position_in_player_inventory_t *event_data =
            &event->completed_query_save_dynamic_object_position_in_player_inventory;
        size_t hash = hashtable_hash(&event_data->query_id,
            sizeof(event_data->query_id));
        request_t *request = hashtable_find(_request_table,
            event_data->query_id, hash);
        if (!request)
        {
            LOG_DEBUG("Event for save dynamic object position in player "
                "inventory query completed, but query was cancelled.");
            break;
        }
        query_save_dynamic_object_position_in_player_inventory_t *request_data =
            &request->query_save_dynamic_object_position_in_player_inventory;
        request_data->on_complete(event_data->query_id, request_data->user_data,
            event_data->error, request_data->dobj_uuid,
            event_data->equipment_slot, event_data->x, event_data->y);
    }
        break;
    case WDBC_EVENT_COMPLETED_QUERY_REMOVE_DYNAMIC_OBJECT_FROM_PLAYER_CONTAINER:
    {
        wdbc_completed_query_remove_dynamic_object_from_player_container_t *event_data =
            &event->completed_query_remove_dynamic_object_from_player_container;
        size_t hash = hashtable_hash(&event_data->query_id,
            sizeof(event_data->query_id));
        request_t *request = hashtable_find(_request_table,
            event_data->query_id, hash);
        if (!request)
        {
            LOG_DEBUG("Event for remove dynamic object from player container "
                "completed, but query was cancelled.");
            break;
        }
        query_remove_dynamic_object_from_player_container_t *request_data =
            &request->query_remove_dynamic_object_from_player_container;
        request_data->on_complete(event_data->query_id, request_data->user_data,
            event_data->error, request_data->dobj_uuid);
    }
        break;
    }
}

void
wdbc_cancel_query(wdbc_query_id_t query_id)
{
    size_t      hash        = hashtable_hash(&query_id, sizeof(query_id));
    request_t   *request    = hashtable_find(_request_table, query_id, hash);
    switch (request->type)
    {
    case REQUEST_QUERY_CREATE_PLAYER_CHARACTER:
        _free(request->query_create_player_character.items);
        break;
    case REQUEST_DISCONNECT:
    case REQUEST_QUERY_PLAYER_CHARACTER_LIST:
    case REQUEST_QUERY_PLAYER_CHARACTER:
    case REQUEST_QUERY_CREATE_DYNAMIC_OBJECT_IN_PLAYER_CONTAINER:
    case REQUEST_QUERY_SAVE_PLAYER_CHARACTER_POSITION_AND_DIRECTION:
    case REQUEST_QUERY_SAVE_DYNAMIC_OBJECT_POSITION_IN_PLAYER_INVENTORY:
    case REQUEST_QUERY_REMOVE_DYNAMIC_OBJECT_FROM_PLAYER_CONTAINER:
        break;
    }
    hashtable_erase(_request_table, query_id,
        hashtable_hash(&query_id, sizeof(query_id)));
}

wdbc_query_id_t
wdbc_query_player_character_list(
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        wdbc_player_character_t *characters, uint32 num_characters),
    void *user_data, account_db_id_t account_id)
{
    if (!DB_OK())
        return WDBC_INVALID_QUERY_ID;
    wdbc_query_id_t query_id = _next_query_id();
    request_t request =
    {
        .type = REQUEST_QUERY_PLAYER_CHARACTER_LIST,
        .query_player_character_list.query_id       = query_id,
        .query_player_character_list.account_id     = account_id,
        .query_player_character_list.user_data      = user_data,
        .query_player_character_list.on_complete    = on_complete
    };
    _push_request(query_id, &request);
    LOG_DEBUG("Started query player character list.");
    return query_id;
}

wdbc_query_id_t
wdbc_query_player_character(
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        wdbc_player_character_t *character,
        wdbc_dynamic_object_in_db_t *items, uint32 num_items),
    void *user_data, account_db_id_t account_db_id,
    player_db_guid_t player_db_id)
{
    if (!DB_OK())
        return WDBC_INVALID_QUERY_ID;
    wdbc_query_id_t query_id = _next_query_id();
    request_t request =
    {
        .type = REQUEST_QUERY_PLAYER_CHARACTER,
        .query_player_character.query_id        = query_id,
        .query_player_character.account_db_id   = account_db_id,
        .query_player_character.player_db_id    = player_db_id,
        .query_player_character.user_data       = user_data,
        .query_player_character.on_complete     = on_complete
    };
    _push_request(query_id, &request);
    LOG_DEBUG("Started query player character.");
    return query_id;
}

wdbc_query_id_t
wdbc_query_create_player_character(
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        wdbc_player_character_t *character),
    void *user_data, account_db_id_t account_id, const char *name, int race,
    int sex, uint32 instance_id, int position[3], int direction,
    wdbc_dynamic_object_t *items, uint32 num_items)
{
    muta_assert(on_complete);
    if (!DB_OK())
        return WDBC_INVALID_QUERY_ID;
    size_t name_len = strlen(name);
    if (ar_check_player_character_name(name, name_len))
    {
        LOG("Can't create player character: invalid name.");
        return WDBC_INVALID_QUERY_ID;
    }
    if (sex != 0 && sex != 1)
    {
        LOG("Can't create player character: invalid sex.");
        return WDBC_INVALID_QUERY_ID;
    }
    if (direction >= NUM_ISODIRS)
    {
        LOG("Can't create player character: invalid direction.");
        return WDBC_INVALID_QUERY_ID;
    }
    wdbc_query_id_t query_id = _next_query_id();
    request_t request =
    {
        .type = REQUEST_QUERY_CREATE_PLAYER_CHARACTER,
        .query_create_player_character =
        {
            .query_id       = query_id,
            .account_id     = account_id,
            .user_data      = user_data,
            .on_complete    = on_complete,
            .race           = (uint8)race,
            .sex            = (uint8)sex,
            .direction      = (uint8)direction,
            .instance_id    = (uint32)instance_id,
            .x              = (int32)position[0],
            .y              = (int32)position[1],
            .z              = (uint8)position[2],
            .items          = _malloc(num_items * sizeof(*items)),
            .num_items      = num_items
        }
    };
    memcpy(request.query_create_player_character.items, items,
        num_items * sizeof(*items));
    strcpy(request.query_create_player_character.name, name);
    _push_request(query_id, &request);
    return query_id;
}

wdbc_query_id_t
wdbc_query_create_dynamic_object_in_player_container(
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        wdbc_dynamic_object_in_db_t *dobj),
    void *user_data, dobj_type_id_t type_id, player_db_guid_t player_db_id,
    equipment_slot_id_t equipment_slot, uint8 x, uint8 y)
{
    if (!DB_OK())
    {
        LOG_ERROR("Can't create dynamic object: DB offline.");
        return WDBC_INVALID_QUERY_ID;
    }
    if (equipment_slot > NUM_EQUIPMENTS_SLOTS)
    {
        LOG("Can't create dynamic object: invalid equipment slot.");
        return WDBC_INVALID_QUERY_ID;
    }
    if (y * x + x > MAX_BAG_SIZE)
    {
        LOG("Can't create dynamic object: invalid position inside bag.");
        return WDBC_INVALID_QUERY_ID;
    }
    wdbc_query_id_t query_id = _next_query_id();
    request_t request =
    {
        .type = REQUEST_QUERY_CREATE_DYNAMIC_OBJECT_IN_PLAYER_CONTAINER,
        .query_create_dynamic_object_in_player_container.query_id       = query_id,
        .query_create_dynamic_object_in_player_container.type_id        = type_id,
        .query_create_dynamic_object_in_player_container.player_db_id   = player_db_id,
        .query_create_dynamic_object_in_player_container.equipment_slot = equipment_slot,
        .query_create_dynamic_object_in_player_container.x              = x,
        .query_create_dynamic_object_in_player_container.y              = y,
        .query_create_dynamic_object_in_player_container.user_data      = user_data,
        .query_create_dynamic_object_in_player_container.on_complete    = on_complete
    };
    _push_request(query_id, &request);
    return query_id;
}

wdbc_query_id_t
wdbc_query_save_player_character_position_and_direction(
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        player_db_guid_t player_db_id, uint32 instance_id, uint8 direction,
        int32 x, int32 y, uint8 z),
    void *user_data, player_db_guid_t player_db_id, uint32 instance_id,
    uint8 direction, int32 x, int32 y, uint8 z)
{
    if (!DB_OK())
    {
        LOG_ERROR("Cannot save player position: DB offline.");
        return WDBC_INVALID_QUERY_ID;
    }
    wdbc_query_id_t query_id = _next_query_id();
    request_t request =
    {
        .type = REQUEST_QUERY_SAVE_PLAYER_CHARACTER_POSITION_AND_DIRECTION,
        .query_save_player_character_position_and_direction =
        {
            .query_id       = query_id,
            .player_db_id   = player_db_id,
            .instance_id    = instance_id,
            .x              = x,
            .y              = y,
            .z              = z,
            .direction      = direction,
            .user_data      = user_data,
            .on_complete    = on_complete
        }
    };
    _push_request(query_id, &request);
    return query_id;
}

wdbc_query_id_t
wdbc_query_save_dynamic_object_position_in_player_inventory(
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        uuid16_t dobj_uuid, equipment_slot_id_t equipment_slot_id, uint8 x,
        uint8 y),
    void *user_data, uuid16_t dobj_uuid, equipment_slot_id_t equipment_slot,
    uint8 x, uint8 y)
{
    muta_assert(equipment_slot < NUM_EQUIPMENTS_SLOTS);
    if (!DB_OK())
    {
        LOG_ERROR("Cannot save dynamic object position in player inventory: DB "
            "offline.");
        return WDBC_INVALID_QUERY_ID;
    }
    wdbc_query_id_t query_id = _next_query_id();
    request_t request =
    {
        .type = REQUEST_QUERY_SAVE_DYNAMIC_OBJECT_POSITION_IN_PLAYER_INVENTORY,
        .query_save_dynamic_object_position_in_player_inventory =
        {
            .query_id       = query_id,
            .dobj_uuid      = dobj_uuid,
            .equipment_slot = equipment_slot,
            .x              = x,
            .y              = y,
            .user_data      = user_data,
            .on_complete    = on_complete
        }
    };
    _push_request(query_id, &request);
    return query_id;
}

wdbc_query_id_t
wdbc_query_remove_dynamic_object_from_player_container(
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        uuid16_t dobj_db_uuid),
    void *user_data,
    uuid16_t dobj_db_uuid)
{
    if (!DB_OK())
    {
        LOG_ERROR("Cannot save dynamic object position in player inventory: DB "
            "offline.");
        return WDBC_INVALID_QUERY_ID;
    }
    wdbc_query_id_t query_id = _next_query_id();
    request_t request =
    {
        .type = REQUEST_QUERY_REMOVE_DYNAMIC_OBJECT_FROM_PLAYER_CONTAINER,
        .query_remove_dynamic_object_from_player_container =
        {
            .query_id       = query_id,
            .dobj_uuid      = dobj_db_uuid,
            .on_complete    = on_complete,
            .user_data      = user_data
        }
    };
    _push_request(query_id, &request);
    return query_id;
}

static thread_ret_t
_main(void *args)
{
    if (!mysql_init(&_mysql))
        muta_panic_print("mysql_init() failed.");
    /*my_bool yes = 1;
    mysql_options(&_mysql, MYSQL_OPT_RECONNECT, &yes);*/
    if (!mysql_real_connect(&_mysql, _ip, _username, _password,
        _shard_db_name, _port, 0, CLIENT_MULTI_STATEMENTS))
    {
        LOG("mysql_real_connect failed: %s", mysql_error(&_mysql));
        if (mysql_errno(&_mysql) == ER_ACCESS_DENIED_ERROR)
            muta_panic_print("Bad world DB account info in config.cfg");
        mysql_close(&_mysql);
        sleep_ms(500);
        wdbc_event_t event = {.type = WDBC_EVENT_FAILED_TO_CONNECT};
        _post_event(&event);
        return 0;
    }
    LOG("Connected do DB.");
    _query_generate_uuid_stmt = _prepare_statement(_query_generate_uuid_str);
    _query_player_character_list_stmt = _prepare_statement(
        _query_player_character_list_str);
    _query_player_character_stmt = _prepare_statement(
        _query_player_character_str);
    _query_create_player_character_stmt = _prepare_statement(
        _query_create_player_character_str);
    _query_create_dynamic_object_in_player_container_stmt = _prepare_statement(
        _query_create_dynamic_object_in_player_container_str);
    _query_save_player_character_position_direction_stmt = _prepare_statement(
        _query_save_player_character_position_direction_str);
    _query_player_dynamic_objects_stmt = _prepare_statement(
        _query_player_dynamic_objects_str);
    _query_save_dynamic_object_position_in_player_inventory_stmt =
        _prepare_statement(
            _query_save_dynamic_object_position_in_player_inventory_str);
    _query_remove_dynamic_object_from_player_container_stmt =
        _prepare_statement(
            _query_remove_dynamic_object_from_player_container_str);
    {
        wdbc_event_t event = {.type = WDBC_EVENT_CONNECTED};
        _post_event(&event);
    }
    for (;;)
    {
        request_t requests[64];
        uint32 num_requests = event_wait(&_requests, requests,
            sizeof(requests) / sizeof(requests[0]), 500);
        for (uint32 i = 0; i < num_requests; ++i)
        {
            switch (requests[i].type)
            {
            case REQUEST_DISCONNECT:
            {
                _free_stmts();
                mysql_close(&_mysql);
                wdbc_event_t event = {.type = WDBC_EVENT_DISCONNECTED};
                _post_event(&event);
                return 0;
            }
            case REQUEST_QUERY_PLAYER_CHARACTER_LIST:
                if (_handle_request_query_player_character_list(
                    &requests[i].query_player_character_list))
                    goto disconnect;
                break;
            case REQUEST_QUERY_PLAYER_CHARACTER:
                if (_handle_request_query_player_character(
                    &requests[i].query_player_character))
                    goto disconnect;
                break;
            case REQUEST_QUERY_CREATE_PLAYER_CHARACTER:
                if (_handle_request_query_create_player_character(
                    &requests[i].query_create_player_character))
                    goto disconnect;
                break;
            case REQUEST_QUERY_CREATE_DYNAMIC_OBJECT_IN_PLAYER_CONTAINER:
                if (_handle_request_query_create_dynamic_object_in_player_container(
                    &requests[i].query_create_dynamic_object_in_player_container))
                    goto disconnect;
                break;
            case REQUEST_QUERY_SAVE_PLAYER_CHARACTER_POSITION_AND_DIRECTION:
            {
                if (_handle_request_query_save_player_character_position_and_direction(
                    &requests[i].query_save_player_character_position_and_direction))
                    goto disconnect;
            }
                break;
            case REQUEST_QUERY_SAVE_DYNAMIC_OBJECT_POSITION_IN_PLAYER_INVENTORY:
            {
                if (_handle_request_query_save_dynamic_object_position_in_player_inventory(
                    &requests[i].query_save_dynamic_object_position_in_player_inventory))
                    goto disconnect;
            }
                break;
            case REQUEST_QUERY_REMOVE_DYNAMIC_OBJECT_FROM_PLAYER_CONTAINER:
            {
                if (_handle_request_query_remove_dynamic_object_from_player_container(
                    &requests[i].query_remove_dynamic_object_from_player_container))
                    goto disconnect;
            }
                break;
            }
        }
    }
    disconnect:
        _free_stmts();
        mysql_close(&_mysql);
        LOG("Requesting disconnect due to a failed query.");
        wdbc_event_t event = {.type = WDBC_EVENT_DISCONNECTED};
        _post_event(&event);
        return 0;
}

static inline wdbc_query_id_t
_next_query_id(void)
{
    _running_query_id++;
    if (_running_query_id == WDBC_INVALID_QUERY_ID)
        _running_query_id++;
    return _running_query_id;
}

static MYSQL_STMT *
_prepare_statement(const char *query)
{
    MYSQL_STMT *stmt = mysql_stmt_init(&_mysql);
    if (!stmt)
    {
        LOG("mysql_stmt_init() failed: %s.", mysql_error(&_mysql));
        muta_panic_print("mysql_stmt_init() failed: %s.", mysql_error(&_mysql));
    }
    if (mysql_stmt_prepare(stmt, query, -1))
    {
        LOG("mysql_stmt_prepare() failed. Query was:\n%s\nMYSQL error: %s.",
            query, mysql_stmt_error(stmt));
        mysql_stmt_close(stmt);
        muta_panic_print("mysql_stmt_prepare() failed: %s.",
            mysql_stmt_error(stmt));
    }
    return stmt;
}

static inline MYSQL_BIND
_bind_account_id(account_db_id_t *buf)
{
    MYSQL_BIND bind =
    {
        .buffer_type    = MYSQL_TYPE_LONGLONG,
        .buffer         = buf,
        .buffer_length  = sizeof(account_db_id_t),
        .is_unsigned    = 1
    };
    return bind;
}

static inline MYSQL_BIND
_bind_null_terminated_str(char *buf, unsigned long buf_len)
{
    muta_assert(buf_len > 0);
    static char nts_indicator = STMT_INDICATOR_NTS;
    MYSQL_BIND bind =
    {
        .buffer_type    = MYSQL_TYPE_VAR_STRING,
        .buffer         = buf,
        .buffer_length  = buf_len - 1,
        .u.indicator    = &nts_indicator
    };
    return bind;
}

static inline MYSQL_BIND
_bind_non_null_terminated_str(char *buf, unsigned long buf_len)
{
    muta_assert(buf_len > 0);
    MYSQL_BIND bind =
    {
        .buffer_type    = MYSQL_TYPE_STRING,
        .buffer         = buf,
        .buffer_length  = buf_len
    };
    return bind;
}

static inline MYSQL_BIND
_bind_int8(int8 *buf)
{
    MYSQL_BIND bind =
    {
        .buffer_type    = MYSQL_TYPE_TINY,
        .buffer         = buf,
        .buffer_length  = sizeof(int8),
        .is_unsigned    = 0
    };
    return bind;
}

static inline MYSQL_BIND
_bind_uint8(uint8 *buf)
{
    MYSQL_BIND bind =
    {
        .buffer_type    = MYSQL_TYPE_TINY,
        .buffer         = buf,
        .buffer_length  = sizeof(int8),
        .is_unsigned    = 1
    };
    return bind;
}

static inline MYSQL_BIND
_bind_int32(int32 *buf)
{
    MYSQL_BIND bind =
    {
        .buffer_type    = MYSQL_TYPE_LONG,
        .buffer         = buf,
        .buffer_length  = sizeof(int32),
        .is_unsigned    = 0
    };
    return bind;
}

static inline MYSQL_BIND
_bind_uint32(uint32 *buf)
{
    MYSQL_BIND bind =
    {
        .buffer_type      = MYSQL_TYPE_LONG,
        .buffer           = buf,
        .buffer_length    = sizeof(uint32),
        .is_unsigned      = 1
    };
    return bind;
}

static inline MYSQL_BIND
_bind_int64(int64 *buf)
{
    MYSQL_BIND bind =
    {
        .buffer_type    = MYSQL_TYPE_LONGLONG,
        .buffer         = buf,
        .buffer_length  = sizeof(int64),
        .is_unsigned    = 0
    };
    return bind;
}

static inline MYSQL_BIND
_bind_uint64(uint64 *buf)
{
    MYSQL_BIND bind =
    {
        .buffer_type      = MYSQL_TYPE_LONGLONG,
        .buffer           = buf,
        .buffer_length    = sizeof(uint64),
        .is_unsigned      = 1
    };
    return bind;
}

static inline MYSQL_BIND
_bind_equipment_slot(equipment_slot_id_t *slot_id)
{
    return _bind_uint8(slot_id);
}

static inline MYSQL_BIND
_bind_blob(uint8 *buf, unsigned long buf_len)
{
    muta_assert(buf_len > 0);
    MYSQL_BIND bind =
    {
        .buffer_type    = MYSQL_TYPE_BLOB,
        .buffer         = buf,
        .buffer_length  = buf_len
    };
    return bind;
}

static int
_generate_uuid(uuid16_t *ret_uuid)
{
    memset(ret_uuid, 0, sizeof(*ret_uuid));
    MYSQL_BIND results[1] =
    {
        _bind_blob(ret_uuid->data, sizeof(ret_uuid->data))
    };
    MYSQL_STMT *stmt = _query_generate_uuid_stmt;
    if (_execute_statement_with_results(stmt, 0, results))
    {
        LOG_ERROR("Failed to generate uuid.");
        return 1;
    }
    if (mysql_stmt_fetch(stmt))
    {
        LOG_ERROR("Failed to generate uuid, mysql_stmt_fetch() failed: ",
            mysql_stmt_error(stmt));
        return 1;
    }
    muta_assert(mysql_stmt_fetch(stmt) == MYSQL_NO_DATA);
    return 0;
}

static int
_handle_request_query_player_character_list(
    query_player_character_list_t *query)
{
    LOG_DEBUG("Handling query player character list for account id %" PRIu64
        ".", query->account_id);
    uint32 max_characters = MAX_CHARACTERS_PER_ACC;
    MYSQL_BIND args[2] =
    {
        _bind_account_id(&query->account_id),
        _bind_uint32(&max_characters)
    };
    uint64  id;
    char    name[MAX_CHARACTER_NAME_LEN + 1] = {0};
    uint8   race;
    uint8   sex;
    uint8   direction;
    uint32  instance_id;
    int32   x;
    int32   y;
    uint8   z;
    MYSQL_BIND results[9] =
    {
        _bind_uint64(&id),
        _bind_null_terminated_str(name, sizeof(name)),
        _bind_uint8(&race),
        _bind_uint8(&sex),
        _bind_uint8(&direction),
        _bind_uint32(&instance_id),
        _bind_int32(&x),
        _bind_int32(&y),
        _bind_uint8(&z)
    };
    if (_execute_statement_with_results(_query_player_character_list_stmt, args,
        results))
    {
        LOG_ERROR("Failed to execute query for player character list of "
            "account id %" PRIu64 ".", query->account_id);
        return 1;
    }
    wdbc_event_t event =
    {
        .type = WDBC_EVENT_COMPLETED_QUERY_PLAYER_CHARACTER_LIST,
        .completed_query_player_character_list.query_id = query->query_id,
        .completed_query_player_character_list.num_characters   = 0,
        .completed_query_player_character_list.error            = 0
    };
    wdbc_completed_query_player_character_list_event_t *event_data =
        &event.completed_query_player_character_list;
    for (;;)
    {
        if (event_data->num_characters == MAX_CHARACTERS_PER_ACC)
            break;
        int r = mysql_stmt_fetch(_query_player_character_list_stmt);
        if (r == MYSQL_NO_DATA)
            break;
        if (r)
        {
            LOG_ERROR("Error fetching player character: %s.",
                mysql_stmt_error(_query_player_character_list_stmt));
            event.completed_query_player_character_list.error = 1;
            break;
        }
        strcpy(event_data->characters[event_data->num_characters].name,
            name);
        event_data->characters[event_data->num_characters].id   = id;
        event_data->characters[event_data->num_characters].race = race;
        event_data->characters[event_data->num_characters].sex  = sex;
        event_data->characters[event_data->num_characters].direction
            = direction;
        event_data->characters[event_data->num_characters].instance_id
            = instance_id;
        event_data->characters[event_data->num_characters].x = x;
        event_data->characters[event_data->num_characters].y = y;
        event_data->characters[event_data->num_characters].z = z;
        event_data->num_characters++;
    }
    LOG_DEBUG("Found %d characters for account id %" PRIu64 ".",
        (int)event_data->num_characters, query->account_id);
    for (int i = 0; i < (int)event_data->num_characters; ++i)
        LOG_DEBUG("Character: id %" PRIu64 ", name %s.",
            event_data->characters[i].id, event_data->characters[i].name);
    _post_event(&event);
    return 0;
}

static int
_handle_request_query_player_character(query_player_character_t *query)
{
    MYSQL_BIND character_args[] =
    {
        _bind_account_id(&query->account_db_id),
        _bind_uint64(&query->player_db_id)
    };
    uint64  player_id;
    char    name[MAX_CHARACTER_NAME_LEN + 1];
    uint8   race;
    uint8   sex;
    uint8   direction;
    uint32  instance_id;
    int32   x;
    int32   y;
    uint8   z;
    MYSQL_BIND character_results[] =
    {
        _bind_uint64(&player_id),
        _bind_null_terminated_str(name, sizeof(name)),
        _bind_uint8(&race),
        _bind_uint8(&sex),
        _bind_uint8(&direction),
        _bind_uint32(&instance_id),
        _bind_int32(&x),
        _bind_int32(&y),
        _bind_uint8(&z)
    };
    if (_execute_statement_with_results(_query_player_character_stmt,
        character_args, character_results))
    {
        LOG_ERROR("Failed to execute query player character.\n");
        return 1;
    }
    int r = mysql_stmt_fetch(_query_player_character_stmt);
    if (r == MYSQL_NO_DATA)
    {
        LOG_DEBUG("No character %" PRIu64 " with account id %" PRIu64 " found.",
            query->player_db_id, query->account_db_id);
        wdbc_event_t event =
        {
            .type = WDBC_EVENT_COMPLETED_QUERY_PLAYER_CHARACTER,
            .completed_query_player_character.query_id  = query->query_id,
            .completed_query_player_character.error     = 1
        };
        _post_event(&event);
        return 0;
    } else if (r)
    {
        LOG_DEBUG("mysql_stmt_fetch failed: %s.",
            mysql_stmt_error(_query_player_character_stmt));
        wdbc_event_t event =
        {
            .type = WDBC_EVENT_COMPLETED_QUERY_PLAYER_CHARACTER,
            .completed_query_player_character.query_id  = query->query_id,
            .completed_query_player_character.error     = 2
        };
        _post_event(&event);
        return 0;
    }
    r = mysql_stmt_fetch(_query_player_character_stmt);
    muta_assert(r == MYSQL_NO_DATA);
    muta_assert(player_id == query->player_db_id);
    uuid16_t                dobj_db_id;
    dobj_type_id_t   dobj_type_id;
    equipment_slot_id_t     dobj_slot_id;
    int32                   dobj_x;
    int32                   dobj_y;
    MYSQL_BIND dobj_args[] =
    {
        _bind_uint64(&query->player_db_id)
    };
    char owner_type_buf[32];
    MYSQL_BIND dobj_results[] =
    {
        _bind_blob(dobj_db_id.data, sizeof(dobj_db_id.data)),
        _bind_null_terminated_str(owner_type_buf, sizeof(owner_type_buf)),
        _bind_uint32(&dobj_type_id),
        _bind_equipment_slot(&dobj_slot_id),
        _bind_int32(&dobj_x),
        _bind_int32(&dobj_y)
    };
    if (_execute_statement_with_results(_query_player_dynamic_objects_stmt,
        dobj_args, dobj_results))
        return 1;
    if (mysql_stmt_store_result(_query_player_dynamic_objects_stmt))
    {
        LOG_ERROR("mysql_stmt_store_result() failed: %s.",
            mysql_error(&_mysql));
        return 1;
    }
    unsigned long long num_items = mysql_stmt_num_rows(
        _query_player_dynamic_objects_stmt);
    if (num_items > UINT32_MAX)
    {
        LOG_ERROR("Too many results (over uint32 max).");
        return 1;
    }
    wdbc_dynamic_object_in_db_t *items = _malloc(
        (uint32)num_items * sizeof(wdbc_dynamic_object_in_db_t));
    for (unsigned long long i = 0; i < num_items; ++i)
    {
        int r = mysql_stmt_fetch(_query_player_dynamic_objects_stmt);
        if (r)
        {
            _free(items);
            LOG_ERROR("mysql_fetch() failed: %s.\n", mysql_error(&_mysql));
            return 1;
        }
        /* Todo: validate */
        wdbc_dynamic_object_in_db_t *item = &items[i];
        item->id = dobj_db_id;
        enum wdbc_dynamic_object_owner_type owner_type;
        if (!strncmp(owner_type_buf, "player_container", 16 + 1))
            owner_type = WDBC_DYNAMIC_OBJECT_OWNER_PLAYER_CONTAINER;
        else if (!strncmp(owner_type_buf, "player_equipped", 15 + 1))
            owner_type = WDBC_DYNAMIC_OBJECT_OWNER_PLAYER_EQUIPPED;
        else
        {
            LOG_ERROR("Bad dynamic object owner type in DB.");
            _free(items);
            return 1;
        }
        item->data.owner_type                               = owner_type;
        item->data.type_id                                  = dobj_type_id;
        item->data.owner.player_container.equipment_slot    = dobj_slot_id;
        item->data.owner.player_container.x                 = (uint8)dobj_x;
        item->data.owner.player_container.y                 = (uint8)dobj_y;
    }
    r = mysql_stmt_fetch(_query_player_dynamic_objects_stmt);
    muta_assert(r == MYSQL_NO_DATA);
    wdbc_event_t event =
    {
        .type = WDBC_EVENT_COMPLETED_QUERY_PLAYER_CHARACTER,
        .completed_query_player_character =
        {
            .query_id  = query->query_id,
            .error     = 0,
            .character =
            {
                .id             = player_id,
                .race           = race,
                .sex            = sex,
                .direction      = direction,
                .instance_id    = instance_id,
                .x              = x,
                .y              = y,
                .z              = z
            },
            .items      = items,
            .num_items  = (uint32)num_items
        }
    };
    strcpy(event.completed_query_player_character.character.name, name);
    LOG_DEBUG("Completed query to get player character, posting event. Args - "
        "account id %" PRIu64 ", character id %" PRIu64 ". Results - id: %"
        PRIu64 ", name: %s, race: %u, sex: %u, direction: %u, instance id: %u, "
        "x: %d, y: %d, z: %u.", query->account_db_id, query->player_db_id,
        player_id, name, (uint)race, (uint)sex, (uint)direction, instance_id, x,
        y, (uint)z);
    _post_event(&event);
    return 0;
}

static int
_handle_request_query_create_player_character(
    query_create_player_character_t *query)
{
    size_t name_len = strlen(query->name) + 1;
    muta_assert(name_len > 0);
    if (_start_transaction())
    {
        LOG_ERROR("_start_transaction failed: %s.", mysql_error(&_mysql));
        return 1;
    }
    {
        MYSQL_BIND args[] =
        {
            _bind_account_id(&query->account_id),
            _bind_null_terminated_str(query->name, name_len),
            _bind_uint8(&query->race),
            _bind_uint8(&query->sex),
            _bind_uint8(&query->direction),
            _bind_uint32(&query->instance_id),
            _bind_int32(&query->x),
            _bind_int32(&query->y),
            _bind_uint8(&query->z)
        };
        if (_execute_statement_without_results(
            _query_create_player_character_stmt, args))
            return 1;
    }
    uint64                  player_db_id    = (uint64)mysql_insert_id(&_mysql);
    wdbc_dynamic_object_t   *items          = query->items;
    uint32                  num_items       = query->num_items;
    for (uint32 i = 0; i < num_items; ++i)
    {
        uuid16_t uuid;
        if (_generate_uuid(&uuid))
            goto rollback;
        wdbc_dynamic_object_t *item = &items[i];
        int32 x = 0;
        int32 y = 0;
        equipment_slot_id_t equipment_slot;
        MYSQL_BIND owner_type_bind;
        switch (items[i].owner_type)
        {
        case WDBC_DYNAMIC_OBJECT_OWNER_PLAYER_CONTAINER:
            owner_type_bind = _bind_null_terminated_str("player_container",
                16 + 1);
            x               = (int32)item->owner.player_container.x;
            y               = (int32)item->owner.player_container.y;
            equipment_slot  = item->owner.player_container.equipment_slot;
            break;
        case WDBC_DYNAMIC_OBJECT_OWNER_PLAYER_EQUIPPED:
            owner_type_bind = _bind_null_terminated_str("player_equipped",
                15 + 1);
            equipment_slot  = item->owner.player_equipped.equipment_slot;
            break;
        default:
            muta_assert(0);
        }
        MYSQL_BIND args[] =
        {
            _bind_blob(uuid.data, sizeof(uuid.data)),
            _bind_uint32(&item->type_id),
            owner_type_bind,
            _bind_int32(&x),
            _bind_int32(&y),
            _bind_uint64(&player_db_id),
            _bind_equipment_slot(&equipment_slot)
        };
        if (_execute_statement_without_results(
            _query_create_dynamic_object_in_player_container_stmt, args))
            return 1;
    }
    wdbc_event_t event =
    {
        .type = WDBC_EVENT_COMPLETED_QUERY_CREATE_PLAYER_CHARACTER,
        .completed_query_create_player_character.error = 0,
        .completed_query_create_player_character.query_id = query->query_id,
        .completed_query_create_player_character.character =
        {
            .id             = player_db_id,
            .race           = query->race,
            .sex            = query->sex,
            .direction      = query->direction,
            .instance_id    = query->instance_id,
            .x              = query->x,
            .y              = query->y,
            .z              = query->z
        }
    };
    strcpy(event.completed_query_create_player_character.character.name,
        query->name);
    if (_commit_transaction())
    {
        LOG_ERROR("_commit_transaction failed: %s.", mysql_error(&_mysql));
        goto rollback;
    }
    _post_event(&event);
    return 0;
    rollback:
        LOG_ERROR("Errors, rolling back transaction.");
        if (_rollback_transaction())
            return 1;
        wdbc_event_t fail_event =
        {
            .type = WDBC_EVENT_COMPLETED_QUERY_CREATE_PLAYER_CHARACTER,
            .completed_query_create_player_character = {.error = 0}
        };
        _post_event(&fail_event);
        return 0;
}

static int
_handle_request_query_create_dynamic_object_in_player_container(
    query_create_dynamic_object_in_player_container_t *query)
{
    uuid16_t uuid;
    if (_generate_uuid(&uuid))
    {
        LOG_ERROR("Failed to generate UUID.");
        return 1;
    }
    int32_t             x               = (int32_t)query->x;
    int32_t             y               = (int32_t)query->y;
    equipment_slot_id_t equipment_slot  = query->equipment_slot;
    MYSQL_BIND args[7] =
    {
        _bind_blob(uuid.data, sizeof(uuid.data)),
        _bind_uint32(&query->type_id),
        _bind_null_terminated_str("player_container", 16 + 1),
        _bind_int32(&x),
        _bind_int32(&y),
        _bind_uint64(&query->player_db_id),
        _bind_equipment_slot(&equipment_slot)
    };
    if (_execute_statement_without_results(
        _query_create_dynamic_object_in_player_container_stmt, args))
    {
        LOG_ERROR("Failed to execute query create dynamic object in player "
            "bag.");
        return 1;
    }
    wdbc_event_t event =
    {
        .type = WDBC_EVENT_COMPLETED_QUERY_CREATE_DYNAMIC_OBJECT_IN_PLAYER_CONTAINER,
        .completed_query_create_dynamic_object_in_player_container =
        {
            .query_id   = query->query_id,
            .error      = 0,
            .dobj       =
            {
                .id     = uuid,
                .data   =
                {
                    .type_id    = query->type_id,
                    .owner_type = WDBC_DYNAMIC_OBJECT_OWNER_PLAYER_CONTAINER,
                    .owner.player_container =
                    {
                        .player_db_id   = query->player_db_id,
                        .x              = query->x,
                        .y              = query->y,
                        .equipment_slot = query->equipment_slot
                    }
                }
            }
        }
    };
    _post_event(&event);
    return 0;
}

static int
_handle_request_query_save_player_character_position_and_direction(
    query_save_player_character_position_and_direction_t *query)
{
    MYSQL_STMT *stmt = _query_save_player_character_position_direction_stmt;
    MYSQL_BIND args[6] =
    {
        _bind_uint32(&query->instance_id),
        _bind_uint8(&query->direction),
        _bind_int32(&query->x),
        _bind_int32(&query->y),
        _bind_uint8(&query->z),
        _bind_uint64(&query->player_db_id)
    };
    if (_execute_statement_without_results(stmt, args))
    {
        LOG_ERROR("Failed to execute query save player character position and "
            "direction.");
        return 1;
    }
    int error = 0;
    if (!mysql_stmt_affected_rows(stmt))
    {
        LOG_ERROR("msyql_stmt_affected_rows() returned 0.");
        error = 2;
    }
    wdbc_event_t event =
    {
        .type = WDBC_EVENT_COMPLETED_QUERY_SAVE_PLAYER_CHARACTER_POSITION_AND_DIRECTION,
        .completed_query_save_player_character_position_and_direction =
        {
            .query_id       = query->query_id,
            .error          = error,
            .instance_id    = query->instance_id,
            .direction      = query->direction,
            .x              = query->x,
            .y              = query->y,
            .z              = query->z
        }
    };
    _post_event(&event);
    return 0;
}

static int
_handle_request_query_save_dynamic_object_position_in_player_inventory(
    query_save_dynamic_object_position_in_player_inventory_t *query)
{
    MYSQL_STMT *stmt =
        _query_save_dynamic_object_position_in_player_inventory_stmt;
    equipment_slot_id_t equipment_slot  = query->equipment_slot;
    int32               x               = (int32)query->x;
    int32               y               = (int32)query->y;
    muta_assert(equipment_slot < NUM_EQUIPMENTS_SLOTS);
    MYSQL_BIND args[4] =
    {
        _bind_equipment_slot(&equipment_slot),
        _bind_int32(&x),
        _bind_int32(&y),
        _bind_blob(query->dobj_uuid.data, sizeof(query->dobj_uuid.data)),
    };
    if (_execute_statement_without_results(stmt, args))
    {
        LOG_ERROR("Failed to execute query save dynamic object position in "
            "player inventory.");
        return 1;
    }
    int error = 0;
    if (!mysql_stmt_affected_rows(stmt))
    {
        LOG_ERROR("msyql_stmt_affected_rows() returned 0.");
        error = 2;
    }
    wdbc_event_t event =
    {
        .type = WDBC_EVENT_COMPLETED_QUERY_SAVE_DYNAMIC_OBJECT_POSITION_IN_PLAYER_INVENTORY,
        .completed_query_save_dynamic_object_position_in_player_inventory =
        {
            .query_id       = query->query_id,
            .error          = error,
            .equipment_slot = query->equipment_slot,
            .x              = query->x,
            .y              = query->y
        }
    };
    _post_event(&event);
    return 0;
}

static int
_handle_request_query_remove_dynamic_object_from_player_container(
    query_remove_dynamic_object_from_player_container_t *query)
{
    MYSQL_STMT *stmt = _query_remove_dynamic_object_from_player_container_stmt;
    MYSQL_BIND args[1] =
    {
        _bind_blob(query->dobj_uuid.data, sizeof(query->dobj_uuid.data))
    };
    if (_execute_statement_without_results(stmt, args))
    {
        LOG_ERROR("Failed to execute query remove dynamic object from player "
            "container");
        return 1;
    }
    int error = 0;
    if (!mysql_stmt_affected_rows(stmt))
    {
        LOG_ERROR("msyql_stmt_affected_rows() returned 0.");
        error = 2;
    }
    wdbc_event_t event =
    {
        .type = WDBC_EVENT_COMPLETED_QUERY_REMOVE_DYNAMIC_OBJECT_FROM_PLAYER_CONTAINER,
        .completed_query_save_dynamic_object_position_in_player_inventory =
        {
            .query_id       = query->query_id,
            .error          = error,
        }
    };
    _post_event(&event);



    return 0;
}

static int
_execute_statement_with_results(MYSQL_STMT *stmt, MYSQL_BIND *args,
    MYSQL_BIND *results)
{
    if (mysql_stmt_reset(stmt))
    {
        LOG("musql_stmt_reset() failed: %s.", mysql_stmt_error(stmt));
        muta_panic_print("musql_stmt_reset() failed: %s.",
            mysql_stmt_error(stmt));
    }
    if (mysql_stmt_bind_param(stmt, args))
    {
        LOG("mysql_stmt_bind_param() failed: %s.", mysql_stmt_error(stmt));
        muta_panic_print("mysql_stmt_bind_param() failed: %s.",
            mysql_stmt_error(stmt));
    }
    if (mysql_stmt_bind_result(stmt, results))
    {
        LOG_ERROR("mysql_stmt_bind_result() failed: %s.",
            mysql_stmt_error(stmt));
        muta_panic_print("mysql_stmt_bind_result() failed: %s.",
            mysql_stmt_error(stmt));
    }
    if (mysql_stmt_execute(stmt))
    {
        LOG_ERROR("mysql_stmt_execute() failed: %s.", mysql_stmt_error(stmt));
        return 1;
    }
#if 0
    if (mysql_stmt_store_result(stmt))
    {
        LOG_ERROR("mysql_stmt_store_result() failed: %s.",
            mysql_stmt_error(stmt));
        muta_panic_print("mysql_stmt_store_result() failed: %s.",
            mysql_stmt_error(stmt));
    }
#endif
    return 0;
}

static int
_execute_statement_without_results(MYSQL_STMT *stmt, MYSQL_BIND *args)
{
    if (mysql_stmt_reset(stmt))
    {
        LOG("musql_stmt_reset() failed: %s.", mysql_stmt_error(stmt));
        muta_panic_print("musql_stmt_reset() failed: %s.",
            mysql_stmt_error(stmt));
    }
    if (mysql_stmt_bind_param(stmt, args))
    {
        LOG("mysql_stmt_bind_param() failed: %s.", mysql_stmt_error(stmt));
        muta_panic_print("mysql_stmt_bind_param() failed: %s.",
            mysql_stmt_error(stmt));
    }
    if (mysql_stmt_execute(stmt))
    {
        LOG_ERROR("mysql_stmt_execute() failed: %s.", mysql_stmt_error(stmt));
        return 1;
    }
    return 0;
}

static void
_push_request(wdbc_query_id_t query_id, request_t *request)
{
    hashtable_einsert(_request_table, query_id,
        hashtable_hash(&query_id, sizeof(query_id)), *request);
    event_push(&_requests, request, 1);
}

static void
_free_stmts(void)
{
    mysql_stmt_close(_query_generate_uuid_stmt);
    mysql_stmt_close(_query_player_character_list_stmt);
    mysql_stmt_close(_query_player_character_stmt);
    mysql_stmt_close(_query_create_player_character_stmt);
    mysql_stmt_close(_query_create_dynamic_object_in_player_container_stmt);
    mysql_stmt_close(_query_save_player_character_position_direction_stmt);
    mysql_stmt_close(_query_player_dynamic_objects_stmt);
    mysql_stmt_close(
        _query_save_dynamic_object_position_in_player_inventory_stmt);
    mysql_stmt_close(
        _query_remove_dynamic_object_from_player_container_stmt);
}

static void
_request_disconnect(const char *reason)
{
    if (reason)
        LOG("Requesting WDBD disconnect, reason: %s.", reason);
    request_t request = {.type = REQUEST_DISCONNECT};
    event_push(&_requests, &request, 1);
}

static inline int
_start_transaction(void)
    {return mysql_query(&_mysql, "START TRANSACTION");}

static inline int
_commit_transaction(void)
    {return mysql_commit(&_mysql);}

static inline int
_rollback_transaction(void)
    {return mysql_rollback(&_mysql);}

static void *
_malloc(uint32 num_bytes)
{
    mutex_lock(&_segpool_mutex);
    void *ret = segpool_malloc(&_segpool, num_bytes);
    mutex_unlock(&_segpool_mutex);
    return ret;
}

static void
_free(void *mem)
{
    mutex_lock(&_segpool_mutex);
    segpool_free(&_segpool, mem);
    mutex_unlock(&_segpool_mutex);
}
