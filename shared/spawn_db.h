#ifndef MUTA_SHARED_SPAWN_DB_H
#define MUTA_SHARED_SPAWN_DB_H

#include "mdb.h"
#include "common_defs.h"

typedef struct spawn_db_t spawn_db_t;

enum spawn_db_spawn_type
{
    SPAWN_DB_SPAWN_TYPE_DYNAMIC_OBJECT = 0,
    SPAWN_DB_SPAWN_TYPE_CREATURE
};

enum spawn_db_index
{
    SPAWN_DB_INDEX_ID = 0,
    SPAWN_DB_INDEX_TAG,
    SPAWN_DB_INDEX_SPAWN_TYPE,
    SPAWN_DB_INDEX_TYPE_ID,
    SPAWN_DB_INDEX_X,
    SPAWN_DB_INDEX_Y,
    SPAWN_DB_INDEX_Z,
    SPAWN_DB_INDEX_DIRECTION,
    SPAWN_DB_INDEX_SEX,
    NUM_SPAWN_DB_INDICES
};

struct spawn_db_t
{
    mdb_t   mdb;
    int     indices[NUM_SPAWN_DB_INDICES];
};

int
spawn_db_open(spawn_db_t *db, const char *file_path);

void
spawn_db_close(spawn_db_t *db);

uint32
spawn_db_num_rows(spawn_db_t *db);

mdb_row_t
spawn_db_get_row_by_index(spawn_db_t *db, uint32 index);

uint32
spawn_db_get_id(spawn_db_t *db, mdb_row_t row);

dchar *
spawn_db_get_tag(spawn_db_t *db, mdb_row_t row);

enum spawn_db_spawn_type
spawn_db_get_spawn_type(spawn_db_t *db, mdb_row_t row);

uint32
spawn_db_get_type_id(spawn_db_t *db, mdb_row_t row);

enum iso_dir
spawn_db_get_direction(spawn_db_t *db, mdb_row_t row);

int32_t
spawn_db_get_x(spawn_db_t *db, mdb_row_t row);

int32_t
spawn_db_get_y(spawn_db_t *db, mdb_row_t row);

uint8
spawn_db_get_z(spawn_db_t *db, mdb_row_t row);

int
spawn_db_get_sex(spawn_db_t *db, mdb_row_t row);
/* Return value is one of those defined in enum ent_sex entities.h. */

#endif /* MUTA_SHARED_SPAWN_DB_H */
