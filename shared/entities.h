/* entities.h
 * Entity template definitions. The entity types of MUTA are:
 *
 * - Player
 *   Self-explanatory
 *
 * - Creature
 *   Mobile objects.
 *
 * - Dynamic object
 *   Immobile objects such as trees, etc.
 *
 * - Static object
 *   Stored in the map data, cannot be moved or interacted with. */

#ifndef MUTA_SHARED_ENTITIES_H
#define MUTA_SHARED_ENTITIES_H

#include "common_defs.h"

#define ENT_LOAD_ALL \
    (ENT_LOAD_CREATURES | ENT_LOAD_PLAYER_RACES | ENT_LOAD_DYNAMIC_OBJECTS | \
    ENT_LOAD_STATIC_OBJECTS)
#define ENT_BASE_MOVE_SPEED             228
#define ENT_MOVE_MAX_SECONDS            5.f
/* The most amount of time traveling from a tile to the next may take. Speed,
 * represented as a value between 0 to 255, is a reverse percentage of this. */
#define ENT_MAX_CREATURE_TYPE_NAME_LEN  255

#define ENT_MOVE_SPEED_TO_SECONDS(move_speed) \
    (ENT_MOVE_MAX_SECONDS - \
    (((float)move_speed) / 255.f * ENT_MOVE_MAX_SECONDS))

/* Types defined here */
typedef struct static_object_def_t  static_object_def_t;
typedef struct dynamic_object_def_t dynamic_object_def_t;
typedef struct player_race_def_t    player_race_def_t;
typedef struct creature_def_t       creature_def_t;

enum ent_load_flag
{
    ENT_LOAD_CREATURES          = (1 << 0),
    ENT_LOAD_PLAYER_RACES       = (1 << 1),
    ENT_LOAD_DYNAMIC_OBJECTS    = (1 << 2),
    ENT_LOAD_STATIC_OBJECTS     = (1 << 3)
};

enum ent_graphic_type
{
    ENT_GRAPHIC_NONE        = 0,
    ENT_GRAPHIC_ISO_SPRITE  = 1,
    ENT_GRAPHIC_AE_SET      = 2
};

enum ent_sex
{
    ENT_SEX_MALE = 0,
    ENT_SEX_FEMALE
};

enum ent_creature_flags
{
    ENT_CREATURE_SEX         = (1 << 0),
    ENT_CREATURE_TALKABLE    = (1 << 1),
    ENT_CREATURE_ATTACKABLE  = (1 << 2)
};

enum ent_dynamic_object_flags
{
    ENT_DYNAMIC_OBJECT_DISABLE_PICK_UP = (1 << 0)
};

/* Static object definition */
struct static_object_def_t
{
    sobj_type_id_t  id;
    dchar           *name;
    dchar           *description;
    union
    {
        uint32 iso_sprite;
    } graphic_data;
    uint8 graphic_type;
    uint8 passthrough;
};

/* Player race definition */
struct player_race_def_t
{
    player_race_id_t    id;
    dchar               *name;
    dchar               *description;
    uint32              display_id;
    uint32              start_area_id;
};

/* Dynamic object definition */
struct dynamic_object_def_t
{
    dobj_type_id_t  id;
    dchar           *name;
    dchar           *description;
    uint32          display_id;
    bool8           is_container;
    uint8           container_width;
    uint8           container_height;
    uint8           width_in_container;
    uint8           height_in_container;
    uint8           flags;
};

/* Creature definition */
struct creature_def_t
{
    creature_type_id_t  id;
    dchar               *name;
    dchar               *description;
    uint32              display_id;
    /* display
     * A creature's display is a set of presentative traits, such as the
     * creature's animations and sound effects bundled together. */
    dchar               *script_name; /* script_name: tmp */
};

int
ent_load(uint32 flags);
/* ent_load()
 * Load entity definitions. Use the flags parameter to specify which definitions
 * will be loaded - see enum ent_load_flag. If flags is 0, all definitions will
 * be loaded.
 * The return value is 0 on success. On failure, it is a bitflag of the ENT_LOAD
 * flags that failed to load. */

void
ent_destroy(void);

creature_def_t *
ent_get_creature_def(creature_type_id_t id);

static_object_def_t *
ent_get_static_object_def(sobj_type_id_t id);

dynamic_object_def_t *
ent_get_dynamic_object_def(dobj_type_id_t id);

player_race_def_t *
ent_get_player_race_def(player_race_id_t id);

uint32
ent_num_creature_defs(void);

uint32
ent_num_static_object_defs(void);

uint32
ent_num_dynamic_object_defs(void);

uint32
ent_num_player_race_defs(void);

void
ent_for_each_creature_def(
    void (*callback)(creature_def_t *def, void *user_data), void *user_data);

void
ent_for_each_static_object_def(
    void (*callback)(static_object_def_t *def, void *user_data),
    void *user_data);

void
ent_for_each_dynamic_object_def(
    void (*callback)(dynamic_object_def_t *def, void *user_data),
    void *user_data);

void
ent_for_each_player_race_def(
    void (*callback)(player_race_def_t *def, void *user_data), void *user_data);

float
ent_convert_move_speed_to_seconds(uint8 move_speed);

#endif /* MUTA_SHARED_ENTITIES_H */
