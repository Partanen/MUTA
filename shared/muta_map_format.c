#include "muta_map_format.h"
#include "ksys.h"
#include "types.h"
#include "stb/stb_sprintf.h"
#include "common_utils.h"

typedef struct map_db_load_context_t map_db_load_context_t;

#define NUM_TILES_PER_CHUNK \
    (MAP_CHUNK_W * MAP_CHUNK_W * MAP_CHUNK_T)
#define MAP_PADDING         64
#define CHUNK_FILE_PADDING  64
#define MAP_DB_PADDING      32

struct map_db_load_context_t
{
    muta_map_db_t   *db;
    dchar           *file_name;
    bool32          have_filename;
};

static int
_on_map_db_def(parse_def_file_context_t *ctx, const char *def, const char *val);

static int
_on_map_db_opt(parse_def_file_context_t *ctx, const char *opt, const char *val);

static int
_finalize_map_db_entry(map_db_load_context_t *context);

int
muta_map_file_init_for_edit(muta_map_file_t *mf, const char *name,
    uint32 version, uint32 w, uint32 h)
{
    memset(mf, 0, sizeof(muta_map_file_t));
    int ret = 0;
    if (muta_map_file_set_name(mf, name))
        return 1;
    mf->header.version = version;
    mf->relative_chunk_paths = calloc(w * h, sizeof(char*));
    if (!mf->relative_chunk_paths)
        {ret = 2; goto out;}
    mf->absolute_chunk_paths = calloc(w * h, sizeof(char*));
    if (!mf->absolute_chunk_paths)
        {ret = 3; goto out;}
    mf->header.w = w;
    mf->header.h = h;
    uint32 gv[3] = MUTA_VERSION_ARRAY;
    for (int i = 0; i < 3; ++i)
        mf->header.game_version[i] = gv[i];
    out:
        if (ret)
            muta_map_file_destroy(mf);
        return ret;

}

int
muta_map_file_load(muta_map_file_t *mf, const char *path)
{
    memset(mf, 0, sizeof(muta_map_file_t));
    /* Create a temporary spot for file paths */
    uint dir_path_len = (uint)strlen(path);
    for (int i = dir_path_len - 1; i >= 0; --i)
    {
        if (path[i] != '/' && i != 0)
            continue;
        dir_path_len = i + 1;
        break;
    }
    DEBUG_PRINTFF("opening file %s.\n", path);
    /* Read file */
    FILE *f = fopen(path, "rb");
    if (!f)
        return 1;
    int ret = 0;
    if (fread_uint8_arr(f, (uint8*)mf->header.name, MUTA_MAP_FILE_NAME_LEN))
        {ret = 2; goto out;}
    if (fread_uint32(f, &mf->header.id))
        {ret = 3; goto out;}
    if (fread_uint32(f, &mf->header.version))
        {ret = 4; goto out;}
    if (fread_uint32_arr(f, mf->header.game_version, 3))
        {ret = 5; goto out;}
    uint32 dims[2];
    if (fread_uint32_arr(f, dims, 2))
        {ret = 6; goto out;}
    mf->relative_chunk_paths = calloc(dims[0] * dims[1], sizeof(char*));
    if (!mf->relative_chunk_paths)
        {ret = 7; goto out;}
    mf->absolute_chunk_paths = calloc(dims[0] * dims[1], sizeof(char*));
    if (!mf->relative_chunk_paths)
        {ret = 8; goto out;}
    mf->header.w = dims[0];
    mf->header.h = dims[1];
    if (fseek(f, MAP_PADDING, SEEK_CUR))
        {ret = 9; goto out;}
    uint32 i, j;
    uint16 name_len;
    for (i = 0 ; i < mf->header.w; ++i)
        for (j = 0; j < mf->header.h; ++j)
        {
            if (fread_uint16(f, &name_len))
                {ret = 10; goto out;}
            dchar **rel_path = &mf->relative_chunk_paths[j * mf->header.w + i];
            *rel_path = create_empty_dynamic_str(name_len);
            if (!*rel_path)
                {ret = 11; goto out;}
            if (fread_uint8_arr(f, (uint8*)*rel_path, name_len))
                {ret = 12; goto out;}
            set_dynamic_str_len(*rel_path, name_len);
            /* Set absolute path */
            dchar **abs_path =  &mf->absolute_chunk_paths[j * mf->header.w + i];
            *abs_path = dstr_create_empty(dir_path_len + 1 + name_len);
            if (!*abs_path)
                {ret = 13; goto out;}
            memcpy(*abs_path, path, dir_path_len);
            (*abs_path)[dir_path_len] = '/';
            memcpy(*abs_path + dir_path_len + 1, *rel_path, name_len);
            set_dynamic_str_len(*abs_path, dir_path_len + 1 + name_len);
        }
    out:
        if (ret)
            muta_map_file_destroy(mf);
        fclose(f);
        return ret;
}

int
muta_map_file_save(muta_map_file_t *mf, const char *path)
{
    if (!str_is_valid_file_name(mf->header.name))
        return 1;
    if (mf->header.w < 1 || mf->header.h < 1)
        return 2;
    uint32 game_version[3] = MUTA_VERSION_ARRAY;
    for (int i = 0; i < 3; ++i)
        mf->header.game_version[i] = game_version[i];
    FILE *f = fopen(path, "wb+");
    if (!f)
        return 4;
    int ret = 0;
    if (fwrite_uint8_arr(f, (uint8*)mf->header.name, MUTA_MAP_FILE_NAME_LEN))
        {ret = 4; goto out;}
    if (fwrite_uint32(f, mf->header.id))
        {ret = 5; goto out;}
    if (fwrite_uint32(f, mf->header.version))
        {ret = 6; goto out;}
    if (fwrite_uint32_arr(f, mf->header.game_version, 3))
        {ret = 7; goto out;}
    if (fwrite_uint32(f, mf->header.w))
        {ret = 8; goto out;}
    if (fwrite_uint32(f, mf->header.h))
        {ret = 9; goto out;}
    /* Padding */
    if (fseek(f, MAP_PADDING, SEEK_CUR))
        {ret = 10; goto out;}
    uint32  i, j;
    for (i = 0; i < mf->header.w; ++i)
        for (j = 0; j < mf->header.h; ++j)
        {
            dchar   *fp     = muta_map_file_get_chunk_relative_path(mf, i, j);
            uint16  fp_len  = (uint16)dstr_len(fp);
            if (!str_is_valid_file_path(fp))
                {ret = 11; goto out;}
            if (fwrite_uint16(f, fp_len))
                {ret = 12; goto out;}
            if (fwrite_uint8_arr(f, (uint8*)fp, fp_len))
                {ret = 13; goto out;}
        }
    out:
        fclose(f);
        return ret;
}

void
muta_map_file_destroy(muta_map_file_t *mf)
{
    uint32 num = mf->header.w * mf->header.h;
    for (uint32 i = 0; i < num; ++i)
        free_dynamic_str(mf->relative_chunk_paths[i]);
    for (uint32 i = 0; i < num; ++i)
        free_dynamic_str(mf->absolute_chunk_paths[i]);
    free(mf->relative_chunk_paths);
    free(mf->absolute_chunk_paths);
    memset(mf, 0, sizeof(muta_map_file_t));
}

int
muta_map_file_set_name(muta_map_file_t *mf, const char *name)
{
    if (!str_is_valid_file_name(name))
        return 1;
    uint len = (uint)strlen(name);
    if (len > MUTA_MAP_FILE_NAME_LEN)
        return 2;
    memcpy(mf->header.name, name, len + 1);
    return 0;
}

int
muta_map_file_set_chunk_path(muta_map_file_t *mf, uint32 x, uint32 y,
    const char *path)
{
    if (mf->header.w <= x || mf->header.h <= y)
        return 1;
    if (!mf->relative_chunk_paths)
        return 2;
    if (!str_is_valid_file_path(path))
        return 3;
    dchar **s = &mf->relative_chunk_paths[y * mf->header.w + x];
    dchar *ns = set_dynamic_str(*s, path);
    if (!ns)
        return 4;
    *s = ns;
    return 0;
}

char *
muta_map_file_get_chunk_relative_path(muta_map_file_t *mf, uint32 x, uint32 y)
{
    if (x >= mf->header.w || y >= mf->header.h)
        return 0;
    if (!mf->relative_chunk_paths)
        return 0;
    return mf->relative_chunk_paths[y * mf->header.w + x];
}

char *
muta_map_file_get_chunk_absolute_path(muta_map_file_t *mf, uint32 x, uint32 y)
{

    if (x >= mf->header.w || y >= mf->header.h)
        return 0;
    if (!mf->relative_chunk_paths)
        return 0;
    return mf->absolute_chunk_paths[y * mf->header.w + x];
}

char *
muta_map_file_get_chunk_name(muta_map_file_t *mf, uint32 x, uint32 y)
{
    char *n = muta_map_file_get_chunk_relative_path(mf, x, y);
    if (!n) return 0;
    uint path_len = (uint)strlen(n);
    char *ret;
    for (int i = path_len - 1; i >= 0; --i)
    {
        if (n[i] != '/' && i != 0)
            continue;
        ret = n + i;
        if (i != 0)
            ret++;
        break;
    }
    return ret;
}

int
muta_map_file_set_chunk_path_pattern(muta_map_file_t *mf,
    const char *directory_path, const char *pattern)
{
    if (!str_is_valid_file_path(pattern))
        return 1;
    uint32 x, y;
    for (x = 0; x < mf->header.w; ++x)
        for (y = 0; y < mf->header.h; ++y)
        {
            dchar **rel_path = &mf->relative_chunk_paths[y * mf->header.w + x];
            dstr_setf(rel_path, "%s%u_%u.dat", pattern, x, y);
            dchar **abs_path = &mf->absolute_chunk_paths[y * mf->header.w + x];
            dstr_set(abs_path, directory_path);
            dstr_append_char(abs_path, '/');
            dstr_append(abs_path, *rel_path);
        }
    return 0;
}

int
muta_chunk_file_init(muta_chunk_file_t *mch)
{
    memset(mch, 0, sizeof(muta_chunk_file_t));
    mch->tiles = calloc(NUM_TILES_PER_CHUNK, sizeof(tile_t));
    if (!mch->tiles)
        return 1;
    mch->static_objs = calloc(256, sizeof(stored_static_obj_t));
    if (!mch->static_objs)
    {
        muta_chunk_file_destroy(mch);
        return 2;
    }
    mch->max_static_objs = 256;
    return 0;
}

int
muta_chunk_file_load(muta_chunk_file_t *mch, const char *path)
{
    int     err;
    FILE    *f = fopen(path, "rb");
    if (!f)
        {err = 1; goto fail;}
    /* Name */
    if (fread_uint8_arr(f, (uint8*)mch->header.name, MUTA_CHUNK_FILE_NAME_LEN))
        {err = 2; goto fail;}
    mch->header.name[MUTA_CHUNK_FILE_NAME_LEN] = 0;
    /* Location (in chunks) */
    if (fread_uint32_arr(f, mch->header.location, 2))
        {err = 3; goto fail;}
    /* Number of static objects */
    if (fread_uint32(f, &mch->header.num_static_objs))
        {err = 4; goto fail;}
    /* Padding */
    if (fseek(f, CHUNK_FILE_PADDING, SEEK_CUR))
        {err = 5; goto fail;}
    /* Allocate more static objs if required */
    uint num_objs = mch->header.num_static_objs;
    if (num_objs > mch->max_static_objs)
    {
        stored_static_obj_t *objs = realloc(mch->static_objs,
            num_objs * sizeof(stored_static_obj_t));
        if (!objs) {err = 6; goto fail;}
        mch->static_objs        = objs;
        mch->max_static_objs    = mch->header.num_static_objs;
    }
    /* Tiles */
    if (fread_uint16_arr(f, mch->tiles, NUM_TILES_PER_CHUNK))
        {err = 7; goto fail;}
    /* Static objects */
    stored_static_obj_t *objs = mch->static_objs;
    for (uint32 i = 0; i < num_objs; ++i)
    {
        if (fread_uint32(f, &objs[i].type_id))
            {err = 8; goto fail;}
        if (fread_uint8(f, &objs[i].dir) ||
            fread_uint8(f, &objs[i].x) ||
            fread_uint8(f, &objs[i].y) ||
            fread_uint8(f,  &objs[i].z))
            {err = 9; goto fail;}
        if (objs[i].dir >= NUM_ISODIRS)
            {err = 10; goto fail;}
    }
    fclose(f);
    return 0;
    fail:
        safe_fclose(f);
        memset(mch->tiles, 0, NUM_TILES_PER_CHUNK * sizeof(tile_t));
        mch->header.num_static_objs = 0;
        return err;
}

int
muta_chunk_file_check(muta_chunk_file_t *mch, uint32 num_tile_types)
{
    for (int i = 0; i < MAP_CHUNK_W * MAP_CHUNK_W * MAP_CHUNK_T; ++i)
        if (mch->tiles[i] >= num_tile_types)
            return 1;
    return 0;
}

void
muta_chunk_file_destroy(muta_chunk_file_t *mf)
{
    free(mf->tiles);
    free(mf->static_objs);
    memset(mf, 0, sizeof(muta_chunk_file_t));
}

int
muta_chunk_file_save(muta_chunk_file_t *mch, const char *path)
{
    DEBUG_PRINTFF("attempting to save to path '%s'.\n", path);
    /* +1 slash */
    FILE *f = fopen(path, "wb+");
    if (!f)
        return 1;
    int ret = 0;
    if (fwrite_uint8_arr(f, (uint8*)mch->header.name, MUTA_CHUNK_FILE_NAME_LEN))
        {ret = 2; goto out;}
    if (fwrite_uint32_arr(f, mch->header.location, 2))
        {ret = 3; goto out;}
    if (fwrite_uint32(f, mch->header.num_static_objs))
        {ret = 4; goto out;}
    if (fseek(f, CHUNK_FILE_PADDING, SEEK_CUR))
        {ret = 5; goto out;}
    if (fwrite_uint16_arr(f, mch->tiles, NUM_TILES_PER_CHUNK))
        {ret = 6; goto out;}
    uint32 num_objs             = mch->header.num_static_objs;
    stored_static_obj_t *objs   = mch->static_objs;
    for (uint32 i = 0; i < num_objs; ++i)
    {
        if (objs[i].dir > NUM_ISODIRS)
            {ret = 8; goto out;}
        if (fwrite_uint32(f, objs[i].type_id) ||
            fwrite_uint8(f, objs[i].dir) ||
            fwrite_uint8(f, objs[i].x) ||
            fwrite_uint8(f, objs[i].y) ||
            fwrite_uint8(f, objs[i].z))
            {ret = 9; goto out;}
    }
    out:
        fclose(f);
        return ret;
}

int
muta_chunk_file_set_name(muta_chunk_file_t *mch, const char *name)
{
    if (!str_is_valid_file_name(name))
        return 1;
    int len = (int)strlen(name);
    if (len > MUTA_CHUNK_FILE_NAME_LEN)
        return 2;
    memcpy(mch->header.name, name, len + 1);
    return 0;
}

void
muta_chunk_file_clear_static_objects(muta_chunk_file_t *mch)
    {mch->header.num_static_objs = 0;}

int
muta_chunk_file_push_static_obj(muta_chunk_file_t *mch,
    sobj_type_id_t type_id, uint8 dir, uint8 x, uint8 y, int8 z)
{
    if (x >= MAP_CHUNK_W || y >= MAP_CHUNK_W || z >= MAP_CHUNK_T || z < 0)
        return 1;
    if (mch->header.num_static_objs == mch->max_static_objs)
    {
        uint32 new_max = mch->max_static_objs * 105 / 100;
        stored_static_obj_t *new_objs = realloc(mch->static_objs,
            new_max * sizeof(stored_static_obj_t));
        if (!new_objs) return 2;
        mch->max_static_objs    = new_max;
        mch->static_objs        = new_objs;
    }
    stored_static_obj_t *obj = &mch->static_objs[mch->header.num_static_objs++];
    obj->type_id    = type_id;
    obj->dir        = dir;
    obj->x          = x;
    obj->y          = y;
    obj->z          = z;
    return 0;
}

void
muta_chunk_file_erase_static_obj(muta_chunk_file_t * mch, uint32 index)
{
    uint32 num = mch->header.num_static_objs;
    if (num <= index)
        return;
    if (index != mch->header.num_static_objs - 1)
    {
        uint32 nxt = index + 1;
        memmove(mch->static_objs + index, mch->static_objs + nxt,
            (num - nxt) * sizeof(stored_static_obj_t));
    }
    mch->header.num_static_objs--;
}

int
muta_map_db_load(muta_map_db_t *db, const char *fp)
{
    memset(db, 0, sizeof(muta_map_db_t));
    map_db_load_context_t context =
    {
        .db             = db,
        .file_name      = dstr_create_empty(64),
        .have_filename = 0
    };
    int ret = 0;
    if (parse_def_file(fp, _on_map_db_def, _on_map_db_opt, &context))
    {
        ret = 1;
        goto out;
    }
    if (darr_num(db->entries) && _finalize_map_db_entry(&context))
    {
        ret = 1;
        goto out;
    }
    dstr_free(&context.file_name);
    out:
        if (ret)
            muta_map_db_destroy(db);
        return ret;
}

void
muta_map_db_destroy(muta_map_db_t *db)
{
    uint32 num_entries = darr_num(db->entries);
    for (uint32 i = 0; i < num_entries; ++i)
    {
        dstr_free(&db->entries[i].name);
        dstr_free(&db->entries[i].file_path);
        dstr_free(&db->entries[i].directory_path);
    }
    darr_free(db->entries);
}

uint32
muta_map_db_num_entries(muta_map_db_t *db)
    {return darr_num(db->entries);}

muta_map_db_entry_t *
muta_map_db_get_entry_by_name(muta_map_db_t *db, const char *name)
{
    if (!db->entries)
        return 0;
    uint32 num_entries = darr_num(db->entries);
    for (uint32 i = 0; i < num_entries; ++i)
        if (streq(db->entries[i].name, name))
            return &db->entries[i];
    return 0;
}

muta_map_db_entry_t *
muta_map_db_get_entry_by_id(muta_map_db_t *db, uint32 id)
{
    if (!db->entries)
        return 0;
    uint32 num_entries = darr_num(db->entries);
    for (uint32 i = 0; i < num_entries; ++i)
        if (db->entries[i].id == id)
            return &db->entries[i];
    return 0;
}

static int
_on_map_db_def(parse_def_file_context_t *ctx, const char *def, const char *val)
{
    const char *err_str = 0;
    if (!streq(def, "map"))
    {
        err_str = "bad definition type";
        goto fail;
    }
    long long v = strtoll(val, 0, 10);
    if (v < 0 || v > 0xFFFFFFFF)
    {
        err_str = "bad map ID (must be 0 - 0xFFFFFFFF)";
        goto fail;
    }
    uint32                  id = (uint32)v;
    map_db_load_context_t   *context    = ctx->user_data;
    muta_map_db_t           *db         = context->db;
    if (muta_map_db_get_entry_by_id(db, id))
    {
        err_str = "same map id defined more than once";
        goto fail;
    }
    if (darr_num(db->entries) && _finalize_map_db_entry(ctx->user_data))
    {
        err_str = "_finalize_map_db_entry failed";
        goto fail;
    }
    muta_map_db_entry_t entry =
    {
        .id             = id,
        .name           = 0,
        .file_path      = 0,
        .directory_path = 0
    };
    darr_push(db->entries, entry);
    return 0;
    fail:
        printf("Parsing map db file failed at line %zu: %s.\n", ctx->line,
            err_str);
        return 1;
}

static int
_on_map_db_opt(parse_def_file_context_t *ctx, const char *opt, const char *val)
{
    map_db_load_context_t   *context    = ctx->user_data;
    muta_map_db_t           *db         = context->db;
    muta_map_db_entry_t *entry = &db->entries[darr_num(db->entries) - 1];
    if (streq(opt, "name"))
    {
        if (entry->name)
            goto fail;
        entry->name = dstr_create(val);
    } else
    if (streq(opt, "file_name"))
    {
        if (context->have_filename)
            goto fail;
        dstr_set(&context->file_name, val);
        context->have_filename = 1;
    } else
    if (streq(opt, "directory_path"))
    {
        if (entry->directory_path)
            goto fail;
        entry->directory_path = dstr_create(val);
    } else
        goto fail;
    return 0;
    fail:
        printf("Parsing map db file failed at line %zu.\n", ctx->line);
        return 1;
}

static int
_finalize_map_db_entry(map_db_load_context_t *context)
{
    if (!context->have_filename)
        return 1;
    muta_map_db_t   *db     = context->db;
    muta_map_db_entry_t  *entry  = &db->entries[darr_num(db->entries) - 1];
    if (!entry->name)
        return 1;
    if (!entry->directory_path)
        return 1;
    entry->file_path = dstr_create(entry->directory_path);
    dstr_append_char(&entry->file_path, '/');
    dstr_append(&entry->file_path, context->file_name);
    return 0;
}
