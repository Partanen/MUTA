#ifndef MUTA_COMMON_UTILS_H
#define MUTA_COMMON_UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "ksys.h"
#include "types.h"
#include "compiler.h"
#include "stb/stb_sprintf.h"

typedef const char* cstr;

typedef struct byte_buf_t               byte_buf_t;
typedef struct byte_buf_t               bbuf_t;
typedef struct byte_buf_t               dynamic_bbuf_t;
typedef struct double_cmd_buf_arr_t     double_cmd_buf_arr_t;
typedef struct double_cmd_buf_t         double_cmd_buf_t;
typedef struct double_msg_buf_t         double_msg_buf_t;
typedef int32                           spinlock_t;
typedef struct darr_head_t              darr_head_t;
typedef struct segfit_stack_alloc_t     segfit_stack_alloc_t;
typedef struct segfit_stack_list_t      segfit_stack_list_t;
typedef struct segfit_stack_t           segfit_stack_t;
typedef struct segpool_header_t         segpool_header_t;
typedef struct segpool_t                segpool_t;
typedef struct obj_pool_alloc_t         obj_pool_alloc_t;
typedef struct obj_pool_t               obj_pool_t;
typedef struct blocking_queue_t         blocking_queue_t;
typedef struct event_buf_t              event_buf_t;
typedef struct dynamic_stack_t          dynamic_stack_t;
typedef struct parse_def_file_context_t parse_def_file_context_t;
typedef struct arg_str_t                arg_str_t;
typedef struct config_option_t          config_option_t;

#ifdef _WIN32
    #define SET_BREAKPOINT() DebugBreak()
    #define muta_daemonize() 1
#elif defined __linux__
    #define SET_BREAKPOINT() raise(SIGINT)
    #define muta_daemonize() daemon(1, 0)
#endif

#define FNV_32_SEED     ((uint32)0x811C9DC5)
#define FNV_32_PRIME    ((uint32)0x01000193)
#define FNV_64_SEED     ((uint64)0xcbf29ce484222325)
#define FNV_64_PRIME    ((uint64)1099511628211)

#define HASH_FROM_NUM(value) (value) /* Todo: do away with */

#define DYNAMIC_STACK_ALIGNMENT 8

enum config_option_type
{
    CONFIG_OPTION_TYPE_INT16,
    CONFIG_OPTION_TYPE_UINT16,
    CONFIG_OPTION_TYPE_INT32,
    CONFIG_OPTION_TYPE_UINT32,
    CONFIG_OPTION_TYPE_INT64,
    CONFIG_OPTION_TYPE_UINT64,
    CONFIG_OPTION_TYPE_BOOL32,
    CONFIG_OPTION_TYPE_STR,
    CONFIG_OPTION_TYPE_CUSTOM
};

enum config_parse_error
{
    CINFIG_PARSE_ERROR_OK,
    CONFIG_PARSE_ERROR_PARSING_FAILED,
    CONFIG_PARSE_ERROR_MISSING_OPTIONS
};

enum parse_cfg_file_result
{
    PARSE_CFG_FILE_SUCCESS,
    PARSE_CFG_FILE_FOPEN_FAILED,
    PARSE_CFG_FILE_SYNTAX_ERROR
};

enum parse_def_file_result
{
    PARSE_DEF_FILE_SUCCESS = 0,
    PARSE_DEF_FILE_FOPEN_FAILED,
    PARSE_DEF_FILE_SYNTAX_ERROR
};

struct byte_buf_t
{
    uint8   *mem;
    int     num_bytes;
    int     max_bytes;
};

struct double_cmd_buf_arr_t
{
    void    *mem;
    int     num, max;
};

struct double_cmd_buf_t
{
    double_cmd_buf_arr_t wbuf, rbuf;
    int             item_sz;
    spinlock_t      spinlock;
};
/* A double-buffered array of fixed size items. The read buffer can be read by
 * calling double_cmd_buf_swap() on another thread while the write buffer is
 * being written to. */

struct double_msg_buf_t
{
    dynamic_bbuf_t  wbuf, rbuf;
    mutex_t         mtx;
};

struct darr_head_t
{
    uint32 num;
    uint32 cap;
};
/* The header at the beginning of a dynamic array - see the darr_ macros */

struct segfit_stack_alloc_t
{
    void    *mem;
    uint32  size;
};

struct segfit_stack_list_t
{
    void **res;
    void **free;
};

struct segfit_stack_t
{
    segfit_stack_list_t     *lists;
    segfit_stack_alloc_t    *allocs;
};
/* A stack-like segregated fit allocator context for when temporary allocations
 * of variable sizes are needed temporarily, but they can all be discared at
 * the same time later without individually being freed.
 * See the segfit_ functions. */

struct segpool_header_t
{
    uint32 num_bytes;
};

struct segpool_t
{
    segpool_header_t    ***blocks;
    void                **allocs;
};

struct obj_pool_alloc_t
{
    void    *mem;
    uint32  num;
};

struct obj_pool_t
{
    uint32              item_size;
    obj_pool_alloc_t    *allocs; /* darr */
    void                **free; /* darr */
    uint32              cap;
};

struct blocking_queue_t
{
    mutex_t     mtx;
    cond_var_t  cv;
    void        *items;
    uint32      num;
    uint32      max;
    uint32      item_size;
};

struct event_buf_t
{
    cond_var_t  write_cond_var;
    mutex_t     write_mutex;
    cond_var_t  read_cond_var;
    void        *events;
    uint32      item_size;
    int32       num_events;
    int32       max_events;
};

struct dynamic_stack_t
{
    void    *memory;
    size_t  num;
    size_t  max;
};

struct parse_def_file_context_t
{
    size_t  line;
    void    *user_data;
};

struct arg_str_t
{
    dchar   **argv;
    int     argc;
};

struct config_option_t
{
    const char              *name;
    enum config_option_type type;
    bool32                  optional;
    union
    {
        struct
        {
            int16 *value;
        } i16;
        struct
        {
            uint16 *value;
        } u16;
        struct
        {
            int32 *value;
        } i32;
        struct
        {
            uint32  *value;
        } u32;
        struct
        {
            int64 *value;
        } i64;
        struct
        {
            uint64  *value;
        } u64;
        struct
        {
            bool32 *value;
        } b32;
        struct
        {
            dchar **value;
        } str;
        struct
        {
            /* Return non-zero if bad val */
            int (*callback)(const char *val, void *ret_value); 
            void *value;
        } custom;
    };
};

#ifdef _MUTA_DEBUG
    #define DEBUG_PRINTF(fmt_, ...) printf("[DEBUG] " fmt_, ##__VA_ARGS__)
    #define DEBUG_PRINTFF(fmt_, ...) \
        printf("[DEBUG] %s: " fmt_, __func__, ##__VA_ARGS__)
    #define DEBUG_PUTS(str_) printf("[DEBUG] %s\n", (str_))
    #define IMPLEMENTME() printf("%s: IMPLEMENTME\n", __func__)
    #define FIXME() printf("%s: FIXME, line %d!\n", __func__, __LINE__);

    #ifdef __linux__
        #define muta_assert(cond_) assert((cond_))
    #elif defined(_WIN32)
        #define muta_assert(cond_) \
        if (!(cond_)) \
        { \
            SET_BREAKPOINT(); \
            assert(cond_); \
        }
    #else
        #error "Unsupported platform."
    #endif
#else
    #define DEBUG_PRINTF(str, ...)  ((void)0)
    #define DEBUG_PRINTFF(str, ...) ((void)0)
    #define DEBUG_PUTS(str)         ((void)0)
    #define muta_assert(cond_)      ((void)0)
    #define IMPLEMENTME()           ((void)0)
    #define FIXME()                 ((void)0)
#endif

#define PI 3.14159265358979323846

#define MIN(val, min) ((val) < (min) ? (val) : (min))
/* Return the lower one of two values */
#define MAX(val, max) ((val) > (max) ? (val) : (max))
/* Return the higher one of two values */
#define CLAMP(val, min, max) (MAX((MIN((val), (max))), (min)))
#define ABS(val) ((val) < 0 ? -(val) : (val))
#define CEILF(val) ((val) - (float)(int)(val) != 0.0f ? \
    (val) > 0.0f ? ((float)((int)(val) + 1)) : (float)(int)(val) : \
    val)
#define ROUNDF(val) ((val) >= 0.0f ? \
    ((val) - (float)(int)(val) >= 0.5f ? \
        (float)((int)(val) + 1) : (float)(int)(val)) : \
    ((val) + (float)(-(int)(val)) <= -0.5f ? \
         (float)((int)(val) - 1) : (float)(int)(val)))
/* If remainder is exactly 0.5f, moves away from zero */
#define GET_BITFLAG(val, flag)      (((val) & (flag)) == (flag))
#define SET_BITFLAG_ON(val, flag)   ((val) |= (flag))
#define SET_BITFLAG_OFF(val, flag)  ((val) &= ~(flag))

static inline uint32
next_pow2_uint32(uint32 num)
{
    uint32 ret = num - 1;
    ret |= ret >> 1;
    ret |= ret >> 2;
    ret |= ret >> 4;
    ret |= ret >> 8;
    ret |= ret >> 16;
    ret++;
    return ret;
}

static inline int
safe_fclose(FILE *f) {if (f) return fclose(f); return 0;}

#define BSWAP16(v) ((((v) >> 8) & 0xffu) | (((v) & 0xffu) << 8))
#define BSWAP32(v) \
    ((((v) & 0xff000000u) >> 24) | (((v) & 0x00ff0000u) >>  8) | \
     (((v) & 0x0000ff00u) << 8)  | (((v) & 0x000000ffu) << 24))
#define BSWAP64(v) \
    ((((v) & 0xff00000000000000ull) >> 56) | \
     (((v) & 0x00ff000000000000ull) >> 40) | \
     (((v) & 0x0000ff0000000000ull) >> 24) | \
     (((v) & 0x000000ff00000000ull) >> 8)  | \
     (((v) & 0x00000000ff000000ull) << 8)  | \
     (((v) & 0x0000000000ff0000ull) << 24) | \
     (((v) & 0x000000000000ff00ull) << 40) | \
     (((v) & 0x00000000000000ffull) << 56))
#if (MUTA_ENDIANNESS == MUTA_LIL_ENDIAN)
    #define TO_SYS_ENDIAN16(v) (v)
    #define TO_SYS_ENDIAN32(v) (v)
    #define TO_SYS_ENDIAN64(v) (v)
#else
    #define TO_SYS_ENDIAN16(v) BSWAP16(v)
    #define TO_SYS_ENDIAN32(v) BSWAP32(v)
    #define TO_SYS_ENDIAN64(v) BSWAP64(v)
#endif

#define BBUF_INIT(buf_, mem_, max_) \
    (buf_)->mem        = (uint8*)(mem_); \
    (buf_)->max_bytes  = (max_); \
    (buf_)->num_bytes  = 0; \

#define BBUF_INITIALIZER(mem, max) {(mem), 0, (max)}

#define BBUF_WRITE(buf, type, val) \
    *(type*)((buf)->mem + (buf)->num_bytes) = (val); \
    (buf)->num_bytes += sizeof(type);

#define BBUF_WRITE_BYTES(buf_, mem_, num_) \
    muta_assert(BBUF_FREE_SPACE(buf_) >= (num_)); \
    memcpy((void*)((buf_)->mem + (buf_)->num_bytes), (void*)(mem_), (num_)); \
    (buf_)->num_bytes += num_;

#define BBUF_READ(buf, ret) \
    memcpy((ret), (buf)->mem + (buf)->num_bytes, sizeof(*(ret))); \
    (buf)->num_bytes += sizeof(*(ret));

#define BBUF_CLEAR(buf) ((buf)->num_bytes = 0)
#define BBUF_FREE_SPACE(buf) ((buf)->max_bytes - (buf)->num_bytes)
#define BBUF_CAN_FIT(buf, sz) ((buf)->max_bytes - (buf)->num_bytes <= (sz))
#define BBUF_CUR_PTR(buf) ((uint8*)(buf)->mem + (buf)->num_bytes)
#define BBUF_GET(buf, offset) ((uint8*)buf->mem + offset)

static inline void *
bbuf_reserve(byte_buf_t *buf, int num_bytes);

int
copy_byte_buf(byte_buf_t *dst, byte_buf_t *src);

int
dynamic_bbuf_init(dynamic_bbuf_t *buf, int sz);

void
dynamic_bbuf_destroy(dynamic_bbuf_t *buf);

static inline int
dynamic_bbuf_write_bytes(dynamic_bbuf_t *buf, const void *bytes, int num);

static inline void *
dynamic_bbuf_reserve(dynamic_bbuf_t *buf, int num);

static inline int
dynamic_bbuf_reserve_to_buf(dynamic_bbuf_t *buf, int num, byte_buf_t *ret_buf);

static inline int
_dynamic_bbuf_enlarge(dynamic_bbuf_t *buf, size_t min_size);

static inline int
_dynamic_bbuf_ensure_capacity_for_n_more(dynamic_bbuf_t *buf, size_t n);

#define double_cmd_buf_arr_clear(arr) (arr)->num = 0

int
double_cmd_buf_init(double_cmd_buf_t *q, int initial_num, int item_sz);

int
double_cmd_buf_push(double_cmd_buf_t *q, void *data);

int
double_cmd_buf_push_static(double_cmd_buf_t *q, void *data);

int
double_cmd_buf_push_nondynamic(double_cmd_buf_t *q, void *data);
/* Push without dynamically allocating more space. */

double_cmd_buf_arr_t *
double_cmd_buf_swap(double_cmd_buf_t *q);

void
double_cmd_buf_destroy(double_cmd_buf_t *q);

int
double_msg_buf_init(double_msg_buf_t *buf, int num_initial_bytes);

void
double_msg_buf_destroy(double_msg_buf_t *buf);

byte_buf_t *
double_msg_buf_begin_write(double_msg_buf_t *buf, int num_bytes);
/* If this returns 0, do NOT call double_msg_buf_finalize_write() */

void
double_msg_buf_finalize_write(double_msg_buf_t *buf);

int
double_msg_buf_write(double_msg_buf_t *buf, const void *data, int num_bytes);
/* Returns 0 on success */

byte_buf_t *
double_msg_buf_swap(double_msg_buf_t *buf);

void
double_msg_buf_clear(double_msg_buf_t *buf);

void
spinlock_init(spinlock_t volatile *sl);

#define SPINLOCK_INITIALIZER 0

int
spinlock_lock(spinlock_t volatile *sl);

int
spinlock_unlock(spinlock_t volatile *sl);

uint8 *
load_file_to_buffer(const char *path, size_t *ret_len);
/* Loads in binary format. Must be freed using free(). Option ret_len is
 * optional, and if provided, receives the length of the buffer. */

dchar *
load_text_file_to_dstr(const char *path);

void
str_strip_symbols(char *str, const char *symbols, int num_symbols);

int
str_strip_symbolsn(char *str, const char *symbols, int num_symbols,
    int str_len);
/* Returns the new length of the string */

void
str_strip_undef_symbols(char *str, const char *symbols, int num_symbols);
/* Strip out symbols NOT defined in the 'symbols' parameter */

bool32
str_contains_one_or_more_of_symbols(const char *str, const char *symbols,
    int num_symbols);

bool32
str_contains_one_or_more_of_symbolsn(const char *str, const char *symbols,
    int num_symbols, int str_len);

bool32
str_contains_only_symbols(const char *str, const char *symbols,
    int num_symbols);

bool32
str_contains_only_symbolsn(const char *str, const char *symbols,
    int num_symbols, int str_len);

int
str_insensitive_cmp(const char *str1, const char *str2);

void
str_strip_trailing_spaces(char *str);
/* Remove spaces at the beginning of a string and pad with zeros at the end.
 * Also remvoves tabs and newlines */

bool32
str_is_valid_file_name(const char *str);

bool32
str_is_valid_file_path(const char *str);

bool32
str_is_int(const char *str);

bool32
str_is_float(const char *str);
/* str_is_float()
 * Check whether string can be be represented as a float. To be interpreted as a
 * float, the string must have exactly one dot which is not the first or the
 * last character, and all the other characters must be digits. */


bool32
str_is_ascii(const char *str);

int
str_strip_non_ascii(char *str);

int
str_strip_ctrl_chars(char *str);

void
str_strip_non_numbers(char *str);

int
str_to_bool(const char *str, bool32 *ret_bool);

void
str_to_upper(char *str);

void
str_to_lower(char *str);

char *
str_find(char *str, const char *key);

char *
str_insensitive_find(char *str, const char *key);

bool32
str_contains(char *str, const char *key);

bool32
str_insensitive_contains(char *str, const char *key);

size_t
str_count_chars(const char *str, char c);

void
str_copy_and_null_terminate(char *dst, const char *src, size_t src_len);
/* str_copy_and_null_terminate()
 * Copy and null-terminate a string. dst must be at least src_len + 1 wide. */

bool32
str_ends_with(const char *restrict str, const char *restrict end);
/* If strlen(end) is 0, returns true. */

#define str_to_uint64(str) ((uint64)strtoull((str), 0, 10))
#define str_to_uint32(str) ((uint32)str_to_uint64(str))
static inline bool32 streq(const char *sa, const char *sb);

static inline uint32
fnv_hash32_from_str(const char *str);

static inline uint32
fnv_hash32_from_data(const void *str, size_t len);

static inline uint64
fnv_hash64_from_str(const char *str);

static inline uint64
fnv_hash64_from_data(const void *str, size_t len);

static inline int
highest_bit_uint32(uint32_t n);

dchar *
create_dynamic_str(const char *txt);

dchar *
create_empty_dynamic_str(uint len);

void
free_dynamic_str(const dchar *str);

dchar *
set_dynamic_str(dchar *str, const char *txt);

uint
get_dynamic_str_len(const dchar *str);

uint
get_dynamic_str_cap(const dchar *str);

dchar *
set_dynamic_str_len(dchar *str, uint len);

dchar *
append_to_dynamic_str(dchar *dstr, const char *s);

dchar *
set_dynamic_strvf(dchar *str, const char *fmt, va_list args);

dchar *
set_dynamic_strf(dchar *str, const char *fmt, ...);

dchar *
dstr_create(const char *s);

dchar *
dstr_create_empty(uint32 len);

void
dstr_free(dchar **dstr);

void
dstr_set(dchar **dstr, const char *s);

void
dstr_setf(dchar **dstr, const char *fmt, ...)
CHECK_PRINTF_ARGS(2, 3);

void
dstr_setvf(dchar **dstr, const char *fmt, va_list args);

void
dstr_append(dchar **dstr, const char *s);

void
dstr_append_char(dchar **dstr, char c);

void
dstr_set_len(dchar **dstr, uint32 len);

void
dstr_reserve(dchar **dstr, uint32 len);

#define dstr_len(dstr) get_dynamic_str_len(dstr)
#define dstr_cap(dstr) get_dynamic_str_cap(dstr)

enum parse_cfg_file_result
parse_cfg_file(const char *path,
    void (*callback)(void *ctx, const char *opt, const char *val),
    void *context);
/* Parse an ini-like file consisting of "opt = val" pairs.
 * The callback function will be called for every line in the form of
 * opt = val. If there's no assignment character, val will be 0.  Both, opt and
 * val have any trailing spaces removed. Lines starting with # are ignored.
 * context is an optional pointer to a user-defined structure. */

enum parse_def_file_result
parse_def_file(const char *fp,
    int (*on_def)(parse_def_file_context_t *ctx, const char *def,
        const char *val),
    int (*on_opt)(parse_def_file_context_t *ctx, const char *opt,
        const char *val),
    void *user_data);
/* Parse a .def file consisting of definition beginnings and "opt = val" pairs.
 * A typical entry will look something like:
 *
 * my_entry: xyz
 *     value1 = 5
 *     value2 = abc
 *
 * on_def() is called when a new definition begins. on_opt() is called when a
 * new option is detected. user_data is an optional pointer to a user-defined
 * context - it will be the "user_data" member of parse_def_file_context_t.
 * Callbacks should return non-zero if a format error is detected, which will
 * stop the function, or zero on success. */

enum config_parse_error
parse_cfg_file_with_option_list(const char *path, config_option_t *opts,
    uint32 num_opts);

void
arg_str_init(arg_str_t *arg_str);

void
arg_str_destroy(arg_str_t *arg_str);

void
arg_str_parse(arg_str_t *arg_str, const char *str);

bool32
is_chat_msg_legal(const char *msg);

bool32
is_chat_msg_legaln(const char *msg, int len);

void
strip_illegal_chat_msg_symbols(char *msg);

void
muta_panic(void);

void
muta_panic_print(const char *fmt, ...)
CHECK_PRINTF_ARGS(1, 2);

void
set_muta_panic_callback(void (*callback)(const char *));
/* Set a callback function that will be called instead of exit() when panicking.
 * If muta_panic_print is called, the string is passed on to the callback.
 * Otherwise the string will be null. */

int fwrite_zeros(FILE *f, size_t num);
int fwrite_uint8_arr(FILE *f, uint8 *val, size_t len);
int fwrite_uint16_arr(FILE *f, uint16 *val, size_t len);
int fwrite_uint32_arr(FILE *f, uint32 *val, size_t len);
int fwrite_uint64_arr(FILE *f, uint64 *val, size_t len);
int fwrite_int8(FILE *f, int8 val);
int fwrite_uint8(FILE *f, uint8 val);
int fwrite_int16(FILE *f, int16 val);
int fwrite_uint16(FILE *f, uint16 val);
int fwrite_int32(FILE *f, int32 val);
int fwrite_uint32(FILE *f, uint32 val);
int fwrite_int64(FILE *f, int64 val);
int fwrite_uint64(FILE *f, uint64 val);
int fread_uint8_arr(FILE *f, uint8 *ret, size_t len);
int fread_uint16_arr(FILE *f, uint16 *ret, size_t len);
int fread_uint32_arr(FILE *f, uint32 *ret, size_t len);
int fread_int8(FILE *f, int8 *ret);
int fread_uint8(FILE *f, uint8 *ret);
int fread_int16(FILE *f, int16 *ret);
int fread_uint16(FILE *f, uint16 *ret);
int fread_int32(FILE *f, int32 *ret);
int fread_uint32(FILE *f, uint32 *ret);
int fread_int64(FILE *f, int64 *ret);
int fread_uint64(FILE *f, uint64 *ret);
int fread_uint64_arr(FILE *f, uint64 *ret, size_t len);

/* Alloc functions that will never return 0, unless sz was 0 */
void *emalloc(size_t sz);
void *ecalloc(size_t sz);
void *erealloc(void *p, size_t sz);

/* For dynamic arrays, num is the number of objects, cap is the current maximum
 * capacity before the space must be reallocated.
 * You can use darr_reserve() to initialize a dynamic array like this:
 * int *vector = 0;
 * darr_reserve(vector, 5); */

void *
darr_ensure_growth_by(void *darr, uint32 grow_by, uint32 item_sz);

void
darr_sized_erase(void *darr, uint32 index, uint32 item_sz);

#define _darr_head(darr) \
    ((darr_head_t*)(darr) - 1)

#define _darr_num(darr) \
    (_darr_head(darr)->num)

#define darr_head(darr) \
    ((darr) ? _darr_head(darr) : 0)

#define darr_num(darr) \
    ((darr) ? ((darr_head_t*)(darr) - 1)->num : 0)

#define darr_cap(darr) \
    ((darr) ? ((darr_head_t*)(darr) - 1)->cap : 0)

#define darr_reserve(darr, cap_) \
    ((darr) = darr_ensure_growth_by((darr), \
        darr_num(darr) < (cap_) ? (cap_) - darr_num(darr) : 0, \
        sizeof(*(darr))))

#define darr_free(darr) \
    (free(darr_head(darr)), (darr) = 0)

#define darr_push(darr, val) \
    ((darr) = darr_ensure_growth_by((darr), 1, sizeof(*(darr))), \
    (darr)[darr_head(darr)->num++] = (val))

#define darr_push_empty(darr) \
    ((darr) = darr_ensure_growth_by((darr), 1, sizeof(*(darr))), \
    &(darr)[darr_head(darr)->num++])

#define darr_insert(darr, index, value) \
    ((index) == darr_num(darr) ? darr_push((darr), (value)) : \
    (((darr) = darr_ensure_growth_by((darr), 1, sizeof(*(darr)))), \
    memmove((darr) + (index) + 1, \
        (darr) + (index), (_darr_num(darr) - (index)) * sizeof(*(darr))), \
    (darr)[(index)] = (value), _darr_num(darr)++))

#define darr_clear(darr) \
    ((darr) ? _darr_head(darr)->num = 0 : 0)

#define darr_erase_last(darr) \
    ((darr) ? (_darr_head(darr)->num ? _darr_head(darr)->num-- : 0) : 0)

#define darr_erase(darr, index) \
    (darr_sized_erase(darr, index, sizeof(*(darr))))

#define darr_erase_unordered(darr, index) \
    (index != --_darr_num(darr) ? \
    (void)(darr[index] = darr[_darr_num(darr)]) : (void)0)

/* fixed_pool
 * A type-generic fixed-size pool of items that may be accessed via pointer or
 * index. */

#define fixed_pool(type) \
    struct \
    { \
        type    *all; \
        uint32  *free; \
        uint32  num_free; \
        uint32  max; \
    }

#define fixed_pool_init(fp, max_) \
    ((void)((fp)->all      = ecalloc((max_) * sizeof(*(fp)->all))), \
     (void)((fp)->free     = emalloc((max_) * sizeof(uint32))), \
     (void)((fp)->num_free = (max_)), \
     (void)((fp)->max      = (max_)), \
     (void)(init_fixed_pool_free_slots((fp)->free, (max_))))

#define fixed_pool_destroy(fp) \
    ((void)(free((fp)->all)), (void)(free((fp)->free)), \
    memset((fp), 0, sizeof(*(fp))))

#define fixed_pool_new(fp) \
    ((fp)->num_free ? &(fp)->all[(fp)->free[--(fp)->num_free]] : 0)

#define fixed_pool_free(fp, ptr) \
    ((fp)->free[(fp)->num_free++] = fixed_pool_index((fp), (ptr)))

#define fixed_pool_clear(fp) \
    ((void)memset((fp)->all, 0, (fp)->max * sizeof(*(fp)->all)), \
     (void)init_fixed_pool_free_slots((fp)->free, (fp)->max), \
     (fp)->num_free = (fp)->max)

#define fixed_pool_num_reserved(fp) \
    ((fp)->max - (fp)->num_free)

#define fixed_pool_index(fp, ptr) \
    ((uint32)((ptr) - (fp)->all))

static inline void
init_fixed_pool_free_slots(uint32 *free, uint32 max)
{
    for (uint32 i = 0; i < max; ++i)
        free[i] = i;
}

/* dynamic_pool
 * Similar to fixed pool, but will grow like a dynamic array. Memory
 * addresses may change upon insertion - hence, elements should be accessed via
 * index (an index is returned by the dynamic_pool_new() macro). */

#define dynamic_pool            fixed_pool
#define dynamic_pool_index      fixed_pool_index
#define dynamic_pool_init       fixed_pool_init
#define dynamic_pool_destroy    fixed_pool_destroy
#define dynamic_pool_new(dp) \
    ((dp)->num_free ? &(dp)->all[(dp)->free[--((dp)->num_free)]] : \
    (((dp)->all = dynamic_pool_grow((dp)->all, sizeof(*(dp)->all), \
    &(dp)->free, &(dp)->num_free, &(dp)->max)), \
    &(dp)->all[(dp)->free[--((dp)->num_free)]]))
#define dynamic_pool_free           fixed_pool_free
#define dynamic_pool_clear          fixed_pool_clear
#define dynamic_pool_num_reserved   fixed_pool_num_reserved

void *
dynamic_pool_grow(void *all, size_t type_size, uint32 **free, uint32 *num_free,
    uint32 *max);

void
segfit_stack_init(segfit_stack_t *sfs, uint32 largest_block);

void
segfit_stack_destroy(segfit_stack_t *sfs);

void *
segfit_stack_malloc(segfit_stack_t *sfs, uint32 num_bytes);

void
segfit_stack_clear(segfit_stack_t *sfs);

void
segpool_init(segpool_t *pool);

void
segpool_destroy(segpool_t *pool);

void *
segpool_malloc(segpool_t *pool, uint32 num_bytes);

void
segpool_free(segpool_t *pool, void *mem);

void
obj_pool_init(obj_pool_t *op, uint32 num_items, uint32 item_size);

void
obj_pool_destroy(obj_pool_t *op);

void
obj_pool_clear(obj_pool_t *op);

void *
obj_pool_reserve(obj_pool_t *op);

void *
obj_pool_reserve_static(obj_pool_t *op);
/* Attempt to reserve, but don't allocate more if there are no free slots,
 * returning 0 instead. */

void
obj_pool_free(obj_pool_t *op, void *ptr);

void
blocking_queue_init(blocking_queue_t *q, uint32 num, uint32 item_size);

void
blocking_queue_destroy(blocking_queue_t *q);

void
blocking_queue_push(blocking_queue_t *q, void *item);

void
blocking_queue_push_num(blocking_queue_t *q, void *items, uint32 num);

uint32
blocking_queue_pop_num(blocking_queue_t *q, uint32 num, void *ret_array);

void
blocking_queue_clear(blocking_queue_t *q);

void
event_init(event_buf_t *buf, uint32 item_size, int32 max);

void
event_destroy(event_buf_t *buf);

void
event_clear(event_buf_t *buf);

void
event_push(event_buf_t *buf, void *evs, int32 num);

void
event_push_no_wait(event_buf_t *buf, void *evs, int32 num);

int
event_wait(event_buf_t *buf, void *evs, int32 max, int timeout_ms);
/* event_wait()
 * -1 as timeout means infinite */

void
dynamic_stack_init(dynamic_stack_t *stack, size_t num_initial_bytes);

void
dynamic_stack_destroy(dynamic_stack_t *stack);

void *
dynamic_stack_push(dynamic_stack_t *stack, size_t num_bytes);
/* Aligned by DYNAMIC_STACK_ALIGNMENT  bytes */

void *
dynamic_stack_push_static(dynamic_stack_t *stack, size_t num_bytes);
/* Returns null if not enough memory has already been allocated. */

void
dynamic_stack_pop(dynamic_stack_t *stack, size_t offset);
/* Pop back to the given offset */

void
dynamic_stack_clear(dynamic_stack_t *stack);

size_t
dynamic_stack_offset(dynamic_stack_t *stack, void *memory);

void *
dynamic_stack_at(dynamic_stack_t *stack, size_t offset);
/* This function does no checks to see if the memory is within range,
 * except for debug assertions. */

int
copy_file(const char *src_path, const char *dst_path);

#define pqueue(type) \
    struct {type *all;}

#define pqueue_init(pq, num) \
    ((pq)->all = 0, darr_reserve((pq)->all, (num)))

#define pqueue_destroy(pq) \
    darr_free((pq)->all)

#define pqueue_num(pq) \
    darr_num((pq)->all)

#define pqueue_push(pq, value, compare) \
    if (!darr_num((pq)->all) || \
        compare((pq)->all[_darr_num((pq)->all) - 1], (value)) >= 0) \
        darr_push((pq)->all, (value)); \
    else \
        for (uint32 i = 0; i < _darr_num((pq)->all); ++i) \
            if (compare((pq)->all[i], (value)) <= 0) \
            { \
                darr_insert((pq)->all, i, (value)); \
                break; \
            }

#define pqueue_push_ext(pq, value, compare, ctx) \
    if (!darr_num((pq)->all) || \
        compare((pq)->all[_darr_num((pq)->all) - 1], (value), (ctx)) >= 0) \
        darr_push((pq)->all, (value)); \
    else \
        for (uint32 i = 0; i < _darr_num((pq)->all); ++i) \
            if (compare((pq)->all[i], (value), (ctx)) <= 0) \
            { \
                darr_insert((pq)->all, i, (value)); \
                break; \
            }

#define pqueue_pop(pq) \
    ((pq)->all[--_darr_num((pq)->all)])

#define pqueue_clear(pq) \
    darr_clear((pq)->all)

#define pqueue_erase(pq, val) \
    for (uint32 i = 0; i < _darr_num((pq)->all); ++i) \
        if (!memcmp(&(pq)->all[i], &(val), sizeof(*(pq)->all))) \
        { \
            darr_erase((pq)->all, i); \
            break; \
        }

static inline void *
bbuf_reserve(byte_buf_t *buf, int num_bytes)
{
    if (BBUF_FREE_SPACE(buf) < num_bytes)
        return 0;
    uint8 *ret = &buf->mem[buf->num_bytes];
    buf->num_bytes += num_bytes;
    return (void*)ret;
}

static inline void
bbuf_cut_portion(byte_buf_t *buf, int start, int end)
{
    if (start >= end || end > buf->num_bytes || start < 0)
        return;
    buf->num_bytes -= end - start;
    memmove(buf->mem + start, buf->mem + end, buf->num_bytes - start);
}

#define DYN_BBUF_ENLARGE_IF_NECESSARY(buf, alloc_sz, ret_val_) \
if (BBUF_FREE_SPACE(buf) < (alloc_sz)) \
{ \
    size_t min = buf->max_bytes + num; \
    if (_dynamic_bbuf_enlarge((buf), min) != 0) \
        return (ret_val_); \
}

static inline int
_dynamic_bbuf_enlarge(dynamic_bbuf_t *buf, size_t min)
{
    size_t new_sz = (size_t)buf->max_bytes * 105 / 100; /* Grow by 5 % */
    if (new_sz < min) new_sz = min;
    void *new_mem = realloc(buf->mem, new_sz);
    if (!new_mem) return 1;
    buf->mem        = new_mem;
    buf->max_bytes  = (int)new_sz;
    return 0;
}

static inline int
_dynamic_bbuf_ensure_capacity_for_n_more(dynamic_bbuf_t *buf, size_t n)
{
    if ((size_t)BBUF_FREE_SPACE(buf) < (n))
        return _dynamic_bbuf_enlarge(buf, buf->num_bytes + n);
    return 0;
}

static inline int
dynamic_bbuf_write_bytes(dynamic_bbuf_t *buf, const void *bytes, int num)
{
    DYN_BBUF_ENLARGE_IF_NECESSARY(buf, num, 1);
    uint8 *mem = &buf->mem[buf->num_bytes];
    memcpy(mem, bytes, num);
    buf->num_bytes += num;
    return 0;
}

static inline void *
dynamic_bbuf_reserve(dynamic_bbuf_t *buf, int num)
{
    DYN_BBUF_ENLARGE_IF_NECESSARY(buf, num, 0);
    uint8 *ret = &buf->mem[buf->num_bytes];
    buf->num_bytes += num;
    return (void*)ret;
}

static inline int
dynamic_bbuf_reserve_to_buf(dynamic_bbuf_t *buf, int num, byte_buf_t *ret_buf)
{
    uint8 *mem = (uint8*)dynamic_bbuf_reserve(buf, num);
    if (!mem)
        return 1;
    BBUF_INIT(ret_buf, mem, num);
    return 0;
}

static inline bool32
streq(const char *sa, const char *sb)
{
    uint32 i = 0;
    for (;; ++i)
    {
        if (sa[i] != sb[i])
            return 0;
        if (sa[i] == 0)
            return 1;
    }
}

static inline uint32
fnv_hash32_from_str(const char *str)
{
    uint32 hash = FNV_32_SEED;
    for (const char *c = str; *c; ++c)
        {hash ^= *c; hash *= FNV_32_PRIME;}
    return hash;
}

static inline uint32
fnv_hash32_from_data(const void *str, size_t len)
{
    uint32 hash = FNV_32_SEED;
    for (const char *c = str; c < (const char*)str + len; ++c)
        {hash ^= *c; hash *= FNV_32_PRIME;}
    return hash;
}

static inline uint64
fnv_hash64_from_str(const char *str)
{
    uint64 hash = FNV_64_SEED;
    for (const char *c = str; *c; ++c)
        {hash ^= *c; hash *= FNV_64_PRIME;}
    return hash;
}

static inline uint64
fnv_hash64_from_data(const void *str, size_t len)
{
    uint64 hash = FNV_64_SEED;
    for (const char *c = str; c < (const char*)str + len; ++c)
        {hash ^= *c; hash *= FNV_64_PRIME;}
    return hash;
}

static inline int
highest_bit_uint32(uint32_t n)
{
    int ret = 0;
    for (int i = 31; i >= 0; --i)
    {
        if (!(n & (1 << i)))
            continue;
        ret = i;
        break;
    }
    return ret;
}

#endif /* MUTA_COMMON_UTILS_H */
