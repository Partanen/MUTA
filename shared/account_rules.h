#ifndef MUTA_SHARED_ACCOUNT_RULES
#define MUTA_SHARED_ACCOUNT_RULES

#include <stddef.h>

int
ar_check_player_character_name(const char *name, size_t name_len);
/* ar_check_player_character_name()
   Check if a player character name contains any illegal symbols, is too short,
   or is too long. Return value is 0 if the name is legal. */

#endif /* MUTA_SHARED_ACCOUNT_RULES */
