typedef uint8 svchan_msg_type_t;
#define SVCHAN_MSGTSZ sizeof(svchan_msg_type_t)
#include "sv_common_defs.h"
#include "crypt.h"
#include "rwbits.inl"
#define WRITE_SVCHAN_MSG_TYPE(mem, val) \
    *(svchan_msg_type_t*)(mem) = (svchan_msg_type_t)(val); \
    (mem) = (uint8*)(mem) + sizeof(svchan_msg_type_t);
enum svchan_msg_types
{
    SVCHAN_MSG_PUB_KEY = 0,
    SVCHAN_MSG_STREAM_HEADER,
    SVCHAN_SV_MSG_LOGIN_RESULT,
    SVCHAN_CL_MSG_LOGIN,
    SVCHAN_CL_STILL_HERE,

    NUM_SVCHAN_MSG_TYPES
};

#if NUM_SVCHAN_MSG_TYPES > 255
#   error too many SVCHAN_MSG_TYPES types
#endif

MSG_WRITE_PREP_DEFINITION(svchan_msg, SVCHAN_MSGTSZ, WRITE_SVCHAN_MSG_TYPE);

typedef struct
{
    uint8 key[CRYPTCHAN_PUB_KEY_SZ];
} svchan_msg_pub_key_t;

#define SVCHAN_MSG_PUB_KEY_SZ (CRYPTCHAN_PUB_KEY_SZ)

static inline int
svchan_msg_pub_key_write(byte_buf_t *buf, svchan_msg_pub_key_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVCHAN_MSGTSZ + SVCHAN_MSG_PUB_KEY_SZ);
    if (!mem) return 1;
    WRITE_SVCHAN_MSG_TYPE(mem, SVCHAN_MSG_PUB_KEY);
    WRITE_UINT8_FIXARR(mem, s->key, CRYPTCHAN_PUB_KEY_SZ);

    return 0;
}

static inline int
svchan_msg_pub_key_read(byte_buf_t *buf, svchan_msg_pub_key_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVCHAN_MSG_PUB_KEY_SZ);
    if (!mem) return 1;
    READ_UINT8_FIXARR(mem, s->key, CRYPTCHAN_PUB_KEY_SZ);

    return 0;
}

typedef struct
{
    uint8 header[CRYPTCHAN_STREAM_HEADER_SZ];
} svchan_msg_stream_header_t;

#define SVCHAN_MSG_STREAM_HEADER_SZ (CRYPTCHAN_STREAM_HEADER_SZ)

static inline int
svchan_msg_stream_header_write(byte_buf_t *buf, svchan_msg_stream_header_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVCHAN_MSGTSZ + SVCHAN_MSG_STREAM_HEADER_SZ);
    if (!mem) return 1;
    WRITE_SVCHAN_MSG_TYPE(mem, SVCHAN_MSG_STREAM_HEADER);
    WRITE_UINT8_FIXARR(mem, s->header, CRYPTCHAN_STREAM_HEADER_SZ);

    return 0;
}

static inline int
svchan_msg_stream_header_read(byte_buf_t *buf, svchan_msg_stream_header_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVCHAN_MSG_STREAM_HEADER_SZ);
    if (!mem) return 1;
    READ_UINT8_FIXARR(mem, s->header, CRYPTCHAN_STREAM_HEADER_SZ);

    return 0;
}

typedef struct
{
    uint8 result;
} svchan_sv_msg_login_result_t;

#define SVCHAN_SV_MSG_LOGIN_RESULT_SZ (sizeof(uint8))

static inline int
svchan_sv_msg_login_result_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    svchan_sv_msg_login_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVCHAN_MSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + SVCHAN_SV_MSG_LOGIN_RESULT_SZ);
    if (!mem) return 1;
    WRITE_SVCHAN_MSG_TYPE(mem, SVCHAN_SV_MSG_LOGIN_RESULT);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->result);

    return cryptchan_encrypt(cc, dst, src, SVCHAN_SV_MSG_LOGIN_RESULT_SZ);
}

static inline int
svchan_sv_msg_login_result_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    svchan_sv_msg_login_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        SVCHAN_SV_MSG_LOGIN_RESULT_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        SVCHAN_SV_MSG_LOGIN_RESULT_SZ))
        return -1;
    READ_UINT8(mem, &s->result);

    return 0;
}

typedef struct
{
    uint8 name_len;
    uint8 password_len;
    const char *name;
    const char *password;
} svchan_cl_msg_login_t;

#define SVCHAN_CL_MSG_LOGIN_SZ (sizeof(uint8) + sizeof(uint8))

#define SVCHAN_CL_MSG_LOGIN_COMPUTE_SZ(name_len, password_len) \
    (SVCHAN_CL_MSG_LOGIN_SZ + (name_len) + (password_len))

#define SVCHAN_CL_MSG_LOGIN_MAX_SZ \
    (SVCHAN_CL_MSG_LOGIN_COMPUTE_SZ(MAX_SVCHAN_NAME_LEN, MAX_SVCHAN_PASSWORD_LEN))

static inline int
svchan_cl_msg_login_write_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    svchan_cl_msg_login_t *s)
{
    int sz = SVCHAN_CL_MSG_LOGIN_COMPUTE_SZ(s->name_len, s->password_len);
    if (sz > SVCHAN_CL_MSG_LOGIN_MAX_SZ)
        return -1;

    uint8 *mem = prep_svchan_msg_write_var_encrypted(buf, SVCHAN_CL_MSG_LOGIN, sz);
    if (!mem) return 1;

    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->name_len);
    WRITE_UINT8(mem, s->password_len);
    WRITE_STR(mem, s->name, s->name_len);
    WRITE_STR(mem, s->password, s->password_len);

    return cryptchan_encrypt(cc, dst, src, sz);
}

static inline int
svchan_cl_msg_login_read_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    svchan_cl_msg_login_t *s)
{
    msg_sz_t sz;
    int r = prep_msg_read_var_encrypted(buf, SVCHAN_CL_MSG_LOGIN_MAX_SZ, &sz);
    if (r) return r;

    uint8 *mem = bbuf_reserve(buf, sz);

    if (cryptchan_decrypt(cc, mem, mem, sz) < 0)
        return -1;

    READ_UINT8(mem, &s->name_len);
    READ_UINT8(mem, &s->password_len);

    if (SVCHAN_CL_MSG_LOGIN_COMPUTE_SZ(s->name_len, s->password_len) >
        SVCHAN_CL_MSG_LOGIN_MAX_SZ)
        return -1;

    if (sz < SVCHAN_CL_MSG_LOGIN_COMPUTE_SZ(s->name_len, s->password_len)) return -1;

    READ_VARCHAR(mem, s->name, s->name_len);
    READ_VARCHAR(mem, s->password, s->password_len);

    return 0;
}

static inline int
svchan_cl_still_here_write(byte_buf_t *buf)
{
    uint8 *mem = bbuf_reserve(buf, SVCHAN_MSGTSZ);
    if (!mem) return 1;
    WRITE_SVCHAN_MSG_TYPE(mem, SVCHAN_CL_STILL_HERE);

    return 0;
}

