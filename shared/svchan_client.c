#include "svchan_client.h"
#include "svchan_packets.h"
#include "sv_time.h"

#define STILL_HERE_FREQUENCY 2000

enum svchan_client_event
{
    SVCHAN_CLIENT_EVENT_CONNECTION_FAILED,
    SVCHAN_CLIENT_EVENT_CONNECTED,
    SVCHAN_CLIENT_EVENT_READ
};

enum svchan_client_state
{
    CLIENT_DISCONNECTED,
    CLIENT_CONNECTING,
    CLIENT_AWAIT_PUB_KEY,
    CLIENT_AWAIT_STREAM_HEADER,
    CLIENT_AWAIT_LOGIN_RESULT,
    CLIENT_AUTHED
};

static thread_ret_t
_main(void *args);

static void
_disconnect_internal(svchan_client_t *client);

static int
_flush_for_space(svchan_client_t *client, uint32 num_bytes);

static bbuf_t
_send_msg_internal_protocol(svchan_client_t *client, uint32 num_bytes);

static bbuf_t
_send_msg_internal_protocol_var_encrypted(svchan_client_t *client,
    uint32 num_bytes);

static int
_read_packet_authed(svchan_client_t *client);

static int
_read_packet_unauthed(svchan_client_t *client);

static int
_read_socket_async(svchan_client_t *client);

static void
_read_new_data(svchan_client_t *client, uint8 *buf, uint32 num_bytes);

static int
_handle_svchan_msg_pub_key(svchan_client_t *client, svchan_msg_pub_key_t *s);

static int
_handle_svchan_msg_stream_header(svchan_client_t *client,
    svchan_msg_stream_header_t *s);

static int
_handle_svchan_sv_msg_login_result(svchan_client_t *client,
    svchan_sv_msg_login_result_t *s);

int
svchan_client_init(svchan_client_t *client,
    svchan_client_callbacks_t *callbacks, void *user_data, uint32 in_buf_size,
    uint32 out_buf_size, bool32 async)
{
    muta_assert(callbacks->post_event);
    muta_assert(callbacks->on_authed);
    muta_assert(callbacks->on_read_packet);
    muta_assert(callbacks->on_disconnect);
    muta_assert(callbacks->on_connection_failed);
    muta_assert(callbacks->send_still_here_msg);
    if (!async)
        muta_assert(callbacks->on_connected);
    muta_assert(in_buf_size);
    muta_assert(out_buf_size);
    memset(client, 0, sizeof(*client));
    if (thread_init(&client->thread))
        return 1;
    segpool_init(&client->segpool);
    mutex_init(&client->segpool_mutex);
    client->in_buf.memory       = emalloc(in_buf_size);
    client->in_buf.max          = in_buf_size;
    client->out_buf.memory      = emalloc(out_buf_size);
    client->out_buf.max         = out_buf_size;
    client->callbacks           = *callbacks;
    client->user_data           = user_data;
    client->last_sent_something = get_program_ticks_ms();
    client->state               = CLIENT_DISCONNECTED;
    client->async               = async;
    return 0;
}

void
svchan_client_destroy(svchan_client_t *client)
{
}

int
svchan_client_connect(svchan_client_t *client, addr_t *address,
    const char *name, const char *password)
{
    muta_assert(client->state == CLIENT_DISCONNECTED);
    muta_assert(name);
    muta_assert(password);
    if ((client->socket = net_tcp_ipv4_sock()) == KSYS_INVALID_SOCKET)
        return 1;
    if (make_socket_non_block(client->socket))
    {
        close_socket(client->socket);
        return 2;
    }
    client->in_buf.num  = 0;
    client->out_buf.num = 0;
    client->address     = *address;
    client->running     = 1;
    client->out_buf.num = 0;
    client->in_buf.num  = 0;
    dstr_set(&client->name, name);
    dstr_set(&client->password, password);
    if (thread_create(&client->thread, _main, client))
    {
        close_socket(client->socket);
        client->running = 0;
        return 3;
    }
    client->state = CLIENT_CONNECTING;
    DEBUG_PRINTFF("Attempting to connect to %d.%d.%d.%d on port %u.\n",
        ADDR_IP_TO_PRINTF_ARGS(&client->address), client->address.port);
    return 0;
}

void
svchan_client_disconnect(svchan_client_t *client)
{
    _disconnect_internal(client);
    client->callbacks.on_disconnect(client);
    DEBUG_PRINTFF("Disconnected!\n");
}

bool32
svchan_client_is_connecting(svchan_client_t *client)
{
    return client->state != CLIENT_DISCONNECTED &&
        client->state != CLIENT_AUTHED;
}

bool32
svchan_client_is_connected(svchan_client_t *client)
    {return client->state == CLIENT_AUTHED;}

void
svchan_client_handle_event(svchan_client_event_t *event)
{
    switch (event->type)
    {
    case SVCHAN_CLIENT_EVENT_CONNECTION_FAILED:
    {
        muta_assert(event->client->state != CLIENT_DISCONNECTED);
        muta_assert(event->client->state != CLIENT_AUTHED);
        _disconnect_internal(event->client);
        event->client->callbacks.on_connection_failed(event->client,
            SVCHAN_CLIENT_ERROR_CONNECTION_REFUSED);
    }
        break;
    case SVCHAN_CLIENT_EVENT_CONNECTED:
    {
        if (event->client->state == CLIENT_DISCONNECTED)
        {
            DEBUG_PRINTFF("Disconnected before connected event fired(?)\n");
            break;
        }
        if (!event->client->async)
            thread_join(&event->client->thread);
        event->client->state = CLIENT_AWAIT_PUB_KEY;
        svchan_msg_pub_key_t key_msg;
        if (cryptchan_init(&event->client->cryptchan, key_msg.key))
            goto disconnect;
        bbuf_t bb = _send_msg_internal_protocol(event->client,
            SVCHAN_MSG_PUB_KEY_SZ);
        if (!bb.max_bytes)
            goto disconnect;
        svchan_msg_pub_key_write(&bb, &key_msg);
        if (event->client->callbacks.on_connected)
            event->client->callbacks.on_connected(event->client);
        break;
        disconnect:
        {
            DEBUG_PRINTFF("SVCHAN_CLIENT_EVENT_CONNECTED: disconnecting!\n");
            svchan_client_disconnect(event->client);
        }
    }
        break;
    case SVCHAN_CLIENT_EVENT_READ:
    {
        if (event->client->state == CLIENT_DISCONNECTED)
        {
            if (event->read.num_bytes > 0)
                goto free_memory;
            break;
        }
        if (event->read.num_bytes <= 0)
        {
            DEBUG_PRINTFF("SVCHAN_CLIENT_EVENT_READ: disconnecting, num_bytes "
                "was %d\n", event->read.num_bytes);
            svchan_client_disconnect(event->client);
            break;
        }
        _read_new_data(event->client, event->read.memory,
            (uint32)event->read.num_bytes);
        free_memory:
            mutex_lock(&event->client->segpool_mutex);
            segpool_free(&event->client->segpool, event->read.memory);
            mutex_unlock(&event->client->segpool_mutex);

    }
        break;
    default:
        muta_assert(0);
    }
}

void
svchan_client_flush(svchan_client_t *client)
{
    if (client->state == CLIENT_DISCONNECTED)
        return;
    uint64  time_now    = get_program_ticks_ms();
    int     num         = client->out_buf.num;
    if (!num)
    {
        if (client->state != CLIENT_AUTHED)
            return;
        if (time_now - client->last_sent_something < STILL_HERE_FREQUENCY &&
            time_now >= client->last_sent_something) /* Check for overflow */
            return;
        client->callbacks.send_still_here_msg(client);
        if (!svchan_client_is_connected(client))
            return;
        num = client->out_buf.num;
    }
    int num_sent = 0;
    while (num_sent < num)
    {
        int num_to_send = MIN(MUTA_MTU, num);
        if (net_send_all(client->socket, client->out_buf.memory + num_sent,
            num_to_send) <= 0)
        {
            DEBUG_PRINTFF("failed!\n");
            svchan_client_disconnect(client);
            return;
        }
        num_sent += num_to_send;
    }
    client->out_buf.num         = 0;
    client->last_sent_something = time_now;
    return;
}

bbuf_t
svchan_client_send(svchan_client_t *client, uint32 num_bytes)
{
    muta_assert(client->state == CLIENT_AUTHED);
    bbuf_t ret = {0};
    if (_flush_for_space(client, num_bytes))
    {
        DEBUG_PRINTFF("Failed!\n");
        svchan_client_disconnect(client);
        return ret;
    }
    BBUF_INIT(&ret, client->out_buf.memory + client->out_buf.num, num_bytes);
    client->out_buf.num += num_bytes;
    return ret;
}

bbuf_t
svchan_client_send_const_encrypted(svchan_client_t *client, uint32 num_bytes)
{
    return svchan_client_send(client, CRYPT_MSG_ADDITIONAL_BYTES + num_bytes);
}

bbuf_t
svchan_client_send_var_encrypted(svchan_client_t *client, uint32 num_bytes)
{
    return svchan_client_send(client,
        sizeof(msg_sz_t) + CRYPT_MSG_ADDITIONAL_BYTES + num_bytes);
}

const char *
svchan_client_error_str(enum svchan_client_connection_error error)
{
    switch (error)
    {
    case SVCHAN_CLIENT_ERROR_CONNECTION_REFUSED:
        return "connection refused";
    case SVCHAN_CLIENT_ERROR_SERVER_CLOSED_CONNECTION:
        return "server closed connection";
    case SVCHAN_CLIENT_ERROR_BAD_USERNAME_OR_PASSWORD:
        return "bad username or password";
    }
    muta_assert(0);
    return 0;
}

void
svchan_client_read(svchan_client_t *client)
    {_read_socket_async(client);}

void
svchan_client_read_and_handle(svchan_client_t *client)
{
    uint8   buf[MUTA_MTU];
    int     num_bytes = net_recv(client->socket, buf, sizeof(buf));
    if (num_bytes < 0)
    {
        DEBUG_PRINTFF("net_rect() returned < 0.\n");
        return;
    }
    if (!num_bytes)
    {
        svchan_client_disconnect(client);
        return;
    }
    _read_new_data(client, buf, num_bytes);
}

static thread_ret_t
_main(void *args)
{
    /*-- Connect to server --*/
    svchan_client_t         *client = args;
    fd_set                  socket_set;
    struct timeval          wait_tv;
    svchan_client_event_t   event;
    net_connect(client->socket, client->address);
    for (;;)
    {
        if (!interlocked_compare_exchange_int32(&client->running, -1, -1))
            return 0;
        FD_ZERO(&socket_set);
        FD_SET(client->socket, &socket_set);
        wait_tv.tv_sec  = 0;
        wait_tv.tv_usec = 500000;
        select((int)client->socket + 1, 0, &socket_set, 0, &wait_tv);
        if (!interlocked_compare_exchange_int32(&client->running, -1, -1))
            return 0;
        if (!FD_ISSET(client->socket, &socket_set))
            continue;
        int         result;
        socklen_t   result_len = sizeof(result);
        if (getsockopt(client->socket, SOL_SOCKET, SO_ERROR, (void*)&result,
            &result_len) < 0)
        {
            event.type      = SVCHAN_CLIENT_EVENT_CONNECTION_FAILED;
            event.client    = client;
            client->callbacks.post_event(&event);
            return 0;
        }
        if (result)
        {
            if (result == NET_SOCKET_CONNECT_IN_PROGRESS)
                continue;
            sleep_ms(500);
            event.type      = SVCHAN_CLIENT_EVENT_CONNECTION_FAILED;
            event.client    = client;
            client->callbacks.post_event(&event);
            return 0;
        }
        break; /* Success! */
    }
    DEBUG_PRINTFF("svchan_client connected - beginning authentication...\n");
    event.type      = SVCHAN_CLIENT_EVENT_CONNECTED;
    event.client    = client;
    client->callbacks.post_event(&event);
    if (!client->async)
        return 0;
    /*-- Receive messages from the server if in async mode --*/
    for (;;)
    {
        if (!interlocked_compare_exchange_int32(&client->running, -1, -1))
            return 0;
        FD_ZERO(&socket_set);
        FD_SET(client->socket, &socket_set);
        wait_tv.tv_sec  = 0;
        wait_tv.tv_usec = 500000;
        select((int)client->socket + 1, &socket_set, 0, 0, &wait_tv);
        if (!interlocked_compare_exchange_int32(&client->running, -1, -1))
            return 0;
        if (!FD_ISSET(client->socket, &socket_set))
            continue;
        int num_bytes = _read_socket_async(client);
        if (num_bytes <= 0)
            return 0;
    }
    return 0;
}

static void
_disconnect_internal(svchan_client_t *client)
{
    if (client->state == CLIENT_DISCONNECTED)
        return;
    interlocked_decrement_int32(&client->running);
    net_close_blocking_sock(client->socket);
    if (!client->async || (client->async && client->state == CLIENT_CONNECTING))
        thread_join(&client->thread);
    client->state = CLIENT_DISCONNECTED;
}

static int
_flush_for_space(svchan_client_t *client, uint32 num_bytes)
{
    int num_sent = 0;
    while (client->out_buf.max - (client->out_buf.num - num_sent) < num_bytes)
    {
        int num_to_send = MIN(MUTA_MTU, client->out_buf.num - num_sent);
        if (net_send_all(client->socket, client->out_buf.memory + num_sent,
            num_to_send) <= 0)
            return 1;
        num_sent += num_to_send;
    }
    memmove(client->out_buf.memory, client->out_buf.memory + num_sent,
        client->out_buf.num - num_sent);
    client->out_buf.num -= num_sent;
    return 0;
}

static bbuf_t
_send_msg_internal_protocol(svchan_client_t *client, uint32 num_bytes)
{
    bbuf_t ret = {0};
    num_bytes += SVCHAN_MSGTSZ;
    if (_flush_for_space(client, num_bytes))
        return ret;
    BBUF_INIT(&ret, client->out_buf.memory + client->out_buf.num, num_bytes);
    client->out_buf.num += num_bytes;
    return ret;
}

static bbuf_t
_send_msg_internal_protocol_var_encrypted(svchan_client_t *client,
    uint32 num_bytes)
{
    return _send_msg_internal_protocol(client,
        sizeof(msg_sz_t) + CRYPT_MSG_ADDITIONAL_BYTES + num_bytes);
}

static int
_read_packet_authed(svchan_client_t *client)
{
    int num_bytes = client->callbacks.on_read_packet(client,
        client->in_buf.memory, client->in_buf.num);
    if (num_bytes < 0)
        return 1;
    memmove(client->in_buf.memory,
        client->in_buf.memory + client->in_buf.num - num_bytes, num_bytes);
    client->in_buf.num = num_bytes;
    return 0;
}

static int
_read_packet_unauthed(svchan_client_t *client)
{
    bbuf_t bb = BBUF_INITIALIZER(client->in_buf.memory, client->in_buf.num);
    svchan_msg_type_t   msg_type;
    int                 dc          = 0;
    int                 incomplete  = 0;
    while (BBUF_FREE_SPACE(&bb) >= SVCHAN_MSGTSZ)
    {
        BBUF_READ(&bb, &msg_type);
        switch (msg_type)
        {
        case SVCHAN_MSG_PUB_KEY:
        {
            DEBUG_PUTS("SVCHAN_MSG_PUB_KEY");
            svchan_msg_pub_key_t s;
            incomplete = svchan_msg_pub_key_read(&bb, &s);
            if (!incomplete)
                dc = _handle_svchan_msg_pub_key(client, &s);
        }
            break;
        case SVCHAN_MSG_STREAM_HEADER:
        {
            DEBUG_PUTS("SVCHAN_MSG_STREAM_HEADER");
            svchan_msg_stream_header_t s;
            incomplete = svchan_msg_stream_header_read(&bb, &s);
            if (!incomplete)
                dc = _handle_svchan_msg_stream_header(client, &s);
        }
            break;
        case SVCHAN_SV_MSG_LOGIN_RESULT:
        {
            DEBUG_PUTS("SVCHAN_SV_MSG_LOGIN_RESULT");
            svchan_sv_msg_login_result_t s;
            incomplete = svchan_sv_msg_login_result_read_const_encrypted(&bb,
                &client->cryptchan, &s);
            if (!incomplete)
            {
                dc = _handle_svchan_sv_msg_login_result(client, &s);
                if (!dc && client->state == CLIENT_AUTHED)
                    goto out;
            }
        }
            break;
        default:
            dc = 1;
            break;
        }
        if (dc || incomplete < 0)
        {
            DEBUG_PRINTFF("bad msg: msg_type %u, dc %d, incomplete %d.\n", msg_type,
                dc, incomplete);
            return 1;
        }
        if (incomplete)
        {
            bb.num_bytes -= SVCHAN_MSGTSZ;
            break;
        }
    }
    out:;
        uint32 free_space = BBUF_FREE_SPACE(&bb);
        memmove(client->in_buf.memory,
            client->in_buf.memory + client->in_buf.num - free_space, free_space);
        client->in_buf.num = free_space;
        return 0;
}

static int
_read_socket_async(svchan_client_t *client)
{
    uint8   buf[MUTA_MTU];
    int     num_bytes = net_recv(client->socket, buf, sizeof(buf));
    svchan_client_event_t event;
    event.type              = SVCHAN_CLIENT_EVENT_READ;
    event.client            = client;
    event.read.num_bytes    = num_bytes;
    if (num_bytes > 0)
    {
        mutex_lock(&client->segpool_mutex);
        event.read.memory = segpool_malloc(&client->segpool, num_bytes);
        mutex_unlock(&client->segpool_mutex);
        memcpy(event.read.memory, buf, num_bytes);
    }
    client->callbacks.post_event(&event);
    return num_bytes;
}

static void
_read_new_data(svchan_client_t *client, uint8 *buf, uint32 num_bytes)
{
    uint32 num_copied = 0;
    while (num_copied < num_bytes)
    {
        uint32 num_to_copy = MIN(
            client->in_buf.max - client->in_buf.num,
            num_bytes - num_copied);
        memcpy(client->in_buf.memory + client->in_buf.num, buf + num_copied,
            num_to_copy);
        client->in_buf.num += num_to_copy;
        num_copied += num_to_copy;
        int r;
        if (client->state == CLIENT_AUTHED)
            r = _read_packet_authed(client);
        else
        {
            r = _read_packet_unauthed(client);
            if (!r && client->state == CLIENT_AUTHED && client->in_buf.num)
                r = _read_packet_authed(client);
        }
        if (!r)
            continue;
        if (client->state == CLIENT_AUTHED)
        {
            /*-- Authed already: call normal disconnect. --*/
            DEBUG_PRINTFF("SVCHAN_CLIENT_EVENT_READ: bad packet, "
                "disconnecting!\n");
            svchan_client_disconnect(client);
        } else
        {
            /*-- Unauthed; call on_connection_failed(). --*/
            int reason = SVCHAN_CLIENT_ERROR_SERVER_CLOSED_CONNECTION;
            if (client->state == CLIENT_AWAIT_LOGIN_RESULT)
                reason = SVCHAN_CLIENT_ERROR_BAD_USERNAME_OR_PASSWORD;
            client->callbacks.on_connection_failed(client, reason);
        }
        return;
    }
}

static int
_handle_svchan_msg_pub_key(svchan_client_t *client, svchan_msg_pub_key_t *s)
{
    if (client->state != CLIENT_AWAIT_PUB_KEY)
        return 1;
    svchan_msg_stream_header_t header_msg;
    if (cryptchan_cl_store_pub_key(&client->cryptchan, s->key,
        header_msg.header))
        return 2;
    bbuf_t bb = _send_msg_internal_protocol(client,
        SVCHAN_MSG_STREAM_HEADER_SZ);
    if (!bb.max_bytes)
        return 3;
    svchan_msg_stream_header_write(&bb, &header_msg);
    client->state = CLIENT_AWAIT_STREAM_HEADER;
    return 0;
}

static int
_handle_svchan_msg_stream_header(svchan_client_t *client,
    svchan_msg_stream_header_t *s)
{
    if (client->state != CLIENT_AWAIT_STREAM_HEADER)
        return 1;
    if (cryptchan_store_stream_header(&client->cryptchan, s->header))
        return 2;
    if (!cryptchan_is_encrypted(&client->cryptchan))
        return 3;
    svchan_cl_msg_login_t login_msg;
    login_msg.name          = client->name;
    login_msg.name_len      = dstr_len(client->name);
    login_msg.password      = client->password;
    login_msg.password_len  = dstr_len(client->password);
    bbuf_t bb = _send_msg_internal_protocol_var_encrypted(client,
        SVCHAN_CL_MSG_LOGIN_COMPUTE_SZ(login_msg.name_len,
            login_msg.password_len));
    if (!bb.max_bytes)
        return 4;
    int r = svchan_cl_msg_login_write_var_encrypted(&bb, &client->cryptchan,
        &login_msg);
    muta_assert(!r);
    client->state = CLIENT_AWAIT_LOGIN_RESULT;
    return 0;
}

static int
_handle_svchan_sv_msg_login_result(svchan_client_t *client,
    svchan_sv_msg_login_result_t *s)
{
    if (client->state != CLIENT_AWAIT_LOGIN_RESULT)
        return 1;
    if (s->result)
        return 2;
    client->state = CLIENT_AUTHED;
    DEBUG_PRINTFF("Successfully authenticated with server!\n");
    client->callbacks.on_authed(client);
    return 0;
}
