/* wdb_connector.h
 * API for the world database.
 * Unless stated otherwise, functions should only be called from a single
 * thread. */

#ifndef MUTA_SHARED_WORLD_DB_H
#define MUTA_SHARED_WORLD_DB_H

#define WDBC_INVALID_QUERY_ID 0xFFFFFFFF

#include "types.h"
#include "common_defs.h"

/* Forward declaration(s) */
typedef struct log_t    log_t;

/* Types defined here */
typedef uint32                              wdbc_query_id_t;
typedef struct wdbc_player_character_t      wdbc_player_character_t;
typedef struct wdbc_dynamic_object_t        wdbc_dynamic_object_t;
typedef struct wdbc_dynamic_object_in_db_t  wdbc_dynamic_object_in_db_t;
typedef struct wdbc_svchan_client_event_t   wdbc_svchan_client_event_t;
typedef struct wdbc_completed_query_player_character_list_event_t
    wdbc_completed_query_player_character_list_event_t;
typedef struct wdbc_completed_query_player_character_t
    wdbc_completed_query_player_character_t;
typedef struct wdbc_completed_query_create_player_character_t
    wdbc_completed_query_create_player_character_t;
typedef struct wdbc_completed_query_create_dynamic_object_in_player_container_t
    wdbc_completed_query_create_dynamic_object_in_player_container_t;
typedef struct
    wdbc_completed_query_save_player_character_position_and_direction_t
    wdbc_completed_query_save_player_character_position_and_direction_t;
typedef struct
    wdbc_completed_query_save_dynamic_object_position_in_player_inventory_t
    wdbc_completed_query_save_dynamic_object_position_in_player_inventory_t;
typedef struct
    wdbc_completed_query_remove_dynamic_object_from_player_container_t
    wdbc_completed_query_remove_dynamic_object_from_player_container_t;
typedef struct wdbc_event_t                 wdbc_event_t;

enum wdbc_event
{
    WDBC_EVENT_CONNECTED,
    WDBC_EVENT_FAILED_TO_CONNECT,
    WDBC_EVENT_DISCONNECTED,
    WDBC_EVENT_COMPLETED_QUERY_PLAYER_CHARACTER_LIST,
    WDBC_EVENT_COMPLETED_QUERY_PLAYER_CHARACTER,
    WDBC_EVENT_COMPLETED_QUERY_CREATE_PLAYER_CHARACTER,
    WDBC_EVENT_COMPLETED_QUERY_CREATE_DYNAMIC_OBJECT_IN_PLAYER_CONTAINER,
    WDBC_EVENT_COMPLETED_QUERY_SAVE_PLAYER_CHARACTER_POSITION_AND_DIRECTION,
    WDBC_EVENT_COMPLETED_QUERY_SAVE_DYNAMIC_OBJECT_POSITION_IN_PLAYER_INVENTORY,
    WDBC_EVENT_COMPLETED_QUERY_REMOVE_DYNAMIC_OBJECT_FROM_PLAYER_CONTAINER
};

enum wdbc_dynamic_object_owner_type
{
    WDBC_DYNAMIC_OBJECT_OWNER_PLAYER_CONTAINER,
    WDBC_DYNAMIC_OBJECT_OWNER_PLAYER_EQUIPPED
};

struct wdbc_player_character_t
{
    player_db_guid_t        id;
    char                    name[MAX_CHARACTER_NAME_LEN + 1];
    uint8                   race;
    uint8                   sex;
    uint8                   direction;
    instance_runtime_id_t   instance_id;
    int32                   x;
    int32                   y;
    uint8                   z;
};

struct wdbc_dynamic_object_t
{
    dobj_type_id_t                      type_id;
    enum wdbc_dynamic_object_owner_type owner_type;
    union
    {
        struct
        {
            player_db_guid_t    player_db_id;
            equipment_slot_id_t equipment_slot;
            uint8               x;
            uint8               y;
        } player_container;
        struct
        {
            player_db_guid_t    player_db_id;
            equipment_slot_id_t equipment_slot;
        } player_equipped;
    } owner;
};

struct wdbc_dynamic_object_in_db_t
{
    uuid16_t                id;
    wdbc_dynamic_object_t   data;
};

struct wdbc_completed_query_player_character_list_event_t
{
    wdbc_query_id_t         query_id;
    int                     error;
    wdbc_player_character_t characters[MAX_CHARACTERS_PER_ACC];
    uint32                  num_characters;
};

struct wdbc_completed_query_player_character_t
{
    wdbc_query_id_t             query_id;
    int                         error;
    wdbc_player_character_t     character;
    wdbc_dynamic_object_in_db_t *items;
    uint32                      num_items;
};

struct wdbc_completed_query_create_player_character_t
{
    wdbc_query_id_t         query_id;
    int                     error;
    wdbc_player_character_t character;
};

struct wdbc_completed_query_create_dynamic_object_in_player_container_t
{
    wdbc_query_id_t             query_id;
    int                         error;
    wdbc_dynamic_object_in_db_t dobj;
};

struct wdbc_completed_query_save_player_character_position_and_direction_t
{
    wdbc_query_id_t     query_id;
    int                 error;
    uint32              instance_id;
    uint8               direction;
    int32               x;
    int32               y;
    uint8               z;
};

struct wdbc_completed_query_save_dynamic_object_position_in_player_inventory_t
{
    wdbc_query_id_t     query_id;
    int                 error;
    equipment_slot_id_t equipment_slot;
    uint8               x;
    uint8               y;
};

struct wdbc_completed_query_remove_dynamic_object_from_player_container_t
{
    wdbc_query_id_t query_id;
    int             error;
};

struct wdbc_event_t
{
    enum wdbc_event type;
    union
    {
        wdbc_completed_query_player_character_list_event_t
            completed_query_player_character_list;
        wdbc_completed_query_player_character_t
            completed_query_player_character;
        wdbc_completed_query_create_player_character_t
            completed_query_create_player_character;
        wdbc_completed_query_create_dynamic_object_in_player_container_t
            completed_query_create_dynamic_object_in_player_container;
        wdbc_completed_query_save_player_character_position_and_direction_t
            completed_query_save_player_character_position_and_direction;
        wdbc_completed_query_save_dynamic_object_position_in_player_inventory_t
            completed_query_save_dynamic_object_position_in_player_inventory;
        wdbc_completed_query_remove_dynamic_object_from_player_container_t
            completed_query_remove_dynamic_object_from_player_container;
    };
};

int
wdbc_init(const char *ip,
    uint16 port, /* Use 0 for default */
    const char *unix_socket, /* If used, will be prioritized instead of IP */
    const char *shard_db_name,
    const char *username,
    const char *password,
    uint32 max_pending_queries, log_t *log,
    int log_info_category,
    int log_error_category,
    int log_debug_category,
    void (*post_event)(wdbc_event_t *event));

void
wdbc_destroy(void);

int
wdbc_connect(void);

void
wdbc_handle_event(wdbc_event_t *event);

void
wdbc_cancel_query(wdbc_query_id_t query_id);
/* Callback is not called if a query is cancelled. */

wdbc_query_id_t
wdbc_query_player_character_list(
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        wdbc_player_character_t *characters, uint32 num_characters),
    void *user_data, account_db_id_t account_id);

wdbc_query_id_t
wdbc_query_player_character(
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        wdbc_player_character_t *character,
        wdbc_dynamic_object_in_db_t *items, uint32 num_items),
    void *user_data, account_db_id_t account_db_id,
    player_db_guid_t player_db_id);

wdbc_query_id_t
wdbc_query_create_player_character(
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        wdbc_player_character_t *character),
    void *user_data, account_db_id_t account_id, const char *name, int race,
    int sex, uint32 instance_id, int position[3], int direction,
    wdbc_dynamic_object_t *items, uint32 num_items);

wdbc_query_id_t
wdbc_query_create_dynamic_object_in_player_container(
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        wdbc_dynamic_object_in_db_t *dobj),
    void *user_data, dobj_type_id_t type_id,
    player_db_guid_t player_db_id, equipment_slot_id_t equipment_slot, uint8 x,
    uint8 y);

wdbc_query_id_t
wdbc_query_save_player_character_position_and_direction(
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        player_db_guid_t player_db_id, uint32 instance_id, uint8 direction,
        int32 x, int32 y, uint8 z),
    void *user_data, player_db_guid_t player_db_id, uint32 instance_id,
    uint8 direction, int32 x, int32 y, uint8 z);

wdbc_query_id_t
wdbc_query_save_dynamic_object_position_in_player_inventory(
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        uuid16_t dobj_db_uuid, equipment_slot_id_t equipment_slot_id, uint8 x,
        uint8 y),
    void *user_data, uuid16_t dobj_db_uuid,
    equipment_slot_id_t equipment_slot_id, uint8 x, uint8 y);

wdbc_query_id_t
wdbc_query_remove_dynamic_object_from_player_container(
    void (*on_complete)(wdbc_query_id_t query_id, void *user_data, int error,
        uuid16_t dobj_db_uuid),
    void *user_data,
    uuid16_t dobj_db_uuid);

#endif /* MUTA_SHARED_WORLD_DB_H */
