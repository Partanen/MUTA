#ifndef MUTA_SHARED_TYPES_H
#define MUTA_SHARED_TYPES_H

#include <stdint.h>
#include <stddef.h>

typedef unsigned char           uchar;
typedef unsigned short          ushort;
typedef unsigned int            uint;
typedef int8_t                  int8;
typedef uint8_t                 uint8;
typedef int32_t                 int32;
typedef uint32_t                uint32;
typedef int32_t                 bool32;
typedef int8_t                  bool8;
typedef int16_t                 int16;
typedef uint16_t                uint16;
typedef int64_t                 int64;
typedef uint64_t                uint64;
typedef long int                lint;
typedef long long int           llint;
typedef long unsigned int       luint;
typedef long long unsigned int  lluint;
typedef float                   f32;
typedef double                  f64;

 /* Dynamic string (use dchar * instead of char *) */
typedef char                    dchar;

typedef uint16                  tile_t;
typedef uint16                  emote_id_t;
typedef uint16                  msg_sz_t;   /* Part of an encrypted message */

/* Instances and maps */
typedef uint32                  map_id_t; /* Comes from map db */
typedef uint32                  instance_runtime_id_t;

/* Database related player ID types */
typedef uint64                  account_db_id_t;
typedef uint64                  player_db_guid_t; /* For characters */

/* Object type IDs such as player races, creature types, etc. */
typedef uint8                   player_race_id_t;
typedef uint32                  sobj_type_id_t;
typedef uint32                  dobj_type_id_t;
typedef uint32                  creature_type_id_t;

/* Runtime IDs for individual objects. */
typedef uint32                  player_runtime_id_t; /* "world session id" */
typedef uint32                  creature_runtime_id_t;
typedef uint32                  dobj_runtime_id_t;
typedef uint64                  item_runtime_id_t; /* Stored in the shard db */

/* Equipment slots for players */
typedef uint8                   equipment_slot_id_t;

/* uuid, used in db */
typedef struct {uint8 data[16];} uuid16_t;

/* Abilities */
typedef uint32 ability_id_t;

#define MUTA_LIL_ENDIAN 1
#define MUTA_BIG_ENDIAN 2
#define MUTA_ENDIANNESS MUTA_LIL_ENDIAN

#endif /* MUTA_SHARED_TYPES_H */
