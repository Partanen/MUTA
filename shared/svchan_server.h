/* svchan_server.h
 * A reusable server-to-server connection interface. Used by the side that
 * receives the connection: see svchan_client.h for the side that connects. */

#ifndef MUTA_SHARED_SVCHAN_SERVER_H
#define MUTA_SHARED_SVCHAN_SERVER_H

#include "netpoll.h"
#include "common_utils.h"

/* Forward declaration(s) */
typedef struct cryptchan_t cryptchan_t;

/* Types defined here */
typedef struct svchan_server_event_t        svchan_server_event_t;
typedef struct svchan_server_callbacks_t    svchan_server_callbacks_t;
typedef struct svchan_server_t              svchan_server_t;
typedef struct svchan_account_info_t        svchan_account_info_t;
typedef struct svchan_server_client_t       svchan_server_client_t;
typedef struct svchan_server_timer_t        svchan_server_timer_t;

enum svchan_server_disconnect_reason
{
    SVCHAN_SERVER_DC_CONNECTION_LOST,
    SVCHAN_SERVER_DC_TIMEOUT,
    SVCHAN_SERVER_DC_MANUAL,
    SVCHAN_SERVER_DC_PROTOCOL_ERROR,
    SVCHAN_SERVER_DC_INTERNAL_ERROR
};

struct svchan_server_event_t
{
    int             type;
    svchan_server_t *server;
    union
    {
        struct
        {

            socket_t    socket;
            uint32      address;
            uint32      listen_socket_index;
        } accept;
        struct
        {
            uint32  client_index;
            uint64  client_id;
            int     num_bytes;
            uint8   *memory;
        } read_client;
        struct
        {
            uint64 time_diff; /* Time passed since last check. */
        } check_timeouts;
    };
};

struct svchan_server_callbacks_t
{
    void (*post_event)(svchan_server_event_t *event);

    int (*on_authed)(svchan_server_client_t *client);
    /* on_authed()
     * Called when a client is authenticated. After this, it's up to the user to
     * handle any messages with on_read_packet(). */

    int (*on_read_packet)(svchan_server_client_t *client, uint8 *memory,
        int num_bytes);
    /* read_packet()
     * Read a received packet.
     * Must return < 1 if the client should be disconnected, or the number of
     * unread bytes left in the buffer. */

    void (*on_disconnect)(svchan_server_client_t *client, int reason);
    /* This function is always called, whether svchan_server_client_disconnect()
     * was called or client disconnected in another fashion. */
};

struct svchan_server_t
{
    svchan_account_info_t               *account_infos;
    uint16                              *ports;
    uint32                              *allowed_addresses;
    netpoll_t                           netpoll;
    thread_t                            thread;
    int32                               running;
    socket_t                            *listen_sockets;
    svchan_server_callbacks_t           callbacks;
    fixed_pool(svchan_server_client_t)  clients;
    svchan_server_timer_t               *timers;
    uint32                              num_timers;
    uint64                              running_client_id;
    segpool_t                           segpool;
    mutex_t                             segpool_mutex;
    uint32                              *flush_clients;
    uint32                              num_flush_clients;
    uint32                              client_in_buf_size;
    uint32                              client_out_buf_size;
};

int
svchan_server_init(svchan_server_t *server,
    svchan_server_callbacks_t *callbacks, uint32 max_clients,
    uint32 client_in_buf_size, uint32 client_out_buf_size);

void
svchan_server_destroy(svchan_server_t *server);

int
svchan_server_start(svchan_server_t *server);
/* svchan_server_start()
 * Start the server, making it listen to connections and messages.
 * NOTE: At the moment this is it's expected this is only called once in each
 * program! */

void
svchan_server_stop(svchan_server_t *server);

void
svchan_server_add_account_info(svchan_server_t *server, char *name,
    char *password);

void
svchan_server_add_port(svchan_server_t *server, uint16 port);

void
svchan_server_add_address(svchan_server_t *server, uint32 address);

void
svchan_server_handle_event(svchan_server_event_t *event);

void
svchan_server_flush(svchan_server_t *server);
/* svchan_server_flush()
 * Flush buffered messages to clients. Recommended to call every frame, or every
 * time there's an event posted on the application's main thread. */

void
svchan_server_client_set_user_data(svchan_server_client_t *client,
    void *user_data);
/* svchan_server_client_set_user_data()
 * Set the user data of the client. Can be used to identify the client later.
 * Normally this is set in the on_authed callback. */

void *
svchan_server_client_get_user_data(svchan_server_client_t *client);

cryptchan_t *
svchan_server_client_get_cryptchan(svchan_server_client_t *client);

bbuf_t
svchan_server_client_send(svchan_server_client_t *client, uint32 num_bytes);
/* svchan_server_client_send()
 * Send an uncencrypted message to client. The returned bbuf_t's max_bytes
 * member will be 0 if this fails.
 * Note: num_bytes must include the size of the message type enumerator used by
 * the protocol - it is not appended automatically. */

bbuf_t
svchan_server_client_send_const_encrypted(svchan_server_client_t *client,
    uint32 num_bytes);
/* svchan_server_client_send_const_encrypted()
 * Same as svchan_server_client_send(), but allocates extra space for required
 * const size encryption. */

bbuf_t
svchan_server_client_send_var_encrypted(svchan_server_client_t *client,
    uint32 num_bytes);
/* svchan_server_client_send_var_encrypted()
 * Same as svchan_server_client_send(), but allocates extra space for required
 * variable size encryption. */

void
svchan_server_client_disconnect(svchan_server_client_t *client);

#endif /* MUTA_SHARED_SVCHAN_SERVER_H */
