#include "mdb.h"
#include "common_utils.h"
#include "containers.h"
#include "common_utils.h"

#define MDB_COL_BYTE_SZ (MDB_MAX_COL_NAME_LEN + sizeof(uint8) * 2)

#ifndef MDB_DEBUG
    #define MDB_DEBUG 0
#endif

#if MDB_DEBUG
    #define MDB_DB_PRINTF(str, ...) printf((str), ##__VA_ARGS__)
#else
    #define MDB_DB_PRINTF(str, ...) ((void)0)
#endif

#define CHECK_ROW(db_, row_, row_len_) \
    ((db_)->num_rows && (row_len_) > 0 && (row_) >= sizeof(uint32) \
    && ((row_) - sizeof(uint32)) / (row_len_) < (db_)->num_rows \
    && ((row_) - sizeof(uint32)) % (row_len_) == 0)

#define GET_ROW_PTR(db_, row_, row_len_) \
    (CHECK_ROW((db_), (row_), (row_len_)) ? \
    (void*)((char*)(db_)->rows + (row_)) : 0)

static uint32
_mdb_compute_row_len(mdb_t *db);

static uint32
_mdb_compute_row_len_on_disk(mdb_t *db);

static uint32
_mdb_compute_str_block_offset(mdb_t *db);

static void *
_mdb_get_field(mdb_t *db, void *row, uint32 col, int *ret_type /* Optional */);

static int
_mdb_set_field(mdb_t *db, void *row, uint32 col, void *val);

static uint32
_mdb_compute_col_offset(mdb_t *db, uint32 index);

static uint32
_get_col_type_size(int type);

static uint32
_get_col_type_size_on_disk(int type);

int
mdb_save(mdb_t *db, const char *fp)
{
    if (!fp)
    {
        MDB_DB_PRINTF("%s: file path was null.\n", __func__);
        return 1;
    }
    int fp_len = (int)strlen(fp);
    if (!fp_len)
        return 2;
    char    *tmp_fp     = stack_alloc(fp_len + 6);
    bool32  have_slash  = 0;
    int     dst, src;
    for (dst = fp_len, src = fp_len - 1; dst >= 0; --dst)
    {
        if (!have_slash && (fp[src] == '/' || dst == 0))
        {
            have_slash = 1;
            tmp_fp[dst] = '.';
        } else
            tmp_fp[dst] = fp[src--];
    }
    memcpy(tmp_fp + fp_len + 1, ".tmp", 4);
    tmp_fp[fp_len + 5] = 0;
    FILE *f = fopen(tmp_fp, "wb+");
    if (!f)
    {
        MDB_DB_PRINTF("%s: could not open file '%s'.\n", __func__, tmp_fp);
        return 3;
    }
    int ret = 0;
    #define WRITE_OR_OUT(val_, type_, ret_) \
        if (fwrite_##type_(f, val_)) {ret = (ret_); goto out;}
    fwrite_uint8_arr(f, (uint8*)db->name, MDB_MAX_NAME_LEN);
    WRITE_OR_OUT(darr_num(db->cols), uint32, 4);
    WRITE_OR_OUT(db->num_rows, uint32, 5);
    WRITE_OR_OUT(db->running_id, uint32, 6);
    WRITE_OR_OUT(db->version++, uint32, 7);
    if (fseek(f, MDB_HEADER_PAD_BYTES, SEEK_CUR))
        {ret = 8; goto out;}
    for (uint32 i = 0; i < darr_num(db->cols); ++i)
    {
        uint32 name_len = dstr_len(db->cols[i].name);
        if (name_len > MDB_MAX_COL_NAME_LEN)
            {ret = 9; goto out;}
        if (fwrite_uint8_arr(f, (uint8*)db->cols[i].name, name_len))
            {ret = 10; goto out;}
        WRITE_OR_OUT(MDB_MAX_COL_NAME_LEN - name_len, zeros, 11);
        WRITE_OR_OUT(db->cols[i].type, uint8, 12);
        WRITE_OR_OUT(db->cols[i].is_key, uint8, 13);
    }
    uint32  str_block_beg   = _mdb_compute_str_block_offset(db);
    uint32  row_len         = _mdb_compute_row_len(db);
    char    *row            = (char*)db->rows + sizeof(uint32);
    uint32  str_block_os    = 0;
    for (uint32 i = 0; i < db->num_rows; ++i)
    {
        for (uint32 j = 0; j < darr_num(db->cols); ++j)
        {
            mdb_col_t   *col = &db->cols[j];
            void        *src = row + col->offset;
            switch (col->type)
            {
                case MDB_COL_INT8:
                    MDB_DB_PRINTF("%s: writing int8: %d\n", __func__,
                        *(int8*)src);
                    WRITE_OR_OUT(*(int8*)src, int8, 14);
                    break;
                case MDB_COL_UINT8:
                    MDB_DB_PRINTF("%s: writing uint8: %u\n", __func__,
                        *(uint8*)src);
                    WRITE_OR_OUT(*(uint8*)src, uint8, 15);
                    break;
                case MDB_COL_INT16:
                    MDB_DB_PRINTF("%s: writing int16: %d\n", __func__,
                        *(int16*)src);
                    WRITE_OR_OUT(*(int16*)src, int16, 16);
                    break;
                case MDB_COL_UINT16:
                    MDB_DB_PRINTF("%s: writing uint16: %d\n", __func__,
                        *(uint16*)src);
                    WRITE_OR_OUT(*(uint16*)src, uint16, 17);
                    break;
                case MDB_COL_INT32:
                    MDB_DB_PRINTF("%s: writing int32: %d\n", __func__,
                        *(int32*)src);
                    WRITE_OR_OUT(*(int32*)src, int32, 18);
                    break;
                case MDB_COL_UINT32:
                    MDB_DB_PRINTF("%s: writing uint32: %d\n", __func__,
                        *(uint32*)src);
                    WRITE_OR_OUT(*(uint32*)src, uint32, 19);
                    break;
                case MDB_COL_STR:
                {
                    dchar       **dstr  = (dchar**)src;
                    uint32      len     = dstr_len(*dstr);
                    /* Pointer to the str block */
                    MDB_DB_PRINTF("%s: writing str offset %u\n", __func__,
                        str_block_os);
                    WRITE_OR_OUT(str_block_os, uint32, 20);
                    long int fos = ftell(f);
                    if (fos < 0)
                        {ret = 21; goto out;}
                    if (fseek(f, str_block_beg + str_block_os, SEEK_SET))
                        {ret = 22; goto out;}
                    /* Write the string to the block instead of inline */
                    WRITE_OR_OUT(len, uint32, 23);
                    if (fwrite_uint8_arr(f, (uint8*)*dstr, len))
                        {ret = 24; goto out;}
                    if (fseek(f, fos, SEEK_SET))
                        {ret = 25; goto out;}
                    MDB_DB_PRINTF("%s: writing str: %s, offset into block: %u, "
                        "block begin %u, len: %u\n", __func__, *(char**)src,
                        str_block_os, str_block_beg, len);
                    str_block_os += sizeof(uint32) + len;
                }
                    break;
                default:
                    {ret = 26; goto out;}
            }
        }
        row += row_len;
    }
    out:
        fclose(f);
        if (!ret)
        {
#if _WIN32
            DeleteFile(fp);
#endif
            rename(tmp_fp, fp);
        } else
            MDB_DB_PRINTF("%s failed with code %d\n", __func__, ret);
        return ret;
    #undef WRITE_OR_OUT
}

int
mdb_open(mdb_t *db, const char *fp)
{
    memset(db, 0, sizeof(mdb_t));
    FILE *f = fopen(fp, "rb");
    if (!f) return 1;

    int ret = 0;

    if (fread_uint8_arr(f, (uint8*)db->name, MDB_MAX_NAME_LEN))
        {ret = 2; goto out;}

    #define READ_OR_OUT(dst, type, ret_val) \
        if (fread_##type(f, (dst))) \
            {ret = ret_val; goto out;}

    uint32 num_cols;
    READ_OR_OUT(&num_cols,          uint32, 3);
    READ_OR_OUT(&db->num_rows,      uint32, 4);
    READ_OR_OUT(&db->running_id,    uint32, 5);
    READ_OR_OUT(&db->version,       uint32, 6);

    MDB_DB_PRINTF("%s: num rows: %u\n", __func__, db->num_rows);
    MDB_DB_PRINTF("%s: num cols: %u\n", __func__, num_cols);

    fseek(f, MDB_HEADER_PAD_BYTES, SEEK_CUR);

    darr_reserve(db->cols, num_cols);

    char col_name[MDB_MAX_COL_NAME_LEN + 1] = {0};
    for (uint32 i = 0; i < num_cols; ++i)
    {
        if (fread_uint8_arr(f, (uint8*)col_name, MDB_MAX_COL_NAME_LEN))
            {ret = 9; goto out;}

        mdb_col_t *col = darr_push_empty(db->cols);
        READ_OR_OUT(&col->type, uint8, 1);

        if (col->type > NUM_MDB_COL_TYPES)
            {ret = 10; goto out;}

        READ_OR_OUT(&col->is_key, uint8, 11);

        col->name = create_dynamic_str(col_name);
        if (!col->name) {ret = 12; goto out;}

        col->offset = _mdb_compute_col_offset(db, i);

        MDB_DB_PRINTF("%s: read column type %s\n", __func__,
            mdb_col_type_to_str(col->type));
    }

    /* Read the rows */
    uint32 row_len      = _mdb_compute_row_len(db);
    uint32 max_rows     = MAX(db->num_rows, db->num_rows * 105 / 100);
    uint32 rows_mem_sz  = sizeof(uint32) + max_rows * row_len;

    db->rows = calloc(1, rows_mem_sz);
    if (!db->rows) {ret = 13; goto out;}

    /* Save the memory block size of the row array */
    *(uint32*)db->rows = rows_mem_sz;

    char    *row            = (char*)db->rows + sizeof(uint32);
    uint32  str_block_beg   = _mdb_compute_str_block_offset(db);

    for (uint32 i = 0; i < db->num_rows; ++i)
    {
        for (uint32 j = 0; j < darr_num(db->cols); ++j)
        {
            mdb_col_t   *col = &db->cols[j];
            void        *dst = row + col->offset;
            switch (col->type)
            {
                case MDB_COL_INT8:
                {
                    MDB_DB_PRINTF("%s: reading int8, val %d\n", __func__,
                        *(int8*)dst);
                    READ_OR_OUT((int8*)dst, int8, 14);
                }
                    break;
                case MDB_COL_UINT8:
                {
                    MDB_DB_PRINTF("%s: reading uint8, val %d\n", __func__,
                        *(uint8*)dst);
                    READ_OR_OUT((uint8*)dst, uint8, 15);
                }
                    break;
                case MDB_COL_INT16:
                {
                    MDB_DB_PRINTF("%s: reading int16, val %d\n", __func__,
                         *(int16*)dst);
                    READ_OR_OUT((int16*)dst, int16, 16);
                }
                    break;
                case MDB_COL_UINT16:
                {
                    MDB_DB_PRINTF("%s: reading uint16, val %d\n",
                        __func__, *(uint16*)dst);
                    READ_OR_OUT((uint16*)dst, uint16, 17);
                }
                    break;
                case MDB_COL_INT32:
                {
                    MDB_DB_PRINTF("%s: reading int32, val %d\n",
                        __func__, *(int32*)dst);
                    READ_OR_OUT((int32*)dst, int32, 18);
                }
                    break;
                case MDB_COL_UINT32:
                {
                    MDB_DB_PRINTF("%s: reading uint32, val %d\n",
                        __func__, *(uint32*)dst);
                    READ_OR_OUT((uint32*)dst, uint32, 19);
                }
                    break;
                case MDB_COL_STR:
                {
                    uint32 str_os; /* Relative offset into the string block */
                    READ_OR_OUT(&str_os, uint32, 20);

                    MDB_DB_PRINTF("%s: str_os (str_block_beg: %u): %u\n",
                        __func__, str_block_beg, str_os);

                    long int fos = ftell(f);
                    if (fseek(f, str_block_beg + str_os, SEEK_SET))
                        {ret = 21; goto out;}

                    uint32 str_len;
                    READ_OR_OUT(&str_len, uint32, 22);

                    MDB_DB_PRINTF("%s: reading string, len %u\n", __func__,
                        str_len);

                    dchar **dstr    = (dchar**)dst;
                    *dstr           = create_empty_dynamic_str(str_len);
                    if (!*dstr && str_len > 0)
                        {ret = 23; goto out;}

                    if (fread_uint8_arr(f, (uint8*)*dstr, str_len))
                        {ret = 24; goto out;}

                    set_dynamic_str_len(*dstr, str_len);

                    if (fseek(f, fos, SEEK_SET))
                        {ret = 25; goto out;}

                    MDB_DB_PRINTF("%s: reading str, val %s, offset from block:"
                        " %u, block offset: %u, len: %u\n", __func__, *dstr,
                        str_os, str_block_beg, str_len);
                }
                    break;
                default:
                    {ret = 25; goto out;}
            }

        }
        row += row_len;
    }
    #undef READ_OR_OUT

    out:
        fclose(f);
        if (ret)
        {
            MDB_DB_PRINTF("%s failed with code %d.\n", __func__, ret);
            mdb_close(db);
        }
        return ret;
}

int
mdb_new(mdb_t *db)
{
    memset(db, 0, sizeof(mdb_t));

    int ret = 0;

    darr_reserve(db->cols, 16);

    db->rows = calloc(1, 4096);
    if (!db->rows) return 2;

    *(uint32*)db->rows = 4096;

    if (mdb_new_col(db, "id", MDB_COL_UINT32, 1))
        {ret = 2; goto out;}

    out:
        return ret;
}

void
mdb_close(mdb_t *db)
{
    for (uint32 i = 0; i < mdb_num_rows(db); ++i)
        for (uint32 j = 0; j < mdb_num_cols(db); ++j)
        {
            if (db->cols[j].type != MDB_COL_STR)
                continue;
            void *field = mdb_get_field(db, mdb_get_row_by_index(db, i), j);
            assert(field);
            free_dynamic_str(*(char**)field);
        }
    for (uint32 i = 0; i < mdb_num_cols(db); ++i)
        free_dynamic_str(db->cols[i].name);
    free(db->rows);
    darr_free(db->cols);
    memset(db, 0, sizeof(mdb_t));
}

int
mdb_new_col(mdb_t *db, const char *name, int type, bool32 is_key)
{
    for (uint i = 0; i < darr_num(db->cols); ++i)
        if (streq(db->cols[i].name, name))
            return 1;

    if (type < 0 || type >= NUM_MDB_COL_TYPES)
        return 2;

    uint32 old_row_len = _mdb_compute_row_len(db);
    uint32 new_row_len;

    mdb_col_t *col  = darr_push_empty(db->cols);
    col->name       = dstr_create(name);
    col->type       = type;
    col->is_key     = is_key ? 1 : 0;
    new_row_len     = _mdb_compute_row_len(db);
    col->offset     = _mdb_compute_col_offset(db, darr_num(db->cols) - 1);

    uint32 new_req = sizeof(uint32) + db->num_rows * new_row_len;
    if (!new_req) return 0;

    if (!db->rows || *(uint32*)db->rows < new_req)
    {
        new_req             = MAX(new_req, new_req * 105 / 100);
        db->rows            = erealloc(db->rows, new_req);
        *(uint32*)db->rows  = new_req;
    }

    void *dst, *src;
    char *rows = (char*)db->rows + sizeof(uint32);

    if (db->num_rows > 0)
    {
        /* Move rows to accomodate for the changes */
        for (uint32 i = db->num_rows - 1;; --i)
        {
            src = rows + i * old_row_len;
            dst = rows + i * new_row_len;
            memmove(dst, src, old_row_len);
            memset((char*)dst + old_row_len, 0, new_row_len - old_row_len);
            if (i == 0) break;
        }
    }
    return 0;
}

int
mdb_del_col(mdb_t *db, uint32 col_index)
{
    if (col_index >= darr_num(db->cols))    return 1;
    if (darr_num(db->cols) == 1)            return 2;

    mdb_col_t *col = &db->cols[col_index];
    if (streq(col->name, "id"))
        return 3;

    uint32  col_sz      = _get_col_type_size(col->type);
    uint32  col_os      = col->offset;
    uint32  old_row_len = _mdb_compute_row_len(db);

    darr_erase(db->cols, col_index);

    char    *rows       = (char*)db->rows + sizeof(uint32);
    uint32  num_rows    = db->num_rows;
    uint32  new_row_len = old_row_len - col_sz;
    char    *dst;

    for (uint32 i = 0; i < num_rows; ++i)
    {
        dst = rows + col_os;
        memcpy(dst, dst + col_sz, old_row_len - col_os - 1);
        dst = rows - i * col_sz;
        memmove(dst, rows, new_row_len);
        rows += old_row_len;
    }
    return 0;
}

int
mdb_new_row(mdb_t *db, uint32 *ret_id)
{
    uint32 row_len = _mdb_compute_row_len(db);
    if (!row_len) return 1;

    uint32 req = sizeof(uint32) + (MAX((db->num_rows + 1),
        (db->num_rows + 1) * 105 / 100) * row_len);

    if (!db->rows || *(uint32*)db->rows < req)
    {
        db->rows = erealloc(db->rows, req);
        *(uint32*)db->rows = req;
    }

    void *row = (char*)db->rows + sizeof(uint32) + db->num_rows * row_len;
    memset(row, 0, row_len);

    uint32 id = db->running_id++;
    _mdb_set_field(db, row, mdb_get_col_index(db, "id"), &id);
    *ret_id = id;

    db->num_rows++;
    return 0;
}

static void *
_mdb_get_row_by_id(mdb_t *db, uint32 id)
{
    int col_index = mdb_get_col_index(db, "id");
    if (col_index < 0) return 0;

    uint32  row_len     = _mdb_compute_row_len(db);
    char    *rows       = (char*)db->rows + sizeof(uint32);
    uint32  num_rows    = db->num_rows;
    uint32  col_offset  = db->cols[col_index].offset;

    for (uint32 i = 0; i < num_rows; ++i)
    {
        uint32 row_id = *(uint32*)(rows + col_offset);
        if (row_id == id) return rows;
        rows += row_len;
    }
    return 0;
}

int
mdb_del_row(mdb_t *db, mdb_row_t row)
{
    uint32  row_len = _mdb_compute_row_len(db);
    void    *rowp   = GET_ROW_PTR(db, row, row_len);
    if (!rowp) return 1;
    db->num_rows--;
    uint32 row_index = (row - sizeof(uint32)) / row_len;
    uint32 num_bytes = (db->num_rows - row_index) * row_len;
    memmove(rowp, (char*)rowp + row_len, num_bytes);
    return 0;
}

mdb_row_t
mdb_get_row(mdb_t *db, uint32 id)
{
    void *row = _mdb_get_row_by_id(db, id);
    if (!row) return 0;
    mdb_row_t ret = (mdb_row_t)((char*)row - (char*)db->rows);
    MDB_DB_PRINTF("%s: returning %u\n", __func__, ret);
    return ret;
}

mdb_row_t
mdb_get_row_by_index(mdb_t *db, int index)
{
    if (index < 0 || (uint32)index >= db->num_rows)
        return 0;
    uint32 row_len  = _mdb_compute_row_len(db);
    return sizeof(uint32) + (uint32)index * row_len;
}

void *
mdb_get_field(mdb_t *db, mdb_row_t row, uint32 col)
{
    uint32  row_len         = _mdb_compute_row_len(db);
    uint32  tot_row_bytes   = db->num_rows * row_len;

    if (row < sizeof(uint32) || row >= tot_row_bytes) /* Unaligned */
        return 0;

    void *ret = _mdb_get_field(db, (char*)db->rows + row, col, 0);
    return ret;
}

int
mdb_set_field(mdb_t *db, mdb_row_t row, uint32 col, void *val)
{
    uint32  row_len         = _mdb_compute_row_len(db);
    uint32  tot_row_bytes   = db->num_rows * row_len;
    if (row < sizeof(uint32) || row >= tot_row_bytes)
    /* TODO: Check unaligned */
    {
        MDB_DB_PRINTF("%s: invalid row (%u).\n", __func__, row);
        return 1;
    }
    return _mdb_set_field(db, (char*)db->rows + row, col, val);
}

int
mdb_create_and_set_field_by_name_with_type(mdb_t *mdb, mdb_row_t row,
    const char *col_name, enum mdb_col_type col_type,
        bool32 col_is_key, void *val)
{
    int col_index = mdb_get_col_index(mdb, col_name);
    if (col_index < 0)
    {
        if (mdb_new_col(mdb, col_name, col_type, col_is_key))
            return 1;
        col_index = mdb_get_col_index(mdb, col_name);
        if (col_index < 0)
            return 2;
    }
    if (mdb_get_col_type(mdb, col_index) != col_type)
        return 3;
    if (mdb_set_field(mdb, row, col_index, val))
        return 4;
    return 0;
}

int
mdb_get_col_index(mdb_t *db, const char *name)
{
    for (uint32 i = 0; i < darr_num(db->cols); ++i)
        if (streq(db->cols[i].name, name)) return (int)i;
    return -1;
}

enum mdb_col_type
mdb_get_col_type(mdb_t *db, uint32 col)
{
    if (!darr_num(db->cols) || col >= darr_num(db->cols))
        return NUM_MDB_COL_TYPES;
    return db->cols[col].type;
}

int
mdb_get_col_index_with_type(mdb_t *db, enum mdb_col_type type,
    const char *name)
{
    int index = mdb_get_col_index(db, name);
    if (index < 0)
        return -1;
    if (mdb_get_col_type(db, index) != type)
        return -1;
    return index;
}

int
mdb_get_or_create_col_index(mdb_t *db, enum mdb_col_type type, const char *name,
    bool32 col_is_key)
{
    int index = mdb_get_col_index_with_type(db, type, name);
    if (index >= 0)
        return index;
    if (mdb_new_col(db, name, type, col_is_key))
        return -1;
    return mdb_get_col_index(db, name);
}

int
mdb_set_name(mdb_t *db, const char *name)
{
    if (!name)
    {
        db->name[0] = 0;
        return 0;
    }
    uint len = (uint)strlen(name);
    if (len > MDB_MAX_NAME_LEN)
        return 2;
    memcpy(db->name, name, len + 1);
    return 0;
}

static uint32
_mdb_compute_row_len(mdb_t *db)
{
    uint len = 0;
    for (uint32 i = 0; i < darr_num(db->cols); ++i)
        len += _get_col_type_size(db->cols[i].type);
    return len;
}

static uint32
_mdb_compute_row_len_on_disk(mdb_t *db)
{
    uint len = 0;
    for (uint32 i = 0; i < darr_num(db->cols); ++i)
        len += _get_col_type_size_on_disk(db->cols[i].type);
    return len;
}

static uint32
_mdb_compute_str_block_offset(mdb_t *db)
{
    return MDB_MAX_NAME_LEN + sizeof(uint32) * 4 + \
        MDB_HEADER_PAD_BYTES + darr_num(db->cols) * MDB_COL_BYTE_SZ + \
        db->num_rows * _mdb_compute_row_len_on_disk(db);
}

static void *
_mdb_get_field(mdb_t *db, void *row, uint32 col, int *ret_type)
{
    if (col >= darr_num(db->cols))
        return 0;
    mdb_col_t *c = &db->cols[col];
    if (ret_type)
        *ret_type = c->type;
    return (char*)row + c->offset;
}

static int
_mdb_set_field(mdb_t *db, void *row, uint32 col, void *val)
{
    int     col_type;
    void    *dst = _mdb_get_field(db, row, col, &col_type);
    if (!dst)
        return 1;
    switch (col_type)
    {
        case MDB_COL_INT8:      *(int8*)dst     = *(int8*)val;      break;
        case MDB_COL_UINT8:     *(uint8*)dst    = *(uint8*)val;     break;
        case MDB_COL_INT16:     *(int16*)dst    = *(int16*)val;     break;
        case MDB_COL_UINT16:    *(uint16*)dst   = *(uint16*)val;    break;
        case MDB_COL_INT32:     *(int32*)dst    = *(int32*)val;     break;
        case MDB_COL_UINT32:    *(uint32*)dst   = *(uint32*)val;    break;
        case MDB_COL_STR:
        {
            dchar **dstr = dst;
            dstr_set(dstr, (char*)val);
        }
            break;
        default:
            muta_assert(0);
    }
    return 0;
}

static uint32
_mdb_compute_col_offset(mdb_t *db, uint32 index)
{
    uint32 ret = 0;
    for (uint32 i = 0; i < index; ++i)
        ret += _get_col_type_size(db->cols[i].type);
    return ret;
}

static uint32
_get_col_type_size(int type)
{
    uint32 ret;
    switch (type)
    {
        case MDB_COL_INT8:      ret = 1; break;
        case MDB_COL_UINT8:     ret = 1; break;
        case MDB_COL_INT16:     ret = 2; break;
        case MDB_COL_UINT16:    ret = 2; break;
        case MDB_COL_INT32:     ret = 4; break;
        case MDB_COL_UINT32:    ret = 4; break;
        case MDB_COL_STR:       ret = sizeof(dchar*); break;
        default:                ret = 0; muta_assert(0);
    }
    return ret;
}

static uint32
_get_col_type_size_on_disk(int type)
{
    uint32 ret;
    switch (type)
    {
        case MDB_COL_INT8:      ret = 1; break;
        case MDB_COL_UINT8:     ret = 1; break;
        case MDB_COL_INT16:     ret = 2; break;
        case MDB_COL_UINT16:    ret = 2; break;
        case MDB_COL_INT32:     ret = 4; break;
        case MDB_COL_UINT32:    ret = 4; break;
        case MDB_COL_STR:       ret = 4; break;
        default:                ret = 0; muta_assert(0);
    }
    return ret;
}

const char *
mdb_col_type_to_str(int type)
{
    const char *ret;
    switch (type)
    {
        case MDB_COL_INT8:      ret = "int8";   break;
        case MDB_COL_UINT8:     ret = "uint8";  break;
        case MDB_COL_INT16:     ret = "int16";  break;
        case MDB_COL_UINT16:    ret = "uint16"; break;
        case MDB_COL_INT32:     ret = "int32";  break;
        case MDB_COL_UINT32:    ret = "uint32"; break;
        case MDB_COL_STR:       ret = "string"; break;
        default: ret = 0;
    }
    return ret;
}
