#ifndef MUTA_NET_H
#define MUTA_NET_H

#include "ksys.h"
#include "types.h"

typedef struct addr_t addr_t;

enum sock_shutdown_t
{
#ifdef __linux__
    SOCKSD_READ  = SHUT_RD,
    SOCKSD_WRITE = SHUT_WR,
    SOCKSD_BOTH  = SHUT_RDWR
#elif defined(_WIN32)
    SOCKSD_READ  = SD_RECEIVE,
    SOCKSD_WRITE = SD_SEND,
    SOCKSD_BOTH  = SD_BOTH
#else
    #error "Unsupported platform"
#endif
};

#ifdef __linux__
    #define NET_SOCKET_CONNECT_IN_PROGRESS EINPROGRESS
#elif defined(_WIN32)
    #define NET_SOCKET_CONNECT_IN_PROGRESS WSAEWOULDBLOCK
#endif

struct addr_t
{
    uint32 ip;
    uint16 port;
};

#define ADDR_A(addr_ptr) (((uint8*)&((addr_ptr)->ip))[3])
#define ADDR_B(addr_ptr) (((uint8*)&((addr_ptr)->ip))[2])
#define ADDR_C(addr_ptr) (((uint8*)&((addr_ptr)->ip))[1])
#define ADDR_D(addr_ptr) (((uint8*)&((addr_ptr)->ip))[0])
#define ADDR_IP_TO_PRINTF_ARGS(addr_ptr) \
    ADDR_A((addr_ptr)), ADDR_B((addr_ptr)), \
    ADDR_C((addr_ptr)), ADDR_D((addr_ptr))

#define net_listen(sock, backlog) listen((sock), (backlog))

void
addr_init(addr_t *addr, uint8 a, uint8 b, uint8 c, uint8 d, uint16 port);

int
addr_init_from_str(addr_t *addr, const char *ip, uint16 port);

addr_t
create_addr(uint8 a, uint8 b, uint8 c, uint8 d, uint16 port);

socket_t
net_async_tcp_socket();

int
net_send_all(socket_t sock, const void *msg, int len);
/* TCP send - returns the number of bytes sent or < 0 on error */

int
net_send_all_to(socket_t sock, const void *msg, int len, addr_t *addr);
/* UDP send - Returns the number of bytes sent or < 0 on error */

#define net_recv(sock, buffer, buf_len) recv((sock), (buffer), (buf_len), 0)
/* TCP recv - returns the number of bytes received */

static inline int
net_disable_nagle(socket_t sock);

int
net_recv_from(socket_t sock, void *buffer, int buf_len, addr_t *sender_addr);
/* UDP recv - returns the number of bytes received */

int
net_bind(socket_t fd, uint16 port);

int
net_make_sock_reusable(socket_t sock);

socket_t
net_tcp_ipv4_listen_sock(uint16 port, int backlog);

socket_t
net_accept(socket_t sock, addr_t *addr);

int
net_connect(socket_t sock, addr_t addr);

int
net_close_blocking_sock(socket_t sock);

int
net_shutdown_sock(socket_t sock, enum sock_shutdown_t type);

socket_t
net_tcp_ipv4_sock();
/* Also makes the socket reusable */

int
net_addr_of_sock(socket_t sock, addr_t *ret_addr);

void
addr_to_sockaddr(const addr_t *addr, struct sockaddr_in *sa);

void
addr_from_sockaddr(addr_t *addr, const struct sockaddr *sa);

uint32
uint32_ip_from_uint8s(uint8 a, uint8 b, uint8 c, uint8 d);

static inline int
net_disable_nagle(socket_t sock)
{
    int opt = 1;
    return setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char*)&opt, sizeof(opt));
}

#endif /* MUTA_NET_H */
