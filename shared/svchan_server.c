#include "svchan_server.h"
#include "net.h"
#include "sv_time.h"
#include "svchan_packets.h"
#include <ctype.h>

#define TIMEOUT_CHECK_FREQUENCY 2000
#define TIMEOUT                 10000
#define CLIENT_OUT_BUF_SZ       MUTA_MTU

enum svchan_server_event
{
    SVCHAN_SERVER_EVENT_ACCEPT,
    SVCHAN_SERVER_EVENT_READ_CLIENT,
    SVCHAN_SERVER_EVENT_CHECK_TIMEOUTS
};

enum svchan_server_client_state
{
    CLIENT_DISCONNECTED,
    CLIENT_AWAIT_PUB_KEY,
    CLIENT_AWAIT_STREAM_HEADER,
    CLIENT_AWAIT_LOGIN,
    CLIENT_AUTHED
};

struct svchan_account_info_t
{
    dchar   *name;
    dchar   *password;
    void    *user_data;
};

struct svchan_server_client_t
{
    socket_t        socket;
    svchan_server_t *server;
    void            *user_data;
    uint32          address;
    int             state;
    uint32          account_index;
    uint32          timer_index;
    uint64          id;
    uint32          flush_index; /* Index in server->flush_clients. 0xFFFFFFFF
                                  * if none. */
    cryptchan_t     cryptchan;
    struct
    {
        uint8   *memory;
        uint32  num;
        uint32  max;
    } in_buf;
    struct
    {
        uint8   memory[CLIENT_OUT_BUF_SZ];
        uint32  num;
    } out_buf;
};

struct svchan_server_timer_t
{
    uint32 client_index;
    uint64 time_idle;
};

static thread_ret_t
_main(void *args);

static bool32
_name_legal(const char *name, uint32 len);

static bool32
_password_legal(const char *name, uint32 len);

static void
_check_timeouts(svchan_server_t *server, uint64 time_diff);

static void
_disconnect_client(svchan_server_t *server, svchan_server_client_t *client,
    int reason);

static void
_add_client_to_flush_clients(svchan_server_t *server,
    svchan_server_client_t *client);

static int
_flush_client_for_space(svchan_server_client_t *client, uint32 num_bytes);

static bbuf_t
_send_to_client_internal_protocol(svchan_server_client_t *client,
    uint32 num_bytes);
/* _send_to_client_internal_protocol()
 * Send something to a client using the svchan_server_t protocol. Called only
 * before the client gets authed and is exposed to the user. */

static bbuf_t
_send_const_encrypted_to_client_internal_protocol(
    svchan_server_client_t *client, uint32 num_bytes);

static int
_read_unauthed_client_packet(svchan_server_t *server,
    svchan_server_client_t *client);

static int
_read_authed_client_packet(svchan_server_t *server,
    svchan_server_client_t *client);

static int
_handle_svchan_msg_pub_key(svchan_server_t *server,
    svchan_server_client_t *client, svchan_msg_pub_key_t *s);

static int
_handle_svchan_msg_stream_header(svchan_server_t *server,
    svchan_server_client_t *client, svchan_msg_stream_header_t *s);

static int
_handle_svchan_cl_msg_login(svchan_server_t *server,
    svchan_server_client_t *client, svchan_cl_msg_login_t *s);

int
svchan_server_init(svchan_server_t *server,
    svchan_server_callbacks_t *callbacks, uint32 max_clients,
    uint32 client_in_buf_size, uint32 client_out_buf_size)
{
    muta_assert(callbacks->post_event);
    muta_assert(callbacks->on_authed);
    muta_assert(callbacks->on_read_packet);
    muta_assert(callbacks->on_disconnect);
    memset(server, 0, sizeof(*server));
    int err;
    if (thread_init(&server->thread))
        {err = 1; goto fail;}
    server->callbacks = *callbacks;
    fixed_pool_init(&server->clients, max_clients ? max_clients : 16);
    server->timers = emalloc(max_clients * sizeof(svchan_server_timer_t));
    mutex_init(&server->segpool_mutex);
    segpool_init(&server->segpool);
    server->client_in_buf_size  = client_in_buf_size;
    server->client_out_buf_size = client_out_buf_size;
    server->flush_clients       = emalloc(
        max_clients * sizeof(*server->flush_clients));
    return 0;
    fail:
        svchan_server_destroy(server);
        return err;
}

void
svchan_server_destroy(svchan_server_t *server)
{
}

int
svchan_server_start(svchan_server_t *server)
{
    muta_assert(darr_num(server->ports));
    muta_assert(darr_num(server->account_infos));
    int err;
    darr_reserve(server->listen_sockets, darr_num(server->ports));
    darr_clear(server->listen_sockets);
    server->num_flush_clients = 0;
    if (netpoll_init(&server->netpoll))
        {err = 1; goto fail;}
    for (uint32 i = 0; i < darr_num(server->ports); ++i)
    {
        socket_t s = net_tcp_ipv4_listen_sock(server->ports[i], 5);
        if (s == KSYS_INVALID_SOCKET)
        {
            DEBUG_PRINTFF("Bad socket, errno: %d.", errno);
            err = 2;
            goto fail;
        }
        netpoll_event_t event;
        event.events    = NETPOLL_READ;
        event.data.ptr  =
            &server->listen_sockets[darr_num(server->listen_sockets)];
        if (netpoll_add(&server->netpoll, s, &event))
        {
            close_socket(s);
            err = 3;
            goto fail;
        }
        darr_push(server->listen_sockets, s);
    }
    server->running = 1;
    if (thread_create(&server->thread, _main, server))
    {
        server->running = 0;
        err             = 4;
        goto fail;
    }
    return 0;
    fail:
        /* ................ */
        return err;
}

void
svchan_server_stop(svchan_server_t *server)
{
    interlocked_decrement_int32(&server->running);
    thread_join(&server->thread);
    netpoll_destroy(&server->netpoll);
}

void
svchan_server_add_account_info(svchan_server_t *server, char *name,
    char *password)
{
    muta_assert(name);
    muta_assert(password);
    svchan_account_info_t account_info;
    account_info.name       = dstr_create(name);
    account_info.password   = dstr_create(password);
    darr_push(server->account_infos, account_info);
}

void
svchan_server_add_port(svchan_server_t *server, uint16 port)
{
    muta_assert(!server->running);
    for (uint32 i = 0; i < darr_num(server->ports); ++i)
        if (server->ports[i] == port)
            return;
    darr_push(server->ports, port);
}

void
svchan_server_add_address(svchan_server_t *server, uint32 address)
{
    for (uint32 i = 0; i < darr_num(server->allowed_addresses); ++i)
        if (server->allowed_addresses[i] == address)
            return;
    darr_push(server->allowed_addresses, address);
}

void
svchan_server_handle_event(svchan_server_event_t *event)
{
    switch (event->type)
    {
    case SVCHAN_SERVER_EVENT_ACCEPT:
    {
        svchan_server_t *server = event->server;
        svchan_server_client_t *client = fixed_pool_new(&server->clients);
        if (!client)
            goto fail;
        client->socket          = event->accept.socket;
        client->user_data       = 0;
        client->state           = CLIENT_AWAIT_PUB_KEY;
        client->account_index   = 0xFFFFFFFF;
        client->address         = event->accept.address;
        client->timer_index     = server->num_timers++;
        client->id              = server->running_client_id++;
        client->in_buf.num      = 0;
        client->in_buf.max      = MUTA_MTU;
        client->in_buf.memory   = emalloc(client->in_buf.max);
        client->out_buf.num     = 0;
        client->flush_index     = 0xFFFFFFFF;
        client->server          = server;
        svchan_server_timer_t *timer = &server->timers[client->timer_index];
        timer->client_index     = fixed_pool_index(&server->clients, client);
        timer->time_idle        = 0;
        netpoll_event_t np_event;
        np_event.events     = NETPOLL_READ;
        np_event.data.ptr   = client;
        if (netpoll_add(&server->netpoll, client->socket, &np_event))
            _disconnect_client(server, client, SVCHAN_SERVER_DC_INTERNAL_ERROR);
        DEBUG_PRINTFF("Accepted a connection.\n");
        break;
        fail:
            close_socket(event->accept.socket);
    }
        break;
    case SVCHAN_SERVER_EVENT_READ_CLIENT:
    {
        svchan_server_t *server = event->server;
        svchan_server_client_t *client =
            &server->clients.all[event->read_client.client_index];
        /*-- Client disconnected or client slot was reused --*/
        if (client->state == CLIENT_DISCONNECTED ||
            client->id != event->read_client.client_id)
        {
            if (event->read_client.num_bytes > 0)
                goto free_memory;
            break;
        }
        if (event->read_client.num_bytes <= 0)
        {
            _disconnect_client(server, client,
                SVCHAN_SERVER_DC_CONNECTION_LOST);
            break;
        }
        /* Process message: first copy to client's temp buffer, then read
         * contents. */
        uint32 num_copied = 0;
        while (num_copied < (uint32)event->read_client.num_bytes)
        {
            muta_assert(client->in_buf.max - client->in_buf.num > 0);
            uint32 num_to_copy = MIN(client->in_buf.max - client->in_buf.num,
                (uint32)event->read_client.num_bytes - num_copied);
            memcpy(client->in_buf.memory + client->in_buf.num,
                event->read_client.memory + num_copied, num_to_copy);
            client->in_buf.num += num_to_copy;
            int r;
            if (client->state == CLIENT_AUTHED)
                r = _read_authed_client_packet(server, client);
            else
                r = _read_unauthed_client_packet(server, client);
            /* Kick if the packet was bad or the client's buffer is full. */
            if (r || client->in_buf.num == client->in_buf.max)
            {
                _disconnect_client(server, client,
                    SVCHAN_SERVER_DC_PROTOCOL_ERROR);
                break;
            }
            num_copied += num_to_copy;
        }
        svchan_server_timer_t *timer = &server->timers[client->timer_index];
        timer->time_idle = 0;
        free_memory:
            mutex_lock(&server->segpool_mutex);
            segpool_free(&server->segpool, event->read_client.memory);
            mutex_unlock(&server->segpool_mutex);
    }
        break;
    case SVCHAN_SERVER_EVENT_CHECK_TIMEOUTS:
        _check_timeouts(event->server, event->check_timeouts.time_diff);
        break;
    default:
        muta_assert(0);
    }
}

void
svchan_server_flush(svchan_server_t *server)
{
    if (!server->running)
        return;
    for (uint32 i = 0; i < server->num_flush_clients; ++i)
    {
        top:;
        svchan_server_client_t *client =
            &server->clients.all[server->flush_clients[i]];
        while (client->out_buf.num)
        {
            int num_to_send = MIN(MUTA_MTU, client->out_buf.num);
            if (net_send_all(client->socket, client->out_buf.memory,
                num_to_send) <= 0)
            {
                _disconnect_client(server, client,
                    SVCHAN_SERVER_DC_CONNECTION_LOST);
                if (i < server->num_flush_clients)
                    goto top;
                else
                    break;
            }
            memmove(client->out_buf.memory, client->out_buf.memory + num_to_send,
                client->out_buf.num - num_to_send);
            client->out_buf.num -= num_to_send;
        }
        client->flush_index = 0xFFFFFFFF;
    }
    server->num_flush_clients = 0;
}

void
svchan_server_client_set_user_data(svchan_server_client_t *client,
    void *user_data)
    {client->user_data = user_data;}

void *
svchan_server_client_get_user_data(svchan_server_client_t *client)
    {return client->user_data;}

cryptchan_t *
svchan_server_client_get_cryptchan(svchan_server_client_t *client)
    {return &client->cryptchan;}

bbuf_t
svchan_server_client_send(svchan_server_client_t *client, uint32 num_bytes)
{
    muta_assert(client->state == CLIENT_AUTHED);
    bbuf_t ret = {0};
    if (_flush_client_for_space(client, num_bytes))
    {
        _disconnect_client(client->server, client,
            SVCHAN_SERVER_DC_CONNECTION_LOST);
        return ret;
    }
    BBUF_INIT(&ret, client->out_buf.memory + client->out_buf.num, num_bytes);
    client->out_buf.num += num_bytes;
    _add_client_to_flush_clients(client->server, client);
    return ret;
}

bbuf_t
svchan_server_client_send_const_encrypted(svchan_server_client_t *client,
    uint32 num_bytes)
{
    return svchan_server_client_send(client, CRYPT_MSG_ADDITIONAL_BYTES +
        num_bytes);
}

bbuf_t
svchan_server_client_send_var_encrypted(svchan_server_client_t *client,
    uint32 num_bytes)
{
    return svchan_server_client_send(client, sizeof(msg_sz_t) +
        CRYPT_MSG_ADDITIONAL_BYTES + num_bytes);
}

void
svchan_server_client_disconnect(svchan_server_client_t *client)
    {_disconnect_client(client->server, client, SVCHAN_SERVER_DC_MANUAL);}

static thread_ret_t
_main(void *args)
{
    uint64 last_checked_timers = get_program_ticks_ms();
    svchan_server_t *server = args;
    netpoll_event_t events[64];
    uint8           buf[MUTA_MTU];
    while (interlocked_compare_exchange_int32(&server->running, -1, -1))
    {
        int num_events = netpoll_wait(&server->netpoll, events, 64,
            TIMEOUT_CHECK_FREQUENCY);
        for (int i = 0; i < num_events; ++i)
        {
            if (!(events[i].events & (NETPOLL_READ | NETPOLL_HUP)))
                continue;
            /*-- Listen socket --*/
            if (events[i].data.ptr >= (void*)server->listen_sockets &&
                events[i].data.ptr < (void*)(server->listen_sockets +
                darr_num(server->listen_sockets)))
            {
                addr_t      address;
                socket_t    socket = net_accept(*(socket_t*)events[i].data.ptr,
                    &address);
                if (socket == KSYS_INVALID_SOCKET)
                    continue;
                if (make_socket_non_block(socket))
                {
                    close_socket(socket);
                    continue;
                }
                svchan_server_event_t event;
                event.type              = SVCHAN_SERVER_EVENT_ACCEPT;
                event.server            = server;
                event.accept.socket     = socket;
                event.accept.address    = address.ip;
                event.accept.listen_socket_index =
                    (uint32)((socket_t*)events[i].data.ptr -
                        server->listen_sockets);
                server->callbacks.post_event(&event);
            } else
            /*-- Client sent something --*/
            {
                svchan_server_client_t *client = events[i].data.ptr;
                int num_bytes = net_recv(client->socket, buf, sizeof(buf));
                svchan_server_event_t event;
                event.type      = SVCHAN_SERVER_EVENT_READ_CLIENT;
                event.server    = server;
                event.read_client.client_index = fixed_pool_index(
                    &server->clients, client);
                event.read_client.client_id = client->id;
                event.read_client.num_bytes = num_bytes;
                if (num_bytes > 0)
                {
                    mutex_lock(&server->segpool_mutex);
                    event.read_client.memory = segpool_malloc(&server->segpool,
                        num_bytes);
                    mutex_unlock(&server->segpool_mutex);
                    memcpy(event.read_client.memory, buf, num_bytes);
                }
                server->callbacks.post_event(&event);
            }
        }
        /* Timeout check. If enough time has passed, post a timeout check
         * event. The check will be executed later in svchan_handle_event(). */
        uint64 time_now     = get_program_ticks_ms();
        uint64 time_diff    = time_now - last_checked_timers;
        if (time_diff < TIMEOUT_CHECK_FREQUENCY)
            continue;
        last_checked_timers = time_now;
        svchan_server_event_t event;
        event.type                      = SVCHAN_SERVER_EVENT_CHECK_TIMEOUTS;
        event.server                    = server;
        event.check_timeouts.time_diff  = time_diff;
        server->callbacks.post_event(&event);
    }
    return 0;
}

static bool32
_name_legal(const char *name, uint32 len)
{
    if (len > MAX_SVCHAN_NAME_LEN)
        return 0;
    for (uint32 i = 0; i < len; ++i)
        if (!isascii(name[i]))
            return 0;
    return 1;
}

static bool32
_password_legal(const char *password, uint32 len)
{
    if (len > MAX_SVCHAN_PASSWORD_LEN)
        return 0;
    for (uint32 i = 0; i < len; ++i)
    {
        if (!isascii(password[i]))
            return 0;
        if (iscntrl(password[i]))
            return 0;
    }
    return 1;
}

static void
_check_timeouts(svchan_server_t *server, uint64 time_diff)
{
    for (uint32 i = 0; i < server->num_timers; ++i)
    {
        top:
        server->timers[i].time_idle += time_diff;
        if (server->timers[i].time_idle < TIMEOUT)
            continue;
        _disconnect_client(server,
            &server->clients.all[server->timers[i].client_index],
            SVCHAN_SERVER_DC_TIMEOUT);
        DEBUG_PRINTFF("svchan server client timed out.\n");
        if (i < server->num_timers)
            goto top;
    }
}

static void
_disconnect_client(svchan_server_t *server, svchan_server_client_t *client,
    int reason)
{
    muta_assert(client->state != CLIENT_DISCONNECTED);
    if (client->state == CLIENT_AUTHED)
        server->callbacks.on_disconnect(client, reason);
    netpoll_del(&server->netpoll, client->socket);
    close_socket(client->socket);
    server->timers[client->timer_index] = server->timers[--server->num_timers];
    uint32 other_index = server->timers[client->timer_index].client_index;
    server->clients.all[other_index].timer_index = client->timer_index;
    client->state = CLIENT_DISCONNECTED;
    free(client->in_buf.memory);
    if (client->flush_index != 0xFFFFFFFF)
    {
        /*-- Swap last flush client with this one. --*/
        muta_assert(server->num_flush_clients);
        server->flush_clients[client->flush_index] =
            server->flush_clients[--server->num_flush_clients];
        uint32 index = server->flush_clients[client->flush_index];
        server->clients.all[index].flush_index = client->flush_index;
    }
    fixed_pool_free(&server->clients, client);
    DEBUG_PRINTFF("Disconnected svchan server client!\n");
}

static void
_add_client_to_flush_clients(svchan_server_t *server,
    svchan_server_client_t *client)
{
    if (client->flush_index != 0xFFFFFFFF)
        return;
    client->flush_index = server->num_flush_clients++;
    server->flush_clients[client->flush_index] = fixed_pool_index(
        &server->clients, client);
}

static int
_flush_client_for_space(svchan_server_client_t *client, uint32 num_bytes)
{
    int num_sent = 0;
    while (CLIENT_OUT_BUF_SZ - (client->out_buf.num - num_sent) < num_bytes)
    {
        int num_to_send = MIN(MUTA_MTU, client->out_buf.num);
        if (net_send_all(client->socket, client->out_buf.memory + num_sent,
            num_to_send) <= 0)
            return 1;
        num_sent += num_to_send;
    }
    memmove(client->out_buf.memory, client->out_buf.memory + num_sent,
        client->out_buf.num - num_sent);
    client->out_buf.num -= num_sent;
    return 0;
}

static bbuf_t
_send_to_client_internal_protocol(svchan_server_client_t *client,
    uint32 num_bytes)
{
    bbuf_t ret = {0};
    num_bytes += SVCHAN_MSGTSZ;
    if (_flush_client_for_space(client, num_bytes))
        return ret;
    BBUF_INIT(&ret, client->out_buf.memory + client->out_buf.num, num_bytes);
    client->out_buf.num += num_bytes;
    _add_client_to_flush_clients(client->server, client);
    return ret;
}

static bbuf_t
_send_const_encrypted_to_client_internal_protocol(
    svchan_server_client_t *client, uint32 num_bytes)
{
    return _send_to_client_internal_protocol(client,
        CRYPT_MSG_ADDITIONAL_BYTES + num_bytes);
}

static int
_read_unauthed_client_packet(svchan_server_t *server,
    svchan_server_client_t *client)
{
    bbuf_t bb = BBUF_INITIALIZER(client->in_buf.memory, client->in_buf.num);
    svchan_msg_type_t   msg_type;
    int                 dc          = 0;
    int                 incomplete  = 0;
    while (BBUF_FREE_SPACE(&bb) >= SVCHAN_MSGTSZ)
    {
        BBUF_READ(&bb, &msg_type);
        switch (msg_type)
        {
        case SVCHAN_MSG_PUB_KEY:
        {
            DEBUG_PUTS("SVCHAN_MSG_PUB_KEY");
            svchan_msg_pub_key_t s;
            incomplete = svchan_msg_pub_key_read(&bb, &s);
            if (!incomplete)
                dc = _handle_svchan_msg_pub_key(server, client, &s);
        }
            break;
        case SVCHAN_MSG_STREAM_HEADER:
        {
            DEBUG_PUTS("SVCHAN_MSG_STREAM_HEADER");
            svchan_msg_stream_header_t s;
            incomplete = svchan_msg_stream_header_read(&bb, &s);
            if (!incomplete)
                dc = _handle_svchan_msg_stream_header(server, client, &s);
        }
            break;
        case SVCHAN_CL_MSG_LOGIN:
        {
            DEBUG_PUTS("SVCHAN_CL_MSG_LOGIN");
            svchan_cl_msg_login_t s;
            incomplete = svchan_cl_msg_login_read_var_encrypted(&bb,
                &client->cryptchan, &s);
            if (!incomplete)
                dc = _handle_svchan_cl_msg_login(server, client, &s);
        }
            break;
        default:
            dc = 1;
            break;
        }
        if (dc || incomplete)
            break;
    }
    if (dc || incomplete < 0)
    {
        DEBUG_PRINTFF("(svchan_server) Bad msg! msg_type: %u, dc: %d, "
            "incomplete: %d\n", msg_type, dc, incomplete);
        return 1;
    }
    uint32 free_space = BBUF_FREE_SPACE(&bb);
    memmove(client->in_buf.memory,
        client->in_buf.memory + client->in_buf.num - free_space, free_space);
    client->in_buf.num = free_space;
    return 0;
}

static int
_read_authed_client_packet(svchan_server_t *server,
    svchan_server_client_t *client)
{
    int num_left = server->callbacks.on_read_packet(client,
        client->in_buf.memory, client->in_buf.num);
    if (num_left < 0)
        return 1;
    muta_assert((uint32)num_left <= client->in_buf.num);
    memmove(client->in_buf.memory,
        client->in_buf.memory + client->in_buf.num - (uint32)num_left,
        num_left);
    client->in_buf.num = (uint32)num_left;
    return 0;
}

static int
_handle_svchan_msg_pub_key(svchan_server_t *server,
    svchan_server_client_t *client, svchan_msg_pub_key_t *s)
{
    if (client->state != CLIENT_AWAIT_PUB_KEY)
        return 1;
    svchan_msg_pub_key_t        key_msg;
    svchan_msg_stream_header_t  header_msg;
    if (cryptchan_init(&client->cryptchan, key_msg.key))
        return 2;
    if (cryptchan_sv_store_pub_key(&client->cryptchan, s->key,
        header_msg.header))
        return 3;
    bbuf_t bb = _send_to_client_internal_protocol(client,
        SVCHAN_MSG_PUB_KEY_SZ);
    if (!bb.max_bytes)
        return 4;
    svchan_msg_pub_key_write(&bb, &key_msg);
    bb = _send_to_client_internal_protocol(client,
        SVCHAN_MSG_STREAM_HEADER_SZ);
    if (!bb.max_bytes)
        return 5;
    svchan_msg_stream_header_write(&bb, &header_msg);
    client->state = CLIENT_AWAIT_STREAM_HEADER;
    return 0;
}

static int
_handle_svchan_msg_stream_header(svchan_server_t *server,
    svchan_server_client_t *client, svchan_msg_stream_header_t *s)
{
    if (client->state != CLIENT_AWAIT_STREAM_HEADER)
        return 1;
    if (cryptchan_store_stream_header(&client->cryptchan, s->header))
        return 2;
    client->state = CLIENT_AWAIT_LOGIN;
    return 0;
}

static int
_handle_svchan_cl_msg_login(svchan_server_t *server,
    svchan_server_client_t *client, svchan_cl_msg_login_t *s)
{
    muta_assert(s->name_len <= MAX_SVCHAN_NAME_LEN);
    muta_assert(s->password_len <= MAX_SVCHAN_PASSWORD_LEN);
    if (!_name_legal(s->name, s->name_len))
        return 1;
    if (!_password_legal(s->password, s->password_len))
        return 2;
    char name[MAX_SVCHAN_NAME_LEN + 1];
    char password[MAX_SVCHAN_PASSWORD_LEN + 1];
    memcpy(name, s->name, s->name_len);
    memcpy(password, s->password, s->password_len);
    name[s->name_len]           = 0;
    password[s->password_len]   = 0;
    /*-- Find account --*/
    uint32 account_index = 0xFFFFFFFF;
    for (uint32 i = 0; i < darr_num(server->account_infos); ++i)
    {
        if (!streq(server->account_infos[i].name, name) ||
            !streq(server->account_infos[i].password, password))
            continue;
        account_index = i;
        break;
    }
    if (account_index == 0xFFFFFFFF)
        return 3;
    client->account_index   = account_index;
    client->state           = CLIENT_AUTHED;
    bbuf_t bb = _send_const_encrypted_to_client_internal_protocol(client,
        SVCHAN_SV_MSG_LOGIN_RESULT_SZ);
    if (!bb.max_bytes)
        return 4;
    svchan_sv_msg_login_result_t result_msg;
    result_msg.result = 0;
    int r = svchan_sv_msg_login_result_write_const_encrypted(&bb,
        &client->cryptchan, &result_msg);
    muta_assert(!r);
    if (server->callbacks.on_authed(client))
        return 4;
    DEBUG_PRINTFF("Authed client!\n");
    return 0;
}
