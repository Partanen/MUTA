#include "types.h"

bool32
emote_is_id_valid(emote_id_t id);

emote_id_t
emote_invalid_id();

uint
emote_num_emotes();

emote_id_t
emote_get_id_by_name(const char *emote);

const char *
emote_get_name_by_id(emote_id_t id);

const char *
emote_get_targeted_str(emote_id_t emote);

const char *
emote_get_non_targeted_str(emote_id_t emote);

const char *
emote_get_me_targeted_str(emote_id_t emote);

const char *
emote_get_me_non_targeted_str(emote_id_t emote);
