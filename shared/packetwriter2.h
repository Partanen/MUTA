#ifndef MUTA_PACKETWRITER2_H
#define MUTA_PACKETWRITER2_H

#include <string.h>
#include "types.h"
#include "crypt.h"
#include "common_utils.h"

typedef uint16 pw2_packet_size_t;

#if MUTA_ENDIANNESS == MUTA_LIL_ENDIAN
    #define PW2_DEFINE(type, bswap_callback, utype) \
        static inline void \
        pw2_write_##type(uint8 **dst, type val) \
        { \
            memcpy(*dst, &val, sizeof(val)); \
            *dst += sizeof(type); \
        } \
        static inline void \
        pw2_read_##type(uint8 **src, type *ret_val) \
        { \
            memcpy(ret_val, *src, sizeof(type)); \
            *src += sizeof(type); \
        }
#elif MUTA_ENDIANNESS == MUTA_BIG_ENDIAN
    #define PW2_BSWAP_NONE(v) (v)
    #define PW2_BSWAP16(v) ((((v) >> 8) & 0xffu) | (((v) & 0xffu) << 8))
    #define PW2_BSWAP32(v) \
        ((((v) & 0xff000000u) >> 24) | (((v) & 0x00ff0000u) >>  8) | \
        (((v) & 0x0000ff00u) << 8)  | (((v) & 0x000000ffu) << 24))
    #define PW2_BSWAP64(v) \
        ((((v) & 0xff00000000000000ull) >> 56) | \
        (((v) & 0x00ff000000000000ull) >> 40) | \
        (((v) & 0x0000ff0000000000ull) >> 24) | \
        (((v) & 0x000000ff00000000ull) >> 8)  | \
        (((v) & 0x00000000ff000000ull) << 8)  | \
        (((v) & 0x0000000000ff0000ull) << 24) | \
        (((v) & 0x000000000000ff00ull) << 40) | \
        (((v) & 0x00000000000000ffull) << 56))
    #define PW2_DEFINE(type, bswap_callback, utype) \
        static inline void \
        pw2_write_##type(uint8 **dst, type val) \
        { \
            utype tmp; \
            memcpy(&tmp, &val, sizeof(type)); \
            tmp = bswap_callback(tmp); \
            memcpy(*dst, &tmp, sizeof(val)); \
            *dst += sizeof(type); \
        } \
        static inline void \
        pw2_read_##type(uint8 **src, type *ret_val) \
        { \
            utype tmp; \
            memcpy(&tmp, *src, sizeof(type)); \
            tmp = bswap_callback(tmp); \
            memcpy(ret_val, &tmp, sizeof(type)); \
            *src += sizeof(type); \
        }
#else
    #error "Muta endianness undefined."
#endif

PW2_DEFINE(int8, PW2_BSWAP_NONE, uint8);
PW2_DEFINE(uint8, PW2_BSWAP_NONE, uint8);
PW2_DEFINE(int16, PW2_BSWAP16, uint16);
PW2_DEFINE(uint16, PW2_BSWAP16, uint16);
PW2_DEFINE(int32, PW2_BSWAP32, uint32);
PW2_DEFINE(uint32, PW2_BSWAP32, uint32);
PW2_DEFINE(int64, PW2_BSWAP64, uint64);
PW2_DEFINE(uint64, PW2_BSWAP64, uint64);
PW2_DEFINE(f32, PW2_BSWAP32, uint32);
PW2_DEFINE(f64, PW2_BSWAP64, uint64);

#define pw2_write_packet_size   pw2_write_uint16
#define pw2_read_packet_size    pw2_read_uint16

static inline void
pw2_write_char(uint8 **dst, char val)
    {pw2_write_int8(dst, (int8)val);}

static inline void
pw2_read_char(uint8 **src, char *val)
{
    int8 tmp;
    pw2_read_int8(src, &tmp);
    *val = (char)tmp;
}

#endif /* MUTA_PACKETWRITER2_H */
