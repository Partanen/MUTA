#ifndef MUTA_CRYPT_H
#define MUTA_CRYPT_H

#include <sodium.h>
#include "types.h"

typedef crypto_secretstream_xchacha20poly1305_state cryptstream_t;
typedef struct cryptchan_t                          cryptchan_t;

#define CRYPTCHAN_PUB_KEY_SZ        crypto_box_PUBLICKEYBYTES
#define CRYPTCHAN_SEC_KEY_SZ        crypto_box_SECRETKEYBYTES
#define CRYPTCHAN_HANDSHAKE_SZ      CRYPTCHAN_PUB_KEY_SZ
#define CRYPTCHAN_SESSION_KEY_SZ    crypto_kx_SESSIONKEYBYTES
#define CRYPT_MSG_ADDITIONAL_BYTES  crypto_secretstream_xchacha20poly1305_ABYTES
#define CRYPTCHAN_STREAM_HEADER_SZ \
    crypto_secretstream_xchacha20poly1305_HEADERBYTES
#define CRYPT_PW_HASH_SZ            crypto_pwhash_STRBYTES
#define CRYPTSTREAM_KEY_SZ          crypto_stream_chacha20_ietf_KEYBYTES
#define CRYPTSTREAM_NONCE_SZ        crypto_stream_chacha20_ietf_NONCEBYTES

typedef uint8 cryptchan_stream_header_t[CRYPTCHAN_STREAM_HEADER_SZ];
typedef uint8 crypt_pk_t[CRYPTCHAN_PUB_KEY_SZ];
typedef uint8 crypt_sk_t[CRYPTCHAN_PUB_KEY_SZ];

enum crypt_pw_hash_strength_t
{
    CRYPT_PW_STRENGTH_LOW,
    CRYPT_PW_STRENGTH_MEDIUM,
    CRYPT_PW_STRENGTH_STRONG
};

struct cryptchan_t
{
    uint8           flags;
    uint8           wx[CRYPTCHAN_SESSION_KEY_SZ]; /* Write key */
    cryptstream_t   wstream;
    uint8           rx[CRYPTCHAN_SESSION_KEY_SZ]; /* Read  key */
    cryptstream_t   rstream;
    uint8           sk[CRYPTCHAN_SEC_KEY_SZ];
    uint8           pk[CRYPTCHAN_PUB_KEY_SZ];
};

#define crypt_init() sodium_init()
/* Call before calling any of the following functions */

int
cryptchan_init(cryptchan_t *c, uint8 *ret_pub_key);
/* The public key to be sent to the other party will be returned in ret_pub_key
 * if return value equals 0. */

void
cryptchan_init_from_keys(cryptchan_t *c, uint8 *wx, uint8 *rx, uint8 *sk,
    uint8 *pk, uint8 *read_key, uint8 *read_nonce, uint8 *write_key,
    uint8 *write_nonce);

#define cryptchan_clear(c) ((void)((c)->flags = 0))

bool32
cryptchan_is_encrypted(cryptchan_t *c);

bool32
cryptchan_is_initialized(cryptchan_t *c);

int
cryptchan_cl_store_pub_key(cryptchan_t *c,
    const uint8 sv_pub_key[CRYPTCHAN_PUB_KEY_SZ],
    uint8 ret_header[CRYPTCHAN_STREAM_HEADER_SZ]);
/* Return value will be > 0 if in progress, 0 if successful or -1 if
 * handshaking failed or the state of the channel was wrong. The header to be
 * sent to the other party will be returned in ret_header */

int
cryptchan_sv_store_pub_key(cryptchan_t *c,
    const uint8 cl_pub_key[CRYPTCHAN_PUB_KEY_SZ],
    uint8 ret_header[CRYPTCHAN_STREAM_HEADER_SZ]);
/* Return values same as above */

int
cryptchan_store_stream_header(cryptchan_t *c,
    const uint8 header[CRYPTCHAN_STREAM_HEADER_SZ]);
/* When the stream header has been received, assuming the public key was
 * received first, the channel is ready to use. */

/* encrypt() and decrypt() may be called after the handshaking is finished. The
 * dst can be the same as src. */
int
cryptchan_encrypt(cryptchan_t *c, uint8 *dst, const uint8 *src, int src_len);

int
cryptchan_decrypt(cryptchan_t *c, uint8 *dst, const uint8 *src, int src_len);
/* Returns > 0 if this was the last part of the stream, 0 if otherwise
 * successful or < 0 if there were errors. */

int
crypt_storable_pw_hash(const char *pw, int pw_len, char *ret_hash, int strength);

static inline bool32
crypt_test_stored_pw_hash(const char *pw_str, int pw_len,
    const char *stored_pw);

static inline int
crypt_hash(const uint8 *data, int data_len, const uint8 *key, int key_len,
    uint8 *ret_hash, int hash_len);

#define crypt_rand_bytes(buf, buf_len) randombytes_buf((buf), (buf_len))



static inline bool32
crypt_test_stored_pw_hash(const char *pw_str, int pw_len, const char *stored_pw)
    {return crypto_pwhash_str_verify(stored_pw, pw_str, pw_len) == 0;}

static inline int
crypt_hash(const uint8 *data, int data_len, const uint8 *key, int key_len,
    uint8 *ret_hash, int hash_len)
{
    return crypto_generichash(ret_hash, hash_len, data, data_len, key, key_len);
}

#endif /* MUTA_CRYPT_H */
