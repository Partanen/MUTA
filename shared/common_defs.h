#ifndef MUTA_COMMON_DEFS_H
#define MUTA_COMMON_DEFS_H

#include "types.h"

#define MUTA_VERSION_STR            "0.8.0"
#define MUTA_VERSION_ARRAY          {0, 8, 0}

#define MUTA_MTU                    1472
#define MIN_CHARACTER_NAME_LEN      2
#define MAX_CHARACTER_NAME_LEN      16
#define MIN_PW_LEN                  6
#define MAX_PW_LEN                  64
#define MIN_ACC_NAME_LEN            2
#define MAX_ACC_NAME_LEN            16
#define DEFAULT_MAX_PF_REQUEST_FREQ 250
#define DEFAULT_LOGIN_PORT          3489
#define DEFAULT_CLIENT_PORT         3490
#define MAX_CHARACTERS_PER_ACC      8
#define MAX_CHAT_MSG_LEN            255
#define MAX_RACE_NAME_LEN           16
#define CLIENT_LOGIN_PROOF_KEY_LEN  128
#define MIN_SHARD_NAME_LEN          1
#define MAX_SHARD_NAME_LEN          16
#define AUTH_TOKEN_SZ               64
#define MAX_DISPLAY_NAME_LEN        64
#define MAX_BAGS_PER_PLAYER         5
#define MAX_BAG_WIDTH               32
#define MAX_BAG_HEIGHT              32
#define MAX_BAG_SIZE                (MAX_BAG_WIDTH * MAX_BAG_HEIGHT)
#define MAX_BAG_X                   (MAX_BAG_WIDTH - 1)
#define MAX_BAG_Y                   (MAX_BAG_HEIGHT - 1)
#define MAX_PLAYER_BAG_INDEX        (MAX_BAGS_PER_PLAYER - 1)
#define INVALID_ISODIR              NUM_ISODIRS
#define HIGHEST_ISODIR              (NUM_ISODIRS - 1)
#define MAP_CHUNK_W                 256 /* X/Y width */
#define MAP_CHUNK_T                 256 /* Height */
#define MAP_CHUNK_SIZE_IN_TILES     (MAP_CHUNK_W * MAP_CHUNK_W * MAP_CHUNK_T)
#define MAX_EQUIPMENT_SLOT          (NUM_EQUIPMENTS_SLOTS - 1)
#define EQUIPMENT_SLOT_NONE         0xFF
#define MAX_ITEM_THROW_DISTANCE     5
#define MAX_ITEM_THROW_HEIGHT       5
#define MAX_COMMON_ENTITY_TYPE      (NUM_COMMON_ENTITY_TYPES - 1)
#define MAX_PLAYER_ABILITIES        32

enum common_error_codes
{
    MUTA_ERR_OUT_OF_MEMORY = 112,
    MUTA_ERR_OPERATING_SYSTEM
};

enum character_creation_fail_reasons
{
    CREATE_CHARACTER_FAIL_DB_BUSY = 1,
    CREATE_CHARACTER_FAIL_EXISTS,
    CREATE_CHARACTER_FAIL_UNKNOWN
};

enum shard_select_error
{
    SHARD_SELECT_ERROR_UNKNOWN = 1,
    SHARD_SELECT_ERROR_FULL
};

enum enter_world_error
{
    ENTER_WORLD_ERROR_INSTANCE_DOWN = 1,
    ENTER_WORLD_ERROR_INSTANCE_PART_DOWN,
    ENTER_WORLD_ERROR_FULL,
    ENTER_WORLD_ERROR_INTERNAL,
    ENTER_WORLD_ERROR_UNKNOWN
};
/* Codes sent to player when they attempt to select a character. */

enum account_login_result_code
{
    ACCOUNT_LOGIN_OK = 0,
    ACCOUNT_LOGIN_WRONG_INFO,
    ACCOUNT_LOGIN_ALREADY_IN,
    ACCOUNT_LOGIN_INTERNAL_ERROR
};
/* Codes sent to player upon login attempt. */

enum iso_dir
{
    ISODIR_NORTH = 0,
    ISODIR_NORTH_EAST,
    ISODIR_EAST,
    ISODIR_SOUTH_EAST,
    ISODIR_SOUTH,
    ISODIR_SOUTH_WEST,
    ISODIR_WEST,
    ISODIR_NORTH_WEST,
    NUM_ISODIRS
};
/* isometric directions
 * West  North
 *    ^  ^
 *     \/
 *     /\
 *    v  v
 * South East */

enum common_entity_type
{
    COMMON_ENTITY_PLAYER = 0,
    COMMON_ENTITY_CREATURE,
    COMMON_ENTITY_DYNAMIC_OBJECT,
    COMMON_ENTITY_STATIC_OBJECT,
    NUM_COMMON_ENTITY_TYPES
};

enum equipment_slot
{
    EQUIPMENT_SLOT_BAG1 = 0,
    EQUIPMENT_SLOT_BAG2,
    EQUIPMENT_SLOT_BAG3,
    EQUIPMENT_SLOT_BAG4,
    EQUIPMENT_SLOT_BAG5,
    NUM_EQUIPMENTS_SLOTS
};

enum bag_slot
{
    BAG_SLOT1 = EQUIPMENT_SLOT_BAG1,
    BAG_SLOT2 = EQUIPMENT_SLOT_BAG2,
    BAG_SLOT3 = EQUIPMENT_SLOT_BAG3,
    BAG_SLOT4 = EQUIPMENT_SLOT_BAG4,
    BAG_SLOT5 = EQUIPMENT_SLOT_BAG5,
    MAX_BAG_SLOT_ID
};

enum chat_msg_type
{
    CHAT_MSG_GLOBAL_BROADCAST,
    CHAT_MSG_SYSTEM
};

enum common_ability_charge_result
{
    COMMON_ABILITY_CHARGE_RESULT_SUCCESS,
    COMMON_ABILITY_CHARGE_RESULT_TARGET_LOST,
    COMMON_ABILITY_CHARGE_RESULT_SCRIPT_FAILED,
    COMMON_ABILITY_CHARGE_RESULT_CANCELLED,
    NUM_COMMON_ABILIY_CHARGE_RESULTS
};

#define MAX_COMMON_ABILITY_CHARGE_RESULT (NUM_COMMON_ABILIY_CHARGE_RESULTS - 1)

static inline const char *
character_creation_fail_to_str(int err);

static inline bool32
iso_dir_is_diagonal(enum iso_dir dir);

static inline int
iso_dir_to_vec2(int dir, int *ret_x, int *ret_y);

static inline const char *
iso_dir_to_uncapitalized_str(int dir);

static inline const char *
iso_dir_to_capitalized_str(int dir);

static inline const char *
iso_dir_to_proper_str(int dir);
/* Capitalize first letter. */

static inline int
vec2_to_iso_dir(int dx, int dy);

static inline const char *
character_creation_fail_to_str(int err)
{
    if (err == 0)
        return "no error";
    if (err == CREATE_CHARACTER_FAIL_DB_BUSY)
        return "database is busy";
    if (err == CREATE_CHARACTER_FAIL_EXISTS)
        return "name taken";
    return "unknown error";
}

static inline bool32
iso_dir_is_diagonal(enum iso_dir dir)
{
    switch (dir)
    {
    case ISODIR_NORTH_EAST:
    case ISODIR_NORTH_WEST:
    case ISODIR_SOUTH_EAST:
    case ISODIR_SOUTH_WEST:
        return 1;
    default:
        return 0;
    }
}

static inline int
iso_dir_to_vec2(int dir, int *ret_x, int *ret_y)
{
    switch (dir)
    {
    case ISODIR_NORTH:      *ret_x =  0; *ret_y = -1; break;
    case ISODIR_NORTH_EAST: *ret_x =  1; *ret_y = -1; break;
    case ISODIR_EAST:       *ret_x =  1; *ret_y =  0; break;
    case ISODIR_SOUTH_EAST: *ret_x =  1; *ret_y =  1; break;
    case ISODIR_SOUTH:      *ret_x =  0; *ret_y =  1; break;
    case ISODIR_SOUTH_WEST: *ret_x = -1; *ret_y =  1; break;
    case ISODIR_WEST:       *ret_x = -1; *ret_y =  0; break;
    case ISODIR_NORTH_WEST: *ret_x = -1; *ret_y = -1; break;
    default:                *ret_x =  0; *ret_y =  0; return 1;
    }
    return 0;
}

static inline const char *
iso_dir_to_uncapitalized_str(int dir)
{
    switch (dir)
    {
    case ISODIR_NORTH:
        return "north";
    case ISODIR_NORTH_EAST:
        return "northeast";
    case ISODIR_EAST:
        return "east";
    case ISODIR_SOUTH_EAST:
        return "southeast";
    case ISODIR_SOUTH:
        return "south";
    case ISODIR_SOUTH_WEST:
        return "southwest";
    case ISODIR_WEST:
        return "west";
    case ISODIR_NORTH_WEST:
        return "northwest";
    default:
        return "unknown";
    }
}

static inline const char *
iso_dir_to_capitalized_str(int dir)
{
    switch (dir)
    {
    case ISODIR_NORTH:
        return "NORTH";
    case ISODIR_NORTH_EAST:
        return "NORTHEAST";
    case ISODIR_EAST:
        return "EAST";
    case ISODIR_SOUTH_EAST:
        return "SOUTHEAST";
    case ISODIR_SOUTH:
        return "SOUTH";
    case ISODIR_SOUTH_WEST:
        return "SOUTHWEST";
    case ISODIR_WEST:
        return "WEST";
    case ISODIR_NORTH_WEST:
        return "NORTHWEST";
    default:
        return "UNKNOWN";
    }
}

static inline const char *
iso_dir_to_proper_str(int dir)
{
    switch (dir)
    {
    case ISODIR_NORTH:
        return "North";
    case ISODIR_NORTH_EAST:
        return "Northeast";
    case ISODIR_EAST:
        return "East";
    case ISODIR_SOUTH_EAST:
        return "Southeast";
    case ISODIR_SOUTH:
        return "South";
    case ISODIR_SOUTH_WEST:
        return "Southwest";
    case ISODIR_WEST:
        return "West";
    case ISODIR_NORTH_WEST:
        return "Northwest";
    default:
        return "Unknown";
    }
}

static inline int
vec2_to_iso_dir(int dx, int dy)
{
    static enum iso_dir table[9] =
    {
        /* x, y */
        ISODIR_NORTH_WEST, ISODIR_NORTH,   ISODIR_NORTH_EAST,
        ISODIR_WEST,       INVALID_ISODIR, ISODIR_EAST,
        ISODIR_SOUTH_WEST, ISODIR_SOUTH,   ISODIR_SOUTH_EAST
    };
    int fdx = dx;
    if (fdx < -1)
        fdx = -1;
    else if (fdx > 1)
        fdx = 1;
    fdx++;
    int fdy = dy;
    if (fdy < -1)
        fdy = -1;
    else if (fdy > 1)
        fdy = 1;
    fdy++;
    return table[fdy * 3 + fdx];
}

#endif /* MUTA_COMMON_DEFS_H */
