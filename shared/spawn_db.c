#include "spawn_db.h"
#include "entities.h"

typedef struct column_t column_t;

struct column_t
{
    const char          *name;
    enum mdb_col_type   type;
};

int
spawn_db_open(spawn_db_t *db, const char *file_path)
{
    if (mdb_open(&db->mdb, file_path))
    {
        printf("%s: failed to open spawns db from %s.\n", __func__, file_path);
        return 1;
    }
    column_t columns[NUM_SPAWN_DB_INDICES] =
    {
        {.name = "id",          .type = MDB_COL_UINT32},
        {.name = "tag",         .type = MDB_COL_STR},
        {.name = "spawn_type",  .type = MDB_COL_UINT8},
        {.name = "type_id",     .type = MDB_COL_UINT32},
        {.name = "x",           .type = MDB_COL_INT32},
        {.name = "y",           .type = MDB_COL_INT32},
        {.name = "z",           .type = MDB_COL_UINT8},
        {.name = "direction",   .type = MDB_COL_UINT8},
        {.name = "sex",         .type = MDB_COL_UINT8}
    };
    for (int i = 0; i < NUM_SPAWN_DB_INDICES; ++i)
    {
        int index = mdb_get_col_index(&db->mdb, columns[i].name);
        if (index < 0)
        {
            printf("%s: missing column %s in spawn db.\n", __func__,
                columns[i].name);
            goto fail;
        }
        if (mdb_get_col_type(&db->mdb, index) != columns[i].type)
        {
            printf("%s: bad type for column %s in spawn db.\n", __func__,
                columns[i].name);
            goto fail;
        }
        db->indices[i] = index;
    }
    return 0;
    fail:
        mdb_close(&db->mdb);
        return 1;
}

void
spawn_db_close(spawn_db_t *db)
    {mdb_close(&db->mdb);}

uint32
spawn_db_num_rows(spawn_db_t *db)
    {return mdb_num_rows(&db->mdb);}

mdb_row_t
spawn_db_get_row_by_index(spawn_db_t *db, uint32 index)
    {return mdb_get_row_by_index(&db->mdb, index);}

uint32
spawn_db_get_id(spawn_db_t *db, mdb_row_t row)
{
    return *(uint32*)mdb_get_field(&db->mdb, row,
        db->indices[SPAWN_DB_INDEX_ID]);
}

dchar *
spawn_db_get_tag(spawn_db_t *db, mdb_row_t row)
{
    return *(dchar**)mdb_get_field(&db->mdb, row,
        db->indices[SPAWN_DB_INDEX_TAG]);
}

enum spawn_db_spawn_type
spawn_db_get_spawn_type(spawn_db_t *db, mdb_row_t row)
{
    uint8 val = *(uint8*)mdb_get_field(&db->mdb, row,
        db->indices[SPAWN_DB_INDEX_SPAWN_TYPE]);
    if (val == 0)
        return SPAWN_DB_SPAWN_TYPE_DYNAMIC_OBJECT;
    muta_assert(val == 1);
    return SPAWN_DB_SPAWN_TYPE_CREATURE;
}

uint32
spawn_db_get_type_id(spawn_db_t *db, mdb_row_t row)
{
    return *(uint32*)mdb_get_field(&db->mdb, row,
        db->indices[SPAWN_DB_INDEX_TYPE_ID]);
}

enum iso_dir
spawn_db_get_direction(spawn_db_t *db, mdb_row_t row)
{
    uint8 val = *(uint8*)mdb_get_field(&db->mdb, row,
        db->indices[SPAWN_DB_INDEX_DIRECTION]);
    muta_assert(val >= 0 && val < NUM_ISODIRS);
    return (enum iso_dir)val;
}

int32_t
spawn_db_get_x(spawn_db_t *db, mdb_row_t row)
{
    return *(int32*)mdb_get_field(&db->mdb, row, db->indices[SPAWN_DB_INDEX_X]);
}

int32_t
spawn_db_get_y(spawn_db_t *db, mdb_row_t row)
{
    return *(int32*)mdb_get_field(&db->mdb, row, db->indices[SPAWN_DB_INDEX_Y]);
}

uint8
spawn_db_get_z(spawn_db_t *db, mdb_row_t row)
{
    return *(uint8*)mdb_get_field(&db->mdb, row, db->indices[SPAWN_DB_INDEX_Z]);
}

int
spawn_db_get_sex(spawn_db_t *db, mdb_row_t row)
{
    uint8 val = *(uint8*)mdb_get_field(&db->mdb, row,
        db->indices[SPAWN_DB_INDEX_SEX]);
    if (val == ENT_SEX_MALE)
        return ENT_SEX_MALE;
    return ENT_SEX_FEMALE;
}
