#ifndef MUTA_NETPOLL
#define MUTA_NETPOLL

#define NETPOLL_LOOPBACK_PORT 4343

#include "types.h"
#include "ksys.h"

typedef struct netpoll_t netpoll_t;

/*-- Linux --*/
#if defined(__linux__)
#include <sys/epoll.h>
typedef struct epoll_event netpoll_event_t;

enum netpoll_flag
{
    NETPOLL_READ    = EPOLLIN,
    NETPOLL_WRITE   = EPOLLOUT,
    NETPOLL_ET      = EPOLLET,
    NETPOLL_ERR     = EPOLLERR,
    NETPOLL_HUP     = (EPOLLHUP | EPOLLRDHUP)
};

struct netpoll_t
{
    int fd;
};

/*-- Windows --*/
#elif defined(_WIN32)
typedef struct netpoll_event_t          netpoll_event_t;
typedef struct netpoll_added_socket_t   netpoll_added_socket_t;

enum netpoll_flag
{
    NETPOLL_READ    = POLLIN,
    NETPOLL_WRITE   = POLLOUT,
    NETPOLL_ET      = 0,
    NETPOLL_ERR     = POLLERR,
    NETPOLL_HUP     = POLLHUP
};

union netpoll_data_t
{
    void    *ptr;
    int     fd;
    uint32  u32;
    uint64  u64;
};

struct netpoll_event_t
{
    uint32                  events;
    union netpoll_data_t    data;
};

struct netpoll_added_socket_t
{
    socket_t        socket;
    netpoll_event_t event;
};

struct netpoll_t
{
    WSAPOLLFD               *fds;           /* Dynamic array */
    union netpoll_data_t    *datas;         /* Dynamic array */
    netpoll_added_socket_t  *added;         /* Dynamic array */
    socket_t                *deleted;       /* Dynamic array */
    mutex_t                 add_mutex;
    mutex_t                 del_mutex;
    socket_t                loopback_read, loopback_write;
    uint32                  num_events;
    uint32                  index;
};

#endif /* __linux__, _WIN32 */

int
netpoll_init(netpoll_t *np);

void
netpoll_destroy(netpoll_t *np);
/* netpoll_destroy()
 * Destroy a netpoll. May not be called if any thread is currently inside
 * netpoll_wait() for the given netpoll instance. */

int
netpoll_wait(netpoll_t *np, netpoll_event_t *ret_events, int max_events,
    int timeout_ms);
/* netpoll_wait()
 * Wait for new events. Up to max_events are written into the array provided in
 * parameter ret_events.
 * If timeout_ms is -1, function will wait indefinitely. */

int
netpoll_add(netpoll_t *np, socket_t s, netpoll_event_t *e);
/* netpoll_add()
 * Register a given socket with a netpoll. Parameter e describes which events
 * are of interest, and provides a way to add socket-identifying user data. */

int
netpoll_del(netpoll_t *np, socket_t s);
/* netpoll_del()
 * Deregister a socket with a netpoll instance. */

#endif /* MUTA_NETPOLL */
