#ifndef MUTA_COMPILER_H
#define MUTA_COMPILER_H

#ifndef restrict
    #if defined(__restrict)
        #define restrict __restrict /* MSVC */
    #else
        #define restrict
    #endif
#endif

#ifdef __GNUC__
    #define CHECK_PRINTF_ARGS(string_index, first_to_check) \
        __attribute__((format(printf, string_index, first_to_check)))
#else
    #define CHECK_PRINTF_ARGS(string_index, first_to_check)
#endif

#endif /* MUTA_COMPILER_H */
