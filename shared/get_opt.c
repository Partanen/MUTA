#include <string.h>
#include "get_opt.h"

void
get_opt_context_init(get_opt_context_t *context)
{
    context->arg = 0;
    context->ind = 1;
}

int
get_opt(get_opt_context_t *context, int argc, char **argv, const char *opts)
{
    if (context->ind >= argc)
        return -1;
    if (argv[context->ind][0] != '-')
        return '?';
    int         opt         = argv[context->ind++][1];
    const char  *list_opt   = strchr(opts, opt);
    if (!list_opt)
        return '?';
    if (list_opt[1] != ':')
        return opt;
    if (context->ind >= argc)
        return '?';
    context->arg = argv[context->ind++];
    return opt;
}
