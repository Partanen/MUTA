#include "log.h"
#include "common_utils.h"
#define __STDC_WANT_LIB_EXT1__ 1
#include <time.h>

typedef struct log_event_t log_event_t;

struct log_tag_t
{
    char    *str;
    uint32  len;
};

struct log_event_t
{
    log_t   *log;
    char    *msg;
};

static int32        _running;
static event_buf_t  _event_buf;
static segpool_t    _segpool;
static mutex_t      _segpool_mutex;
static thread_t     _thread;

static void
_handle_events(log_event_t *events, int num);

static thread_ret_t
_main(void *args);

int
log_api_init(uint32 queue_size)
{
    int err;
    event_init(&_event_buf, sizeof(log_event_t), queue_size ? queue_size : 64);
    segpool_init(&_segpool);
    mutex_init(&_segpool_mutex);
    if (thread_init(&_thread))
        {err = 1; goto fail;}
    _running = 1;
    if (thread_create(&_thread, _main, 0))
    {
        _running    = 0;
        err         = 2;
        goto fail;
    }
    return 0;
    fail:
        log_api_destroy();
        return err;
}

void
log_api_destroy(void)
{
    if (_running)
    {
        interlocked_decrement_int32(&_running);
        thread_join(&_thread);
    }
    event_destroy(&_event_buf);
    segpool_destroy(&_segpool);
    mutex_destroy(&_segpool_mutex);
    thread_destroy(&_thread);
}

static dchar *
_create_log_file_path(const char *directory_path, const char *base_file_name)
{
    if (directory_path)
        directory_path = ".";
    if (!str_is_valid_file_name(directory_path))
    {
        DEBUG_PRINTFF("directory path is invalid.\n");
        return 0;
    }
    if (!str_is_valid_file_name(base_file_name))
    {
        DEBUG_PRINTFF("base file name is invalid.\n");
        return 0;
    }
    struct tm t;
    if (get_local_time(&t))
    {
        DEBUG_PRINTFF("Could not get local time.\n");
        return 0;
    }
    char buf[64];
    uint32 time_len = (uint32)strftime(buf, sizeof(buf), "%d-%b-%Y_%H-%M_", &t);
    if (!time_len)
        return 0;
    uint32 dir_len  = (uint32)strlen(directory_path);
    uint32 name_len = (uint32)strlen(base_file_name);
    /* The null terminator's size is already included in time_len. */
    dchar *path = dstr_create_empty(time_len + dir_len + name_len);
    dstr_append(&path, directory_path);
    dstr_append(&path, "/");
    dstr_append(&path, buf);
    dstr_append(&path, base_file_name);
    return path;
}

int
log_init(log_t *log, const char *directory_path, const char *base_file_name,
    log_category_t *cats, uint32 num_cats)
{
    if (!num_cats)
        return 1;
    memset(log, 0, sizeof(*log));
    dchar *path = _create_log_file_path(directory_path, base_file_name);
    if (!path)
        return 2;
    int err;
    log->file = fopen(path, "a+");
    dstr_free(&path);
    if (!log->file)
        {err = 2; goto fail;}
    log->tags       = ecalloc(num_cats * sizeof(log_tag_t));
    log->num_tags   = num_cats;
    for (uint32 i = 0; i < num_cats; ++i)
    {
        uint32 len = (uint32)strlen(cats[i].tag);
        log->tags[i].str    = ecalloc(len + 2 + 1);
        log->tags[i].str[0] = '[';
        memcpy(log->tags[i].str + 1, cats[i].tag, len);
        log->tags[i].str[1 + len]   = ' ';
        log->tags[i].len            = len + 2;
    }
    return 0;
    fail:
        log_destroy(log);
        return err;
}

void
log_destroy(log_t *log)
{
    while (interlocked_compare_exchange_int32(&log->queued_count, -1, -1))
        sleep_ms(16);
    safe_fclose(log->file);
    free(log->tags);
    memset(log, 0, sizeof(*log));
}

void
log_printf(log_t *log, uint category, const char *fmt, ...)
{
    muta_assert(category < log->num_tags);
    va_list args;
    va_start(args, fmt);
    int num = vsnprintf(0, 0, fmt, args);
    va_end(args);
    if (num <= 0)
        return;
    struct tm t;
    char time_buf[64];
    get_local_time(&t);
    uint32 time_len = (uint32)strftime(time_buf, sizeof(time_buf),
        "%d/%m/%Y %H:%M:%S]]", &t);
    if (!time_len)
    {
        DEBUG_PRINTFF("Failed to get timestamp!\n");
        return;
    }
    uint32 tag_len = log->tags[category].len;
    mutex_lock(&_segpool_mutex);
    num += tag_len;
    num += time_len;
    num += 1; /* Space */
    /* No need to add null terminator: already included in time_len */
    void *memory = segpool_malloc(&_segpool, num);
    mutex_unlock(&_segpool_mutex);
    log_event_t event;
    event.log = log;
    event.msg = memory;
    char *dst = event.msg;
    memcpy(dst, log->tags[category].str, tag_len);
    dst += tag_len;
    memcpy(dst, time_buf, time_len);
    dst += time_len - 1;
    if (num > 0)
        *(dst++) = ' ';
    va_start(args, fmt);
    vsnprintf(dst, num - tag_len - (time_len -1), fmt, args);
    va_end(args);
    event_push(&_event_buf, &event, 1);
    interlocked_increment_int32(&log->queued_count);
    return;
}

static void
_handle_events(log_event_t *events, int num)
{
    for (int i = 0; i < num; ++i)
    {
        fputs(events[i].msg, events[i].log->file);
        fputc('\n', events[i].log->file);
        fflush(events[i].log->file);
        fputs(events[i].msg, stdout);
        fputc('\n', stdout);
    }
    mutex_lock(&_segpool_mutex);
    for (int i = 0; i < num; ++i)
    {
        segpool_free(&_segpool, events[i].msg);
        interlocked_decrement_int32(&events[i].log->queued_count);
    }
    mutex_unlock(&_segpool_mutex);
}

static thread_ret_t
_main(void *args)
{
    log_event_t events[64];
    int num;
    while (interlocked_compare_exchange_int32(&_running, -1, -1))
    {
        num = event_wait(&_event_buf, events, 64, 500);
        _handle_events(events, num);
    }
    while ((num = event_wait(&_event_buf, events, 64, 0)))
        _handle_events(events, num);
    return 0;
}
