CC				=	gcc
ALL_DEPS		=
ALL				=	make all-internal
C_FLAGS			=	-Wall -std=gnu99 -g -DEBUG -D_MUTA_DEBUG
L_FLAGS			=	-std=gnu99 -Wall -g
COMPILE_OBJ		=	mkdir -p $(OBJ_DIR) && $(CC) -c $(C_FLAGS) $? -o $@
LINK_PROGRAM	=	$(CC) $(L_FLAGS) $(OBJS) -o $(OBJ_DIR)/$(EXE_NAME)
EXE_NAME		=	scriptbuilder
S				=	/

.DEFAULT_GOAL := all

all-internal:
	make -j$(shell grep -c '^processor' /proc/cpuinfo) objs
	make link
