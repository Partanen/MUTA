#include "header_parser.h"
#include "../../../shared/get_opt.h"
#include "../../../shared/common_utils.h"

static get_opt_context_t    _opts = GET_OPT_CONTEXT_INITIALIZER;
parse_context_t             _parse_context;

void
parse_header(parse_context_t *parser, const char *header_path);

int main(int argc, char **argv)
{
    const char  *dir_path = 0;
    int         opt;
    while ((opt = get_opt(&_opts, argc, argv, "hd:")) != -1)
    {
        switch (opt)
        {
        case 'h':
            return 0;
        case 'd':
            dir_path = _opts.arg;
            break;
        case '?':
            puts("Unknown command line option.");
            return 1;
        }
    }
    if (dir_path)
    {
        dir_entry_t     entry;
        dir_handle_t    directory   = open_directory(dir_path, &entry);
        dchar           *file_path  = 0;
        do
        {
            char *name = get_dir_entry_name(&entry);
            if (!name)
                continue;
            if (str_ends_with(name, ".h"))
            {
                dstr_set(&file_path, dir_path);
                dstr_append(&file_path, "/");
                dstr_append(&file_path, name);
                parse_header(&_parse_context, name);
            }
        } while (!get_next_file_in_directory(directory, &entry));
        close_directory(directory);
        dstr_free(&file_path);
    }
    return 0;
}
