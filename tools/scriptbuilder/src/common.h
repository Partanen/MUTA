#ifndef MUTA_SCRIPTBUILDER_PARSE_CONTEXT_H
#define MUTA_SCRIPTBUILDER_PARSE_CONTEXT_H

typedef struct token_type_t     token_type_t;
typedef struct script_t         script_t;
typedef struct parse_context_t  parse_context_t;

struct token_type_t
{
    const char *str;
};

struct script_t
{
    int tmp;
};

struct parse_context_t
{
    script_t *scripts; /* darr */
};

token_type_t *
find_token_type(const char *name, uint32 name_len);

#endif /* MUTA_SCRIPTBUILDER_PARSE_CONTEXT_H */
