#include "../../../shared/common_utils.h"

typedef struct token_t token_t;

struct token_t
{
};

void
parse_header(parse_context_t *parser, const char *header_path)
{
    dchar   *file           = load_text_file_to_dstr(header_path);
    uint32  file_len        = dstr_len(file);
    char    *token_begin    = file;
    for (;;)
    {
        while (isspace(token_begin))
            token_begin++;
        uint32 token_len = 0;
        for (; token_begin[token_len] && !isspace(token_begin[token_len]);)
            token_len++;
        token_type_t *token_type = find_token_type(token_begin, token_len);
        if (!token_type)
            continue;
        
    }
}

