#define _MUTA_COMMON_UTILS_NO_NET
#include <limits.h>
#include "../../shared/mdb.c"
#include "../../shared/common_utils.c"

#define MDB_EDIT_VER 0
#define INPUT_BUF_SZ 1024

#define CREATE_FILE_NAME(fp, len) \
    if ((len) < 4 || !streq((fp) + (len) - 4, ".mdb")) \
    { \
        fp = stack_alloc((len) + 1 + 4); \
        memcpy(fp, _input_buf, len); \
        memcpy(fp + len, ".mdb", 4); \
        fp[len + 4] = 0; \
    }

static char         _input_buf[INPUT_BUF_SZ];
static mdb_t        _db;
static mdb_row_t    _selected_row;
static dchar        *_open_fp;

static int _prompt(const char *prompt_override, bool32 strip_trailing_spaces);
static void _new_col(void);
static void _new_row(void);
static int  _new(void);
static void _save_as(void);
static void _save(void);
static void _help(void);
static int _open(void);
static void _dump(void);
static void _get_field(void);
static void _set_field(void);
static void _select(void);
static void _dump_row(void);
static void _del_row(void);
static void _print_val_by_type(void *val, int type);

int main(int argc, char **argv)
{
    printf("mdb edit v. %u\n", MDB_EDIT_VER);
    while (!_prompt(0, 1))
    {
        if (streq(_input_buf, "help"))
            _help();
        else if (streq(_input_buf, "new"))
            _new();
        else if (streq(_input_buf, "open"))
            _open();
        else
            printf("\"%s\": no such command.\n", _input_buf);
    }
    mdb_close(&_db);
    return 0;
}

static int
_prompt(const char *prompt_override, bool32 strip_trailing_spaces)
{
    printf(prompt_override ? prompt_override : "[mdb] ");
    char *s = fgets(_input_buf, INPUT_BUF_SZ, stdin);
    if (s)
    {
        char *c = s;
        for (; *c; ++c)
            if (*c == '\n') {*c = 0; break;}
        if (strip_trailing_spaces)
            str_strip_trailing_spaces(s);
        if (streq(_input_buf, "quit"))
        {
            while (_selected_row && printf("[Save (y/n)?]: ")
            && (s = fgets(_input_buf, INPUT_BUF_SZ, stdin)))
            {
                str_strip_trailing_spaces(_input_buf);
                str_to_lower(_input_buf);
                if (streq(_input_buf, "yes") || streq(_input_buf, "y"))
                {
                    mdb_save(&_db, _open_fp);
                    printf("\nSaved %s.\n", _open_fp);
                    break;
                } else
                if (streq(_input_buf, "no") || streq(_input_buf, "n"))
                    break;
            }
            mdb_close(&_db);
            exit(0);
        }
        return 0;
    }
    return 1;
}

static void
_help()
{
    puts(
        "new:  create new mdb file\n"
        "open: open an existing mdb file for editing\n"
        "quit: quit");
}

static int
_new()
{
    mdb_t db;
    if (mdb_new(&db))
        return 1;
    while (!_prompt("[new db name (max 32 chars):] ", 1))
    {
        if (!mdb_set_name(&db, _input_buf))
        {
            printf("Set name to '%s'.\n", _input_buf);
            break;
        }
        printf("Invalid name: %s\n", _input_buf);
    }
    while (!_prompt("[new db file path:] ", 1))
    {
        if (str_is_valid_file_path(_input_buf))
            break;
        printf("Invalid file path: %s\n", _input_buf);
    }
    char *fp = _input_buf;
    uint len = strlen(_input_buf);
    CREATE_FILE_NAME(fp, len);
    if (mdb_save(&db, fp))
    {
        printf("Failed to create file %s.\n", fp);
        return 2;
    }
    printf("Created file %s.\n", fp);
    return 0;
}

static int
_open()
{
    while (!_prompt("[file path:] ", 1))
    {
        int r;
        if (!(r = mdb_open(&_db, _input_buf)))
            break;
        printf("Failed to open file: %s (code %d).\n", _input_buf, r);
    }
    dstr_set(&_open_fp, _input_buf);
    printf("Opened %s. Type 'help' for options.\n", _input_buf);
    while (!_prompt("[edit db] ", 1))
    {
        if (streq(_input_buf, "help"))
            puts(
"new col:   create new column\n"
"new row:   create new row\n"
"del row:   delete a row\n"
"select:    select a row\n"
"dump:      print out a database\n"
"save as:   save the opened file to the specified path\n"
"save:      save the opened file\n"
"get field: print a field from selected row\n"
"set field: set value of a field for selected row\n"
"dump row:  print all the fields in the selected row");
        else if (streq(_input_buf, "new col"))
            _new_col();
        else if (streq(_input_buf, "new row"))
            _new_row();
        else if (streq(_input_buf, "del row"))
            _del_row();
        else if (streq(_input_buf, "dump"))
            _dump();
        else if (streq(_input_buf, "save_as"))
            _save_as();
        else if (streq(_input_buf, "save"))
            _save();
        else if (streq(_input_buf, "get field"))
            _get_field();
        else if (streq(_input_buf, "set field"))
            _set_field();
        else if (streq(_input_buf, "select"))
            _select();
        else if (streq(_input_buf, "dump row"))
            _dump_row();
        else
            printf("Invalid command: '%s'.\n", _input_buf);
    }
    return 0;
}

static void
_new_col(void)
{
    puts("Create a new column");
    char col_name[MDB_MAX_COL_NAME_LEN + 1];
    while (!_prompt("[column name:] ", 1))
    {
        uint len = strlen(_input_buf);
        if (len > MDB_MAX_NAME_LEN)
        {
            puts("Name too long.");
            continue;
        }
        memcpy(col_name, _input_buf, len + 1);
        break;
    }
    puts("Enter column type. Valid options:\n"
        "int8, uint8, int16, uint16, int32, uint32, string");
    int type = NUM_MDB_COL_TYPES;
    while (!_prompt("[column type:] ", 1))
    {
        if (streq(_input_buf, "int8"))          type = MDB_COL_INT8;
        else if (streq(_input_buf, "uint8"))    type = MDB_COL_UINT8;
        else if (streq(_input_buf, "int16"))    type = MDB_COL_INT16;
        else if (streq(_input_buf, "uint16"))   type = MDB_COL_UINT16;
        else if (streq(_input_buf, "int32"))    type = MDB_COL_INT32;
        else if (streq(_input_buf, "uint32"))   type = MDB_COL_UINT32;
        else if (streq(_input_buf, "string"))   type = MDB_COL_STR;
        if (type != NUM_MDB_COL_TYPES)
            break;
        printf("Invalid type: %s.\n", _input_buf);
    }
    bool32 is_key = 0;
    puts("Use column as a search key?");
    while (!_prompt("[use as key (yes/no):] ", 1))
    {
        if (streq(_input_buf, "yes"))
            is_key = 1;
        else if (streq(_input_buf, "no"))
            is_key = 0;
        else
        {
            printf("Invalid option: %s\n", _input_buf);
            continue;
        }
        break;
    }
    if (mdb_new_col(&_db, col_name, type, is_key))
        printf("Failed to create new column %s.\n", col_name);
    else
        printf("Created new column %s.\n", col_name);
}

static void
_new_row(void)
{
    uint32 id;
    if (!mdb_new_row(&_db, &id))
        printf("Created new row, id %u. You can select it with 'select'.\n", id);
    else
        puts("Failed to create row!");
}

static void
_save_as(void)
{
    while (!_prompt("[name ('cancel' to cancel):] ", 1))
    {
        if (streq(_input_buf, "cancel"))
        {
            puts("Canceled save.");
            break;
        }
        char *fp = _input_buf;
        uint len = strlen(_input_buf);
        CREATE_FILE_NAME(fp, len);
        if (mdb_save(&_db, fp))
            printf("Failed to save file to '%s'.\n", _input_buf);
        else
        {
            printf("Saved %s.\n", fp);
            break;
        }
    }
}

static void
_save(void)
{
    int r = mdb_save(&_db, _open_fp);
    if (r)
        printf("Failed to save file to '%s' (code %d).\n", _open_fp, r);
    else
        printf("Saved %s.\n", _open_fp);
}

static void
_dump(void)
{
    printf("Dumping db '%s'...\n", _db.name);
    printf("Columns: ");
    uint32 num_cols = mdb_num_cols(&_db);
    uint32 num_rows = mdb_num_rows(&_db);
    for (uint32 i = 0; i < num_cols; ++i)
        printf("[%s (%s)] ", _db.cols[i].name,
            mdb_col_type_to_str(_db.cols[i].type));
    putc('\n', stdout);
    for (uint32 i = 0; i < num_rows; ++i)
    {
        printf("row index %u: ", i);
        mdb_row_t row = mdb_get_row_by_index(&_db, i);
        for (uint32 j = 0; j < num_cols; ++j)
        {
            void *val = mdb_get_field(&_db, row, j);
            _print_val_by_type(val, mdb_get_col_type(&_db, j));
            putc(' ', stdout);
        }
        putc('\n', stdout);
    }
}

static void
_get_field(void)
{
    if (!_selected_row)
    {
        puts("No row selected. See 'select'.");
        return;
    }
    if (_prompt("[field name:] ", 1))
        return;
    int index = mdb_get_col_index(&_db, _input_buf);
    if (index < 0)
    {
        printf("No such column: '%s'.\n", _input_buf);
        return;
    }
    void *f = mdb_get_field(&_db, _selected_row, index);
    if (!f)
    {
        printf("Failed to get field '%s'.\n", _input_buf);
        return;
    }
    switch (_db.cols[index].type)
    {
        case MDB_COL_INT8:
            printf("Field value [int8]: %d\n", (int)*(int8*)f);
            break;
        case MDB_COL_UINT8:
            printf("Field value [uint8]: %u\n", (uint)*(uint8*)f);
            break;
        case MDB_COL_INT16:
            printf("Field value [int16]: %d\n", (int)*(int16*)f);
            break;
        case MDB_COL_UINT16:
            printf("Field value [uint16]: %d\n", (uint)*(uint16*)f);
            break;
        case MDB_COL_INT32:
            printf("Field value [int32]: %d\n", *(int32*)f);
            break;
        case MDB_COL_UINT32:
            printf("Field value [uint32]: %u\n", *(uint32*)f);
            break;
        case MDB_COL_STR:
            printf("Field value [string]: %s\n", *(char**)f);
            break;
        case NUM_MDB_COL_TYPES:
            break;
    }
}

static void
_set_field(void)
{
    if (!_selected_row)
    {
        puts("No row selected - try 'select [id]'.");
        return;
    }
    if (_prompt("[col name:] ", 1))
        return;
    int index = mdb_get_col_index(&_db, _input_buf);
    if (index < 0)
    {
        printf("No column named %s.\n", _input_buf);
        return;
    }
    printf("Field '%s' value: ", _db.cols[index].name);
    _print_val_by_type(mdb_get_field(&_db, _selected_row, index),
        mdb_get_col_type(&_db, index));
    putc('\n', stdout);
    char buf[32];
    stbsp_snprintf(buf, 32, "[value (%s):] ",
        mdb_col_type_to_str(_db.cols[index].type));
    if (_prompt(buf, 0))
        return;
    if (mdb_get_col_type(&_db, index) != MDB_COL_STR)
    {
        str_strip_trailing_spaces(_input_buf);
        str_strip_non_ascii(_input_buf);
        str_strip_ctrl_chars(_input_buf);
        if (!str_is_int(_input_buf))
        {
            printf("Invalid value: %s\n", _input_buf);
            return;
        }
    } else
    {
        str_strip_non_ascii(_input_buf);
        str_strip_ctrl_chars(_input_buf);
    }
    int r;
    if (mdb_get_col_type(&_db, index) == MDB_COL_STR)
        r = mdb_set_field(&_db, _selected_row, index, _input_buf);
    else
    {
        switch (mdb_get_col_type(&_db, index))
        {
        case MDB_COL_INT8:
        {
            int v = atoi(_input_buf);
            if (v < -128 || v > 127)
                goto out_of_range;
            *(int8*)buf = (int8)v;
        }
            break;
        case MDB_COL_UINT8:
        {
            int v = atoi(_input_buf);
            if (v < 0 || v > 255)
                goto out_of_range;
            *(uint8*)buf = (uint8)v;
        }
            break;
        case MDB_COL_INT16:
        {
            int v = atoi(_input_buf);
            if (v < -32768 || v > 32767)
                goto out_of_range;
            *(int16*)buf = (int16)v;
        }
            break;
        case MDB_COL_UINT16:
        {
            int v = atoi(_input_buf);
            if (v < 0 || v > 65535)
                goto out_of_range;
            *(uint16*)buf = (uint16)v;
        }
            break;
        case MDB_COL_INT32:
            *(int32*)buf = atoi(_input_buf);
            break;
        case MDB_COL_UINT32:
            *(uint32*)buf = str_to_uint32(_input_buf);
            break;
        default:
            exit(EXIT_FAILURE);
        }
        r = mdb_set_field(&_db, _selected_row, index, buf);
    }
    if (r)
    {
        printf("%s: row was %u\n", __func__, _selected_row);
        printf("Failed to set value '%s' to field.\n", _input_buf);
    } else
        printf("Set field to '%s'.\n", _input_buf);
    return;
    out_of_range:
        printf("Value out of range: %s\n", _input_buf);
}

static void
_select(void)
{
    _selected_row = 0;
    while (!_prompt("[row id ('cancel' to cancel'):] ", 1))
    {
        if (streq(_input_buf, "cancel"))
        {
            puts("Canceled select.");
            break;
        }
        uint32 id = str_to_uint32(_input_buf);
        if (!(_selected_row = mdb_get_row(&_db, id)))
        {
            printf("Row id %u not found.\n", id);
            continue;
        }
        printf("Selected row id %u.\n", id);
        break;
    }
}

static void
_dump_row(void)
{
    if (!_selected_row)
    {
        puts("No row selected.");
        return;
    }
    printf("columns: ");
    for (uint32 i = 0; i < mdb_num_cols(&_db); ++i)
        printf("%s ", mdb_col_type_to_str(_db.cols[i].type));
    putc('\n', stdout);
    for (uint32 i = 0; i < mdb_num_cols(&_db); ++i)
    {
        void *val = mdb_get_field(&_db, _selected_row, i);
        _print_val_by_type(val, mdb_get_col_type(&_db, i));
        putc(' ', stdout);
    }
    putc('\n', stdout);
}

static void
_del_row(void)
{
    if (!_selected_row)
    {
        puts("No row selected");
        return;
    }
    if (mdb_del_row(&_db, _selected_row))
        puts("Failed to delete row.");
    else
        puts("Deleted row.");
    _selected_row = 0;
}

static void
_print_val_by_type(void *val, int type)
{
    switch (type)
    {
        case MDB_COL_INT8:      printf("%d", (int)*(int8*)val);     break;
        case MDB_COL_UINT8:     printf("%u", (uint)*(uint8*)val);   break;
        case MDB_COL_INT16:     printf("%d", (int)*(int16*)val);    break;
        case MDB_COL_UINT16:    printf("%u", (uint)*(uint16*)val);  break;
        case MDB_COL_INT32:     printf("%d", (int)*(int32*)val);    break;
        case MDB_COL_UINT32:    printf("%u", (uint)*(uint32*)val);  break;
        case MDB_COL_STR:       printf("%s", *(char**)val);         break;
    }
}
