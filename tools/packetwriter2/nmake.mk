CC 				=	cl
EXE_NAME		=	packetwriter2.exe
C_FLAGS_COMMON	=	/W3 $(INCLUDES) /D_CRT_SECURE_NO_WARNINGS /EHsc
C_FLAGS_DEBUG	=	$(C_FLAGS_COMMON) /D_MUTA_DEBUG /D_MUTA_COMMON_UTILS_NO_NET /DEBUG /Zi /Od /Tc
C_FLAGS			=	$(C_FLAGS_DEBUG)
L_FLAGS_DEBUG	=	/SUBSYSTEM:CONSOLE /DEBUG
L_FLAGS			=	$(L_FLAGS_DEBUG)
COMPILE_OBJ 	=	(if not exist $(OBJ_DIR) (mkdir $(OBJ_DIR))) && cl /c $(C_FLAGS) $? /Fo$@
OBJ_DIR			=	.
LINK_PROGRAM	=	link $(L_FLAGS) $(OBJS) $(LIBS) /OUT:$(OBJ_DIR)\$(EXE_NAME)
ALL_DEPS		=	$(OBJS) link
ALL				=	
RM				=	del /Q /F
S				=	\\

.default: all
