CC				=	gcc
ALL_DEPS		=
ALL				=	make all-internal
C_FLAGS			=	-Wall -std=gnu99 -g -DEBUG -D_MUTA_DEBUG -D_MUTA_COMMON_UTILS_NO_NET -fsanitize=address
L_FLAGS			=	-std=gnu99 -Wall -g -fsanitize=address
COMPILE_OBJ		=	mkdir -p $(OBJ_DIR) && $(CC) -c $(C_FLAGS) $? -o $@
LINK_PROGRAM	=	$(CC) $(L_FLAGS) $(OBJS) -o $(OBJ_DIR)/$(EXE_NAME)
EXE_NAME		=	packetwriter2
S				=	/

.DEFAULT_GOAL := all

all-internal:
	make -j$(shell grep -c '^processor' /proc/cpuinfo) objs
	make link

run-debug:
	gdb --args $(OBJ_DIR)/$(EXE_NAME) -i test.def -o test.h -v
