# Packetwriter 2

Packetwriter 2 generates C serialization code from a custom packet definition
file format. It's the second iteration of the original MUTA packetwriter.

## Building
```make``` or ```nmake```

## Usage
Assuming your input file is named "input.def" and outputfile is named
"output.h":
```./packetwriter2 -i input.def -o output.h```

## Supported data types
* int8, uint8, int16, uint16, int32, uint32, int64, int64
* char (treated as 8 bit wide signed int)
* f32, f64
* Structs consisting of any of the above types or other structs

### Arrays
There are two types of arrays: fixed size and variable size.
* Fixed size notation: ```my_array = uint32[4]```
* Variable size notation with maximum: ```my_array = uint32{4}```
* Variable size notation with minimum and maximum: ```my_array = uint32{2, 4}```
* Variable size notation with explicit length type: ```my_array = uint32 <uint8> {MY_MIN, MY_MAX}```

Arrays can be used in both, structs and packets.

### Ranges
It is possible to assign minimum and maximum legal values to numeric variables.

```my_int = int32 (min = 0, max = 50)```
```my_int_array = int32[5] (max = MY_INT_MAX_VALUE)```

## Format

MUTA's def file format is used with custom keywords.

```
# This line is a comment. Comments are only allowed on their own lines, not at
# the end of other lines.

# Define include guards used in the generated header.
include_guard: MY_PACKETS_H

# Declare a file to include.
# relative_path defines which notation is used for the include: <> for a system
# path or "" for a relative path
include: common_defs.h
    relative_path = true

# Declare a group of packets. All packet types must belong to a group.
# first_opcode is an optional opcode value given to the first packet defined
# for this group. It can either be a number or a constant. Any packets following
# will increment the previous opcode by 1 (default C enum behaviour).
group: my_packets
    first_opcode = 0

# Define a struct - it's definition will appear in the generated header.
struct: my_vector_t
    x = int32 (min = -100, max = 100)
    y = int32 (min = -100, max = 100)

# Define a packet
# __group must be a previously declared group
# __encrypt is optional and is set to false by default
# Note that strings (char arrays of any type) are not null-terminated unless
# you null-terminate them yourself.
packet: my_packet
    __group     = my_packets
    __encrypt   = false
    vectors     = my_vector_t <uint8> {MAX_VECTORS}
    string      = char[16]
```
