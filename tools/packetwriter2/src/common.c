#include "common.h"
#include <limits.h>
#include <float.h>

primitive_t primitives[NUM_PRIMITIVES];
struct_t    *structs;
group_t     *groups;
include_t   *includes;
packet_t    *packets;
dchar       *include_guard;
segpool_t   segpool;
bool32      verbose_printing_on;

void
init_common(void)
{
    segpool_init(&segpool);
    primitives[PRIMITIVE_INT8].name             = "int8";
    primitives[PRIMITIVE_INT8].is_signed        = 1;
    primitives[PRIMITIVE_INT8].is_float         = 0;
    primitives[PRIMITIVE_INT8].min_range.i      = INT8_MIN;
    primitives[PRIMITIVE_INT8].max_range.i      = INT8_MAX;
    primitives[PRIMITIVE_INT8].size             = 1;
    primitives[PRIMITIVE_UINT8].name            = "uint8";
    primitives[PRIMITIVE_UINT8].is_signed       = 0;
    primitives[PRIMITIVE_UINT8].is_float        = 0;
    primitives[PRIMITIVE_UINT8].min_range.u     = 0;
    primitives[PRIMITIVE_UINT8].max_range.u     = UINT8_MAX;
    primitives[PRIMITIVE_UINT8].size            = 1;
    primitives[PRIMITIVE_INT16].name            = "int16";
    primitives[PRIMITIVE_INT16].is_signed       = 1;
    primitives[PRIMITIVE_INT16].is_float        = 0;
    primitives[PRIMITIVE_INT16].min_range.i     = INT16_MIN;
    primitives[PRIMITIVE_INT16].max_range.i     = INT16_MAX;
    primitives[PRIMITIVE_INT16].size            = 2;
    primitives[PRIMITIVE_UINT16].name           = "uint16";
    primitives[PRIMITIVE_UINT16].is_signed      = 0;
    primitives[PRIMITIVE_UINT16].is_float       = 0;
    primitives[PRIMITIVE_UINT16].min_range.u    = 0;
    primitives[PRIMITIVE_UINT16].max_range.u    = UINT16_MAX;
    primitives[PRIMITIVE_UINT16].size           = 2;
    primitives[PRIMITIVE_INT32].name            = "int32";
    primitives[PRIMITIVE_INT32].is_signed       = 1;
    primitives[PRIMITIVE_INT32].is_float        = 0;
    primitives[PRIMITIVE_INT32].min_range.i     = INT32_MIN;
    primitives[PRIMITIVE_INT32].max_range.i     = INT32_MAX;
    primitives[PRIMITIVE_INT32].size            = 4;
    primitives[PRIMITIVE_UINT32].name           = "uint32";
    primitives[PRIMITIVE_UINT32].is_signed      = 0;
    primitives[PRIMITIVE_UINT32].is_float       = 0;
    primitives[PRIMITIVE_UINT32].min_range.u    = 0;
    primitives[PRIMITIVE_UINT32].max_range.u    = UINT32_MAX;
    primitives[PRIMITIVE_UINT32].size           = 4;
    primitives[PRIMITIVE_INT64].name            = "int64";
    primitives[PRIMITIVE_INT64].is_signed       = 1;
    primitives[PRIMITIVE_INT64].is_float        = 0;
    primitives[PRIMITIVE_INT64].min_range.i     = INT64_MIN;
    primitives[PRIMITIVE_INT64].max_range.i     = INT64_MAX;
    primitives[PRIMITIVE_INT64].size            = 8;
    primitives[PRIMITIVE_UINT64].name           = "uint64";
    primitives[PRIMITIVE_UINT64].is_signed      = 0;
    primitives[PRIMITIVE_UINT64].is_float       = 0;
    primitives[PRIMITIVE_UINT64].min_range.u    = 0;
    primitives[PRIMITIVE_UINT64].max_range.u    = UINT64_MAX;
    primitives[PRIMITIVE_UINT64].size           = 8;
    primitives[PRIMITIVE_FLOAT].name            = "f32";
    primitives[PRIMITIVE_FLOAT].is_signed       = 1;
    primitives[PRIMITIVE_FLOAT].is_float        = 1;
    primitives[PRIMITIVE_FLOAT].min_range.f     = FLT_MIN;
    primitives[PRIMITIVE_FLOAT].max_range.f     = FLT_MAX;
    primitives[PRIMITIVE_FLOAT].size            = 4;
    primitives[PRIMITIVE_DOUBLE].name           = "f64";
    primitives[PRIMITIVE_DOUBLE].is_signed      = 1;
    primitives[PRIMITIVE_DOUBLE].is_float       = 1;
    primitives[PRIMITIVE_DOUBLE].min_range.f    = DBL_MIN;
    primitives[PRIMITIVE_DOUBLE].max_range.f    = DBL_MAX;
    primitives[PRIMITIVE_DOUBLE].size           = 8;
    primitives[PRIMITIVE_CHAR].name             = "char";
    primitives[PRIMITIVE_CHAR].is_signed        = 1;
    primitives[PRIMITIVE_CHAR].is_float         = 0;
    primitives[PRIMITIVE_CHAR].min_range.i      = INT8_MIN;
    primitives[PRIMITIVE_CHAR].max_range.i      = INT8_MAX;
    primitives[PRIMITIVE_CHAR].size             = 1;
}
