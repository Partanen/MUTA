#include "common.h"
#include <ctype.h>
#include <inttypes.h>

#define parser_panic(fmt, ...) \
    muta_panic_print("Line: %zu. " fmt, _line_number, ##__VA_ARGS__)

enum parser_state
{
    PARSER_STATE_NONE = 0,
    PARSER_STATE_STRUCT,
    PARSER_STATE_GROUP,
    PARSER_STATE_INCLUDE,
    PARSER_STATE_PACKET
};

static enum parser_state    _parser_state;
static size_t               _line_number;

static int
_on_def_callback(parse_def_file_context_t *ctx, const char *def,
    const char *val);

static int
_on_opt_callback(parse_def_file_context_t *ctx, const char *opt,
    const char *val);

static void
_on_parser_state_struct_opt(const char *opt, const char *val);

static void
_on_parser_state_group_opt(const char *opt, const char *val);

static void
_on_parser_state_packet_opt(const char *opt, const char *val);

static void
_on_parser_state_include_opt(const char *opt, const char *val);

static void
_finish_current_state(void);

static void
_check_variable_or_constant_name(const char *name);

static void
_check_struct_name(const char *name);
/* _check_struct_name()
 * Panics if name is illegal. */

static void
_check_group_name(const char *name);
/* _check_group_name()
 * Panics if name is illegal */

static void
_check_packet_name(const char *name);
/* _check_packet_name()
 * Panics if name is illegal. */

static bool32
_find_brackets_in_type_name(const char *name, size_t len, char left, char right,
    const char **ret_left, const char **ret_right);

static int
_find_complete_type(const char *name, complete_type_t *ret);

static primitive_t *
_find_primitive(const char *name);

static struct_t *
_find_struct(const char *name);

static struct_member_t *
_find_struct_member(struct_t *s, const char *name);

static packet_member_t *
_find_packet_member(packet_t *packet, const char *name);

static group_t *
_find_group(const char *name);

static bool32
_is_maybe_constant(const char *value);
/*_is_maybe_constant()
 * True if not an int and not a float. */

static void
_init_range_num_from_str(range_t *range, int float_signed_or_unsigned);

void
parse(const char *file_path)
{
    printf_verbose("Parsing '%s'.\n", file_path);
    if (parse_def_file(file_path, _on_def_callback, _on_opt_callback, 0))
        parser_panic("Failed to parse file '%s'.", file_path);
    _finish_current_state();
}

static int
_on_def_callback(parse_def_file_context_t *ctx, const char *def,
    const char *val)
{
    _line_number = ctx->line;
    _finish_current_state();
    if (streq(def, "struct"))
    {
        printf_verbose("Struct '%s'\n", val);
        _check_struct_name(val);
        if (_find_struct(val))
            parser_panic("Struct '%s' defined twice.", val);
        if (_find_primitive(val))
            parser_panic("Bad struct name '%s': a primitive with the same "
                "name was already defined.", val);
        struct_t new_struct = {0};
        new_struct.name = dstr_create(val);
        darr_push(structs, new_struct);
        _parser_state = PARSER_STATE_STRUCT;
    } else
    if (streq(def, "group"))
    {
        printf_verbose("Group '%s'\n", val);
        _check_group_name(val);
        if (_find_group(val))
            parser_panic("Group '%s' defined twice.", val);
        group_t new_group = {0};
        new_group.name          = dstr_create(val);
        new_group.opcode_type   = 0xFFFFFFFF;
        darr_push(groups, new_group);
        _parser_state = PARSER_STATE_GROUP;
    } else
    if (streq(def, "include"))
    {
        printf_verbose("Include: '%s'.\n", val);
        if (!str_is_valid_file_path(val))
            parser_panic("Illegal include file path '%s'.", val);
        include_t new_include;
        new_include.path                    = dstr_create(val);
        new_include.relative_path           = 1;
        new_include.explicit_relative_path  = 0;
        darr_push(includes, new_include);
        _parser_state = PARSER_STATE_INCLUDE;
    } else
    if (streq(def, "packet"))
    {
        printf_verbose("Packet '%s'\n", val);
        _check_packet_name(val);
        packet_t packet = {0};
        packet.group    = 0xFFFFFFFF;
        packet.name     = dstr_create(val);
        darr_push(packets, packet);
        _parser_state = PARSER_STATE_PACKET;
    } else
    if (streq(def, "include_guard"))
    {
        _check_variable_or_constant_name(val);
        if (include_guard)
            parser_panic("include_guard defined more than once.");
        include_guard = dstr_create(val);
    } else
        parser_panic("Undefined keyword '%s'.", def);
    return 0;
}

static int
_on_opt_callback(parse_def_file_context_t *ctx, const char *opt,
    const char *val)
{
    _line_number = ctx->line;
    switch (_parser_state)
    {
    case PARSER_STATE_STRUCT:
        _on_parser_state_struct_opt(opt, val);
        break;
    case PARSER_STATE_GROUP:
        _on_parser_state_group_opt(opt, val);
        break;
    case PARSER_STATE_PACKET:
        _on_parser_state_packet_opt(opt, val);
        break;
    case PARSER_STATE_INCLUDE:
        _on_parser_state_include_opt(opt, val);
        break;
    default:
        muta_assert(0);
    }
    return 0;
}

static void
_on_parser_state_struct_opt(const char *opt, const char *val)
{
    printf_verbose("Struct member '%s': '%s'.\n", opt, val);
    struct_t *s = &structs[darr_num(structs) - 1];
    if (_find_struct_member(s, opt))
        parser_panic("Member '%s' defined twice in struct '%s'.", opt,
            s->name);
    complete_type_t complete_type;
    int e;
    if ((e = _find_complete_type(val, &complete_type)))
        parser_panic("Member '%s' in struct '%s' has invalid type (%d).",
            opt, s->name, e);
    struct_member_t member = {0};
    member.name             = dstr_create(opt);
    member.complete_type    = complete_type;
    darr_push(s->members, member);
}

static void
_on_parser_state_group_opt(const char *opt, const char *val)
{
    group_t *group = &groups[darr_num(groups) - 1];
    if (streq(opt, "first_opcode"))
    {
        printf_verbose("Group option first_opcode: '%s'.\n", val);
        if (group->first_opcode)
            parser_panic("First opcode for group '%s' defined twice.",
                group->name);
        if (!str_is_int(val))
            _check_variable_or_constant_name(val);
        else if (val[0] == '-')
            parser_panic("Bad first_opcode for group '%s': cannot be < 0.",
                group->name);
        group->first_opcode = dstr_create(val);
    } else
    if (streq(opt, "opcode_type"))
    {
        if (group->opcode_type != 0xFFFFFFFF)
            parser_panic("Option 'opcode_type' defined more than once for "
                "group '%s'.", group->name);
        primitive_t *primitive = _find_primitive(val);
        if (!primitive)
            parser_panic("Invalid opcode type for group '%s': must be an "
                "unsigned primitive type.", group->name);
        if (primitive->is_signed)
            parser_panic("Invalid opcode type for group '%s': must be an "
                "unsigned type.", group->name);
        group->opcode_type = (uint32)(primitive - primitives);
    } else
        parser_panic("Invalid group option for group '%s'.", group->name);
}

static void
_on_parser_state_packet_opt(const char *opt, const char *val)
{
    packet_t *packet = &packets[darr_num(packets) - 1];
    if (streq(opt, "__group"))
    {
        printf_verbose("Packet '%s' __group: '%s'.\n", packet->name, val);
        if (packet->group != 0xFFFFFFFF)
            parser_panic("__group defined more than once for packet '%s'.",
                packet->name);
        group_t *group = _find_group(val);
        if (!group)
            parser_panic("Bad group '%s' for packet '%s': "
                "Groups must be defined before the packets that use them.",
                val, packet->name);
        packet->group = (uint32)(group - groups);
        darr_push(group->packets, (uint32)(packet - packets));
        if (group->opcode_type != 0xFFFFFFFF)
        {
            if (darr_num(group->packets) ==
                primitives[group->opcode_type].max_range.u)
            parser_panic("Opcode type for group '%s' is too small.",
                group->name);
            if (str_is_int(group->first_opcode))
            {
                uint64 total = str_to_uint64(group->first_opcode) +
                    darr_num(group->packets);
                if (total == primitives[group->opcode_type].max_range.u)
                    parser_panic("Opcode type for group '%s' is too small.",
                        group->name);
            }
        }
    } else
    if (streq(opt, "__encrypt"))
    {
        printf_verbose("Packet '%s' __encrypt: '%s'.\n", packet->name, val);
        if (packet->encrypt_defined)
            parser_panic("Field __encrypt defined more than once for packet "
                "'%s'.", packet->name);
        if (streq(val, "true"))
            packet->encrypt = 1;
        else if (streq(val, "false"))
            packet->encrypt = 0;
        else
            parser_panic("Invalid option for field __encrypt for packet '%s': "
                "must be 'true' or 'false'.", packet->name);
        packet->encrypt_defined = 1;
    } else
    {
        /* It's a member variable? */
        printf_verbose("Packet member '%s': '%s'.\n", opt, val);
        if (_find_packet_member(packet, opt))
            parser_panic("Member '%s' defined twice for packet '%s'.", opt,
                packet->name);
        _check_variable_or_constant_name(opt);
        complete_type_t complete_type;
        if (_find_complete_type(val, &complete_type))
            parser_panic("Invalid type for member '%s' in packet '%s'.", opt,
                packet->name);
        packet_member_t packet_member;
        packet_member.name          = dstr_create(opt);
        packet_member.complete_type = complete_type;
        darr_push(packet->members, packet_member);
    }
}

static void
_on_parser_state_include_opt(const char *opt, const char *val)
{
    printf_verbose("Include option '%s': '%s'.\n", opt, val);
    include_t *include = &includes[darr_num(includes) - 1];
    if (!streq(opt, "relative_path"))
        parser_panic("Invalid option '%s' for include '%s'.", opt,
            include->path);
    if (include->explicit_relative_path)
        parser_panic("relative_path defined twice for include '%s'.",
            include->path);
    if (streq(val, "true"))
        include->relative_path = 1;
    else if (streq(val, "false"))
        include->relative_path = 0;
    else
        parser_panic("Invalid value for include '%s' relative_path: must be "
            "'true' or 'false'", include->path);
    include->explicit_relative_path = 1;
}

static void
_finish_current_state(void)
{
    switch (_parser_state)
    {
    case PARSER_STATE_STRUCT:
    {
        struct_t *s = &structs[darr_num(structs) - 1];
        if (!darr_num(s->members))
            parser_panic("Bad struct '%s': must have > 0 members.",
                s->name);
    }
        break;
    case PARSER_STATE_GROUP:
    {
        group_t *group = &groups[darr_num(groups) - 1];
        if (!group->first_opcode)
            group->first_opcode = dstr_create("0");
        else if (!str_is_int(group->first_opcode) &&
            group->opcode_type == 0xFFFFFFFF)
            parser_panic("Option 'opcode_type' is required for group '%s' "
                "because option 'first_opcode' isn't numeric.", group->name);

        /* If the group has 0 packets, we don't write it. */
    }
        break;
    case PARSER_STATE_PACKET:
    {
        packet_t *packet = &packets[darr_num(packets) - 1];
        if (packet->group == 0xFFFFFFFF)
            parser_panic("Packet '%s' does not belong to a valid group "
                "(is the option __group defined?).", packet->name);
    }
        break;
    case PARSER_STATE_INCLUDE:
    case PARSER_STATE_NONE:
        break;
    }
    _parser_state = PARSER_STATE_NONE;
}

static void
_check_variable_or_constant_name(const char *name)
{
    if (!name[0])
        parser_panic("Bad variable or constant name: length must be > 0");
    for (const char *c = name; *c; ++c)
        if (!isalpha(*c) && !(c != name && isdigit(*c)) && *c != '_')
            parser_panic("Bad variable or constant name '%s': may only "
                "contain letters, numbers or underscores.", name);
}

static void
_check_struct_name(const char *name)
{
    if (isdigit(name[0]))
        parser_panic("Struct name '%s' is illegal: first letter may not be a "
            "number.", name);
    for (const char *c = name; *c; ++c)
    {
        if (!isascii(*c))
            parser_panic("Struct name '%s' is illegal: must be ascii.",
                name);
        if (!isalpha(*c) && !isdigit(*c) && *c != '_')
            parser_panic("Struct name '%s' is illegal: may only contain "
                "letters, numbers and underscores.", name);
    }
}

static void
_check_group_name(const char *name)
{
    if (isdigit(name[0]))
        parser_panic("Bad group name '%s': cannot begin with a number.",
            name);
    for (const char *c = name; *c; ++c)
        if (!isalpha(*c) && !isdigit(*name) && *c != '_')
            parser_panic("Bad group name '%s': may only contain letters, "
                "numbers and underscores.", name);
}

static void
_check_packet_name(const char *name)
{
    for (const char *c = name; *c; ++c)
        if (!isalpha(*c) && !(isdigit(*c) && c != name) && *c != '_')
            parser_panic("Bad packet name '%s': may only contain letters, "
                "numbers and underscores, and may not begin with a number.",
                name);
}

static bool32
_find_brackets_in_type_name(const char *name, size_t len, char left, char right,
    const char **ret_left, const char **ret_right)
{
    *ret_left   = memchr(name, left, len);
    *ret_right  = memchr(name, right, len);
    if (*ret_left && *ret_right)
    {
        if (*ret_right < *ret_left)
            goto invalid_brackets;
    } else if (*ret_left || *ret_right)
        goto invalid_brackets;
    size_t left_count   = 0;
    size_t right_count  = 0;
    for (size_t i = 0; i < len; ++i)
    {
        if (name[i] == left)
        {
            if (++left_count > 1)
                goto invalid_brackets;
        } else
        if (name[i] == right)
        {
            if (++right_count > 1)
                goto invalid_brackets;
        }
    }
    return *ret_left ? 1 : 0;
    invalid_brackets:
        parser_panic("Invalid brackets '%c%c' in type name '%s'.", left,
            right, name);
        return 0;
}

static int
_find_complete_type(const char *name, complete_type_t *ret)
{
    int     error   = 0;
    uint32  len     = (uint32)strlen(name);
    ret->type_index             = INVALID_TYPE_INDEX;
    ret->have_min_range         = 0;
    ret->have_max_range         = 0;
    ret->array_min              = 0;
    ret->array_max              = 0;
    ret->array_type             = ARRAY_TYPE_NONE;
    ret->min_range.str          = 0;
    ret->max_range.str          = 0;
    ret->length_type_override   = 0xFFFFFFFF;
    if (!len)
        {error = 1; goto out;}
    /* Is it an array? */
    const char *left_array_bracket, *right_array_bracket;
    /* Fixed size array */
    if (_find_brackets_in_type_name(name, len, '[', ']',
        &left_array_bracket, &right_array_bracket))
        ret->array_type = ARRAY_TYPE_FIX;
    /* Variable size array */
    else if (_find_brackets_in_type_name(name, len, '{', '}',
        &left_array_bracket, &right_array_bracket))
        ret->array_type = ARRAY_TYPE_VAR;
    /* Array limits */
    if (ret->array_type != ARRAY_TYPE_NONE)
    {
        /* Copy stuff inside brackets into a tmp buffer */
        size_t array_max_str_len =
            (size_t)(right_array_bracket - left_array_bracket) - 1;
        if (!array_max_str_len)
            parser_panic("Bad array size for '%s': at least max capcity is "
                "required.", name);
        char *array_max_str = segpool_malloc(&segpool,
            (uint32)(array_max_str_len + 1));
        memcpy(array_max_str, left_array_bracket + 1, array_max_str_len);
        array_max_str[array_max_str_len] = 0;
        char symbols[] = {' ', '\t', '\n'};
        str_strip_symbols(array_max_str , symbols, sizeof(symbols));
        char *min_max_strs[2] = {0};
        if (str_count_chars(array_max_str, ',') == 1)
        {
            char    *tmp    = memchr(array_max_str, ',', array_max_str_len);
            uint32  min_len = (uint32)(tmp - array_max_str);
            min_max_strs[0] = segpool_malloc(&segpool, min_len + 1);
            memcpy(min_max_strs[0], array_max_str, min_len);
            min_max_strs[0][min_len] = 0;
            uint32 max_len = (uint32)strlen(tmp + 1);
            min_max_strs[1] = segpool_malloc(&segpool, max_len + 1);
            memcpy(min_max_strs[1], tmp + 1, max_len);
            min_max_strs[1][max_len] = 0;
        } else
        {
            uint32 max_len = (uint32)strlen(array_max_str);
            min_max_strs[1] = segpool_malloc(&segpool, max_len + 1);
            memcpy(min_max_strs[1], array_max_str, max_len + 1);
        }
        /* If both strings are numeric, make a range comparison. */
        if (min_max_strs[0] && min_max_strs[1] && str_is_int(min_max_strs[0]) &&
            str_is_int(min_max_strs[1]))
        {
            uint64 umin, umax;
            sscanf(min_max_strs[0], "%" PRIu64, &umin);
            sscanf(min_max_strs[1], "%" PRIu64, &umax);
            if (umin > umax)
                parser_panic("Bad array size for '%s': min must be > max.",
                    name);
            if (!umax)
                parser_panic("Bad array size for '%s': max must be > 0.",
                    name);
        } else
        if (str_is_int(min_max_strs[1]))
        {
            uint64 umax;
            sscanf(min_max_strs[1], "%" PRIu64, &umax);
            if (!umax)
                parser_panic("Bad array size for '%s': max must be > 0.",
                    name);
        }
        if (ret->array_type == ARRAY_TYPE_FIX)
        {
            if (ret->array_min && ret->array_max)
                parser_panic("Fixed size array '%s' has both, min and max "
                    "defined. Fixed size arrays may only define max length.",
                    name);
        } else
        if (ret->array_type == ARRAY_TYPE_VAR)
        {
            if (!min_max_strs[0])
                min_max_strs[0] = "0";
        }
        ret->array_min = min_max_strs[0];
        ret->array_max = min_max_strs[1];
        segpool_free(&segpool, array_max_str);
    }
    /* Find possible length type override */
    const char *left_len_bracket, *right_len_bracket;
    if (_find_brackets_in_type_name(name, len, '<', '>', &left_len_bracket,
        &right_len_bracket))
    {
        if (ret->array_type != ARRAY_TYPE_VAR)
            parser_panic("Array length type specified for non-variable size "
                "array type '%s'.", name);
        if (right_len_bracket > left_array_bracket)
            parser_panic("Length type for '%s' invalid: must come before array "
                "declaration.", name);
        uint32 len_len = (uint32)(right_len_bracket - left_len_bracket) - 1;
        if (!len_len)
            parser_panic("Empty length type override for type '%s'.", name);
        char *len_str = segpool_malloc(&segpool, len_len + 1);
        memcpy(len_str, left_len_bracket + 1, len_len);
        len_str[len_len] = 0;
        primitive_t *primitive = _find_primitive(len_str);
        segpool_free(&segpool, len_str);
        if (!primitive)
            parser_panic("Length type '%s' for type '%s' does not exist.",
                len_str, name);
        if (primitive->is_signed)
            parser_panic("Length type '%s' for type '%s' is invalid: must be "
                "unsigned.", len_str, name);
        if (str_is_int(ret->array_max) &&
            str_to_uint64(ret->array_max) > primitive->max_range.u)
            parser_panic("Array length type '%s' for type '%s' is too small.",
                len_str, name);
        ret->length_type_override = (uint32)(primitive - primitives);
    }
    /* Find the correct type or struct */
    char *type_name = segpool_malloc(&segpool, len + 1);
    memcpy(type_name, name, len + 1);
    size_t type_name_len = 0;
    for (; type_name_len < len; ++type_name_len)
        if (isspace(type_name[type_name_len]) ||
            type_name[type_name_len] == '{' ||
            type_name[type_name_len] == '[' || type_name[type_name_len] == '('
            || type_name[type_name_len] == '<' ||
            type_name[type_name_len] == '>')
            break;
    type_name[type_name_len] = 0;
    for (const char *c = type_name; *c; ++c)
        if (!isalpha(*c) && !(c != type_name && isdigit(*c)) && *c != '_')
            parser_panic("Bad typename for '%s': may only contain letters, "
                "numbers and underscores.", name);
    /* Find out if it's a primitive type or a struct */
    /* First check for primitives */
    primitive_t *primitive_type = 0;
    struct_t    *struct_type    = 0;
    for (uint i = 0; i < NUM_PRIMITIVES; ++i)
    {
        if (streq(primitives[i].name, type_name))
        {
            primitive_type  = &primitives[i];
            ret->type_index = i;
            break;
        }
    }
    /* Then check for structs */
    if (!primitive_type)
    {
        uint32 numstructs = darr_num(structs);
        for (uint32 i = 0; i < numstructs; ++i)
            if (streq(structs[i].name, type_name))
            {
                struct_type     = &structs[i];
                ret->type_index = NUM_PRIMITIVES + i;
                break;
            }
    }
    segpool_free(&segpool, type_name);
    if (!primitive_type && !struct_type)
        {error = 2; goto out;}
    /* Find numeric range brackets ().
     * The range brackets must come last on the line and be either empty or
     * contain specifiers for min, max, or both values. For example:
     * (min = 2, max = 4)
     * (max = 200) */
    const char *left_range_bracket, *right_range_bracket;
    if (_find_brackets_in_type_name(name, len, '(', ')', &left_range_bracket,
        &right_range_bracket))
    {
        if (struct_type)
            parser_panic("Bad use of range for type '%s': may not be used "
                "with structs.", name);
        uint32 range_len = (uint32)(right_range_bracket - left_range_bracket) -
            1;
        if (range_len)
        {
            /* min and max strs: "min = x" and "max = y". Null if none.
             * segpool-allocated. */
            char *min_str   = 0;
            char *max_str   = 0;
            char *range_str = segpool_malloc(&segpool, range_len + 1);
            memcpy(range_str, left_range_bracket + 1, range_len);
            range_str[range_len] = 0;
            char symbols[] = {' ', '\t', '\r', '\n'};
            str_strip_symbols(range_str, symbols, sizeof(symbols));
            int     float_signed_or_unsigned; /* 0, 1 or 2 */
            char    null_str[2];
            char    mm_strs[2][4] = {0};
            size_t  num = str_count_chars(range_str, ',');
            if (num > 1)
                parser_panic("Malformed min-max range contents in '%s'.",
                    name);
            if (!strncmp(range_str, "min", 3))
                strcpy(mm_strs[0], "min");
            else if (!strncmp(range_str, "max", 3))
                strcpy(mm_strs[0], "max");
            /* Its probably of format (min = x, max = y) */
            if (num == 1)
            {
                char *keyword2 = (char*)memchr(range_str, ',', range_len) + 1;
                if (!strncmp(keyword2, "min", 3))
                    strcpy(mm_strs[1], "min");
                else if (!strncmp(keyword2, "max", 3))
                    strcpy(mm_strs[1], "max");
                /* Create min_str and max_str.
                 * The '=' chars must be there so unsafe len doesn't matter. */
                char    *s[2];
                uint32  s_len[2];
                s[0] = (char*)memchr(range_str, '=', range_len) + 1;
                s[1] = (char*)memchr(s[0], '=', range_len) + 1;
                s_len[0] = (uint32)((char*)memchr(s[0], ',', range_len) - s[0]);
                s_len[1] = (uint32)strlen(s[1]);
                int min_index = -1;
                int max_index = -1;
                for (int i = 0; i < 2; ++i)
                {
                    if (streq(mm_strs[i], "min"))
                    {
                        if (min_index != -1)
                            parser_panic("Min range defined twice for '%s'.",
                                name);
                        min_index = i;
                    } else
                    if (streq(mm_strs[i], "max"))
                    {
                        if (max_index != -1)
                            parser_panic("Min range defined twice for '%s'.",
                                name);
                        max_index = i;
                    }
                }
                if (min_index == -1 || max_index == -1)
                    parser_panic("Malformed min-max range contents in "
                        "'%s'.", name);
                min_str = (char*)segpool_malloc(&segpool, s_len[min_index] + 1);
                max_str = (char*)segpool_malloc(&segpool, s_len[max_index] + 1);
                memcpy(min_str, s[min_index], s_len[min_index]);
                memcpy(max_str, s[max_index], s_len[max_index]);
                min_str[s_len[min_index]] = 0;
                max_str[s_len[max_index]] = 0;
            } else
            {
                /* Try out the singular format of min=x or max=y */
                num = sscanf(range_str, "%3s=%1s", mm_strs[0], null_str);
                if (num != 2)
                    parser_panic("Malformed range for type '%s'.", name);
                bool32 min_or_max;
                if (streq(mm_strs[0], "min"))
                    min_or_max = 0;
                else if (streq(mm_strs[0], "max"))
                    min_or_max = 1;
                else
                    parser_panic("Illegal keyword in range for '%s': "
                        "allowed keywords are 'min' and 'max'.", name);
                char    *s      = (char*)memchr(range_str, '=', range_len) + 1;
                uint32  s_len   = (uint32)strlen(s);
                char    **str   = min_or_max ? &max_str : &min_str;
                *str = segpool_malloc(&segpool, s_len + 1);
                memcpy(*str, s, s_len);
                (*str)[s_len] = 0;
            }
            if (primitive_type->is_float)
                float_signed_or_unsigned = 0;
            else if (primitive_type->is_signed)
                float_signed_or_unsigned = 1;
            else
                float_signed_or_unsigned = 2;
            if (min_str)
            {
                ret->have_min_range         = 1;
                ret->min_range.str          = min_str;
                ret->min_range.is_constant  = _is_maybe_constant(min_str);
                if (ret->min_range.is_constant)
                    _check_variable_or_constant_name(min_str);
                else
                    _init_range_num_from_str(&ret->min_range,
                        float_signed_or_unsigned);
            }
            if (max_str)
            {
                ret->have_max_range         = 1;
                ret->max_range.str          = max_str;
                ret->max_range.is_constant  = _is_maybe_constant(max_str);
                if (ret->max_range.is_constant)
                    _check_variable_or_constant_name(max_str);
                else
                    _init_range_num_from_str(&ret->max_range,
                        float_signed_or_unsigned);
            }
            /* Check range min and max value validity per type. */
            switch (float_signed_or_unsigned)
            {
            case 0: /* Float */
                if (ret->have_min_range && !ret->min_range.is_constant &&
                    ret->min_range.f < primitive_type->min_range.f)
                    parser_panic("Range min for '%s' too low: minimum"
                        "is %f.", name, primitive_type->min_range.f);
                if (ret->have_max_range && !ret->max_range.is_constant &&
                    ret->max_range.f > primitive_type->max_range.f)
                    parser_panic("Range max for '%s' too high: maximum "
                        "is %f.", name, primitive_type->max_range.f);
                break;
            case 1: /* Signed */
                if (ret->have_min_range && !ret->min_range.is_constant &&
                    ret->min_range.i < primitive_type->min_range.i)
                    parser_panic("Range min for '%s' too low: minimum"
                        "is %" PRId64 ".", name, primitive_type->min_range.i);
                if (ret->have_max_range && !ret->max_range.is_constant &&
                    ret->max_range.i > primitive_type->max_range.i)
                    parser_panic("Range max for '%s' too high: maximum "
                        "is %" PRId64 ".", name, primitive_type->max_range.i);
                break;
            case 2: /* Unsigned */
                if (ret->have_min_range && !ret->min_range.is_constant &&
                    ret->min_range.u < primitive_type->min_range.u)
                    parser_panic("Range min for '%s' too low: minimum"
                        "is %" PRIu64 ".", name, primitive_type->min_range.u);
                if (ret->have_max_range && !ret->max_range.is_constant &&
                    ret->max_range.u > primitive_type->max_range.u)
                    parser_panic("Range max for '%s' too high: maximum "
                        "is %" PRIu64 ".", name, primitive_type->max_range.u);
                break;
            }
            segpool_free(&segpool, range_str);
        } /* End of range check */
    }
    error = 0;
    out:;
        return error;
}

static primitive_t *
_find_primitive(const char *name)
{
    for (uint i = 0; i < NUM_PRIMITIVES; ++i)
        if (streq(primitives[i].name, name))
            return &primitives[i];
    return 0;
}

static struct_t *
_find_struct(const char *name)
{
    uint32 num = darr_num(structs);
    for (uint32 i = 0; i < num; ++i)
        if (streq(name, structs[i].name))
            return &structs[i];
    return 0;
}

static struct_member_t *
_find_struct_member(struct_t *s, const char *name)
{
    struct_member_t *members    = s->members;
    uint32          num_members = darr_num(members);
    for (uint32 i = 0; i < num_members; ++i)
        if (streq(members[i].name, name))
            return &members[i];
    return 0;
}

static packet_member_t *
_find_packet_member(packet_t *packet, const char *name)
{
    packet_member_t *members    = packet->members;
    uint32          num_members = darr_num(members);
    for (uint32 i = 0; i < num_members; ++i)
        if (streq(members[i].name, name))
            return &members[i];
    return 0;
}

static group_t *
_find_group(const char *name)
{
    uint32 num_groups = darr_num(groups);
    for (uint32 i = 0; i < num_groups; ++i)
        if (streq(groups[i].name, name))
            return &groups[i];
    return 0;
}

static bool32
_is_maybe_constant(const char *value)
{
    if (str_is_int(value))
        return 0;
    if (str_is_float(value))
        return 0;
    return 1;
}

static void
_init_range_num_from_str(range_t *range, int float_signed_or_unsigned)
{
    int num;
    switch (float_signed_or_unsigned)
    {
    case 0: /* Float */
        num = sscanf(range->str, "%lf", &range->f);
        break;
    case 1: /* Signed */
        num = sscanf(range->str, "%" PRId64, &range->i);
        break;
    case 2: /* Unsigned */
        num = sscanf(range->str, "%" PRIu64, &range->u);
        break;
    default:
        num = 0;
        muta_assert(0);
    }
    muta_assert(num == 1);
}
