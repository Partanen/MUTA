#ifndef MUTA_PACKETWRITER2_COMMON_H
#define MUTA_PACKETWRITER2_COMMON_H

#include "../../../shared/common_utils.h"

#define INVALID_TYPE_INDEX 0xFFFFFFFF
#define printf_verbose(fmt, ...) \
    (verbose_printing_on ? (void)printf(fmt, ##__VA_ARGS__) : (void)0)

#define VAR_ARRAY_DEFAULT_LEN_TYPE PRIMITIVE_UINT16
/* VAR_ARRAY_DEFAULT_LEN_TYPE
 * Used if variable size array's max size is a constant and no length type is
 * explicitly specified. */

typedef struct range_t          range_t;
typedef struct primitive_t      primitive_t;
typedef struct group_t          group_t;
typedef struct struct_t         struct_t;
typedef struct complete_type_t  complete_type_t;
typedef struct struct_member_t  struct_member_t;
typedef struct include_t        include_t;
typedef struct packet_member_t  packet_member_t;
typedef struct packet_t         packet_t;

enum primitive
{
    PRIMITIVE_INT8 = 0,
    PRIMITIVE_UINT8,
    PRIMITIVE_INT16,
    PRIMITIVE_UINT16,
    PRIMITIVE_INT32,
    PRIMITIVE_UINT32,
    PRIMITIVE_INT64,
    PRIMITIVE_UINT64,
    PRIMITIVE_FLOAT,
    PRIMITIVE_DOUBLE,
    PRIMITIVE_CHAR,
    NUM_PRIMITIVES
};

enum array_type
{
    ARRAY_TYPE_NONE,
    ARRAY_TYPE_FIX,
    ARRAY_TYPE_VAR
};

struct range_t
{
    bool32  is_constant;
    char    *str;
    union
    {
        int64   i;
        uint64  u;
        double  f;
    };
};

struct primitive_t
{
    const char  *name;
    bool32      is_signed;
    bool32      is_float;
    range_t     min_range;
    range_t     max_range;
    uint64      size;
};

struct group_t
{
    dchar       *name;
    dchar       *first_opcode;  /* String, can be a constant */
    uint32      opcode_type;    /* 0xFFFFFFFF if none. */
    uint32      *packets;       /* darr */
};

struct struct_t
{
    dchar           *name;
    struct_member_t *members; /* darr */
};

struct complete_type_t
{
    uint32          type_index;
    /* type_index
     * If < NUM_PRIMITIVES, this is one of the primitives. If >= NUM_PRIMITIVES,
     * it is structs[type_index - NUM_PRIMITIVES]. */
    enum array_type array_type;
    bool32          have_min_range;
    bool32          have_max_range;
    range_t         min_range;
    range_t         max_range;
    char            *array_min;
    char            *array_max;
    uint32          length_type_override;
    /* length_type_override
     * 0xFFFFFFFF if none. Otherwise, if this is a variable size array and an
     * array length type was defined with the <type> notation, an index of a
     * primitive unsigned type. */
};

struct struct_member_t
{
    dchar           *name;
    complete_type_t complete_type;
};

struct include_t
{
    dchar   *path;
    bool32  relative_path;          /* Use "" or <>? */
    bool32  explicit_relative_path; /* To check if option is defined > 1 */
};

struct packet_member_t
{
    dchar           *name;
    complete_type_t complete_type;
};

struct packet_t
{
    dchar           *name;
    uint32          group; /* 0xFFFFFFFF if none */
    packet_member_t *members;
    bool32          encrypt;
    bool32          encrypt_defined;
};

extern primitive_t primitives[NUM_PRIMITIVES];

extern struct_t     *structs;
extern group_t      *groups;
extern include_t    *includes;
extern packet_t     *packets;
extern dchar        *include_guard;
/* structs, groups, includes, packets and the include guard string are filled
 * during the parser phase. */

extern segpool_t segpool;
/* segpool
 * For temporary memory allocations. */

extern bool32 verbose_printing_on;


void
init_common(void);

#endif /* MUTA_PACKETWRITER2_COMMON_H */
