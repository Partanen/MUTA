#include "common.h"
#include "../../../shared/get_opt.h"

static get_opt_context_t _opts = GET_OPT_CONTEXT_INITIALIZER;

void
parse(const char *file_path);

void
generate(const char *file_path);

static void
_print_usage(void);

int main(int argc, char **argv)
{
    if (argc == 1)
    {
        _print_usage();
        return 0;
    }
    const char  *opts   = "i:o:vh";
    const char  *in_fp  = 0;
    const char  *out_fp = 0;
    int         opt;
    while ((opt = get_opt(&_opts, argc, argv, opts)) != -1)
    {
        switch (opt)
        {
        case 'i':
            if (in_fp)
                muta_panic_print("Input file defined twice.");
            in_fp = _opts.arg;
            break;
        case 'o':
            if (out_fp)
                muta_panic_print("Output file defined twice.");
            out_fp = _opts.arg;
            break;
        case 'v':
            verbose_printing_on = 1;
            break;
        case 'h':
            _print_usage();
            return 0;
        case '?':
            muta_panic_print("Unknown command line option.");
            break;
        }
    }
    if (!in_fp)
        muta_panic_print("No input file passed.");
    if (out_fp && streq(in_fp, out_fp))
        muta_panic_print("Input and output file paths cannot be the same.");
    init_common();
    parse(in_fp);
    if (out_fp)
        generate(out_fp);
    return 0;
}

static void
_print_usage(void)
{
    puts(
        "packetwriter2\n"
        "Example: packetwriter2 -i packets.def -o packets.h\n"
        "-h:        Print this dialogue.\n"
        "-i [PATH]: Path to input file.\n"
        "-o [PATH]: Path to output file.\n"
        "-v:        Verbose output.");
}
