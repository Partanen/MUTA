instructions for MUTA_packetwriter v1.0 written by Lommi

HOW TO USE:
-in command line in correct folder type and run:

main.exe [source file path] [destination file path]

^	 	^	  		^
|	 	|	  		|

file	 	1. arg	  		2. arg
to	 	source   		destination
run	 	file	  		file

for example: main.exe source.h destination.h

-the program reads pseudo code from source file and writes working
source code to destination file based on the source

main.exe = executes the program
source.h = pseudo code file that the packetwriter reads
destination.h = actual working code for MUTA, written on the base of source.h
*****************************************************************************
Source.h syntax

types:
int8[]
int8
int16
int32
int64
uint8
uint16
uint32
uint64
int -10000-20000
uint 0-20000
varchar

******* EXAMPLE SOURCE CODE *************************************************
type_create: muta_svmsg_type -------------------------------------------> declare the packet type
type_create: muta_client_msg_type

type_set: muta_server_msg_type 1 ---------------------------------------> declare where the packets below belong to, the enums are started from the number at the end

packet: svmsg_login_accept SVMSG_LOGIN_ACCEPT --------------------------> declare packet. give name, give title
    uint32: token ------------------------------------------------------> declare data inside packet. "type: name"
    uint16: token
packet: svmsg_player_init_data SVMSG_PLAYER_INIT_DATA
    uint32: entity_id
packet: svmsg_global_chat_broadcast SVMSG_GLOBAL_CHAT_BROADCAST
    varchar: name
    varchar: msg
packet: ebin SVMSG_EBIN 
    [WRITEONLY] --------------------------------------------------------> only the write function is created for this packet
packet: ebin SVMSG_EBIN 
    [CRYPTED] ----------------------------------------------------------> creates elements needed for crypted packets
    varchar: msg
type_set: muta_client_msg_type 1 ---------------------------------------> declare where the packets below belong to
packet: clmsg_player_init_data SVMSG_PLAYER_INIT_DATA
    uint32: entity_id
...
...
