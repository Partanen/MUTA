/*THIS FILE IS USED TO GENERATE THE PACKETS FOR MUTA! DO NOT CHANGE ANYTHIIIIING!*/

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <limits.h>
#include <string.h>
#include <ctype.h>

#define OP_CODE_CHUNK 2
#define TYPEDEF_CHUNK 2
#define PACKETS_CHUNK 64
#define DATA_CHUNK 16

enum packet_type_t
{
    PACKET_TYPE_NORMAL = 0,
    PACKET_TYPE_WRITEONLY
};

enum vartypes_t
{
    VARTYPE_NORMAL = 0,
    VARTYPE_ARRAY,
    VARTYPE_CHAR,
    VARTYPE_FIXCHAR,
    VARTYPE_VARLEN
};

enum crypt_types_t
{
    CRYPT_NONE = 0,
    CRYPT_VAR,
    CRYPT_CONST
};

typedef struct
{
    char name[32];
    char type[16];
} typedef_t;

typedef struct
{
    char name[32];
    char limit[32];
    char type[16];
} data_t;

typedef struct
{
    char   title[64];
    char   name[64];
    char   op_code[32];

    int    type;
    int    num_total_data;
    int    num_data;
    int    num_arraydata;
    int    num_chardata;
    int    num_fixchardata;
    int    num_varlendata;
    int    crypt;

    data_t *data;
    data_t *arraydata;
    data_t *chardata;
    data_t *fixchardata;
    data_t *varlendata;
} op_code_packet_t;

typedef struct
{
    char name[32];
    char title[32];
    char  start_val[32];
    int  num_packets;
    op_code_packet_t *packets;
} op_code_t;

FILE             *fs, *fd; /*fs = source file. fd = destination file*/
size_t           nread = 0; /*Used for reading the header*/
char             *header_buf; /*for storing the header text from source*/
char             line[512];
char             typedef_buf[32], typedef_msg_buf[32];
char             buf1[64], buf2[64], buf3[64];
char             cur_op_code[32];
char             *p; /*used to scroll through individual chars in buffers*/
char             tmp;
int              datac           = 0; /*used to calculate different data together*/
int              packetsc        = 0; /*used to calculate different data together*/
int              num_packets     = 0;
int              num_op_codes    = 0;
int              num_typedefs    = 0;
int              packet_index    = -1;
int              op_code_index   = -1;
int              data_index      = 0;
int              val1, val2;
int              c, cval; /*used for reading the header*/
unsigned int     uval1, uval2;
op_code_t        *op_codes;
typedef_t        *typedefs;

void add_data(const char *s_datatype, int vartype);

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        /*argv[0] program name*/
        printf("\nSource or Destination file not defined. Shutting down %s.\n", argv[0]);
    }
    else
    {
        /*argv[1] = source file*/
        fs = fopen(argv[1], "r+");
        /*argv[2] = destination file*/
        fd = fopen(argv[2], "w+");

        if (!fs)
        {
            printf("Failed to open %s. Shutting down.", argv[1]);

            return 1;
        }
        else if (!fd)
        {
            printf("Failed to open %s. Shutting down.", argv[2]);
            return 2;
        }
        else
        {
            header_buf = (char*)malloc(sizeof(char));
            typedefs = (typedef_t*)malloc(sizeof(typedef_t) * TYPEDEF_CHUNK);
            op_codes = (op_code_t*)malloc(sizeof(op_code_t) * OP_CODE_CHUNK);
//READ SOURCE FILE ONCE
            while (fgets(line, 256, fs))
            {
                if (sscanf(line, "header:%c", &tmp))
                {
                    while ((c = fgetc(fs)) != '$')
                    {
                        cval = nread + 1;
                        header_buf = (char*)realloc(header_buf, cval * sizeof(char));
                        header_buf[nread++] = (char)c;
                    }
                    header_buf = realloc(header_buf, nread + 1);
                    header_buf[nread] = '\0';
                    printf("%s\n", header_buf);
                }
                if (sscanf(line, "typedef: %s %s", buf1, buf2))
                {
                    strcpy(typedefs[num_typedefs].type, buf1);
                    strcpy(typedefs[num_typedefs].name, buf2);
                    ++num_typedefs;
                    typedefs = (typedef_t*)realloc(typedefs, sizeof(typedef_t) * (TYPEDEF_CHUNK * num_typedefs));
                    puts("typedef");
                }
                if (sscanf(line, "typedef_set: %s %s", buf1, buf2))
                {
                    strcpy(typedef_buf, buf1);
                    strcpy(typedef_msg_buf, buf2);
                    puts("typedef_set");
                }
                if (sscanf(line, "type_create: %s %s", buf1, buf2))
                {
                    op_codes[num_op_codes].packets = malloc(
                        sizeof(op_codes[num_op_codes].packets) * PACKETS_CHUNK);
                    op_codes[num_op_codes].num_packets = 0;
                    strcpy(op_codes[num_op_codes].name, buf1);
                    strcpy(op_codes[num_op_codes].title, buf2);
                    ++num_op_codes;
                    op_codes = (op_code_t*)realloc(op_codes, sizeof(op_code_t) * (OP_CODE_CHUNK * num_op_codes));
                    puts("type_create");
                }
                if (sscanf(line, "type_set: %s %s", buf1, buf2))
                {
                    ++op_code_index;
                    strcpy(op_codes[op_code_index].start_val, buf2);
                    packet_index = -1;
                    strcpy(cur_op_code, buf1);
                    puts("type_set");
                }
                if (sscanf(line, "packet: %s", buf1))
                {
                    ++packet_index;
                    ++op_codes[op_code_index].num_packets;
                    data_index = 0;

                    op_codes[op_code_index].packets[packet_index].data = (data_t*)malloc(
                        sizeof(data_t) * DATA_CHUNK);
                    op_codes[op_code_index].packets[packet_index].arraydata = (data_t*)malloc(
                        sizeof(data_t) * DATA_CHUNK);
                    op_codes[op_code_index].packets[packet_index].chardata = (data_t*)malloc(
                        sizeof(data_t) * DATA_CHUNK);
                    op_codes[op_code_index].packets[packet_index].fixchardata = (data_t*)malloc(
                        sizeof(data_t) * DATA_CHUNK);
                    op_codes[op_code_index].packets[packet_index].varlendata = (data_t*)malloc(
                        sizeof(data_t) * DATA_CHUNK);

                    op_codes[op_code_index].packets[packet_index].type = PACKET_TYPE_NORMAL;
                    op_codes[op_code_index].packets[packet_index].num_total_data = 0;
                    op_codes[op_code_index].packets[packet_index].num_data = 0;
                    op_codes[op_code_index].packets[packet_index].num_arraydata = 0;
                    op_codes[op_code_index].packets[packet_index].num_chardata = 0;
                    op_codes[op_code_index].packets[packet_index].num_fixchardata = 0;
                    op_codes[op_code_index].packets[packet_index].num_varlendata = 0;
                    op_codes[op_code_index].packets[packet_index].crypt = CRYPT_NONE;
                    strcpy(op_codes[op_code_index].packets[packet_index].op_code, cur_op_code);
                    strcpy(op_codes[op_code_index].packets[packet_index].name, buf1);

                    strcpy(buf2, buf1);
                    p = buf2;
                    while (*p){ if (islower(*p)) *p = toupper(*p); ++p; }
                    strcpy(op_codes[op_code_index].packets[packet_index].title, buf2);

                    op_codes[op_code_index].packets = (op_code_packet_t*)realloc(
                        op_codes[op_code_index].packets,
                        sizeof(op_code_packet_t) * (PACKETS_CHUNK * op_codes[op_code_index].num_packets));
                    puts("packet");
                }
                if (sscanf(line, "    [%s]", buf1))
                {
                    if (strcmp(buf1, "WRITEONLY]") == 0)
                        op_codes[op_code_index].packets[packet_index].type = PACKET_TYPE_WRITEONLY;
                    if (strcmp(buf1, "CC]") == 0)
                        op_codes[op_code_index].packets[packet_index].crypt = CRYPT_CONST;
                    if (strcmp(buf1, "CV]") == 0)
                        op_codes[op_code_index].packets[packet_index].crypt = CRYPT_VAR;
                }
                if (sscanf(line, "    varchar8: %s %s", buf1, buf2))
                {
                    strcpy(buf3, buf1);
                    sprintf(buf1, "%s_len", buf1);
                    add_data("uint8", VARTYPE_VARLEN);
                    strcpy(buf1, buf3);
                    add_data("const char", VARTYPE_CHAR);
                }
                if (sscanf(line, "    varchar16: %s %s", buf1, buf2))
                {
                    strcpy(buf3, buf1);
                    sprintf(buf1, "%s_len", buf1);
                    add_data("uint16", VARTYPE_VARLEN);
                    strcpy(buf1, buf3);
                    add_data("const char", VARTYPE_CHAR);
                }
                if (sscanf(line, "    uint8*: %s %s", buf1, buf2))
                {
                    strcpy(buf3, buf1);
                    sprintf(buf1, "%s_len", buf1);
                    add_data("uint8", VARTYPE_VARLEN);
                    strcpy(buf1, buf3);
                    add_data("uint8", VARTYPE_ARRAY);
                }
                if (sscanf(line, "    uint16*: %s %s", buf1, buf2))
                {
                    strcpy(buf3, buf1);
                    sprintf(buf1, "%s_len", buf1);
                    add_data("uint16", VARTYPE_VARLEN);
                    strcpy(buf1, buf3);
                    add_data("uint16", VARTYPE_ARRAY);
                }
                if (sscanf(line, "    uint32*: %s %s", buf1, buf2))
                {
                    strcpy(buf3, buf1);
                    sprintf(buf1, "%s_len", buf1);
                    add_data("uint32", VARTYPE_VARLEN);
                    strcpy(buf1, buf3);
                    add_data("uint32", VARTYPE_ARRAY);
                }
                if (sscanf(line, "    uint64*: %s %s", buf1, buf2))
                {
                    strcpy(buf3, buf1);
                    sprintf(buf1, "%s_len", buf1);
                    add_data("uint64", VARTYPE_VARLEN);
                    strcpy(buf1, buf3);
                    add_data("uint64", VARTYPE_ARRAY);
                }
                if (sscanf(line, "    int8*: %s %s", buf1, buf2))
                {
                    strcpy(buf3, buf1);
                    sprintf(buf1, "%s_len", buf1);
                    add_data("int8", VARTYPE_VARLEN);
                    strcpy(buf1, buf3);
                    add_data("int8", VARTYPE_ARRAY);
                }
                if (sscanf(line, "    int16*: %s %s", buf1, buf2))
                {
                    strcpy(buf3, buf1);
                    sprintf(buf1, "%s_len", buf1);
                    add_data("int16", VARTYPE_VARLEN);
                    strcpy(buf1, buf3);
                    add_data("int16", VARTYPE_ARRAY);
                }
                if (sscanf(line, "    int32*: %s %s", buf1, buf2))
                {
                    strcpy(buf3, buf1);
                    sprintf(buf1, "%s_len", buf1);
                    add_data("int32", VARTYPE_VARLEN);
                    strcpy(buf1, buf3);
                    add_data("int32", VARTYPE_ARRAY);
                }
                if (sscanf(line, "    int64*: %s %s", buf1, buf2))
                {
                    strcpy(buf3, buf1);
                    sprintf(buf1, "%s_len", buf1);
                    add_data("int64", VARTYPE_VARLEN);
                    strcpy(buf1, buf3);
                    add_data("int64", VARTYPE_ARRAY);
                }
                if (sscanf(line, "    uint8[]: %s %s", buf1, buf2))
                    add_data("uint8", VARTYPE_FIXCHAR);
                if (sscanf(line, "    double: %s", buf1))
                    add_data("int64", VARTYPE_NORMAL);
                if (sscanf(line, "    int64: %s", buf1))
                    add_data("int64", VARTYPE_NORMAL);
                if (sscanf(line, "    uint64: %s", buf1))
                    add_data("uint64", VARTYPE_NORMAL);
                if (sscanf(line, "    uint32: %s", buf1))
                    add_data("uint32", VARTYPE_NORMAL);
                if (sscanf(line, "    int32: %s", buf1))
                    add_data("int32", VARTYPE_NORMAL);
                if (sscanf(line, "    float: %s", buf1))
                    add_data("float", VARTYPE_NORMAL);
                if (sscanf(line, "    uint16: %s", buf1))
                    add_data("uint16", VARTYPE_NORMAL);
                if (sscanf(line, "    int16: %s", buf1))
                    add_data("int16", VARTYPE_NORMAL);
                if (sscanf(line, "    int8: %s", buf1))
                    add_data("int8", VARTYPE_NORMAL);
                if (sscanf(line, "    uint8: %s", buf1))
                    add_data("uint8", VARTYPE_NORMAL);
                if (sscanf(line, "    uint: %s %d-%d", buf1, &val1, &val2))
                {
                    if (val1 >= 0 && val2 <= UINT_MAX && val2 > USHRT_MAX)
                        add_data("uint32", VARTYPE_NORMAL);
                    else if (val1 >= 0 && val2 <= USHRT_MAX && val2 > UCHAR_MAX)
                        add_data("uint16", VARTYPE_NORMAL);
                    else if (val1 >= 0 && val2 <= UCHAR_MAX && val2 > 0)
                        add_data("uint8", VARTYPE_NORMAL);
                }
                if (sscanf(line, "    int: %s -%d-%d", buf1, &val1, &val2))
                {
                    val1 *= -1;
                    if (val1 >= INT_MIN && val1 < 0 && val2 <= INT_MAX && val2 > SHRT_MAX)
                        add_data("int32", VARTYPE_NORMAL);
                    else if (val1 >= SHRT_MIN && val1 < 0 && val2 <= SHRT_MAX && val2 > CHAR_MAX)
                        add_data("int16", VARTYPE_NORMAL);
                    else if (val1 >= CHAR_MIN && val1 < 0 && val2 <= CHAR_MAX && val2 > 0)
                        add_data("int8", VARTYPE_NORMAL);
                }
            }

/*WRITE EVERYTHING TO DESTINATION FILE*/
/*WRITE TYPEDEFS FIRST*/
            for (int i = 0; i < num_typedefs; ++i)
            {
                fprintf(fd, "typedef %s %s;\n", typedefs[i].type, typedefs[i].name);
            }
            for (int i = 0; i < num_typedefs; ++i)
            {
                fprintf(fd, "#define %s sizeof(%s)\n", typedef_buf, typedefs[i].name);
            }
/*HEADER AT THE TOP*/
            fprintf(fd, header_buf);
/*ENUMS*/
            for (int i = 0; i < num_op_codes; ++i)
            {
                fprintf(fd, "enum %s\n{\n", op_codes[i].name);
                p = op_codes[i].name;
                while (*p){ if (islower(*p)) *p = toupper(*p); ++p; }
                for (int j = 0; j < op_codes[i].num_packets; ++j)
                {
                    if (j == 0 && op_codes[i].num_packets == 0)
                        fprintf(fd, "    %s = %s,\n\n    NUM_%s\n", op_codes[i].packets[j].title,
                            op_codes[i].start_val, op_codes[i].name);
                    else if (j == 0 && op_codes[i].num_packets > 0)
                        fprintf(fd, "    %s = %s,\n", op_codes[i].packets[j].title,
                            op_codes[i].start_val);
                    else if (j == op_codes[i].num_packets - 1)
                    {
                        fprintf(fd, "    %s,\n\n    NUM_%s\n", op_codes[i].packets[j].title, op_codes[i].name);
                    }
                    else
                        fprintf(fd, "    %s,\n", op_codes[i].packets[j].title);

                    p = op_codes[i].name;
                }
                fprintf(fd, "};\n\n#if NUM_%s > 255\n\
#   error too many %s types\n\
#endif\n\n\
MSG_WRITE_PREP_DEFINITION(%s, %s, WRITE_%s_TYPE);\n\n",
op_codes[i].name, op_codes[i].name, op_codes[i].title, typedef_buf, typedef_msg_buf);
            }

/*PACKET STRUCT*/
            for (int i = 0; i < num_op_codes; ++i)
            {
                for (int j = 0; j < op_codes[i].num_packets; ++j)
                {
                    if (op_codes[i].packets[j].type != PACKET_TYPE_WRITEONLY)
                    {
                        fprintf(fd, "typedef struct\n{\n");
                        for (int k = 0; k < op_codes[i].packets[j].num_varlendata; ++k) /*length variables should always come first*/
                        {
                            fprintf(fd, "    %s %s;\n", op_codes[i].packets[j].varlendata[k].type,
                                 op_codes[i].packets[j].varlendata[k].name);
                        }
                        for (int k = 0; k < op_codes[i].packets[j].num_data; ++k)
                        {
                            fprintf(fd, "    %s %s;\n", op_codes[i].packets[j].data[k].type,
                                 op_codes[i].packets[j].data[k].name);
                        }
                        for (int k = 0; k < op_codes[i].packets[j].num_chardata; ++k)
                        {
                            fprintf(fd, "    %s *%s;\n", op_codes[i].packets[j].chardata[k].type,
                                 op_codes[i].packets[j].chardata[k].name);
                        }
                        for (int k = 0; k < op_codes[i].packets[j].num_fixchardata; ++k)
                        {
                            fprintf(fd, "    %s %s[%s];\n", op_codes[i].packets[j].fixchardata[k].type,
                                 op_codes[i].packets[j].fixchardata[k].name,
                                 op_codes[i].packets[j].fixchardata[k].limit);
                        }
                        for (int k = 0; k < op_codes[i].packets[j].num_arraydata; ++k)
                        {
                            fprintf(fd, "    %s *%s;\n", op_codes[i].packets[j].arraydata[k].type,
                                 op_codes[i].packets[j].arraydata[k].name);
                        }
                        fprintf(fd, "} %s_t;\n\n", op_codes[i].packets[j].name);

                        fprintf(fd, "#define %s_SZ ", op_codes[i].packets[j].title);
                        packetsc = 0;
                        fprintf(fd, "(");
                        datac = op_codes[i].packets[j].num_varlendata +
                            op_codes[i].packets[j].num_fixchardata +
                            op_codes[i].packets[j].num_data - 1;
                        for (int k = 0; k < op_codes[i].packets[j].num_varlendata; ++k)
                        {
                            if (packetsc == datac)
                                fprintf(fd, "sizeof(%s)", op_codes[i].packets[j].varlendata[k].type);
                            else
                                fprintf(fd, "sizeof(%s) + ", op_codes[i].packets[j].varlendata[k].type);
                            ++packetsc;
                        }
                        for (int k = 0; k < op_codes[i].packets[j].num_data; ++k)
                        {
                            if (packetsc == datac)
                                fprintf(fd, "sizeof(%s)", op_codes[i].packets[j].data[k].type);
                            else
                                fprintf(fd, "sizeof(%s) + ", op_codes[i].packets[j].data[k].type);
                            ++packetsc;
                        }
                        for (int k = 0; k < op_codes[i].packets[j].num_fixchardata; ++k)
                        {
                            if (packetsc == datac)
                                fprintf(fd, "%s", op_codes[i].packets[j].fixchardata[k].limit);
                            else
                                fprintf(fd, "%s + ", op_codes[i].packets[j].fixchardata[k].limit);
                            ++packetsc;
                        }
                        fprintf(fd, ")\n\n");

/*Create compute definitions for pointer type variables*/
                        if (op_codes[i].packets[j].num_chardata > 0 ||
                            op_codes[i].packets[j].num_arraydata > 0)
                        {
                            fprintf(fd, "#define %s_COMPUTE_SZ(", op_codes[i].packets[j].title);
                            packetsc = 0;
                            datac = op_codes[i].packets[j].num_chardata +
                                op_codes[i].packets[j].num_arraydata - 1;
                            for (int k = 0; k < op_codes[i].packets[j].num_chardata; ++k)
                            {
                                if (packetsc == datac)
                                    fprintf(fd, "%s_len) \\", op_codes[i].packets[j].chardata[k].name);
                                else
                                    fprintf(fd, "%s_len, ", op_codes[i].packets[j].chardata[k].name);
                                ++packetsc;
                            }
                            for (int k = 0; k < op_codes[i].packets[j].num_arraydata; ++k)
                            {
                                if (packetsc == datac)
                                    fprintf(fd, "%s_len) \\", op_codes[i].packets[j].arraydata[k].name);
                                else
                                    fprintf(fd, "%s_len, ", op_codes[i].packets[j].arraydata[k].name);
                                ++packetsc;
                            }
                            fprintf(fd, "\n    (%s_SZ + ", op_codes[i].packets[j].title);
                            packetsc = 0;
                            for (int k = 0; k < op_codes[i].packets[j].num_chardata; ++k)
                            {
                                if (packetsc == datac)
                                    fprintf(fd, "(%s_len))\n\n", op_codes[i].packets[j].chardata[k].name);
                                else
                                    fprintf(fd, "(%s_len) + ", op_codes[i].packets[j].chardata[k].name);
                                ++packetsc;
                            }
                            for (int k = 0; k < op_codes[i].packets[j].num_arraydata; ++k)
                            {
                                if (packetsc == datac)
                                    fprintf(fd, "(%s_len * sizeof(%s)))\n\n", op_codes[i].packets[j].arraydata[k].name,
                                        op_codes[i].packets[j].arraydata[k].type);
                                else
                                    fprintf(fd, "(%s_len * sizeof(%s)) + ", op_codes[i].packets[j].arraydata[k].name,
                                        op_codes[i].packets[j].arraydata[k].type);
                                ++packetsc;
                            }
                            fprintf(fd, "#define %s_MAX_SZ \\\n", op_codes[i].packets[j].title);
                            fprintf(fd, "    (%s_COMPUTE_SZ(", op_codes[i].packets[j].title);
                            packetsc = 0;
                            for (int k = 0; k < op_codes[i].packets[j].num_chardata; ++k)
                            {
                                p = op_codes[i].packets[j].chardata[k].name;
                                while (*p){ if (islower(*p)) *p = toupper(*p); ++p; }
                                if (packetsc == datac)
                                    fprintf(fd, "%s))\n\n", op_codes[i].packets[j].chardata[k].limit);
                                else
                                {
                                    fprintf(fd, "%s, ", op_codes[i].packets[j].chardata[k].limit);
                                }
                                ++packetsc;

                                p = op_codes[i].packets[j].chardata[k].name;
                                while (*p){ if (isupper(*p)) *p = tolower(*p); ++p; }
                            }
                            for (int k = 0; k < op_codes[i].packets[j].num_arraydata; ++k)
                            {
                                p = op_codes[i].packets[j].arraydata[k].name;
                                while (*p){ if (islower(*p)) *p = toupper(*p); ++p; }
                                if (packetsc == datac)
                                    fprintf(fd, "%s))\n\n", op_codes[i].packets[j].arraydata[k].limit);
                                else
                                    fprintf(fd, "%s, ", op_codes[i].packets[j].arraydata[k].limit);
                                ++packetsc;
                                p = op_codes[i].packets[j].arraydata[k].name;
                                while (*p){ if (isupper(*p)) *p = tolower(*p); ++p; }
                            }
                        }
                    }
/*WRITE*/
                    if (op_codes[i].packets[j].type == PACKET_TYPE_WRITEONLY)
                    {
                        fprintf(fd, "static inline int\n%s_write(byte_buf_t *buf)\n{\n\
    uint8 *mem = bbuf_reserve(buf, %s);\n\
    if (!mem) return 1;\n    WRITE_%s_TYPE(mem, %s);\n",
        op_codes[i].packets[j].name, typedef_buf, typedef_msg_buf, op_codes[i].packets[j].title);
                    }
                    else if (op_codes[i].packets[j].type == PACKET_TYPE_NORMAL)
                    {
                        if (op_codes[i].packets[j].crypt == CRYPT_VAR)
                        {
                            packetsc = 0;
                            fprintf(fd, "static inline int\n%s_write_var_encrypted(byte_buf_t *buf, \
cryptchan_t *cc,\n    %s_t *s)\n{\n", op_codes[i].packets[j].name, op_codes[i].packets[j].name);
                            fprintf(fd, "    int sz = %s_COMPUTE_SZ(", op_codes[i].packets[j].title);
                            for (int k = 0; k < op_codes[i].packets[j].num_chardata; ++k)
                            {
                                 if (packetsc == datac)
                                     fprintf(fd, "s->%s_len);\n", op_codes[i].packets[j].chardata[k].name);
                                 else
                                     fprintf(fd, "s->%s_len, ", op_codes[i].packets[j].chardata[k].name);
                                 ++packetsc;
                            }
                            for (int k = 0; k < op_codes[i].packets[j].num_arraydata; ++k)
                            {
                                 if (packetsc == datac)
                                     fprintf(fd, "s->%s_len);\n", op_codes[i].packets[j].arraydata[k].name);
                                 else
                                     fprintf(fd, "s->%s_len, ", op_codes[i].packets[j].arraydata[k].name);
                                 ++packetsc;
                            }
                            fprintf(fd, "    if (sz > %s_MAX_SZ)\n        return -1;\n\n",
                                op_codes[i].packets[j].title);
                            fprintf(fd, "    uint8 *mem = prep_%s_write_var_encrypted(buf, %s, sz);\n",
                                op_codes[i].title, op_codes[i].packets[j].title);
                            fprintf(fd, "    if (!mem) return 1;\n\n");
                        }
                        else if (op_codes[i].packets[j].crypt == CRYPT_CONST)
                        {
                            fprintf(fd, "static inline int\n%s_write_const_encrypted(byte_buf_t *buf, \
cryptchan_t *cc,\n    %s_t *s)\n{\n", op_codes[i].packets[j].name, op_codes[i].packets[j].name);
                            fprintf(fd, "    uint8 *mem = bbuf_reserve(buf, %s + \\\n\
        CRYPT_MSG_ADDITIONAL_BYTES + %s_SZ);\n    if (!mem) return 1;\n    WRITE_%s_TYPE(mem, %s);\n",
                                typedef_buf, op_codes[i].packets[j].title,
                                typedef_msg_buf, op_codes[i].packets[j].title);
                        }
                        else if (op_codes[i].packets[j].num_chardata > 0 ||
                                 op_codes[i].packets[j].num_arraydata > 0)
                        {
                            fprintf(fd, "static inline int\n%s_write(byte_buf_t *buf, %s_t *s)\n{\n",
                                     op_codes[i].packets[j].name, op_codes[i].packets[j].name);
                            packetsc = 0;
                            fprintf(fd, "    int sz = %s_COMPUTE_SZ(", op_codes[i].packets[j].title);
                            for (int k = 0; k < op_codes[i].packets[j].num_chardata; ++k)
                            {
                                 if (packetsc == datac)
                                     fprintf(fd, "s->%s_len);\n", op_codes[i].packets[j].chardata[k].name);
                                 else
                                     fprintf(fd, "s->%s_len, ", op_codes[i].packets[j].chardata[k].name);
                                 ++packetsc;
                            }
                            for (int k = 0; k < op_codes[i].packets[j].num_arraydata; ++k)
                            {
                                 if (packetsc == datac)
                                     fprintf(fd, "s->%s_len);\n", op_codes[i].packets[j].arraydata[k].name);
                                 else
                                     fprintf(fd, "s->%s_len, ", op_codes[i].packets[j].arraydata[k].name);
                                 ++packetsc;
                            }
                            fprintf(fd, "    if (sz > %s_MAX_SZ)\n        return -1;\n\n",
                                op_codes[i].packets[j].title);

                            fprintf(fd, "    uint8 *mem = bbuf_reserve(buf, %s + \\\n\
        %s_COMPUTE_SZ(",
                                typedef_buf, op_codes[i].packets[j].title);
                            packetsc = 0;
                            for (int k = 0; k < op_codes[i].packets[j].num_chardata; ++k)
                            {
                                    if (packetsc == datac)
                                        fprintf(fd, "s->%s_len));\n", op_codes[i].packets[j].chardata[k].name);
                                    else
                                        fprintf(fd, "s->%s_len, ", op_codes[i].packets[j].chardata[k].name);
                                    ++packetsc;
                            }
                            for (int k = 0; k < op_codes[i].packets[j].num_arraydata; ++k)
                            {
                                    if (packetsc == datac)
                                        fprintf(fd, "s->%s_len));\n", op_codes[i].packets[j].arraydata[k].name);
                                    else
                                        fprintf(fd, "s->%s_len, ", op_codes[i].packets[j].arraydata[k].name);
                                    ++packetsc;
                            }
                            fprintf(fd, "    if (!mem) return 1;\n    WRITE_%s_TYPE(mem, %s);\n",
                                typedef_msg_buf, op_codes[i].packets[j].title);
                        }
                        else if (op_codes[i].packets[j].crypt == CRYPT_NONE)
                        {
                            fprintf(fd, "static inline int\n%s_write(byte_buf_t *buf, %s_t *s)\n{\n",
                                     op_codes[i].packets[j].name, op_codes[i].packets[j].name);

                            if (op_codes[i].packets[j].num_chardata == 0)
                            {
                                fprintf(fd, "    uint8 *mem = bbuf_reserve(buf, %s + %s_SZ);\n\
    if (!mem) return 1;\n    WRITE_%s_TYPE(mem, %s);\n", typedef_buf, op_codes[i].packets[j].title,
                                    typedef_msg_buf, op_codes[i].packets[j].title);
                            }
                            else
                            {
                                fprintf(fd, "    uint8 *mem = bbuf_reserve(buf, %s + %s_COMPUTE_SZ(",
                                    typedef_buf, op_codes[i].packets[j].title);
                                packetsc = 0;
                                for (int k = 0; k < op_codes[i].packets[j].num_chardata; ++k)
                                {
                                        if (packetsc == datac)
                                            fprintf(fd, "s->%s_len));\n", op_codes[i].packets[j].chardata[k].name);
                                        else
                                            fprintf(fd, "s->%s_len, ", op_codes[i].packets[j].chardata[k].name);
                                        ++packetsc;
                                }
                                for (int k = 0; k < op_codes[i].packets[j].num_arraydata; ++k)
                                {
                                        if (packetsc == datac)
                                            fprintf(fd, "s->%s_len));\n", op_codes[i].packets[j].arraydata[k].name);
                                        else
                                            fprintf(fd, "s->%s_len, ", op_codes[i].packets[j].arraydata[k].name);
                                        ++packetsc;
                                }
                                fprintf(fd, "    if (!mem) return 1;\n    WRITE_%s_TYPE(mem, %s);\n",
                                    typedef_msg_buf, op_codes[i].packets[j].title);
                            }
                        }

                        if (op_codes[i].packets[j].crypt == CRYPT_VAR ||
                            op_codes[i].packets[j].crypt == CRYPT_CONST)
                            fprintf(fd, "    uint8 *dst = mem;\n    mem += CRYPT_MSG_ADDITIONAL_BYTES;\n\
    uint8 *src = mem;\n\n");

                        for (int k = 0; k < op_codes[i].packets[j].num_varlendata; ++k)
                        {
                            p = op_codes[i].packets[j].varlendata[k].type;
                            while (*p){ if (islower(*p)) *p = toupper(*p); ++p; }
                                fprintf(fd, "    WRITE_%s(mem, s->%s);\n",
                                    op_codes[i].packets[j].varlendata[k].type,
                                    op_codes[i].packets[j].varlendata[k].name);
                        }
                        for (int k = 0; k < op_codes[i].packets[j].num_data; ++k)
                        {
                            p = op_codes[i].packets[j].data[k].type;
                            while (*p){ if (islower(*p)) *p = toupper(*p); ++p; }
                                fprintf(fd, "    WRITE_%s(mem, s->%s);\n",
                                    op_codes[i].packets[j].data[k].type,
                                    op_codes[i].packets[j].data[k].name);
                        }
                        for (int k = 0; k < op_codes[i].packets[j].num_chardata; ++k)
                        {
                                fprintf(fd, "    WRITE_STR(mem, s->%s, s->%s_len);\n",
                                    op_codes[i].packets[j].chardata[k].name,
                                    op_codes[i].packets[j].chardata[k].name);
                        }
                        for (int k = 0; k < op_codes[i].packets[j].num_fixchardata; ++k)
                        {
                                fprintf(fd, "    WRITE_UINT8_FIXARR(mem, s->%s, %s);\n",
                                    op_codes[i].packets[j].fixchardata[k].name,
                                    op_codes[i].packets[j].fixchardata[k].limit);
                        }
                        for (int k = 0; k < op_codes[i].packets[j].num_arraydata; ++k)
                        {
                            p = op_codes[i].packets[j].arraydata[k].type;
                            while (*p){ if (islower(*p)) *p = toupper(*p); ++p; }
                            fprintf(fd, "    WRITE_%s_VARARR(mem, s->%s, s->%s_len);\n",
                                op_codes[i].packets[j].arraydata[k].type,
                                op_codes[i].packets[j].arraydata[k].name,
                                op_codes[i].packets[j].arraydata[k].name);
                        }
                    }
                    if (op_codes[i].packets[j].crypt == CRYPT_VAR)
                        fprintf(fd, "\n    return cryptchan_encrypt(cc, dst, src, sz);\n}\n\n");
                    else if (op_codes[i].packets[j].crypt == CRYPT_CONST)
                        fprintf(fd, "\n    return cryptchan_encrypt(cc, dst, src, %s_SZ);\n}\n\n",
                            op_codes[i].packets[j].title);
                    else
                        fprintf(fd, "\n    return 0;\n}\n\n");

/*READ*/
                    if (op_codes[i].packets[j].type != PACKET_TYPE_WRITEONLY)
                    {
                        if (op_codes[i].packets[j].crypt == CRYPT_CONST)
                        {
                            fprintf(fd, "static inline int\n%s_read_const_encrypted(byte_buf_t *buf, \
cryptchan_t *cc,\n    %s_t *s)\n{\n", op_codes[i].packets[j].name, op_codes[i].packets[j].name);
                            fprintf(fd, "    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \\\n\
        %s_SZ);\n    if (!mem) return 1;\n", op_codes[i].packets[j].title);
                            fprintf(fd, "    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \\\n\
        %s_SZ))\n        return -1;\n", op_codes[i].packets[j].title);
                        }
                        else if (op_codes[i].packets[j].crypt == CRYPT_VAR)
                        {
                            fprintf(fd, "static inline int\n%s_read_var_encrypted(byte_buf_t *buf, \
cryptchan_t *cc,\n    %s_t *s)\n{\n", op_codes[i].packets[j].name, op_codes[i].packets[j].name);
                            fprintf(fd, "\
    msg_sz_t sz;\n\
    int r = prep_msg_read_var_encrypted(buf, %s_MAX_SZ, &sz);\n\
    if (r) return r;\n\n\
    uint8 *mem = bbuf_reserve(buf, sz);\n\n\
    if (cryptchan_decrypt(cc, mem, mem, sz) < 0)\n\
        return -1;\n\n", op_codes[i].packets[j].title);
                        }
                        else if (op_codes[i].packets[j].crypt == CRYPT_NONE)
                        {
                            fprintf(fd, "static inline int\n%s_read(byte_buf_t *buf, %s_t *s)\n{\n",
                                op_codes[i].packets[j].name, op_codes[i].packets[j].name);

                            if (op_codes[i].packets[j].num_chardata == 0 &&
                                op_codes[i].packets[j].num_arraydata == 0)
                            {
                                fprintf(fd, "    uint8 *mem = bbuf_reserve(buf, %s_SZ);\n    if (!mem) return 1;\n",
                                    op_codes[i].packets[j].title);
                            }
                        }

                        if (op_codes[i].packets[j].num_chardata > 0 ||
                            op_codes[i].packets[j].num_arraydata > 0)
                        {
                            if (op_codes[i].packets[j].crypt == CRYPT_NONE)
                            {
                                fprintf(fd, "    int free_space = (int)BBUF_FREE_SPACE(buf);\n\n\
    if (free_space < %s_SZ)\n\
        return 1;\n\n", op_codes[i].packets[j].title);
                                fprintf(fd, "    uint8 *mem = BBUF_CUR_PTR(buf);\n");
                            }
                            for (int k = 0; k < op_codes[i].packets[j].num_varlendata; ++k)
                            {
                                fprintf(fd, "    READ_%s(mem, &s->%s);\n",
                                op_codes[i].packets[j].varlendata[k].type,
                                op_codes[i].packets[j].varlendata[k].name);
                            }
                        }
                        if (op_codes[i].packets[j].num_chardata > 0 ||
                            op_codes[i].packets[j].num_arraydata > 0)
                        {
                            packetsc = 0;
                            fprintf(fd, "\n    if (%s_COMPUTE_SZ(", op_codes[i].packets[j].title);
                            for (int k = 0; k < op_codes[i].packets[j].num_chardata; ++k)
                            {
                                 if (packetsc == datac)
                                     fprintf(fd, "s->%s_len) >\n        %s_MAX_SZ)\n        return -1;\n",
                                     op_codes[i].packets[j].chardata[k].name,
                                     op_codes[i].packets[j].title);
                                 else
                                     fprintf(fd, "s->%s_len, ", op_codes[i].packets[j].chardata[k].name);
                                 ++packetsc;
                            }
                            for (int k = 0; k < op_codes[i].packets[j].num_arraydata; ++k)
                            {
                                 if (packetsc == datac)
                                     fprintf(fd, "s->%s_len) >\n        %s_MAX_SZ)\n        return -1;\n",
                                     op_codes[i].packets[j].arraydata[k].name,
                                     op_codes[i].packets[j].title);
                                 else
                                     fprintf(fd, "s->%s_len, ", op_codes[i].packets[j].arraydata[k].name);
                                 ++packetsc;
                            }
                            if (op_codes[i].packets[j].crypt == CRYPT_VAR)
                            {
                                packetsc = 0;
                                fprintf(fd, "\n    if (sz < %s_COMPUTE_SZ(", op_codes[i].packets[j].title);
                                for (int k = 0; k < op_codes[i].packets[j].num_chardata; ++k)
                                {
                                     if (packetsc == datac)
                                         fprintf(fd, "s->%s_len)) return -1;\n\n",
                                         op_codes[i].packets[j].chardata[k].name,
                                         op_codes[i].packets[j].title);
                                     else
                                         fprintf(fd, "s->%s_len, ", op_codes[i].packets[j].chardata[k].name);
                                     ++packetsc;
                                }
                                for (int k = 0; k < op_codes[i].packets[j].num_arraydata; ++k)
                                {
                                     if (packetsc == datac)
                                         fprintf(fd, "s->%s_len)) return -1;\n\n",
                                         op_codes[i].packets[j].arraydata[k].name,
                                         op_codes[i].packets[j].title);
                                     else
                                         fprintf(fd, "s->%s_len, ", op_codes[i].packets[j].arraydata[k].name);
                                     ++packetsc;
                                }
                            }
                        }

                        if (op_codes[i].packets[j].crypt == CRYPT_NONE &&
                            (op_codes[i].packets[j].num_chardata > 0 ||
                            op_codes[i].packets[j].num_arraydata > 0))
                        {
                            fprintf(fd, "\n    int req_sz = %s_COMPUTE_SZ(", op_codes[i].packets[j].title);
                            packetsc = 0;
                            for (int k = 0; k < op_codes[i].packets[j].num_chardata; ++k)
                            {
                                if (packetsc == datac)
                                    fprintf(fd, "s->%s_len);\n", op_codes[i].packets[j].chardata[k].name);
                                else
                                    fprintf(fd, "s->%s_len, ", op_codes[i].packets[j].chardata[k].name);
                                ++packetsc;
                            }
                            for (int k = 0; k < op_codes[i].packets[j].num_arraydata; ++k)
                            {
                                if (packetsc == datac)
                                    fprintf(fd, "s->%s_len);\n", op_codes[i].packets[j].arraydata[k].name);
                                else
                                    fprintf(fd, "s->%s_len, ", op_codes[i].packets[j].arraydata[k].name);
                                ++packetsc;
                            }

                             fprintf(fd, "    if (free_space < req_sz) return 2;\n\n    bbuf_reserve(buf, req_sz);\n\n");
                        }
                        for (int k = 0; k < op_codes[i].packets[j].num_data; ++k)
                        {
                            fprintf(fd, "    READ_%s(mem, &s->%s);\n",
                            op_codes[i].packets[j].data[k].type,
                            op_codes[i].packets[j].data[k].name);
                        }
                        for (int k = 0; k < op_codes[i].packets[j].num_chardata; ++k)
                        {
                            fprintf(fd, "    READ_VARCHAR(mem, s->%s, s->%s_len);\n",
                                op_codes[i].packets[j].chardata[k].name,
                                op_codes[i].packets[j].chardata[k].name);
                        }
                        for (int k = 0; k < op_codes[i].packets[j].num_fixchardata; ++k)
                        {
                            fprintf(fd, "    READ_UINT8_FIXARR(mem, s->%s, %s);\n",
                            op_codes[i].packets[j].fixchardata[k].name,
                            op_codes[i].packets[j].fixchardata[k].limit);
                        }
                        for (int k = 0; k < op_codes[i].packets[j].num_arraydata; ++k)
                        {
                            fprintf(fd, "    READ_%s_VARARR(mem, s->%s, s->%s_len);\n",
                            op_codes[i].packets[j].arraydata[k].type,
                            op_codes[i].packets[j].arraydata[k].name,
                            op_codes[i].packets[j].arraydata[k].name);
                        }
                        fprintf(fd, "\n    return 0;\n}\n\n");
                    }
                }
            }
        }
        fclose(fs);
        fclose(fd);
    }

    return 0;
}

void add_data(const char *s_datatype, int vartype)
{
    if (op_code_index == -1)
        return;

    int numdata = op_codes[op_code_index].packets[packet_index].num_data;
    int numchars = op_codes[op_code_index].packets[packet_index].num_chardata;
    int numarrays = op_codes[op_code_index].packets[packet_index].num_arraydata;
    int numfixchars = op_codes[op_code_index].packets[packet_index].num_fixchardata;
    int numvarlendata = op_codes[op_code_index].packets[packet_index].num_varlendata;

    switch(vartype)
    {
        case VARTYPE_NORMAL:
        {
            strcpy(op_codes[op_code_index].packets[packet_index].data[numdata].type, s_datatype);
            strcpy(op_codes[op_code_index].packets[packet_index].data[numdata].name, buf1);
            ++op_codes[op_code_index].packets[packet_index].num_data;
            ++numdata;
            op_codes[op_code_index].packets[packet_index].data = (data_t*)realloc(
                op_codes[op_code_index].packets[packet_index].data,
                sizeof(data_t) * (DATA_CHUNK * numdata));
        }break;
        case VARTYPE_CHAR:
        {
            strcpy(op_codes[op_code_index].packets[packet_index].chardata[numchars].type, s_datatype);
            strcpy(op_codes[op_code_index].packets[packet_index].chardata[numchars].name, buf1);
            strcpy(op_codes[op_code_index].packets[packet_index].chardata[numchars].limit, buf2);
            ++op_codes[op_code_index].packets[packet_index].num_chardata;
            --op_codes[op_code_index].packets[packet_index].num_total_data;
            ++numchars;
            op_codes[op_code_index].packets[packet_index].chardata = (data_t*)realloc(
                op_codes[op_code_index].packets[packet_index].chardata,
                sizeof(data_t) * (DATA_CHUNK * numchars));
        }break;
        case VARTYPE_ARRAY:
        {
            strcpy(op_codes[op_code_index].packets[packet_index].arraydata[numarrays].type, s_datatype);
            strcpy(op_codes[op_code_index].packets[packet_index].arraydata[numarrays].name, buf1);
            strcpy(op_codes[op_code_index].packets[packet_index].arraydata[numarrays].limit, buf2);
            ++op_codes[op_code_index].packets[packet_index].num_arraydata;
            ++numarrays;
            op_codes[op_code_index].packets[packet_index].arraydata = (data_t*)realloc(
                op_codes[op_code_index].packets[packet_index].arraydata,
                sizeof(data_t) * (DATA_CHUNK * numarrays));
        }break;
        case VARTYPE_FIXCHAR:
        {
            strcpy(op_codes[op_code_index].packets[packet_index].fixchardata[numfixchars].type, s_datatype);
            strcpy(op_codes[op_code_index].packets[packet_index].fixchardata[numfixchars].name, buf1);
            strcpy(op_codes[op_code_index].packets[packet_index].fixchardata[numfixchars].limit, buf2);
            ++op_codes[op_code_index].packets[packet_index].num_fixchardata;
            ++numfixchars;
            op_codes[op_code_index].packets[packet_index].fixchardata = (data_t*)realloc(
                op_codes[op_code_index].packets[packet_index].fixchardata,
                sizeof(data_t) * (DATA_CHUNK * numfixchars));
        }break;
        case VARTYPE_VARLEN:
        {
            strcpy(op_codes[op_code_index].packets[packet_index].varlendata[numvarlendata].type, s_datatype);
            strcpy(op_codes[op_code_index].packets[packet_index].varlendata[numvarlendata].name, buf1);
            ++op_codes[op_code_index].packets[packet_index].num_varlendata;
            ++numvarlendata;
            op_codes[op_code_index].packets[packet_index].varlendata = (data_t*)realloc(
                op_codes[op_code_index].packets[packet_index].varlendata,
                sizeof(data_t) * (DATA_CHUNK * numvarlendata));
        }break;
    }

    ++op_codes[op_code_index].packets[packet_index].num_total_data;
}
