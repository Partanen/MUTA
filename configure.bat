@echo off

set ERRORLEVEL=0

::Print help
if "%~1"=="-h" goto print_help
if "%~1"=="--help" goto print_help

::Check architecture
set arch=%1
if "%arch%"=="x86" goto do_configure
if "%arch%"=="x64" goto do_configure
if not "%arch%"=="" goto bad_arch
set arch=x64

:do_configure
    echo Configuring MUTA for architecture %arch%...

    echo Configuring client...
    cd client && call configure.bat %arch%
    if ERRORLEVEL 1 goto out
    cd ..

    echo Configuring master...
    cd master && call configure.bat %arch%
    if ERRORLEVEL 1 goto out
    cd ..

    echo Configuring sim...
    cd sim && call configure.bat %arch%
    if ERRORLEVEL 1 goto out
    cd ..

    echo Configuring proxy...
    cd proxy && call configure.bat %arch%
    if ERRORLEVEL 1 goto out
    cd ..

    echo Configuring login...
    cd login && call configure.bat %arch%
    if ERRORLEVEL 1 goto out
    cd ..

    goto success

:bad_arch
    echo %0: error, architecture must be x86 or x64, was %arch%
    set ERRORLEVEL=1
    goto out

:print_help
    echo Usage: %0 [architecture]
    echo Valid architectures: x86, x64
    echo Example: %0 x64
    goto out

:success
    echo %0: configured for architecture %arch%

:out

