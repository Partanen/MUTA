# Entities
Entities are different types of game objects in the world of MUTA. MUTA makes a
distinction between three different entity types which are presented here. Note
that tiles are not considered part of the entity concept.

# Players
Any players characters in the world are player entities.

# Static objects
Static objects are scenery objects that cannot be interacted with. They are
permanent parts of the map and thus both the client and server always know their
locations. They are similar to tiles, but may be targeted by players.

# Dynamic objects
Dynamic objects are objects that can be interacted with, such as trees that can
be cut down. These are spawned and controlled by the server.

# Creatures
Creatures are mobile entities such as monsters and NPCs.
