# Protocols
The individual programs that constitute MUTA communicate each other using binary
protocols, each protocol specifically crafted for communication between the two
specific ends. For example, the protocol between the simulation server and the
master server is different to the protocol between the login server and the
master server.

# Writing protocols using Packetwriter 2
Protocols are defined in .def files. A custom tool called Packetwriter 2 is used
to generate C code from these files. Packetwriter 2 sources may be found under
the tools/packetwriter2 directory. After building it, it can be used to generate
C code as such:

´´´
./packetwriter2 -i my_protocol.def -o my_protocol.h
´´´

The above code would produce the file my_protocol.h that would include the
generate C code, assuming my_protocl.def is a valid protocol definition file.
For the .def file format specification and examples, check the README.md file
under tools/packetwriter2.
