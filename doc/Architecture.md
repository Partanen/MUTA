# MUTA Architecture

## Programs
MUTA's server side consists of 4 programs:
* login server
* master server
* proxy server
* sim server
The server side programs can run either on the same or completely different
machines. Multiple proxy and simlation servers on different machines can be
used to distribute server load.

Player connect to the server using the client program.

There are also a bunch of tool programs included in the repo, found in the tools
directory.

### Client
The MUTA client is used by players to connect to the game. It displays an
isometric visualization of the world surrounding the player.

The client also contains various development tools, including the map editor.
These can be accessed by pressing F1 in the login screen. 

### Master server
The master server is the central, authoritative piece of a single game world. It
knows all of the entities (players, creatures, objects, etc.) in the world, but
does no simulation (pathfinding, running scripts, etc.) by itself. Instead, the
master server distributes the simulation work to any simulation servers
connected to it. If two simulation servers ever disagree about the state of the
world, the master server is authoritative.

The master server keeps a network connection to the following:
- Proxy servers (one or more)
- Simulation servers (one or more, each simulating a given parts of different
  instances)
- The login server, which tells the master which players are allowed to log in.

### Simulation server
Handles heavy computation duties for a world, including pathfinding and
scripting. The simulation duties of a single world can be divided to multiple
simulation servers by assigning rectangular areas of the world map to different
individual servers.

Keeps a network connection to it's master server, and the master's world state
is always authoritative.

### Login server
When players want to log in to play, they first connect to the login server. If
the login is successful, the login server sends an authorization token to the
player and to the master server the player wants to connect to. Then the client
attempts to send the token to the master server and hop in to the game world.

The login server connects to:
- A MariaDB connection to the account database
- Master servers (one or more)
- Clients

### Proxy server
Players logged into a world do not directly connect to the master server.
Instead, they connect through proxy servers. Each proxy server can be connected
to multiple master servers, and each master server can have multiple proxies
connected to it. The main purpose of this is to hide the IP address of the
master server, and to spread resources over multiple machines in case of a
DDOS attack.

The proxy servers only forwards messages between players and the master server.
It also does some simple message validation and can disconnect players whose
clients send malformed packets. 

The proxy server connects to:
- Master servers (one or more)
- Clients
