-- The account database may be used by multiple shards.

CREATE USER muta_accounts@localhost IDENTIFIED BY 'muta_accounts';

CREATE DATABASE muta_accounts DEFAULT CHARACTER SET utf8 COLLATE
    utf8_general_ci;

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, ALTER, LOCK TABLES,
    CREATE TEMPORARY TABLES ON muta_accounts.* TO muta_accounts@localhost;

USE muta_accounts;

CREATE TABLE accounts
(
    id          BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    name        VARCHAR(32)     NOT NULL,
    password    CHAR(128)       NOT NULL,
    email       TEXT,
    last_login  TIMESTAMP       NOT NULL DEFAULT '0000-00-00 00:00:00',

    CONSTRAINT unique_name UNIQUE (name),
    CONSTRAINT unique_id   UNIQUE(id),
    PRIMARY KEY(id)
);

CREATE TABLE characters
(
    id          BIGINT UNSIGNED AUTO_INCREMENT,
    account_id  BIGINT UNSIGNED,
    shard       INT UNSIGNED,

    PRIMARY KEY(id)
);
