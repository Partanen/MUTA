-- The shard database stores the state of a single shard. This includes player
-- characters, creatures, etc.

CREATE DATABASE muta_shard DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER muta_shard@localhost IDENTIFIED BY 'muta_shard';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, ALTER, LOCK TABLES, EXECUTE,
    CREATE TEMPORARY TABLES ON muta_shard.* TO muta_shard@localhost;

USE muta_shard;

CREATE TABLE player_characters
(
    id          BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    account_id  BIGINT UNSIGNED NOT NULL,
    name        VARCHAR(16) NOT NULL,
    race        TINYINT UNSIGNED NOT NULL,
    sex         TINYINT UNSIGNED NOT NULL,
    direction   TINYINT UNSIGNED NOT NULL,
    instance_id INT UNSIGNED NOT NULL DEFAULT 0,
    x           INT NOT NULL,
    y           INT NOT NULL,
    z           TINYINT UNSIGNED NOT NULL,
    CONSTRAINT unique_id UNIQUE(id),
    CONSTRAINT unique_name UNIQUE(name)
);

CREATE TABLE dynamic_objects
(
    id              BINARY(16) NOT NULL PRIMARY KEY,
    type            INT UNSIGNED NOT NULL,
    in_type         ENUM('player_container', 'player_equipped') NOT NULL,
    equipment_slot  TINYINT UNSIGNED NOT NULL,
    x               INT NOT NULL,
    y               INT NOT NULL,
    player_id       BIGINT UNSIGNED NOT NULL,
    CONSTRAINT unique_id UNIQUE(id)
);

CREATE TABLE player_bags
(
    player_id       BIGINT UNSIGNED NOT NULL PRIMARY KEY,
    dobj_type_id    INT UNSIGNED NOT NULL,
    inventory_index TINYINT UNSIGNED NOT NULL
);

CREATE FUNCTION UuidToBin(_uuid BINARY(36))
    RETURNS BINARY(16)
    LANGUAGE SQL DETERMINISTIC CONTAINS SQL SQL SECURITY INVOKER
RETURN
    UNHEX(CONCAT(
        SUBSTR(_uuid, 15, 4),
        SUBSTR(_uuid, 10, 4),
        SUBSTR(_uuid,  1, 8),
        SUBSTR(_uuid, 20, 4),
        SUBSTR(_uuid, 25) ));

CREATE FUNCTION UuidFromBin(_bin BINARY(16))
    RETURNS BINARY(36)
    LANGUAGE SQL DETERMINISTIC CONTAINS SQL SQL SECURITY INVOKER
RETURN
    LCASE(CONCAT_WS('-',
        HEX(SUBSTR(_bin,  5, 4)),
        HEX(SUBSTR(_bin,  3, 2)),
        HEX(SUBSTR(_bin,  1, 2)),
        HEX(SUBSTR(_bin,  9, 2)),
        HEX(SUBSTR(_bin, 11))));
