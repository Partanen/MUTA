#ifndef MUTA_LOGIN_SHARDS_H
#define MUTA_LOGIN_SHARDS_H
#include "../../shared/types.h"
#include "../../shared/net.h"

/* Forward declaration(s) */
typedef struct accept_shard_event_t accept_shard_event_t;
typedef struct read_shard_event_t   read_shard_event_t;

int
shards_init(void);

void
shards_destroy(void);

int
shards_start(void);

uint32
shards_get_index(const char *name);

const char *
shards_get_name(uint32 shard_index);

addr_t
shards_get_address(uint32 index);

bool32
shards_exists(const char *name);

/* Shards are accessed by index. The indices are in the same order as they are
 * com_config.shards */

bool32
shards_is_online(uint32 index);

int
shards_select_for_player(const char *account_name, uint64 account_id,
    uint32 login_session_id, uint8 *token, uint32 ip, uint32 shard_index);
/* Asynchronous call, return value dispatched as an event. */

void
shards_cancel_select_for_player(uint32 shard_index, uint64 account_id,
    uint32 login_session_id);

void
shards_flush();

#endif /* MUTA_LOGIN_SHARDS_H */
