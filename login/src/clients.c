#include <time.h>
#include "clients.h"
#include "common.h"
#include "event.h"
#include "shards.h"
#include "db.h"
#include "../../shared/netpoll.h"
#include "../../shared/common_utils.h"
#include "../../shared/common_defs.h"
#include "../../shared/crypt.h"
#include "../../shared/proxy_packets.h"
#include "../../shared/client_packets.h"
#include "../../shared/account_rules.h"
#include "../../shared/containers.h"
#include "../../shared/sv_time.h"
#include "../../shared/hashtable.h"

#define CLIENT_IN_BUF_SIZE MUTA_MTU
#define IS_CLIENT(ptr_) \
    ((client_t*)(ptr_) >= _clients.pool.all && \
    (client_t*)(ptr_) < _clients.pool.all + _clients.pool.max)
#define CLIENT_BY_INDEX(id_) \
    (_clients.pool.all[(id_)].flags.in_use ? &_clients.pool.all[(id_)] : 0)
#define CLIENT_TIMEOUT                  5000
#define CLIENT_TIMEOUT_CHECK_FREQUENCY  250

typedef struct client_t         client_t;
typedef struct client_timer_t   client_timer_t;

struct client_t
{
    socket_t    socket;
    uint32      id;
    uint32      name_hash;
    uint32      shard;
    uint32      timeout_index; /* Only relevant if confirmed_by_shard */
    struct
    {
        uint8   *mem;
        uint32  num_bytes;
    } in_buf;
    struct
    {
        uint in_use:1;
        uint account_queried:1; /* Is an asynchronous query on-going? */
        uint authed:1;
        uint selected_shard:1;
        uint started_crypt:1;
        uint confirmed_by_shard:1;
    } flags;
    cryptchan_t cryptchan;
    uint64      account_id;
    char        account_name[MAX_ACC_NAME_LEN + 1];
    uint8       token[AUTH_TOKEN_SZ];
};

struct client_timer_t
{
    uint32 client;
    uint64 start;
};

static netpoll_t    _netpoll;
static socket_t     _listen_socket;
static thread_t     _thread;
static bool32       _running;
static segpool_t    _segpool;
static mutex_t      _segpool_mutex;
static uint64       _last_client_timeout_check;

static struct
{
    uint32                          running_id;
    fixed_pool(client_t)            pool;
    uint8                           *in_mem;    /* Memory used by clients' in-buffers */
    hashtable(uint32, uint32)       id_table;
    hashtable(uint64, client_t*)    account_table;
    /* After a shard accepts a client, it will timeout after a while. */
    client_timer_t                  *timeouts;
    int                             num_timeouts;
} _clients;

static thread_ret_t
_main(void *args);

static void
_check_netpoll_events(netpoll_event_t *events, int num_events);

static int
_read_packet(client_t *cl);
/* Return values:
 * < 0 if client should be disconnected,
 * > 0 if msg is incomplete
 * 0 if message was read successfully */

static int
_new_client(socket_t socket, addr_t *address);

static void
_disconnect_client(client_t *cl, bool32 del_from_netpoll);

static int
_handle_clmsg_pub_key(client_t *cl, clmsg_pub_key_t *s);

static int
_handle_clmsg_stream_header(client_t *cl, clmsg_stream_header_t *s);

static int
_handle_clmsg_login_request(client_t *cl, clmsg_login_request_t *s);

static int
_handle_clmsg_select_shard(client_t *cl, clmsg_select_shard_t *s);

static int
_handle_clmsg_cancel_select_shard(client_t *cl);

int
cl_init(void)
{
    int     ret         = 0;
    uint32  max_clients = com_config.max_clients;
    fixed_pool_init(&_clients.pool, max_clients);
    _clients.in_mem = emalloc(max_clients * CLIENT_IN_BUF_SIZE);
    for (uint32 i = 0; i < max_clients; ++i)
        _clients.pool.all[i].in_buf.mem = _clients.in_mem + i * CLIENT_IN_BUF_SIZE;
    _clients.timeouts = emalloc(max_clients * sizeof(client_timer_t));
    if (thread_init(&_thread))
        {ret = 1; goto fail;}
    if (netpoll_init(&_netpoll))
        {ret = 2; goto fail;}
    mutex_init(&_segpool_mutex);
    segpool_init(&_segpool);
    int err;
    hashtable_init(_clients.id_table, max_clients, &err);
    if (err)
        muta_panic();
    hashtable_init(_clients.account_table, max_clients, &err);
    if (err)
        muta_panic();
    return ret;
    fail:
        LOGF("Failed with code %d.", ret);
        cl_destroy();
        return 0;
}

void
cl_destroy(void)
{
    fixed_pool_destroy(&_clients.pool);
    free(_clients.in_mem);
    thread_destroy(&_thread);
}

int
cl_start(void)
{
    int ret         = 0;
    _running        = 1;
    _listen_socket  = net_tcp_ipv4_listen_sock(DEFAULT_LOGIN_PORT,
        com_config.listen_backlog);
    _last_client_timeout_check = com_current_tick;
    if (_listen_socket == KSYS_INVALID_SOCKET)
    {
        LOGF("Failed to create listen socket!");
        ret = 1;
        goto fail;
    }
    netpoll_event_t e;
    e.events    = NETPOLL_READ;
    e.data.ptr  = &_listen_socket;
    if (netpoll_add(&_netpoll, _listen_socket, &e))
    {
        LOGF("Failed to add listen socket to netpoll!");
        ret = 2;
        goto fail;
    }
    if (thread_create(&_thread, _main, 0))
    {
        LOGF("Failed to create thread!");
        ret = 3;
        goto fail;
    }
    return ret;
    fail:
        LOGF("Failed with code %d.", ret);
        return ret;
}

void
cl_stop(void)
{
    _running = 0;
    thread_join(&_thread);
    close_socket(_listen_socket);
}

void
cl_accept(accept_client_event_t *event)
{
    int result = _new_client(event->socket, &event->address);
    if (!result)
    {
        LOG("Accepted new client.");
        return;
    }
    LOGF("_new_client() failed with error %d.", result);
    close_socket(event->socket);
}

void
cl_read(read_client_event_t *event)
{
    client_t *cl = CLIENT_BY_INDEX(event->client_index);
    if (!cl)
    {
        DEBUG_LOG("Attempted to read client no longer in use!");
        goto out;
    }
    void    *buf        = cl->in_buf.mem + cl->in_buf.num_bytes;
    int     num_bytes   = event->num_bytes;
    if (num_bytes <= 0)
    {
       LOG("Client closed connection or an error was occured while receiving.");
       _disconnect_client(cl, 0);
       return;
    }
    int num_moved = 0;
    while (num_moved < num_bytes)
    {
        int max_bytes   = CLIENT_IN_BUF_SIZE - cl->in_buf.num_bytes;
        int num_to_move = MIN((num_bytes - num_moved), max_bytes);
        int num_left;
        memcpy(buf, (char*)event->memory + num_moved, num_to_move);
        cl->in_buf.num_bytes += num_to_move;
        DEBUG_PRINTFF("NUMBYTES: %d\n", cl->in_buf.num_bytes);
        if ((num_left = _read_packet(cl)) < 0)
        {
            _disconnect_client(cl, 1);
            goto out;
        }
        cl->in_buf.num_bytes = num_left;
        num_moved += num_to_move;
    }
    out:
        if (event->num_bytes <= 0)
            return;
        mutex_lock(&_segpool_mutex);
        segpool_free(&_segpool, event->memory);
        mutex_unlock(&_segpool_mutex);
}

void
cl_finish_login_attempt(account_login_query_finished_event_t *event)
{
    LOG("Finished an account login query for a client.");
    size_t login_session_id_hash = hashtable_hash(&event->login_session_id,
        sizeof(event->login_session_id));
    uint32 *cl_index = hashtable_find(_clients.id_table,
        event->login_session_id, login_session_id_hash);
    if (!cl_index)
    {
        DEBUG_LOG("Client disconnected before account login query finished.");
        return;
    }
    client_t *cl = &_clients.pool.all[*cl_index];
    if (cl->name_hash != event->name_hash)
    {
        LOG("Received finish login attempt, but the account id hash was "
            "different!");
        return;
    }
    svmsg_login_result_t result;
    result.result = 0;
    union
    {
        uint8 login_result[sizeof(svmsg_t) + CRYPT_MSG_ADDITIONAL_BYTES +
            SVMSG_LOGIN_RESULT_SZ];
        uint8 shard_list_item[sizeof(svmsg_t) + sizeof(pw2_packet_size_t) +
            CRYPT_MSG_ADDITIONAL_BYTES + SVMSG_SHARD_LIST_ITEM_MAX_SZ];
    } buf;
    bbuf_t bb = BBUF_INITIALIZER(buf.login_result, sizeof(buf.login_result));
    svmsg_login_result_write(&bb, &cl->cryptchan, &result);
    if (net_send_all(cl->socket, bb.mem, bb.num_bytes) <= 0)
    {
        LOG("Failed to send login result to client.");
        _disconnect_client(cl, 1);
        return;
    }
    DEBUG_PRINTFF("LOGIN MSG WAS %d BYTES\n", bb.num_bytes);
    if (event->result)
    {
        LOGF("Login failed, error %d.", event->result);
        _disconnect_client(cl, 1);
        return;
    }
    DEBUG_LOG("Client successfully logged in");
    /*-- Check if the account is already logged in */
    size_t account_id_hash = hashtable_hash(&event->account_id,
        sizeof(event->account_id));
    client_t **old_client = hashtable_find(_clients.account_table,
        event->account_id, account_id_hash);
    if (old_client)
        _disconnect_client(*old_client, 1);
    cl->account_id      = event->account_id;
    cl->flags.authed    = 1;
    int err;
    hashtable_insert(_clients.account_table, cl->account_id, account_id_hash,
        cl, &err);
    if (err)
        muta_panic();
    svmsg_shard_list_item_t s;
    for (uint32 i = 0; i < com_config.shards.num; ++i)
    {
        BBUF_INIT(&bb, buf.shard_list_item, sizeof(buf.shard_list_item));
        shard_info_t *si = &com_config.shards.items[i];
        s.online    = (uint8)shards_is_online(shards_get_index(si->name));
        s.name.len  = (uint8)strlen(si->name);
        memcpy(s.name.data, si->name, s.name.len);
        svmsg_shard_list_item_write(&bb, &s);
        if (net_send_all(cl->socket, bb.mem, bb.num_bytes) <= 0)
        {
            LOGF("net_send_all() failed.");
            _disconnect_client(cl, 1);
            return;
        }
        DEBUG_LOGF("Sent shard info for %s to client.", si->name);
    }
}

int
cl_finish_select_shard_attempt(uint64 account_id, uint32 login_session_id,
    int result)
{
    size_t login_session_id_hash = hashtable_hash(&login_session_id,
        sizeof(login_session_id));
    uint32 *cl_index = hashtable_find(_clients.id_table, login_session_id,
        login_session_id_hash);
    if (!cl_index)
    {
        LOG("Client of login session id %u disconnected before shard selection "
            "finished.", login_session_id);
        return 1;
    }
    client_t *cl = &_clients.pool.all[*cl_index];
    if (cl->account_id != account_id)
    {
        LOGF("Account ids do not match!");
        return 2;
    }
    union
    {
        uint8 fail[sizeof(svmsg_t) + SVMSG_SHARD_SELECT_FAIL_SZ];
        uint8 success[sizeof(svmsg_t) + sizeof(msg_sz_t) +
            CRYPT_MSG_ADDITIONAL_BYTES + SVMSG_SHARD_SELECT_SUCCESS_MAX_SZ];
    } buf;
    bbuf_t bb;
    if (result)
    {
        svmsg_shard_select_fail_t f_msg;
        f_msg.reason = (uint8)result;
        BBUF_INIT(&bb, buf.fail, sizeof(buf.fail));
        svmsg_shard_select_fail_write(&bb, &f_msg);
        cl->flags.selected_shard = 0;
        LOG("Player failed to select a shard (error %d).", result);
    } else
    {
        BBUF_INIT(&bb, buf.success, sizeof(buf.success));
        svmsg_shard_select_success_t s_msg;
        const char  *shard_name = shards_get_name(cl->shard);
        addr_t      addr        = shards_get_address(cl->shard);
        s_msg.shard_name.len    = (uint8)strlen(shard_name);
        memcpy(s_msg.shard_name.data, shard_name, s_msg.shard_name.len);
        s_msg.ip                = addr.ip;
        s_msg.port              = addr.port;
        memcpy(s_msg.token, cl->token, AUTH_TOKEN_SZ);
        int r = svmsg_shard_select_success_write(&bb, &cl->cryptchan, &s_msg);
        muta_assert(!r);
        LOG("Player successfully selected a shard.");
        cl->flags.confirmed_by_shard = 1;
        cl->timeout_index = _clients.num_timeouts++;
        muta_assert((uint32)_clients.num_timeouts <= _clients.pool.max);
        client_timer_t client_timer;
        client_timer.client = *cl_index;
        client_timer.start  = get_program_ticks_ms();
        _clients.timeouts[cl->timeout_index] = client_timer;
    }
    if (net_send_all(cl->socket, bb.mem, bb.num_bytes) <= 0)
    {
        _disconnect_client(cl, 1);
        return 3;
    }
    return 0;
}

void
cl_check_timeouts(void)
{
    uint64 time_now     = com_current_tick;
    uint64 time_diff    = com_current_tick - _last_client_timeout_check;
    if (time_diff < CLIENT_TIMEOUT_CHECK_FREQUENCY &&
        com_current_tick > _last_client_timeout_check)
        return;
    _last_client_timeout_check = time_now;
    int num_timers = _clients.num_timeouts;
    for (int i = 0; i < num_timers; ++i)
    {
        if (time_now < _clients.timeouts[i].start ||
            time_now - _clients.timeouts[i].start >= CLIENT_TIMEOUT)
        {
            _disconnect_client(&_clients.pool.all[_clients.timeouts[i].client],
                1);
            num_timers--;
            i--;
        } else
            break;
    }
    _clients.num_timeouts = num_timers;
}

void
cl_on_shard_status_changed(uint32 shard, bool32 online)
{
    uint32      max_clients = com_config.max_clients;
    client_t    *cl;
    uint8       buf[sizeof(svmsg_t) + SVMSG_SHARD_LIST_ITEM_MAX_SZ];
    bbuf_t      bb = BBUF_INITIALIZER(buf, sizeof(buf));
    svmsg_shard_list_item_t s_msg;
    s_msg.online    = (uint8)online;
    const char *shard_name = shards_get_name(shard);
    s_msg.name.len  = (uint8)strlen(shard_name);
    memcpy(s_msg.name.data, shard_name, s_msg.name.len);
    svmsg_shard_list_item_write(&bb, &s_msg);
    for (uint32 i = 0; i < max_clients; ++i)
    {
        cl = &_clients.pool.all[i];
        if (!cl->flags.in_use)
            continue;
        if (!cl->flags.authed)
            continue;
        if (cl->flags.selected_shard && cl->shard == shard)
            cl->flags.selected_shard = 0;
        if (net_send_all(cl->socket, bb.mem, bb.num_bytes) <= 0)
            _disconnect_client(cl, 1);
    }
    LOG("Notified clients of server %s status change to %s.",
        shards_get_name(shard), online ? "online" : "offline");
}

static thread_ret_t
_main(void *args)
{
    netpoll_event_t events[64];
    while (_running)
    {
        int num_events = netpoll_wait(&_netpoll, events, 64, 5000);
        _check_netpoll_events(events, num_events);
    }
    return 0;
}

static void
_check_netpoll_events(netpoll_event_t *events, int num_events)
{
    uint8   buf[CLIENT_IN_BUF_SIZE];
    netpoll_event_t *e;
    for (int i = 0; i < num_events; ++i)
    {
        if (!(events[i].events & (NETPOLL_READ | NETPOLL_HUP)))
            continue;
        e = &events[i];
        event_t ne;
        if (IS_CLIENT(e->data.ptr))
        {
            client_t *cl = e->data.ptr;
            ne.type = EVENT_READ_CLIENT;
            int num_bytes = net_recv(cl->socket, buf, sizeof(buf));
            ne.read_client.client_index = fixed_pool_index(&_clients.pool, cl);
            ne.read_client.num_bytes    = num_bytes;
            if (num_bytes > 0)
            {
                mutex_lock(&_segpool_mutex);
                ne.read_client.memory = segpool_malloc(&_segpool, num_bytes);
                memcpy(ne.read_client.memory, buf, num_bytes);
                mutex_unlock(&_segpool_mutex);
            } else
                netpoll_del(&_netpoll, cl->socket);
            event_push(&com_event_buf, &ne, 1);
        } else
        if ((socket_t*)e->data.ptr == &_listen_socket)
        {
            /*-- It's a new connection. Take it in. --*/
            socket_t s = net_accept(_listen_socket, &ne.accept_client.address);
            if (s == KSYS_INVALID_SOCKET)
            {
                LOGF("net_accept() returned invalid socket!");
                continue;
            }
            if (!_clients.pool.num_free)
            {
                LOGF("Accepted connection, but no client slots free! Closing.");
                close_socket(s);
                continue;
            }
            ne.type                 = EVENT_ACCEPT_CLIENT;
            ne.accept_client.socket = s;
            event_push(&com_event_buf, &ne, 1);
        }
    }
}

static int
_read_packet(client_t *cl)
{
    bbuf_t bb = BBUF_INITIALIZER(cl->in_buf.mem, cl->in_buf.num_bytes);
    bool32      incomplete  = 0;
    int         dc          = 0;
    clmsg_t     msg_type;
    while (BBUF_FREE_SPACE(&bb) >= sizeof(clmsg_t))
    {
        BBUF_READ(&bb, &msg_type);
        DEBUG_PRINTFF("TYPE: %u\n", msg_type);
        switch (msg_type)
        {
        case CLMSG_PUB_KEY:
        {
            clmsg_pub_key_t s;
            incomplete = clmsg_pub_key_read(&bb, &s);
            if (!incomplete)
                dc = _handle_clmsg_pub_key(cl, &s);
        }
            break;
        case CLMSG_STREAM_HEADER:
        {
            clmsg_stream_header_t s;
            incomplete = clmsg_stream_header_read(&bb, &s);
            if (!incomplete)
                dc = _handle_clmsg_stream_header(cl, &s);
        }
            break;
        case CLMSG_LOGIN_REQUEST:
        {
            DEBUG_PUTS("CLMSG_LOGIN_REQUEST");
            clmsg_login_request_t s;
            incomplete = clmsg_login_request_read(&bb, &cl->cryptchan, &s);
            if (!incomplete)
                dc = _handle_clmsg_login_request(cl, &s);
        }
            break;
        case CLMSG_SELECT_SHARD:
        {
            DEBUG_PUTS("CLMSG_SELECT_SHARD");
            clmsg_select_shard_t s;
            incomplete = clmsg_select_shard_read(&bb, &s);
            if (!incomplete)
                dc = _handle_clmsg_select_shard(cl, &s);
        }
            break;
        case CLMSG_CANCEL_SELECT_SHARD:
            DEBUG_PRINTFF("CLMSG_CANCEL_SELECT_SHARD, num_bytes: %d\n",
                bb.num_bytes);
            dc = _handle_clmsg_cancel_select_shard(cl);
            break;
        case CLMSG_KEEP_ALIVE:
            break;
        default:
            dc = 1;
        }
        if (dc || incomplete < 0)
        {
            LOGF("msg_type: %d, dc: %d, incomplete: %d.", (int)msg_type,
                dc, incomplete);
            return -1;
        }
        if (incomplete)
        {
            bb.num_bytes -= sizeof(clmsg_t);
            break;
        }
    }
    DEBUG_PRINTFF("FREE SPACE: %d\n", BBUF_FREE_SPACE(&bb));
    return BBUF_FREE_SPACE(&bb);
}

static int
_new_client(socket_t socket, addr_t *address)
{
    client_t *cl = fixed_pool_new(&_clients.pool);
    if (!cl)
        return 1;
    muta_assert(!cl->flags.in_use);
    netpoll_event_t event;
    event.events    = NETPOLL_READ;
    event.data.ptr  = cl;
    memset(&cl->cryptchan, 0, sizeof(cryptchan_t));
    memset(&cl->flags, 0, sizeof(cl->flags));
    cl->in_buf.num_bytes    = 0;
    cl->socket              = socket;
    cl->id                  = _clients.running_id++;
    cl->account_name[0]     = 0;
    if (net_disable_nagle(socket))
        return 3;
    if (netpoll_add(&_netpoll, socket, &event))
        return 4;
    int err;
    size_t id_hash = hashtable_hash(&cl->id, sizeof(cl->id));
    uint32_t index = fixed_pool_index(&_clients.pool, cl);
    hashtable_insert(_clients.id_table, cl->id, id_hash, index, &err);
    if (err)
        muta_panic();
    cl->flags.in_use = 1;
    return 0;
}

static void
_disconnect_client(client_t *cl, bool32 del_from_netpoll)
{
    muta_assert(cl->flags.in_use);
    LOG("Disconnecting client.");
    if (cl->flags.confirmed_by_shard)
    {
        memmove(&_clients.timeouts[cl->timeout_index],
            &_clients.timeouts[cl->timeout_index] + 1,
            (_clients.num_timeouts - (cl->timeout_index + 1)) *
            sizeof(client_timer_t));
        _clients.num_timeouts--;
    }
    cl->flags.in_use = 0;
    if (del_from_netpoll)
        netpoll_del(&_netpoll, cl->socket);
    net_shutdown_sock(cl->socket, SOCKSD_BOTH);
    muta_assert(_clients.pool.num_free < _clients.pool.max);
    if (cl->flags.authed)
    {
        size_t account_id_hash = hashtable_hash(&cl->account_id,
            sizeof(cl->account_id));
        hashtable_erase(_clients.account_table, cl->account_id,
            account_id_hash);
    }
    size_t id_hash = hashtable_hash(&cl->id, sizeof(cl->id));
    hashtable_erase(_clients.id_table, cl->id, id_hash);
    fixed_pool_free(&_clients.pool, cl);
}

static int
_handle_clmsg_pub_key(client_t *cl, clmsg_pub_key_t *s)
{
    if (cl->flags.started_crypt)
        return 1;
    if (cryptchan_is_encrypted(&cl->cryptchan))
        return 2;
    if (cryptchan_init(&cl->cryptchan, 0))
        return 3;
    svmsg_pub_key_t         p_msg;
    svmsg_stream_header_t   h_msg;
    int r;
    if ((r = cryptchan_sv_store_pub_key(&cl->cryptchan, s->key, h_msg.header)))
    {
        LOGF("cryptchan_sv_store_pub_key() failed, error %d.", r);
        return 4;
    }
    memcpy(p_msg.key, cl->cryptchan.pk, CRYPTCHAN_PUB_KEY_SZ);
    uint8   buf[2 * sizeof(svmsg_t) + SVMSG_STREAM_HEADER_SZ + SVMSG_PUB_KEY_SZ];
    bbuf_t  bb = BBUF_INITIALIZER(buf, sizeof(buf));
    svmsg_pub_key_write(&bb, &p_msg);
    svmsg_stream_header_write(&bb, &h_msg);
    if (net_send_all(cl->socket, bb.mem, bb.num_bytes) <= 0)
    {
        LOGF("net_send_all() returned <= 0.");
        return 4;
    }
    DEBUG_LOGF("Received client's public key.");
    cl->flags.started_crypt = 1;
    return 0;
}

static int
_handle_clmsg_stream_header(client_t *cl, clmsg_stream_header_t *s)
{
    return cryptchan_store_stream_header(&cl->cryptchan, s->header);
}

static int
_handle_clmsg_login_request(client_t *cl, clmsg_login_request_t *s)
{
    LOG("Login request incoming.");
    if (cl->flags.account_queried)
    {
        LOGF("Client attempted to log in twice.");
        return 1;
    }
    LOGF("New login request!");
    if (ar_check_player_character_name(s->account_name.data,
        s->account_name.len))
    {
        LOGF("Invalid account name!");
        return 2;
    }
    memcpy(cl->account_name, s->account_name.data, s->account_name.len);
    cl->account_name[s->account_name.len] = 0;
    if (s->password.len < MIN_PW_LEN)
        return 3;
    cl->name_hash = fnv_hash32_from_data(s->account_name.data,
        s->account_name.len);
    muta_assert(s->password.len <= MAX_PW_LEN);
    /* Dispatch async db query */
    DEBUG_LOG("Querying account login for login session id %u.", cl->id);
    if (db_query_account_login(cl->id, s->account_name.data,
        s->account_name.len, cl->name_hash, s->password.data, s->password.len))
        return 4;
    cl->flags.account_queried = 1;
    LOG("Started an account login query for a client.");
    return 0;
}

static int
_handle_clmsg_select_shard(client_t *cl, clmsg_select_shard_t *s)
{
    if (!cl->flags.authed)
        return 1;
    if (cl->flags.selected_shard)
        return 2;
    if (s->shard_name.len <= 1)
        return 3;
    char name[MAX_SHARD_NAME_LEN + 1];
    memcpy(name, s->shard_name.data, s->shard_name.len);
    name[s->shard_name.len] = 0;
    if (!str_is_ascii(name))
        return 4;
    /* Check if shard id is valid */
    if (!shards_exists(name))
        return 5;
    union
    {
        uint8 list_item[sizeof(svmsg_t) + SVMSG_SHARD_LIST_ITEM_MAX_SZ];
        uint8 shard_select_fail[sizeof(svmsg_t) + SVMSG_SHARD_SELECT_FAIL_SZ];
    } buf;
    bbuf_t bb;
    uint32 shard_index = shards_get_index(name);
    if (!shards_is_online(shard_index))
    {
        svmsg_shard_list_item_t s_msg;
        s_msg.name.len = s->shard_name.len;
        memcpy(s_msg.name.data, name, s->shard_name.len);
        s_msg.online = 0;
        BBUF_INIT(&bb, buf.list_item, sizeof(buf.list_item));
        int r = svmsg_shard_list_item_write(&bb, &s_msg);
        muta_assert(!r);
        if (net_send_all(cl->socket, bb.mem, bb.num_bytes) <= 0)
            return 6;
        LOGF("Sending not online message to player for shard %s.",
            name);
        return 0;
    }
    cl->flags.selected_shard    = 1;
    cl->shard                   = shard_index;
    /* Send information to the shard of the selection */
    addr_t addr;
    if (net_addr_of_sock(cl->socket, &addr))
        return 7;
    if (shards_select_for_player(cl->account_name, cl->account_id, cl->id,
        cl->token, addr.ip,
        shard_index))
    {
        BBUF_INIT(&bb, buf.shard_select_fail, sizeof(buf.shard_select_fail));
        svmsg_shard_select_fail_t l_msg;
        l_msg.reason = SHARD_SELECT_ERROR_UNKNOWN;
        svmsg_shard_select_fail_write(&bb, &l_msg);
        if (net_send_all(cl->socket, bb.mem, bb.num_bytes) <= 0)
            return 8;
        cl->flags.selected_shard = 0;
    }
    return 0;
}

static int
_handle_clmsg_cancel_select_shard(client_t *cl)
{
    if (!cl->flags.selected_shard)
        return 0;
    shards_cancel_select_for_player(cl->shard, cl->account_id, cl->id);
    cl->flags.selected_shard        = 0;
    cl->flags.confirmed_by_shard    = 0;
    return 0;
}
