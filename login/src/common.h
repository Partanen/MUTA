#ifndef MUTA_LOGIN_COMMON_H
#define MUTA_LOGIN_COMMON_H

#include "../../shared/types.h"
#include "../../shared/common_defs.h"
#include "../../shared/sv_common_defs.h"
#include "../../shared/net.h"
#include "../../shared/common_utils.h"

#define MAX_SHARDS 64

#define LOG(fmt_, ...) printf(fmt_ "\n", ##__VA_ARGS__)
#define LOGF(fmt_, ...) printf("%s: " fmt_ "\n", __func__, ##__VA_ARGS__)
#ifdef _MUTA_DEBUG
    #define DEBUG_LOG(fmt_, ...) printf("[DEBUG] " fmt_ "\n", ##__VA_ARGS__)
    #define DEBUG_LOGF(fmt_, ...) \
        printf("[DEBUG] %s: " fmt_ "\n", __func__, ##__VA_ARGS__)
#else
    #define DEBUG_LOG ((void)0)
    #define DEBUG_LOGF ((void)0)
#endif

/* Forward declaration(s) */
typedef struct event_buf_t  event_buf_t;
typedef struct kth_pool_t   kth_pool_t;

/* Types defined here */
typedef struct shard_info_t shard_info_t;
typedef struct config_t     config_t;

struct shard_info_t
{
    addr_t  address;
    char    name[MAX_SHARD_NAME_LEN + 1];
};

struct config_t
{
    uint32 max_clients;
    uint32 listen_backlog;
    bool32 auto_create_accounts;
    /* If on and an account doens't exist, create it */
    struct
    {
        shard_info_t    items[MAX_SHARDS];
        uint32          num;
    } shards;
    char shards_password[MAX_LOGIN_SERVER_PW_LEN + 1];
};

extern event_buf_t  com_event_buf;
extern config_t     com_config;
extern uint64       com_current_tick; /* Tick time of the main loop (ms) */

int
com_init(void);

void
com_destroy(void);

#endif
