#include "event.h"
#include "common.h"
#include "../../shared/common_utils.h"
#include "../../shared/sv_time.h"
#include "../../shared/crypt.h"

typedef struct shards_parse_context_t shards_parse_context_t;

struct shards_parse_context_t
{
    bool32  have_ip;
    char    name[MAX_SHARD_NAME_LEN + 1];
    uint8   ip[4];
};

event_buf_t com_event_buf;
config_t    com_config;
uint64      com_current_tick; /* Tick time of the main loop */

static void
_read_config_callback(void *ctx, const char *opt, const char *val);

static int
_on_shard_def(parse_def_file_context_t *ctx, const char *opt, const char *val);

static int
_on_shard_opt(parse_def_file_context_t *ctx, const char *opt, const char *val);

int
com_init(void)
{
    int ret = 0;
    if (init_socket_api())
    {
        LOGF("Failed to init socket API!");
        ret = 1;
        goto fail;
    }
    if (init_time())
        {ret = 2; goto fail;}
    if (crypt_init())
        {ret = 2; goto fail;}
    event_init(&com_event_buf, sizeof(event_t), 512);
    com_config.max_clients      = 128;
    com_config.listen_backlog   = 5;
    strcpy(com_config.shards_password, "password");
    com_config.auto_create_accounts = 1;
    shards_parse_context_t sp_ctx = {0};
    if (parse_def_file("shards.def", _on_shard_def, _on_shard_opt, &sp_ctx))
        LOG("Warning: errors parsing shard list from shards.def.");
    if (parse_cfg_file("config.cfg", _read_config_callback, 0))
        LOG("Warning: errors parsing configuration from config.cfg.");
    com_current_tick = get_program_ticks_ms();
    return ret;
    fail:
        LOGF("Failed with code %d.", ret);
        com_destroy();
        return ret;
}

void
com_destroy(void)
{
}

static void
_read_config_callback(void *ctx, const char *opt, const char *val)
{
    if (streq(opt, "max clients"))
    {
        uint32 max_clients = str_to_uint32(val);
        if (!max_clients)
            return;
        com_config.max_clients = max_clients;
    } else
    if (streq(opt, "listen backlog"))
    {
        uint32 listen_backlog = str_to_uint32(val);
        if (!listen_backlog)
            return;
        com_config.listen_backlog = listen_backlog;
    } else
    if (streq(opt, "auto create accounts"))
    {
        bool32 v;
        if (str_to_bool(val, &v))
            return;
        com_config.auto_create_accounts = v;
    } else
    if (streq(opt, "shards password"))
    {
        if (strlen(val) > MAX_LOGIN_SERVER_PW_LEN)
            return;
        strcpy(com_config.shards_password, val);
    }
}

static int
_on_shard_def(parse_def_file_context_t *ctx, const char *opt, const char *val)
{
    shards_parse_context_t *sp_ctx = ctx->user_data;
    if (!streq(opt, "shard"))
    {
        LOG("Invalid option '%s' in shards.def.", opt);
        return 1;
    }
    if (com_config.shards.num == MAX_SHARDS)
    {
        LOG("Too many shards defined (max %u).", MAX_SHARDS);
        return 2;
    }
    if (strlen(val) < MIN_SHARD_NAME_LEN || strlen(val) > MAX_SHARD_NAME_LEN)
    {
        LOG("Shard name '%s' length invalid (min %u, max %u).", val,
            MIN_SHARD_NAME_LEN, MAX_SHARD_NAME_LEN);
        return 3;
    }
    strcpy(sp_ctx->name, val);
    sp_ctx->have_ip = 0;
    return 0;
}

static int
_on_shard_opt(parse_def_file_context_t *ctx, const char *opt, const char *val)
{
    shards_parse_context_t *sp_ctx = ctx->user_data;
    if (streq(opt, "address"))
    {
        if (sp_ctx->have_ip)
        {
            LOG("Error: address defined twice for shard '%s'.",
                sp_ctx->name);
            return 1;
        }
        if (sscanf(val, "%hhu.%hhu.%hhu.%hhu", &sp_ctx->ip[0], &sp_ctx->ip[1],
            &sp_ctx->ip[2], &sp_ctx->ip[3]) != 4)
        {
            LOG("Error: Invalid IP address for shard '%s'", sp_ctx->name);
            return 2;
        }
        shard_info_t *si = &com_config.shards.items[com_config.shards.num++];
        strcpy(si->name, sp_ctx->name);
        si->address = create_addr(sp_ctx->ip[0], sp_ctx->ip[1], sp_ctx->ip[2],
            sp_ctx->ip[3], DEFAULT_CLIENT_PORT);
    } else
        return 3;
    return 0;
}
