#include "shards.h"
#include "common.h"
#include "event.h"
#include "clients.h"
#include "../../shared/common_utils.h"
#include "../../shared/sv_common_defs.h"
#include "../../shared/crypt.h"
#include "../../shared/netpoll.h"
#include "../../shared/login_packets.h"
#include "../../shared/svchan_server.h"

#define SHARD_IN_BUF_SIZE   MUTA_MTU
#define SHARD_OUT_BUF_SIZE  MUTA_MTU * 8
#define IS_SHARD(ptr_) \
    ((shard_t*)(ptr_) >= _shards && (shard_t*)(ptr_) < _shards + MAX_SHARDS)
#define SHARD_INDEX(ptr_) ((uint32)((shard_t*)(ptr_) - _shards))

typedef struct shard_t shard_t;

enum shard_flag
{
    SHARD_FLAG_ONLINE_FOR_PLAYERS = (1 << 0)
};

struct shard_t
{
    svchan_server_client_t  *svchan_client; /* NULL if not connected. */
    uint8                   flags;
};

static svchan_server_t  _svchan_server;
static shard_t          _shards[MAX_SHARDS];

static void
_svchan_post_event(svchan_server_event_t *event);

static int
_svchan_on_authed(svchan_server_client_t *client);

static int
_svchan_on_read_packet(svchan_server_client_t *client, uint8 *memory,
    int num_bytes);

static void
_svchan_on_disconnect(svchan_server_client_t *client, int reason);

static void
_on_disconnect(shard_t *shard);

static void
_disconnect_shard(shard_t *shard);

static inline bbuf_t
_send_msg_to_shard(shard_t *shard, uint32 num_bytes);

static inline bbuf_t
_send_var_encrypted_msg_to_shard(shard_t *shard, uint32 num_bytes);

static int
_handle_tlmsg_went_online_for_players(shard_t *shard);

static int
_handle_tlmsg_went_offline_for_players(shard_t *shard);

static int
_handle_tlmsg_player_select_shard_result(shard_t *shard,
    tlmsg_player_select_shard_result_t *s);

int
shards_init(void)
{
    int ret = 0;
    svchan_server_callbacks_t callbacks;
    callbacks.post_event        = _svchan_post_event;
    callbacks.on_authed         = _svchan_on_authed;
    callbacks.on_read_packet    = _svchan_on_read_packet;
    callbacks.on_disconnect     = _svchan_on_disconnect;
    if (svchan_server_init(&_svchan_server, &callbacks, MAX_SHARDS,
        SHARD_IN_BUF_SIZE, SHARD_OUT_BUF_SIZE))
        {ret = 1; goto fail;}
    svchan_server_add_port(&_svchan_server, DEFAULT_SHARD_TO_LOGIN_PORT);
    svchan_server_add_address(&_svchan_server,
        uint32_ip_from_uint8s(127, 0, 0, 1));
    svchan_server_add_account_info(&_svchan_server, "admin",
        com_config.shards_password);
    uint16 port = DEFAULT_SHARD_TO_LOGIN_PORT;
    memset(_shards, 0, sizeof(_shards));
    LOG("Listening to shards on port %u.", port);
    return ret;
    fail:
        LOGF("Failed with code %d.", ret);
        return ret;
}

void
shards_destroy(void)
{
}

int
shards_start(void)
{
    int r = svchan_server_start(&_svchan_server);
    if (r)
        DEBUG_LOGF("svchan_server_start returned %d.\n", r);
    return r;
}

uint32
shards_get_index(const char *name)
{
    for (uint32 i = 0; i < com_config.shards.num; ++i)
        if (streq(com_config.shards.items[i].name, name))
            return i;
    return MAX_SHARDS;
}

const char *
shards_get_name(uint32 index)
{
    muta_assert(index < com_config.shards.num);
    return com_config.shards.items[index].name;
}

addr_t
shards_get_address(uint32 index)
{
    muta_assert(index < com_config.shards.num);
    return com_config.shards.items[index].address;
}

bool32
shards_exists(const char *name)
{
    for (uint32 i = 0; i < com_config.shards.num; ++i)
        if (streq(com_config.shards.items[i].name, name))
            return 1;
    return 0;
}

bool32
shards_is_online(uint32 index)
{
    muta_assert(index < com_config.shards.num);
    shard_t *shard = &_shards[index];
    return shard->svchan_client && shard->flags & SHARD_FLAG_ONLINE_FOR_PLAYERS;
}

int
shards_select_for_player(const char *account_name, uint64 account_id,
    uint32 login_session_id, uint8 *token, uint32 ip, uint32 shard_index)
{
    muta_assert(shard_index < com_config.shards.num);
    shard_t *shard = &_shards[shard_index];
    flmsg_player_select_shard_t l_msg;
    l_msg.account_name.len = (uint8)strlen(account_name);
    memcpy(l_msg.account_name.data, account_name, l_msg.account_name.len);
    l_msg.account_id            = account_id;
    l_msg.login_session_id      = login_session_id;
    l_msg.ip                    = ip;
    memcpy(l_msg.token, token, AUTH_TOKEN_SZ);
    bbuf_t  bb = _send_var_encrypted_msg_to_shard(shard,
        flmsg_player_select_shard_compute_sz(&l_msg));
    if (!bb.max_bytes)
    {
        _disconnect_shard(shard);
        return 1;
    }
    flmsg_player_select_shard_write(&bb,
        svchan_server_client_get_cryptchan(shard->svchan_client), &l_msg);
    LOG("Sent player's select message to shard %s.",
        com_config.shards.items[shard_index].name);
    return 0;
}

void
shards_cancel_select_for_player(uint32 shard_index, uint64 account_id,
    uint32 login_session_id)
{
    muta_assert(shard_index < com_config.shards.num);
    shard_t *shard = &_shards[shard_index];
    if (!(shard->flags & SHARD_FLAG_ONLINE_FOR_PLAYERS))
        return;
    flmsg_player_cancel_select_shard_t s;
    s.account_id        = account_id;
    s.login_session_id  = login_session_id;
    bbuf_t  bb = _send_msg_to_shard(shard, FLMSG_PLAYER_CANCEL_SELECT_SHARD_SZ);
    if (!bb.max_bytes)
    {
        _disconnect_shard(shard);
        return;
    }
    flmsg_player_cancel_select_shard_write(&bb, &s);
}

void
shards_flush()
    {svchan_server_flush(&_svchan_server);}

static void
_svchan_post_event(svchan_server_event_t *event)
{
    event_t new_event;
    new_event.type                      = EVENT_SHARD_SVCHAN_SERVER;
    new_event.shard_svchan_server.event = *event;
    event_push(&com_event_buf, &new_event, 1);
}

static int
_svchan_on_authed(svchan_server_client_t *client)
{
    shard_t *shard = 0;
    for (uint32 i = 0; i < MAX_SHARDS; ++i)
    {
        if (_shards[i].svchan_client)
            continue;
        shard = &_shards[i];
        break;
    }
    if (!shard)
        return 1;
    shard->flags            = 0;
    shard->svchan_client    = client;
    svchan_server_client_set_user_data(client, shard);
    return 0;
}

static int
_svchan_on_read_packet(svchan_server_client_t *client, uint8 *memory,
    int num_bytes)
{
    shard_t *shard      = svchan_server_client_get_user_data(client);
    bbuf_t  bb          = BBUF_INITIALIZER(memory, num_bytes);
    int     incomplete  = 0;
    int     dc          = 0;
    lmsg_t  msg_type;
    while (BBUF_FREE_SPACE(&bb) >= sizeof(msg_type))
    {
        BBUF_READ(&bb, &msg_type);
        switch (msg_type)
        {
        case TLMSG_WENT_ONLINE_FOR_PLAYERS:
            DEBUG_LOG("TLMSG_WENT_ONLINE_FOR_PLAYERS");
            dc = _handle_tlmsg_went_online_for_players(shard);
            break;
        case TLMSG_WENT_OFFLINE_FOR_PLAYERS:
            DEBUG_LOG("TLMSG_WENT_OFFLINE_FOR_PLAYERS");
            dc = _handle_tlmsg_went_offline_for_players(shard);
            break;
        case TLMSG_PLAYER_SELECT_SHARD_RESULT:
        {
            DEBUG_LOG("TLMSG_PLAYER_SELECT_SHARD_RESULT");
            tlmsg_player_select_shard_result_t s;
            incomplete = tlmsg_player_select_shard_result_read(
                &bb, svchan_server_client_get_cryptchan(shard->svchan_client),
                &s);
            if (!incomplete)
                dc = _handle_tlmsg_player_select_shard_result(shard, &s);
        }
            break;
        case TLMSG_STILL_HERE:
            break;
        default:
            dc = 1;
            break;
        }
        if (dc || incomplete)
            break;
    }
    if (dc || incomplete < 0)
    {
        DEBUG_LOGF("msg_type %u, dc: %d, incomplete: %d", msg_type, dc,
            incomplete);
        return -1;
    }
    return BBUF_FREE_SPACE(&bb);
}

static void
_svchan_on_disconnect(svchan_server_client_t *client, int reason)
    {_on_disconnect(svchan_server_client_get_user_data(client));}

static void
_on_disconnect(shard_t *shard)
{
    LOG("Disconnecting shard.");
    shard->svchan_client    = 0;
    shard->flags            = 0;
    if (shard->flags & SHARD_FLAG_ONLINE_FOR_PLAYERS)
        cl_on_shard_status_changed(SHARD_INDEX(shard), 0);
}

static void
_disconnect_shard(shard_t *shard)
    {svchan_server_client_disconnect(shard->svchan_client);}

static inline bbuf_t
_send_msg_to_shard(shard_t *shard, uint32 num_bytes)
{
    return svchan_server_client_send(shard->svchan_client,
        sizeof(lmsg_t) + num_bytes);
}

static inline bbuf_t
_send_var_encrypted_msg_to_shard(shard_t *shard, uint32 num_bytes)
{
    return _send_msg_to_shard(shard, sizeof(msg_sz_t) +
        CRYPT_MSG_ADDITIONAL_BYTES + num_bytes);
}

static int
_handle_tlmsg_went_online_for_players(shard_t *shard)
{
    if (shard->flags & SHARD_FLAG_ONLINE_FOR_PLAYERS)
        return 0;
    shard->flags |= SHARD_FLAG_ONLINE_FOR_PLAYERS;
    cl_on_shard_status_changed(SHARD_INDEX(shard), 1);
    return 0;
}

static int
_handle_tlmsg_went_offline_for_players(shard_t *shard)
{
    if (!(shard->flags & SHARD_FLAG_ONLINE_FOR_PLAYERS))
        return 0;
    shard->flags &= ~SHARD_FLAG_ONLINE_FOR_PLAYERS;
    cl_on_shard_status_changed(SHARD_INDEX(shard), 0);
    return 0;
}

static int
_handle_tlmsg_player_select_shard_result(shard_t *shard,
    tlmsg_player_select_shard_result_t *s)
{
    int r = cl_finish_select_shard_attempt(s->account_id, s->login_session_id,
        s->result);
    if (!r)
        return 0;
    DEBUG_LOGF("cl_finish_select_shard_attempt() failed with code %d.", r);
    flmsg_player_cancel_select_shard_t c_msg;
    c_msg.account_id        = s->account_id;
    c_msg.login_session_id  = s->login_session_id;
    bbuf_t bb = _send_msg_to_shard(shard, FLMSG_PLAYER_CANCEL_SELECT_SHARD_SZ);
    if (!bb.max_bytes)
        return 2;
    flmsg_player_cancel_select_shard_write(&bb, &c_msg);
    return 0;
}
