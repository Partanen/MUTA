CC					=	gcc
EXE_NAME			=	muta_login
INCLUDES			=	-I ../libs/linux/include
C_FLAGS_COMMON		=	$(INCLUDES) -Wall -std=gnu99
C_FLAGS_DEBUG		=	$(C_FLAGS_COMMON) -O0 -g -DEBUG -D_MUTA_DEBUG
C_FLAGS_OPTIMIZED 	=	$(C_FLAGS_COMMON) -DNDEBUG -O2 -flto
C_FLAGS				=	$(C_FLAGS_DEBUG)
L_FLAGS_COMMON		=	$(LIB_DIRS) -std=gnu99 -Wall -pthread -Wl,-rpath='.'
L_FLAGS_DEBUG		=	$(L_FLAGS_COMMON) -O0 -g
L_FLAGS_OPTIMIZED	=	$(L_FLAGS_COMMON) -O2 -flto -static-libgcc
L_FLAGS				=	$(L_FLAGS_DEBUG)
LIBS				=	-lsodium -lmariadb
LIB_DIRS			=	-L ../libs/linux/lib \
						-Wl,-rpath='.'
COMPILE_OBJ 		=	$(CC) -c $(C_FLAGS) $< -o $@
RM					=	rm -f
S					=	/
ALL_DEPS			=
ALL					=	make -j$(shell grep -c '^processor' /proc/cpuinfo) objs && make link

.DEFAULT_GOAL := all

run:
	cd rundir && ./run.sh

run-debug:
	cd rundir && ./run-debug.sh./run-debug.sh

link:
	$(CC) $(L_FLAGS) $(OBJS) $(LIBS) -o $(RUN_DIR)/$(EXE_NAME)

link-sanitize: L_FLAGS = $(L_FLAGS_DEBUG) -fsanitize=address
link-sanitize: link
