# MUTA
Multi-User Time-Wasting Activity

A 2D isometric MMOPRG.

## Current state of the project
No game content yet, still implementing the required infrastructure for that.
MUTA is currently in development by a single person and builds on other
machines are not tested regularly, so it is possible building on a different
setup () will produce issues.

## Cloning the repository
The git-lfs plugin must be installed.

## Building
### GNU/Linux
#### Requirements:
* libmariadb    
* libreadline
* libgl (at least OpenGL version 2.0)
* make
* automake
* gcc

```
$ ./configure
$ make depend
$ make
```

To only build server side software, type instead of make, use "make server".

### Windows
#### Requirements
* Visual Studio 13 or later
Open the x64 version of Visual Studio Developer Command Prompt and run:

```
$ configure.bat x64
$ nmake
```

To only build server side software, type instead of nmake, use "nmake server".

### Setting up the database for development
Install MariaDB, then run the database init scripts in the sql directory:

```
$ mysql -u root -p -e "SOURCE create_account_db.sql;"
$ mysql -u root -p -e "SOURCE create_shard_db.sql;"
```
