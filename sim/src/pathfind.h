#ifndef MUTA_SIM_PATHFIND_H
#define MUTA_SIM_PATHFIND_H

#include "../../shared/types.h"

#define PF_INVALID_HANDLE 0xFFFFFFFF

/* Forward declaration(s) */
typedef struct part_t part_t;

/* Types defined here */
typedef struct pf_user_data_t pf_user_data_t;

struct pf_user_data_t
{
    int     entity_type;
    uint32  index;
};

void
pf_init(void);

void
pf_destroy(void);

int
pf_find(pf_user_data_t user_data, part_t *part, int start_x, int start_y,
    int start_z, int end_x, int end_y, int end_z);
/* Fails if start and end positions are the same. */

pf_user_data_t
pf_get_user_data(void *args);
/* Get user data from the arguments found in a part event. */

void
pf_free_args(void *args);
/* Free arguments after usage. */

#endif  /* MUTA_SIM_PATHFIND_H */
