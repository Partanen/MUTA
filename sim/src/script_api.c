#include "script_api.h"
#include "player.h"
#include "creature.h"
#include "dynamic_object.h"
#include "master.h"
#include "common.h"
#include "../../shared/common_utils.h"
#include "../../shared/sim_packets.h"

static inline entity_t *
_get_entity_of_script_entity(script_entity_t *entity);

static inline void
_get_common_type_and_runtime_id_of_entity(script_entity_t *entity,
    enum common_entity_type *ret_type, uint32 *ret_runtime_id);

bool32
script_is_entity_moving(script_entity_t *entity)
{
    switch (entity->type)
    {
    case SCRIPT_ENTITY_TYPE_PLAYER:
        return pl_is_moving(entity->data);
    default:
        muta_assert(0);
    }
    return 0;
}

void
script_stop_charge(script_entity_t *entity)
{
    switch (entity->type)
    {
    case SCRIPT_ENTITY_TYPE_PLAYER:
        pl_stop_charging_ability(entity->data);
        break;
    default:
        muta_assert(0);
    }
}

void
script_get_entity_position(script_entity_t *entity, int ret_position[3])
{
    entity_t *e = _get_entity_of_script_entity(entity);;
    for (int i = 0; i < 3; ++i)
        ret_position[i] = e->position[i];
}

enum iso_dir
script_get_entity_direction(script_entity_t *entity)
{
    entity_t *e = _get_entity_of_script_entity(entity);;
    return (enum iso_dir)e->direction;
}

void
script_compute_distance_vec(int position_a[3], int position_b[3],
    int ret_distance[3])
{
    ret_distance[0] = abs(position_a[0] - position_b[0]);
    ret_distance[1] = abs(position_a[1] - position_b[1]);
    ret_distance[2] = abs(position_a[2] - position_b[2]);
}

void
ability_script_request_deal_damage(ability_script_t *script,
    script_entity_t *entity, script_entity_t *target, uint32_t damage)
{
    fsimmsg_ability_script_request_deal_damage_t s =
    {
        .damage              = damage,
        .script_id           = script->id
    };
    enum common_entity_type entity_type, target_type;
    _get_common_type_and_runtime_id_of_entity(entity, &entity_type,
        &s.entity_runtime_id);
    _get_common_type_and_runtime_id_of_entity(target, &target_type,
        &s.target_runtime_id);
    s.entity_type = (uint8)entity_type;
    s.target_type = (uint8)target_type;
    bbuf_t bb = master_send(FSIMMSG_ABILITY_SCRIPT_REQUEST_DEAL_DAMAGE_SZ);
    if (bb.max_bytes)
        fsimmsg_ability_script_request_deal_damage_write(&bb, &s);
    LOG_DEBUG_EXT("sim entity_type: %d, entity_runtime_id: %d, target_type: %d, target_runtime_id: %d",
        (int)s.entity_type, (int)s.entity_runtime_id, (int)s.target_type, (int)s.target_runtime_id);
}

void
_script_log_info(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vfprintf(stdout, fmt, args);
    va_end(args);
}

void
_script_log_debug(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vfprintf(stdout, fmt, args);
    va_end(args);
}

void
_script_log_error(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);
}

static inline entity_t *
_get_entity_of_script_entity(script_entity_t *entity)
{
    entity_t *e;
    switch (entity->type)
    {
    case SCRIPT_ENTITY_TYPE_PLAYER:
        e = &((player_t*)entity->data)->entity;
        break;
    case SCRIPT_ENTITY_TYPE_CREATURE:
        e = &((creature_t*)entity->data)->entity;
        break;
    case SCRIPT_ENTITY_TYPE_DYNAMIC_OBJECT:
        e = &((dynamic_object_t*)entity->data)->entity;
        break;
    default:
        muta_assert(0);
    }
    return e;
}

static inline void
_get_common_type_and_runtime_id_of_entity(script_entity_t *entity,
    enum common_entity_type *ret_type, uint32 *ret_runtime_id)
{
    switch (entity->type)
    {
    case SCRIPT_ENTITY_TYPE_PLAYER:
    {
        player_t *player = entity->data;
        *ret_type       = COMMON_ENTITY_PLAYER;
        *ret_runtime_id = player->world_session_id;
    }
        break;
    case SCRIPT_ENTITY_TYPE_CREATURE:
    {
        creature_t *creature = entity->data;
        *ret_type       = COMMON_ENTITY_CREATURE;
        *ret_runtime_id = creature->runtime_id;
    }
        break;
    case SCRIPT_ENTITY_TYPE_DYNAMIC_OBJECT:
    {
        dynamic_object_t *dobj = entity->data;
        *ret_type       = COMMON_ENTITY_DYNAMIC_OBJECT;
        *ret_runtime_id = dobj->runtime_id;
    }
        break;
    default:
        muta_assert(0);
    }
}
