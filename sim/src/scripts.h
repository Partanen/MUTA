#ifndef MUTA_SIM_SCRIPTS_H
#define MUTA_SIM_SCRIPTS_H

#include "script_api.h"
#include "../../shared/types.h"

void
scripts_init(void);

void
scripts_destroy(void);

ability_script_t *
scripts_find_ability(ability_id_t id);

#endif /* MUTA_SIM_SCRIPTS_H */
