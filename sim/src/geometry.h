/* geometry.h
 * geos on the sim server represents piece of the world map. Each geo may be
 * used by multiple different instance parts if they're simulating different
 * instances. */

#ifndef MUTA_SIM_GEOMETRY_H
#define MUTA_SIM_GEOMETRY_H

#include "../../shared/muta_map_format.h"

/* Forward declaration(s) */
typedef struct muta_chunk_file_t muta_chunk_file_t;

/* Types defined here */
typedef struct geo_t geo_t;

struct geo_t
{
    uint32              map_id;
    muta_chunk_file_t   *chunks;
    /* Chunk relative position on parent map. */
    uint32              x_in_chunks;
    uint32              y_in_chunks;
    uint32              w_in_chunks;
    uint32              h_in_chunks;
};
/* Represents a square area on the world map (1 - N chunks). */

void
geo_init(void);

void
geo_destroy(void);

geo_t *
geo_find(uint32 map_id, uint32 x_in_chunks, uint32 y_in_chunks,
    uint32 w_in_chunks, uint32 h_in_chunks);

geo_t *
geo_new(uint32 map_id, uint32 x_in_chunks, uint32 y_in_chunks,
    uint32 w_in_chunks, uint32 h_in_chunks);

int
geo_get_width_in_tiles(geo_t *geo);

int
geo_get_height_in_tiles(geo_t *geo);

muta_chunk_file_t *
geo_get_chunk_of_tile(geo_t *geo, int x, int y);
/* Takes in untranslated coordinates from the whole world map. Returns 0 if
 * location outside of map. */

tile_t
geo_get_tile(muta_chunk_file_t *chunk, int x, int y, int z);
/* coordinates must be relative to chunk (0 - MUTA_CHUNK_W, 0 - MUTA_CHUNK_W,
 * 0 - MUTA_CHUNK_T) */

void
geo_global_to_chunk_coords(geo_t *geo, int x, int y, int z, int *ret_x,
    int *ret_y, int *ret_z);

bool32
geo_is_location_within_map_limits(geo_t *geo, int x, int y, int z);

bool32
geo_is_throw_trajectory_valid(geo_t *geo, int x1, int y1, int z1, int x2, int y2,
    int z2);
/* If throwing an object, check if it's route is blocked by any tiles on the
 * way. Assumes both locations are within map limits. */

#endif /* MUTA_SIM_GEOMETRY_H */
