/* charge_timer.h
 * Some common code used by all entities that can cast. */

#ifndef MUTA_SIM_CHARGE_TIMER_H
#define MUTA_SIM_CHARGE_TIMER_H

#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct script_entity_t script_entity_t;

/* Types defined here */
typedef struct charge_timer_t charge_timer_t;
typedef struct charge_timer_array_t charge_timer_array_t;

struct charge_timer_t
{
    float           current_ms;
    float           max_ms;
    uint32          entity_index;
    ability_id_t    ability_id;
    uint32          target_index;
    uint32          target_id;
    uint8           target_type; /* One of enum entity_type. */
};

struct charge_timer_array_t
{
    charge_timer_t  *data;
    uint32          num;
    uint32          max;
};

void
charge_timer_array_init(charge_timer_array_t *array, uint32 max);

void
charge_timer_array_destroy(charge_timer_array_t *array);

uint32
charge_timer_array_new(charge_timer_array_t *array, float max_ms,
    uint32 entity_index, ability_id_t ability_id, uint32 target_index,
    uint32 target_id, uint8 target_type);

void
charge_timer_array_free(charge_timer_array_t *array, uint32 index);

void
charge_timer_get_target(charge_timer_t *timer, script_entity_t *ret_target);

#endif /* MUTA_SIM_CHARGE_TIMER_H */
