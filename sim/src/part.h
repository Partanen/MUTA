/* part.h
 * Instance parts. */

#ifndef MUTA_SIM_PART_H
#define MUTA_SIM_PART_H

#include "defs.h"
#include "../../shared/types.h"
#include "../../shared/common_utils.h"

/* Forward declaration(s) */
typedef struct geo_t        geo_t;
typedef struct tile_def_t   tile_def_t;

/* Types defined here */
typedef struct part_t                           part_t;
typedef struct part_pathfind_success_event_t    part_pathfind_success_event_t;
typedef struct part_pathfind_fail_event_t       part_pathfind_fail_event_t;
typedef struct part_event_t                     part_event_t;

enum part_event
{
    PART_EVENT_PATHFIND_SUCCESS,
    PART_EVENT_PATHFIND_FAIL
};

struct part_t
{
    uint32      id;
    uint32      instance_id;
    geo_t       *geo;
    event_buf_t event_buf;
    uint32      num_unfinished_jobs;
};

struct part_pathfind_success_event_t
{
    void    *args;
    uint32  num_directions;
    uint8   directions[MAX_PATHFIND_PATH];
};

struct part_pathfind_fail_event_t
{
    void *args;
};

struct part_event_t
{
    int type;
    union
    {
        part_pathfind_success_event_t   pathfind_success;
        part_pathfind_fail_event_t      pathfind_fail;
    };
};

void
part_init(void);

void
part_destroy(void);

part_t *
part_load(uint32 part_id, uint32 instance_id, uint32 map_id, uint32 x_in_chunks,
    uint32 y_in_chunks, uint32 w_in_chunks, uint32 h_in_chunks);

void
part_unload(part_t *part);

part_t *
part_find(uint32 part_id);

uint32
part_get_index(part_t *part);

part_t *
part_get(uint32 index);

void
part_update(float delta_seconds);

tile_t
part_get_tile(part_t *part, int x, int y, int z);
/* Returns TILE_INVISIBLE_COLLIDER if position is outside of map. */

tile_def_t *
part_get_tile_def(part_t *part, int x, int y, int z);
/* Convenience function for common part_get_tile() usage. */

void
part_push_events(part_t *part, part_event_t *events, int32 num);

#endif /* MUTA_SIM_PART_H */
