#include "instance.h"
#include "part.h"
#include "geometry.h"
#include "../../shared/hashtable.h"
#include "../../shared/common_utils.h"
#include "../../shared/common_defs.h"

typedef struct instance_t   instance_t;
typedef struct inst_table   inst_table_t;

struct instance_t
{
    uint32 *parts; /* darr */
};

hashtable_define(inst_table, uint32, instance_t);

static inst_table_t _table;

void
inst_init(void)
{
    inst_table_einit(&_table, 16);
}

void
inst_destroy(void)
{
    uint32      key;
    instance_t  value;
    hashtable_for_each_pair(_table, key, value)
        darr_free(value.parts);
    inst_table_destroy(&_table);
}

void
inst_add_part(uint32 instance_id, part_t *part)
{
    instance_t *inst = inst_table_find(&_table, instance_id);
    if (!inst)
    {
        instance_t new_inst = {.parts = 0};
        inst_table_einsert(&_table, instance_id, new_inst);
        inst = inst_table_find(&_table, instance_id);
    }
    darr_push(inst->parts, part_get_index(part));
}

void
inst_del_part(uint32 instance_id, part_t *part)
{
    instance_t *inst = inst_table_find(&_table, instance_id);
    muta_assert(inst);
    uint32 index    = part_get_index(part);
    uint32 num      = darr_num(inst->parts);
    for (uint32 i = 0; i < num; ++i)
    {
        if (inst->parts[i] != index)
            continue;
        darr_erase_unordered(inst->parts, i);
        break;
    }
    if (!darr_num(inst->parts))
        inst_table_erase(&_table, instance_id);
}

part_t *
inst_find_part(uint32 instance_id, int x, int y)
{
    instance_t *inst = inst_table_find(&_table, instance_id);
    if (!inst)
        return 0;
    int chunk_x = x / MAP_CHUNK_W;
    int chunk_y = y / MAP_CHUNK_W;
    uint32 num = darr_num(inst->parts);
    for (uint32 i = 0; i < num; ++i)
    {
        part_t *part = part_get(inst->parts[i]);
        if (part->geo->x_in_chunks == chunk_x &&
            part->geo->y_in_chunks == chunk_y)
            return part;
    }
    return 0;
}
