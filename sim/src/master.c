#include "master.h"
#include "common.h"
#include "part.h"
#include "player.h"
#include "dynamic_object.h"
#include "instance.h"
#include "creature.h"
#include "geometry.h"
#include "../../shared/svchan_client.h"
#include "../../shared/sim_packets.h"
#include "../../shared/entities.h"

static svchan_client_t _client;

static void
_svchan_post_event(svchan_client_event_t *event);

static void
_svchan_on_authed(svchan_client_t *client);

static int
_svchan_on_read_packet(svchan_client_t *client, uint8 *memory, int num_bytes);

static void
_svchan_on_disconnect(svchan_client_t *client);

static void
_svchan_on_connection_failed(svchan_client_t *client,
    enum svchan_client_connection_error reason);

static void
_svchan_send_still_here_msg(svchan_client_t *client);

static void
_svchan_on_connected(svchan_client_t *client);

static int
_handle_tsimmsg_load_instance_part(tsimmsg_load_instance_part_t *s);

static int
_handle_tsimmsg_unload_instance_part(tsimmsg_unload_instance_part_t *s);

static int
_handle_tsimmsg_request_spawn_player(tsimmsg_request_spawn_player_t *s);

static int
_handle_tsimmsg_request_despawn_player(tsimmsg_request_despawn_player_t *s);

static int
_handle_tsimmsg_request_spawn_dynamic_object(
    tsimmsg_request_spawn_dynamic_object_t *s);

static int
_handle_tsimmsg_request_despawn_dynamic_object(
    tsimmsg_request_despawn_dynamic_object_t *s);

static int
_handle_tsimmsg_request_spawn_creature(tsimmsg_request_spawn_creature_t *s);

static int
_handle_tsimmsg_request_despawn_creature(tsimmsg_request_despawn_creature_t *s);

static int
_handle_tsimmsg_request_move_player_in_direction(
    tsimmsg_request_move_player_in_direction_t *s);

static int
_handle_tsimmsg_request_find_path_player(tsimmsg_request_find_path_player_t *s);

static int
_handle_tsimmsg_request_player_drop_dynamic_object(
    tsimmsg_request_player_drop_dynamic_object_t *s);

static int
_handle_tsimmsg_make_dynamic_object_active(
    tsimmsg_make_dynamic_object_active_t *s);

static int
_handle_tsimmsg_spawn_noncontrolled_dynamic_object(
    tsimmsg_spawn_noncontrolled_dynamic_object_t *s);

static int
_handle_tsimmsg_request_player_use_ability(
    tsimmsg_request_player_use_ability_t *s);

static int
_handle_tsimmsg_request_player_stop_ability_charge(
    tsimmsg_request_player_stop_ability_charge_t *s);

static int
_handle_tsimmsg_entity_dealt_damage_read(tsimmsg_entity_dealt_damage_t *s);

void
master_init(void)
{
    int err;
    svchan_client_callbacks_t callbacks;
    callbacks.post_event            = _svchan_post_event;
    callbacks.on_authed             = _svchan_on_authed;
    callbacks.on_read_packet        = _svchan_on_read_packet;
    callbacks.on_disconnect         = _svchan_on_disconnect;
    callbacks.on_connection_failed  = _svchan_on_connection_failed;
    callbacks.send_still_here_msg   = _svchan_send_still_here_msg;
    callbacks.on_connected          = _svchan_on_connected;
    if (svchan_client_init(&_client, &callbacks, 0, 0xFFFF / 2, 0xFFFF / 2, 1))
        {err = 1; goto fail;}
    return;
    fail:
        muta_panic_print("%s failed with error %d.", __func__, err);
}

void
master_destroy(void)
{
    svchan_client_destroy(&_client);
}

void
master_connect(void)
{
    addr_t address = create_addr(com_config.master_address[0],
        com_config.master_address[1], com_config.master_address[2],
        com_config.master_address[3], com_config.master_port);
    if (svchan_client_connect(&_client, &address, com_config.master_username,
            com_config.master_password))
        muta_panic_print("svchan_client_connect failed.");
}

void
master_disconnect(void)
{
    svchan_client_disconnect(&_client);
}

bbuf_t
master_send(uint32 num_bytes)
    {return svchan_client_send(&_client, sizeof(simmsg_t) + num_bytes);}

void
master_flush(void)
    {svchan_client_flush(&_client);}

static void
_svchan_post_event(svchan_client_event_t *event)
{
    com_event_t com_event;
    com_event.type          = COM_EVENT_SVCHAN_CLIENT;
    com_event.svchan_client = *event;
    com_push_events(&com_event, 1);
}

static void
_svchan_on_authed(svchan_client_t *client)
{
}

static int
_svchan_on_read_packet(svchan_client_t *client, uint8 *memory, int num_bytes)
{
    bbuf_t bb = BBUF_INITIALIZER(memory, num_bytes);
    int incomplete  = 0;
    int dc          = 0;
    simmsg_t msg_type;
    while (BBUF_FREE_SPACE(&bb) >= sizeof(simmsg_t))
    {
        BBUF_READ(&bb, &msg_type);
        switch (msg_type)
        {
        case TSIMMSG_LOAD_INSTANCE_PART:
        {
            tsimmsg_load_instance_part_t s;
            incomplete = tsimmsg_load_instance_part_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_load_instance_part(&s);
        }
            break;
        case TSIMMSG_UNLOAD_INSTANCE_PART:
        {
            tsimmsg_unload_instance_part_t s;
            incomplete = tsimmsg_unload_instance_part_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_unload_instance_part(&s);
        }
            break;
        case TSIMMSG_REQUEST_SPAWN_PLAYER:
        {
            tsimmsg_request_spawn_player_t s;
            incomplete = tsimmsg_request_spawn_player_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_request_spawn_player(&s);
        }
            break;
        case TSIMMSG_REQUEST_DESPAWN_PLAYER:
        {
            tsimmsg_request_despawn_player_t s;
            incomplete = tsimmsg_request_despawn_player_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_request_despawn_player(&s);
        }
            break;
        case TSIMMSG_REQUEST_SPAWN_DYNAMIC_OBJECT:
        {
            tsimmsg_request_spawn_dynamic_object_t s;
            incomplete = tsimmsg_request_spawn_dynamic_object_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_request_spawn_dynamic_object(&s);
        }
            break;
        case TSIMMSG_REQUEST_DESPAWN_DYNAMIC_OBJECT:
        {
            tsimmsg_request_despawn_dynamic_object_t s;
            incomplete = tsimmsg_request_despawn_dynamic_object_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_request_despawn_dynamic_object(&s);
        }
            break;
        case TSIMMSG_REQUEST_SPAWN_CREATURE:
        {
            tsimmsg_request_spawn_creature_t s;
            incomplete = tsimmsg_request_spawn_creature_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_request_spawn_creature(&s);
        }
            break;
        case TSIMMSG_REQUEST_DESPAWN_CREATURE:
        {
            tsimmsg_request_despawn_creature_t s;
            incomplete = tsimmsg_request_despawn_creature_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_request_despawn_creature(&s);
        }
            break;
        case TSIMMSG_REQUEST_MOVE_PLAYER_IN_DIRECTION:
        {
            DEBUG_PUTS("TSIMMSG_REQUEST_MOVE_PLAYER_IN_DIRECTION");
            tsimmsg_request_move_player_in_direction_t s;
            incomplete = tsimmsg_request_move_player_in_direction_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_request_move_player_in_direction(&s);
        }
            break;
        case TSIMMSG_REQUEST_FIND_PATH_PLAYER:
        {
            tsimmsg_request_find_path_player_t s;
            incomplete = tsimmsg_request_find_path_player_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_request_find_path_player(&s);
        }
            break;
        case TSIMMSG_REQUEST_PLAYER_DROP_DYNAMIC_OBJECT:
        {
            tsimmsg_request_player_drop_dynamic_object_t s;
            incomplete = tsimmsg_request_player_drop_dynamic_object_read(&bb,
                &s);
            if (!incomplete)
                dc = _handle_tsimmsg_request_player_drop_dynamic_object(&s);
        }
            break;
        case TSIMMSG_MAKE_DYNAMIC_OBJECT_ACTIVE:
        {
            tsimmsg_make_dynamic_object_active_t s;
            incomplete = tsimmsg_make_dynamic_object_active_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_make_dynamic_object_active(&s);
        }
            break;
        case TSIMMSG_SPAWN_NONCONTROLLED_DYNAMIC_OBJECT:
        {
            tsimmsg_spawn_noncontrolled_dynamic_object_t s;
            incomplete = tsimmsg_spawn_noncontrolled_dynamic_object_read(&bb,
                &s);
            if (!incomplete)
                dc = _handle_tsimmsg_spawn_noncontrolled_dynamic_object(&s);
        }
            break;
        case TSIMMSG_REQUEST_PLAYER_USE_ABILITY:
        {
            tsimmsg_request_player_use_ability_t s;
            incomplete = tsimmsg_request_player_use_ability_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_request_player_use_ability(&s);
        }
            break;
        case TSIMMSG_REQUEST_PLAYER_STOP_ABILITY_CHARGE:
        {
            tsimmsg_request_player_stop_ability_charge_t s;
            incomplete = tsimmsg_request_player_stop_ability_charge_read(&bb,
                &s);
            if (!incomplete)
                dc = _handle_tsimmsg_request_player_stop_ability_charge(&s);
        }
            break;
        case TSIMMSG_ENTITY_DEALT_DAMAGE:
        {
            tsimmsg_entity_dealt_damage_t s;
            incomplete = tsimmsg_entity_dealt_damage_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_entity_dealt_damage_read(&s);
        }
            break;
#if 0
        case TSIMMSG_REQUEST_TELEPORT_PLAYER_WITHIN_CURRENT_INSTANCE:
        {
            tsimmsg_request_teleport_player_within_current_instance_t s;
            incomplete = tsimmsg_request_teleport_player_within_current_instance_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_request_teleport_player_within_current_instance(&s);
        }
            break;
        case TSIMMSG_REQUEST_TELEPORT_PLAYER_TO_ANOTHER_INSTANCE:
        {
            tsimmsg_request_teleport_player_to_another_instance_t s;
            incomplete = tsimmsg_request_teleport_player_to_another_instance_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_request_teleport_player_to_another_instance(&s);
        }
            break;
        case TSIMMSG_REQUEST_CHANGE_PLAYER_SIM:
        {
            tsimmsg_request_change_player_sim_t s;
            incomplete = tsimmsg_request_change_player_sim_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tsimmsg_request_change_player_sim(&s);
        }
            break;
#endif
        default:
            LOG_EXT("Unknown message type %u.", (uint)msg_type);
            dc = 1;
            break;
        }
        if (dc || incomplete < 0)
        {
            LOG_EXT("Bad message: msg_type %u, dc %d, incomplete %d",
                (uint)msg_type, dc, incomplete);
            return -1;
        }
        if (incomplete)
        {
            bb.num_bytes -= sizeof(simmsg_t);
            break;
        }
    }
    return BBUF_FREE_SPACE(&bb);
}

static void
_svchan_on_disconnect(svchan_client_t *client)
{
    LOG("Disconnected from master.");
}

static void
_svchan_on_connection_failed(svchan_client_t *client,
    enum svchan_client_connection_error reason)
{
    master_connect();
}

static void
_svchan_send_still_here_msg(svchan_client_t *client)
{
    bbuf_t bb = master_send(0);
    if (bb.max_bytes)
        simmsg_keep_alive_write(&bb);
}

static void
_svchan_on_connected(svchan_client_t *client)
{
}

static int
_handle_tsimmsg_load_instance_part(tsimmsg_load_instance_part_t *s)
{
    part_t *part = part_find(s->part_id);
    if (part)
    {
        LOG_ERROR("Master sent load message, but part %u already exists.",
            s->part_id);
        return 1;
    }
    part = part_load(s->part_id, s->instance_id, s->map_id, s->chunk_x,
        s->chunk_y, s->chunk_w, s->chunk_h);
    if (!part)
        return 2;
    fsimmsg_load_instance_part_result_t r =
    {
        .part_id    = s->part_id,
        .result     = 0
    };
    bbuf_t bb = master_send(FSIMMSG_LOAD_INSTANCE_PART_RESULT_SZ);
    if (!bb.max_bytes)
        return 3;
    fsimmsg_load_instance_part_result_write(&bb, &r);
    return 0;
}

static int
_handle_tsimmsg_unload_instance_part(tsimmsg_unload_instance_part_t *s)
{
    part_t *part = part_find(s->part_id);
    if (!part)
    {
        LOG_ERROR("Master requested to unload part %u, but part does not "
            "exist.", s->part_id);
        return 1;
    }
    part_unload(part);
    return 0;
}

static int
_handle_tsimmsg_request_spawn_player(tsimmsg_request_spawn_player_t *s)
{
    if (pl_find(s->world_session_id))
    {
        LOG_ERROR("Master requested spawn player world_session_id %u, but "
            "player already exists.", s->world_session_id);
        return 1;
    }
    part_t *part = inst_find_part(s->instance_id, s->x, s->y);
    if (!part)
    {
        LOG_ERROR("Master requested spawn player into instance %u at position "
            "%d, %d, but part is not simulated by this sim.", s->instance_id,
            s->x, s->y);
        return 2;
    }
    char name[MAX_CHARACTER_NAME_LEN + 1];
    char display_name[MAX_DISPLAY_NAME_LEN + 1];
    memcpy(name, s->name.data, s->name.len);
    name[s->name.len] = 0;
    memcpy(display_name, s->display_name.data, s->display_name.len);
    display_name[s->display_name.len] = 0;
    int r;
    if ((r = pl_spawn(s->world_session_id, s->db_id, name, display_name, s->sex,
        s->race, part, s->direction, s->x, s->y, s->z, s->move_speed, 1,
        s->abilities.data, s->abilities.len, s->health_current, s->health_max)))
    {
        LOG_EXT("pl_spawn() failed with code %d.", r);
        return 3;
    }
    return 0;
}

static int
_handle_tsimmsg_request_despawn_player(tsimmsg_request_despawn_player_t *s)
{
    player_t *pl = pl_find(s->world_session_id);
    if (!pl)
    {
        LOG_ERROR("Master requested despawning player %u, but player does "
            "not exist.", s->world_session_id);
        return 1;
    }
    pl_despawn(pl);
    return 0;
}

static int
_handle_tsimmsg_request_spawn_dynamic_object(
    tsimmsg_request_spawn_dynamic_object_t *s)
{
    if (dobj_find(s->runtime_id))
    {
        LOG_ERROR("Master requested spawn dynamic object %u, but object "
            "already exists.", s->runtime_id);
        return 1;
    }
    part_t *part = inst_find_part(s->instance_id, s->x, s->y);
    if (!part)
    {
        LOG_ERROR("Master requested spawn dynamic object into instance "
            "%u at position %d, %d, but part not controlled by this sim.",
            s->instance_id, (int)s->x, (int)s->y);
        return 2;
    }
    bool32 controlled_by_me = 1;
    bool32 pending_spawn    = 0;
    if (dobj_spawn(s->runtime_id, s->type_id, part, (int)s->direction, s->x,
        s->y, s->z, controlled_by_me, pending_spawn))
        return 3;
    return 0;
}

static int
_handle_tsimmsg_request_despawn_dynamic_object(
    tsimmsg_request_despawn_dynamic_object_t *s)
{
    dynamic_object_t *obj = dobj_find(s->runtime_id);
    if (!obj)
    {
        LOG_ERROR("Master requested despawning dynamic object %u, but object "
            "does not exist.", s->runtime_id);
        return 1;
    }
    dobj_despawn(obj);
    return 0;
}

static int
_handle_tsimmsg_request_spawn_creature(tsimmsg_request_spawn_creature_t *s)
{
    if (creature_find(s->runtime_id))
    {
        LOG_ERROR("Master requested spawn creature %u, but creature "
            "already exists.", s->runtime_id);
        return 1;
    }
    part_t *part = inst_find_part(s->instance_id, s->x, s->y);
    if (!part)
    {
        LOG_ERROR("Master requested spawn creature into instance "
            "%u at position %d, %d, but part not controlled by this sim.",
            s->instance_id, (int)s->x, (int)s->y);
        return 2;
    }
    if (creature_spawn(s->runtime_id, s->type_id, part, (int)s->direction, s->x,
        s->y, s->z, 1, s->health_current, s->health_max))
        return 3;
    return 0;
}

static int
_handle_tsimmsg_request_despawn_creature(tsimmsg_request_despawn_creature_t *s)
{
    creature_t *creature = creature_find(s->runtime_id);
    if (!creature)
    {
        LOG_ERROR("Master requested despawning creature %u, but creature "
            "does not exist.", s->runtime_id);
        return 1;
    }
    creature_despawn(creature);
    return 0;
}

static int
_handle_tsimmsg_request_move_player_in_direction(
    tsimmsg_request_move_player_in_direction_t *s)
{
    player_t *pl = pl_find(s->world_session_id);
    if (!pl)
    {
        LOG_EXT("Master requested move player session id %u, but player not "
            "found.\n", s->world_session_id);
        return 1;
    }
    pl_move_in_direction(pl, s->direction);
    return 0;
}

static int
_handle_tsimmsg_request_find_path_player(tsimmsg_request_find_path_player_t *s)
{
    player_t *pl = pl_find(s->world_session_id);
    if (!pl)
    {
        LOG_EXT("Master requested to find path for player session id %u, but "
            "player does not exist on this sim.", s->world_session_id);
        return 1;
    }
    pl_find_path(pl, s->x, s->y, s->z);
    return 0;
}

static int
_handle_tsimmsg_request_player_drop_dynamic_object(
    tsimmsg_request_player_drop_dynamic_object_t *s)
{
    if (dobj_find(s->dobj_runtime_id))
    {
        LOG_ERROR("Master requested spawn dropped dynamic object %u, but object "
            "already exists.", s->dobj_runtime_id);
        return 1;
    }
    player_t *pl = pl_find(s->player_runtime_id);
    if (!pl)
    {
        LOG_ERROR("Master requested spawn dropped dynamic object %u, but "
            "player %u not found.", s->dobj_runtime_id, s->player_runtime_id);
        return 1;
    }
    part_t *part = inst_find_part(s->instance_id, s->x, s->y);
    if (!part)
    {
        LOG_ERROR("Master requested spawn dropped dynamic object into instance "
            "%u at position %d, %d, but part not controlled by this sim.",
            s->instance_id, (int)s->x, (int)s->y);
        return 3;
    }
    if (!geo_is_location_within_map_limits(part->geo, s->x, s->y, s->z))
    {
        LOG_ERROR("Master requested spawn dropped dynamic object into instance "
            "%u at position %d, %d, but position is out of map limits.",
            s->instance_id, (int)s->x, (int)s->y);
        return 4;
    }
    dynamic_object_def_t *def = ent_get_dynamic_object_def(s->type_id);
    if (!def)
    {
        LOG_ERROR("Master requested spawn dropped dynamic object %u, but "
            "dynamic object type %u s not defined.", s->dobj_runtime_id,
            s->type_id);
        return 5;
    }
    if (dobj_collides_with_tiles(def, part->geo, s->x, s->y, s->z))
    {
        LOG_ERROR("Master requested spawn dropped dynamic object %u, but "
            "dynamic object would collied tiles at given position.",
            s->dobj_runtime_id);
        fsimmsg_player_drop_dynamic_object_fail_t r =
        {
            .dobj_runtime_id    = s->dobj_runtime_id,
            .player_runtime_id  = s->player_runtime_id,
            .disconnect_player  = 1
        };
        bbuf_t bb = master_send(FSIMMSG_PLAYER_DROP_DYNAMIC_OBJECT_FAIL_SZ);
        if (bb.max_bytes)
            fsimmsg_player_drop_dynamic_object_fail_write(&bb, &r);
        return 0;
    }
    if (!geo_is_throw_trajectory_valid(part->geo, pl->entity.position[0],
        pl->entity.position[1], pl->entity.position[2], s->x, s->y, s->z))
    {
        LOG("Master requested spawn dropped dynamic object %u, but "
            "throw trajectory is invalid.", s->dobj_runtime_id);
        fsimmsg_player_drop_dynamic_object_fail_t r =
        {
            .dobj_runtime_id    = s->dobj_runtime_id,
            .player_runtime_id  = s->player_runtime_id,
            .disconnect_player  = 0
        };
        bbuf_t bb = master_send(FSIMMSG_PLAYER_DROP_DYNAMIC_OBJECT_FAIL_SZ);
        if (bb.max_bytes)
            fsimmsg_player_drop_dynamic_object_fail_write(&bb, &r);
        return 0;
    }
    bool32 controlled_by_me = 1;
    bool32 pending_spawn    = 1;
    if (dobj_spawn(s->dobj_runtime_id, s->type_id, part,
        (int)s->direction, s->x, s->y, s->z, controlled_by_me, pending_spawn))
        return 4;
    return 0;
}

static int
_handle_tsimmsg_make_dynamic_object_active(
    tsimmsg_make_dynamic_object_active_t *s)
{
    dynamic_object_t *dobj = dobj_find(s->dobj_runtime_id);
    if (!dobj)
    {
        LOG_ERROR("Dynamic object %u not found.", s->dobj_runtime_id);
        return 1;
    }
    if (!(dobj->flags | DOBJ_FLAG_CONTROLLED_BY_ME))
    {
        LOG_ERROR("Dynamic object %u not controlled by this sim.",
            s->dobj_runtime_id);
        return 2;
    }
    if (!(dobj->flags | DOBJ_FLAG_PENDING_SPAWN))
    {
        LOG_ERROR("Dynamic object %u is not a pending spawn.",
            s->dobj_runtime_id);
        return 3;
    }
    dobj_complete_pending_spawn(dobj);
    return 0;
}

static int
_handle_tsimmsg_spawn_noncontrolled_dynamic_object(
    tsimmsg_spawn_noncontrolled_dynamic_object_t *s)
{
    if (dobj_find(s->runtime_id))
    {
        LOG_ERROR("Master requested spawn non-controlled dynamic object %u, "
            "but object already exists.", s->runtime_id);
        return 1;
    }
    part_t *part = inst_find_part(s->instance_id, s->x, s->y);
    if (!part)
    {
        LOG_ERROR("Master requested spawn non-controleld dynamic object into "
            "instance %u at position %d, %d, but part not controlled by this "
            "sim.", s->instance_id, (int)s->x, (int)s->y);
        return 2;
    }
    bool32 controlled_by_me = 0;
    bool32 pending_spawn    = 0;
    if (dobj_spawn(s->runtime_id, s->type_id, part, (int)s->direction, s->x,
        s->y, s->z, controlled_by_me, pending_spawn))
        return 3;
    return 0;
}

static int
_handle_tsimmsg_request_player_use_ability(
    tsimmsg_request_player_use_ability_t *s)
{
    player_t *pl = pl_find(s->player_runtime_id);
    if (!pl)
        return 1;
    if (pl_use_ability(pl, s->ability_id,
        (enum common_entity_type)s->target_type, s->target_runtime_id))
        return 2;
    return 0;
}

static int
_handle_tsimmsg_request_player_stop_ability_charge(
    tsimmsg_request_player_stop_ability_charge_t *s)
{
    player_t *pl = pl_find(s->player_runtime_id);
    if (!pl)
        return 1;
    if (pl_on_requested_stop_ability_charge(pl))
        return 2;
    return 0;
}

static int
_handle_tsimmsg_entity_dealt_damage_read(tsimmsg_entity_dealt_damage_t *s)
{
    assert(s->entity_type < NUM_COMMON_ENTITY_TYPES);
    assert(s->target_type < NUM_COMMON_ENTITY_TYPES);
    switch (s->target_type)
    {
    case COMMON_ENTITY_PLAYER:
    {
        player_t *player = pl_find(s->target_runtime_id);
        if (!player)
            return 1;
        pl_take_damage(player, s->damage, s->health_current);
        break;
    }
    case COMMON_ENTITY_CREATURE:
    {
        creature_t *creature = creature_find(s->entity_runtime_id);
        if (!creature)
            return 2;
        creature_take_damage(creature, s->damage, s->health_current);
        break;
    }
    }
    return 0;
}
