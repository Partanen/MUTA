#include "part.h"
#include "player.h"
#include "geometry.h"
#include "common.h"
#include "pathfind.h"
#include "instance.h"
#include "../../shared/hashtable.h"
#include "../../shared/common_utils.h"
#include "../../shared/tile.h"

typedef struct part_t       part_t;
typedef struct part_table   part_table_t;

hashtable_define(part_table, uint32, uint32);

fixed_pool(part_t)  _pool;
part_table_t        _table;

static void
_wait_for_events(part_t *part);

static void
_handle_part_event_pathfind_fail(part_t *part,
    part_pathfind_fail_event_t *event);

static void
_handle_part_event_pathfind_success(part_t *part,
    part_pathfind_success_event_t *event);

void
part_init(void)
{
    fixed_pool_init(&_pool, com_config.max_instance_parts);
    part_table_einit(&_table, com_config.max_instance_parts);
}

void
part_destroy(void)
{
    part_table_destroy(&_table);
    fixed_pool_destroy(&_pool);
}

part_t *
part_load(uint32 part_id, uint32 instance_id, uint32 map_id, uint32 x_in_chunks,
    uint32 y_in_chunks, uint32 w_in_chunks, uint32 h_in_chunks)
{
    muta_assert(!part_table_exists(&_table, part_id));
    part_t *part = fixed_pool_new(&_pool);
    if (!part)
    {
        LOG_WARN("Attempted to create part but no free slots left.\n");
        return 0;
    }
    geo_t *geo = geo_find(map_id, x_in_chunks, y_in_chunks, w_in_chunks,
        h_in_chunks);
    if (!geo && !(geo = geo_new(map_id, x_in_chunks, y_in_chunks, w_in_chunks,
        h_in_chunks)))
    {
        LOG_WARN("Failed to create geometry %u [%u, %u, %u, %u] for part %u.",
            x_in_chunks, y_in_chunks, w_in_chunks, h_in_chunks, part_id);
        goto fail;
    }
    part->geo           = geo;
    part->id            = part_id;
    part->instance_id   = instance_id;
    part_table_einsert(&_table, part_id, fixed_pool_index(&_pool, part));
    event_init(&part->event_buf, sizeof(part_event_t), com_config.max_players);
    LOG("Created part with id %u, map_id %u, [%u, %u, %u, %u].", part_id,
        map_id, x_in_chunks, y_in_chunks, w_in_chunks, h_in_chunks);
    inst_add_part(instance_id, part);
    return part;
    fail:
        fixed_pool_free(&_pool, part);
        return 0;
}

void
part_unload(part_t *part)
{
    while (part->num_unfinished_jobs)
        _wait_for_events(part);
    inst_del_part(part->instance_id, part);
    event_destroy(&part->event_buf);
    part_table_erase(&_table, part->id);
    fixed_pool_free(&_pool, part);
}

part_t *
part_find(uint32 part_id)
{
    uint32 *index = part_table_find(&_table, part_id);
    return index ? &_pool.all[*index] : 0;
}

uint32
part_get_index(part_t *part)
    {return fixed_pool_index(&_pool, part);}

part_t *
part_get(uint32 index)
    {return &_pool.all[index];}

void
part_update(float delta_s)
{
    uint32 part_id;
    uint32 part_index;
    hashtable_for_each_pair(_table, part_id, part_index)
    {
        part_t *part = &_pool.all[part_index];
        _wait_for_events(part);
    }
}

tile_t
part_get_tile(part_t *part, int x, int y, int z)
{
    muta_chunk_file_t *chunk = geo_get_chunk_of_tile(part->geo, x, y);
    if (!chunk)
        return TILE_INVISIBLE_COLLIDER;
    int cx, cy, cz;
    geo_global_to_chunk_coords(part->geo, x, y, z, &cx, &cy, &cz);
    return geo_get_tile(chunk, cx, cy, cz);
}

tile_def_t *
part_get_tile_def(part_t *part, int x, int y, int z)
    {return tile_get_def(part_get_tile(part, x, y, z));}

void
part_push_events(part_t *part, part_event_t *events, int32 num)
    {event_push(&part->event_buf, events, num);}

static void
_wait_for_events(part_t *part)
{
    while (part->num_unfinished_jobs)
    {
        part_event_t events[64];
        int32 num_events = event_wait(&part->event_buf, events, 64, -1);
        muta_assert(part->num_unfinished_jobs >= (uint32)num_events);
        for (int32 i = 0; i < num_events; ++i)
        {
            switch (events[i].type)
            {
            case PART_EVENT_PATHFIND_FAIL:
                _handle_part_event_pathfind_fail(part,
                    &events[i].pathfind_fail);
                break;
            case PART_EVENT_PATHFIND_SUCCESS:
                _handle_part_event_pathfind_success(part,
                    &events[i].pathfind_success);
                break;
            default:
                muta_assert(0);
            }
        }
        part->num_unfinished_jobs -= (uint32)num_events;
    }
}

static void
_handle_part_event_pathfind_fail(part_t *part,
    part_pathfind_fail_event_t *event)
{
    LOG("Pathfinding failed!\n");
    pf_user_data_t data = pf_get_user_data(event->args);
    pf_free_args(event->args);
    switch (data.entity_type)
    {
    case ENTITY_TYPE_PLAYER:
        pl_on_pathfind_fail(pl_get(data.index));
        break;
    default:
        muta_assert(0);
    }
}

static void
_handle_part_event_pathfind_success(part_t *part,
    part_pathfind_success_event_t *event)
{
    LOG("Pathfinding success!");
    pf_user_data_t data = pf_get_user_data(event->args);
    pf_free_args(event->args);
    switch (data.entity_type)
    {
    case ENTITY_TYPE_PLAYER:
        pl_on_pathfind_success(pl_get(data.index), event->directions,
            event->num_directions);
        break;
    default:
        muta_assert(0);
    }
}
