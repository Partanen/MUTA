#ifndef MUTA_SIM_INSTANCE_H
#define MUTA_SIM_INSTANCE_H

#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct part_t part_t;

void
inst_init(void);

void
inst_destroy(void);

void
inst_add_part(uint32 instance_id, part_t *part);

void
inst_del_part(uint32 instance_id, part_t *part);

part_t *
inst_find_part(uint32 instance_id, int x, int y);

#endif /* MUTA_SIM_INSTANCE_H */
