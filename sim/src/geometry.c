#include <math.h>
#include "geometry.h"
#include "common.h"
#include "../../shared/common_utils.h"
#include "../../shared/common_defs.h"
#include "../../shared/entities.h"
#include "../../shared/tile.h"

#define TILE_INDEX(x, y, z) \
    ((x) * MAP_CHUNK_W * MAP_CHUNK_T + (y) * MAP_CHUNK_T + (z))

static muta_map_file_t  *_files;
static geo_t            **_geos; /* darr */

void
geo_init(void)
{
    muta_map_db_t db;
    if (muta_map_db_load(&db, "data/common/maps.def"))
        muta_panic_print("Map database not found.");
    uint32 num_entries = muta_map_db_num_entries(&db);
    if (!num_entries)
        muta_panic_print("Map database contains 0 entries.");
    for (uint32 i = 0; i < num_entries; ++i)
    {
        muta_map_file_t map_file;
        if (muta_map_file_load(&map_file, db.entries[i].file_path))
            muta_panic_print("Failed to load map %s.\n",
                db.entries[i].file_path);
        if (map_file.header.w < 3 || map_file.header.h < 3)
            muta_panic_print("Map file %s dimensions are less than 3x3.",
                db.entries[i].file_path);
        darr_push(_files, map_file);
    }
    muta_map_db_destroy(&db);
}

void
geo_destroy(void)
{
    for (uint32 i = 0; i < darr_num(_files); ++i)
        muta_map_file_destroy(&_files[i]);
    for (uint32 i = 0; i < darr_num(_geos); ++i) {/* ? */}
    darr_free(_geos);
}

geo_t *
geo_find(uint32 map_id, uint32 x_in_chunks, uint32 y_in_chunks,
    uint32 w_in_chunks, uint32 h_in_chunks)
{
    uint32 num_geos = darr_num(_geos);
    for (uint32 i = 0; i < num_geos; ++i)
        if (_geos[i]->map_id == map_id &&
            _geos[i]->x_in_chunks == x_in_chunks &&
            _geos[i]->y_in_chunks == y_in_chunks &&
            _geos[i]->w_in_chunks == w_in_chunks &&
            _geos[i]->h_in_chunks == h_in_chunks)
            return _geos[i];
    return 0;
}

geo_t *
geo_new(uint32 id, uint32 x_in_chunks, uint32 y_in_chunks,
    uint32 w_in_chunks, uint32 h_in_chunks)
{
    geo_t           geo     = {0};
    muta_map_file_t *file   = 0;
    for (uint32 i = 0; i < darr_num(_files); ++i)
    {
        if (_files[i].header.id != id)
            continue;
        file = &_files[i];
        break;
    }
    if (!file)
    {
        LOG_EXT("No map with id %u exists.\n", id);
        return 0;
    }
    geo.map_id      = id;
    geo.x_in_chunks = x_in_chunks;
    geo.y_in_chunks = y_in_chunks;
    geo.w_in_chunks = w_in_chunks;
    geo.h_in_chunks = h_in_chunks;
    geo.chunks      = emalloc(
        w_in_chunks * h_in_chunks * sizeof(muta_chunk_file_t));
    /* To be able to pathfind (etc.) to surrounding servers, the part must be
     * aware of the surrounding chunks as well. So we load each surrounding
     * chunk in addition to the ones we simulate. The surrounding chunks may
     * be controlled by other sim servers. */
    uint32 sx = x_in_chunks ? x_in_chunks - 1 : x_in_chunks;
    uint32 sy = y_in_chunks ? y_in_chunks - 1 : y_in_chunks;
    uint32 ex = x_in_chunks + w_in_chunks;
    if (ex < file->header.w)
        ex++;
    uint32 ey = y_in_chunks + h_in_chunks;
    if (ey < file->header.h)
        ey++;
    for (uint32 x = sx; x < ex; ++x)
    {
        for (uint32 y = sy; y < ey; ++y)
        {
            const char *path = muta_map_file_get_chunk_absolute_path(file, x,
                y);
            if (!path)
            {
                LOG_EXT("No path for %u, %u in map %s\n", x, y,
                    file->header.name);
                goto fail;
            }
            uint32 index = y * file->header.w + x;
            if (muta_chunk_file_init(&geo.chunks[index]) ||
                muta_chunk_file_load(&geo.chunks[index], path))
                muta_panic_print("Failed to load chunk file %s.\n", path);
            /* Static objects may never be moved. To speed up lookup time in
             * pathfinding, mark every position of a static object with an
             * invisible tile. */
            for (uint32 i = 0; i < geo.chunks[index].header.num_static_objs;
                ++i)
            {
                stored_static_obj_t *static_obj =
                    &geo.chunks[index].static_objs[i];
                if (static_obj->x >= MAP_CHUNK_W ||
                    static_obj->y >= MAP_CHUNK_W ||
                    static_obj->z < 0 || static_obj->z >= MAP_CHUNK_T)
                    continue;
                static_object_def_t *def = ent_get_static_object_def(
                    static_obj->type_id);
                if (!def)
                    muta_panic_print("Definition for static object type %u in "
                        "chunk %s not found.", static_obj->type_id, path);
                if (def->passthrough)
                    continue;
                geo.chunks[index].tiles[TILE_INDEX(static_obj->x, static_obj->y,
                    static_obj->z)] = TILE_INVISIBLE_COLLIDER;

            }
        }
    }
    geo_t *ret = emalloc(sizeof(geo_t));
    *ret = geo;
    darr_push(_geos, ret);
    return ret;
    fail:
        free(geo.chunks);
        return 0;
}

int
geo_get_width_in_tiles(geo_t *geo)
    {return geo->w_in_chunks * MAP_CHUNK_W;}

int
geo_get_height_in_tiles(geo_t *geo)
    {return geo->h_in_chunks * MAP_CHUNK_W;}

muta_chunk_file_t *
geo_get_chunk_of_tile(geo_t *geo, int x, int y)
{
    if (x < 0 || y < 0 || x >= (int)geo->w_in_chunks * MAP_CHUNK_W ||
        y >= (int)geo->h_in_chunks * MAP_CHUNK_W)
        return 0;
    x -= geo->x_in_chunks * MAP_CHUNK_W;
    y -= geo->x_in_chunks * MAP_CHUNK_W;
    return &geo->chunks[y / MAP_CHUNK_W * geo->w_in_chunks + x / MAP_CHUNK_W];
}

tile_t
geo_get_tile(muta_chunk_file_t *chunk, int x, int y, int z)
    {return chunk->tiles[TILE_INDEX(x, y, z)];}

void
geo_global_to_chunk_coords(geo_t *geo, int x, int y, int z, int *ret_x,
    int *ret_y, int *ret_z)
{
    *ret_x = x % MAP_CHUNK_W;
    *ret_y = y % MAP_CHUNK_W;
    *ret_z = z % MAP_CHUNK_T;
}

bool32
geo_is_location_within_map_limits(geo_t *geo, int x, int y, int z)
{
    if (z < 0 || z >= MAP_CHUNK_T ||
        x < 0 || x >= geo_get_width_in_tiles(geo) ||
        y < 0 || y >= geo_get_height_in_tiles(geo))
        return 0;
    return 1;
}

bool32
geo_is_throw_trajectory_valid(geo_t *geo, int x1, int y1, int z1, int x2,
    int y2, int z2)
{
    muta_assert(geo_is_location_within_map_limits(geo, x1, y1, z1));
    muta_assert(geo_is_location_within_map_limits(geo, x2, y2, z2));
    float   x_diff  = (float)(x2 - x1);
    float   y_diff  = (float)(y2 - y1);
    int     z_diff;
    if (z2 > z1)
        z_diff = abs(z2 - z1);
    else
        z_diff = 1;
    int len = (int)sqrtf(x_diff * x_diff + y_diff * y_diff);
    if (!len)
        return 1;
    for (int i = 0; i < len; ++i)
    {
        float percentage    = (float)i / (float)len;
        float x_add         = percentage * x_diff;
        float y_add         = percentage * y_diff;
        for (int j = 0; j < z_diff; ++j)

        {
            int target_x = (int)roundf(x1 + x_add);
            int target_y = (int)roundf(y1 + y_add);
            int target_z = z1 + j;
            muta_chunk_file_t *chunk = geo_get_chunk_of_tile(geo, target_x,
                target_y);
            tile_t tile = geo_get_tile(chunk, target_x, target_y, target_z);
            tile_def_t *tile_def = tile_get_def(tile);
            if (!tile_def->passthrough)
            {
                DEBUG_PRINTFF("TILE %d, %d, %d not passhthrough\n", target_x, target_y, target_z);
                return 0;
            }
        }
    }
    return 1;
}
