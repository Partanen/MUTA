#include "../script_api.h"

extern ability_script_t attack_ability_script;

ability_script_t *all_ability_scripts[] =
{
    &attack_ability_script
};

uint32_t num_all_ability_scripts =
    sizeof(all_ability_scripts) / sizeof(all_ability_scripts[0]);
