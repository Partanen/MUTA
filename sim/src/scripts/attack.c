#include "../script_api.h"

static enum ability_script_use_result
_use(ability_script_t const * script, script_entity_t *user,
    script_entity_t *target);

static uint32_t
_compute_charge_time(script_entity_t *user, script_entity_t *target);

static bool32
_on_charge_finished(script_entity_t *user, script_entity_t *target);

static void
_on_user_moved(script_entity_t *user, script_entity_t *target);

ability_script_t attack_ability_script =
{
    .id                     = 0,
    .use                    = _use,
    .compute_charge_time    = _compute_charge_time,
    .on_charge_finished     = _on_charge_finished,
    .on_user_moved          = _on_user_moved
};

static enum ability_script_use_result _use(ability_script_t const * script,
    script_entity_t *user, script_entity_t *target)
{
    if (script_is_entity_moving(user))
        return ABILITY_SCRIPT_USE_FAIL;
    if (user->data == target->data)
        return ABILITY_SCRIPT_USE_FAIL;
    int user_pos[3];
    int target_pos[3];
    int distance_vec[3];
    script_get_entity_position(user, user_pos);
    script_get_entity_position(target, target_pos);
    script_compute_distance_vec(user_pos, target_pos, distance_vec);
    if (distance_vec[0] > 1 || distance_vec[1] > 1 || distance_vec[2] > 0)
        return ABILITY_SCRIPT_USE_FAIL;
    return ABILITY_SCRIPT_USE_SUCCESS;
}

static uint32_t _compute_charge_time(script_entity_t *user,
    script_entity_t *target)
{
    return 1000;
}

static bool32 _on_charge_finished(script_entity_t *user,
    script_entity_t *target)
{
    if (!target)
        return 0;
    int user_pos[3];
    int target_pos[3];
    int distance_vec[3];
    script_get_entity_position(user, user_pos);
    script_get_entity_position(target, target_pos);
    script_compute_distance_vec(user_pos, target_pos, distance_vec);
    if (distance_vec[0] > 1 || distance_vec[1] > 1 || distance_vec[2] > 0)
        return 0;
    ability_script_request_deal_damage(&attack_ability_script, user, target,
        10);
    return 1;
}

static void
_on_user_moved(script_entity_t *user, script_entity_t *target)
{
    script_stop_charge(user);
}
