#ifndef MUTA_CLIENT_ALL_SCRIPTS_H
#define MUTA_CLIENT_ALL_SCRIPTS_H

#include "../script_api.h"

extern ability_script_t *all_ability_scripts[];
extern uint32_t num_all_ability_scripts;

#endif /* MUTA_CLIENT_ALL_SCRIPTS_H */
