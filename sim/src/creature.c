#include "creature.h"
#include "common.h"
#include "master.h"
#include "part.h"
#include "../../shared/common_utils.h"
#include "../../shared/hashtable.h"
#include "../../shared/sim_packets.h"

typedef struct creature_table creature_table_t;

hashtable_define(creature_table, creature_runtime_id_t, creature_index_t);

static fixed_pool(creature_t)   _pool;
static creature_table_t         _table;

void
creature_init(void)
{
    uint32 max = com_config.max_creatures;
    fixed_pool_init(&_pool, max);
    creature_table_einit(&_table, (size_t)(1.5f * (float)max));
}

void
creature_destroy(void)
{
    creature_table_destroy(&_table);
    fixed_pool_destroy(&_pool);
}

int
creature_spawn(creature_runtime_id_t runtime_id, creature_type_id_t type_id,
    part_t *part, int direction, int x, int y, int z, bool32 controlled_by_me,
    uint32 health_current, uint32 health_max)
{
    creature_t *creature = fixed_pool_new(&_pool);
    if (!creature)
    {
        LOG_ERROR("Cannot spawn creature object: too many already spawned.");
        fsimmsg_spawn_creature_fail_t s = {.runtime_id = runtime_id};
        bbuf_t bb = master_send(FSIMMSG_SPAWN_CREATURE_FAIL_SZ);
        if (bb.max_bytes)
            fsimmsg_spawn_creature_fail_write(&bb, &s);
        return 0;
    }
    creature->runtime_id            = runtime_id;
    creature->type_id               = type_id;
    creature->entity.position[0]    = x;
    creature->entity.position[1]    = y;
    creature->entity.position[2]    = z;
    creature->entity.direction      = (uint8)direction;
    creature->health_current        = health_current;
    creature->health_max            = health_max;
    creature->flags                 = 0;
    if (controlled_by_me)
        creature->flags |= CREATURE_CONTROLLED_BY_ME;
    creature_table_einsert(&_table, runtime_id, fixed_pool_index(&_pool,
        creature));
    LOG("Spawned creature id %u with type id %u.", runtime_id, type_id);
    if (controlled_by_me)
    {
        fsimmsg_confirm_spawn_creature_t s =
        {
            .runtime_id = runtime_id,
            .instance_part_id = part->id,
            .x                  = x,
            .y                  = y,
            .z                  = (uint8)z,
            .direction          = (uint8)direction
        };
        bbuf_t bb = master_send(FSIMMSG_CONFIRM_SPAWN_CREATURE_SZ);
        if (bb.max_bytes)
            fsimmsg_confirm_spawn_creature_write(&bb, &s);
    }
    return 0;
}

void
creature_despawn(creature_t *creature)
{
    creature_runtime_id_t runtime_id = creature->runtime_id;
    creature_table_erase(&_table, runtime_id);
    fixed_pool_free(&_pool, creature);
    LOG("Despawned creature %u.", runtime_id);
}

creature_t *
creature_find(creature_runtime_id_t runtime_id)
{
    creature_index_t *index = creature_table_find(&_table, runtime_id);
    if (!index)
        return 0;
    return &_pool.all[*index];
}

creature_index_t
creature_get_index(creature_t *creature)
    {return fixed_pool_index(&_pool, creature);}

creature_t *
creature_get(creature_index_t index)
    {return &_pool.all[index];}

void
creature_take_damage(creature_t *creature, uint32 damage, uint32 health_current)
{
    creature->health_current = health_current;
}
