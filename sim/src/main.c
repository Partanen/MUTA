#include <signal.h>
#include "common.h"
#include "player.h"
#include "dynamic_object.h"
#include "creature.h"
#include "geometry.h"
#include "part.h"
#include "master.h"
#include "pathfind.h"
#include "instance.h"
#include "scripts.h"
#include "../../shared/get_opt.h"
#include "../../shared/types.h"
#include "../../shared/common_utils.h"
#include "../../shared/sv_time.h"
#include "../../shared/entities.h"
#include "../../shared/tile.h"
#include "../../shared/crypt.h"
#include "../../shared/abilities.h"

#define MAX_EVENTS 64

static bool32               _daemonize;
static get_opt_context_t    _opts       = GET_OPT_CONTEXT_INITIALIZER;
static const char           *_help_str  =
    "MUTA Simulation Server\n"
    "Command line options:\n"
    "-h: Print this help dialogue.\n"
    "-d: Run in daemon mode (Linux only).\n"
    "-c: config file path.\n"
    "-l: log directory path.";
static const char   *_log_dir_path  = ".";
static const char   *_cfg_path      = "config.cfg";
int32               _running        = 1; /* Atomic */
com_event_t         _events[MAX_EVENTS];

static void
_sig_handler(int sig);

int main(int argc, char **argv)
{
    int opt;
    while ((opt = get_opt(&_opts, argc, argv, "hdl:c:")) != -1)
    {
        switch (opt)
        {
        case 'd':
            _daemonize = 1;
            break;
        case 'h':
            puts(_help_str);
            return 0;
        case 'l':
            _log_dir_path = _opts.arg;
            break;
        case 'c':
            _cfg_path = _opts.arg;
            break;
        case '?':
            puts("Unknown command line options.");
            return 1;
        }
    }
    if (_daemonize && muta_daemonize())
    {
        puts("Running in daemon mode failed.");
        return 1;
    }
    int err;
    init_time();
    if (init_socket_api())
        {err = 2; goto fail;}
    if (crypt_init())
        {err = 3; goto fail;}
    signal(SIGINT, _sig_handler);
    com_init(_cfg_path, _log_dir_path);
    LOG("MUTA Sim Server started");
    if ((err = ent_load(ENT_LOAD_ALL)))
    {
        LOG("Failed to load entity definitions (error %d).", err);
        goto save_log;
    }
    if ((err = tile_load(0)))
    {
        LOG("Failed to load tile definitions (error %d).", err);
        goto save_log;
    }
    geo_init();
    pl_init();
    dobj_init();
    creature_init();
    if (ab_load())
        muta_panic_print("Failed to load abilities.");
    scripts_init();
    pf_init();
    inst_init();
    part_init();
    master_init();
    master_connect();
    uint64 last_tick            = get_program_ticks_ms();
    double target_delta_seconds = 1.0f / 60.f;
    double accumulated_delta    = 0;
    while (interlocked_compare_exchange_int32(&_running, -1, -1))
    {
        uint64  current_tick    = get_program_ticks_ms();
        double  delta_seconds   = (double)(current_tick - last_tick) / 1000.0;
        int     max_sleep;
        if (delta_seconds < target_delta_seconds)
            max_sleep = (int)((target_delta_seconds - delta_seconds) * 1000.0);
        else
            max_sleep = 0;
        int num = com_wait_events(_events, MAX_EVENTS, (int)max_sleep);
        for (int i = 0; i < num; ++i)
        {
            switch (_events[i].type)
            {
            case COM_EVENT_SVCHAN_CLIENT:
                svchan_client_handle_event(&_events[i].svchan_client);
                break;
            }
        }
        master_flush();
        accumulated_delta += delta_seconds;
        while (accumulated_delta > target_delta_seconds)
        {
            part_update((float)target_delta_seconds);
            /* Players must be updated after parts.  */
            pl_update((float)target_delta_seconds);
            accumulated_delta -= target_delta_seconds;
            last_tick = current_tick;
        }
    }
    master_destroy();
    part_destroy();
    inst_destroy();
    dobj_destroy();
    pl_destroy();
    geo_destroy();
    pf_destroy();
    scripts_destroy();
    creature_destroy();
    save_log:
        com_destroy();
        return 0;
    fail:
        printf("Error initializing, code %d\n", err);
        return 1;
}

static void
_sig_handler(int sig)
    {interlocked_decrement_int32(&_running);}
