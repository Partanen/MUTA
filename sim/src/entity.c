#include "entity.h"
#include "part.h"
#include "../../shared/tile.h"
#include "../../shared/common_utils.h"

static bool32
_check_diagonal_blockers(part_t *part, int new_x, int new_y, int new_z,
    int ax, int ay);
/* Returns non-zero if path from a tile to given tile is diagonally blocked. */

#define DO_RET \
    while (1) { \
        DEBUG_PRINTFF("Failed on line %d\n", __LINE__); \
        return 0; \
    }

static bool32
_move_towards_checked_ramp_up(part_t *part, tile_def_t *tile_forward,
    int direction, int ax, int ay, int new_position[3], int ret_position[3])
{
    tile_def_t *t;
    /* Moving diagonally */
    if (ax != 0 && ay != 0)
    {
        t = part_get_tile_def(part, new_position[0] - ax, new_position[1],
            new_position[2]);
        if (!t->passthrough && tile_check_ramp_up(direction, t->ramp))
            DO_RET;
        t = part_get_tile_def(part, new_position[0], new_position[1] - ay,
            new_position[2]);
        if (!t->passthrough && tile_check_ramp_up(direction, t->ramp))
            DO_RET;
    }
    new_position[2] += 1;
    /* Check the tile on top of the ramp (destination) */
    t = part_get_tile_def(part, new_position[0], new_position[1],
        new_position[2]);
    if (!t->passthrough)
        DO_RET;
    for (int i = 0; i < 3; ++i)
        ret_position[i] = new_position[i];
    return 1;
}

bool32
entity_find_ground_move_destination(part_t *part, int current_position[3],
    int direction, int ret_position[3])
{
    muta_assert(direction >= 0 && direction < NUM_ISODIRS);
    int ax, ay;
    iso_dir_to_vec2(direction, &ax, &ay);
    int new_position[3] = {
        current_position[0] + ax,
        current_position[1] + ay,
        current_position[2]};
    tile_def_t *tile_under = part_get_tile_def(part, current_position[0],
        current_position[1], current_position[2] - 1);
    tile_def_t *tile_forward = part_get_tile_def(part, new_position[0],
        new_position[1], new_position[2]);
    if (tile_under->ramp == TILE_RAMP_NONE)
    {
        if (!tile_forward->passthrough)
        {
            /* If not a ramp up from this direction, we cannot pass. */
            if (tile_check_ramp_up(direction, tile_forward->ramp))
                DO_RET;
            return _move_towards_checked_ramp_up(part, tile_forward, direction,
                ax, ay, new_position, ret_position);
        } else /* The destination tile is passthrough */
        {
            tile_def_t *tile_under_forward = part_get_tile_def(part,
                new_position[0], new_position[1], new_position[2] - 1);
            if (tile_under_forward->passthrough)
                DO_RET;
            if (ax != 0 && ay != 0)
                if (_check_diagonal_blockers(part, new_position[0],
                    new_position[1], new_position[2], ax, ay))
                    DO_RET;
        }
    } else /* Standing on a ramp */
    {
        if (!tile_forward->passthrough)
        {
            /* Maybe move up a stair normally? */
            if (tile_check_ramp_up(direction, tile_forward->ramp))
                DO_RET;
            return _move_towards_checked_ramp_up(part, tile_forward, direction,
                ax, ay, new_position, ret_position);
        }
        /* Let's try moving down the ramp. */
        if (!tile_check_ramp_down(direction, tile_under->ramp))
        {
            new_position[2] -= 1;
            tile_def_t *tile_under_forward = part_get_tile_def(part,
                new_position[0], new_position[1], new_position[2] - 1);
            if (tile_under_forward->passthrough)
                DO_RET;
        } else /* Just move normally */
        {
            tile_def_t *tile_under_forward = part_get_tile_def(part,
                new_position[0], new_position[1], new_position[2] - 1);
            if (tile_under_forward->passthrough)
                return 0;
            if (ax != 0 && ay != 0)
                if (_check_diagonal_blockers(part, new_position[0],
                    new_position[1], new_position[2], ax, ay))
                    DO_RET;
        }
    }
    for (int i = 0; i < 3; ++i)
        ret_position[i] = new_position[i];
    return 1;
}

bool32
entity_can_move_to_position(part_t *part, tile_t tile_under_id,
    tile_t tile_forward_id, int direction)
{
#if 0
    int ax, ay;
    iso_dir_to_vec2(direction, &ax, &ay);
    int new_position[3] = {
        e->position[0] + ax,
        e->position[1] + ay,
        e->position[2]};
    tile_def_t *tile_under      = tile_get_def(tile_under_id);
    tile_def_t *tile_forward    = tile_get_def(tile_forward_id);
    if (tile_under->ramp == TILE_RAMP_NONE)
    {
        if (!tile_forward->passthrough)
        {
            /* If not a ramp up from this direction, we cannot pass. */
            if (tile_check_ramp_up(direction, tile_forward->ramp))
                DO_RET;
            return _move_towards_checked_ramp_up(part, tile_forward, direction,
                ax, ay, new_position, ret_position);
        } else /* The destination tile is passthrough */
        {
            tile_def_t *tile_under_forward = part_get_tile_def(part,
                new_position[0], new_position[1], new_position[2] - 1);
            if (tile_under_forward->passthrough)
                DO_RET;
            if (ax != 0 && ay != 0)
                if (_check_diagonal_blockers(part, new_position[0],
                    new_position[1], new_position[2], ax, ay))
                    DO_RET;
        }
    } else /* Standing on a ramp */
    {
        if (!tile_forward->passthrough)
        {
            /* Maybe move up a stair normally? */
            if (tile_check_ramp_up(direction, tile_forward->ramp))
                DO_RET;
            return _move_towards_checked_ramp_up(part, tile_forward, direction,
                ax, ay, new_position, ret_position);
        }
        /* Let's try moving down the ramp. */
        if (!tile_check_ramp_down(direction, tile_under->ramp))
        {
            new_position[2] -= 1;
            tile_def_t *tile_under_forward = part_get_tile_def(part,
                new_position[0], new_position[1], new_position[2] - 1);
            if (tile_under_forward->passthrough)
                DO_RET;
        } else /* Just move normally */
        {
            tile_def_t *tile_under_forward = part_get_tile_def(part,
                new_position[0], new_position[1], new_position[2] - 1);
            if (tile_under_forward->passthrough)
                return 0;
            if (ax != 0 && ay != 0)
                if (_check_diagonal_blockers(part, new_position[0],
                    new_position[1], new_position[2], ax, ay))
                    DO_RET;
        }
    }
#endif
    return 0;
}

static bool32
_check_diagonal_blockers(part_t *part, int new_x, int new_y, int new_z,
    int ax, int ay)
{
    tile_def_t *t;
    t = part_get_tile_def(part, new_x - ax, new_y, new_z);
    if (!t->passthrough)
        return 1;
    t = part_get_tile_def(part, new_x, new_y - ay, new_z);
    if (!t->passthrough)
        return 1;
    return 0;
}
