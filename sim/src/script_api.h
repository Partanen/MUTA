/* script_api.h
 * Sim server scripting API. Only these functions should be used by scripts to
 * access internal engine data in side scripts. */

#ifndef MUTA_SIM_SCRIPT_API_H
#define MUTA_SIM_SCRIPT_API_H

#include <stdint.h>
#include "../../shared/compiler.h"

/* Forward declaration(s). */
typedef struct ability_def_t ability_def_t;

/* Types defined here. */
typedef struct script_entity_t  script_entity_t;
typedef struct ability_script_t ability_script_t;
typedef int32_t                 bool32;

#define SCRIPT_LOG_INFO(fmt, ...) \
    _script_log_info("[SCRIPT INFO] " fmt "\n", ##__VA_ARGS__)

#define SCRIPT_LOG_DEBUG(fmt, ...) \
    _script_log_debug("[SCRIPT DEBUG] " fmt "\n", ##__VA_ARGS__)

#define SCRIPT_LOG_ERROR(fmt, ...) \
    _script_log_error("[SCRIPT ERROR] " fmt "\n", ##__VA_ARGS__)

enum script_entity_type
{
    SCRIPT_ENTITY_TYPE_PLAYER,
    SCRIPT_ENTITY_TYPE_CREATURE,
    SCRIPT_ENTITY_TYPE_DYNAMIC_OBJECT
};

enum ability_script_use_result
{
    ABILITY_SCRIPT_USE_SUCCESS = 0,
    ABILITY_SCRIPT_USE_FAIL
};

struct script_entity_t
{
    enum script_entity_type type;
    void                    *data;
};

struct ability_script_t
{
    uint32_t id;

    enum ability_script_use_result (*use)(ability_script_t const * script,
        script_entity_t *user, script_entity_t *target);
    /* Required */

    bool32 (*on_charge_finished)(script_entity_t *entity,
        script_entity_t *target);
    /* Required if compute_charge_time is provided.
     * Should return non-zero if charge was successful.
     * target is null if the target  was lost during charging. */

    uint32_t (*compute_charge_time)(script_entity_t *user,
        script_entity_t *target);
    /* Optional.
     * If null, ability is considered instant. */

    void (*on_user_moved)(script_entity_t *user, script_entity_t *target);
    /* Optional.
     * Only relevant if compute_charge_time() is not null. */

    void (*on_user_stopped_charge)(script_entity_t *user);
    /* Optional. */

    /* The following data is autofilled during initialization. Initialize to 0.
     * */
    ability_def_t *def; /* Direct pointer to a hashtable item. */
};

bool32
script_is_entity_moving(script_entity_t *entity);

void
script_stop_charge(script_entity_t *user);

void
script_get_entity_position(script_entity_t *entity, int ret_position[3]);

enum iso_dir
script_get_entity_direction(script_entity_t *entity);

void
script_compute_distance_vec(int position_a[3], int position_b[3],
    int ret_distance[3]);

void
ability_script_request_deal_damage(ability_script_t *script,
    script_entity_t *entity, script_entity_t *target, uint32_t damage);
/* This sends a request to the master server and thus is not instantaneous. */

void
_script_log_info(const char *fmt, ...)
CHECK_PRINTF_ARGS(1, 2);

void
_script_log_debug(const char *fmt, ...)
CHECK_PRINTF_ARGS(1, 2);

void
_script_log_error(const char *fmt, ...)
CHECK_PRINTF_ARGS(1, 2);

#endif /* MUTA_SIM_SCRIPT_API_H */
