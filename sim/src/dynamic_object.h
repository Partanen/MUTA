#ifndef MUTA_SIM_DYNAMIC_OBJECT_H
#define MUTA_SIM_DYNAMIC_OBJECT_H

#include "entity.h"

/* Forward declaration(s) */
typedef struct player_t             player_t;
typedef struct dynamic_object_def_t dynamic_object_def_t;
typedef struct geo_t                geo_t;

/* Types defined here */
typedef struct dynamic_object_t dynamic_object_t;
typedef uint32                  dobj_index_t;

enum dobj_flag
{
    DOBJ_FLAG_CONTROLLED_BY_ME  = (1 << 0),
    DOBJ_FLAG_PENDING_SPAWN     = (1 << 1)
};

struct dynamic_object_t
{
    dobj_runtime_id_t   runtime_id;
    dobj_type_id_t      type_id;
    entity_t            entity;
    uint8               flags;
};

void
dobj_init(void);

void
dobj_destroy(void);

int
dobj_spawn(dobj_runtime_id_t runtime_id,
    dobj_type_id_t type_id, part_t *part,
    int direction, int x, int y, int z, bool32 controlled_by_me,
    bool32 pending_spawn);
/* Assumes a dynamic object with the same runtime id does not already exist.
 * Returns non-zero if parameters sent by server are invalid, but will return 0
 * if spawning fails due to too many objects existing or other similar reason
 * that isn't a bug. */

void
dobj_despawn(dynamic_object_t *obj);

dynamic_object_t *
dobj_find(dobj_runtime_id_t runtime_id);

dobj_index_t
dobj_get_index(dynamic_object_t *obj);

dynamic_object_t *
dobj_get(dobj_index_t index);

void
dobj_complete_pending_spawn(dynamic_object_t *obj);

bool32
dobj_collides_with_tiles(dynamic_object_def_t *def, geo_t *geo, int x, int y,
    int z);

#endif /* MUTA_SIM_DYNAMIC_OBJECT_H */
