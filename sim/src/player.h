#ifndef MUTA_SIM_PLAYER_H
#define MUTA_SIM_PLAYER_H

#include "entity.h"
#include "defs.h"
#include "../../shared/common_defs.h"

/* Forward declaration(s) */
typedef struct player_race_def_t    player_race_def_t;
typedef struct part_t               part_t;
typedef struct dynamic_object_t     dynamic_object_t;

/* Types defined here */
typedef uint32              pl_index_t;
typedef struct pl_ability_t pl_ability_t;
typedef struct player_t     player_t;

enum pl_flag
{
    PL_CONTROLLED_BY_ME     = (1 << 0), /* Controlled by this sim? */
    PL_FINDING_PATH         = (1 << 1), /* Waiting for result from pathfind mod. */
    PL_DESPAWNING           = (1 << 2),
    PL_PATHFIND_CANCELLED   = (1 << 3)
};

struct pl_ability_t
{
    ability_id_t id;
};

struct player_t
{
    player_runtime_id_t world_session_id;
    player_db_guid_t    db_id;
    entity_t            entity;
    uint32              next_time_can_walk;
    uint32              move_timer_index; /* 0xFFFFFFFF if none */
    uint32              charge_timer_index; /* 0xFFFFFFFF if none */
    player_race_def_t   *race;
    uint32              health_current;
    uint32              health_max;
    struct
    {
        uint8   directions[MAX_PATHFIND_PATH];
        uint32  num_directions;
        struct
        {
            int32 x;
            int32 y;
            uint8 z;
            bool8 do_move;
        } after_cancel;
        /* after_cancel is used to mark the destination of the next path to find
         * if the current pathfinding process is cancelled (do_move will be
         * true). Only relevant if PL_PATHFIND_CANCELLED is on. */
    } pathfind;
    struct
    {
        pl_ability_t    data[MAX_PLAYER_ABILITIES];
        uint32          num;
    } abilities;
    char    name[MAX_CHARACTER_NAME_LEN + 1];
    char    display_name[MAX_DISPLAY_NAME_LEN + 1];
    uint8   flags;
    uint8   sex;
    uint8   move_speed;
};

void
pl_init(void);

void
pl_destroy(void);

int
pl_spawn(player_runtime_id_t world_session_id, player_db_guid_t db_id,
    const char *name, const char *display_name, int sex, int race, part_t *part,
    int direction, int x, int y, int z, uint8 move_speed,
    bool32 controlled_by_me, uint32 abilities[MAX_PLAYER_ABILITIES],
    uint32 num_abilities, uint32 health_current, uint32 health_max);
/* Returs non-zero if request is illegal. Otherwise, will by itself send
 * confirm or spawn fail messages to master (if this sim is the controller). */

void
pl_despawn(player_t *pl);

player_t *
pl_find(player_runtime_id_t world_session_id);

player_t *
pl_get(pl_index_t index);

pl_index_t
pl_get_index(player_t *pl);

void
pl_find_path(player_t *pl, int x, int y, int z);

void
pl_on_pathfind_success(player_t *pl, uint8 directions[MAX_PATHFIND_PATH],
    uint32 num_directions);

void
pl_on_pathfind_fail(player_t *pl);

void
pl_move_in_direction(player_t *pl, int direction);

float
pl_get_move_time_left(player_t *pl);

void
pl_update(float dt_s);

int
pl_use_ability(player_t *pl, ability_id_t ability_id,
    enum common_entity_type target_type, uint32 target_id);

int
pl_on_requested_stop_ability_charge(player_t *pl);

bool32
pl_is_charging_ability(player_t *pl);

void
pl_stop_charging_ability(player_t *pl);
/* Called from scripting API. Only stops the charge timer, does not send the
 * player any messages about having stopped. */

bool32
pl_is_moving(player_t *pl);

void
pl_take_damage(player_t *pl, uint32 damage, uint32 health_current);
/* Called when the master server sends a damage message. */

#endif /*MUTA_SIM_PLAYER_H */
