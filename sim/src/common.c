#include <inttypes.h>
#include "common.h"
#include "../../shared/common_utils.h"
#include "../../shared/sv_common_defs.h"
#include "../../shared/kthp.h"

static log_category_t _log_cats[] =
{
    {LOG_CATEGORY_INFO, "INFO"},
    {LOG_CATEGORY_DEBUG, "DEBUG"},
    {LOG_CATEGORY_WARNING, "WARN"},
    {LOG_CATEGORY_ERROR, "ERROR"}
};

static event_buf_t  _event_buf;
static kth_pool_t   _threadpool;

log_t           com_log;
com_config_t    com_config;
dynamic_stack_t com_stack;

static void
_on_cfg_opt(void *ctx, const char *opt, const char *val);

void
com_init(const char *cfg_path, const char *log_dir_path)
{
    int err = 0;
    int r;
    if (log_api_init(64))
        {err = 1; goto fail;}
    if ((r = log_init(&com_log, log_dir_path, "sim", _log_cats,
        NUM_LOG_CATEGORIES)))
    {
        printf("log_init() failed with code %d\n", r);
        err = 2;
        goto fail;
    }
    dynamic_stack_init(&com_stack, 0xFFFF);
    com_config.max_instance_parts   = 32;
    com_config.max_players          = 4096;
    com_config.max_dynamic_objects  = 4096;
    com_config.max_creatures        = 4096;
    uint8 master_address[4] = {127, 0, 0, 1};
    memcpy(com_config.master_address, master_address, sizeof(master_address));
    com_config.master_port = DEFAULT_SIM_PORT;
    com_config.master_username  = dstr_create("admin");
    com_config.master_password  = dstr_create("password");
    com_config.pathfind_grid_w  = 64;
    com_config.pathfind_grid_t  = 16;
    com_config.max_extra_threads      = 2;
    if (parse_cfg_file(cfg_path, _on_cfg_opt, 0))
    {
        LOG_ERROR("Failed to parse config file from path '%s'.\n",
            cfg_path);
        err = 3;
        goto fail;
    }
    event_init(&_event_buf, sizeof(com_event_t), 512);
    if (kth_pool_init(&_threadpool, (int)com_config.max_extra_threads, 256))
        {err = 4; goto fail;}
    if (kth_pool_run(&_threadpool))
        {err = 5; goto fail;}
    return;
    fail:
        muta_panic_print("%s failed with error %d.", __func__, err);
}

void
com_destroy(void)
{
    dynamic_stack_destroy(&com_stack);
    kth_pool_destroy(&_threadpool);
    event_destroy(&_event_buf);
    log_destroy(&com_log);
    log_api_destroy();
}

void
com_push_events(com_event_t *events, int32 num)
    {event_push(&_event_buf, events, num);}

void
com_push_events_no_wait(com_event_t *events, int32 num)
    {event_push_no_wait(&_event_buf, events, num);}

int
com_wait_events(com_event_t *ret_events, int32 max, int timeout)
    {return event_wait(&_event_buf, ret_events, max, timeout);}

void
com_async_call(void (*callback)(void *user_data), void *args)
{
    if (kth_pool_add_job(&_threadpool, callback, args))
        muta_panic_print("%s failed.", __func__);
}

static void
_on_cfg_opt(void *ctx, const char *opt, const char *val)
{
    if (streq(opt, "max_instance_parts"))
    {
        com_config.max_instance_parts = str_to_uint32(val);
        if (!com_config.max_instance_parts)
            muta_panic_print("Config option '%s' must be > 0.", opt);
    } else
    if (streq(opt, "max_player"))
    {
        uint32 v = str_to_uint32(val);
        if (!v)
            muta_panic_print("Config option '%s' must be > 0.", opt);
        com_config.max_players = v;
    } else
    if (streq(opt, "max_dynamic_objects"))
    {
        uint32 v = str_to_uint32(val);
        if (!v)
            muta_panic_print("Config option '%s' must be > 0.", opt);
        com_config.max_dynamic_objects = v;
    } else
    if (streq(opt, "max_creatures"))
    {
        uint32 v = str_to_uint32(val);
        if (!v)
            muta_panic_print("Config option '%s' must be > 0.", opt);
        com_config.max_creatures = v;
    } else
    if (streq(opt, "master_address")) {
        uint v[4];
        if (sscanf(val, "%u.%u.%u.%u", &v[0], &v[1], &v[2], &v[3]))
            muta_panic_print("Config option '%s' is ill-formed.", opt);
        for (int i = 0; i < 4; ++i)
        {
            if (com_config.master_address[i] > UINT8_MAX)
                muta_panic_print("Config option '%s' numbers must be <= %d.",
                    val, (int)UINT8_MAX);
            com_config.master_address[i] = v[i];
        }
    } else
    if (streq(opt, "master_port")) {
        uint32 v = str_to_uint32(val);
        if (v > UINT16_MAX)
            muta_panic_print("Config option '%s' must be <= %d.\n", opt,
                (int)UINT16_MAX);
        com_config.master_port = (uint16)v;
    } else
    if (streq(val, "master_username"))
    {
        dstr_set(&com_config.master_username, val);
        if (!dstr_len(com_config.master_username))
            muta_panic_print("Config option '%s' length must be > 0.", opt);
    } else
    if (streq(val, "master_password"))
    {
        dstr_set(&com_config.master_password, val);
        if (!dstr_len(com_config.master_password))
            muta_panic_print("Config option '%s' length must be > 0.", opt);
    } else
    if (streq(val, "pathfind_grid_w"))
    {
        uint32 v = str_to_uint32(val);
        if (!v)
            muta_panic_print("Config option '%s' must be > 0.", opt);
        if (v % 2)
            muta_panic_print("Config option '%s' must be divisible by 2.", opt);
        com_config.pathfind_grid_w = v;
    } else
    if (streq(val, "pathfind_grid_t"))
    {
        uint32 v = str_to_uint32(val);
        if (!v)
            muta_panic_print("Config option '%s' must be > 0.", opt);
        if (v % 2)
            muta_panic_print("Config option '%s' must be divisible by 2.", opt);
        com_config.pathfind_grid_t = v;
    } else
    if (streq(val, "max_extra_threads"))
    {
        uint32 v = str_to_uint32(val);
        if (!v)
            muta_panic_print("Config option '%s' must be > 0.", opt);
        com_config.max_extra_threads = v;
    } else
        muta_panic_print("Unknown config option '%s'.", opt);
}
