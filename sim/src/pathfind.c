#include "pathfind.h"
#include "part.h"
#include "common.h"
#include "entity.h"
#include "../../shared/common_utils.h"
#include "../../shared/common_defs.h"

#define NO_PARENT   (-1)
#define NODE_OPEN   (1 << 0)

typedef struct node_t   node_t;
typedef struct grid_t   grid_t;
typedef struct args_t   args_t;
typedef uint16          score_t;

struct node_t
{
    score_t g;
    score_t h;
    int32   parent;
    uint8   flags;
};
/* A single a star node */

struct grid_t
{
    node_t          *nodes;
    pqueue(int32)   open; /* Contains indices to nodes in the grid. */
};
/* A tile node grid representing a part of the world. */

struct args_t
{
    pf_user_data_t  user_data;
    int             start[3];
    int             end[3];
    part_t          *part;
};

static struct
{
    grid_t  *all;
    uint32  num;
} _grids;

static int                      _grid_w;
static int                      _grid_t;
static obj_pool_t               _args;
static event_buf_t              _grid_queue;

static void
_job(void *args); /* Thread callback. */

static inline grid_t *
_find_free_grid(void);

static inline int32
_grid_pos_to_index(int grid_dims[3], int x, int y, int z);
/* Convert position on a grid to an index on the grid (opposite of
 * _grid_index_to_pos). */

static inline void
_grid_index_to_pos(int grid_dim[3], int32 index,
    int32 *restrict ret_x, int32 *restrict ret_y, int32 *restrict ret_z);
/* Convert an index in the grid to a position in the grid (opposite of
 * _grid_pos_to_index()). */

static inline node_t *
_get_node(grid_t *grid, int grid_dim[3], int x, int y, int z);
/* Returns 0 if outside of dimensions. */

static inline score_t
_compute_manhattan(int start_x, int start_y, int end_x, int end_y);

static uint32
_build_path(grid_t *grid, int grid_dim[3], int32 first_index, int32 node_index,
    uint8 ret_directions[MAX_PATHFIND_PATH]);
/* Returns the number of directions written. */

static inline int
_compare_nodes(int32 a, int32 b, grid_t *grid);

void
pf_init(void)
{
    _grid_w     = (int)com_config.pathfind_grid_w;
    _grid_t     = (int)com_config.pathfind_grid_t;
    _grids.num  = com_config.max_extra_threads;
    _grids.all  = emalloc(_grids.num * sizeof(grid_t));
    for (uint32 i = 0; i < com_config.max_extra_threads; ++i)
    {
        grid_t *grid = &_grids.all[i];
        grid->nodes = emalloc(_grid_w * _grid_w * _grid_t * sizeof(node_t));
        pqueue_init(&grid->open, 64);
    }
    obj_pool_init(&_args, 128, sizeof(args_t));
    event_init(&_grid_queue, sizeof(uint32), _grids.num);
    for (uint32 i = 0; i < _grids.num; ++i)
        event_push_no_wait(&_grid_queue, &i, 1);
}

void
pf_destroy(void)
{
    obj_pool_destroy(&_args);
    event_destroy(&_grid_queue);
}

int
pf_find(pf_user_data_t user_data, part_t *part, int start_x,
    int start_y, int start_z, int end_x, int end_y, int end_z)
{
    /* Check if start and end positions are the same. */
    if (start_x == end_x && start_y == end_y && start_z == end_z)
    {
        LOG_EXT("Ignoring request - start and end are the same.");
        return 1;
    }
    /* Check if outside of max grid bounds. */
    if (abs(end_x - start_x) > _grid_w / 2 ||
        abs(end_y - start_y) > _grid_w / 2 ||
        abs(end_z - start_z) > _grid_t / 2)
    {
        LOG_EXT("Ignoring request - attempting to find path outside of grid "
            "bounds.");
        return 2;
    }
    /* Args will be freed later using pf_free_args() from another module. */
    args_t *args    = obj_pool_reserve(&_args);
    args->user_data = user_data;
    args->start[0]  = start_x;
    args->start[1]  = start_y;
    args->start[2]  = start_z;
    args->end[0]    = end_x;
    args->end[1]    = end_y;
    args->end[2]    = end_z;
    args->part      = part;
    com_async_call(_job, args);
    part->num_unfinished_jobs++;
    LOG("Pathfinding from %d.%d.%d to %d.%d.%d.", start_x, start_y, start_z,
        end_x, end_y, end_z);
    return 0;
}

pf_user_data_t
pf_get_user_data(void *args)
    {return ((args_t*)args)->user_data;}

void
pf_free_args(void *args)
    {obj_pool_free(&_args, args);}

static void
_job(void *args_ptr)
{
    args_t *args = args_ptr;
    grid_t *grid = _find_free_grid();
    /* Start and end positions */
    int s[3] = {args->start[0], args->start[1], args->start[2]};
    int e[3] = {args->end[0], args->end[1], args->end[2]};
    /* Calculate our grid's position: position - (grid_size / 2) */
    int grid_pos[3] = {
        s[0] - _grid_w / 2,
        s[1] - _grid_w / 2,
        s[2] - _grid_t / 2};
    /* Grid dimensions, clamped to map. */
    int grid_dim[3] = {_grid_w, _grid_w, _grid_t};
    for (int i = 0; i < 3; ++i)
        if (grid_pos[i] < 0)
        {
            grid_dim[i] += grid_pos[i];
            grid_pos[i] = 0;
        }
    /* End on grid for computing the manhattan distance in 2 dimensions. */
    int end_on_grid[2];
    for (int i = 0; i < 2; ++i)
        end_on_grid[i] = e[i] - grid_pos[i];
    /* End index on grid for testing if destination was reached. */
    int32 end_index_on_grid = _grid_pos_to_index(grid_dim, e[0] - grid_pos[0],
        e[1] - grid_pos[1], e[2] - grid_pos[2]);
    /* Initialize grid */
    memset(grid->nodes, 0, grid_dim[0] * grid_dim[1] * grid_dim[2] *
        sizeof(node_t));
    pqueue_clear(&grid->open);
    /* Push the first node into the open list */
    int32 first_index = _grid_pos_to_index(grid_dim, s[0] - grid_pos[0],
        s[1] - grid_pos[1], s[2] - grid_pos[2]);
    grid->nodes[first_index].parent = NO_PARENT;
    grid->nodes[first_index].flags |= NODE_OPEN;
    pqueue_push_ext(&grid->open, first_index, _compare_nodes, grid);
    /* Event to post to part when we're done. */
    part_event_t event;
    for (;;)
    {
        /* Failed to find path. */
        if (!pqueue_num(&grid->open))
        {
            /* Post fail event to part. */
            event.type                  = PART_EVENT_PATHFIND_FAIL;
            event.pathfind_fail.args    = args;
            goto out;
        }
        /* Pop node with lowest f */
        int32 current_index = pqueue_pop(&grid->open);
        muta_assert(current_index >= 0);
        muta_assert(current_index < grid_dim[0] * grid_dim[1] * grid_dim[2]);
        int32 pos[3];
        _grid_index_to_pos(grid_dim, current_index, &pos[0], &pos[1], &pos[2]);
        /* Check if destination was reached */
        if (current_index == end_index_on_grid)
        {

            {
                int32 a1[3], a2[3];
                _grid_index_to_pos(grid_dim, first_index, &a1[0], &a1[1], &a1[2]);
                _grid_index_to_pos(grid_dim, current_index, &a2[0], &a2[1], &a2[2]);
                for (int i = 0; i < 3; ++i)
                {
                    a1[i] += grid_pos[i];
                    a2[i] += grid_pos[i];
                }
                DEBUG_PRINTFF("BUILDING PATH FROM %d.%d.%d to %d.%d.%d!\n",
                    a1[0], a1[1], a1[2],
                    a2[0], a2[1], a2[2]);
            }


            event.type                  = PART_EVENT_PATHFIND_SUCCESS;
            event.pathfind_success.args = args;
            event.pathfind_success.num_directions = _build_path(grid, grid_dim,
                first_index, current_index, event.pathfind_success.directions);
            goto out;
        }
        node_t *current_node = &grid->nodes[current_index];
        current_node->flags &= ~NODE_OPEN;
        /* Iterate through surrounding nodes */
        for (int i = pos[0] - 1; i < pos[0] + 2; ++i)
            for (int j = pos[1] - 1; j < pos[1] + 2; ++j)
            {
                if (i == pos[0] && j == pos[1]) /* Ignore current */
                    continue;
                /* Check if outside of grid bounds. */
                if (i < 0 || j < 0 || i >= grid_dim[0] || j >= grid_dim[1])
                    continue;
                /* Check if the tile can be entered from the current tile. */
                int direction = vec2_to_iso_dir(i - pos[0], j - pos[1]);
                /* Translate to world pos. */
                int current_pos[3];
                for (int i = 0; i < 3; ++i)
                    current_pos[i] = pos[i] + grid_pos[i];
                int next_pos[3];
                if (!entity_find_ground_move_destination(args->part,
                    current_pos, direction, next_pos))
                    continue;
                /* Translate position to grid. */
                for (int i = 0; i < 3; ++i)
                    next_pos[i] -= grid_pos[i];
                /* Check if path is too long */
                score_t next_g = current_node->g + 1;
                if (next_g >= MAX_PATHFIND_PATH)
                    continue;
                node_t *next_node = _get_node(grid, grid_dim, next_pos[0],
                    next_pos[1], next_pos[2]);
                if (next_g < next_node->g || !next_node->g || !current_node->g)
                {
                    next_node->parent   = current_index;
                    next_node->g        = next_g;
                    next_node->h        = _compute_manhattan(i, j,
                        end_on_grid[0], end_on_grid[1]);
                    int32 next_index = (int32)(next_node - grid->nodes);
                    if (next_node->flags & NODE_OPEN)
                        pqueue_erase(&grid->open, next_index);
                    pqueue_push_ext(&grid->open, next_index, _compare_nodes,
                        grid);
                    next_node->flags |= NODE_OPEN;
                }
            }
    }
    out:;
        /* Free grid for use by other threads */
        uint32 grid_index = (uint32)(grid - _grids.all);
        event_push(&_grid_queue, &grid_index, 1);
        /* Post success or failure event to part. */
        part_push_events(args->part, &event, 1);
        DEBUG_PRINTFF("Pushed part event.\n");
}

static inline grid_t *
_find_free_grid(void)
{
    uint32 index;
    event_wait(&_grid_queue, &index, 1, -1);
    muta_assert(index < _grids.num);
    return &_grids.all[index];
}

static inline int32
_grid_pos_to_index(int grid_dim[3], int x, int y, int z)
{
    int32 ret = z * grid_dim[0] * grid_dim[1] + y * grid_dim[0] + x;
    muta_assert(ret >= 0);
    muta_assert(ret< grid_dim[0] * grid_dim[1] * grid_dim[2]);
    return ret;
}
    //{return x * grid_dim[0] * grid_dim[1] + y * grid_dim[2] + z;}

static inline void
_grid_index_to_pos(int grid_dim[3], int32 index,
    int32 *restrict ret_x, int32 *restrict ret_y, int32 *restrict ret_z)
{
    muta_assert(index < grid_dim[0] * grid_dim[1] * grid_dim[2]);
    int32 lsz = grid_dim[0] * grid_dim[1];
    *ret_z      = index / lsz;
    *ret_y      = (*ret_z) > 0 ? (index % ((*ret_z) * lsz)) / grid_dim[0] : 0;
    *ret_x      = index % grid_dim[0];
    muta_assert(*ret_x >= 0 && *ret_x < grid_dim[0]);
    muta_assert(*ret_y >= 0 && *ret_y < grid_dim[1]);
    muta_assert(*ret_z >= 0 && *ret_z < grid_dim[2]);
}

static inline node_t *
_get_node(grid_t *grid, int grid_dim[3], int x, int y, int z)
{
    muta_assert(x >= 0);
    muta_assert(y >= 0);
    muta_assert(z >= 0);
    muta_assert(x < grid_dim[0]);
    muta_assert(y < grid_dim[1]);
    muta_assert(z < grid_dim[2]);
    return &grid->nodes[_grid_pos_to_index(grid_dim, x, y, z)];
}

static inline score_t
_compute_manhattan(int start_x, int start_y, int end_x, int end_y)
{
    int     dx  = abs(end_x - start_x);
    int     dy  = abs(end_y - start_y);
    score_t ret = dx + dy;
    if (start_x != end_x && start_y != end_y)
        ret -= 1;
    return ret;
}

static uint32
_build_path(grid_t *grid, int grid_dim[3], int32 first_index, int32 last_index,
    uint8 ret_directions[MAX_PATHFIND_PATH])
{
    int32   current_index   = last_index;
    uint32  num_directions  = 0;
    for (;;)
    {
        node_t  *current        = &grid->nodes[current_index];
        int32   parent_index    = current->parent;
        int     current_pos[3];
        int     parent_pos[3];
        _grid_index_to_pos(grid_dim, current_index, &current_pos[0],
            &current_pos[1], &current_pos[2]);
        _grid_index_to_pos(grid_dim, parent_index, &parent_pos[0],
            &parent_pos[1], &parent_pos[2]);
        muta_assert(abs(parent_pos[0] - current_pos[0]) <= 1);
        muta_assert(abs(parent_pos[1] - current_pos[1]) <= 1);
        int direction = vec2_to_iso_dir(current_pos[0] - parent_pos[0],
            current_pos[1] - parent_pos[1]);
        ret_directions[num_directions++] = (uint8)direction;
        current_index = parent_index;
        if (current_index == first_index)
            break;
    }
    return num_directions;
}

static inline int
_compare_nodes(int32 a, int32 b, grid_t *grid)
{
    int32 a_total = grid->nodes[a].g + grid->nodes[a].h;
    int32 b_total = grid->nodes[b].g + grid->nodes[b].h;
    if (a_total == b_total)
        return 0;
    return a_total > b_total ? 1 : -1;
}
