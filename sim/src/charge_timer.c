#include "charge_timer.h"
#include "player.h"
#include "creature.h"
#include "dynamic_object.h"
#include "script_api.h"
#include "../../shared/common_utils.h"

void
charge_timer_array_init(charge_timer_array_t *array, uint32 max)
{
    array->num  = 0;
    array->max  = max;
    array->data = emalloc(max * sizeof(charge_timer_t));
}

void
charge_timer_array_destroy(charge_timer_array_t *array)
    {free(array->data);}

uint32
charge_timer_array_new(charge_timer_array_t *array, float max_ms,
    uint32 entity_index, ability_id_t ability_id, uint32 target_index,
    uint32 target_id, uint8 target_type)
{
    uint32 index = array->num++;
    muta_assert(index != array->max);
    charge_timer_t *timer = &array->data[index];
    timer->current_ms   = 0;
    timer->max_ms       = max_ms;
    timer->entity_index = entity_index;
    timer->ability_id   = ability_id;
    timer->target_index = target_index;
    timer->target_id    = target_id;
    timer->target_type  = target_type;
    return index;
}

void
charge_timer_array_free(charge_timer_array_t *array, uint32 index)
{
    muta_assert(array->num);
    array->data[index] = array->data[--array->num];
}

void
charge_timer_get_target(charge_timer_t *timer, script_entity_t *ret_target)
{
    switch (timer->target_type)
    {
    case ENTITY_TYPE_PLAYER:
        ret_target->type = SCRIPT_ENTITY_TYPE_PLAYER;
        ret_target->data = pl_get(timer->target_index);
        break;
    case ENTITY_TYPE_CREATURE:
        ret_target->type = SCRIPT_ENTITY_TYPE_CREATURE;
        ret_target->data = creature_get(timer->target_index);
        break;
    case ENTITY_TYPE_DYNAMIC_OBJECT:
        ret_target->type = SCRIPT_ENTITY_TYPE_DYNAMIC_OBJECT;
        ret_target->data = dobj_get(timer->target_index);
        break;
    default:
        muta_assert(0);
    }
    muta_assert(ret_target->data);
}
