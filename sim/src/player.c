#include <math.h>
#include "player.h"
#include "common.h"
#include "part.h"
#include "master.h"
#include "pathfind.h"
#include "dynamic_object.h"
#include "creature.h"
#include "script_api.h"
#include "scripts.h"
#include "charge_timer.h"
#include "../../shared/hashtable.h"
#include "../../shared/common_utils.h"
#include "../../shared/entities.h"
#include "../../shared/sim_packets.h"
#include "../../shared/abilities.h"

typedef struct move_timer_t             move_timer_t;
typedef struct finished_charge_timer_t  finished_charge_timer_t;
typedef struct player_table             player_table_t;

hashtable_define(player_table, player_runtime_id_t, pl_index_t);

struct move_timer_t
{
    pl_index_t  entity_index;
    float       time_left;
    bool8       pathfinding; /* If pathfinding, the next motion will be started
                                from the player's path list. */
};

struct finished_charge_timer_t
{
    ability_id_t    ability_id;
    uint32          user_index;
    uint32          target_id;
    uint32          target_index;
    uint8           target_type;
};

static fixed_pool(player_t) _pool;
static player_table_t       _table;

static struct
{
    pl_index_t  *indices;
    uint32      num;
} _despawned;

static struct
{
    move_timer_t    *all;
    uint32          num;
} _move_timers;

charge_timer_array_t _charge_timers;

static void
_free_move_timer(uint32 index);

static void
_update_move_timers(float delta_s);

static void
_update_charge_timers(float delta_s);

static void
_complete_despawns(void);

static void
_move_in_direction(player_t *pl, int direction);

static script_entity_t
_get_script_entity(player_t *pl);

static script_entity_t
_get_creature_script_entity(creature_t *cr);

void
pl_init(void)
{
    fixed_pool_init(&_pool, com_config.max_players);
    player_table_einit(&_table, com_config.max_players);
    _despawned.indices  = emalloc(com_config.max_players * sizeof(pl_index_t));
    _despawned.num      = 0;
    _move_timers.all = emalloc(com_config.max_players * sizeof(move_timer_t));
    _move_timers.num = 0;
    charge_timer_array_init(&_charge_timers, com_config.max_players);
}

void
pl_destroy(void)
{
    charge_timer_array_destroy(&_charge_timers);
    free(_despawned.indices);
    free(_move_timers.all);
    fixed_pool_destroy(&_pool);
    player_table_destroy(&_table);
}

int
pl_spawn(player_runtime_id_t world_session_id, player_db_guid_t db_id,
    const char *name, const char *display_name, int sex, int race, part_t *part,
    int direction, int x, int y, int z, uint8 move_speed,
    bool32 controlled_by_me, uint32 abilities[MAX_PLAYER_ABILITIES],
    uint32 num_abilities, uint32 health_current, uint32 health_max)
{
    muta_assert(!player_table_exists(&_table, world_session_id));
    player_race_def_t *race_def = ent_get_player_race_def(race);
    if (!race_def)
    {
        LOG_EXT("Race %d not found\n", race);
        return 1;
    }
    player_t *pl = fixed_pool_new(&_pool);
    if (!pl)
    {
        fsimmsg_spawn_player_fail_t s;
        s.world_session_id  = world_session_id;
        s.db_id             = db_id;
        bbuf_t bb = master_send(FSIMMSG_SPAWN_PLAYER_FAIL_SZ);
        if (bb.max_bytes)
            fsimmsg_spawn_player_fail_write(&bb, &s);
        return 0;
    }
    pl->world_session_id    = world_session_id;
    pl->db_id               = db_id;
    pl->sex                 = sex;
    pl->race                = race_def;
    pl->entity.direction    = direction;
    pl->entity.position[0]  = x;
    pl->entity.position[1]  = y;
    pl->entity.position[2]  = z;
    pl->entity.part_index   = part_get_index(part);
    pl->flags               = 0;
    pl->move_speed          = move_speed;
    pl->move_timer_index    = 0xFFFFFFFF;
    pl->charge_timer_index  = 0xFFFFFFFF;
    pl->abilities.num       = 0;
    pl->health_current      = health_current;
    pl->health_max          = health_max;
    muta_assert(sizeof(pl->abilities.data[0]) == sizeof(abilities[0]));
    muta_assert(num_abilities < MAX_PLAYER_ABILITIES);
    memcpy(pl->abilities.data, abilities, num_abilities * sizeof(abilities[0]));
    pl->abilities.num = num_abilities;
    player_table_einsert(&_table, world_session_id, pl_get_index(pl));
    if (controlled_by_me)
    {
        pl->flags |= PL_CONTROLLED_BY_ME;
        fsimmsg_confirm_spawn_player_t s;
        s.db_id = db_id;
        s.world_session_id = world_session_id;
        s.instance_part_id = part->id;
        s.x = x;
        s.y = y;
        s.z = z;
        memcpy(s.abilities.data, abilities,
            num_abilities * sizeof(abilities[0]));
        s.abilities.len = num_abilities;
        bbuf_t bb = master_send(fsimmsg_confirm_spawn_player_compute_sz(&s));
        if (bb.max_bytes)
            fsimmsg_confirm_spawn_player_write(&bb, &s);
    }
    LOG("Spawned player %u at position %d, %d, %d.", world_session_id,
        x, y, z);
    return 0;
}

void
pl_despawn(player_t *pl)
{
    muta_assert(!(pl->flags & PL_DESPAWNING));
    _despawned.indices[_despawned.num++] = pl_get_index(pl);
    pl->flags |= PL_DESPAWNING;
    LOG("Despawned player %u.", pl->world_session_id);
}

player_t *
pl_find(player_runtime_id_t world_session_id)
{
    pl_index_t *index = player_table_find(&_table, world_session_id);
    if (!index)
        return 0;
    player_t *pl = &_pool.all[*index];
    if (pl->flags & PL_DESPAWNING)
    {
        DEBUG_PRINTFF("called for player already despawned.\n");
        return 0;
    }
    return pl;
}

pl_index_t
pl_get_index(player_t *pl)
    {return fixed_pool_index(&_pool, pl);}

void
pl_find_path(player_t *pl, int x, int y, int z)
{
    if (pl->flags & PL_FINDING_PATH)
    {
        LOG_DEBUG_EXT("Player %u already finding path.", pl->world_session_id);
        pl->flags |= PL_PATHFIND_CANCELLED;
        pl->pathfind.after_cancel.do_move   = 1;
        pl->pathfind.after_cancel.x         = x;
        pl->pathfind.after_cancel.y         = y;
        pl->pathfind.after_cancel.z         = z;
        if (pl->move_timer_index != 0xFFFFFFFF)
            _move_timers.all[pl->move_timer_index].pathfinding = 0;
        return;
    }
    pf_user_data_t data = {
        .entity_type    = ENTITY_TYPE_PLAYER,
        .index          = pl_get_index(pl)};
    if (!pf_find(data, part_get(pl->entity.part_index),
        pl->entity.position[0], pl->entity.position[1], pl->entity.position[2],
        x, y, z))
        pl->flags |= PL_FINDING_PATH;
}

void
pl_on_pathfind_success(player_t *pl, uint8 directions[MAX_PATHFIND_PATH],
    uint32 num_directions)
{
    DEBUG_PRINTFF("Success, num directions: %u\n", num_directions);
    muta_assert(num_directions > 0);
    muta_assert(pl->flags & PL_FINDING_PATH);
    pl->flags &= ~PL_FINDING_PATH;
    bool32 cancelled = pl->flags & PL_PATHFIND_CANCELLED;
    pl->flags &= ~PL_PATHFIND_CANCELLED;
    if (!cancelled)
    {
        for (uint32 i = 0; i < num_directions; ++i)
            pl->pathfind.directions[i] = directions[i];
        pl->pathfind.num_directions = num_directions;
        if (pl->move_timer_index == 0xFFFFFFFF)
            _move_in_direction(pl,
                pl->pathfind.directions[--pl->pathfind.num_directions]);
    } else if (pl->pathfind.after_cancel.do_move)
    {
        pl_find_path(pl, pl->pathfind.after_cancel.x,
            pl->pathfind.after_cancel.y, pl->pathfind.after_cancel.z);
    }
}

void
pl_on_pathfind_fail(player_t *pl)
{
    muta_assert(pl->flags & PL_FINDING_PATH);
    pl->flags &= ~PL_FINDING_PATH;
    pl->flags &= ~PL_PATHFIND_CANCELLED;
}

void
pl_move_in_direction(player_t *pl, int direction)
{
    if (pl->flags & PL_FINDING_PATH)
    {
        pl->flags |= PL_PATHFIND_CANCELLED;
        pl->pathfind.after_cancel.do_move = 0;
        return;
    }
    if (pl->pathfind.num_directions)
    {
        pl->pathfind.num_directions = 0;
        if (pl->move_timer_index != 0xFFFFFFFF)
            _move_timers.all[pl->move_timer_index].pathfinding = 0;
    }
    _move_in_direction(pl, direction);
}

float
pl_get_move_time_left(player_t *pl)
{
    if (pl->move_timer_index == 0xFFFFFFFF)
        return 0.f;
    return _move_timers.all[pl->move_timer_index].time_left;
}

void
pl_update(float delta_s)
{
    _update_charge_timers(delta_s);
    _update_move_timers(delta_s);
    _complete_despawns();
}

player_t *
pl_get(pl_index_t index)
{
    muta_assert(index < _pool.max);
    return &_pool.all[index];
}

int
pl_use_ability(player_t *pl, ability_id_t ability_id,
    enum common_entity_type target_type, uint32 target_id)
{
    if (pl->charge_timer_index != 0xFFFFFFFF)
    {
        fsimmsg_player_use_ability_result_t s =
        {
            .player_runtime_id  = pl->world_session_id,
            .ability_id         = ability_id,
            .target_type        = target_type,
            .target_runtime_id  = target_id,
            .result = SIM_PLAYER_USE_ABILITY_RESULT_ALREADY_CASTING
        };
        bbuf_t bb = master_send(FSIMMSG_PLAYER_USE_ABILITY_RESULT_SZ);
        if (bb.max_bytes)
            fsimmsg_player_use_ability_result_write(&bb, &s);
        return 0;
    }
    script_entity_t target;
    uint32          target_index;
    uint8           target_type_for_timer;
    switch (target_type)
    {
    case COMMON_ENTITY_PLAYER:
        target.type             = SCRIPT_ENTITY_TYPE_PLAYER;
        target.data             = pl_find(target_id);
        target_index            = pl_get_index(target.data);
        target_type_for_timer   = ENTITY_TYPE_PLAYER;
        break;
    case COMMON_ENTITY_CREATURE:
        target.type             = SCRIPT_ENTITY_TYPE_CREATURE;
        target.data             = creature_find(target_id);
        target_index            = creature_get_index(target.data);
        target_type_for_timer   = ENTITY_TYPE_CREATURE;
        break;
    case COMMON_ENTITY_DYNAMIC_OBJECT:
        target.type             = SCRIPT_ENTITY_TYPE_DYNAMIC_OBJECT;
        target.data             = dobj_find(target_id);
        target_index            = dobj_get_index(target.data);
        target_type_for_timer   = ENTITY_TYPE_DYNAMIC_OBJECT;
        break;
    default:
        LOG_ERROR("Bad target entity type.");
        return 1;
    }
    if (!target.data)
    {
        LOG_ERROR("Target not found.");
        return 2;
    }
    uint32 num_abilities    = pl->abilities.num;
    bool32 have_ability     = 0;
    for (uint32 i = 0; i < num_abilities; ++i)
        if (pl->abilities.data[i].id == ability_id)
        {
            have_ability = 1;
            break;
        }
    if (!have_ability)
    {
        LOG_DEBUG_EXT("Player %u has no ability id %u.", pl->world_session_id,
            ability_id);
        fsimmsg_player_use_ability_result_t s =
        {
            .player_runtime_id  = pl->world_session_id,
            .ability_id         = ability_id,
            .target_type        = target_type,
            .target_runtime_id  = target_id,
            .result = SIM_PLAYER_USE_ABILITY_RESULT_PLAYER_HAS_NO_SPELL
        };
        bbuf_t bb = master_send(FSIMMSG_PLAYER_USE_ABILITY_RESULT_SZ);
        if (bb.max_bytes)
            fsimmsg_player_use_ability_result_write(&bb, &s);
        return 0;
    }
    ability_script_t *script = scripts_find_ability(ability_id);
    muta_assert(script);
    script_entity_t user = _get_script_entity(pl);
    enum ability_script_use_result result = script->use(script, &user, &target);
    fsimmsg_player_use_ability_result_t s =
    {
        .player_runtime_id  = pl->world_session_id,
        .ability_id         = ability_id,
        .target_type        = target_type,
        .target_runtime_id  = target_id
    };
    switch (result)
    {
    case ABILITY_SCRIPT_USE_SUCCESS:
        s.result = SIM_PLAYER_USE_ABILITY_RESULT_SUCCESS;
        if (script->compute_charge_time)
        {
            muta_assert(pl->charge_timer_index == 0xFFFFFFFF);
            uint32_t charge_time = script->compute_charge_time(&user, &target);
            pl->charge_timer_index = charge_timer_array_new(&_charge_timers,
                (float)charge_time, pl_get_index(pl), ability_id, target_index,
                target_id, target_type_for_timer);
        }
        break;
    case ABILITY_SCRIPT_USE_FAIL:
        s.result = SIM_PLAYER_USE_ABILITY_RESULT_FAIL;;
        break;
    }
    bbuf_t bb = master_send(FSIMMSG_PLAYER_USE_ABILITY_RESULT_SZ);
    if (bb.max_bytes)
        fsimmsg_player_use_ability_result_write(&bb, &s);
    return 0;
}

int
pl_on_requested_stop_ability_charge(player_t *pl)
{
    if (!pl_is_charging_ability(pl))
    {
        LOG_ERROR("Player %u is not charging ability.", pl->world_session_id);
        bbuf_t bb = master_send(FSIMMSG_PLAYER_STOP_CHARGE_ABILITY_FAIL_SZ);
        if (bb.max_bytes)
        {
            fsimmsg_player_stop_charge_ability_fail_t s =
            {
                .player_runtime_id = pl->world_session_id
            };
            fsimmsg_player_stop_charge_ability_fail_write(&bb, &s);
        }
    } else
    {
        charge_timer_t *timer = &_charge_timers.data[pl->charge_timer_index];
        ability_id_t ability_id = timer->ability_id;
        pl_stop_charging_ability(pl);
        bbuf_t bb = master_send(FSIMMSG_PLAYER_FINISHED_ABILITY_CHARGE_SZ);
        if (bb.max_bytes)
        {
            fsimmsg_player_finished_ability_charge_t s =
            {
                .player_runtime_id  = pl->world_session_id,
                .ability_id         = ability_id,
                .target_runtime_id  = timer->target_id,
                .result             = COMMON_ABILITY_CHARGE_RESULT_CANCELLED
            };
            switch (timer->target_type)
            {
            case ENTITY_TYPE_PLAYER:
                s.target_type = COMMON_ENTITY_PLAYER;
                break;
            case ENTITY_TYPE_CREATURE:
                s.target_type = COMMON_ENTITY_CREATURE;
                break;
            case ENTITY_TYPE_DYNAMIC_OBJECT:
                s.target_type = COMMON_ENTITY_DYNAMIC_OBJECT;
                break;
            default:
                muta_assert(0);
            }
            fsimmsg_player_finished_ability_charge_write(&bb, &s);
        }


        ability_script_t *script = scripts_find_ability(ability_id);
        if (script->on_user_stopped_charge)
        {
            script_entity_t script_entity = _get_script_entity(pl);
            script->on_user_stopped_charge(&script_entity);
        }
    }
    return 0;
}

bool32
pl_is_charging_ability(player_t *pl)
    {return pl->charge_timer_index  != 0xFFFFFFFF;}

void
pl_stop_charging_ability(player_t *pl)
{
    muta_assert(pl->charge_timer_index != 0xFFFFFFFF);
    charge_timer_array_free(&_charge_timers, pl->charge_timer_index);
    pl->charge_timer_index = 0xFFFFFFFF;
}

bool32
pl_is_moving(player_t *pl)
    {return pl->move_timer_index != 0xFFFFFFFF;}

void
pl_take_damage(player_t *pl, uint32 damage, uint32 health_current)
{
    pl->health_current = health_current;
}

static void
_free_move_timer(uint32 index)
{
    muta_assert(index < _move_timers.num);
    uint32 last = --_move_timers.num;
    if (index == last)
        return;
    _move_timers.all[index] = _move_timers.all[last];
    pl_get(_move_timers.all[index].entity_index)->move_timer_index = index;
}

static void
_update_move_timers(float delta_s)
{
    pl_index_t *pathfinders = dynamic_stack_push(&com_stack,
        _pool.max * sizeof(pl_index_t));
    uint32 num_pathfinders  = 0;
    uint32 num_timers       = _move_timers.num;
    /* Update timers for players currently moving from tile to tile. */
    for (uint32 i = 0; i < num_timers; ++i)
    {
        move_timer_t *timer = &_move_timers.all[i];
        timer->time_left -= delta_s;
        if (timer->time_left <= 0.f)
        {
            _pool.all[timer->entity_index].move_timer_index = 0xFFFFFFFF;
            _free_move_timer(i);
            num_timers--;
            i--;
            if (timer->pathfinding)
               pathfinders[num_pathfinders++] = timer->entity_index;
        }
    }
    /* Move stopped players to their next tile. */
    for (uint32 i = 0; i < num_pathfinders; ++i)
    {
        LOG_DEBUG_EXT(
            "Attempting to move player at index %u to next tile in path.",
            pathfinders[i]);
        player_t *pl = pl_get(pathfinders[i]);
        muta_assert(pl->pathfind.num_directions > 0);
        muta_assert(
            pl->pathfind.num_directions < sizeof(pl->pathfind.directions) -
                sizeof(pl->pathfind.directions[0]));
        _move_in_direction(pl,
            pl->pathfind.directions[--pl->pathfind.num_directions]);
    }
    dynamic_stack_pop(&com_stack,
        dynamic_stack_offset(&com_stack, pathfinders));
}

static void
_update_charge_timers(float dt_s)
{
    charge_timer_t  *data               = _charge_timers.data;
    uint32          num                 = _charge_timers.num;
    float           dt_ms               = dt_s * 1000.0f;
    uint32          num_finished_timers = 0;
    finished_charge_timer_t *finished_timers = dynamic_stack_push(&com_stack,
        num * sizeof(finished_charge_timer_t));
    for (uint32 i = 0; i < num; ++i)
    {
        charge_timer_t *timer = &data[i];
        timer->current_ms += dt_ms;
        if (timer->current_ms < timer->max_ms)
            continue;
        finished_charge_timer_t finished_charge_timer =
        {
            .ability_id     = timer->ability_id,
            .user_index     = timer->entity_index,
            .target_id      = timer->target_id,
            .target_index   = timer->target_index,
            .target_type    = timer->target_type
        };
        finished_timers[num_finished_timers++] = finished_charge_timer;
        player_t *pl = pl_get(timer->entity_index);
        muta_assert(pl->charge_timer_index != 0xFFFFFFFF);
        if (i != num - 1)
        {
            charge_timer_t *last_timer = &data[--num];
            *timer = *last_timer;
            player_t *last_pl = pl_get(timer->entity_index);
            muta_assert(last_pl != pl);
            muta_assert(last_pl->charge_timer_index != 0xFFFFFFFF);
            last_pl->charge_timer_index = i;
            i -= 1;
        } else {
            num -= 1;
        }
        pl->charge_timer_index = 0xFFFFFFFF;
    }
    _charge_timers.num = num;
    for (uint32 i = 0; i < num_finished_timers; ++i)
    {
        finished_charge_timer_t *timer  = &finished_timers[i];
        player_t                *user   = pl_get(timer->user_index);
        ability_script_t *script = scripts_find_ability(timer->ability_id);
        muta_assert(script);
        fsimmsg_player_finished_ability_charge_t s =
        {
            .player_runtime_id  = user->world_session_id,
            .ability_id         = timer->ability_id,
            .target_runtime_id  = timer->target_id
        };
        switch (timer->target_type)
        {
        case ENTITY_TYPE_PLAYER:
        {
            script_entity_t user_entity = _get_script_entity(user);
            player_t *target = pl_get(timer->target_index);
            if (target->world_session_id != timer->target_id ||
                (target->flags & PL_DESPAWNING))
            {
                if (script->on_charge_finished(&user_entity, 0))
                    s.result = COMMON_ABILITY_CHARGE_RESULT_SUCCESS;
                else
                    s.result = COMMON_ABILITY_CHARGE_RESULT_SCRIPT_FAILED;
            } else /* Lost target. */
            {
                script_entity_t target_entity = _get_script_entity(target);
                if (script->on_charge_finished(&user_entity, &target_entity))
                    s.result = COMMON_ABILITY_CHARGE_RESULT_SUCCESS;
                else
                    s.result = COMMON_ABILITY_CHARGE_RESULT_SCRIPT_FAILED;
            }
            s.target_type = COMMON_ENTITY_PLAYER;
        }
            break;
        case ENTITY_TYPE_CREATURE:
            script_entity_t user_entity = _get_script_entity(user);
            creature_t *target = creature_get(timer->target_index);
            if (target->runtime_id != timer->target_id)
            {
                if (script->on_charge_finished(&user_entity, 0))
                    s.result = COMMON_ABILITY_CHARGE_RESULT_SUCCESS;
                else
                    s.result = COMMON_ABILITY_CHARGE_RESULT_SCRIPT_FAILED;
            } else /* Lost target. */
            {
                script_entity_t target_entity = _get_creature_script_entity(
                    target);
                if (script->on_charge_finished(&user_entity, &target_entity))
                    s.result = COMMON_ABILITY_CHARGE_RESULT_SUCCESS;
                else
                    s.result = COMMON_ABILITY_CHARGE_RESULT_SCRIPT_FAILED;
            }
            s.target_type = COMMON_ENTITY_CREATURE;
            break;
        case ENTITY_TYPE_DYNAMIC_OBJECT:
            muta_assert(0);
            s.target_type = COMMON_ENTITY_DYNAMIC_OBJECT;
            break;
        default:
            muta_assert(0);
        }
        bbuf_t bb = master_send(FSIMMSG_PLAYER_FINISHED_ABILITY_CHARGE_SZ);
        if (!bb.max_bytes)
            continue;
        fsimmsg_player_finished_ability_charge_write(&bb, &s);
    }
    dynamic_stack_pop(&com_stack,
        dynamic_stack_offset(&com_stack, finished_timers));
}

static void
_complete_despawns(void)
{
    pl_index_t  *indices        = _despawned.indices;
    uint32      num_to_despawn  = _despawned.num;
    for (uint32 i = 0;i < num_to_despawn; ++i)
    {
        player_t *pl = pl_get(indices[i]);
        muta_assert(pl->flags & PL_DESPAWNING);
        muta_assert(player_table_exists(&_table, pl->world_session_id));
        player_runtime_id_t id = pl->world_session_id;
        player_table_erase(&_table, pl->world_session_id);
        fixed_pool_free(&_pool, pl);
        if (pl->move_timer_index != 0xFFFFFFFF)
            _free_move_timer(pl->move_timer_index);
        if (pl->charge_timer_index != 0xFFFFFFFF)
            charge_timer_array_free(&_charge_timers, pl->charge_timer_index);
        fsimmsg_confirm_despawn_player_t s;
        s.world_session_id = pl->world_session_id;
        bbuf_t bb = master_send(FSIMMSG_CONFIRM_DESPAWN_PLAYER_SZ);
        if (bb.max_bytes)
        {
            fsimmsg_confirm_despawn_player_write(&bb, &s);
            LOG_DEBUG("Confirmed despawn of player %u to master.", id);
        }
    }
    _despawned.num = 0;
}

static void
_move_in_direction(player_t *pl, int direction)
{
    if (pl->move_timer_index != 0xFFFFFFFF) /* Already moving */
        return;
#ifdef _MUTA_DEBUG
    int old_position[3];
    for (int i = 0; i < 3; ++i)
        old_position[i] = pl->entity.position[i];
#endif
    if (!entity_find_ground_move_destination(part_get(pl->entity.part_index),
        pl->entity.position, direction, pl->entity.position))
    {
        LOG_DEBUG("Can't move player in direction: "
            "entity_find_ground_move_destination() returned non-zero.");
        /* Stop pathfinding. */
        pl->pathfind.num_directions = 0;
        return;
    }
    muta_assert(_move_timers.num < _pool.max);
    /* Set a timer for this move */
    uint32 timer_index = _move_timers.num++;
    pl->move_timer_index = timer_index;
    move_timer_t *timer = &_move_timers.all[timer_index];
    timer->entity_index = pl_get_index(pl);
    timer->time_left    = ent_convert_move_speed_to_seconds(pl->move_speed);
    timer->pathfinding  = pl->pathfind.num_directions ? 1 : 0;
    if (pl->charge_timer_index != 0xFFFFFFFF)
    {
        charge_timer_t *timer = &_charge_timers.data[pl->charge_timer_index];
        ability_script_t *script = scripts_find_ability(timer->ability_id);
        muta_assert(script);
        if (script->on_user_moved)
        {
            script_entity_t user = _get_script_entity(pl);
            script_entity_t target;
            charge_timer_get_target(timer, &target);
            script->on_user_moved(&user, &target);
        }
    }
    /* Send message to master */
    fsimmsg_confirm_move_player_t s =
    {
        .world_session_id   = pl->world_session_id,
        .x                  = pl->entity.position[0],
        .y                  = pl->entity.position[1],
        .z                  = pl->entity.position[2]
    };
    bbuf_t bb = master_send(FSIMMSG_CONFIRM_MOVE_PLAYER_SZ);
    if (bb.max_bytes)
        fsimmsg_confirm_move_player_write(&bb, &s);
    LOG_DEBUG("Move player from %d %d %d to %d %d %d.", old_position[0],
        old_position[1], old_position[2], pl->entity.position[0],
        pl->entity.position[1], pl->entity.position[2]);
}

static script_entity_t
_get_script_entity(player_t *pl)
{
    script_entity_t script_entity =
    {
        .type   = SCRIPT_ENTITY_TYPE_PLAYER,
        .data   = pl
    };
    return script_entity;
}

static script_entity_t
_get_creature_script_entity(creature_t *cr)
{
    script_entity_t script_entity =
    {
        .type   = SCRIPT_ENTITY_TYPE_CREATURE,
        .data   = cr
    };
    return script_entity;
}
