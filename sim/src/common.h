#ifndef MUTA_SIM_COMMON_H
#define MUTA_SIM_COMMON_H

#include "../../shared/log.h"
#include "../../shared/svchan_client.h"

#define _LOG(category, fmt, ...) \
    log_printf(&com_log, category, fmt, ##__VA_ARGS__)

#define _LOG_EXT(category, fmt, ...) \
    log_printf(&com_log, category, "[%s, %s] " fmt, __FILE__, __func__, \
        ##__VA_ARGS__)

#define LOG(fmt, ...) \
    _LOG(LOG_CATEGORY_INFO, fmt, ##__VA_ARGS__)

#define LOG_ERROR(fmt, ...) \
    _LOG(LOG_CATEGORY_ERROR, fmt, ##__VA_ARGS__)

#define LOG_WARN(fmt, ...) \
    _LOG(LOG_CATEGORY_WARNING, fmt, ##__VA_ARGS__)

#define LOG_EXT(fmt, ...) \
    _LOG_EXT(LOG_CATEGORY_INFO, fmt, ##__VA_ARGS__)

#ifdef _MUTA_DEBUG
    #define LOG_DEBUG(fmt, ...) \
        _LOG(LOG_CATEGORY_DEBUG, fmt, ##__VA_ARGS__)
    #define LOG_DEBUG_EXT(fmt, ...) \
        _LOG_EXT(LOG_CATEGORY_DEBUG, fmt, ##__VA_ARGS__)
#else
    #define LOG_DEBUG(fmt, ...)     ((void)0)
    #define LOG_DEBUG_EXT(fmt, ...) ((void)0)
#endif

typedef struct com_event_t  com_event_t;
typedef struct com_config_t com_config_t;

enum log_category
{
    LOG_CATEGORY_INFO,
    LOG_CATEGORY_DEBUG,
    LOG_CATEGORY_WARNING,
    LOG_CATEGORY_ERROR,
    NUM_LOG_CATEGORIES
};

enum com_event
{
    COM_EVENT_SVCHAN_CLIENT
};

struct com_event_t
{
    int type;
    union
    {
        svchan_client_event_t svchan_client;
    };
};

struct com_config_t
{
    uint32  max_instance_parts;
    uint32  max_players;
    uint32  max_dynamic_objects;
    uint32  max_creatures;
    uint8   master_address[4];
    uint16  master_port;
    dchar   *master_username;
    dchar   *master_password;
    uint32  pathfind_grid_w;
    uint32  pathfind_grid_t;
    uint32  max_extra_threads;
};

extern com_config_t     com_config;
extern log_t            com_log;
extern dynamic_stack_t  com_stack;

void
com_init(const char *cfg_path, const char *log_dir_path);

void
com_destroy(void);

void
com_push_events(com_event_t *events, int32 num);

void
com_push_events_no_wait(com_event_t *events, int32 num);

int
com_wait_events(com_event_t *ret_events, int32 max, int timeout);

void
com_async_call(void (*callback)(void *user_data), void *args);

#endif /* MUTA_SIM_COMMON_H */
