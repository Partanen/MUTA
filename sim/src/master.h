#ifndef MUTA_SIM_MASTER_H
#define MUTA_SIM_MASTER_H

#include "../../shared/common_utils.h"

void
master_init(void);

void
master_destroy(void);

void
master_connect(void);

void
master_disconnect(void);

bbuf_t
master_send(uint32 num_bytes);

void
master_flush(void);

#endif /* MUTA_SIM_MASTER_H */
