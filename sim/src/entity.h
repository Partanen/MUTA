#ifndef MUTA_SIM_ENTITY_H
#define MUTA_SIM_ENTITY_H

#include "../../shared/types.h"
#include "../../shared/common_defs.h"

/* Forward declaration(s) */
typedef struct part_t part_t;

/* Types defined here. */
typedef struct entity_t entity_t;

enum entity_type
{
    ENTITY_TYPE_PLAYER,
    ENTITY_TYPE_CREATURE,
    ENTITY_TYPE_DYNAMIC_OBJECT
};

struct entity_t
{
    int     position[3]; /* Global position on full world map. */
    uint32  part_index;
    uint8   direction;
};

struct entity_target_t
{
    uint8   type; /* One of enum entity_type */
    uint32  index;
};

bool32
entity_find_ground_move_destination(part_t *part, int current_position[3],
    int direction, int ret_position[3]);
/* Returns non-zero if position changed. */

bool32
entity_can_move_to_position(part_t *part, tile_t tile_under_id,
    tile_t tile_forward_id, int direction);

static inline enum common_entity_type
entity_type_to_common_entity_type(enum entity_type type)
{
    switch (type)
    {
    case ENTITY_TYPE_PLAYER:
        return COMMON_ENTITY_PLAYER;
    case ENTITY_TYPE_CREATURE:
        return COMMON_ENTITY_CREATURE;
    case ENTITY_TYPE_DYNAMIC_OBJECT:
        return COMMON_ENTITY_DYNAMIC_OBJECT;
    }
    return 0;
}

#endif /* MUTA_SIM_ENTITY_H */
