#include "dynamic_object.h"
#include "common.h"
#include "part.h"
#include "master.h"
#include "player.h"
#include "geometry.h"
#include "../../shared/common_utils.h"
#include "../../shared/hashtable.h"
#include "../../shared/common_defs.h"
#include "../../shared/sim_packets.h"
#include "../../shared/tile.h"

typedef struct dobj_table dobj_table_t;

hashtable_define(dobj_table, dobj_runtime_id_t, dobj_index_t);

static dobj_table_t                 _table;
static fixed_pool(dynamic_object_t) _pool;

void
dobj_init(void)
{
    fixed_pool_init(&_pool, com_config.max_dynamic_objects);
    dobj_table_einit(&_table,
        (size_t)((float)com_config.max_dynamic_objects * 1.5f));
}

void
dobj_destroy(void)
{
    fixed_pool_destroy(&_pool);
    dobj_table_destroy(&_table);
}

int
dobj_spawn(dobj_runtime_id_t runtime_id,
    dobj_type_id_t type_id, part_t *part,
    int direction, int x, int y, int z, bool32 controlled_by_me,
    bool32 pending_spawn)
{
    muta_assert(direction >= 0 && direction < NUM_ISODIRS);
    dynamic_object_t *obj = fixed_pool_new(&_pool);
    if (!obj)
    {
        LOG_ERROR("Cannot spawn dynamic object: too many already spawned.");
        fsimmsg_spawn_dynamic_object_fail_t s = {.runtime_id = runtime_id};
        bbuf_t bb = master_send(FSIMMSG_SPAWN_DYNAMIC_OBJECT_FAIL_SZ);
        if (bb.max_bytes)
            fsimmsg_spawn_dynamic_object_fail_write(&bb, &s);
        return 0;
    }
    dobj_table_insert(&_table, runtime_id, fixed_pool_index(&_pool, obj));
    obj->runtime_id         = runtime_id;
    obj->type_id            = type_id;
    obj->entity.position[0] = x;
    obj->entity.position[1] = y;
    obj->entity.position[2] = z;
    obj->entity.part_index  = part_get_index(part);
    obj->entity.direction   = (uint8)direction;
    obj->flags              = 0;
    if (pending_spawn)
        obj->flags |= DOBJ_FLAG_PENDING_SPAWN;
    if (controlled_by_me)
    {
        if (!pending_spawn)
        {
            fsimmsg_confirm_spawn_dynamic_object_t s =
            {
                .runtime_id = runtime_id,
                .instance_part_id = part->id,
                .x                  = x,
                .y                  = y,
                .z                  = (uint8)z,
                .direction          = (uint8)direction
            };
            bbuf_t bb = master_send(FSIMMSG_CONFIRM_SPAWN_DYNAMIC_OBJECT_SZ);
            if (bb.max_bytes)
                fsimmsg_confirm_spawn_dynamic_object_write(&bb, &s);
        } else
        {
            fsimmsg_confirm_player_drop_dynamic_object_t s =
            {
                .runtime_id = runtime_id,
                .instance_part_id = part->id,
                .x                  = x,
                .y                  = y,
                .z                  = (uint8)z,
                .direction          = (uint8)direction
            };
            bbuf_t bb = master_send(
                FSIMMSG_CONFIRM_PLAYER_DROP_DYNAMIC_OBJECT_SZ);
            if (bb.max_bytes)
                fsimmsg_confirm_player_drop_dynamic_object_write(&bb, &s);
        }
    }
    LOG("Spawned dynamic object %u with type id %u.", runtime_id, type_id);
    return 0;
}

void
dobj_despawn(dynamic_object_t *obj)
{
    dobj_runtime_id_t runtime_id = obj->runtime_id;
    dobj_table_erase(&_table, runtime_id);
    fixed_pool_free(&_pool, obj);
    LOG("Despawned dynamic object %u.", runtime_id);
}

dynamic_object_t *
dobj_find(dobj_runtime_id_t runtime_id)
{
    uint32 *index = dobj_table_find(&_table, runtime_id);
    if (!index)
        return 0;
    return &_pool.all[*index];
}

dobj_index_t
dobj_get_index(dynamic_object_t *obj)
    {return fixed_pool_index(&_pool, obj);}

dynamic_object_t *
dobj_get(dobj_index_t index)
    {return &_pool.all[index];}

void
dobj_complete_pending_spawn(dynamic_object_t *obj)
{
    muta_assert(obj->flags | DOBJ_FLAG_PENDING_SPAWN);
    obj->flags &= ~DOBJ_FLAG_PENDING_SPAWN;
}

bool32
dobj_collides_with_tiles(dynamic_object_def_t *dobj_def, geo_t *geo, int x,
    int y, int z)
{
    muta_chunk_file_t *chunk = geo_get_chunk_of_tile(geo, x, y);
    int cx, cy, cz;
    geo_global_to_chunk_coords(geo, x, y, z, &cx, &cy, &cz);
    tile_t tile = geo_get_tile(chunk, cx, cy, cz);
    tile_def_t *tile_def = tile_get_def(tile);
    return !tile_def->passthrough;
}
