#ifndef MUTA_SIM_CREATURE_H
#define MUTA_SIM_CREATURE_H

#include "entity.h"

typedef struct creature_t   creature_t;
typedef uint32              creature_index_t;

enum creature_flag
{
    CREATURE_CONTROLLED_BY_ME = (1 << 0)
};

struct creature_t
{
    creature_runtime_id_t   runtime_id;
    creature_type_id_t      type_id;
    entity_t                entity;
    uint32                  health_current;
    uint32                  health_max;
    uint8                   flags;
};

void
creature_init(void);

void
creature_destroy(void);

int
creature_spawn(creature_runtime_id_t runtime_id, creature_type_id_t type_id,
    part_t *part, int direction, int x, int y, int z, bool32 controlled_by_me,
    uint32 health_current, uint32 health_max);
/* Assumes a creature with the same runtime id does not already exist.
 * Returns non-zero if parameters sent by server are invalid, but will return 0
 * if spawning fails due to too many objects existing or other similar reason
 * that isn't a bug. */

void
creature_despawn(creature_t *creature);

creature_t *
creature_find(creature_runtime_id_t runtime_id);

creature_index_t
creature_get_index(creature_t *creature);

creature_t *
creature_get(creature_index_t index);

void
creature_take_damage(creature_t *creature, uint32 damage, uint32 health_current);
/* Called when the master server sends a damage message. */

#endif /* MUTA_SIM_CREATURE_H */
