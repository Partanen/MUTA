# MUTA

# \
!ifndef 0 # \
include nmake.mk # \
!else
include gnu.mk
# \
!endif

all:
	cd client && $(MAKE)
	cd master && $(MAKE)
	cd sim && $(MAKE)
	cd proxy && $(MAKE)
	cd login && $(MAKE)

server:
	cd master && $(MAKE)
	cd sim && $(MAKE)
	cd proxy && $(MAKE)
	cd login && $(MAKE)

depend:
	cd dep && $(MAKE) all
	cd client && $(MAKE) depend
	cd master && $(MAKE) depend
	cd sim && $(MAKE) depend
	cd proxy && $(MAKE) depend
	cd login && $(MAKE) depend

clean:
	cd client && $(MAKE) clean
	cd master && $(MAKE) clean
	cd sim && $(MAKE) clean
	cd proxy && $(MAKE) clean
	cd login && $(MAKE) clean
