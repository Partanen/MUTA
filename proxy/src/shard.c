#include "shard.h"
#include "common.h"
#include "event.h"
#include "clients.h"
#include "../../shared/common_utils.h"
#include "../../shared/proxy_packets.h"
#include "../../shared/sv_common_defs.h"
#include "../../shared/net.h"
#include "../../shared/netpoll.h"
#include "../../shared/crypt.h"
#include "../../shared/containers.h"
#include "../../shared/account_rules.h"
#include "../../shared/hashtable.h"

#define SHARD_IN_BUF_SZ     MUTA_MTU
#define SHARD_OUT_BUF_SZ    (MUTA_MTU * 4)
#define SHARD_INDEX(shard_) ((uint32)((shard_) - _shards))

typedef struct account_name_t               account_name_t;
typedef struct client_info_t                client_info_t;
typedef struct shard_t                      shard_t;
typedef struct shard_info_t                 shard_info_t;
typedef struct socket_client_info_table     socket_client_info_table_t;
typedef struct account_client_info_table    account_client_info_table_t;

enum client_info_flag
{
    CLIENT_INFO_FLAG_IN_USE             = (1 << 0),
    CLIENT_INFO_FLAG_AUTHED             = (1 << 1)
};

enum shard_flag
{
    SHARD_FLAG_WILL_FLUSH = (1 << 0)
};

struct account_name_t
{
    char str[MAX_ACC_NAME_LEN + 1];
};

hashtable_define(socket_client_info_table, uint32, uint32);
hashtable_define(account_client_info_table, account_name_t, uint32);

struct client_info_t
{
    uint32          socket_id; /* Assigned by the shard */
    uint32          flags;
    uint64          account_id;
    uint32          ip;
    uint32          client_index; /* For clients.h/c. Valid if flagged authed */
    account_name_t  account_name;
    uint8           token[AUTH_TOKEN_SZ];
    cryptchan_t     cryptchan;
};

struct shard_t
{
    socket_t    socket;
    uint32      flags;
    addr_t      address;
    cryptchan_t cryptchan;
    struct
    {
        uint8   *memory;
        int     num_bytes;
    } in_buf;
    struct
    {
        uint8   memory[SHARD_OUT_BUF_SZ];
        int     num_bytes;
    } out_buf;
    char                                name[MAX_SHARD_NAME_LEN + 1];
    thread_t                            connect_thread;
    fixed_pool(client_info_t)           client_infos;
    socket_client_info_table_t          socket_client_info_table;
    account_client_info_table_t         account_client_info_table;
};

struct shard_info_t
{
    addr_t address;
};

static shard_info_t                 _shard_infos[MAX_SHARDS];
static uint32                       _num_shard_infos;
static shard_t                      _shards[MAX_SHARDS];
static uint32                       _num_shards;
static shard_t                      *_shards_to_flush[MAX_SHARDS];
static uint32                       _num_shards_to_flush;
static bool32                       _running;
static char                         _shard_password[MAX_PROXY_PW_LEN + 1];
static thread_t                     _thread;
static netpoll_t                    _netpoll;
static segpool_t                    _segpool;
static mutex_t                      _segpool_mutex;
static uint8                        _in_buf_mem[MAX_SHARDS * SHARD_IN_BUF_SZ];
static hashtable(uint32, uint32)    _ip_table; /* For checking incoming IPs for
                                                  legality */

static shard_t *
_get_shard_by_name(const char *name);

static bbuf_t
_send_msg_to_shard(shard_t *shard, int len);

static inline bbuf_t
_send_const_crypt_msg_to_shard(shard_t *shard, int len);

static inline bbuf_t
_send_var_crypt_msg_to_shard(shard_t *shard, int len);

static int
_recv_to_bbuf(socket_t s, bbuf_t *bb, int num_bytes);

static socket_t
_connect_to_shard(addr_t a, cryptchan_t *ret_cc, char *ret_name);

static void
_read_config_callback(void *ctx, const char *opt, const char *val);

static thread_ret_t
_main(void *args);

static void
_allocate_event_messages(event_t *events, int num_events);

static void
_check_netpoll_events(netpoll_event_t *events, int num_events);

static thread_ret_t
_connect_to_shard_callback(void *args);

static int
_start_shard_connection_thread(shard_t *shard);

static void
_disconnect_shard(shard_t *shard, bool32 del_from_netpoll);

static int
_new_client_info(shard_t *shard, char *account_name, uint32 socket_id,
    uint64 account_id, uint32 ip, uint8 *token);

static void
_delete_client_info(shard_t *shard, uint32 index);

static int
_read_shard_packet(shard_t *shard); /* Returns 0 on success */

static int
_handle_tproxy_msg_opened_socket(shard_t *shard, tproxy_msg_opened_socket_t *s);

static int
_handle_tproxy_msg_closed_socket(shard_t *shard,
    tproxy_msg_closed_socket_t *s);

static int
_handle_tproxy_msg_player_packet(shard_t *shard, tproxy_msg_player_packet_t *s);

int
shard_init(void)
{
    int ret = 0;
    _shard_password[0] = 0;
    strcpy(_shard_password, "password");
    for (int i = 0; i < MAX_SHARDS; ++i)
    {
        _shards[i].in_buf.memory = _in_buf_mem + i * SHARD_IN_BUF_SZ;
        fixed_pool_init(&_shards[i].client_infos, com_config.max_clients);
        socket_client_info_table_einit(&_shards[i].socket_client_info_table,
            com_config.max_clients);
        account_client_info_table_einit(&_shards[i].account_client_info_table,
            com_config.max_clients);
    }
    int err;
    hashtable_init(_ip_table, com_config.max_clients, &err);
    if (err)
        muta_panic();
    for (int i = 0; i < MAX_SHARDS; ++i)
        thread_init(&_shards[i].connect_thread);
    parse_cfg_file("shards.cfg", _read_config_callback, 0);
    if (!_num_shard_infos)
    {
        LOG("No shards defined in config!");
        ret = 1;
        goto fail;
    }
    if (thread_init(&_thread))
        {ret = 2; goto fail;}
    if (netpoll_init(&_netpoll))
        {ret = 3; goto fail;}
    segpool_init(&_segpool);
    mutex_init(&_segpool_mutex);
    return ret;
    fail:
        LOGF("Failed with code %d.", ret);
        return ret;
}

void
shard_destroy(void)
{
}

int
shard_start(void)
{
    int ret = 0;
    _running = 1;
    muta_assert(!_num_shards);
    for (uint32 i = 0; i < _num_shard_infos; ++i)
    {
        shard_t *shard = &_shards[_num_shards++];
        shard->socket   = KSYS_INVALID_SOCKET;
        shard->address  = _shard_infos[i].address;
        cryptchan_clear(&shard->cryptchan);
        if (_start_shard_connection_thread(shard))
            {ret = 1; goto fail;}
    }
    if (thread_create(&_thread, _main, 0))
        {ret = 2; goto fail;}
    return ret;
    fail:
        LOGF("Failed with code %d.", ret);
        return ret;
}

void
shard_read(read_shard_event_t *event)
{
    shard_t *shard = &_shards[event->shard];
    if (event->num_bytes <= 0) /* No need to free message memory */
    {
        LOG("Shard closed connection.");
        _disconnect_shard(shard, 0);
        return;
    }
    int num_bytes   = event->num_bytes;
    int num_read    = 0;
    while (num_read < num_bytes)
    {
        int max_read    = SHARD_IN_BUF_SZ - shard->in_buf.num_bytes;
        int num_to_read = MIN(num_bytes - num_read, max_read);
        memcpy(shard->in_buf.memory + shard->in_buf.num_bytes,
            (uint8*)event->memory + num_read, num_to_read);
        shard->in_buf.num_bytes += num_to_read;
        if (_read_shard_packet(shard))
        {
            _disconnect_shard(shard, 1);
            goto out;
        }
        num_read += num_to_read;
    }
    out:
        mutex_lock(&_segpool_mutex);
        segpool_free(&_segpool, event->memory);
        mutex_unlock(&_segpool_mutex);
}

void
shard_flush(void)
{
    uint32  num_shards = _num_shards_to_flush;
    shard_t *shard;
    for (uint32 i = 0; i < num_shards; ++i)
    {
        shard = _shards_to_flush[i];
        shard->flags &= ~SHARD_FLAG_WILL_FLUSH;
        muta_assert(!(shard->flags & SHARD_FLAG_WILL_FLUSH));
        if (shard->socket == KSYS_INVALID_SOCKET || !shard->out_buf.num_bytes)
            continue;
        if (net_send_all(shard->socket, shard->out_buf.memory,
            shard->out_buf.num_bytes) <= 0)
            _disconnect_shard(shard, 1);
        shard->out_buf.num_bytes = 0;
    }
    _num_shards_to_flush = 0;
}

bool32
shard_check_client_ip(uint32 ip)
{
    size_t hash = hashtable_hash(&ip, sizeof(ip));
    return hashtable_exists(_ip_table, ip, hash);
}

int
shard_check_client_auth_proof(const char *shard_name, const char *account_name,
    uint8 *token, cryptchan_t *cryptchan, uint32 client_index,
    uint32 *ret_shard_index, uint32 *ret_client_info_index)
{
    muta_assert(strlen(account_name) > 0);
    shard_t *shard = _get_shard_by_name(shard_name);
    if (!shard)
        return 1;
    if (shard->socket == KSYS_INVALID_SOCKET)
        return 2;
    account_name_t name_key;
    strncpy(name_key.str, account_name, sizeof(name_key.str));
    name_key.str[sizeof(name_key.str) - 1] = 0;
    uint32 *client_info_index = account_client_info_table_find(
        &shard->account_client_info_table, name_key);
    if (!client_info_index)
        return 3;
    client_info_t *client_info = &shard->client_infos.all[*client_info_index];
    if (client_info->flags & CLIENT_INFO_FLAG_AUTHED)
        return 4;
    if (memcmp(token, client_info->token, AUTH_TOKEN_SZ))
        return 5;
    bbuf_t bb = _send_const_crypt_msg_to_shard(shard,
        FPROXY_MSG_PLAYER_CRYPTCHAN_SZ);
    if (!bb.max_bytes)
    {
        _disconnect_shard(shard, 1);
        return 6;
    }
    fproxy_msg_player_cryptchan_t c_msg;
    c_msg.socket_id = client_info->socket_id;
    memcpy(c_msg.pk, cryptchan->pk, sizeof(cryptchan->pk));
    memcpy(c_msg.sk, cryptchan->sk, sizeof(cryptchan->sk));
    memcpy(c_msg.wx, cryptchan->wx, sizeof(cryptchan->wx));
    memcpy(c_msg.rx, cryptchan->rx, sizeof(cryptchan->rx));
    memcpy(c_msg.read_key, cryptchan->rstream.k, sizeof(cryptchan->rstream.k));
    memcpy(c_msg.read_nonce, cryptchan->rstream.nonce,
        sizeof(cryptchan->rstream.nonce));
    memcpy(c_msg.write_key, cryptchan->wstream.k, sizeof(cryptchan->wstream.k));
    memcpy(c_msg.write_nonce, cryptchan->wstream.nonce,
        sizeof(cryptchan->wstream.nonce));
    int r = fproxy_msg_player_cryptchan_write_const_encrypted(&bb,
        &shard->cryptchan, &c_msg);
    muta_assert(!r);
    client_info->flags |= CLIENT_INFO_FLAG_AUTHED;
    client_info->client_index   = client_index;
    *ret_client_info_index      = *client_info_index;
    *ret_shard_index            = SHARD_INDEX(shard);
    return 0;
}

void
shard_forward_client_packet(uint32 shard_index, uint32 socket_id, void *memory,
    int num_bytes)
{
    muta_assert(shard_index < MAX_SHARDS);
    shard_t *shard = &_shards[shard_index];
    muta_assert(shard->socket != KSYS_INVALID_SOCKET);
    bbuf_t bb = _send_msg_to_shard(shard,
        FPROXY_MSG_PLAYER_PACKET_COMPUTE_SZ(num_bytes));
    if (!bb.max_bytes)
    {
        _disconnect_shard(shard, 1);
        return;
    }
    fproxy_msg_player_packet_t p_msg;
    p_msg.socket_id = socket_id;
    p_msg.data      = memory;
    p_msg.data_len  = num_bytes;
    int r = fproxy_msg_player_packet_write(&bb, &p_msg);
    muta_assert(!r);
}

bool32
shard_exists(const char *shard_name)
{
    for (int i = 0; i < MAX_SHARDS; ++i)
    {
        if (_shards[i].socket == KSYS_INVALID_SOCKET)
            continue;
        if (streq(shard_name, _shards[i].name))
            return 1;
    }
    return 0;
}

void
shard_delete_client_info(uint32 shard_index, uint32 client_info_index,
    bool32 inform_shard)
{
    muta_assert(shard_index < MAX_SHARDS);
    shard_t *shard = &_shards[shard_index];
    muta_assert(client_info_index < shard->client_infos.max);
    client_info_t *client_info = &shard->client_infos.all[client_info_index];
    /* Assert for being connected here since this is an external function
     * intended for connected clients. */
    muta_assert(client_info->flags & CLIENT_INFO_FLAG_AUTHED);
    if (inform_shard)
    {
        bbuf_t bb = _send_msg_to_shard(shard, FPROXY_MSG_CLOSE_SOCKET_SZ);
        if (!bb.max_bytes)
        {
            _disconnect_shard(shard, 1);
            return;
        }
        fproxy_msg_close_socket_t s;
        s.socket_id = client_info->socket_id;
        fproxy_msg_close_socket_write(&bb, &s);
    }
    _delete_client_info(shard, client_info_index);
}

static shard_t *
_get_shard_by_name(const char *name)
{
    for (uint32 i = 0; i < _num_shards; ++i)
        if (streq(_shards[i].name, name))
            return &_shards[i];
    return 0;
}

static bbuf_t
_send_msg_to_shard(shard_t *shard, int len)
{
    bbuf_t ret = {0};
    int req_len = PROXY_MSGTSZ + len;
    muta_assert(req_len < SHARD_OUT_BUF_SZ);
    if (shard->out_buf.num_bytes + len > SHARD_OUT_BUF_SZ)
    {
        if (net_send_all(shard->socket, shard->out_buf.memory,
            shard->out_buf.num_bytes) < 1)
            return ret;
        shard->out_buf.num_bytes = 0;
    }
    BBUF_INIT(&ret, shard->out_buf.memory + shard->out_buf.num_bytes, req_len);
    shard->out_buf.num_bytes += req_len;
    if (!(shard->flags & SHARD_FLAG_WILL_FLUSH))
    {
        _shards_to_flush[_num_shards_to_flush++] = shard;
        shard->flags |= SHARD_FLAG_WILL_FLUSH;
    }
    return ret;
}

static inline bbuf_t
_send_const_crypt_msg_to_shard(shard_t *shard, int len)
    {return _send_msg_to_shard(shard, CRYPT_MSG_ADDITIONAL_BYTES + len);}

static inline bbuf_t
_send_var_crypt_msg_to_shard(shard_t *shard, int len)
    {return _send_msg_to_shard(shard, sizeof(msg_sz_t) +
        CRYPT_MSG_ADDITIONAL_BYTES + len);}

static int
_recv_to_bbuf(socket_t s, bbuf_t *bb, int num_bytes)
{
    int required = bb->num_bytes + num_bytes;
    int received = 0;
    muta_assert(required <= bb->max_bytes);
    while (received < required)
    {
        int n = net_recv(s, BBUF_CUR_PTR(bb), required - received);
        if (n <= 0)
            return 1;
        bb->num_bytes   += n;
        received        += n;
    }
    return 0;
}

static socket_t
_connect_to_shard(addr_t a, cryptchan_t *ret_cc, char *ret_name)
{
    socket_t s = net_tcp_ipv4_sock();
    if (s == KSYS_INVALID_SOCKET)
        return s;
    if (net_disable_nagle(s))
        goto fail;
    if (net_connect(s, a))
    {
        DEBUG_LOGF("net_connect() failed.");
        goto fail;
    }
    uint8 *ip = (uint8*)&a.ip;
    LOG("Formed inital connection to shard %d.%d.%d.%d. "\
        "Beginning handshake...", ip[3], ip[2], ip[1], ip[0]);
    cryptchan_t cc;
    uint8       buf[MUTA_MTU];
    proxy_msg_type_t mt;
    bbuf_t bb = BBUF_INITIALIZER(buf, sizeof(buf));
    bbuf_t read_bb;
    proxy_msg_pub_key_t key_out;
    if (cryptchan_init(&cc, key_out.key))
    {
        LOGF("cryptchan_init() failed!");
        goto fail;
    }
    /*-- Send our public key --*/
    proxy_msg_pub_key_write(&bb, &key_out);
    if (net_send_all(s, bb.mem, bb.num_bytes) < 0)
    {
        LOGF("Failed to send public key!");
        goto fail;
    }
    BBUF_CLEAR(&bb);
    LOGF("Waiting for public key and stream header...");
    if (_recv_to_bbuf(s, &bb, 2 * PROXY_MSGTSZ + PROXY_MSG_STREAM_HEADER_SZ +
            PROXY_MSG_PUB_KEY_SZ))
    {
        LOGF("shard disconnected during handshake!");
        goto fail;
    }
    /*-- Read the public key --*/
    BBUF_INIT(&read_bb, bb.mem, bb.num_bytes);
    BBUF_READ(&read_bb, &mt);
    if (mt != PROXY_MSG_PUB_KEY)
    {
        LOGF("Did not receive public key!");
        goto fail;
    }
    proxy_msg_pub_key_t         key_in;
    proxy_msg_stream_header_t   header_in;
    proxy_msg_stream_header_t   header_out;
    if (proxy_msg_pub_key_read(&read_bb, &key_in))
    {
        LOGF("Public key message was invalid!");
        goto fail;
    }
    if (cryptchan_cl_store_pub_key(&cc, key_in.key, header_out.header))
    {
        LOGF("cryptchan_cl_store_pub_key() failed!");
        goto fail;
    }
    /*-- Read the stream header --*/
    BBUF_READ(&read_bb, &mt);
    if (mt != PROXY_MSG_STREAM_HEADER)
    {
        LOGF("Did not receive stream header!");
        goto fail;
    }
    if (proxy_msg_stream_header_read(&read_bb, &header_in))
        goto fail;
    if (cryptchan_store_stream_header(&cc, header_in.header))
        goto fail;
    muta_assert(cryptchan_is_encrypted(&cc));
    /*-- Send our stream header --*/
    BBUF_CLEAR(&bb);
    int rr = proxy_msg_stream_header_write(&bb, &header_out);
    muta_assert(!rr);
    if (net_send_all(s, bb.mem, bb.num_bytes) < 0)
    {
        LOGF("Failed to send stream header!");
        goto fail;
    }
    /*-- Send password --*/
    fproxy_msg_login_t out_login;
    out_login.password      = _shard_password;
    out_login.password_len  = (uint8)strlen(_shard_password);
    BBUF_CLEAR(&bb);
    int r = fproxy_msg_login_write_var_encrypted(&bb, &cc, &out_login);
    muta_assert(!r);
    if (net_send_all(s, bb.mem, bb.num_bytes) < 0)
    {
        LOGF("Failed to send password!");
        goto fail;
    }
    /*-- Wait for login result --*/
    BBUF_CLEAR(&bb);
    LOGF("Waiting for login result...");
    if (_recv_to_bbuf(s, &bb, PROXY_MSGTSZ +
        CRYPT_MSG_ADDITIONAL_BYTES + TPROXY_MSG_LOGIN_RESULT_SZ))
    {
        LOGF("shard disconnected during handshake!");
        goto fail;
    }
    BBUF_INIT(&read_bb, bb.mem, bb.num_bytes);
    BBUF_READ(&read_bb, &mt);
    if (mt != TPROXY_MSG_LOGIN_RESULT)
    {
        LOGF("Expected login result message, but received something else!");
        goto fail;
    }
    tproxy_msg_login_result_t in_login_result;
    int n;
    if ((n = tproxy_msg_login_result_read_const_encrypted(&read_bb, &cc,
        &in_login_result)))
    {
        LOGF("Problem reading login result message (%d)!", n);
        goto fail;
    }
    if (in_login_result.result)
    {
        LOG("Login to a shard failed (password), error %u!",
            in_login_result.result);
        goto fail;
    }
    if (in_login_result.shard_name_len < MIN_SHARD_NAME_LEN ||
        in_login_result.shard_name_len > MAX_SHARD_NAME_LEN)
    {
        LOG("Login to shard failed: invalid shard name.");
        goto fail;
    }
    memcpy(ret_name, in_login_result.shard_name,
        in_login_result.shard_name_len);
    ret_name[in_login_result.shard_name_len] = 0;
    LOG("Successfully logged in to shard %s!", ret_name);
    *ret_cc = cc;
    return s;
    fail:
        net_shutdown_sock(s, SOCKSD_BOTH);
        return KSYS_INVALID_SOCKET;
}

static void
_read_config_callback(void *ctx, const char *opt, const char *val)
{
    if (streq(opt, "password"))
    {
        size_t len = strlen(val);
        if (len > MAX_PROXY_PW_LEN)
            return;
        memcpy(_shard_password, val, len + 1);
    } else
    if (streq(opt, "shard"))
    {
        if (_num_shard_infos == MAX_SHARDS)
        {
            muta_panic_print("Too many shards defined in config.");
            return;
        }
        addr_t address;
        if (addr_init_from_str(&address, val, DEFAULT_PROXY_PORT))
        {
            LOG("Invalid shard IP in config: '%s'", val);
            return;
        }
        for (uint32 i = 0; i < _num_shard_infos; ++i)
        {
            if (memcmp(&address, &_shard_infos[i].address, sizeof(addr_t)))
                continue;
            LOG("Warning: two shards with the same address defined in config!");
            return;
        }
        _shard_infos[_num_shard_infos++].address = address;
    }
}

static thread_ret_t
_main(void *args)
{
    netpoll_event_t events[64];
    while (_running)
    {
        int num_events = netpoll_wait(&_netpoll, events, 64, 5000);
        _check_netpoll_events(events, num_events);
    }
    return 0;
}

static void
_allocate_event_messages(event_t *events, int num_events)
{
    mutex_lock(&_segpool_mutex);
    for (int i = 0; i < num_events; ++i)
    {
        if (events[i].read_shard.num_bytes <= 0)
            continue;
        void *memory = segpool_malloc(&_segpool,
            events[i].read_shard.num_bytes);
        memcpy(memory, events[i].read_shard.memory,
            events[i].read_shard.num_bytes);
        events[i].read_shard.memory = memory;
    }
    mutex_unlock(&_segpool_mutex);
}

static void
_check_netpoll_events(netpoll_event_t *events, int num_events)
{
    event_t new_events[8];
    int     num_new_events  = 0;
    int     buf_offset      = 0;
    uint8   buf[MUTA_MTU * 8];
    for (int i = 0; i < num_events; ++i)
    {
        if (!(events[i].events & (NETPOLL_READ | NETPOLL_HUP)))
            continue;
        shard_t *shard = events[i].data.ptr;
        event_t *ne = &new_events[num_new_events++];
        int num_bytes   = net_recv(shard->socket, buf + buf_offset, MUTA_MTU);
        DEBUG_LOGF("received %d bytes.", num_bytes);
        ne->type                    = EVENT_READ_SHARD;
        ne->read_shard.num_bytes    = num_bytes;
        ne->read_shard.memory       = buf + buf_offset;
        if (num_bytes >= 0)
            buf_offset += num_bytes;
        if (num_bytes <= 0)
            netpoll_del(&_netpoll, shard->socket);
        if (num_new_events < 8)
            continue;
        _allocate_event_messages(new_events, num_new_events);
        event_push(com_event_buf, new_events, num_new_events);
        num_new_events  = 0;
        buf_offset      = 0;
    }
    if (!num_new_events)
        return;
    _allocate_event_messages(new_events, num_new_events);
    event_push(com_event_buf, new_events, num_new_events);
}

static thread_ret_t
_connect_to_shard_callback(void *args)
{
    shard_t *shard = args;
    while (_running)
    {
        cryptchan_clear(&shard->cryptchan);
        socket_t s = _connect_to_shard(shard->address, &shard->cryptchan,
            shard->name);
        shard->in_buf.num_bytes     = 0;
        shard->out_buf.num_bytes    = 0;
        shard->flags                = 0;
        if (s == KSYS_INVALID_SOCKET)
        {
            LOG("Failed to connect shard - reconnecting in 2 sec...");
            sleep_ms(2000);
        } else
        {
            shard->socket             = s;
            netpoll_event_t netpoll_event;
            netpoll_event.events    = NETPOLL_READ;
            netpoll_event.data.ptr  = shard;
            int r = netpoll_add(&_netpoll, s, &netpoll_event);
            muta_assert(!r);
            LOG("Connected to a shard.");
            break;
        }
    }
    return 0;
}

static int
_start_shard_connection_thread(shard_t *shard)
{
    return thread_create(&shard->connect_thread, _connect_to_shard_callback,
        shard);
}

static void
_disconnect_shard(shard_t *shard, bool32 del_from_netpoll)
{
    if (del_from_netpoll)
        netpoll_del(&_netpoll, shard->socket);
    net_shutdown_sock(shard->socket, SOCKSD_BOTH);
    shard->socket = KSYS_INVALID_SOCKET;
    LOG("Disconnected a shard.");
    uint32          num_client_infos = shard->client_infos.max;
    client_info_t   *client_info;
    for (uint32 i = 0; i < num_client_infos; ++i)
    {
        client_info = &shard->client_infos.all[i];
        if (!(client_info->flags & CLIENT_INFO_FLAG_IN_USE))
            continue;
        if (!(client_info->flags & CLIENT_INFO_FLAG_AUTHED))
            continue;
        cl_disconnect(client_info->client_index);
        /* Also disconnect any clients attempting to log in to the shard ?*/
    }
    hashtable_clear(shard->socket_client_info_table, 0);
    hashtable_clear(shard->account_client_info_table, 0);
    fixed_pool_clear(&shard->client_infos);
}

static int
_new_client_info(shard_t *shard, char *account_name, uint32 socket_id,
    uint64 account_id, uint32 ip, uint8 *token)
{
    /* If an old connection from the same account exists, disconnect it */
    account_name_t name_key;
    strncpy(name_key.str, account_name, sizeof(name_key.str));
    name_key.str[sizeof(name_key.str) - 1] = 0;
    uint32 *old_client_info_index = account_client_info_table_find(
        &shard->account_client_info_table, name_key);
    if (old_client_info_index)
        cl_disconnect(
            shard->client_infos.all[*old_client_info_index].client_index);
    if (socket_client_info_table_exists(&shard->socket_client_info_table,
        socket_id))
    {
        LOG("Warning: attempted to register two sockets with the same id.");
        return 1;
    }
    client_info_t *client_info = fixed_pool_new(&shard->client_infos);
    if (!client_info)
        return 2;
    client_info->socket_id  = socket_id;
    client_info->account_id = account_id;
    client_info->ip         = ip;
    strncpy(client_info->account_name.str, account_name,
        sizeof(client_info->account_name.str));
    client_info->account_name.str[sizeof(client_info->account_name.str) - 1] =
        0;
    memcpy(client_info->token, token, AUTH_TOKEN_SZ);
    client_info->flags      = 0;
    uint32 client_index = fixed_pool_index(&shard->client_infos, client_info);
    account_client_info_table_einsert(&shard->account_client_info_table,
        name_key, client_index);
    socket_client_info_table_einsert(&shard->socket_client_info_table,
        socket_id, client_index);
    size_t ip_hash      = hashtable_hash(&ip, sizeof(ip));
    uint32 *ip_count    = hashtable_find(_ip_table, ip, ip_hash);
    if (!ip_count)
    {
        uint32 value = 1;
        hashtable_einsert(_ip_table, ip, ip_hash, value);
    } else
        (*ip_count)++;
    client_info->flags |= CLIENT_INFO_FLAG_IN_USE;
    return 0;
}

static void
_delete_client_info(shard_t *shard, uint32 index)
{
    client_info_t *client_info = &shard->client_infos.all[index];
    muta_assert(client_info->flags & CLIENT_INFO_FLAG_IN_USE);
    account_client_info_table_erase(&shard->account_client_info_table,
        client_info->account_name);
    uint32_t    socket_id = client_info->socket_id;
    socket_client_info_table_erase(&shard->socket_client_info_table, socket_id);
    uint32      ip          = client_info->ip;
    size_t      hash        = hashtable_hash(&ip, sizeof(ip));
    uint32_t    *ip_count   = hashtable_find(_ip_table, ip, hash);
    muta_assert(ip_count && *ip_count > 0);
    if (!--(*ip_count))
        hashtable_erase(_ip_table, ip, hash);
    client_info->flags &= ~CLIENT_INFO_FLAG_IN_USE;
}


static int
_read_shard_packet(shard_t *shard)
{
    bbuf_t bb = BBUF_INITIALIZER(shard->in_buf.memory, shard->in_buf.num_bytes);
    proxy_msg_type_t    msg_type;
    int                 incomplete  = 0;
    int                 dc          = 0;
    while (BBUF_FREE_SPACE(&bb) >= PROXY_MSGTSZ)
    {
        BBUF_READ(&bb, &msg_type);
        switch (msg_type)
        {
        case TPROXY_MSG_OPENED_SOCKET:
        {
            tproxy_msg_opened_socket_t s;
            incomplete = tproxy_msg_opened_socket_read_var_encrypted(&bb,
                &shard->cryptchan, &s);
            if (!incomplete)
                dc = _handle_tproxy_msg_opened_socket(shard, &s);
        }
            break;
        case TPROXY_MSG_CLOSED_SOCKET:
        {
            tproxy_msg_closed_socket_t s;
            incomplete = tproxy_msg_closed_socket_read_const_encrypted(&bb,
                &shard->cryptchan, &s);
            if (!incomplete)
                dc = _handle_tproxy_msg_closed_socket(shard, &s);
        }
            break;
        case TPROXY_MSG_PLAYER_PACKET:
        {
            tproxy_msg_player_packet_t s;
            incomplete = tproxy_msg_player_packet_read(&bb, &s);
            if (!incomplete)
                dc = _handle_tproxy_msg_player_packet(shard, &s);
        }
            break;
        default:
            LOGF("Unrecognized packet type %u.", msg_type);
            dc = 1;
            break;
        }
        if (dc || incomplete < 0)
        {
            DEBUG_LOGF("msg_type: %u, dc: %d, incomplete: %d.", msg_type, dc,
                incomplete);
            return 1;
        }
        if (incomplete)
        {
            bb.num_bytes -= sizeof(proxy_msg_type_t);
            break;
        }
    }
    uint32 free_space = BBUF_FREE_SPACE(&bb);
    memmove(shard->in_buf.memory,
        shard->in_buf.memory + shard->in_buf.num_bytes - free_space,
        free_space);
    shard->in_buf.num_bytes = free_space;
    return 0;
}

static int
_handle_tproxy_msg_opened_socket(shard_t *shard, tproxy_msg_opened_socket_t *s)
{
    fproxy_msg_opened_socket_result_t r_msg;
    r_msg.socket_id = s->socket_id;
    char account_name[MAX_ACC_NAME_LEN + 1];
    muta_assert(s->account_name_len <= MAX_ACC_NAME_LEN);
    memcpy(account_name, s->account_name, s->account_name_len);
    account_name[s->account_name_len] = 0;
    if (ar_check_player_character_name(s->account_name, s->account_name_len))
        return 1;
    account_name[s->account_name_len] = 0;
    if (_new_client_info(shard, account_name, s->socket_id, s->account_id,
        s->ip, s->token))
        r_msg.result = 1;
    else
    {
        r_msg.result = 0;
        LOG("Opened socket for client. Account name: %s, ip: "
            "%hhu.%hhu.%hhu.%hhu.", account_name, ((uint8*)&s->ip)[3],
            ((uint8*)&s->ip)[2], ((uint8*)&s->ip)[1], ((uint8*)&s->ip)[0]);
    }
    bbuf_t bb = _send_const_crypt_msg_to_shard(shard,
        FPROXY_MSG_OPENED_SOCKET_RESULT_SZ);
    if (!bb.max_bytes)
        return 1;
    int r = fproxy_msg_opened_socket_result_write_const_encrypted(&bb,
        &shard->cryptchan, &r_msg);
    muta_assert(!r);
    DEBUG_LOG("Sent accept player result %u to shard %s, .", r_msg.result,
        shard->name);
    return 0; /* Should this return non-zero if player already exists? */
}

static int
_handle_tproxy_msg_closed_socket(shard_t *shard,
    tproxy_msg_closed_socket_t *s)
{
    uint32 *client_info_index = socket_client_info_table_find(
        &shard->socket_client_info_table, s->socket_id);
    if (!client_info_index)
    {
        LOGF("Warning: socket not found!");
        return 0;
    }
    client_info_t *client_info = &shard->client_infos.all[*client_info_index];
    uint8 *ip = (uint8*)&client_info->ip;
    if (client_info->flags & CLIENT_INFO_FLAG_AUTHED)
        cl_disconnect(client_info->client_index);
    else
        _delete_client_info(shard, *client_info_index);
    LOGF("Closed proxy socket (account '%s', ip %hhu.%hhu.%hhu.%hhu).",
        client_info->account_name.str, ip[3], ip[2], ip[1], ip[0]);
    return 0;
}

static int
_handle_tproxy_msg_player_packet(shard_t *shard, tproxy_msg_player_packet_t *s)
{
    uint32 socket_id    = s->socket_id;
    size_t hash         = hashtable_hash(&socket_id, sizeof(socket_id));
    uint32 *client_info_index = hashtable_find(shard->socket_client_info_table,
        socket_id, hash);
    if (!client_info_index)
    {
        DEBUG_LOGF("Client info not found. Implement a reply message.");
        return 0;
    }
    client_info_t *client_info = &shard->client_infos.all[*client_info_index];
    if (!(client_info->flags & CLIENT_INFO_FLAG_AUTHED))
    {
        DEBUG_LOGF("Received forwardable data, but client was not connected.");
        return 0;
    }
    cl_forward_shard_packet(client_info->client_index, s->data, s->data_len);
    return 0;
}
