#ifndef MUTA_PROXY_CLIENTS_H
#define MUTA_PROXY_CLIENTS_H

#include "../../shared/ksys.h"
#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct accept_client_event_t    accept_client_event_t;
typedef struct read_client_event_t      read_client_event_t;

int
cl_init(void);

void
cl_destroy(void);

int
cl_start(void);

void
cl_flush(void);

/*-- Event handlers - these are called from the main thread */

void
cl_accept(accept_client_event_t *event);

void
cl_read(read_client_event_t *event);

void
cl_disconnect(uint32 socket_index);

void
cl_forward_shard_packet(uint32 client_index, const void *memory, int num_bytes);

#endif /* MUTA_PROXY_CLIENTS_H */
