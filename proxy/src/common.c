#include "event.h"
#include "common.h"
#include "../../shared/sv_common_defs.h"
#include "../../shared/common_utils.h"
#include "../../shared/crypt.h"

#define MAX_EVENTS 1024

static event_buf_t _com_event_buf;

event_buf_t *com_event_buf = &_com_event_buf;
config_t    com_config;

static void
_read_config(void);

int
com_init(void)
{
    int ret = 0;
    if (init_socket_api())
        {ret = 1; goto fail;}
    if (crypt_init())
        {ret = 2; goto fail;}
    _read_config();
    event_init(com_event_buf, sizeof(event_t), MAX_EVENTS);
    return ret;
    fail:
        LOGF("Failed with code %d.", ret);
        return ret;
}

void
com_destroy(void)
{
}

static void
_read_config(void)
{
    com_config.client_port          = DEFAULT_CLIENT_PORT;
    com_config.num_threads          = 1;
    com_config.accept_queue_size    = 64;
    com_config.max_clients          = 2048;
    com_config.client_timeout       = 10000;
}
