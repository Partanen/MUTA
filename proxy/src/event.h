#ifndef MUTA_PROXY_EVENT_H
#define MUTA_PROXY_EVENT_H

#include "../../shared/types.h"
#include "../../shared/net.h"

typedef struct read_shard_event_t      read_shard_event_t;
typedef struct accept_client_event_t    accept_client_event_t;
typedef struct read_client_event_t      read_client_event_t;
typedef struct event_t                  event_t;

enum event
{
    EVENT_READ_SHARD,
    EVENT_ACCEPT_CLIENT,
    EVENT_READ_CLIENT
};

struct read_shard_event_t
{
    uint32  shard;
    int     num_bytes;
    uint8   *memory;
};

struct accept_client_event_t
{
    socket_t    socket;
    addr_t      address;
};

struct read_client_event_t
{
    uint32  client_index;
    uint32  client_id;
    int     num_bytes;
    uint8   *memory;
};

struct event_t
{
    int type;
    union
    {
        read_shard_event_t      read_shard;
        accept_client_event_t   accept_client;
        read_client_event_t     read_client;
    };
};

#endif /* MUTA_PROXY_EVENT_H */
