/* Storage of shardter servers and connection handling to them */
#ifndef MUTA_PROXY_SHARD_H
#define MUTA_PROXY_SHARD_H

#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct read_shard_event_t   read_shard_event_t;
typedef struct cryptchan_t          cryptchan_t;

int
shard_init(void);

void
shard_destroy(void);

int
shard_start(void);

/*-- Event handlers for events dispatched to the main thread --*/

void
shard_read(read_shard_event_t *event);

void
shard_flush(void);

bool32
shard_check_client_ip(uint32 ip);

int
shard_check_client_auth_proof(const char *shard_name, const char *account_name,
    uint8 *token, cryptchan_t *cryptchan, uint32 client_index,
    uint32 *ret_shard_index, uint32 *ret_client_info_index);
/* shard_check_client_auth_proof()
 * Check if a shard was approved for connection by a shard. If successful,
 * the shard and  client info indices will be written to the pointer parameters.
 * The client info index can be used access certain information about the
 * client.
 * If successful, this function will set the client_info's state to "connected"
 * until shard_delete_shard_info() is called.
 * The account name MUST HAVE BEEN proven valid and be null-terminated. */

void
shard_forward_client_packet(uint32 shard, uint32 socket_id, void *memory,
    int num_bytes);

bool32
shard_exists(const char *shard_name);

void
shard_delete_client_info(uint32 shard, uint32 client_info, bool32 inform_shard);

#endif /* MUTA_PROXY_SHARD_H */
