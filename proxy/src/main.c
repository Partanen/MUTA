#include <stdio.h>
#include "shard.h"
#include "../../shared/kthp.h"
#include "../../shared/common_utils.h"
#include "../../shared/common_defs.h"
#include "../../shared/netpoll.h"
#include "../../shared/net.h"
#include "../../shared/get_opt.h"
#include "common.h"
#include "clients.h"
#include "event.h"

static bool32       _running;
static const char   *_help_str;

static void
_check_events(event_t *events, int num_events);

int main(int argc, char **argv)
{
    bool32              daemonize   = 0;
    get_opt_context_t   opt_ctx     = GET_OPT_CONTEXT_INITIALIZER;
    int                 opt;
    while ((opt = get_opt(&opt_ctx, argc, argv, "dh")) != -1)
    {
        switch (opt)
        {
        case 'd':
            daemonize = 1;
            break;
        case 'h':
            puts(_help_str);
            return 0;
        case '?':
            puts("Invalid command line arguments.");
            return 0;
        }
    }
    if (daemonize && muta_daemonize())
    {
        puts("Running in daemon mode failed");
        return 1;
    }
    LOG("MUTA Proxy Server");
    LOG("Initializing...");
    int ret = 0;
    if (com_init())
        {ret = 1; goto fail;}
    if (shard_init())
       {ret = 2; goto fail;}
    if (cl_init())
       {ret = 3; goto fail;}
    LOG("Done initializing.");
    LOG("Starting up shard connections...");
    if (shard_start())
        {ret = 4; goto fail;}
    LOG("Done starting up shard connections.");
    if (cl_start())
        {ret = 5; goto fail;}
    _running = 1;
    event_t events[64];
    while (_running)
    {
        int num_events = event_wait(com_event_buf, events, 64, -1);
        _check_events(events, num_events);
        shard_flush();
        cl_flush();
    }
    com_destroy();
    shard_destroy();
    cl_destroy();
    return ret;
    fail:
        LOGF("Init failed with code %d.", ret);
        sleep_ms(5000);
        return ret;
}

static void
_check_events(event_t *events, int num_events)
{
    event_t *e;
    for (int i = 0; i < num_events; ++i)
    {
        e = &events[i];
        switch (e->type)
        {
        case EVENT_READ_SHARD:
            shard_read(&e->read_shard);
            break;
        case EVENT_ACCEPT_CLIENT:
            cl_accept(&e->accept_client);
            break;
        case EVENT_READ_CLIENT:
            cl_read(&e->read_client);
            break;
        default:
            muta_assert(0);
        }
    }
}

static const char *_help_str =
    "MUTA Proxy Server\n"
    "Command line parameters:\n"
    "-h: Print this help dialogue.\n"
    "-d: Run in daemon mode (Linux only).";
