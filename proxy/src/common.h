#ifndef MUTA_PROXY_COMMON_H
#define MUTA_PROXY_COMMON_H
#include "../../shared/types.h"
#include <stdio.h>

/* Forward declaration(s) */
typedef struct event_buf_t  event_buf_t;
typedef struct config_t     config_t;

#define MAX_SHARDS 32
#define LOG(fmt_, ...) printf(fmt_ "\n", ##__VA_ARGS__)
#define LOGF(fmt_, ...) printf("%s: " fmt_ "\n", __func__, ##__VA_ARGS__)
#ifdef _MUTA_DEBUG
    #define DEBUG_LOG(fmt_, ...) printf("[DEBUG] " fmt_ "\n", ##__VA_ARGS__)
    #define DEBUG_LOGF(fmt_, ...) \
        printf("[DEBUG] %s: " fmt_ "\n", __func__, ##__VA_ARGS__)
#else
    #define DEBUG_LOG ((void)0)
    #define DEBUG_LOGF ((void)0)
#endif

struct config_t
{
    uint32  num_threads;
    uint16  client_port;
    uint32  accept_queue_size;
    uint32  max_clients;
    uint32  client_timeout; /* In milliseconds */
};

extern event_buf_t  *com_event_buf;
extern config_t     com_config;

int
com_init(void);

void
com_destroy(void);

#endif /* MUTA_PROXY_COMMON_H */
