include config.mk

EXE_NAME		=	muta_proxy.exe
INCLUDES		=	/I ..\libs\windows\include
C_FLAGS_COM		=	/W3 $(INCLUDES) /D_CRT_SECURE_NO_WARNINGS /EHsc
C_FLAGS_DB		=	$(C_FLAGS_COM) /D_MUTA_DEBUG /DEBUG /Od /Zi /Tc
C_FLAGS_OPT		=	$(C_FLAGS_COM) /Ox /GL /Tc
C_FLAGS			=	$(C_FLAGS_DB)
L_FLAGS_DB		=	/SUBSYSTEM:CONSOLE /DEBUG /LIBPATH:..\libs\windows\lib\$(ARCH)
L_FLAGS_OPT		=	/SUBSYSTEM:CONSOLE /LIBPATH:..\libs\windows\lib\x86 /LTCG
L_FLAGS			=	$(L_FLAGS_DB)
LIBS			=	Mswsock.lib libsodium.lib
COMPILE_OBJ		=	cl /c $(C_FLAGS) $< /Fo$@
ALL_DEPS		=	$(OBJS)
ALL				=	$(LINK_PROGRAM)
RM				=	del /Q /F
LINK_PROGRAM	=	link $(L_FLAGS) $(OBJS) $(LIBS) /OUT:$(RUN_DIR)\$(EXE_NAME)
S				=	\\

.default: all

run:
	cd rundir && $(EXE_NAME)
